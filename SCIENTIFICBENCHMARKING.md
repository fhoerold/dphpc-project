# Summary Scientific Benchmarking of Parallel Computing Systems
Source: [Paper](https://htor.inf.ethz.ch/publications/img/hoefler-scientific-benchmarking.pdf)

Aim: Interpretability of results. We call an experiment interpretable if it provides enough information to allow scientists to understand the experiment, draw own conclusions, assess their certainty, and possibly generalize results.

## Good practices

- What to report
    - Report motivation for the application (source code, software environment), input, the compiler (+ flags, libraries version, kernel, storage), the runtime environment, the machine (processor model, accelerator, ram, bus info, nic model, network info), and the measurement methodology.
    - Report min, max, varation, xx% quantile, medaian, arithmetic mean, distribution, etc. instead of only the mean.
    - Report sources of nondeterminism, noise (e.g. system: network background traffic, task scheduling, interrupts, job placement in the batch system, application: load balancing, cache misses, managed memory, JIT compilation, environment: other applications causing interference, or even the application’s stack layout)
- Metrics
    - Speedup (s = TA/TB, relative gain (faster) = 1 - s, base case should be clear, only report ratio with absolute values, can almost always be replaced by lower bounds on execution time e.g. an “ideal scaling” line)
    - Report units unambiguously (flop, flop/s, B=Byte, b=bit, MiB (i.e. i suffix) for base 2, follow recommendation from PARKBENCH committee / IEC 60027-2 standard)
    - Present all results (Do not cherry pich, Use all available resources (e.g. cores) even if it stops scaling, consider the whole application runtime and not only kernels, if existing benchmark suites are used, all benchmarks should be run)
- Summarizing
    -  Deterministic measurements (e.g., flop count) can be summarized with simple algebraic methods while nondeterministic measurements (e.g., runtime) requires statistical techniques.
    -  Use arithmetic mean to summarize costs (e.g. number of instructions). Use the harmonic mean for summarizing rates (e.g. flops per second). Use arithmetic mean for numerator and denominator seperately for rates if those are known (e.g. flops and seconds). Do not average ratios (percentage of system peak, summarize the costs or rates that the ratios base on instead). 
-  Distribution reporting
    -   Normal Distribution: Use standard deviation, Coefficient of Variation, Confidence intervals of the mean (CI especially useful for nondeterministic data, CI tells probability of mean lying inside interval (for normally distributed data)), etc. Check if normally distributed with a Q-Q plot or an analysis specific to the used statistics.
    -   Normalization: Use Log-normalization for log-distributed data. If the data is not normally distributed, it can be normalized by averaging intervals of length k until the result is normal (following the Central Limit Theorem (CLT)). For large enough sample sizes, one can assume normality for most mean-based metrics.
    -   Non-Normal Distribution: Normal distributions are only rarely observed when measuring computer performance, where most system effects lead to increased execution times.  Nonparametric metrics, such as the median or other percentiles, do not assume a specific distribution and are most robust. Additionally, use Confidence intervals of the median similar to CI around the mean for non-normal distributions.
    -   Removing outliers:  We suggest to avoid removal of outliers and instead use robust measures such as percentiles. If necessary, use Tukey’s method.
- Comparing Statistical Data
    - Goal: show statistically significant differences (-> null hypothesis assumes that they are equal).
    - The simplest method is to compare confidence intervals. If 1−alpha confidence intervals do not overlap, then one can be 1−alpha confident that there is a statistically significant difference.
    - To compare two means with one varying variable, one could use the t-test to compare two experiments or the one-factor analysis of variance (ANOVA) which generalizes the t-test to k experiments. The nonparametric Kruskal-Wallis one-way ANOVA test can be used to test if the medians of experiments following nonnormal distributions with one varying factor differ significantly. Other methods include: effect size, quantile regression,
-  Experimental Design
    -  We recommend factorial design to compare the influence of multiple factors. It is often desired to set up a fixed environment with as little variation as possible in order to focus on a specific factor. Consider parameters such as the allocation of nodes, process-to-node mappings, network or on-node interference, and other system effects.
    -  Report setup: Next to the exact system/machine you are using, report warmup of programs, warm vs. cold cache, and all other varying factors and their levels (e.g. several implementations perform better with 2^k processors).
    -  Papers should always indicate if experiments are using strong scaling (constant problem size) or weak scaling (problem size grows with the number of processes). 
    -  Time measurement: (Other measurements follow similar principles) report all measurement, (optional) synchronization, and summarization techniques. Measuring run times induces overheads (need to be <5%). For multiple runs use statistical methods on single events, i.e., before averaging run-time over multiple runs. Source of errors: asynchronicity and clock drift. For multiple runs over multiple processors, use again ANOVA to determine significant differences.
        -  Specific to MPI and OpenMP: Many evaluations use an (MPI or OpenMP) barrier to synchronize processes for time measurement. This may be unreliable because neither MPI nor OpenMP provides timing guarantees for their barrier calls. -> Use a simple delay window scheme.
-  Modeling
    -  In general, we can model any computer system’s capabilities as a k-dimensional space. Each feature is typically expressed as rate of peaks of that machine (i.e. show upper performance bounds).
    -  Other models include: Ideal linear speedup, Serial overheads (Amdahl’s law), and Parallel overheads.
    -  Use graphs, line-plots, histograms, boxplots, and violin plots depending on the number of levels and factors to show.  Only connect measurements by lines if they indicate trends and the interpolation is valid.
- LibSciBench
    - LibSciBench offers high-resolution timers for many architectures (currently x86, x86-64, PowerPC, and Sparc).
    - LibSciBench offers a window-based synchronization mechanism for OpenMP and MPI to synchronize processes based on real-time.
    - LibSciBench has many tools for data analysis.
