# DPHPC Project
**Winter Semester 2021 DPHPC Project<br/>**

Parallelised versions of Cholesky & Gram-Schmidt PolyBench benchmarks

## Polybench

## Where to find the algorithms
In the file [pipeline.ipynb](benchmark/pipeline.ipynb) you can find the name mapping that was used to create all graphs present in our report.

### Gram-Schmidt
| Implementation | File |
| ------ | ------ |
| PolyBench | [gramschmidt/src/gramschmidt.c](gramschmidt/src/gramschmidt.c) |
| B2GS | [gramschmidt/src/blocking_gram_mpi.c](gramschmidt/src/blocking_gram_mpi.c) |
| ICGS (MPI) | [gramschmidt/src/classical_gram_mpi.c](gramschmidt/src/classical_gram_mpi.c) |
| ICGS (OpenMP) | [gramschmidt/src/classical_gram_mpi.c](gramschmidt/src/classical_gram_mpi.c) |
| PMPI | [gramschmidt/src/polybench_mpi_gramschmidt_splitr.c](gramschmidt/src/polybench_mpi_gramschmidt_splitr.c) |
| PMPIO | [gramschmidt/src/polybench_mpi_gramschmidt_splitr_optimized.c](gramschmidt/src/polybench_mpi_gramschmidt_splitr_optimized.c) |

### Cholesky
| Implementation | File |
| ------ | ------ |
| PolyBench | [cholesky/src/cholesky.c](cholesky/src/cholesky.c) |
| MPIC | [cholesky/src/cholesky_mpi_ll_naive.c](cholesky/src/cholesky_mpi_ll_naive.c) |
| OMPC | [cholesky/src/cholesky_openmp_ll_naive.c](cholesky/src/cholesky_openmp_ll_naive.c) |
| OMPSIMDC | [cholesky/src/cholesky_openmp_simd_ll_naive.c](cholesky/src/cholesky_openmp_simd_ll_naive.c) |
| MPIBC | [cholesky/src/cholesky_blocked_mpi.c](cholesky/src/cholesky_blocked_mpi.c) |

## Install/Usage guides

### MPI
```bash
wget https://download.open-mpi.org/release/open-mpi/v4.1/openmpi-4.1.1.tar.bz2
tar -xjf openmpi-4.1.1.tar.bz2
cd openmpi-4.1.1
./configure --enable-mpi-cxx
make -j $(nproc)
sudo make install
```

### LibLsb
```bash
git clone https://github.com/spcl/liblsb
cd liblsb
./configure --with-mpi
make  
sudo make install  
sudo ldconfig
``` 

### CMake

We used CMake to compile/build all our files. You can find a `CMakeLists.txt` files in both the [cholesky](cholesky) and [gramschmidt](gramschmidt) folder.  

Simply run the following commands from the root of the repo to compile using CMake (the example is for Cholesky):  

```bash
cd cholesky
mkdir build && cd build
cmake ..
make
```

### Euler

How to run our files on Euler is explained in [euler.md](euler.md).

### Graphs

All graphs are created by going through the [pipeline.ipynb](benchmark/pipeline.ipynb) file in the [benchmark](benchmark) folder. 
Note that it expects a `dphpc-report` folder outside of this folder (see [overleaf.md](overleaf.md)).
