#!/bin/sh

# Load modules

env2lmod
module load gcc/8.2.0 openmpi/4.0.2 cmake/3.20.3

# Update search directories

echo "Adding $1 to search directories..."

case ":$LD_LIBRARY_PATH:" in
	*:$1/lib:*) echo "> LD_LIBRARY_PATH already configured";;
	*) export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$1/lib";;
esac

case ":$LIBRARY_PATH:" in
        *:$1/lib:*) echo "> LIBRARY_PATH already configured";;
        *) export LIBRARY_PATH="$LIBRARY_PATH:$1/lib";;
esac

case ":$CPATH:" in
        *:$1/include:*) echo "> CPATH already configured";;
        *) export CPATH="$CPATH:$1/include";;
esac

echo "Done"
