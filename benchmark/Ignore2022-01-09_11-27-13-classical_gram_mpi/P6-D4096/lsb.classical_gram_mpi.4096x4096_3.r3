# Sysname : Linux
# Nodename: eu-a6-001-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 10:31:09 2022
# Execution time and date (local): Sun Jan  9 11:31:09 2022
# MPI execution on rank 3 with 6 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 106843826.675781            0 
            3          4096          4096   gramschmidt           MPI          1 107346030.445312            5 
            3          4096          4096   gramschmidt           MPI          2 106546100.552734            6 
            3          4096          4096   gramschmidt           MPI          3 106699404.017578            0 
            3          4096          4096   gramschmidt           MPI          4 108579989.486328            7 
            3          4096          4096   gramschmidt           MPI          5 106830314.152344            2 
            3          4096          4096   gramschmidt           MPI          6 106534398.175781            2 
            3          4096          4096   gramschmidt           MPI          7 106350997.400391            2 
            3          4096          4096   gramschmidt           MPI          8 106429737.031250            8 
            3          4096          4096   gramschmidt           MPI          9 106297703.322266            4 
# Runtime: 1390.782203 s (overhead: 0.000003 %) 10 records
