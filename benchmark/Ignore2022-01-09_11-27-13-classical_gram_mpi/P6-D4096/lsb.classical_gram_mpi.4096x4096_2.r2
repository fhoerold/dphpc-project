# Sysname : Linux
# Nodename: eu-a6-001-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 10:31:09 2022
# Execution time and date (local): Sun Jan  9 11:31:09 2022
# MPI execution on rank 2 with 6 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 106837643.136719            0 
            2          4096          4096   gramschmidt           MPI          1 107339691.000000            4 
            2          4096          4096   gramschmidt           MPI          2 106539626.656250            4 
            2          4096          4096   gramschmidt           MPI          3 106693102.484375            0 
            2          4096          4096   gramschmidt           MPI          4 108573583.121094            6 
            2          4096          4096   gramschmidt           MPI          5 106823860.935547            2 
            2          4096          4096   gramschmidt           MPI          6 106528078.449219            0 
            2          4096          4096   gramschmidt           MPI          7 106344767.640625            0 
            2          4096          4096   gramschmidt           MPI          8 106423294.169922            6 
            2          4096          4096   gramschmidt           MPI          9 106291494.511719            0 
# Runtime: 1390.748180 s (overhead: 0.000002 %) 10 records
