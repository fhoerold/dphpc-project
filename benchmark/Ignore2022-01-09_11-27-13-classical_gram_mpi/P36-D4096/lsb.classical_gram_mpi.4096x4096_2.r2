# Sysname : Linux
# Nodename: eu-a6-002-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 12:11:15 2022
# Execution time and date (local): Sun Jan  9 13:11:15 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 30721679.625000            0 
            2          4096          4096   gramschmidt           MPI          1 30891898.009766            4 
            2          4096          4096   gramschmidt           MPI          2 31150041.892578            2 
            2          4096          4096   gramschmidt           MPI          3 30793220.531250            0 
            2          4096          4096   gramschmidt           MPI          4 30917422.900391            5 
            2          4096          4096   gramschmidt           MPI          5 30817772.625000            2 
            2          4096          4096   gramschmidt           MPI          6 31237966.416016            0 
            2          4096          4096   gramschmidt           MPI          7 30954792.710938            0 
            2          4096          4096   gramschmidt           MPI          8 30985787.414062           12 
            2          4096          4096   gramschmidt           MPI          9 30901995.560547            0 
# Runtime: 417.490283 s (overhead: 0.000006 %) 10 records
