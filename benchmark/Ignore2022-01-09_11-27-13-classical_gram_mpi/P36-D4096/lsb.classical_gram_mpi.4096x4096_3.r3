# Sysname : Linux
# Nodename: eu-a6-002-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 12:11:15 2022
# Execution time and date (local): Sun Jan  9 13:11:15 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 30688223.099609            0 
            3          4096          4096   gramschmidt           MPI          1 30858392.912109            4 
            3          4096          4096   gramschmidt           MPI          2 31116444.066406            2 
            3          4096          4096   gramschmidt           MPI          3 30759782.316406            0 
            3          4096          4096   gramschmidt           MPI          4 30884199.933594            6 
            3          4096          4096   gramschmidt           MPI          5 30784281.396484            0 
            3          4096          4096   gramschmidt           MPI          6 31204358.296875            0 
            3          4096          4096   gramschmidt           MPI          7 30920975.371094            0 
            3          4096          4096   gramschmidt           MPI          8 30952515.869141           13 
            3          4096          4096   gramschmidt           MPI          9 30868421.363281            0 
# Runtime: 417.042732 s (overhead: 0.000006 %) 10 records
