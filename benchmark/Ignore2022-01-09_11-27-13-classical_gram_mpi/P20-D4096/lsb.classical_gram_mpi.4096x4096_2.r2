# Sysname : Linux
# Nodename: eu-a6-006-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 11:01:09 2022
# Execution time and date (local): Sun Jan  9 12:01:09 2022
# MPI execution on rank 2 with 20 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 51954537.111328            0 
            2          4096          4096   gramschmidt           MPI          1 51204976.750000            4 
            2          4096          4096   gramschmidt           MPI          2 52109696.757812            5 
            2          4096          4096   gramschmidt           MPI          3 51929029.839844            0 
            2          4096          4096   gramschmidt           MPI          4 52074451.923828            5 
            2          4096          4096   gramschmidt           MPI          5 51143879.980469            0 
            2          4096          4096   gramschmidt           MPI          6 51070971.669922            0 
            2          4096          4096   gramschmidt           MPI          7 49006644.212891            2 
            2          4096          4096   gramschmidt           MPI          8 49986108.191406            7 
            2          4096          4096   gramschmidt           MPI          9 51678600.718750            3 
# Runtime: 669.889989 s (overhead: 0.000004 %) 10 records
