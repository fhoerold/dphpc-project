# Sysname : Linux
# Nodename: eu-a6-006-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 11:01:09 2022
# Execution time and date (local): Sun Jan  9 12:01:09 2022
# MPI execution on rank 3 with 20 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 51946203.912109            0 
            3          4096          4096   gramschmidt           MPI          1 51196707.285156            4 
            3          4096          4096   gramschmidt           MPI          2 52101379.060547            4 
            3          4096          4096   gramschmidt           MPI          3 51920684.988281            0 
            3          4096          4096   gramschmidt           MPI          4 52065253.978516            5 
            3          4096          4096   gramschmidt           MPI          5 51135389.138672            0 
            3          4096          4096   gramschmidt           MPI          6 51062444.789062            0 
            3          4096          4096   gramschmidt           MPI          7 48998755.654297            0 
            3          4096          4096   gramschmidt           MPI          8 49978276.472656            3 
            3          4096          4096   gramschmidt           MPI          9 51670566.363281            1 
# Runtime: 678.730868 s (overhead: 0.000003 %) 10 records
