# Sysname : Linux
# Nodename: eu-a6-006-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 11:32:45 2022
# Execution time and date (local): Sun Jan  9 12:32:45 2022
# MPI execution on rank 2 with 26 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 37815540.748047            2 
            2          4096          4096   gramschmidt           MPI          1 37754125.798828            5 
            2          4096          4096   gramschmidt           MPI          2 37326150.695312            8 
            2          4096          4096   gramschmidt           MPI          3 37787957.820312            0 
            2          4096          4096   gramschmidt           MPI          4 37266541.841797            6 
            2          4096          4096   gramschmidt           MPI          5 38056250.085938            0 
            2          4096          4096   gramschmidt           MPI          6 38181264.974609            1 
            2          4096          4096   gramschmidt           MPI          7 37645659.285156            2 
            2          4096          4096   gramschmidt           MPI          8 37839295.687500           13 
            2          4096          4096   gramschmidt           MPI          9 37987319.896484            0 
# Runtime: 514.678861 s (overhead: 0.000007 %) 10 records
