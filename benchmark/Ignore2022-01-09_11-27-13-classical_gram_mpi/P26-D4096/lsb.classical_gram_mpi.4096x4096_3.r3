# Sysname : Linux
# Nodename: eu-a6-006-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 11:32:45 2022
# Execution time and date (local): Sun Jan  9 12:32:45 2022
# MPI execution on rank 3 with 26 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 37787741.656250            0 
            3          4096          4096   gramschmidt           MPI          1 37726422.140625            5 
            3          4096          4096   gramschmidt           MPI          2 37298120.232422            2 
            3          4096          4096   gramschmidt           MPI          3 37760334.134766            0 
            3          4096          4096   gramschmidt           MPI          4 37239240.412109            5 
            3          4096          4096   gramschmidt           MPI          5 38027699.517578            4 
            3          4096          4096   gramschmidt           MPI          6 38153233.919922            0 
            3          4096          4096   gramschmidt           MPI          7 37617427.433594            2 
            3          4096          4096   gramschmidt           MPI          8 37811819.357422           13 
            3          4096          4096   gramschmidt           MPI          9 37959402.277344            0 
# Runtime: 512.285445 s (overhead: 0.000006 %) 10 records
