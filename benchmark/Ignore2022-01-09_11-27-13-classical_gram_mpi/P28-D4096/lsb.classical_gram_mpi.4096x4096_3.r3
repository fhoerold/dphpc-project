# Sysname : Linux
# Nodename: eu-a6-006-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 11:41:45 2022
# Execution time and date (local): Sun Jan  9 12:41:45 2022
# MPI execution on rank 3 with 28 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 34796263.289062            2 
            3          4096          4096   gramschmidt           MPI          1 34465501.253906            7 
            3          4096          4096   gramschmidt           MPI          2 34513354.509766            8 
            3          4096          4096   gramschmidt           MPI          3 34808079.470703            2 
            3          4096          4096   gramschmidt           MPI          4 34621165.822266            7 
            3          4096          4096   gramschmidt           MPI          5 34581992.828125            2 
            3          4096          4096   gramschmidt           MPI          6 34725480.480469            0 
            3          4096          4096   gramschmidt           MPI          7 34579537.322266            5 
            3          4096          4096   gramschmidt           MPI          8 34885250.398438            7 
            3          4096          4096   gramschmidt           MPI          9 34649376.765625            0 
# Runtime: 453.469758 s (overhead: 0.000009 %) 10 records
