# Sysname : Linux
# Nodename: eu-a6-006-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 11:41:45 2022
# Execution time and date (local): Sun Jan  9 12:41:45 2022
# MPI execution on rank 2 with 28 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 34795815.835938            0 
            2          4096          4096   gramschmidt           MPI          1 34465008.177734            7 
            2          4096          4096   gramschmidt           MPI          2 34513341.912109            6 
            2          4096          4096   gramschmidt           MPI          3 34807690.181641            2 
            2          4096          4096   gramschmidt           MPI          4 34621330.585938           15 
            2          4096          4096   gramschmidt           MPI          5 34581687.208984            0 
            2          4096          4096   gramschmidt           MPI          6 34725385.259766            1 
            2          4096          4096   gramschmidt           MPI          7 34579141.888672            2 
            2          4096          4096   gramschmidt           MPI          8 34884935.496094           11 
            2          4096          4096   gramschmidt           MPI          9 34649494.164062            0 
# Runtime: 454.480257 s (overhead: 0.000010 %) 10 records
