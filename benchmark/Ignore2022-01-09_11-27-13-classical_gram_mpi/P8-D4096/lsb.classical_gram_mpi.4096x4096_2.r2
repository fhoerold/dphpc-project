# Sysname : Linux
# Nodename: eu-a6-001-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 10:31:09 2022
# Execution time and date (local): Sun Jan  9 11:31:09 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 81555920.171875            0 
            2          4096          4096   gramschmidt           MPI          1 81541907.490234            4 
            2          4096          4096   gramschmidt           MPI          2 81608069.287109            4 
            2          4096          4096   gramschmidt           MPI          3 81725690.984375            0 
            2          4096          4096   gramschmidt           MPI          4 81631992.369141            3 
            2          4096          4096   gramschmidt           MPI          5 81955865.556641            0 
            2          4096          4096   gramschmidt           MPI          6 81944733.078125            0 
            2          4096          4096   gramschmidt           MPI          7 81805251.861328            0 
            2          4096          4096   gramschmidt           MPI          8 81693000.845703            4 
            2          4096          4096   gramschmidt           MPI          9 81909216.339844            0 
# Runtime: 1064.884481 s (overhead: 0.000001 %) 10 records
