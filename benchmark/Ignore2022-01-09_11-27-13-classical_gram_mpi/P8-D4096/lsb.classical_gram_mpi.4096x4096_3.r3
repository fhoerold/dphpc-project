# Sysname : Linux
# Nodename: eu-a6-001-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 10:31:09 2022
# Execution time and date (local): Sun Jan  9 11:31:09 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 81556013.253906            0 
            3          4096          4096   gramschmidt           MPI          1 81541900.052734            6 
            3          4096          4096   gramschmidt           MPI          2 81608172.871094            8 
            3          4096          4096   gramschmidt           MPI          3 81725846.076172            3 
            3          4096          4096   gramschmidt           MPI          4 81631920.300781            7 
            3          4096          4096   gramschmidt           MPI          5 81955827.419922            4 
            3          4096          4096   gramschmidt           MPI          6 81944665.230469            4 
            3          4096          4096   gramschmidt           MPI          7 81805276.876953            2 
            3          4096          4096   gramschmidt           MPI          8 81692904.111328            8 
            3          4096          4096   gramschmidt           MPI          9 81909303.199219            4 
# Runtime: 1065.844418 s (overhead: 0.000004 %) 10 records
