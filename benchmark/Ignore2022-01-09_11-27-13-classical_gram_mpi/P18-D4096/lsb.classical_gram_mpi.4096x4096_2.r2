# Sysname : Linux
# Nodename: eu-a6-011-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 11:00:24 2022
# Execution time and date (local): Sun Jan  9 12:00:24 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 52000882.632812            1 
            2          4096          4096   gramschmidt           MPI          1 51649167.246094            7 
            2          4096          4096   gramschmidt           MPI          2 50185850.556641            6 
            2          4096          4096   gramschmidt           MPI          3 48325934.078125            3 
            2          4096          4096   gramschmidt           MPI          4 46860267.158203            9 
            2          4096          4096   gramschmidt           MPI          5 44550739.421875            4 
            2          4096          4096   gramschmidt           MPI          6 44434200.136719            1 
            2          4096          4096   gramschmidt           MPI          7 43401702.748047            2 
            2          4096          4096   gramschmidt           MPI          8 43569483.894531            5 
            2          4096          4096   gramschmidt           MPI          9 44520165.380859            0 
# Runtime: 625.802570 s (overhead: 0.000006 %) 10 records
