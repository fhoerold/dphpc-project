# Sysname : Linux
# Nodename: eu-a6-011-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 11:00:24 2022
# Execution time and date (local): Sun Jan  9 12:00:24 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 52004341.941406            2 
            3          4096          4096   gramschmidt           MPI          1 51652532.468750            6 
            3          4096          4096   gramschmidt           MPI          2 50189126.968750            8 
            3          4096          4096   gramschmidt           MPI          3 48329336.371094            2 
            3          4096          4096   gramschmidt           MPI          4 46862594.085938            7 
            3          4096          4096   gramschmidt           MPI          5 44553980.378906            0 
            3          4096          4096   gramschmidt           MPI          6 44437301.703125            0 
            3          4096          4096   gramschmidt           MPI          7 43404968.386719            0 
            3          4096          4096   gramschmidt           MPI          8 43571695.113281            6 
            3          4096          4096   gramschmidt           MPI          9 44523254.748047            2 
# Runtime: 624.805154 s (overhead: 0.000005 %) 10 records
