# Sysname : Linux
# Nodename: eu-a6-001-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 10:31:09 2022
# Execution time and date (local): Sun Jan  9 11:31:09 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 213440253.996094            0 
            2          4096          4096   gramschmidt           MPI          1 213653252.849609            4 
            2          4096          4096   gramschmidt           MPI          2 213171318.060547           13 
            2          4096          4096   gramschmidt           MPI          3 213595191.802734            2 
            2          4096          4096   gramschmidt           MPI          4 214082095.695312           11 
            2          4096          4096   gramschmidt           MPI          5 213458218.316406            3 
            2          4096          4096   gramschmidt           MPI          6 213478595.326172            3 
            2          4096          4096   gramschmidt           MPI          7 214112623.589844            2 
            2          4096          4096   gramschmidt           MPI          8 213229804.255859            8 
            2          4096          4096   gramschmidt           MPI          9 213553306.367188            2 
# Runtime: 2786.481440 s (overhead: 0.000002 %) 10 records
