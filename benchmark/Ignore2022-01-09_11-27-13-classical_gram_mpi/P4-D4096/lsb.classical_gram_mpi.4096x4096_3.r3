# Sysname : Linux
# Nodename: eu-a6-001-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 10:31:09 2022
# Execution time and date (local): Sun Jan  9 11:31:09 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 213848250.501953            0 
            3          4096          4096   gramschmidt           MPI          1 214061241.697266            5 
            3          4096          4096   gramschmidt           MPI          2 213577778.943359            6 
            3          4096          4096   gramschmidt           MPI          3 214002897.082031            1 
            3          4096          4096   gramschmidt           MPI          4 214491026.941406            7 
            3          4096          4096   gramschmidt           MPI          5 213865772.484375            1 
            3          4096          4096   gramschmidt           MPI          6 213886062.417969            2 
            3          4096          4096   gramschmidt           MPI          7 214521678.521484            1 
            3          4096          4096   gramschmidt           MPI          8 213637394.791016            9 
            3          4096          4096   gramschmidt           MPI          9 213960766.246094            1 
# Runtime: 2782.704706 s (overhead: 0.000001 %) 10 records
