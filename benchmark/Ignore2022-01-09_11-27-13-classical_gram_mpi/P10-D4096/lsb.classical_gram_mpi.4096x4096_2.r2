# Sysname : Linux
# Nodename: eu-a6-001-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 10:31:09 2022
# Execution time and date (local): Sun Jan  9 11:31:09 2022
# MPI execution on rank 2 with 10 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 71877423.003906            0 
            2          4096          4096   gramschmidt           MPI          1 72910451.410156            8 
            2          4096          4096   gramschmidt           MPI          2 71005595.091797            4 
            2          4096          4096   gramschmidt           MPI          3 71020434.144531            0 
            2          4096          4096   gramschmidt           MPI          4 71136405.109375            8 
            2          4096          4096   gramschmidt           MPI          5 71018976.421875            5 
            2          4096          4096   gramschmidt           MPI          6 71094567.857422            3 
            2          4096          4096   gramschmidt           MPI          7 71164153.892578            4 
            2          4096          4096   gramschmidt           MPI          8 71001338.064453            4 
            2          4096          4096   gramschmidt           MPI          9 71127724.865234            0 
# Runtime: 932.947828 s (overhead: 0.000004 %) 10 records
