# Sysname : Linux
# Nodename: eu-a6-001-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 10:31:09 2022
# Execution time and date (local): Sun Jan  9 11:31:09 2022
# MPI execution on rank 3 with 10 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 71880938.265625            2 
            3          4096          4096   gramschmidt           MPI          1 72913738.751953            5 
            3          4096          4096   gramschmidt           MPI          2 71007718.880859            4 
            3          4096          4096   gramschmidt           MPI          3 71023782.810547            3 
            3          4096          4096   gramschmidt           MPI          4 71139497.867188            7 
            3          4096          4096   gramschmidt           MPI          5 71021963.603516            3 
            3          4096          4096   gramschmidt           MPI          6 71097554.539062            3 
            3          4096          4096   gramschmidt           MPI          7 71167061.058594            5 
            3          4096          4096   gramschmidt           MPI          8 71004314.246094            7 
            3          4096          4096   gramschmidt           MPI          9 71130765.957031            4 
# Runtime: 932.937396 s (overhead: 0.000005 %) 10 records
