# Sysname : Linux
# Nodename: eu-a6-009-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 11:57:47 2022
# Execution time and date (local): Sun Jan  9 12:57:47 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 29657042.179688            0 
            3          4096          4096   gramschmidt           MPI          1 29680330.330078            5 
            3          4096          4096   gramschmidt           MPI          2 29818773.976562            2 
            3          4096          4096   gramschmidt           MPI          3 29760367.394531            0 
            3          4096          4096   gramschmidt           MPI          4 29694721.183594            7 
            3          4096          4096   gramschmidt           MPI          5 29817267.273438            2 
            3          4096          4096   gramschmidt           MPI          6 29711518.578125            0 
            3          4096          4096   gramschmidt           MPI          7 29750460.386719            2 
            3          4096          4096   gramschmidt           MPI          8 29693233.703125            6 
            3          4096          4096   gramschmidt           MPI          9 30054900.154297            2 
# Runtime: 401.706744 s (overhead: 0.000006 %) 10 records
