# Sysname : Linux
# Nodename: eu-a6-009-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 11:57:47 2022
# Execution time and date (local): Sun Jan  9 12:57:47 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 29655149.451172            0 
            2          4096          4096   gramschmidt           MPI          1 29677973.501953            4 
            2          4096          4096   gramschmidt           MPI          2 29816866.875000            2 
            2          4096          4096   gramschmidt           MPI          3 29758108.998047            0 
            2          4096          4096   gramschmidt           MPI          4 29692775.701172            6 
            2          4096          4096   gramschmidt           MPI          5 29814856.472656            2 
            2          4096          4096   gramschmidt           MPI          6 29709653.343750            0 
            2          4096          4096   gramschmidt           MPI          7 29748158.638672            2 
            2          4096          4096   gramschmidt           MPI          8 29691381.650391            7 
            2          4096          4096   gramschmidt           MPI          9 30052676.197266            2 
# Runtime: 401.724259 s (overhead: 0.000006 %) 10 records
