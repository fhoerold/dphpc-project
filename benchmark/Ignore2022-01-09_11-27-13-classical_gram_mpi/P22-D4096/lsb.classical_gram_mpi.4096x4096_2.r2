# Sysname : Linux
# Nodename: eu-a6-006-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 11:13:10 2022
# Execution time and date (local): Sun Jan  9 12:13:10 2022
# MPI execution on rank 2 with 22 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 42682455.441406            0 
            2          4096          4096   gramschmidt           MPI          1 42941932.970703            6 
            2          4096          4096   gramschmidt           MPI          2 42430499.582031            7 
            2          4096          4096   gramschmidt           MPI          3 42827238.392578            2 
            2          4096          4096   gramschmidt           MPI          4 42713898.955078            6 
            2          4096          4096   gramschmidt           MPI          5 42484443.171875            0 
            2          4096          4096   gramschmidt           MPI          6 42623072.019531            0 
            2          4096          4096   gramschmidt           MPI          7 42809228.113281            0 
            2          4096          4096   gramschmidt           MPI          8 42612151.769531            5 
            2          4096          4096   gramschmidt           MPI          9 42503925.824219            0 
# Runtime: 558.885194 s (overhead: 0.000005 %) 10 records
