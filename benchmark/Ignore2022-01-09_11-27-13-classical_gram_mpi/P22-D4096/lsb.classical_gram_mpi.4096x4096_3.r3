# Sysname : Linux
# Nodename: eu-a6-006-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 11:13:10 2022
# Execution time and date (local): Sun Jan  9 12:13:10 2022
# MPI execution on rank 3 with 22 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 42659380.031250            0 
            3          4096          4096   gramschmidt           MPI          1 42918761.351562            5 
            3          4096          4096   gramschmidt           MPI          2 42406913.832031            4 
            3          4096          4096   gramschmidt           MPI          3 42804105.423828            0 
            3          4096          4096   gramschmidt           MPI          4 42690638.031250            6 
            3          4096          4096   gramschmidt           MPI          5 42461473.880859            0 
            3          4096          4096   gramschmidt           MPI          6 42599290.369141            0 
            3          4096          4096   gramschmidt           MPI          7 42785940.943359            0 
            3          4096          4096   gramschmidt           MPI          8 42589073.898438            7 
            3          4096          4096   gramschmidt           MPI          9 42480245.642578            2 
# Runtime: 560.557979 s (overhead: 0.000004 %) 10 records
