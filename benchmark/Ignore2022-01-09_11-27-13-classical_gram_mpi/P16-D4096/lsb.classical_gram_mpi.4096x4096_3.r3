# Sysname : Linux
# Nodename: eu-a6-006-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 10:49:10 2022
# Execution time and date (local): Sun Jan  9 11:49:10 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 52046117.312500            0 
            3          4096          4096   gramschmidt           MPI          1 52179739.904297            4 
            3          4096          4096   gramschmidt           MPI          2 52136634.326172            4 
            3          4096          4096   gramschmidt           MPI          3 52447832.962891            2 
            3          4096          4096   gramschmidt           MPI          4 52030509.660156           11 
            3          4096          4096   gramschmidt           MPI          5 52136129.667969            3 
            3          4096          4096   gramschmidt           MPI          6 52138207.318359            0 
            3          4096          4096   gramschmidt           MPI          7 52078129.447266            3 
            3          4096          4096   gramschmidt           MPI          8 52123511.677734           10 
            3          4096          4096   gramschmidt           MPI          9 52160000.363281            3 
# Runtime: 691.599367 s (overhead: 0.000006 %) 10 records
