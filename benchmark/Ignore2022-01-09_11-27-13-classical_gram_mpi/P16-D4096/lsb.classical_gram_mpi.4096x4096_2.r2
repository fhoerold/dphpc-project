# Sysname : Linux
# Nodename: eu-a6-006-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 10:49:10 2022
# Execution time and date (local): Sun Jan  9 11:49:10 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 52044287.175781            1 
            2          4096          4096   gramschmidt           MPI          1 52178016.714844            4 
            2          4096          4096   gramschmidt           MPI          2 52134923.994141            4 
            2          4096          4096   gramschmidt           MPI          3 52446927.109375            0 
            2          4096          4096   gramschmidt           MPI          4 52028807.568359            8 
            2          4096          4096   gramschmidt           MPI          5 52134286.761719            2 
            2          4096          4096   gramschmidt           MPI          6 52136399.007812            0 
            2          4096          4096   gramschmidt           MPI          7 52076363.599609            2 
            2          4096          4096   gramschmidt           MPI          8 52121782.240234            6 
            2          4096          4096   gramschmidt           MPI          9 52158306.980469            3 
# Runtime: 691.607864 s (overhead: 0.000004 %) 10 records
