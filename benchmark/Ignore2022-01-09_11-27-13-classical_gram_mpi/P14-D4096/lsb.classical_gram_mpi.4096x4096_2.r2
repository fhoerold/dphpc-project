# Sysname : Linux
# Nodename: eu-a6-005-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 10:46:10 2022
# Execution time and date (local): Sun Jan  9 11:46:10 2022
# MPI execution on rank 2 with 14 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 60686297.087891            0 
            2          4096          4096   gramschmidt           MPI          1 59759760.703125            7 
            2          4096          4096   gramschmidt           MPI          2 61481941.888672           10 
            2          4096          4096   gramschmidt           MPI          3 61469227.914062            3 
            2          4096          4096   gramschmidt           MPI          4 61643518.617188           10 
            2          4096          4096   gramschmidt           MPI          5 61577537.978516            3 
            2          4096          4096   gramschmidt           MPI          6 61943942.412109            2 
            2          4096          4096   gramschmidt           MPI          7 62024983.027344            2 
            2          4096          4096   gramschmidt           MPI          8 61481588.826172            9 
            2          4096          4096   gramschmidt           MPI          9 61150268.337891            3 
# Runtime: 801.150248 s (overhead: 0.000006 %) 10 records
