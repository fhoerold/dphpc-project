# Sysname : Linux
# Nodename: eu-a6-005-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 10:46:10 2022
# Execution time and date (local): Sun Jan  9 11:46:10 2022
# MPI execution on rank 3 with 14 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 60690428.169922            2 
            3          4096          4096   gramschmidt           MPI          1 59764357.197266            5 
            3          4096          4096   gramschmidt           MPI          2 61486561.847656            8 
            3          4096          4096   gramschmidt           MPI          3 61473902.343750            2 
            3          4096          4096   gramschmidt           MPI          4 61648123.326172            4 
            3          4096          4096   gramschmidt           MPI          5 61582102.173828            0 
            3          4096          4096   gramschmidt           MPI          6 61948679.792969            0 
            3          4096          4096   gramschmidt           MPI          7 62029708.833984            0 
            3          4096          4096   gramschmidt           MPI          8 61486255.060547            8 
            3          4096          4096   gramschmidt           MPI          9 61154925.066406            2 
# Runtime: 801.158448 s (overhead: 0.000004 %) 10 records
