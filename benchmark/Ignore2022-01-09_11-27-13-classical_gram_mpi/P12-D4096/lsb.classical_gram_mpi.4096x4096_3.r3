# Sysname : Linux
# Nodename: eu-a6-005-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 10:31:09 2022
# Execution time and date (local): Sun Jan  9 11:31:09 2022
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 66424814.361328            0 
            3          4096          4096   gramschmidt           MPI          1 67937298.669922            4 
            3          4096          4096   gramschmidt           MPI          2 65704398.214844            4 
            3          4096          4096   gramschmidt           MPI          3 65748989.099609            0 
            3          4096          4096   gramschmidt           MPI          4 68447558.386719            3 
            3          4096          4096   gramschmidt           MPI          5 66153201.470703            0 
            3          4096          4096   gramschmidt           MPI          6 66168671.533203            0 
            3          4096          4096   gramschmidt           MPI          7 71055805.146484            0 
            3          4096          4096   gramschmidt           MPI          8 65940730.031250            5 
            3          4096          4096   gramschmidt           MPI          9 66012618.789062            1 
# Runtime: 880.622161 s (overhead: 0.000002 %) 10 records
