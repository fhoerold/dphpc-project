# Sysname : Linux
# Nodename: eu-a6-005-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 10:31:09 2022
# Execution time and date (local): Sun Jan  9 11:31:09 2022
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 66421775.677734            0 
            2          4096          4096   gramschmidt           MPI          1 67934205.150391            5 
            2          4096          4096   gramschmidt           MPI          2 65701305.792969            4 
            2          4096          4096   gramschmidt           MPI          3 65745869.312500            1 
            2          4096          4096   gramschmidt           MPI          4 68444339.208984            5 
            2          4096          4096   gramschmidt           MPI          5 66150032.968750            1 
            2          4096          4096   gramschmidt           MPI          6 66165532.009766            1 
            2          4096          4096   gramschmidt           MPI          7 71052612.304688            0 
            2          4096          4096   gramschmidt           MPI          8 65937592.660156            8 
            2          4096          4096   gramschmidt           MPI          9 66009433.003906            1 
# Runtime: 880.612685 s (overhead: 0.000003 %) 10 records
