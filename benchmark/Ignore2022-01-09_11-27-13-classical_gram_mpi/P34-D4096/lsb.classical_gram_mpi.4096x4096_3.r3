# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 12:04:44 2022
# Execution time and date (local): Sun Jan  9 13:04:44 2022
# MPI execution on rank 3 with 34 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 28047581.324219            0 
            3          4096          4096   gramschmidt           MPI          1 28071743.396484            7 
            3          4096          4096   gramschmidt           MPI          2 27824752.972656            2 
            3          4096          4096   gramschmidt           MPI          3 28232731.812500            2 
            3          4096          4096   gramschmidt           MPI          4 27786412.433594            6 
            3          4096          4096   gramschmidt           MPI          5 28320824.503906            0 
            3          4096          4096   gramschmidt           MPI          6 27634440.199219            0 
            3          4096          4096   gramschmidt           MPI          7 27984502.269531            0 
            3          4096          4096   gramschmidt           MPI          8 27877542.812500            8 
            3          4096          4096   gramschmidt           MPI          9 28419690.640625            0 
# Runtime: 376.645977 s (overhead: 0.000007 %) 10 records
