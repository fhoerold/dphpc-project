# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 12:04:44 2022
# Execution time and date (local): Sun Jan  9 13:04:44 2022
# MPI execution on rank 2 with 34 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 28051317.890625            0 
            2          4096          4096   gramschmidt           MPI          1 28074978.789062            8 
            2          4096          4096   gramschmidt           MPI          2 27828472.195312            1 
            2          4096          4096   gramschmidt           MPI          3 28236013.326172            0 
            2          4096          4096   gramschmidt           MPI          4 27790148.140625            5 
            2          4096          4096   gramschmidt           MPI          5 28324208.378906            3 
            2          4096          4096   gramschmidt           MPI          6 27638073.279297            0 
            2          4096          4096   gramschmidt           MPI          7 27987788.753906            4 
            2          4096          4096   gramschmidt           MPI          8 27881045.464844            6 
            2          4096          4096   gramschmidt           MPI          9 28423374.794922            2 
# Runtime: 368.713822 s (overhead: 0.000008 %) 10 records
