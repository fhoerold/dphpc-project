# Sysname : Linux
# Nodename: eu-a6-009-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 11:49:45 2022
# Execution time and date (local): Sun Jan  9 12:49:45 2022
# MPI execution on rank 2 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 34171086.884766            2 
            2          4096          4096   gramschmidt           MPI          1 33974465.765625            6 
            2          4096          4096   gramschmidt           MPI          2 33931200.580078            2 
            2          4096          4096   gramschmidt           MPI          3 33809648.167969            0 
            2          4096          4096   gramschmidt           MPI          4 33952404.136719            7 
            2          4096          4096   gramschmidt           MPI          5 33499498.130859            0 
            2          4096          4096   gramschmidt           MPI          6 33942470.642578            0 
            2          4096          4096   gramschmidt           MPI          7 34239808.580078            2 
            2          4096          4096   gramschmidt           MPI          8 33717676.345703            7 
            2          4096          4096   gramschmidt           MPI          9 34190884.648438            0 
# Runtime: 451.330265 s (overhead: 0.000006 %) 10 records
