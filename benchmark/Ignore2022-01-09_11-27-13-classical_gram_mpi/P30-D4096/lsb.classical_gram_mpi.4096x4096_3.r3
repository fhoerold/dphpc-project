# Sysname : Linux
# Nodename: eu-a6-009-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 11:49:45 2022
# Execution time and date (local): Sun Jan  9 12:49:45 2022
# MPI execution on rank 3 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 34172107.394531            2 
            3          4096          4096   gramschmidt           MPI          1 33974987.595703            5 
            3          4096          4096   gramschmidt           MPI          2 33932350.277344            5 
            3          4096          4096   gramschmidt           MPI          3 33810505.841797            3 
            3          4096          4096   gramschmidt           MPI          4 33953505.568359            5 
            3          4096          4096   gramschmidt           MPI          5 33500119.769531            0 
            3          4096          4096   gramschmidt           MPI          6 33943511.562500            0 
            3          4096          4096   gramschmidt           MPI          7 34240969.689453            0 
            3          4096          4096   gramschmidt           MPI          8 33718241.464844            6 
            3          4096          4096   gramschmidt           MPI          9 34191894.978516            0 
# Runtime: 447.326226 s (overhead: 0.000006 %) 10 records
