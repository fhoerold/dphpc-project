# Sysname : Linux
# Nodename: eu-a6-006-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 11:23:10 2022
# Execution time and date (local): Sun Jan  9 12:23:10 2022
# MPI execution on rank 3 with 24 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 40627480.968750            0 
            3          4096          4096   gramschmidt           MPI          1 41221305.882812            4 
            3          4096          4096   gramschmidt           MPI          2 41062008.621094            9 
            3          4096          4096   gramschmidt           MPI          3 41095361.914062            0 
            3          4096          4096   gramschmidt           MPI          4 40611622.373047            4 
            3          4096          4096   gramschmidt           MPI          5 40954130.994141            0 
            3          4096          4096   gramschmidt           MPI          6 41157628.607422            0 
            3          4096          4096   gramschmidt           MPI          7 40750395.484375            0 
            3          4096          4096   gramschmidt           MPI          8 41068131.279297           12 
            3          4096          4096   gramschmidt           MPI          9 41285476.714844            3 
# Runtime: 541.439693 s (overhead: 0.000006 %) 10 records
