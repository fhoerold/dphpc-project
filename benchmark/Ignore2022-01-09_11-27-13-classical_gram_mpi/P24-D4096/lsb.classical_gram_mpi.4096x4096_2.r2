# Sysname : Linux
# Nodename: eu-a6-006-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 11:23:10 2022
# Execution time and date (local): Sun Jan  9 12:23:10 2022
# MPI execution on rank 2 with 24 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 40650365.701172            0 
            2          4096          4096   gramschmidt           MPI          1 41245042.203125            6 
            2          4096          4096   gramschmidt           MPI          2 41085132.013672            7 
            2          4096          4096   gramschmidt           MPI          3 41118462.138672            0 
            2          4096          4096   gramschmidt           MPI          4 40635087.164062            6 
            2          4096          4096   gramschmidt           MPI          5 40977154.365234            0 
            2          4096          4096   gramschmidt           MPI          6 41180894.062500            3 
            2          4096          4096   gramschmidt           MPI          7 40774010.683594            0 
            2          4096          4096   gramschmidt           MPI          8 41091242.042969            7 
            2          4096          4096   gramschmidt           MPI          9 41308553.472656            3 
# Runtime: 537.766989 s (overhead: 0.000006 %) 10 records
