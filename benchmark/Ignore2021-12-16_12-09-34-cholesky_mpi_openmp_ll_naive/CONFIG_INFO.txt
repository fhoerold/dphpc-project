Invocation: ../benchmark.sh -o /cluster/home/forstesa/repos/dphpc-project/cholesky/benchmark --openmp --mpi 4 -d 10 11 12 13 -p 4 8 16 32 ../build/cholesky_mpi_openmp_ll_naive
Benchmark 'cholesky_mpi_openmp_ll_naive' for
> Dimensions:		1024, 2048, 4096, 8192, 
> Number of processors:	4, 8, 16, 32, 
> Use Fullnode:		false
> CPU Model:		XeonGold_6150
> Use MPI:		true
> Use OpenMP:		true

Timestamp: 2021-12-16_12-09-34
