# Sysname : Linux
# Nodename: eu-a6-008-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 12:52:16 2021
# Execution time and date (local): Thu Dec 16 13:52:16 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 17025252.722656            0 
            2          8192           MPI          1 16956671.031250            5 
            2          8192           MPI          2 17009491.238281            1 
            2          8192           MPI          3 16968349.235352            0 
            2          8192           MPI          4 16950369.468750            5 
            2          8192           MPI          5 17024746.215820            0 
            2          8192           MPI          6 16924785.971680            0 
            2          8192           MPI          7 16923688.759766            0 
            2          8192           MPI          8 16925445.141602            6 
            2          8192           MPI          9 17238302.135742            0 
# Runtime: 211.478789 s (overhead: 0.000008 %) 10 records
