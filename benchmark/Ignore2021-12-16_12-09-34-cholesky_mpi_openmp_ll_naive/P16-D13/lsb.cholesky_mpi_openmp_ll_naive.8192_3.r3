# Sysname : Linux
# Nodename: eu-a6-008-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 12:52:16 2021
# Execution time and date (local): Thu Dec 16 13:52:16 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 17024504.559570            0 
            3          8192           MPI          1 16955932.252930            5 
            3          8192           MPI          2 17008751.219727            6 
            3          8192           MPI          3 16967606.985352            0 
            3          8192           MPI          4 16949630.439453            5 
            3          8192           MPI          5 17023973.618164            0 
            3          8192           MPI          6 16924046.769531            0 
            3          8192           MPI          7 16922952.052734            0 
            3          8192           MPI          8 16924707.662109            8 
            3          8192           MPI          9 17237554.786133            0 
# Runtime: 211.468462 s (overhead: 0.000011 %) 10 records
