# Sysname : Linux
# Nodename: eu-a6-007-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 12:49:03 2021
# Execution time and date (local): Thu Dec 16 13:49:03 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 31390334.425781            0 
            2          8192           MPI          1 31606405.378906            3 
            2          8192           MPI          2 31540102.501953            6 
            2          8192           MPI          3 32777536.256836            1 
            2          8192           MPI          4 33337838.285156            6 
            2          8192           MPI          5 33814192.360352            2 
            2          8192           MPI          6 33435489.204102            0 
            2          8192           MPI          7 33391640.698242            1 
            2          8192           MPI          8 33460075.907227            4 
            2          8192           MPI          9 33422256.774414            0 
# Runtime: 391.484994 s (overhead: 0.000006 %) 10 records
