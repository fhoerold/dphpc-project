# Sysname : Linux
# Nodename: eu-a6-007-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 12:49:03 2021
# Execution time and date (local): Thu Dec 16 13:49:03 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 31385949.755859            0 
            3          8192           MPI          1 31601988.928711            5 
            3          8192           MPI          2 31535698.064453            1 
            3          8192           MPI          3 32772952.162109            0 
            3          8192           MPI          4 33333179.882812            4 
            3          8192           MPI          5 33809511.622070            0 
            3          8192           MPI          6 33430828.449219            0 
            3          8192           MPI          7 33386983.209961            0 
            3          8192           MPI          8 33455409.884766            3 
            3          8192           MPI          9 33417562.519531            0 
# Runtime: 394.428669 s (overhead: 0.000003 %) 10 records
