# Sysname : Linux
# Nodename: eu-a6-002-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 11:20:24 2021
# Execution time and date (local): Thu Dec 16 12:20:24 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 2076600.656250            0 
            3          4096           MPI          1 2074341.473633            3 
            3          4096           MPI          2 2071240.349609            1 
            3          4096           MPI          3 2071946.925781            0 
            3          4096           MPI          4 2071919.576172            5 
            3          4096           MPI          5 2109800.259766            0 
            3          4096           MPI          6 2086954.121094            0 
            3          4096           MPI          7 2090657.894531            0 
            3          4096           MPI          8 2079626.011719            5 
            3          4096           MPI          9 2161232.097656            0 
# Runtime: 25.243347 s (overhead: 0.000055 %) 10 records
