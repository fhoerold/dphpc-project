# Sysname : Linux
# Nodename: eu-a6-002-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 11:20:24 2021
# Execution time and date (local): Thu Dec 16 12:20:24 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 2076541.955078            0 
            2          4096           MPI          1 2074656.074219            4 
            2          4096           MPI          2 2071177.075195            1 
            2          4096           MPI          3 2071876.788086            0 
            2          4096           MPI          4 2071859.199219            4 
            2          4096           MPI          5 2109735.413086            0 
            2          4096           MPI          6 2086892.613281            0 
            2          4096           MPI          7 2090587.837891            0 
            2          4096           MPI          8 2079559.200195            6 
            2          4096           MPI          9 2161168.420898            0 
# Runtime: 25.240897 s (overhead: 0.000059 %) 10 records
