# Sysname : Linux
# Nodename: eu-a6-002-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 11:12:20 2021
# Execution time and date (local): Thu Dec 16 12:12:20 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 306966.514648            0 
            3          2048           MPI          1 306035.862305            1 
            3          2048           MPI          2 305410.368164            1 
            3          2048           MPI          3 329854.978516            0 
            3          2048           MPI          4 306046.558594            0 
            3          2048           MPI          5 322908.342773            0 
            3          2048           MPI          6 306144.942383            0 
            3          2048           MPI          7 305877.510742            0 
            3          2048           MPI          8 305724.388672            0 
            3          2048           MPI          9 306226.284180            0 
# Runtime: 3.802091 s (overhead: 0.000053 %) 10 records
