# Sysname : Linux
# Nodename: eu-a6-002-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 11:12:20 2021
# Execution time and date (local): Thu Dec 16 12:12:20 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 307278.603516            0 
            2          2048           MPI          1 306036.975586            1 
            2          2048           MPI          2 305410.920898            0 
            2          2048           MPI          3 329856.014648            0 
            2          2048           MPI          4 306048.044922            1 
            2          2048           MPI          5 323258.969727            0 
            2          2048           MPI          6 306144.965820            0 
            2          2048           MPI          7 305877.738281            0 
            2          2048           MPI          8 305724.355469            1 
            2          2048           MPI          9 306227.223633            0 
# Runtime: 3.802673 s (overhead: 0.000079 %) 10 records
