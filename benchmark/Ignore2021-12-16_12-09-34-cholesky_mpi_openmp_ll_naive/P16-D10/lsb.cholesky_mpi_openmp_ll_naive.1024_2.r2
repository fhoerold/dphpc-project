# Sysname : Linux
# Nodename: eu-a6-002-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 11:11:19 2021
# Execution time and date (local): Thu Dec 16 12:11:19 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 58963.791016            0 
            2          1024           MPI          1 58068.541992            0 
            2          1024           MPI          2 58294.444336            0 
            2          1024           MPI          3 58731.790039            0 
            2          1024           MPI          4 57732.690430            0 
            2          1024           MPI          5 58406.987305            0 
            2          1024           MPI          6 57660.356445            0 
            2          1024           MPI          7 57856.768555            0 
            2          1024           MPI          8 58637.436523            0 
            2          1024           MPI          9 58017.308594            0 
# Runtime: 0.704134 s (overhead: 0.000000 %) 10 records
