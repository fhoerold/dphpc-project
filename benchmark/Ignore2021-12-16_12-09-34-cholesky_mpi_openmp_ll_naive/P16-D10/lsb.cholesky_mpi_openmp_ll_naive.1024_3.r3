# Sysname : Linux
# Nodename: eu-a6-002-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 11:11:19 2021
# Execution time and date (local): Thu Dec 16 12:11:19 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 58655.985352            0 
            3          1024           MPI          1 58074.470703            2 
            3          1024           MPI          2 58300.387695            0 
            3          1024           MPI          3 58737.140625            0 
            3          1024           MPI          4 57738.162109            0 
            3          1024           MPI          5 58412.656250            0 
            3          1024           MPI          6 57665.459961            0 
            3          1024           MPI          7 57861.894531            0 
            3          1024           MPI          8 58642.803711            0 
            3          1024           MPI          9 58022.961914            0 
# Runtime: 9.704385 s (overhead: 0.000021 %) 10 records
