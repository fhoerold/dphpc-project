# Sysname : Linux
# Nodename: eu-a6-006-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 13:09:22 2021
# Execution time and date (local): Thu Dec 16 14:09:22 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 378055.383789            0 
            3          2048           MPI          1 377520.246094            1 
            3          2048           MPI          2 376188.979492            1 
            3          2048           MPI          3 375148.066406            0 
            3          2048           MPI          4 377338.334961            1 
            3          2048           MPI          5 377455.925781            0 
            3          2048           MPI          6 377986.896484            0 
            3          2048           MPI          7 377605.082031            0 
            3          2048           MPI          8 377551.945312            1 
            3          2048           MPI          9 409164.943359            0 
# Runtime: 7.621694 s (overhead: 0.000052 %) 10 records
