# Sysname : Linux
# Nodename: eu-a6-006-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 13:09:22 2021
# Execution time and date (local): Thu Dec 16 14:09:22 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 378391.783203            0 
            2          2048           MPI          1 377530.774414            2 
            2          2048           MPI          2 376199.485352            1 
            2          2048           MPI          3 375158.403320            0 
            2          2048           MPI          4 377348.686523            1 
            2          2048           MPI          5 377466.827148            0 
            2          2048           MPI          6 377998.305664            0 
            2          2048           MPI          7 377616.085938            0 
            2          2048           MPI          8 377562.757812            1 
            2          2048           MPI          9 409149.869141            0 
# Runtime: 4.622250 s (overhead: 0.000108 %) 10 records
