# Sysname : Linux
# Nodename: eu-a6-006-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 11:09:51 2021
# Execution time and date (local): Thu Dec 16 12:09:51 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 69519.351562            0 
            2          1024           MPI          1 67738.497070            3 
            2          1024           MPI          2 66349.017578            3 
            2          1024           MPI          3 65859.870117            0 
            2          1024           MPI          4 66121.602539            0 
            2          1024           MPI          5 66823.835938            0 
            2          1024           MPI          6 66064.862305            0 
            2          1024           MPI          7 66042.131836            0 
            2          1024           MPI          8 65872.056641            3 
            2          1024           MPI          9 66034.096680            0 
# Runtime: 4.801297 s (overhead: 0.000187 %) 10 records
