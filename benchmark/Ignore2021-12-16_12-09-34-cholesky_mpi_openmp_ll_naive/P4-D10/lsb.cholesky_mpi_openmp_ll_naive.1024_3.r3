# Sysname : Linux
# Nodename: eu-a6-006-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 11:09:51 2021
# Execution time and date (local): Thu Dec 16 12:09:51 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 69496.536133            0 
            3          1024           MPI          1 68107.972656            1 
            3          1024           MPI          2 66368.421875            0 
            3          1024           MPI          3 65871.112305            0 
            3          1024           MPI          4 66132.202148            0 
            3          1024           MPI          5 66836.304688            0 
            3          1024           MPI          6 66075.156250            0 
            3          1024           MPI          7 66052.537109            0 
            3          1024           MPI          8 65882.682617            5 
            3          1024           MPI          9 66044.847656            0 
# Runtime: 0.809041 s (overhead: 0.000742 %) 10 records
