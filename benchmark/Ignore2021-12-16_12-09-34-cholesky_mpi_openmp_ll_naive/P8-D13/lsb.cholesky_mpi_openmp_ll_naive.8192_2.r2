# Sysname : Linux
# Nodename: eu-a6-006-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 12:49:09 2021
# Execution time and date (local): Thu Dec 16 13:49:09 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 17202253.388672            2 
            2          8192           MPI          1 17225150.334961            4 
            2          8192           MPI          2 17210102.651367            5 
            2          8192           MPI          3 17340564.317383            0 
            2          8192           MPI          4 17292225.478516            1 
            2          8192           MPI          5 17283849.998047            0 
            2          8192           MPI          6 17312380.674805            0 
            2          8192           MPI          7 17312039.017578            0 
            2          8192           MPI          8 17261692.394531            5 
            2          8192           MPI          9 17437345.897461            0 
# Runtime: 211.812996 s (overhead: 0.000008 %) 10 records
