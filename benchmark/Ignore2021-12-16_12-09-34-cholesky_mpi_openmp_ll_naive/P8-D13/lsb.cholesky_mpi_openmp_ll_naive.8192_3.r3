# Sysname : Linux
# Nodename: eu-a6-006-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 12:49:09 2021
# Execution time and date (local): Thu Dec 16 13:49:09 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 17201122.437500            0 
            3          8192           MPI          1 17224016.903320            5 
            3          8192           MPI          2 17208976.207031            6 
            3          8192           MPI          3 17339424.271484            0 
            3          8192           MPI          4 17291088.387695            1 
            3          8192           MPI          5 17282717.676758            2 
            3          8192           MPI          6 17311239.472656            0 
            3          8192           MPI          7 17310898.487305            0 
            3          8192           MPI          8 17260559.763672            5 
            3          8192           MPI          9 17436154.047852            2 
# Runtime: 211.798300 s (overhead: 0.000010 %) 10 records
