# Sysname : Linux
# Nodename: eu-a6-006-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 14:38:17 2021
# Execution time and date (local): Thu Dec 16 15:38:17 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 18550493.974609            1 
            3          8192           MPI          1 18188869.534180            3 
            3          8192           MPI          2 18474394.058594            2 
            3          8192           MPI          3 17854755.847656            0 
            3          8192           MPI          4 18153574.345703            1 
            3          8192           MPI          5 17959655.291016            2 
            3          8192           MPI          6 17759111.295898            0 
            3          8192           MPI          7 18015649.859375            0 
            3          8192           MPI          8 18053781.272461            5 
            3          8192           MPI          9 18323983.005859            3 
# Runtime: 218.731937 s (overhead: 0.000008 %) 10 records
