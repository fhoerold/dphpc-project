# Sysname : Linux
# Nodename: eu-a6-006-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 14:38:17 2021
# Execution time and date (local): Thu Dec 16 15:38:17 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 18548905.725586            1 
            2          8192           MPI          1 18187308.800781            4 
            2          8192           MPI          2 18472807.770508            1 
            2          8192           MPI          3 17853223.335938            0 
            2          8192           MPI          4 18152018.376953            4 
            2          8192           MPI          5 17958140.641602            0 
            2          8192           MPI          6 17757587.291016            0 
            2          8192           MPI          7 18014106.657227            0 
            2          8192           MPI          8 18052232.208008            3 
            2          8192           MPI          9 18322406.311523            0 
# Runtime: 218.714024 s (overhead: 0.000006 %) 10 records
