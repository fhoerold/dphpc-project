# Sysname : Linux
# Nodename: eu-a6-006-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 13:07:33 2021
# Execution time and date (local): Thu Dec 16 14:07:33 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 2261076.423828            0 
            2          4096           MPI          1 2278147.646484            1 
            2          4096           MPI          2 2252253.325195            1 
            2          4096           MPI          3 2250302.728516            0 
            2          4096           MPI          4 2249627.709961            2 
            2          4096           MPI          5 2249849.500000            0 
            2          4096           MPI          6 2256524.120117            0 
            2          4096           MPI          7 2278625.577148            0 
            2          4096           MPI          8 2264265.553711            5 
            2          4096           MPI          9 2383524.863281            0 
# Runtime: 28.510220 s (overhead: 0.000032 %) 10 records
