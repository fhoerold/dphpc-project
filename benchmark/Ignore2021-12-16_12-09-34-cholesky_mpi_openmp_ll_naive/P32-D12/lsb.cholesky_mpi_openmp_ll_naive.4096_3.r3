# Sysname : Linux
# Nodename: eu-a6-006-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 13:07:33 2021
# Execution time and date (local): Thu Dec 16 14:07:33 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 2261045.329102            0 
            3          4096           MPI          1 2278175.204102            1 
            3          4096           MPI          2 2252283.290039            1 
            3          4096           MPI          3 2250330.430664            0 
            3          4096           MPI          4 2249658.499023            1 
            3          4096           MPI          5 2249877.180664            0 
            3          4096           MPI          6 2256554.631836            0 
            3          4096           MPI          7 2278658.098633            0 
            3          4096           MPI          8 2264293.527344            5 
            3          4096           MPI          9 2383578.731445            0 
# Runtime: 28.508078 s (overhead: 0.000028 %) 10 records
