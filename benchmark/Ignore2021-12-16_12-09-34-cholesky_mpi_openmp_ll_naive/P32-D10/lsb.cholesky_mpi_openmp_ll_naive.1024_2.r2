# Sysname : Linux
# Nodename: eu-a6-006-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 12:56:25 2021
# Execution time and date (local): Thu Dec 16 13:56:25 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 90856.767578            0 
            2          1024           MPI          1 90991.875977            1 
            2          1024           MPI          2 90438.750977            0 
            2          1024           MPI          3 90567.241211            0 
            2          1024           MPI          4 90514.165039            4 
            2          1024           MPI          5 90683.770508            0 
            2          1024           MPI          6 90629.289062            0 
            2          1024           MPI          7 90514.518555            0 
            2          1024           MPI          8 90308.491211            0 
            2          1024           MPI          9 90171.544922            0 
# Runtime: 1.092426 s (overhead: 0.000458 %) 10 records
