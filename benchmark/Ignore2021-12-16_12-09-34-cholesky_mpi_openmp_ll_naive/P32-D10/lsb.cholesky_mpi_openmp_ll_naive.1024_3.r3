# Sysname : Linux
# Nodename: eu-a6-006-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 12:56:25 2021
# Execution time and date (local): Thu Dec 16 13:56:25 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 90520.594727            0 
            3          1024           MPI          1 90987.990234            3 
            3          1024           MPI          2 90434.814453            0 
            3          1024           MPI          3 90562.947266            0 
            3          1024           MPI          4 90510.611328            0 
            3          1024           MPI          5 90680.644531            0 
            3          1024           MPI          6 90623.782227            0 
            3          1024           MPI          7 90509.783203            0 
            3          1024           MPI          8 90304.188477            0 
            3          1024           MPI          9 90166.963867            0 
# Runtime: 7.091882 s (overhead: 0.000042 %) 10 records
