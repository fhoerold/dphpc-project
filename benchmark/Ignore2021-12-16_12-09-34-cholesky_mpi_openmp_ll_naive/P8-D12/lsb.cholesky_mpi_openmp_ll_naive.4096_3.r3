# Sysname : Linux
# Nodename: eu-a6-003-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 11:18:22 2021
# Execution time and date (local): Thu Dec 16 12:18:22 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 2155637.989258            0 
            3          4096           MPI          1 2162522.050781            1 
            3          4096           MPI          2 2111968.995117            1 
            3          4096           MPI          3 2117430.716797            0 
            3          4096           MPI          4 2152725.167969            1 
            3          4096           MPI          5 2286980.031250            0 
            3          4096           MPI          6 2141981.418945            0 
            3          4096           MPI          7 2109792.526367            0 
            3          4096           MPI          8 2167272.391602            1 
            3          4096           MPI          9 2195099.588867            0 
# Runtime: 29.027166 s (overhead: 0.000014 %) 10 records
