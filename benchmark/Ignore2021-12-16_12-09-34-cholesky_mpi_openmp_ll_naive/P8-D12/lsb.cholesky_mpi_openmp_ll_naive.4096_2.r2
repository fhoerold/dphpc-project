# Sysname : Linux
# Nodename: eu-a6-003-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 11:18:22 2021
# Execution time and date (local): Thu Dec 16 12:18:22 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 2155840.316406            0 
            2          4096           MPI          1 2162702.943359            1 
            2          4096           MPI          2 2112143.544922            1 
            2          4096           MPI          3 2117607.713867            0 
            2          4096           MPI          4 2152903.715820            1 
            2          4096           MPI          5 2287171.741211            0 
            2          4096           MPI          6 2141621.795898            0 
            2          4096           MPI          7 2109969.571289            0 
            2          4096           MPI          8 2167451.058594            1 
            2          4096           MPI          9 2195319.153320            0 
# Runtime: 26.030634 s (overhead: 0.000015 %) 10 records
