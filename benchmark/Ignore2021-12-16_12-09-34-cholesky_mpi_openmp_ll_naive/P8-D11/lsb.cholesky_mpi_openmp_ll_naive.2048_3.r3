# Sysname : Linux
# Nodename: eu-a6-004-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 11:10:33 2021
# Execution time and date (local): Thu Dec 16 12:10:33 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 285534.582031            0 
            3          2048           MPI          1 281820.825195            1 
            3          2048           MPI          2 280061.369141            1 
            3          2048           MPI          3 300472.138672            0 
            3          2048           MPI          4 282480.474609            1 
            3          2048           MPI          5 282501.929688            0 
            3          2048           MPI          6 282708.798828            0 
            3          2048           MPI          7 278392.485352            0 
            3          2048           MPI          8 282588.500000            1 
            3          2048           MPI          9 322610.179688            0 
# Runtime: 3.525077 s (overhead: 0.000113 %) 10 records
