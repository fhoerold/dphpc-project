# Sysname : Linux
# Nodename: eu-a6-004-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 11:10:33 2021
# Execution time and date (local): Thu Dec 16 12:10:33 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 285612.618164            0 
            2          2048           MPI          1 281847.519531            1 
            2          2048           MPI          2 280088.229492            1 
            2          2048           MPI          3 300501.068359            0 
            2          2048           MPI          4 282507.867188            1 
            2          2048           MPI          5 282529.353516            0 
            2          2048           MPI          6 282735.866211            0 
            2          2048           MPI          7 278419.133789            0 
            2          2048           MPI          8 282615.714844            1 
            2          2048           MPI          9 322674.190430            0 
# Runtime: 6.526874 s (overhead: 0.000061 %) 10 records
