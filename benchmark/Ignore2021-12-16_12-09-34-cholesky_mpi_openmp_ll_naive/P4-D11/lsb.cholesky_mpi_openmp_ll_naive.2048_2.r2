# Sysname : Linux
# Nodename: eu-a6-006-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 11:10:33 2021
# Execution time and date (local): Thu Dec 16 12:10:33 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 497397.711914            0 
            2          2048           MPI          1 486537.826172            1 
            2          2048           MPI          2 487948.812500            1 
            2          2048           MPI          3 486188.208008            0 
            2          2048           MPI          4 493941.129883            1 
            2          2048           MPI          5 513978.535156            0 
            2          2048           MPI          6 484487.638672            0 
            2          2048           MPI          7 486970.068359            0 
            2          2048           MPI          8 486956.685547            1 
            2          2048           MPI          9 499926.290039            0 
# Runtime: 5.945702 s (overhead: 0.000067 %) 10 records
