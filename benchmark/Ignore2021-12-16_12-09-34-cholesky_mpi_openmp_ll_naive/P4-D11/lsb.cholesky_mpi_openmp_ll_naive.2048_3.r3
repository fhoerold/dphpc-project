# Sysname : Linux
# Nodename: eu-a6-006-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 11:10:33 2021
# Execution time and date (local): Thu Dec 16 12:10:33 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 497754.768555            0 
            3          2048           MPI          1 486541.576172            1 
            3          2048           MPI          2 487949.192383            0 
            3          2048           MPI          3 486188.998047            0 
            3          2048           MPI          4 493941.880859            1 
            3          2048           MPI          5 513629.478516            0 
            3          2048           MPI          6 484489.057617            0 
            3          2048           MPI          7 486970.431641            0 
            3          2048           MPI          8 486958.327148            0 
            3          2048           MPI          9 499927.808594            0 
# Runtime: 7.945134 s (overhead: 0.000025 %) 10 records
