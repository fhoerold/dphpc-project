# Sysname : Linux
# Nodename: eu-a6-007-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 11:17:09 2021
# Execution time and date (local): Thu Dec 16 12:17:09 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 3979606.293945            0 
            2          4096           MPI          1 3973127.769531            1 
            2          4096           MPI          2 3999658.880859            2 
            2          4096           MPI          3 3988346.204102            0 
            2          4096           MPI          4 3981466.816406            2 
            2          4096           MPI          5 4002243.639648            0 
            2          4096           MPI          6 3985672.041016            0 
            2          4096           MPI          7 3989400.269531            0 
            2          4096           MPI          8 3984914.053711            6 
            2          4096           MPI          9 4099857.446289            0 
# Runtime: 48.160389 s (overhead: 0.000023 %) 10 records
