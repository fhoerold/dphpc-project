# Sysname : Linux
# Nodename: eu-a6-007-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 11:17:09 2021
# Execution time and date (local): Thu Dec 16 12:17:09 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 3979679.668945            0 
            3          4096           MPI          1 3972775.277344            2 
            3          4096           MPI          2 3999740.184570            1 
            3          4096           MPI          3 3988426.251953            0 
            3          4096           MPI          4 3981546.810547            1 
            3          4096           MPI          5 4002325.876953            0 
            3          4096           MPI          6 3985752.551758            0 
            3          4096           MPI          7 3989477.309570            2 
            3          4096           MPI          8 3984995.716797            5 
            3          4096           MPI          9 4099900.627930            0 
# Runtime: 48.161061 s (overhead: 0.000023 %) 10 records
