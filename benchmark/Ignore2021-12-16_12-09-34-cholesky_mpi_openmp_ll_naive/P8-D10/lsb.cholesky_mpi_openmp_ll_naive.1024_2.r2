# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 11:09:50 2021
# Execution time and date (local): Thu Dec 16 12:09:50 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 41638.739258            0 
            2          1024           MPI          1 41003.630859            1 
            2          1024           MPI          2 44649.782227            1 
            2          1024           MPI          3 43057.446289            0 
            2          1024           MPI          4 40686.275391            1 
            2          1024           MPI          5 39926.305664            0 
            2          1024           MPI          6 40922.033203            0 
            2          1024           MPI          7 43831.547852            0 
            2          1024           MPI          8 43297.571289            1 
            2          1024           MPI          9 43382.424805            0 
# Runtime: 0.511483 s (overhead: 0.000782 %) 10 records
