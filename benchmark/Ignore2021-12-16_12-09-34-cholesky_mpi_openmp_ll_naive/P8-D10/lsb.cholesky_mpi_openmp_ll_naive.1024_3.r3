# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 16 11:09:50 2021
# Execution time and date (local): Thu Dec 16 12:09:50 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 41586.598633            0 
            3          1024           MPI          1 40591.751953            3 
            3          1024           MPI          2 44650.304688            1 
            3          1024           MPI          3 43065.911133            0 
            3          1024           MPI          4 40686.359375            0 
            3          1024           MPI          5 39926.208008            0 
            3          1024           MPI          6 40922.959961            0 
            3          1024           MPI          7 43831.192383            0 
            3          1024           MPI          8 43297.290039            1 
            3          1024           MPI          9 43381.839844            0 
# Runtime: 2.513150 s (overhead: 0.000199 %) 10 records
