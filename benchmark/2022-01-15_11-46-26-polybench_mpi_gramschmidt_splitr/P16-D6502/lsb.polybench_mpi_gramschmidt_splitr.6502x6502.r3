# Sysname : Linux
# Nodename: eu-a6-003-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan 15 10:47:01 2022
# Execution time and date (local): Sat Jan 15 11:47:01 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         6502          6502   gramschmidt          0 141687098.755859            0 
         6502          6502   gramschmidt          1 139655858.828125            4 
         6502          6502   gramschmidt          2 142844663.140625            6 
         6502          6502   gramschmidt          3 139536784.181641            0 
         6502          6502   gramschmidt          4 140966199.095703            5 
         6502          6502   gramschmidt          5 143173610.697266            0 
         6502          6502   gramschmidt          6 142114933.783203            0 
         6502          6502   gramschmidt          7 139956223.681641            0 
         6502          6502   gramschmidt          8 143107162.886719            6 
         6502          6502   gramschmidt          9 140656767.603516            0 
# Runtime: 1413.699372 s (overhead: 0.000001 %) 10 records
