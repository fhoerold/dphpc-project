# Sysname : Linux
# Nodename: eu-a6-003-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan 15 10:47:01 2022
# Execution time and date (local): Sat Jan 15 11:47:01 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         6502          6502   gramschmidt          0 144687156.404297            0 
         6502          6502   gramschmidt          1 139656167.349609            7 
         6502          6502   gramschmidt          2 142845956.187500            5 
         6502          6502   gramschmidt          3 139536887.587891            1 
         6502          6502   gramschmidt          4 140967816.033203            6 
         6502          6502   gramschmidt          5 143174115.470703            0 
         6502          6502   gramschmidt          6 142116010.236328            0 
         6502          6502   gramschmidt          7 139957062.386719            1 
         6502          6502   gramschmidt          8 143109151.830078            8 
         6502          6502   gramschmidt          9 140655753.578125            0 
# Runtime: 1416.706146 s (overhead: 0.000002 %) 10 records
