# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 05:05:40 2022
# Execution time and date (local): Sat Jan  8 06:05:40 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096             8           MPI          0 3855263.685547            0 
            3          4096             8           MPI          1 3867679.486328            1 
            3          4096             8           MPI          2 3879026.087891            2 
            3          4096             8           MPI          3 3855570.203125            0 
            3          4096             8           MPI          4 3920230.125000            1 
# Runtime: 36.367000 s (overhead: 0.000011 %) 5 records
