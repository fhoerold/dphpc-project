# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 05:05:40 2022
# Execution time and date (local): Sat Jan  8 06:05:40 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096             8           MPI          0 3855782.681641            0 
            2          4096             8           MPI          1 3868200.203125            1 
            2          4096             8           MPI          2 3879548.603516            2 
            2          4096             8           MPI          3 3856089.056641            1 
            2          4096             8           MPI          4 3920757.023438            1 
# Runtime: 34.373056 s (overhead: 0.000015 %) 5 records
