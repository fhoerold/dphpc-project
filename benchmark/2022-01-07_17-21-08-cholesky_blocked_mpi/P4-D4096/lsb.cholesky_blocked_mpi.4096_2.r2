# Sysname : Linux
# Nodename: eu-a6-011-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:55:27 2022
# Execution time and date (local): Fri Jan  7 17:55:27 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096             8           MPI          0 6879021.490234            0 
            2          4096             8           MPI          1 6879977.501953            2 
            2          4096             8           MPI          2 7196076.085938            5 
            2          4096             8           MPI          3 6944107.031250            0 
            2          4096             8           MPI          4 6846981.615234            2 
# Runtime: 50.607752 s (overhead: 0.000018 %) 5 records
