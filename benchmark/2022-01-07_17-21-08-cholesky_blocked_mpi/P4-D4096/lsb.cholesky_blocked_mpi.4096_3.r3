# Sysname : Linux
# Nodename: eu-a6-011-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:55:27 2022
# Execution time and date (local): Fri Jan  7 17:55:27 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096             8           MPI          0 6879006.421875            0 
            3          4096             8           MPI          1 6879968.152344            1 
            3          4096             8           MPI          2 7196062.265625            8 
            3          4096             8           MPI          3 6944091.923828            0 
            3          4096             8           MPI          4 6846961.808594            7 
# Runtime: 50.607386 s (overhead: 0.000032 %) 5 records
