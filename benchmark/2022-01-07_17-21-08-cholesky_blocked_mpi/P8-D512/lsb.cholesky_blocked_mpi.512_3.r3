# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:32:28 2022
# Execution time and date (local): Fri Jan  7 19:32:28 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512             8           MPI          0 12609.439453            0 
            3           512             8           MPI          1 12299.216797            1 
            3           512             8           MPI          2 12107.632812            1 
            3           512             8           MPI          3 12204.216797            0 
            3           512             8           MPI          4 12311.486328            1 
# Runtime: 6.089208 s (overhead: 0.000049 %) 5 records
