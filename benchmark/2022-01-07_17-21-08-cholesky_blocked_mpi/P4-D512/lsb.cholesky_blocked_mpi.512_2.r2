# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:23:09 2022
# Execution time and date (local): Fri Jan  7 17:23:09 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512             8           MPI          0 14143.482422            0 
            2           512             8           MPI          1 13921.296875            2 
            2           512             8           MPI          2 13993.628906            1 
            2           512             8           MPI          3 13979.455078            0 
            2           512             8           MPI          4 13975.136719            2 
# Runtime: 4.093326 s (overhead: 0.000122 %) 5 records
