# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:23:09 2022
# Execution time and date (local): Fri Jan  7 17:23:09 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512             8           MPI          0 14248.380859            0 
            3           512             8           MPI          1 13959.216797            2 
            3           512             8           MPI          2 14032.267578            0 
            3           512             8           MPI          3 14415.011719            0 
            3           512             8           MPI          4 14012.748047            1 
# Runtime: 4.093095 s (overhead: 0.000073 %) 5 records
