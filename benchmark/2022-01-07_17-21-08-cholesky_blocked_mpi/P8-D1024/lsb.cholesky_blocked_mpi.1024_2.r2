# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:28:34 2022
# Execution time and date (local): Fri Jan  7 17:28:34 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024             8           MPI          0 62939.136719            0 
            2          1024             8           MPI          1 62846.738281            1 
            2          1024             8           MPI          2 62835.054688            1 
            2          1024             8           MPI          3 62064.851562            0 
            2          1024             8           MPI          4 63112.474609            1 
# Runtime: 3.454684 s (overhead: 0.000087 %) 5 records
