# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:28:34 2022
# Execution time and date (local): Fri Jan  7 17:28:34 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024             8           MPI          0 62941.994141            0 
            3          1024             8           MPI          1 62841.603516            1 
            3          1024             8           MPI          2 62822.853516            1 
            3          1024             8           MPI          3 62063.154297            0 
            3          1024             8           MPI          4 63152.974609            1 
# Runtime: 0.451406 s (overhead: 0.000665 %) 5 records
