# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:05:16 2022
# Execution time and date (local): Fri Jan  7 22:05:16 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096             8           MPI          0 3752874.857422            0 
            2          4096             8           MPI          1 3602368.818359            1 
            2          4096             8           MPI          2 3549148.136719            1 
            2          4096             8           MPI          3 3567126.156250            0 
            2          4096             8           MPI          4 3650245.582031            1 
# Runtime: 27.063290 s (overhead: 0.000011 %) 5 records
