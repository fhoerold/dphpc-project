# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 00:22:30 2022
# Execution time and date (local): Sat Jan  8 01:22:30 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192             8           MPI          0 27122691.392578            0 
            2          8192             8           MPI          1 26278013.710938            2 
            2          8192             8           MPI          2 26868121.273438            9 
            2          8192             8           MPI          3 25659829.072266            0 
            2          8192             8           MPI          4 26285747.283203            8 
# Runtime: 197.192358 s (overhead: 0.000010 %) 5 records
