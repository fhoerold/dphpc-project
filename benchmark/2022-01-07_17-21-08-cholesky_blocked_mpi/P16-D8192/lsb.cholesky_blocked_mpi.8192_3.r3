# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 00:22:30 2022
# Execution time and date (local): Sat Jan  8 01:22:30 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192             8           MPI          0 27122486.925781            0 
            3          8192             8           MPI          1 26277815.960938            1 
            3          8192             8           MPI          2 26867921.582031            8 
            3          8192             8           MPI          3 25659636.726562            0 
            3          8192             8           MPI          4 26285551.509766            7 
# Runtime: 197.192262 s (overhead: 0.000008 %) 5 records
