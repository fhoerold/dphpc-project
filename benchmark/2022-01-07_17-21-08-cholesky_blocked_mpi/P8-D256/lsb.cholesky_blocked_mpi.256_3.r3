# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:28:19 2022
# Execution time and date (local): Fri Jan  7 17:28:19 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           256             8           MPI          0 2714.789062            0 
            3           256             8           MPI          1 2911.019531            0 
            3           256             8           MPI          2 2628.376953            0 
            3           256             8           MPI          3 2557.017578            0 
            3           256             8           MPI          4 2585.689453            0 
# Runtime: 4.019744 s (overhead: 0.000000 %) 5 records
