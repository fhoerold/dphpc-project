# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:28:19 2022
# Execution time and date (local): Fri Jan  7 17:28:19 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           256             8           MPI          0 2686.525391            0 
            2           256             8           MPI          1 2563.361328            1 
            2           256             8           MPI          2 2595.775391            0 
            2           256             8           MPI          3 2515.166016            0 
            2           256             8           MPI          4 2545.496094            0 
# Runtime: 0.026590 s (overhead: 0.003761 %) 5 records
