# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 20:11:33 2022
# Execution time and date (local): Fri Jan  7 21:11:33 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192             8           MPI          0 32747483.851562            0 
            2          8192             8           MPI          1 32822544.244141            5 
            2          8192             8           MPI          2 35916849.050781            5 
            2          8192             8           MPI          3 32979276.375000            0 
            2          8192             8           MPI          4 33948620.185547            6 
# Runtime: 257.327775 s (overhead: 0.000006 %) 5 records
