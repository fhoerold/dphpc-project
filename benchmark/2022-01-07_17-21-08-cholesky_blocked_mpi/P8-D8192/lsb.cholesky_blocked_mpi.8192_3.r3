# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 20:11:33 2022
# Execution time and date (local): Fri Jan  7 21:11:33 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192             8           MPI          0 32747520.134766            0 
            3          8192             8           MPI          1 32822574.826172            6 
            3          8192             8           MPI          2 35916867.371094            5 
            3          8192             8           MPI          3 32979399.378906            0 
            3          8192             8           MPI          4 33948651.822266            6 
# Runtime: 258.326709 s (overhead: 0.000007 %) 5 records
