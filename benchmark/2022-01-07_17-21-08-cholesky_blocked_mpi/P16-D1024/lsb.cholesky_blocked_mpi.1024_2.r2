# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 20:55:25 2022
# Execution time and date (local): Fri Jan  7 21:55:25 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024             8           MPI          0 57186.697266            0 
            2          1024             8           MPI          1 56500.335938            1 
            2          1024             8           MPI          2 56571.367188            1 
            2          1024             8           MPI          3 56478.541016            0 
            2          1024             8           MPI          4 56761.992188            1 
# Runtime: 6.401534 s (overhead: 0.000047 %) 5 records
