# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:56:46 2022
# Execution time and date (local): Sat Jan  8 05:56:46 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512             8           MPI          0 15242.076172            0 
            2           512             8           MPI          1 15125.640625            1 
            2           512             8           MPI          2 15158.095703            1 
            2           512             8           MPI          3 14993.431641            0 
            2           512             8           MPI          4 15257.564453            1 
# Runtime: 13.115307 s (overhead: 0.000023 %) 5 records
