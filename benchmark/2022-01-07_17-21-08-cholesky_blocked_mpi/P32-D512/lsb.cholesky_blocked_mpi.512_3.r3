# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:56:46 2022
# Execution time and date (local): Sat Jan  8 05:56:46 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512             8           MPI          0 15240.929688            0 
            3           512             8           MPI          1 15124.419922            1 
            3           512             8           MPI          2 15156.933594            1 
            3           512             8           MPI          3 14992.478516            0 
            3           512             8           MPI          4 15256.238281            1 
# Runtime: 13.115391 s (overhead: 0.000023 %) 5 records
