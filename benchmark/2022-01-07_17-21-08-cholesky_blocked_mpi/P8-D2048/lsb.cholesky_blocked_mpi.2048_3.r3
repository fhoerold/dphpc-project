# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:33:29 2022
# Execution time and date (local): Fri Jan  7 19:33:29 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048             8           MPI          0 631566.728516            0 
            3          2048             8           MPI          1 650538.730469            5 
            3          2048             8           MPI          2 678516.113281            1 
            3          2048             8           MPI          3 620527.978516            0 
            3          2048             8           MPI          4 548270.646484            4 
# Runtime: 12.504976 s (overhead: 0.000080 %) 5 records
