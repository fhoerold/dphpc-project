# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:33:29 2022
# Execution time and date (local): Fri Jan  7 19:33:29 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048             8           MPI          0 631581.324219            0 
            2          2048             8           MPI          1 650556.970703            1 
            2          2048             8           MPI          2 678533.576172            1 
            2          2048             8           MPI          3 620540.302734            0 
            2          2048             8           MPI          4 548286.007812            1 
# Runtime: 4.504614 s (overhead: 0.000067 %) 5 records
