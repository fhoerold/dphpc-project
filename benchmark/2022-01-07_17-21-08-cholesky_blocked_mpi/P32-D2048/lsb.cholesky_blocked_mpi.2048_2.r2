# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:18:55 2022
# Execution time and date (local): Sat Jan  8 05:18:55 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048             8           MPI          0 431627.669922            0 
            2          2048             8           MPI          1 433915.503906            3 
            2          2048             8           MPI          2 432519.068359            1 
            2          2048             8           MPI          3 431630.662109            0 
            2          2048             8           MPI          4 432582.898438            1 
# Runtime: 10.048222 s (overhead: 0.000050 %) 5 records
