# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:18:55 2022
# Execution time and date (local): Sat Jan  8 05:18:55 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048             8           MPI          0 431655.421875            0 
            3          2048             8           MPI          1 433943.800781            4 
            3          2048             8           MPI          2 432547.359375            2 
            3          2048             8           MPI          3 431658.423828            0 
            3          2048             8           MPI          4 432611.074219            1 
# Runtime: 10.054415 s (overhead: 0.000070 %) 5 records
