# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:23:00 2022
# Execution time and date (local): Fri Jan  7 17:23:00 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           256             8           MPI          0 3263.404297            0 
            3           256             8           MPI          1 2916.365234            1 
            3           256             8           MPI          2 3206.328125            0 
            3           256             8           MPI          3 2813.693359            0 
            3           256             8           MPI          4 2800.667969            1 
# Runtime: 0.023341 s (overhead: 0.008568 %) 5 records
