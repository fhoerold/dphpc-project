# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:23:00 2022
# Execution time and date (local): Fri Jan  7 17:23:00 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           256             8           MPI          0 3212.083984            0 
            2           256             8           MPI          1 2877.115234            1 
            2           256             8           MPI          2 2758.662109            0 
            2           256             8           MPI          3 2802.867188            0 
            2           256             8           MPI          4 2776.625000            1 
# Runtime: 1.024140 s (overhead: 0.000195 %) 5 records
