# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:17:45 2022
# Execution time and date (local): Sat Jan  8 05:17:45 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024             8           MPI          0 66971.125000            0 
            2          1024             8           MPI          1 66696.458984            1 
            2          1024             8           MPI          2 67569.509766            1 
            2          1024             8           MPI          3 67469.720703            0 
            2          1024             8           MPI          4 66483.625000            1 
# Runtime: 13.495150 s (overhead: 0.000022 %) 5 records
