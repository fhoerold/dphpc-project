# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:17:45 2022
# Execution time and date (local): Sat Jan  8 05:17:45 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024             8           MPI          0 66969.087891            0 
            3          1024             8           MPI          1 66694.195312            1 
            3          1024             8           MPI          2 67566.865234            1 
            3          1024             8           MPI          3 67467.500000            0 
            3          1024             8           MPI          4 66481.259766            1 
# Runtime: 13.485558 s (overhead: 0.000022 %) 5 records
