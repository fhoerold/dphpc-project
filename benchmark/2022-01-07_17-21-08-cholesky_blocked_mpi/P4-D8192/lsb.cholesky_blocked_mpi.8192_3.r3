# Sysname : Linux
# Nodename: eu-a6-005-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:27:42 2022
# Execution time and date (local): Fri Jan  7 19:27:42 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192             8           MPI          0 45656214.068359            1 
            3          8192             8           MPI          1 45675627.816406            7 
            3          8192             8           MPI          2 45661231.509766            7 
            3          8192             8           MPI          3 45610077.501953            0 
            3          8192             8           MPI          4 45598046.769531            7 
# Runtime: 337.832404 s (overhead: 0.000007 %) 5 records
