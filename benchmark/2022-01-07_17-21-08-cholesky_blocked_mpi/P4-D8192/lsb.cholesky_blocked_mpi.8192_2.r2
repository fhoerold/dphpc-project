# Sysname : Linux
# Nodename: eu-a6-005-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:27:42 2022
# Execution time and date (local): Fri Jan  7 19:27:42 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192             8           MPI          0 45656172.410156            0 
            2          8192             8           MPI          1 45675597.169922            6 
            2          8192             8           MPI          2 45661191.511719            7 
            2          8192             8           MPI          3 45610040.617188            0 
            2          8192             8           MPI          4 45598004.652344            8 
# Runtime: 337.832506 s (overhead: 0.000006 %) 5 records
