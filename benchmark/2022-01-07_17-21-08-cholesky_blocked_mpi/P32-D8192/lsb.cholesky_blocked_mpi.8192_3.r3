# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 11:10:10 2022
# Execution time and date (local): Sat Jan  8 12:10:10 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192             8           MPI          0 28605843.248047            0 
            3          8192             8           MPI          1 28824206.916016            4 
            3          8192             8           MPI          2 28546469.957031            2 
            3          8192             8           MPI          3 28086851.199219            2 
            3          8192             8           MPI          4 28258867.818359            7 
# Runtime: 224.323299 s (overhead: 0.000007 %) 5 records
