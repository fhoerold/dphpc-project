# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 11:10:10 2022
# Execution time and date (local): Sat Jan  8 12:10:10 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192             8           MPI          0 28604260.857422            0 
            2          8192             8           MPI          1 28822611.820312            6 
            2          8192             8           MPI          2 28544889.558594            5 
            2          8192             8           MPI          3 28085297.529297            2 
            2          8192             8           MPI          4 28257304.513672            7 
# Runtime: 224.311668 s (overhead: 0.000009 %) 5 records
