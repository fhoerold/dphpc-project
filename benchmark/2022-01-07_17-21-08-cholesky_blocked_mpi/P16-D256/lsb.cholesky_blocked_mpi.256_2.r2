# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:35:03 2022
# Execution time and date (local): Fri Jan  7 22:35:03 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           256             8           MPI          0 3623.404297            0 
            2           256             8           MPI          1 3685.865234            0 
            2           256             8           MPI          2 3262.425781            0 
            2           256             8           MPI          3 3132.003906            0 
            2           256             8           MPI          4 3127.480469            0 
# Runtime: 12.022072 s (overhead: 0.000000 %) 5 records
