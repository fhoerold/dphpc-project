# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:36:18 2022
# Execution time and date (local): Fri Jan  7 22:36:18 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048             8           MPI          0 441103.609375            0 
            3          2048             8           MPI          1 436395.048828            1 
            3          2048             8           MPI          2 451643.185547            1 
            3          2048             8           MPI          3 452609.406250            0 
            3          2048             8           MPI          4 447532.060547            1 
# Runtime: 9.139052 s (overhead: 0.000033 %) 5 records
