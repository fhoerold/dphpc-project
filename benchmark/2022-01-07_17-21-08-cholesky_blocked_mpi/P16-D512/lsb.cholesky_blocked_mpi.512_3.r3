# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:35:22 2022
# Execution time and date (local): Fri Jan  7 22:35:22 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512             8           MPI          0 11043.992188            0 
            3           512             8           MPI          1 10685.080078            0 
            3           512             8           MPI          2 11310.929688            0 
            3           512             8           MPI          3 10648.400391            0 
            3           512             8           MPI          4 10958.640625            0 
# Runtime: 0.092522 s (overhead: 0.000000 %) 5 records
