# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:40:35 2022
# Execution time and date (local): Fri Jan  7 17:40:35 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024             8           MPI          0 91218.271484            0 
            2          1024             8           MPI          1 94294.714844            1 
            2          1024             8           MPI          2 91579.156250            1 
            2          1024             8           MPI          3 89323.671875            0 
            2          1024             8           MPI          4 94455.736328            2 
# Runtime: 0.657541 s (overhead: 0.000608 %) 5 records
