# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:40:35 2022
# Execution time and date (local): Fri Jan  7 17:40:35 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024             8           MPI          0 91066.568359            0 
            3          1024             8           MPI          1 94300.833984            8 
            3          1024             8           MPI          2 91584.662109            1 
            3          1024             8           MPI          3 89329.214844            0 
            3          1024             8           MPI          4 94460.039062            3 
# Runtime: 0.657014 s (overhead: 0.001826 %) 5 records
