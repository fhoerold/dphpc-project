# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:24:11 2022
# Execution time and date (local): Fri Jan  7 17:24:11 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048             8           MPI          0 769432.363281            0 
            2          2048             8           MPI          1 713772.861328            2 
            2          2048             8           MPI          2 823463.445312            2 
            2          2048             8           MPI          3 762487.587891            0 
            2          2048             8           MPI          4 796237.638672            2 
# Runtime: 11.538307 s (overhead: 0.000052 %) 5 records
