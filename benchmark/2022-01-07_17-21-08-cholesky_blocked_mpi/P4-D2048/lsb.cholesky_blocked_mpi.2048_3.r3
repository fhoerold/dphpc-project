# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:24:11 2022
# Execution time and date (local): Fri Jan  7 17:24:11 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048             8           MPI          0 769433.064453            0 
            3          2048             8           MPI          1 713796.814453            1 
            3          2048             8           MPI          2 823467.789062            2 
            3          2048             8           MPI          3 762491.650391            0 
            3          2048             8           MPI          4 796239.673828            2 
# Runtime: 17.538144 s (overhead: 0.000029 %) 5 records
