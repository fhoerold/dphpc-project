# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:56:31 2022
# Execution time and date (local): Sat Jan  8 05:56:31 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           256             8           MPI          0 4389.552734            0 
            2           256             8           MPI          1 4321.580078            0 
            2           256             8           MPI          2 3988.638672            0 
            2           256             8           MPI          3 4203.177734            0 
            2           256             8           MPI          4 3932.425781            0 
# Runtime: 7.040065 s (overhead: 0.000000 %) 5 records
