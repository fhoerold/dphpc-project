# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:45:28 2022
# Execution time and date (local): Fri Jan  7 19:45:28 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096             8           MPI          0 3743511.806641            0 
            2          4096             8           MPI          1 3580416.279297            8 
            2          4096             8           MPI          2 3943023.455078            1 
            2          4096             8           MPI          3 3981874.273438            0 
            2          4096             8           MPI          4 3669917.626953            1 
# Runtime: 34.566956 s (overhead: 0.000029 %) 5 records
