Invocation: ./benchmark.sh -o ../benchmark/ -d 5 6 7 8 9 10 11 12 13 --openmp -p 4 8 12 16 18 24 30 36 ./build/classical_gram_omp
Benchmark 'classical_gram_omp' for
> Dimensions:		32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 
> Number of processors:	4, 8, 12, 16, 18, 24, 30, 36, 
> CPU Model:		XeonGold_6150
> Use MPI:		false
> Use OpenMP:		true

Timestamp: 2021-12-21_08-32-18
