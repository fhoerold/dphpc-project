# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:38:58 2022
# Execution time and date (local): Wed Jan 12 10:38:58 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 7532447.400391            0 
         1024          1024   gramschmidt          1 443989.898438            4 
         1024          1024   gramschmidt          2 433191.707031            1 
         1024          1024   gramschmidt          3 431057.416016            0 
         1024          1024   gramschmidt          4 438590.384766            1 
         1024          1024   gramschmidt          5 431173.923828            0 
         1024          1024   gramschmidt          6 434577.279297            0 
         1024          1024   gramschmidt          7 432482.943359            0 
         1024          1024   gramschmidt          8 428780.419922            1 
         1024          1024   gramschmidt          9 436196.892578            0 
# Runtime: 11.442527 s (overhead: 0.000061 %) 10 records
