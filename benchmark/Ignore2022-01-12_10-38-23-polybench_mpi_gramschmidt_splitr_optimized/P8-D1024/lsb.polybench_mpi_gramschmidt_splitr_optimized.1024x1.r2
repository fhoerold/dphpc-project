# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:38:58 2022
# Execution time and date (local): Wed Jan 12 10:38:58 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 7524380.810547            0 
         1024          1024   gramschmidt          1 443642.255859            4 
         1024          1024   gramschmidt          2 432771.652344            1 
         1024          1024   gramschmidt          3 430816.503906            0 
         1024          1024   gramschmidt          4 438251.572266            1 
         1024          1024   gramschmidt          5 430836.408203            0 
         1024          1024   gramschmidt          6 434248.375000            0 
         1024          1024   gramschmidt          7 432145.261719            0 
         1024          1024   gramschmidt          8 428452.005859            2 
         1024          1024   gramschmidt          9 435847.978516            0 
# Runtime: 11.431439 s (overhead: 0.000070 %) 10 records
