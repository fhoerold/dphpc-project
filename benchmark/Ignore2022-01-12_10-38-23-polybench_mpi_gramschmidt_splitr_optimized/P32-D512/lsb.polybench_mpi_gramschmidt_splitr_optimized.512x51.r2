# Sysname : Linux
# Nodename: eu-a6-012-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 11:43:51 2022
# Execution time and date (local): Wed Jan 12 12:43:51 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 24422.744141            0 
          512           512   gramschmidt          1 23091.960938            2 
          512           512   gramschmidt          2 21197.439453            0 
          512           512   gramschmidt          3 21264.546875            0 
          512           512   gramschmidt          4 21188.496094            1 
          512           512   gramschmidt          5 23614.267578            0 
          512           512   gramschmidt          6 24831.585938            0 
          512           512   gramschmidt          7 21272.755859            0 
          512           512   gramschmidt          8 21268.968750            1 
          512           512   gramschmidt          9 21204.306641            0 
# Runtime: 0.223396 s (overhead: 0.001791 %) 10 records
