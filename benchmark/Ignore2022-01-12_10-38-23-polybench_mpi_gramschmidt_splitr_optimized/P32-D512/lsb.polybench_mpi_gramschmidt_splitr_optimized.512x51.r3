# Sysname : Linux
# Nodename: eu-a6-012-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 11:43:51 2022
# Execution time and date (local): Wed Jan 12 12:43:51 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 7979897.119141            0 
          512           512   gramschmidt          1 23058.634766            4 
          512           512   gramschmidt          2 21192.744141            0 
          512           512   gramschmidt          3 21263.939453            0 
          512           512   gramschmidt          4 21189.617188            0 
          512           512   gramschmidt          5 23606.826172            0 
          512           512   gramschmidt          6 24820.617188            0 
          512           512   gramschmidt          7 21272.175781            0 
          512           512   gramschmidt          8 21267.527344            0 
          512           512   gramschmidt          9 21199.603516            0 
# Runtime: 8.178805 s (overhead: 0.000049 %) 10 records
