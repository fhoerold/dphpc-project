# Sysname : Linux
# Nodename: eu-a6-008-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:38:58 2022
# Execution time and date (local): Wed Jan 12 10:38:58 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 38565377.097656            0 
         2048          2048   gramschmidt          1 22792950.828125            6 
         2048          2048   gramschmidt          2 23821779.115234            2 
         2048          2048   gramschmidt          3 22807202.966797            0 
         2048          2048   gramschmidt          4 20527148.292969            7 
         2048          2048   gramschmidt          5 23451762.609375            0 
         2048          2048   gramschmidt          6 24928666.894531            1 
         2048          2048   gramschmidt          7 24846407.265625            0 
         2048          2048   gramschmidt          8 23835212.421875            5 
         2048          2048   gramschmidt          9 19373261.681641            0 
# Runtime: 244.949838 s (overhead: 0.000009 %) 10 records
