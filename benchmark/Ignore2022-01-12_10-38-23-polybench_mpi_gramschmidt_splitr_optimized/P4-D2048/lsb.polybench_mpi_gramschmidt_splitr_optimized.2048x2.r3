# Sysname : Linux
# Nodename: eu-a6-008-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:38:58 2022
# Execution time and date (local): Wed Jan 12 10:38:58 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 24586028.703125            0 
         2048          2048   gramschmidt          1 22793819.462891            4 
         2048          2048   gramschmidt          2 23821226.103516            5 
         2048          2048   gramschmidt          3 22787602.791016            0 
         2048          2048   gramschmidt          4 20544182.107422            5 
         2048          2048   gramschmidt          5 23433543.496094            1 
         2048          2048   gramschmidt          6 24951396.859375            0 
         2048          2048   gramschmidt          7 24820640.445312            0 
         2048          2048   gramschmidt          8 23835151.876953            8 
         2048          2048   gramschmidt          9 19390161.031250            0 
# Runtime: 230.963820 s (overhead: 0.000010 %) 10 records
