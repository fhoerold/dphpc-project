# Sysname : Linux
# Nodename: eu-a6-012-05
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 10:49:24 2022
# Execution time and date (local): Wed Jan 12 11:49:24 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 6258683.941406            0 
         1024          1024   gramschmidt          1 282001.244141            4 
         1024          1024   gramschmidt          2 276895.871094            1 
         1024          1024   gramschmidt          3 277845.234375            0 
         1024          1024   gramschmidt          4 276994.744141            1 
         1024          1024   gramschmidt          5 276777.855469            0 
         1024          1024   gramschmidt          6 275132.419922            0 
         1024          1024   gramschmidt          7 285067.371094            0 
         1024          1024   gramschmidt          8 278348.314453            4 
         1024          1024   gramschmidt          9 277633.955078            0 
# Runtime: 8.765439 s (overhead: 0.000114 %) 10 records
