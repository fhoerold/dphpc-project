# Sysname : Linux
# Nodename: eu-a6-012-05
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 10:49:24 2022
# Execution time and date (local): Wed Jan 12 11:49:24 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 2242257.457031            0 
         1024          1024   gramschmidt          1 282075.685547            5 
         1024          1024   gramschmidt          2 276894.015625            2 
         1024          1024   gramschmidt          3 277839.257812            0 
         1024          1024   gramschmidt          4 276995.306641            2 
         1024          1024   gramschmidt          5 276784.351562            0 
         1024          1024   gramschmidt          6 275131.005859            0 
         1024          1024   gramschmidt          7 285005.722656            0 
         1024          1024   gramschmidt          8 278405.128906            2 
         1024          1024   gramschmidt          9 277638.343750            0 
# Runtime: 4.749070 s (overhead: 0.000232 %) 10 records
