# Sysname : Linux
# Nodename: eu-a6-007-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:39:54 2022
# Execution time and date (local): Wed Jan 12 10:39:54 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 1112048907.058594            0 
         8192          8192   gramschmidt          1 1086881975.623047            7 
         8192          8192   gramschmidt          2 1090894332.304688            5 
         8192          8192   gramschmidt          3 1096012982.937500            0 
         8192          8192   gramschmidt          4 1074257173.455078            5 
         8192          8192   gramschmidt          5 1066011375.923828            0 
         8192          8192   gramschmidt          6 1065519196.322266            0 
         8192          8192   gramschmidt          7 1071883488.775391            0 
         8192          8192   gramschmidt          8 966812365.062500            5 
         8192          8192   gramschmidt          9 936549625.542969            0 
# Runtime: 10566.871485 s (overhead: 0.000000 %) 10 records
