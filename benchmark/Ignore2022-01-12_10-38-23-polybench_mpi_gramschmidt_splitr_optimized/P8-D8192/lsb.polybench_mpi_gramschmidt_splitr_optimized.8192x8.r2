# Sysname : Linux
# Nodename: eu-a6-007-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:39:54 2022
# Execution time and date (local): Wed Jan 12 10:39:54 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 1116035148.427734            0 
         8192          8192   gramschmidt          1 1086879577.966797            7 
         8192          8192   gramschmidt          2 1090894946.753906            4 
         8192          8192   gramschmidt          3 1096012187.111328            0 
         8192          8192   gramschmidt          4 1074257312.572266            7 
         8192          8192   gramschmidt          5 1066008444.935547            0 
         8192          8192   gramschmidt          6 1065517941.146484            0 
         8192          8192   gramschmidt          7 1071880897.744141            0 
         8192          8192   gramschmidt          8 966814764.982422            6 
         8192          8192   gramschmidt          9 936548826.964844            0 
# Runtime: 10570.850109 s (overhead: 0.000000 %) 10 records
