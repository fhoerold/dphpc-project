# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:43:24 2022
# Execution time and date (local): Wed Jan 12 10:43:24 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 6028281.935547            0 
          512           512   gramschmidt          1 38221.396484            5 
          512           512   gramschmidt          2 36625.958984            1 
          512           512   gramschmidt          3 36378.894531            0 
          512           512   gramschmidt          4 36293.560547            0 
          512           512   gramschmidt          5 36875.474609            0 
          512           512   gramschmidt          6 36371.285156            0 
          512           512   gramschmidt          7 36394.189453            0 
          512           512   gramschmidt          8 42390.060547            2 
          512           512   gramschmidt          9 35799.330078            0 
# Runtime: 6.363663 s (overhead: 0.000126 %) 10 records
