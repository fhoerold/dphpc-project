# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:43:24 2022
# Execution time and date (local): Wed Jan 12 10:43:24 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 40837.751953            0 
          512           512   gramschmidt          1 38247.775391            2 
          512           512   gramschmidt          2 36679.855469            1 
          512           512   gramschmidt          3 36394.697266            0 
          512           512   gramschmidt          4 36318.658203            1 
          512           512   gramschmidt          5 36897.845703            0 
          512           512   gramschmidt          6 36393.005859            0 
          512           512   gramschmidt          7 36412.800781            0 
          512           512   gramschmidt          8 42419.021484            2 
          512           512   gramschmidt          9 35818.529297            0 
# Runtime: 0.376453 s (overhead: 0.001594 %) 10 records
