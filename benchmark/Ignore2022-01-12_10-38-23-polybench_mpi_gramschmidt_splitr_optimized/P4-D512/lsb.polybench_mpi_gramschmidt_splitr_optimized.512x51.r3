# Sysname : Linux
# Nodename: eu-a6-008-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:38:58 2022
# Execution time and date (local): Wed Jan 12 10:38:58 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 7124688.919922            0 
          512           512   gramschmidt          1 123099.291016            5 
          512           512   gramschmidt          2 119449.150391            1 
          512           512   gramschmidt          3 119463.541016            0 
          512           512   gramschmidt          4 118681.927734            1 
          512           512   gramschmidt          5 121261.205078            0 
          512           512   gramschmidt          6 119891.351562            0 
          512           512   gramschmidt          7 121556.806641            0 
          512           512   gramschmidt          8 118877.792969            1 
          512           512   gramschmidt          9 117625.291016            0 
# Runtime: 8.204627 s (overhead: 0.000098 %) 10 records
