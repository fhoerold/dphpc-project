# Sysname : Linux
# Nodename: eu-a6-008-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:38:58 2022
# Execution time and date (local): Wed Jan 12 10:38:58 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 4112806.457031            0 
          512           512   gramschmidt          1 122934.369141            8 
          512           512   gramschmidt          2 119373.673828            1 
          512           512   gramschmidt          3 119465.458984            0 
          512           512   gramschmidt          4 118535.158203            1 
          512           512   gramschmidt          5 121179.435547            0 
          512           512   gramschmidt          6 119818.671875            0 
          512           512   gramschmidt          7 121492.416016            0 
          512           512   gramschmidt          8 118788.070312            4 
          512           512   gramschmidt          9 117539.958984            0 
# Runtime: 5.191988 s (overhead: 0.000270 %) 10 records
