# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:45:24 2022
# Execution time and date (local): Wed Jan 12 10:45:24 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 58346099.269531            0 
         4096          4096   gramschmidt          1 49617067.699219            4 
         4096          4096   gramschmidt          2 49264593.599609            5 
         4096          4096   gramschmidt          3 49104667.925781            0 
         4096          4096   gramschmidt          4 48727710.701172            7 
         4096          4096   gramschmidt          5 47406299.609375            0 
         4096          4096   gramschmidt          6 46840511.863281            0 
         4096          4096   gramschmidt          7 46908192.158203            0 
         4096          4096   gramschmidt          8 46216025.083984            8 
         4096          4096   gramschmidt          9 44136390.712891            0 
# Runtime: 486.567623 s (overhead: 0.000005 %) 10 records
