# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:45:24 2022
# Execution time and date (local): Wed Jan 12 10:45:24 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 64301049.466797            0 
         4096          4096   gramschmidt          1 49583645.777344            4 
         4096          4096   gramschmidt          2 49231555.929688            4 
         4096          4096   gramschmidt          3 49071755.019531            0 
         4096          4096   gramschmidt          4 48695125.917969            7 
         4096          4096   gramschmidt          5 47374638.039062            0 
         4096          4096   gramschmidt          6 46810122.296875            0 
         4096          4096   gramschmidt          7 46875587.710938            0 
         4096          4096   gramschmidt          8 46185065.589844            5 
         4096          4096   gramschmidt          9 44107263.898438            0 
# Runtime: 492.235864 s (overhead: 0.000004 %) 10 records
