# Sysname : Linux
# Nodename: eu-a6-009-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 10:49:48 2022
# Execution time and date (local): Wed Jan 12 11:49:48 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 13796828.501953            0 
         2048          2048   gramschmidt          1 3624716.818359            4 
         2048          2048   gramschmidt          2 3339148.359375            1 
         2048          2048   gramschmidt          3 3156642.365234            0 
         2048          2048   gramschmidt          4 3164293.853516            4 
         2048          2048   gramschmidt          5 3623864.695312            0 
         2048          2048   gramschmidt          6 3257275.841797            0 
         2048          2048   gramschmidt          7 3165186.431641            0 
         2048          2048   gramschmidt          8 3209773.630859            4 
         2048          2048   gramschmidt          9 2971502.300781            0 
# Runtime: 43.309285 s (overhead: 0.000030 %) 10 records
