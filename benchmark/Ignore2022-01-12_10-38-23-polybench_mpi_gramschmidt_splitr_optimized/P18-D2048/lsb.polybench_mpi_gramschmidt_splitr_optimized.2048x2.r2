# Sysname : Linux
# Nodename: eu-a6-009-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 10:49:48 2022
# Execution time and date (local): Wed Jan 12 11:49:48 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 13821336.734375            0 
         2048          2048   gramschmidt          1 3627780.373047            5 
         2048          2048   gramschmidt          2 3341678.490234            1 
         2048          2048   gramschmidt          3 3159399.437500            0 
         2048          2048   gramschmidt          4 3166834.509766            6 
         2048          2048   gramschmidt          5 3626819.466797            0 
         2048          2048   gramschmidt          6 3259719.277344            0 
         2048          2048   gramschmidt          7 3167898.923828            0 
         2048          2048   gramschmidt          8 3212324.345703            5 
         2048          2048   gramschmidt          9 2973957.281250            0 
# Runtime: 43.357805 s (overhead: 0.000039 %) 10 records
