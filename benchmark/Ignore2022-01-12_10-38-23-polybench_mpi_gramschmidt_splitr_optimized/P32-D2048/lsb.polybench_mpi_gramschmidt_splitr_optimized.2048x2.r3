# Sysname : Linux
# Nodename: eu-a6-012-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 11:44:09 2022
# Execution time and date (local): Wed Jan 12 12:44:09 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 8753935.666016            0 
         2048          2048   gramschmidt          1 1475983.658203            5 
         2048          2048   gramschmidt          2 1522854.386719            1 
         2048          2048   gramschmidt          3 1246867.173828            0 
         2048          2048   gramschmidt          4 1487680.414062            3 
         2048          2048   gramschmidt          5 1374291.783203            0 
         2048          2048   gramschmidt          6 1387881.423828            0 
         2048          2048   gramschmidt          7 1291248.050781            0 
         2048          2048   gramschmidt          8 1495672.544922            1 
         2048          2048   gramschmidt          9 1415442.404297            0 
# Runtime: 21.451913 s (overhead: 0.000047 %) 10 records
