# Sysname : Linux
# Nodename: eu-a6-012-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 11:44:09 2022
# Execution time and date (local): Wed Jan 12 12:44:09 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 10733694.582031            0 
         2048          2048   gramschmidt          1 1475915.761719            4 
         2048          2048   gramschmidt          2 1522668.451172            2 
         2048          2048   gramschmidt          3 1246879.388672            0 
         2048          2048   gramschmidt          4 1487564.142578            2 
         2048          2048   gramschmidt          5 1374210.199219            0 
         2048          2048   gramschmidt          6 1387808.798828            0 
         2048          2048   gramschmidt          7 1291117.287109            0 
         2048          2048   gramschmidt          8 1495545.687500            5 
         2048          2048   gramschmidt          9 1415437.273438            0 
# Runtime: 23.430893 s (overhead: 0.000055 %) 10 records
