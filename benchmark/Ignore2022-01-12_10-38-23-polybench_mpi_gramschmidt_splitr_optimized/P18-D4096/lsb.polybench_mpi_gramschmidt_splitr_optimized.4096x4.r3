# Sysname : Linux
# Nodename: eu-a6-012-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 10:51:16 2022
# Execution time and date (local): Wed Jan 12 11:51:16 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 19274843.630859            0 
         4096          4096   gramschmidt          1 19373669.392578            3 
         4096          4096   gramschmidt          2 19528950.958984            5 
         4096          4096   gramschmidt          3 19539848.468750            0 
         4096          4096   gramschmidt          4 19547845.689453            5 
         4096          4096   gramschmidt          5 19325055.222656            0 
         4096          4096   gramschmidt          6 19387415.880859            0 
         4096          4096   gramschmidt          7 19766908.308594            0 
         4096          4096   gramschmidt          8 19510073.349609            7 
         4096          4096   gramschmidt          9 19773896.876953            0 
# Runtime: 195.028566 s (overhead: 0.000010 %) 10 records
