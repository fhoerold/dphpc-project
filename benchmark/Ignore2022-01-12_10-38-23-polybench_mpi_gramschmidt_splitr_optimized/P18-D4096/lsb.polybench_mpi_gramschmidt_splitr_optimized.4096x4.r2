# Sysname : Linux
# Nodename: eu-a6-012-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 10:51:16 2022
# Execution time and date (local): Wed Jan 12 11:51:16 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 19270271.453125            0 
         4096          4096   gramschmidt          1 19372038.199219            4 
         4096          4096   gramschmidt          2 19527470.990234            5 
         4096          4096   gramschmidt          3 19539434.906250            0 
         4096          4096   gramschmidt          4 19545283.240234            6 
         4096          4096   gramschmidt          5 19324703.785156            0 
         4096          4096   gramschmidt          6 19385965.681641            0 
         4096          4096   gramschmidt          7 19764497.064453            0 
         4096          4096   gramschmidt          8 19509484.423828            7 
         4096          4096   gramschmidt          9 19772436.554688            0 
# Runtime: 195.011639 s (overhead: 0.000011 %) 10 records
