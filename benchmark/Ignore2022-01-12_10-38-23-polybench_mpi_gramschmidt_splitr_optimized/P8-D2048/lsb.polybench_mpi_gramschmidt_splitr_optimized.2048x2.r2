# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:39:23 2022
# Execution time and date (local): Wed Jan 12 10:39:23 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 9161672.246094            0 
         2048          2048   gramschmidt          1 10399528.691406            4 
         2048          2048   gramschmidt          2 10071137.558594            4 
         2048          2048   gramschmidt          3 10100691.611328            0 
         2048          2048   gramschmidt          4 10063581.509766            7 
         2048          2048   gramschmidt          5 9706997.562500            0 
         2048          2048   gramschmidt          6 9882580.582031            0 
         2048          2048   gramschmidt          7 9834812.029297            0 
         2048          2048   gramschmidt          8 9580356.322266            5 
         2048          2048   gramschmidt          9 8020876.103516            0 
# Runtime: 96.822292 s (overhead: 0.000021 %) 10 records
