# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:39:23 2022
# Execution time and date (local): Wed Jan 12 10:39:23 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 9167476.587891            0 
         2048          2048   gramschmidt          1 10399435.722656            4 
         2048          2048   gramschmidt          2 10070516.148438            2 
         2048          2048   gramschmidt          3 10101062.316406            0 
         2048          2048   gramschmidt          4 10062961.818359            6 
         2048          2048   gramschmidt          5 9707308.173828            0 
         2048          2048   gramschmidt          6 9882015.896484            0 
         2048          2048   gramschmidt          7 9834588.380859            0 
         2048          2048   gramschmidt          8 9580789.740234            3 
         2048          2048   gramschmidt          9 8020283.187500            0 
# Runtime: 96.826496 s (overhead: 0.000015 %) 10 records
