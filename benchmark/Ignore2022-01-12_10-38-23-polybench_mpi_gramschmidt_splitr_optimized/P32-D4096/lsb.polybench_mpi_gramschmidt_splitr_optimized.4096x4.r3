# Sysname : Linux
# Nodename: eu-a6-012-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 11:45:19 2022
# Execution time and date (local): Wed Jan 12 12:45:19 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 34608066.556641            0 
         4096          4096   gramschmidt          1 23270226.808594            5 
         4096          4096   gramschmidt          2 22277743.636719            2 
         4096          4096   gramschmidt          3 21835575.482422            0 
         4096          4096   gramschmidt          4 20947214.666016            6 
         4096          4096   gramschmidt          5 21136632.171875            0 
         4096          4096   gramschmidt          6 19994102.746094            0 
         4096          4096   gramschmidt          7 19931182.646484            0 
         4096          4096   gramschmidt          8 19433269.906250            6 
         4096          4096   gramschmidt          9 18981677.910156            0 
# Runtime: 222.415754 s (overhead: 0.000009 %) 10 records
