# Sysname : Linux
# Nodename: eu-a6-012-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 11:45:19 2022
# Execution time and date (local): Wed Jan 12 12:45:19 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 34605859.777344            0 
         4096          4096   gramschmidt          1 23269763.716797            7 
         4096          4096   gramschmidt          2 22277490.865234            3 
         4096          4096   gramschmidt          3 21835482.662109            0 
         4096          4096   gramschmidt          4 20946583.224609            5 
         4096          4096   gramschmidt          5 21136592.429688            0 
         4096          4096   gramschmidt          6 19993902.968750            0 
         4096          4096   gramschmidt          7 19930474.541016            0 
         4096          4096   gramschmidt          8 19433403.861328            6 
         4096          4096   gramschmidt          9 18981251.990234            0 
# Runtime: 222.410869 s (overhead: 0.000009 %) 10 records
