# Sysname : Linux
# Nodename: eu-a6-012-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 11:49:21 2022
# Execution time and date (local): Wed Jan 12 12:49:21 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 227936371.148438            0 
         8192          8192   gramschmidt          1 224266130.693359            7 
         8192          8192   gramschmidt          2 219667976.978516            6 
         8192          8192   gramschmidt          3 215682800.798828            0 
         8192          8192   gramschmidt          4 230988049.148438            6 
         8192          8192   gramschmidt          5 248863269.943359            0 
         8192          8192   gramschmidt          6 245787751.152344            0 
         8192          8192   gramschmidt          7 214412742.464844            0 
         8192          8192   gramschmidt          8 213552957.023438           23 
         8192          8192   gramschmidt          9 210128394.125000            0 
# Runtime: 2251.286528 s (overhead: 0.000002 %) 10 records
