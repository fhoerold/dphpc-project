# Sysname : Linux
# Nodename: eu-a6-008-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:38:58 2022
# Execution time and date (local): Wed Jan 12 10:38:58 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 2157920.820312            0 
         1024          1024   gramschmidt          1 2101404.025391            6 
         1024          1024   gramschmidt          2 2129968.994141            4 
         1024          1024   gramschmidt          3 2114412.007812            0 
         1024          1024   gramschmidt          4 1969563.937500            6 
         1024          1024   gramschmidt          5 1964822.861328            0 
         1024          1024   gramschmidt          6 1973165.822266            0 
         1024          1024   gramschmidt          7 2041664.148438            0 
         1024          1024   gramschmidt          8 2034637.103516            6 
         1024          1024   gramschmidt          9 2049198.232422            0 
# Runtime: 20.536814 s (overhead: 0.000107 %) 10 records
