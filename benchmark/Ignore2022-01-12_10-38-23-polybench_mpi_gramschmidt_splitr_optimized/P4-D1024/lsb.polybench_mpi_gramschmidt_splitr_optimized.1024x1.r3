# Sysname : Linux
# Nodename: eu-a6-008-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:38:58 2022
# Execution time and date (local): Wed Jan 12 10:38:58 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 2159024.382812            0 
         1024          1024   gramschmidt          1 2101333.441406            6 
         1024          1024   gramschmidt          2 2129855.583984            3 
         1024          1024   gramschmidt          3 2114154.605469            0 
         1024          1024   gramschmidt          4 1969399.714844            4 
         1024          1024   gramschmidt          5 1964875.484375            0 
         1024          1024   gramschmidt          6 1972914.152344            0 
         1024          1024   gramschmidt          7 2041561.429688            0 
         1024          1024   gramschmidt          8 2034494.363281            4 
         1024          1024   gramschmidt          9 2049111.152344            0 
# Runtime: 20.536781 s (overhead: 0.000083 %) 10 records
