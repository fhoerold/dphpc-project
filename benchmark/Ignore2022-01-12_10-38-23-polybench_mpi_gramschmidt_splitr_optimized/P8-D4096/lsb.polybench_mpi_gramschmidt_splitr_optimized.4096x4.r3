# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:39:23 2022
# Execution time and date (local): Wed Jan 12 10:39:23 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 88704632.787109            0 
         4096          4096   gramschmidt          1 66438756.128906            4 
         4096          4096   gramschmidt          2 64514213.388672            5 
         4096          4096   gramschmidt          3 66948499.074219            0 
         4096          4096   gramschmidt          4 71755267.951172            4 
         4096          4096   gramschmidt          5 93155499.666016            0 
         4096          4096   gramschmidt          6 97624131.947266            0 
         4096          4096   gramschmidt          7 96968937.791016            0 
         4096          4096   gramschmidt          8 96134451.845703            5 
         4096          4096   gramschmidt          9 95567122.355469            0 
# Runtime: 837.811579 s (overhead: 0.000002 %) 10 records
