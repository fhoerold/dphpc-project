# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:39:23 2022
# Execution time and date (local): Wed Jan 12 10:39:23 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 88700518.013672            0 
         4096          4096   gramschmidt          1 66438738.621094            4 
         4096          4096   gramschmidt          2 64513798.449219            4 
         4096          4096   gramschmidt          3 66948132.169922            0 
         4096          4096   gramschmidt          4 71755096.775391            4 
         4096          4096   gramschmidt          5 93154062.062500            1 
         4096          4096   gramschmidt          6 97623898.957031            0 
         4096          4096   gramschmidt          7 96968358.314453            0 
         4096          4096   gramschmidt          8 96134219.935547            5 
         4096          4096   gramschmidt          9 95566915.937500            0 
# Runtime: 837.803803 s (overhead: 0.000002 %) 10 records
