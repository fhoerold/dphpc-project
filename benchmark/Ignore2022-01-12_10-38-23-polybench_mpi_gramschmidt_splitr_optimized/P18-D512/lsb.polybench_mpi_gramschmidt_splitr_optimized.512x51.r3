# Sysname : Linux
# Nodename: eu-a6-012-05
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 10:48:47 2022
# Execution time and date (local): Wed Jan 12 11:48:47 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 38958.783203            0 
          512           512   gramschmidt          1 37161.582031            1 
          512           512   gramschmidt          2 36087.621094            1 
          512           512   gramschmidt          3 35541.615234            0 
          512           512   gramschmidt          4 35555.560547            0 
          512           512   gramschmidt          5 35497.371094            0 
          512           512   gramschmidt          6 35775.728516            0 
          512           512   gramschmidt          7 35384.236328            0 
          512           512   gramschmidt          8 35608.771484            1 
          512           512   gramschmidt          9 35547.880859            0 
# Runtime: 0.361156 s (overhead: 0.000831 %) 10 records
