# Sysname : Linux
# Nodename: eu-a6-012-05
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 10:48:47 2022
# Execution time and date (local): Wed Jan 12 11:48:47 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 11048575.714844            0 
          512           512   gramschmidt          1 37193.529297            4 
          512           512   gramschmidt          2 36082.888672            0 
          512           512   gramschmidt          3 35541.433594            0 
          512           512   gramschmidt          4 35555.710938            0 
          512           512   gramschmidt          5 35491.787109            0 
          512           512   gramschmidt          6 35780.640625            0 
          512           512   gramschmidt          7 35385.382812            0 
          512           512   gramschmidt          8 35606.759766            0 
          512           512   gramschmidt          9 35548.398438            0 
# Runtime: 11.370795 s (overhead: 0.000035 %) 10 records
