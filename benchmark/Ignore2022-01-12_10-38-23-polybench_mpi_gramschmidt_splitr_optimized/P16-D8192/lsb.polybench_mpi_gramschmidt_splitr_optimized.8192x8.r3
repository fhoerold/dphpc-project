# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:53:54 2022
# Execution time and date (local): Wed Jan 12 10:53:54 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 524843661.236328            0 
         8192          8192   gramschmidt          1 507741448.285156            6 
         8192          8192   gramschmidt          2 513321865.384766            6 
         8192          8192   gramschmidt          3 518348680.177734            0 
         8192          8192   gramschmidt          4 488958382.755859           12 
         8192          8192   gramschmidt          5 493229557.103516            0 
         8192          8192   gramschmidt          6 506779318.494141            0 
         8192          8192   gramschmidt          7 524513501.539062            0 
         8192          8192   gramschmidt          8 492960205.976562            6 
         8192          8192   gramschmidt          9 494237783.398438            0 
# Runtime: 5064.934477 s (overhead: 0.000001 %) 10 records
