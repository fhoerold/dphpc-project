# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:53:54 2022
# Execution time and date (local): Wed Jan 12 10:53:54 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 524849440.966797            0 
         8192          8192   gramschmidt          1 507757684.699219            4 
         8192          8192   gramschmidt          2 513338124.214844            5 
         8192          8192   gramschmidt          3 518364878.896484            0 
         8192          8192   gramschmidt          4 488973923.257812            6 
         8192          8192   gramschmidt          5 493244780.908203            0 
         8192          8192   gramschmidt          6 506794917.953125            0 
         8192          8192   gramschmidt          7 524530722.421875            0 
         8192          8192   gramschmidt          8 492975617.517578            5 
         8192          8192   gramschmidt          9 494253558.017578            0 
# Runtime: 5065.083709 s (overhead: 0.000000 %) 10 records
