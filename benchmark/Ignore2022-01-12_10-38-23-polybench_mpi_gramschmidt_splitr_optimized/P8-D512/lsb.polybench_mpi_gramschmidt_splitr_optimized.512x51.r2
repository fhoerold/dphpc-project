# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:38:58 2022
# Execution time and date (local): Wed Jan 12 10:38:58 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 4051051.250000            0 
          512           512   gramschmidt          1 53043.974609            3 
          512           512   gramschmidt          2 52351.195312            0 
          512           512   gramschmidt          3 52260.398438            0 
          512           512   gramschmidt          4 52256.515625            0 
          512           512   gramschmidt          5 52203.056641            0 
          512           512   gramschmidt          6 52114.484375            0 
          512           512   gramschmidt          7 52434.230469            0 
          512           512   gramschmidt          8 52085.837891            0 
          512           512   gramschmidt          9 52330.113281            0 
# Runtime: 4.522159 s (overhead: 0.000066 %) 10 records
