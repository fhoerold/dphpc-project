# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:38:58 2022
# Execution time and date (local): Wed Jan 12 10:38:58 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 4052264.105469            0 
          512           512   gramschmidt          1 53035.042969            3 
          512           512   gramschmidt          2 52354.820312            0 
          512           512   gramschmidt          3 52261.093750            0 
          512           512   gramschmidt          4 52259.447266            0 
          512           512   gramschmidt          5 52206.330078            0 
          512           512   gramschmidt          6 52115.437500            0 
          512           512   gramschmidt          7 52437.585938            0 
          512           512   gramschmidt          8 52090.511719            0 
          512           512   gramschmidt          9 52338.984375            0 
# Runtime: 4.523392 s (overhead: 0.000066 %) 10 records
