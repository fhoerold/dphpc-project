# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:43:55 2022
# Execution time and date (local): Wed Jan 12 10:43:55 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 5298336.183594            0 
         1024          1024   gramschmidt          1 274458.251953            5 
         1024          1024   gramschmidt          2 269712.515625            2 
         1024          1024   gramschmidt          3 273036.857422            0 
         1024          1024   gramschmidt          4 278574.896484            4 
         1024          1024   gramschmidt          5 270771.703125            0 
         1024          1024   gramschmidt          6 269873.972656            0 
         1024          1024   gramschmidt          7 270630.445312            0 
         1024          1024   gramschmidt          8 272183.173828            2 
         1024          1024   gramschmidt          9 269227.615234            0 
# Runtime: 7.746859 s (overhead: 0.000168 %) 10 records
