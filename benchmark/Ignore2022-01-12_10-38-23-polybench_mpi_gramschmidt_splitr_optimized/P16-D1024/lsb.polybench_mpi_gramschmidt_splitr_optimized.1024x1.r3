# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:43:55 2022
# Execution time and date (local): Wed Jan 12 10:43:55 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 5296510.908203            0 
         1024          1024   gramschmidt          1 274449.427734            5 
         1024          1024   gramschmidt          2 269687.138672            1 
         1024          1024   gramschmidt          3 273091.255859            0 
         1024          1024   gramschmidt          4 278519.835938            4 
         1024          1024   gramschmidt          5 270747.060547            0 
         1024          1024   gramschmidt          6 269862.644531            0 
         1024          1024   gramschmidt          7 270620.894531            0 
         1024          1024   gramschmidt          8 272185.599609            1 
         1024          1024   gramschmidt          9 269197.603516            0 
# Runtime: 7.744926 s (overhead: 0.000142 %) 10 records
