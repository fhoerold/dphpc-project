# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:44:15 2022
# Execution time and date (local): Wed Jan 12 10:44:15 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 14741865.750000            0 
         2048          2048   gramschmidt          1 3343090.818359            5 
         2048          2048   gramschmidt          2 3242281.990234            2 
         2048          2048   gramschmidt          3 3152825.785156            0 
         2048          2048   gramschmidt          4 3026151.945312            5 
         2048          2048   gramschmidt          5 2865376.074219            0 
         2048          2048   gramschmidt          6 2776309.431641            0 
         2048          2048   gramschmidt          7 2900824.039062            0 
         2048          2048   gramschmidt          8 2862183.537109            6 
         2048          2048   gramschmidt          9 2577770.490234            0 
# Runtime: 41.488736 s (overhead: 0.000043 %) 10 records
