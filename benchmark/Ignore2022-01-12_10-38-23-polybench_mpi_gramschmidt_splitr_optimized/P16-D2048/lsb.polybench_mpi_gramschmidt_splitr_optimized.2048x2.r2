# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:44:15 2022
# Execution time and date (local): Wed Jan 12 10:44:15 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 11740195.111328            0 
         2048          2048   gramschmidt          1 3343399.873047            7 
         2048          2048   gramschmidt          2 3242355.746094            3 
         2048          2048   gramschmidt          3 3152677.525391            0 
         2048          2048   gramschmidt          4 3026432.955078            6 
         2048          2048   gramschmidt          5 2865418.597656            0 
         2048          2048   gramschmidt          6 2776208.312500            0 
         2048          2048   gramschmidt          7 2900774.316406            0 
         2048          2048   gramschmidt          8 2862490.320312            7 
         2048          2048   gramschmidt          9 2577826.691406            0 
# Runtime: 38.487842 s (overhead: 0.000060 %) 10 records
