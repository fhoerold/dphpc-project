# Sysname : Linux
# Nodename: eu-a6-012-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 10:54:48 2022
# Execution time and date (local): Wed Jan 12 11:54:48 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 328248813.160156            0 
         8192          8192   gramschmidt          1 290688120.597656            5 
         8192          8192   gramschmidt          2 287334688.146484            4 
         8192          8192   gramschmidt          3 275462584.474609            0 
         8192          8192   gramschmidt          4 275219654.128906            4 
         8192          8192   gramschmidt          5 286003124.113281            0 
         8192          8192   gramschmidt          6 291189951.089844            0 
         8192          8192   gramschmidt          7 288191909.828125            0 
         8192          8192   gramschmidt          8 291388960.091797            4 
         8192          8192   gramschmidt          9 292151856.529297            0 
# Runtime: 2905.879725 s (overhead: 0.000001 %) 10 records
