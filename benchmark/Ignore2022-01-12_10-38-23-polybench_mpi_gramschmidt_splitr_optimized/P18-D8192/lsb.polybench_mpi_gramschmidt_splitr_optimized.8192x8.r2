# Sysname : Linux
# Nodename: eu-a6-012-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 10:54:48 2022
# Execution time and date (local): Wed Jan 12 11:54:48 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 329205723.169922            0 
         8192          8192   gramschmidt          1 290650389.992188            4 
         8192          8192   gramschmidt          2 287297758.052734            5 
         8192          8192   gramschmidt          3 275427178.677734            0 
         8192          8192   gramschmidt          4 275184497.525391            5 
         8192          8192   gramschmidt          5 285966395.080078            0 
         8192          8192   gramschmidt          6 291152635.498047            0 
         8192          8192   gramschmidt          7 288154933.705078            0 
         8192          8192   gramschmidt          8 291351551.542969           10 
         8192          8192   gramschmidt          9 292114644.429688            3 
# Runtime: 2906.505779 s (overhead: 0.000001 %) 10 records
