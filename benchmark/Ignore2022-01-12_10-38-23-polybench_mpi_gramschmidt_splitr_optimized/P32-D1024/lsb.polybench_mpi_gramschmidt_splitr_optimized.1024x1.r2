# Sysname : Linux
# Nodename: eu-a6-012-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 11:44:49 2022
# Execution time and date (local): Wed Jan 12 12:44:49 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 5160239.900391            0 
         1024          1024   gramschmidt          1 133983.888672            6 
         1024          1024   gramschmidt          2 133560.482422            1 
         1024          1024   gramschmidt          3 133208.380859            0 
         1024          1024   gramschmidt          4 133645.251953            1 
         1024          1024   gramschmidt          5 133682.638672            0 
         1024          1024   gramschmidt          6 151364.886719            0 
         1024          1024   gramschmidt          7 136034.990234            0 
         1024          1024   gramschmidt          8 134479.458984            2 
         1024          1024   gramschmidt          9 134222.773438            0 
# Runtime: 6.384470 s (overhead: 0.000157 %) 10 records
