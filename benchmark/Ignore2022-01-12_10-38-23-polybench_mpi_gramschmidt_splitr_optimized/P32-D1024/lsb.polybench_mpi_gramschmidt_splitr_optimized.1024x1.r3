# Sysname : Linux
# Nodename: eu-a6-012-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 11:44:49 2022
# Execution time and date (local): Wed Jan 12 12:44:49 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 5133892.640625            0 
         1024          1024   gramschmidt          1 133975.994141            5 
         1024          1024   gramschmidt          2 133547.343750            1 
         1024          1024   gramschmidt          3 133208.015625            0 
         1024          1024   gramschmidt          4 133620.720703            1 
         1024          1024   gramschmidt          5 133672.732422            0 
         1024          1024   gramschmidt          6 151423.523438            0 
         1024          1024   gramschmidt          7 135953.830078            0 
         1024          1024   gramschmidt          8 134477.533203            1 
         1024          1024   gramschmidt          9 134204.625000            0 
# Runtime: 6.358019 s (overhead: 0.000126 %) 10 records
