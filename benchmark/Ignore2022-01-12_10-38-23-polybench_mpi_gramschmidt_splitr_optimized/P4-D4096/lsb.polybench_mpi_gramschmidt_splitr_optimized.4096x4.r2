# Sysname : Linux
# Nodename: eu-a6-003-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:38:58 2022
# Execution time and date (local): Wed Jan 12 10:38:58 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 439425264.212891            0 
         4096          4096   gramschmidt          1 340974551.646484            5 
         4096          4096   gramschmidt          2 283333445.957031            5 
         4096          4096   gramschmidt          3 267128004.517578            0 
         4096          4096   gramschmidt          4 282651719.304688            6 
         4096          4096   gramschmidt          5 249046258.353516            0 
         4096          4096   gramschmidt          6 250171115.302734            0 
         4096          4096   gramschmidt          7 247895557.623047            0 
         4096          4096   gramschmidt          8 250826725.339844           12 
         4096          4096   gramschmidt          9 253527354.822266            0 
# Runtime: 2864.980069 s (overhead: 0.000001 %) 10 records
