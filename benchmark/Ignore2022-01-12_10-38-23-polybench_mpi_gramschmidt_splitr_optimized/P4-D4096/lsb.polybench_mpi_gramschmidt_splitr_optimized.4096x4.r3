# Sysname : Linux
# Nodename: eu-a6-003-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:38:58 2022
# Execution time and date (local): Wed Jan 12 10:38:58 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 432441110.117188            0 
         4096          4096   gramschmidt          1 340974250.765625            8 
         4096          4096   gramschmidt          2 283333753.480469            4 
         4096          4096   gramschmidt          3 267127207.582031            0 
         4096          4096   gramschmidt          4 282652846.853516            6 
         4096          4096   gramschmidt          5 249045138.185547            0 
         4096          4096   gramschmidt          6 250170733.587891            0 
         4096          4096   gramschmidt          7 247895745.835938            0 
         4096          4096   gramschmidt          8 250827674.882812            5 
         4096          4096   gramschmidt          9 253525991.738281            0 
# Runtime: 2857.994515 s (overhead: 0.000001 %) 10 records
