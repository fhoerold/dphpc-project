Invocation: ./gramschmidt/benchmark.sh -o ./benchmark --mpi -d 9 10 11 12 13 -p 2 4 8 16 18 32 ./build/polybench_mpi_gramschmidt_splitr
Benchmark 'polybench_mpi_gramschmidt_splitr' for
> Dimensions:		512, 1024, 2048, 4096, 8192, 
> Number of processors:	2, 4, 8, 16, 18, 32, 
> CPU Model:		XeonGold_6150
> Use MPI:		true
> Use OpenMP:		false

Timestamp: 2022-01-11_02-29-18
