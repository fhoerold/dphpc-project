# Sysname : Linux
# Nodename: eu-a6-012-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 07:02:40 2022
# Execution time and date (local): Tue Jan 11 08:02:40 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          8192          8192   gramschmidt           MPI          0 619128437.197266            0 
            2          8192          8192   gramschmidt           MPI          1 617059799.537109            5 
            2          8192          8192   gramschmidt           MPI          2 613627577.441406            8 
            2          8192          8192   gramschmidt           MPI          3 614041905.050781            3 
            2          8192          8192   gramschmidt           MPI          4 603431173.347656           10 
            2          8192          8192   gramschmidt           MPI          5 615480975.201172            3 
            2          8192          8192   gramschmidt           MPI          6 604199148.966797           24 
            2          8192          8192   gramschmidt           MPI          7 620616609.039062            4 
            2          8192          8192   gramschmidt           MPI          8 606043391.970703            8 
            2          8192          8192   gramschmidt           MPI          9 612809843.984375            3 
# Runtime: 8005.492844 s (overhead: 0.000001 %) 10 records
