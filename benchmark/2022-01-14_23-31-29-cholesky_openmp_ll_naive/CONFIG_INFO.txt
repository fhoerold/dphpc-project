Invocation: ./cholesky/benchmark.sh -o ./benchmark -t 24:00 --mpi --linear-dim -d 2580 -p 1 ./cholesky/build/cholesky_openmp_ll_naive
Benchmark 'cholesky_openmp_ll_naive' for
> Dimensions:		2580, 
> Number of processors:	1, 
> CPU Model:		XeonGold_6150
> Use MPI:		true
> Use OpenMP:		false

Timestamp: 2022-01-14_23-31-29
