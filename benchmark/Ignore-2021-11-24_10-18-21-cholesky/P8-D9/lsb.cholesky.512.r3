# Sysname : Linux
# Nodename: eu-a6-012-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:19:45 2021
# Execution time and date (local): Wed Nov 24 10:19:45 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
          512      cholesky          0 25925.801758            0 
          512      cholesky          1 25848.631836            1 
          512      cholesky          2 25853.834961            0 
          512      cholesky          3 25808.055664            0 
          512      cholesky          4 25810.139648            0 
          512      cholesky          5 25814.525391            0 
          512      cholesky          6 25826.042969            0 
          512      cholesky          7 25807.198242            0 
          512      cholesky          8 25835.208008            1 
          512      cholesky          9 25856.118164            0 
# Runtime: 0.258406 s (overhead: 0.000774 %) 10 records
