# Sysname : Linux
# Nodename: eu-a6-012-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:19:45 2021
# Execution time and date (local): Wed Nov 24 10:19:45 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
          512      cholesky          0 26119.277344            0 
          512      cholesky          1 25918.500977            3 
          512      cholesky          2 26005.349609            1 
          512      cholesky          3 25946.466797            0 
          512      cholesky          4 25913.005859            1 
          512      cholesky          5 25902.827148            0 
          512      cholesky          6 26001.061523            0 
          512      cholesky          7 25955.884766            0 
          512      cholesky          8 25895.511719            1 
          512      cholesky          9 25909.584961            0 
# Runtime: 0.259596 s (overhead: 0.002311 %) 10 records
