# Sysname : Linux
# Nodename: eu-a6-006-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:41 2021
# Execution time and date (local): Wed Nov 24 10:18:41 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
           32      cholesky          0 9.529297            0 
           32      cholesky          1 12.133789            0 
           32      cholesky          2 4.719727            0 
           32      cholesky          3 4.632812            0 
           32      cholesky          4 4.597656            0 
           32      cholesky          5 4.582031            0 
           32      cholesky          6 4.547852            0 
           32      cholesky          7 4.550781            0 
           32      cholesky          8 4.551758            0 
           32      cholesky          9 4.565430            0 
# Runtime: 0.000096 s (overhead: 0.000000 %) 10 records
