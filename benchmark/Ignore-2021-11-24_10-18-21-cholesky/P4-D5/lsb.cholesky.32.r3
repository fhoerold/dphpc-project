# Sysname : Linux
# Nodename: eu-a6-006-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:41 2021
# Execution time and date (local): Wed Nov 24 10:18:41 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
           32      cholesky          0 9.250977            0 
           32      cholesky          1 11.982422            0 
           32      cholesky          2 4.705078            0 
           32      cholesky          3 4.657227            0 
           32      cholesky          4 4.578125            0 
           32      cholesky          5 4.590820            0 
           32      cholesky          6 4.578125            0 
           32      cholesky          7 4.560547            0 
           32      cholesky          8 4.560547            0 
           32      cholesky          9 4.562500            0 
# Runtime: 0.000077 s (overhead: 0.000000 %) 10 records
