# Sysname : Linux
# Nodename: eu-a6-006-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:19:51 2021
# Execution time and date (local): Wed Nov 24 10:19:51 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
         2048      cholesky          0 1895516.269531            0 
         2048      cholesky          1 1891836.772461            2 
         2048      cholesky          2 1898741.858398            2 
         2048      cholesky          3 1902287.947266            0 
         2048      cholesky          4 1887272.051758            8 
         2048      cholesky          5 1894835.416016            0 
         2048      cholesky          6 1905282.466797            0 
         2048      cholesky          7 1956018.477539            0 
         2048      cholesky          8 1943403.279297            2 
         2048      cholesky          9 1940824.965820            0 
# Runtime: 19.116069 s (overhead: 0.000073 %) 10 records
