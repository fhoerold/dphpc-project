# Sysname : Linux
# Nodename: eu-a6-006-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:19:50 2021
# Execution time and date (local): Wed Nov 24 10:19:50 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
         2048      cholesky          0 1915000.572266            0 
         2048      cholesky          1 1922492.582031            2 
         2048      cholesky          2 1929407.281250            2 
         2048      cholesky          3 1993622.760742            1 
         2048      cholesky          4 1984370.289062            2 
         2048      cholesky          5 1981772.102539            0 
         2048      cholesky          6 1987251.939453            0 
         2048      cholesky          7 2015340.587891            0 
         2048      cholesky          8 2013987.831055            2 
         2048      cholesky          9 2016200.479492            0 
# Runtime: 19.759481 s (overhead: 0.000046 %) 10 records
