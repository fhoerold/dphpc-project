# Sysname : Linux
# Nodename: eu-a6-010-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:59:49 2021
# Execution time and date (local): Wed Nov 24 10:59:49 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
          128      cholesky          0 337.011719            0 
          128      cholesky          1 317.694336            1 
          128      cholesky          2 312.353516            0 
          128      cholesky          3 312.426758            0 
          128      cholesky          4 315.420898            0 
          128      cholesky          5 312.100586            0 
          128      cholesky          6 312.189453            0 
          128      cholesky          7 324.877930            0 
          128      cholesky          8 312.624023            0 
          128      cholesky          9 312.316406            0 
# Runtime: 0.003193 s (overhead: 0.031315 %) 10 records
