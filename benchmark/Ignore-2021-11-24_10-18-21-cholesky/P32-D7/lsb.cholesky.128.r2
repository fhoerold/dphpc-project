# Sysname : Linux
# Nodename: eu-a6-010-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:59:49 2021
# Execution time and date (local): Wed Nov 24 10:59:49 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
          128      cholesky          0 323.265625            0 
          128      cholesky          1 326.903320            2 
          128      cholesky          2 312.923828            0 
          128      cholesky          3 312.375977            0 
          128      cholesky          4 312.435547            0 
          128      cholesky          5 321.806641            0 
          128      cholesky          6 312.341797            0 
          128      cholesky          7 312.149414            0 
          128      cholesky          8 315.790039            0 
          128      cholesky          9 312.040039            0 
# Runtime: 0.003189 s (overhead: 0.062720 %) 10 records
