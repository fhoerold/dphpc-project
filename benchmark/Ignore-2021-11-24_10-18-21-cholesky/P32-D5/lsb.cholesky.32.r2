# Sysname : Linux
# Nodename: eu-a6-010-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:27:54 2021
# Execution time and date (local): Wed Nov 24 10:27:54 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
           32      cholesky          0 8.544922            0 
           32      cholesky          1 10.175781            0 
           32      cholesky          2 4.672852            0 
           32      cholesky          3 4.589844            0 
           32      cholesky          4 4.590820            0 
           32      cholesky          5 4.561523            0 
           32      cholesky          6 4.576172            0 
           32      cholesky          7 4.575195            0 
           32      cholesky          8 4.570312            0 
           32      cholesky          9 4.565430            0 
# Runtime: 0.000080 s (overhead: 0.000000 %) 10 records
