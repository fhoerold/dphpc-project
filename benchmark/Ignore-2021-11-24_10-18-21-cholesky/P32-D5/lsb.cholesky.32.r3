# Sysname : Linux
# Nodename: eu-a6-010-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:27:54 2021
# Execution time and date (local): Wed Nov 24 10:27:54 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
           32      cholesky          0 8.166992            0 
           32      cholesky          1 10.179688            1 
           32      cholesky          2 4.763672            0 
           32      cholesky          3 4.612305            0 
           32      cholesky          4 4.568359            0 
           32      cholesky          5 4.573242            0 
           32      cholesky          6 4.564453            0 
           32      cholesky          7 4.569336            0 
           32      cholesky          8 4.554688            0 
           32      cholesky          9 4.567383            0 
# Runtime: 0.000078 s (overhead: 1.281698 %) 10 records
