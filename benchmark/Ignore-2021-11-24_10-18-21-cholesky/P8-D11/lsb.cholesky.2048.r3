# Sysname : Linux
# Nodename: eu-a6-004-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:20:14 2021
# Execution time and date (local): Wed Nov 24 10:20:14 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
         2048      cholesky          0 1890807.548828            0 
         2048      cholesky          1 1954611.375977            2 
         2048      cholesky          2 1986892.575195            3 
         2048      cholesky          3 2002655.190430            0 
         2048      cholesky          4 2008649.693359            2 
         2048      cholesky          5 2010250.062500            0 
         2048      cholesky          6 2021661.753906            0 
         2048      cholesky          7 2024983.016602            0 
         2048      cholesky          8 2029819.137695            2 
         2048      cholesky          9 2035706.695312            0 
# Runtime: 19.966084 s (overhead: 0.000045 %) 10 records
