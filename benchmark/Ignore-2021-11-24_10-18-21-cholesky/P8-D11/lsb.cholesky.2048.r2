# Sysname : Linux
# Nodename: eu-a6-004-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:20:16 2021
# Execution time and date (local): Wed Nov 24 10:20:16 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
         2048      cholesky          0 1984597.541992            0 
         2048      cholesky          1 2008547.124023            3 
         2048      cholesky          2 2025991.391602           12 
         2048      cholesky          3 2027435.113281            0 
         2048      cholesky          4 2031826.288086            2 
         2048      cholesky          5 2026711.795898            1 
         2048      cholesky          6 2030180.531250            0 
         2048      cholesky          7 2037507.146484            0 
         2048      cholesky          8 2038957.445312            3 
         2048      cholesky          9 1873467.376953            0 
# Runtime: 20.085268 s (overhead: 0.000105 %) 10 records
