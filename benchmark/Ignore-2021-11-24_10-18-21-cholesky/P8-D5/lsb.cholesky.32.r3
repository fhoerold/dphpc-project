# Sysname : Linux
# Nodename: eu-a6-012-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:19:36 2021
# Execution time and date (local): Wed Nov 24 10:19:36 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
           32      cholesky          0 10.174805            0 
           32      cholesky          1 13.439453            0 
           32      cholesky          2 4.724609            0 
           32      cholesky          3 4.616211            0 
           32      cholesky          4 4.586914            0 
           32      cholesky          5 4.596680            0 
           32      cholesky          6 4.629883            0 
           32      cholesky          7 4.571289            0 
           32      cholesky          8 4.536133            0 
           32      cholesky          9 4.572266            0 
# Runtime: 0.000082 s (overhead: 0.000000 %) 10 records
