# Sysname : Linux
# Nodename: eu-a6-012-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:19:36 2021
# Execution time and date (local): Wed Nov 24 10:19:36 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
           32      cholesky          0 8.607422            0 
           32      cholesky          1 10.861328            1 
           32      cholesky          2 4.714844            0 
           32      cholesky          3 4.650391            0 
           32      cholesky          4 4.595703            0 
           32      cholesky          5 4.611328            0 
           32      cholesky          6 4.572266            0 
           32      cholesky          7 4.576172            0 
           32      cholesky          8 4.564453            0 
           32      cholesky          9 4.580078            0 
# Runtime: 0.000071 s (overhead: 1.402394 %) 10 records
