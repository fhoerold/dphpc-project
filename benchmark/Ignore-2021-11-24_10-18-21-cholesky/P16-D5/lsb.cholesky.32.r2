# Sysname : Linux
# Nodename: eu-a6-001-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:24:10 2021
# Execution time and date (local): Wed Nov 24 10:24:10 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
           32      cholesky          0 8.384766            0 
           32      cholesky          1 10.047852            1 
           32      cholesky          2 4.675781            0 
           32      cholesky          3 4.655273            0 
           32      cholesky          4 4.595703            0 
           32      cholesky          5 4.581055            0 
           32      cholesky          6 4.596680            0 
           32      cholesky          7 4.589844            0 
           32      cholesky          8 4.533203            0 
           32      cholesky          9 4.569336            0 
# Runtime: 0.000072 s (overhead: 1.389398 %) 10 records
