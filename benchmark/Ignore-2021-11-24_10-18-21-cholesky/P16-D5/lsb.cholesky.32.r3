# Sysname : Linux
# Nodename: eu-a6-001-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:24:10 2021
# Execution time and date (local): Wed Nov 24 10:24:10 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
           32      cholesky          0 10.275391            0 
           32      cholesky          1 12.725586            0 
           32      cholesky          2 4.668945            0 
           32      cholesky          3 4.637695            0 
           32      cholesky          4 4.556641            0 
           32      cholesky          5 4.584961            0 
           32      cholesky          6 4.652344            0 
           32      cholesky          7 4.564453            0 
           32      cholesky          8 4.541992            0 
           32      cholesky          9 4.546875            0 
# Runtime: 0.000080 s (overhead: 0.000000 %) 10 records
