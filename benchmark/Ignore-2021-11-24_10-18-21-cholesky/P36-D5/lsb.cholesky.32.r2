# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 10:11:30 2021
# Execution time and date (local): Wed Nov 24 11:11:30 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
           32      cholesky          0 8.325195            0 
           32      cholesky          1 9.996094            3 
           32      cholesky          2 4.732422            0 
           32      cholesky          3 4.678711            0 
           32      cholesky          4 4.577148            0 
           32      cholesky          5 4.587891            0 
           32      cholesky          6 4.586914            0 
           32      cholesky          7 4.577148            0 
           32      cholesky          8 4.558594            0 
           32      cholesky          9 4.572266            0 
# Runtime: 0.000080 s (overhead: 3.747393 %) 10 records
