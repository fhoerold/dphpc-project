# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 10:11:30 2021
# Execution time and date (local): Wed Nov 24 11:11:30 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
           32      cholesky          0 8.471680            0 
           32      cholesky          1 9.551758            2 
           32      cholesky          2 4.733398            0 
           32      cholesky          3 4.653320            0 
           32      cholesky          4 4.577148            0 
           32      cholesky          5 4.580078            0 
           32      cholesky          6 4.578125            0 
           32      cholesky          7 4.596680            0 
           32      cholesky          8 4.563477            0 
           32      cholesky          9 4.588867            0 
# Runtime: 0.000079 s (overhead: 2.533525 %) 10 records
