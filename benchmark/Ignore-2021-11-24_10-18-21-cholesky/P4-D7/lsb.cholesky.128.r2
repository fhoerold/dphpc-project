# Sysname : Linux
# Nodename: eu-a6-006-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:55 2021
# Execution time and date (local): Wed Nov 24 10:18:55 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
          128      cholesky          0 332.400391            0 
          128      cholesky          1 317.539062            0 
          128      cholesky          2 312.148438            0 
          128      cholesky          3 324.738281            0 
          128      cholesky          4 311.766602            0 
          128      cholesky          5 311.658203            0 
          128      cholesky          6 316.618164            0 
          128      cholesky          7 311.583984            0 
          128      cholesky          8 311.474609            0 
          128      cholesky          9 316.228516            0 
# Runtime: 0.003190 s (overhead: 0.000000 %) 10 records
