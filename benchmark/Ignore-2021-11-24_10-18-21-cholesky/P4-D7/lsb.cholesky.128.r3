# Sysname : Linux
# Nodename: eu-a6-006-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:55 2021
# Execution time and date (local): Wed Nov 24 10:18:55 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
          128      cholesky          0 321.716797            0 
          128      cholesky          1 339.577148            1 
          128      cholesky          2 311.051758            0 
          128      cholesky          3 310.724609            0 
          128      cholesky          4 321.422852            0 
          128      cholesky          5 310.690430            0 
          128      cholesky          6 310.548828            0 
          128      cholesky          7 310.532227            0 
          128      cholesky          8 316.389648            0 
          128      cholesky          9 310.428711            0 
# Runtime: 0.003192 s (overhead: 0.031327 %) 10 records
