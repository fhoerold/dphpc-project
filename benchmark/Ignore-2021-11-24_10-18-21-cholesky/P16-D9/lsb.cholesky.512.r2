# Sysname : Linux
# Nodename: eu-a6-002-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:22:21 2021
# Execution time and date (local): Wed Nov 24 10:22:21 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
          512      cholesky          0 24628.182617            0 
          512      cholesky          1 24207.479492            0 
          512      cholesky          2 23811.663086            0 
          512      cholesky          3 23891.352539            0 
          512      cholesky          4 24471.404297            0 
          512      cholesky          5 23709.862305            0 
          512      cholesky          6 23802.736328            0 
          512      cholesky          7 23711.159180            0 
          512      cholesky          8 24607.987305            0 
          512      cholesky          9 23707.949219            0 
# Runtime: 0.240595 s (overhead: 0.000000 %) 10 records
