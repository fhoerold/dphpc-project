# Sysname : Linux
# Nodename: eu-a6-002-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:22:21 2021
# Execution time and date (local): Wed Nov 24 10:22:21 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
          512      cholesky          0 24302.310547            0 
          512      cholesky          1 24011.328125            0 
          512      cholesky          2 24542.885742            0 
          512      cholesky          3 24170.471680            0 
          512      cholesky          4 23900.557617            0 
          512      cholesky          5 23984.351562            0 
          512      cholesky          6 24369.858398            0 
          512      cholesky          7 24310.891602            0 
          512      cholesky          8 24120.304688            0 
          512      cholesky          9 23974.053711            0 
# Runtime: 0.241712 s (overhead: 0.000000 %) 10 records
