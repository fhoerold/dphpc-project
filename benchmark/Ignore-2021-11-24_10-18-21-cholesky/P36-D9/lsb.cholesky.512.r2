# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 10:11:47 2021
# Execution time and date (local): Wed Nov 24 11:11:47 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
          512      cholesky          0 25873.200195            0 
          512      cholesky          1 25787.907227            0 
          512      cholesky          2 25788.739258            0 
          512      cholesky          3 25771.993164            0 
          512      cholesky          4 25763.892578            0 
          512      cholesky          5 25756.450195            0 
          512      cholesky          6 27165.802734            1 
          512      cholesky          7 25811.603516            0 
          512      cholesky          8 25794.368164            9 
          512      cholesky          9 25785.585938            0 
# Runtime: 0.259334 s (overhead: 0.003856 %) 10 records
