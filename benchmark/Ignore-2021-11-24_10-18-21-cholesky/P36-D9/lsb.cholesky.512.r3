# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 10:11:47 2021
# Execution time and date (local): Wed Nov 24 11:11:47 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
          512      cholesky          0 26028.697266            0 
          512      cholesky          1 25831.795898            0 
          512      cholesky          2 25820.112305            0 
          512      cholesky          3 25825.520508            0 
          512      cholesky          4 25816.308594            0 
          512      cholesky          5 25827.287109            0 
          512      cholesky          6 27244.980469            1 
          512      cholesky          7 25814.901367            0 
          512      cholesky          8 25820.722656           12 
          512      cholesky          9 25859.675781            0 
# Runtime: 0.259933 s (overhead: 0.005001 %) 10 records
