# Sysname : Linux
# Nodename: eu-a6-002-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:23:33 2021
# Execution time and date (local): Wed Nov 24 10:23:33 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
         2048      cholesky          0 2168141.866211            0 
         2048      cholesky          1 2171535.653320            3 
         2048      cholesky          2 2172421.151367            2 
         2048      cholesky          3 2172573.226562            0 
         2048      cholesky          4 2173919.524414            9 
         2048      cholesky          5 2185079.319336            0 
         2048      cholesky          6 2166402.252930            0 
         2048      cholesky          7 2112418.096680            1 
         2048      cholesky          8 2006421.165039           15 
         2048      cholesky          9 1926566.351562            0 
# Runtime: 21.255551 s (overhead: 0.000141 %) 10 records
