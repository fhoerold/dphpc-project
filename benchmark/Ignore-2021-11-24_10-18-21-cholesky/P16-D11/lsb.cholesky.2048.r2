# Sysname : Linux
# Nodename: eu-a6-002-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:23:36 2021
# Execution time and date (local): Wed Nov 24 10:23:36 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
         2048      cholesky          0 2147532.115234            0 
         2048      cholesky          1 2150779.665039            3 
         2048      cholesky          2 2151974.492188           11 
         2048      cholesky          3 2155149.575195            0 
         2048      cholesky          4 2150741.464844            3 
         2048      cholesky          5 2174529.931641            0 
         2048      cholesky          6 2162696.037109            3 
         2048      cholesky          7 2067064.239258            1 
         2048      cholesky          8 1981196.498047           16 
         2048      cholesky          9 1905626.327148            0 
# Runtime: 21.047375 s (overhead: 0.000176 %) 10 records
