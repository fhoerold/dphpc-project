# Sysname : Linux
# Nodename: eu-a6-002-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:22:08 2021
# Execution time and date (local): Wed Nov 24 10:22:08 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
          128      cholesky          0 361.817383            0 
          128      cholesky          1 326.739258            0 
          128      cholesky          2 311.871094            0 
          128      cholesky          3 311.970703            0 
          128      cholesky          4 322.922852            0 
          128      cholesky          5 311.667969            0 
          128      cholesky          6 311.701172            0 
          128      cholesky          7 316.334961            0 
          128      cholesky          8 311.679688            0 
          128      cholesky          9 311.525391            0 
# Runtime: 0.003217 s (overhead: 0.000000 %) 10 records
