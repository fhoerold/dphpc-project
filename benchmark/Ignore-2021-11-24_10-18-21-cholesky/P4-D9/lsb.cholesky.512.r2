# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:50 2021
# Execution time and date (local): Wed Nov 24 10:18:50 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
          512      cholesky          0 26631.059570            0 
          512      cholesky          1 26868.104492            1 
          512      cholesky          2 27004.462891            1 
          512      cholesky          3 26962.874023            0 
          512      cholesky          4 26957.572266            1 
          512      cholesky          5 27062.517578            0 
          512      cholesky          6 26888.719727            0 
          512      cholesky          7 26864.007812            0 
          512      cholesky          8 26835.968750            1 
          512      cholesky          9 26893.973633            0 
# Runtime: 0.269000 s (overhead: 0.001487 %) 10 records
