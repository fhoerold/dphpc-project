# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:50 2021
# Execution time and date (local): Wed Nov 24 10:18:50 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
          512      cholesky          0 26975.627930            0 
          512      cholesky          1 27182.836914            1 
          512      cholesky          2 26968.678711            1 
          512      cholesky          3 26976.094727            0 
          512      cholesky          4 27099.373047            2 
          512      cholesky          5 27193.858398            0 
          512      cholesky          6 27168.108398            0 
          512      cholesky          7 27142.194336            0 
          512      cholesky          8 26967.277344            2 
          512      cholesky          9 26911.569336            0 
# Runtime: 0.270626 s (overhead: 0.002217 %) 10 records
