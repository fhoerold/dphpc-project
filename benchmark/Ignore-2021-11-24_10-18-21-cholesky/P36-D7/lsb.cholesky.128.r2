# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 10:05:55 2021
# Execution time and date (local): Wed Nov 24 11:05:55 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
          128      cholesky          0 766.851562            0 
          128      cholesky          1 317.720703            0 
          128      cholesky          2 312.388672            2 
          128      cholesky          3 317.159180            0 
          128      cholesky          4 312.215820            0 
          128      cholesky          5 312.110352            0 
          128      cholesky          6 312.215820            0 
          128      cholesky          7 317.376953            0 
          128      cholesky          8 312.212891            0 
          128      cholesky          9 312.154297            0 
# Runtime: 0.003613 s (overhead: 0.055357 %) 10 records
