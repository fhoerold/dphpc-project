# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 10:05:55 2021
# Execution time and date (local): Wed Nov 24 11:05:55 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
          128      cholesky          0 858.710938            0 
          128      cholesky          1 317.999023            0 
          128      cholesky          2 312.341797            0 
          128      cholesky          3 323.659180            0 
          128      cholesky          4 312.375977            1 
          128      cholesky          5 312.125000            0 
          128      cholesky          6 317.809570            0 
          128      cholesky          7 311.994141            0 
          128      cholesky          8 312.235352            0 
          128      cholesky          9 316.563477            0 
# Runtime: 0.003715 s (overhead: 0.026915 %) 10 records
