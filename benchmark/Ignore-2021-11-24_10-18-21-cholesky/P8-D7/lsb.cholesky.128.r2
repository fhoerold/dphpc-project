# Sysname : Linux
# Nodename: eu-a6-004-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:19:00 2021
# Execution time and date (local): Wed Nov 24 10:19:00 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
          128      cholesky          0 320.225586            0 
          128      cholesky          1 317.146484            1 
          128      cholesky          2 326.447266            0 
          128      cholesky          3 312.016602            0 
          128      cholesky          4 311.733398            0 
          128      cholesky          5 317.083984            0 
          128      cholesky          6 311.584961            0 
          128      cholesky          7 311.580078            0 
          128      cholesky          8 311.849609            0 
          128      cholesky          9 317.730469            0 
# Runtime: 0.003169 s (overhead: 0.031558 %) 10 records
