# Sysname : Linux
# Nodename: eu-a6-004-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:19:00 2021
# Execution time and date (local): Wed Nov 24 10:19:00 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
          128      cholesky          0 321.382812            0 
          128      cholesky          1 317.411133            0 
          128      cholesky          2 322.249023            0 
          128      cholesky          3 312.098633            0 
          128      cholesky          4 311.979492            0 
          128      cholesky          5 317.101562            0 
          128      cholesky          6 311.899414            0 
          128      cholesky          7 311.867188            0 
          128      cholesky          8 316.727539            0 
          128      cholesky          9 311.759766            0 
# Runtime: 0.003177 s (overhead: 0.000000 %) 10 records
