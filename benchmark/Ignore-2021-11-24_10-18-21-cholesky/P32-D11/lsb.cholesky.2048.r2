# Sysname : Linux
# Nodename: eu-a6-010-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:29:34 2021
# Execution time and date (local): Wed Nov 24 10:29:34 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
         2048      cholesky          0 1985485.302734            0 
         2048      cholesky          1 2138663.411133            3 
         2048      cholesky          2 2183772.438477           10 
         2048      cholesky          3 2190995.612305            1 
         2048      cholesky          4 2238475.407227           12 
         2048      cholesky          5 2242798.025391            0 
         2048      cholesky          6 2246239.981445            0 
         2048      cholesky          7 2241008.274414            0 
         2048      cholesky          8 2252908.950195           19 
         2048      cholesky          9 2196355.021484            0 
# Runtime: 21.916796 s (overhead: 0.000205 %) 10 records
