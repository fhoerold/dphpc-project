# Sysname : Linux
# Nodename: eu-a6-010-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:29:36 2021
# Execution time and date (local): Wed Nov 24 10:29:36 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
         2048      cholesky          0 2240439.922852            0 
         2048      cholesky          1 2241264.823242            3 
         2048      cholesky          2 2241548.767578           14 
         2048      cholesky          3 2235521.984375            0 
         2048      cholesky          4 2231323.481445            9 
         2048      cholesky          5 2204306.875977            0 
         2048      cholesky          6 2070941.664062            0 
         2048      cholesky          7 1740633.280273            3 
         2048      cholesky          8 1719059.492188           20 
         2048      cholesky          9 1719025.741211            0 
# Runtime: 20.644160 s (overhead: 0.000237 %) 10 records
