# Sysname : Linux
# Nodename: eu-a6-010-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:28:08 2021
# Execution time and date (local): Wed Nov 24 10:28:08 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
          512      cholesky          0 25795.285156            0 
          512      cholesky          1 25769.686523            1 
          512      cholesky          2 25739.808594            0 
          512      cholesky          3 25730.303711            0 
          512      cholesky          4 25722.125000            0 
          512      cholesky          5 25731.640625            0 
          512      cholesky          6 25737.588867            0 
          512      cholesky          7 25733.505859            0 
          512      cholesky          8 25729.649414            0 
          512      cholesky          9 25741.935547            0 
# Runtime: 0.257458 s (overhead: 0.000388 %) 10 records
