# Sysname : Linux
# Nodename: eu-a6-010-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:28:08 2021
# Execution time and date (local): Wed Nov 24 10:28:08 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
          512      cholesky          0 25908.416016            0 
          512      cholesky          1 25879.624023            0 
          512      cholesky          2 25875.131836            0 
          512      cholesky          3 25864.227539            0 
          512      cholesky          4 25866.798828            0 
          512      cholesky          5 25861.703125            0 
          512      cholesky          6 25853.560547            0 
          512      cholesky          7 25860.729492            0 
          512      cholesky          8 25854.707031            0 
          512      cholesky          9 25861.629883            0 
# Runtime: 0.258718 s (overhead: 0.000000 %) 10 records
