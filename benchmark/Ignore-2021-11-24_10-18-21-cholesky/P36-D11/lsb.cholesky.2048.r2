# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 10:07:11 2021
# Execution time and date (local): Wed Nov 24 11:07:11 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
         2048      cholesky          0 1997380.522461            0 
         2048      cholesky          1 2096202.816406            4 
         2048      cholesky          2 2166233.438477           17 
         2048      cholesky          3 2373454.120117            0 
         2048      cholesky          4 2393558.395508           11 
         2048      cholesky          5 2392779.678711            0 
         2048      cholesky          6 2392050.003906            0 
         2048      cholesky          7 2395313.916016            0 
         2048      cholesky          8 2394865.639648           17 
         2048      cholesky          9 2339874.344727            0 
# Runtime: 22.941819 s (overhead: 0.000214 %) 10 records
