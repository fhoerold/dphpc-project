# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 10:07:12 2021
# Execution time and date (local): Wed Nov 24 11:07:12 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         size       region       id         time     overhead 
         2048      cholesky          0 2108680.583984            0 
         2048      cholesky          1 2165773.695312           12 
         2048      cholesky          2 2391778.797852           13 
         2048      cholesky          3 2394381.933594            0 
         2048      cholesky          4 2396642.742188           16 
         2048      cholesky          5 2401126.182617            0 
         2048      cholesky          6 2399543.561523            0 
         2048      cholesky          7 2395632.202148            0 
         2048      cholesky          8 2344504.003906           14 
         2048      cholesky          9 2059063.416992            0 
# Runtime: 23.057230 s (overhead: 0.000239 %) 10 records
