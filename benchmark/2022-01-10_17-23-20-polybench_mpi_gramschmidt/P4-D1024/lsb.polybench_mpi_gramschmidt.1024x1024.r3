# Sysname : Linux
# Nodename: eu-a6-002-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:23:44 2022
# Execution time and date (local): Mon Jan 10 17:23:44 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 10768403.732422            0 
         1024          1024   gramschmidt          1 1676940.623047            4 
         1024          1024   gramschmidt          2 1660672.205078            3 
         1024          1024   gramschmidt          3 1665330.658203            0 
         1024          1024   gramschmidt          4 1660747.039062            2 
         1024          1024   gramschmidt          5 1658290.626953            0 
         1024          1024   gramschmidt          6 1667327.210938            0 
         1024          1024   gramschmidt          7 1634185.876953            0 
         1024          1024   gramschmidt          8 1651092.423828            4 
         1024          1024   gramschmidt          9 1642123.257812            0 
# Runtime: 25.685161 s (overhead: 0.000051 %) 10 records
