# Sysname : Linux
# Nodename: eu-a6-002-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:23:44 2022
# Execution time and date (local): Mon Jan 10 17:23:44 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 1786093.560547            0 
         1024          1024   gramschmidt          1 1678510.960938            6 
         1024          1024   gramschmidt          2 1662672.464844            5 
         1024          1024   gramschmidt          3 1667094.044922            0 
         1024          1024   gramschmidt          4 1662761.718750            5 
         1024          1024   gramschmidt          5 1660169.828125            0 
         1024          1024   gramschmidt          6 1669031.775391            0 
         1024          1024   gramschmidt          7 1636191.753906            0 
         1024          1024   gramschmidt          8 1652962.396484            9 
         1024          1024   gramschmidt          9 1643966.267578            0 
# Runtime: 16.719515 s (overhead: 0.000150 %) 10 records
