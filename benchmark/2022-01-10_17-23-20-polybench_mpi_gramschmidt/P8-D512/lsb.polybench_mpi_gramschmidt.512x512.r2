# Sysname : Linux
# Nodename: eu-a6-011-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:23:44 2022
# Execution time and date (local): Mon Jan 10 17:23:44 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 1067501.054688            0 
          512           512   gramschmidt          1 57970.062500            5 
          512           512   gramschmidt          2 57903.771484            1 
          512           512   gramschmidt          3 57939.947266            0 
          512           512   gramschmidt          4 57847.261719            0 
          512           512   gramschmidt          5 57715.183594            0 
          512           512   gramschmidt          6 57589.851562            0 
          512           512   gramschmidt          7 57524.847656            0 
          512           512   gramschmidt          8 57708.216797            1 
          512           512   gramschmidt          9 57503.570312            0 
# Runtime: 1.587231 s (overhead: 0.000441 %) 10 records
