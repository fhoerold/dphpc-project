# Sysname : Linux
# Nodename: eu-a6-011-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:23:44 2022
# Execution time and date (local): Mon Jan 10 17:23:44 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 64365.802734            0 
          512           512   gramschmidt          1 57970.771484            1 
          512           512   gramschmidt          2 57916.509766            1 
          512           512   gramschmidt          3 57958.832031            0 
          512           512   gramschmidt          4 57823.169922            1 
          512           512   gramschmidt          5 57720.095703            0 
          512           512   gramschmidt          6 57597.957031            0 
          512           512   gramschmidt          7 57533.943359            0 
          512           512   gramschmidt          8 57678.472656            1 
          512           512   gramschmidt          9 57514.476562            0 
# Runtime: 0.584104 s (overhead: 0.000685 %) 10 records
