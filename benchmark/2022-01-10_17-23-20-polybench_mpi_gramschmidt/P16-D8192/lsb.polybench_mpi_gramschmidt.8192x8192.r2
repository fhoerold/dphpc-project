# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:42:08 2022
# Execution time and date (local): Mon Jan 10 17:42:08 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 1014878699.927734            0 
         8192          8192   gramschmidt          1 1043599572.361328            5 
         8192          8192   gramschmidt          2 955683487.173828            6 
         8192          8192   gramschmidt          3 1129426432.833984            0 
         8192          8192   gramschmidt          4 935379185.023438            5 
         8192          8192   gramschmidt          5 1031202058.607422            0 
         8192          8192   gramschmidt          6 999937974.074219            0 
         8192          8192   gramschmidt          7 982219019.240234            0 
         8192          8192   gramschmidt          8 993200193.216797            4 
         8192          8192   gramschmidt          9 1062305678.650391            0 
# Runtime: 10147.832364 s (overhead: 0.000000 %) 10 records
