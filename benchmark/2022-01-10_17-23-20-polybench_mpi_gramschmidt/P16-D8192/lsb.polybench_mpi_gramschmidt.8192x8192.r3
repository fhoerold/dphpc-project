# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:42:08 2022
# Execution time and date (local): Mon Jan 10 17:42:08 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 1021788406.394531            0 
         8192          8192   gramschmidt          1 1043502884.375000            4 
         8192          8192   gramschmidt          2 955595168.083984            5 
         8192          8192   gramschmidt          3 1129321935.214844            0 
         8192          8192   gramschmidt          4 935292753.523438            5 
         8192          8192   gramschmidt          5 1031106611.677734            0 
         8192          8192   gramschmidt          6 999845524.117188            0 
         8192          8192   gramschmidt          7 982128131.257812            0 
         8192          8192   gramschmidt          8 993108528.605469            5 
         8192          8192   gramschmidt          9 1062207197.470703            0 
# Runtime: 10153.897202 s (overhead: 0.000000 %) 10 records
