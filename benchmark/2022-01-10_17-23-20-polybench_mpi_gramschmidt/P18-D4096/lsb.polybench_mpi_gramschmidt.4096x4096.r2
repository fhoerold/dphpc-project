# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 17:46:49 2022
# Execution time and date (local): Mon Jan 10 18:46:49 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 67245691.656250            0 
         4096          4096   gramschmidt          1 49037307.664062            4 
         4096          4096   gramschmidt          2 48885075.939453            1 
         4096          4096   gramschmidt          3 51783942.582031            0 
         4096          4096   gramschmidt          4 48026300.927734            4 
         4096          4096   gramschmidt          5 47776121.619141            0 
         4096          4096   gramschmidt          6 47488979.687500            2 
         4096          4096   gramschmidt          7 46462640.398438            0 
         4096          4096   gramschmidt          8 46380798.468750            3 
         4096          4096   gramschmidt          9 46651168.923828            0 
# Runtime: 499.738077 s (overhead: 0.000003 %) 10 records
