# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 17:46:49 2022
# Execution time and date (local): Mon Jan 10 18:46:49 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 67293179.656250            0 
         4096          4096   gramschmidt          1 49058074.826172            4 
         4096          4096   gramschmidt          2 48907178.968750            2 
         4096          4096   gramschmidt          3 51806655.212891            0 
         4096          4096   gramschmidt          4 48046921.402344            4 
         4096          4096   gramschmidt          5 47809774.445312            0 
         4096          4096   gramschmidt          6 47517331.669922            0 
         4096          4096   gramschmidt          7 46462289.015625            0 
         4096          4096   gramschmidt          8 46400936.910156            4 
         4096          4096   gramschmidt          9 46680090.402344            0 
# Runtime: 499.982495 s (overhead: 0.000003 %) 10 records
