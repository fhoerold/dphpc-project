# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 19:34:51 2022
# Execution time and date (local): Mon Jan 10 20:34:51 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 39434966.097656            0 
         4096          4096   gramschmidt          1 25320774.023438            4 
         4096          4096   gramschmidt          2 23994021.656250            2 
         4096          4096   gramschmidt          3 24646279.906250            0 
         4096          4096   gramschmidt          4 24266956.914062            6 
         4096          4096   gramschmidt          5 23524182.074219            0 
         4096          4096   gramschmidt          6 23551309.031250            0 
         4096          4096   gramschmidt          7 23684782.621094            0 
         4096          4096   gramschmidt          8 23269759.792969            6 
         4096          4096   gramschmidt          9 22787194.470703            0 
# Runtime: 254.480290 s (overhead: 0.000007 %) 10 records
