# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 19:34:51 2022
# Execution time and date (local): Mon Jan 10 20:34:51 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 42435849.800781            0 
         4096          4096   gramschmidt          1 25322356.294922            4 
         4096          4096   gramschmidt          2 23996284.322266            4 
         4096          4096   gramschmidt          3 24647606.753906            0 
         4096          4096   gramschmidt          4 24269163.572266            2 
         4096          4096   gramschmidt          5 23525599.072266            0 
         4096          4096   gramschmidt          6 23553060.835938            0 
         4096          4096   gramschmidt          7 23686984.300781            0 
         4096          4096   gramschmidt          8 23271073.035156            6 
         4096          4096   gramschmidt          9 22788889.216797            0 
# Runtime: 257.496926 s (overhead: 0.000006 %) 10 records
