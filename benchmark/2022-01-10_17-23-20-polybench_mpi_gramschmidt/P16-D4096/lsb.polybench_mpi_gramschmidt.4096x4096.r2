# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:32:38 2022
# Execution time and date (local): Mon Jan 10 17:32:38 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 76267518.984375            0 
         4096          4096   gramschmidt          1 52193229.019531            4 
         4096          4096   gramschmidt          2 51490140.253906            4 
         4096          4096   gramschmidt          3 51297555.824219            0 
         4096          4096   gramschmidt          4 50804765.429688            4 
         4096          4096   gramschmidt          5 50302163.113281            0 
         4096          4096   gramschmidt          6 50613267.261719            1 
         4096          4096   gramschmidt          7 50242447.978516            0 
         4096          4096   gramschmidt          8 50044253.435547            3 
         4096          4096   gramschmidt          9 49967239.029297            0 
# Runtime: 533.222633 s (overhead: 0.000003 %) 10 records
