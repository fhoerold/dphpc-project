# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:32:38 2022
# Execution time and date (local): Mon Jan 10 17:32:38 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 76270983.109375            0 
         4096          4096   gramschmidt          1 52193539.189453            3 
         4096          4096   gramschmidt          2 51490409.421875            4 
         4096          4096   gramschmidt          3 51296722.107422            0 
         4096          4096   gramschmidt          4 50805646.443359            4 
         4096          4096   gramschmidt          5 50302340.183594            0 
         4096          4096   gramschmidt          6 50613400.822266            0 
         4096          4096   gramschmidt          7 50242657.712891            0 
         4096          4096   gramschmidt          8 50044440.300781            3 
         4096          4096   gramschmidt          9 49966450.582031            0 
# Runtime: 533.226641 s (overhead: 0.000003 %) 10 records
