# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 19:33:01 2022
# Execution time and date (local): Mon Jan 10 20:33:01 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 8154048.158203            0 
         1024          1024   gramschmidt          1 137875.714844            3 
         1024          1024   gramschmidt          2 139035.728516            0 
         1024          1024   gramschmidt          3 137502.078125            0 
         1024          1024   gramschmidt          4 137219.976562            0 
         1024          1024   gramschmidt          5 136939.162109            0 
         1024          1024   gramschmidt          6 138048.673828            0 
         1024          1024   gramschmidt          7 137198.960938            0 
         1024          1024   gramschmidt          8 137017.691406            1 
         1024          1024   gramschmidt          9 137065.248047            0 
# Runtime: 9.391984 s (overhead: 0.000043 %) 10 records
