# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 19:33:01 2022
# Execution time and date (local): Mon Jan 10 20:33:01 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 7147151.259766            0 
         1024          1024   gramschmidt          1 138003.361328            5 
         1024          1024   gramschmidt          2 139140.638672            0 
         1024          1024   gramschmidt          3 137557.843750            0 
         1024          1024   gramschmidt          4 137290.921875            0 
         1024          1024   gramschmidt          5 136995.314453            0 
         1024          1024   gramschmidt          6 138147.177734            0 
         1024          1024   gramschmidt          7 137294.632812            0 
         1024          1024   gramschmidt          8 137096.292969            1 
         1024          1024   gramschmidt          9 137121.289062            0 
# Runtime: 8.385835 s (overhead: 0.000072 %) 10 records
