# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 17:55:48 2022
# Execution time and date (local): Mon Jan 10 18:55:48 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 760953173.652344            0 
         8192          8192   gramschmidt          1 580290797.902344            4 
         8192          8192   gramschmidt          2 514417272.597656            5 
         8192          8192   gramschmidt          3 494181843.408203            0 
         8192          8192   gramschmidt          4 485718677.248047            4 
         8192          8192   gramschmidt          5 489273691.148438            0 
         8192          8192   gramschmidt          6 476257595.716797            0 
         8192          8192   gramschmidt          7 394840985.441406            0 
         8192          8192   gramschmidt          8 398336358.482422            4 
         8192          8192   gramschmidt          9 358134344.060547            2 
# Runtime: 4952.404800 s (overhead: 0.000000 %) 10 records
