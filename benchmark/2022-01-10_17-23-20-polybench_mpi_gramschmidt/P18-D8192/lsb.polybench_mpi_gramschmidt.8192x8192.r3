# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 17:55:48 2022
# Execution time and date (local): Mon Jan 10 18:55:48 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 760939431.382812            0 
         8192          8192   gramschmidt          1 580277486.382812            4 
         8192          8192   gramschmidt          2 514404778.394531            5 
         8192          8192   gramschmidt          3 494168772.003906            0 
         8192          8192   gramschmidt          4 485714929.521484            4 
         8192          8192   gramschmidt          5 489252794.726562            0 
         8192          8192   gramschmidt          6 476246801.101562            0 
         8192          8192   gramschmidt          7 394837863.923828            0 
         8192          8192   gramschmidt          8 398318282.210938            5 
         8192          8192   gramschmidt          9 358125820.238281            0 
# Runtime: 4952.287002 s (overhead: 0.000000 %) 10 records
