# Sysname : Linux
# Nodename: eu-a6-011-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:24:14 2022
# Execution time and date (local): Mon Jan 10 17:24:14 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 156293956.853516            0 
         4096          4096   gramschmidt          1 121432133.033203            5 
         4096          4096   gramschmidt          2 136419627.216797            5 
         4096          4096   gramschmidt          3 141375080.863281            0 
         4096          4096   gramschmidt          4 119602603.507812            4 
         4096          4096   gramschmidt          5 116894707.605469            0 
         4096          4096   gramschmidt          6 115019817.347656            0 
         4096          4096   gramschmidt          7 125468564.207031            0 
         4096          4096   gramschmidt          8 128587582.152344            6 
         4096          4096   gramschmidt          9 114051229.550781            0 
# Runtime: 1275.145363 s (overhead: 0.000002 %) 10 records
