# Sysname : Linux
# Nodename: eu-a6-011-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:24:14 2022
# Execution time and date (local): Mon Jan 10 17:24:14 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 156296819.537109            0 
         4096          4096   gramschmidt          1 121435902.916016            4 
         4096          4096   gramschmidt          2 136424673.031250            5 
         4096          4096   gramschmidt          3 141380117.757812            0 
         4096          4096   gramschmidt          4 119607065.785156            4 
         4096          4096   gramschmidt          5 116898971.183594            0 
         4096          4096   gramschmidt          6 115024072.503906            0 
         4096          4096   gramschmidt          7 125473127.376953            0 
         4096          4096   gramschmidt          8 128592242.492188            4 
         4096          4096   gramschmidt          9 114055548.154297            0 
# Runtime: 1275.188617 s (overhead: 0.000001 %) 10 records
