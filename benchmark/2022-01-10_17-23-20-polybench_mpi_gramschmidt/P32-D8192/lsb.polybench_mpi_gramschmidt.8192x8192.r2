# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 19:39:50 2022
# Execution time and date (local): Mon Jan 10 20:39:50 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 292862020.966797            0 
         8192          8192   gramschmidt          1 243763813.726562            5 
         8192          8192   gramschmidt          2 258921201.117188            5 
         8192          8192   gramschmidt          3 275135008.531250            0 
         8192          8192   gramschmidt          4 260924553.611328            5 
         8192          8192   gramschmidt          5 304201505.242188            0 
         8192          8192   gramschmidt          6 277015393.173828            0 
         8192          8192   gramschmidt          7 265361247.273438            0 
         8192          8192   gramschmidt          8 283347138.988281            6 
         8192          8192   gramschmidt          9 285321008.195312            0 
# Runtime: 2746.852953 s (overhead: 0.000001 %) 10 records
