# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 19:39:50 2022
# Execution time and date (local): Mon Jan 10 20:39:50 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 290852321.259766            0 
         8192          8192   gramschmidt          1 243760647.093750            5 
         8192          8192   gramschmidt          2 258917537.003906            5 
         8192          8192   gramschmidt          3 275131240.623047            0 
         8192          8192   gramschmidt          4 260920854.861328            5 
         8192          8192   gramschmidt          5 304197198.646484            0 
         8192          8192   gramschmidt          6 277011616.447266            0 
         8192          8192   gramschmidt          7 265357454.767578            0 
         8192          8192   gramschmidt          8 283343226.023438            6 
         8192          8192   gramschmidt          9 285317106.802734            0 
# Runtime: 2744.809269 s (overhead: 0.000001 %) 10 records
