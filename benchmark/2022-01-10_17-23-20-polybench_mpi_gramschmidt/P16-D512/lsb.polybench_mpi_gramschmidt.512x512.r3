# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:27:14 2022
# Execution time and date (local): Mon Jan 10 17:27:14 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 3048626.375000            0 
          512           512   gramschmidt          1 31628.388672            3 
          512           512   gramschmidt          2 29980.970703            0 
          512           512   gramschmidt          3 30035.386719            0 
          512           512   gramschmidt          4 29788.902344            0 
          512           512   gramschmidt          5 29680.544922            0 
          512           512   gramschmidt          6 29860.972656            0 
          512           512   gramschmidt          7 29753.593750            0 
          512           512   gramschmidt          8 29805.041016            0 
          512           512   gramschmidt          9 29669.101562            0 
# Runtime: 3.318865 s (overhead: 0.000090 %) 10 records
