# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:27:14 2022
# Execution time and date (local): Mon Jan 10 17:27:14 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 9047769.402344            0 
          512           512   gramschmidt          1 31671.750000            3 
          512           512   gramschmidt          2 29981.871094            0 
          512           512   gramschmidt          3 30035.310547            0 
          512           512   gramschmidt          4 29785.396484            0 
          512           512   gramschmidt          5 29685.242188            0 
          512           512   gramschmidt          6 29862.404297            0 
          512           512   gramschmidt          7 29751.097656            0 
          512           512   gramschmidt          8 29806.308594            0 
          512           512   gramschmidt          9 29668.916016            0 
# Runtime: 9.318053 s (overhead: 0.000032 %) 10 records
