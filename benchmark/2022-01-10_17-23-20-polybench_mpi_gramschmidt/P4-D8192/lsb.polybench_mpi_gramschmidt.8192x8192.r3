# Sysname : Linux
# Nodename: eu-a6-008-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:23:45 2022
# Execution time and date (local): Mon Jan 10 17:23:45 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 2464416725.146484            0 
         8192          8192   gramschmidt          1 2224606172.414062            4 
         8192          8192   gramschmidt          2 2404856433.716797            3 
         8192          8192   gramschmidt          3 2172496789.695312            0 
         8192          8192   gramschmidt          4 2187712234.507812            3 
         8192          8192   gramschmidt          5 2015754504.390625            0 
         8192          8192   gramschmidt          6 1980894463.433594            2 
         8192          8192   gramschmidt          7 1948644183.791016            0 
         8192          8192   gramschmidt          8 1893354668.886719            3 
         8192          8192   gramschmidt          9 1899487803.845703            0 
# Runtime: 21192.224035 s (overhead: 0.000000 %) 10 records
