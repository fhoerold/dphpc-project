# Sysname : Linux
# Nodename: eu-a6-008-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:23:45 2022
# Execution time and date (local): Mon Jan 10 17:23:45 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 2462862322.439453            1 
         8192          8192   gramschmidt          1 2224139281.570312            6 
         8192          8192   gramschmidt          2 2404352134.626953            5 
         8192          8192   gramschmidt          3 2172041446.984375            0 
         8192          8192   gramschmidt          4 2187253355.777344            5 
         8192          8192   gramschmidt          5 2015332190.562500            0 
         8192          8192   gramschmidt          6 1980478443.742188            0 
         8192          8192   gramschmidt          7 1948236949.566406            0 
         8192          8192   gramschmidt          8 1892958194.976562            5 
         8192          8192   gramschmidt          9 1899089627.476562            0 
# Runtime: 21186.744010 s (overhead: 0.000000 %) 10 records
