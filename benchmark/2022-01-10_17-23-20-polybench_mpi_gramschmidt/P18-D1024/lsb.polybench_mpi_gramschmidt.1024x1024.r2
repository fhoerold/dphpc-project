# Sysname : Linux
# Nodename: eu-a6-003-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 17:45:47 2022
# Execution time and date (local): Mon Jan 10 18:45:47 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 6265212.562500            0 
         1024          1024   gramschmidt          1 263990.451172            5 
         1024          1024   gramschmidt          2 254872.091797            1 
         1024          1024   gramschmidt          3 255145.078125            0 
         1024          1024   gramschmidt          4 254476.068359            1 
         1024          1024   gramschmidt          5 255501.830078            0 
         1024          1024   gramschmidt          6 254341.888672            0 
         1024          1024   gramschmidt          7 253376.550781            0 
         1024          1024   gramschmidt          8 260145.060547            2 
         1024          1024   gramschmidt          9 261698.007812            0 
# Runtime: 8.578806 s (overhead: 0.000105 %) 10 records
