# Sysname : Linux
# Nodename: eu-a6-003-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 17:45:47 2022
# Execution time and date (local): Mon Jan 10 18:45:47 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 264771.976562            0 
         1024          1024   gramschmidt          1 264092.255859            1 
         1024          1024   gramschmidt          2 254782.599609            1 
         1024          1024   gramschmidt          3 255332.615234            0 
         1024          1024   gramschmidt          4 254388.869141            3 
         1024          1024   gramschmidt          5 255585.154297            0 
         1024          1024   gramschmidt          6 254171.945312            0 
         1024          1024   gramschmidt          7 253542.630859            0 
         1024          1024   gramschmidt          8 260132.587891            1 
         1024          1024   gramschmidt          9 261545.185547            0 
# Runtime: 2.578396 s (overhead: 0.000233 %) 10 records
