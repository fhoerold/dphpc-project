# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 19:32:43 2022
# Execution time and date (local): Mon Jan 10 20:32:43 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 8014600.656250            1 
          512           512   gramschmidt          1 23717.630859            3 
          512           512   gramschmidt          2 22307.941406            0 
          512           512   gramschmidt          3 21887.310547            0 
          512           512   gramschmidt          4 23087.701172            0 
          512           512   gramschmidt          5 26801.996094            0 
          512           512   gramschmidt          6 21743.398438            0 
          512           512   gramschmidt          7 21501.613281            0 
          512           512   gramschmidt          8 21399.716797            2 
          512           512   gramschmidt          9 21428.835938            0 
# Runtime: 8.218518 s (overhead: 0.000073 %) 10 records
