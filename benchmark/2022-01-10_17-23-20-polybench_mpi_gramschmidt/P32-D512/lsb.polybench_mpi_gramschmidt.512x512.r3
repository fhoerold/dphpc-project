# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 19:32:43 2022
# Execution time and date (local): Mon Jan 10 20:32:43 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 8036830.626953            0 
          512           512   gramschmidt          1 23713.867188            3 
          512           512   gramschmidt          2 22342.888672            0 
          512           512   gramschmidt          3 21905.671875            0 
          512           512   gramschmidt          4 23113.636719            0 
          512           512   gramschmidt          5 26824.126953            0 
          512           512   gramschmidt          6 21760.718750            0 
          512           512   gramschmidt          7 21528.333984            0 
          512           512   gramschmidt          8 21420.738281            2 
          512           512   gramschmidt          9 21456.996094            0 
# Runtime: 8.240937 s (overhead: 0.000061 %) 10 records
