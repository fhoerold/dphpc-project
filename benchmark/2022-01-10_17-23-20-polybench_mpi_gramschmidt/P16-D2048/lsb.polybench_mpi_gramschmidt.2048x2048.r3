# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:27:50 2022
# Execution time and date (local): Mon Jan 10 17:27:50 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 31657886.742188            0 
         2048          2048   gramschmidt          1 4957571.728516            4 
         2048          2048   gramschmidt          2 4778039.828125            4 
         2048          2048   gramschmidt          3 4503771.404297            0 
         2048          2048   gramschmidt          4 4618603.675781            1 
         2048          2048   gramschmidt          5 4588118.324219            0 
         2048          2048   gramschmidt          6 4466121.853516            0 
         2048          2048   gramschmidt          7 4427979.734375            0 
         2048          2048   gramschmidt          8 4434303.667969            1 
         2048          2048   gramschmidt          9 4407650.029297            0 
# Runtime: 72.840084 s (overhead: 0.000014 %) 10 records
