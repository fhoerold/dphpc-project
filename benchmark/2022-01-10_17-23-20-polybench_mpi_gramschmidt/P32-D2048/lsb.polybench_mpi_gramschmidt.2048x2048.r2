# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 19:33:50 2022
# Execution time and date (local): Mon Jan 10 20:33:50 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 1689444.460938            0 
         2048          2048   gramschmidt          1 1463473.048828            5 
         2048          2048   gramschmidt          2 1390243.464844            3 
         2048          2048   gramschmidt          3 1344718.158203            0 
         2048          2048   gramschmidt          4 1385335.394531            3 
         2048          2048   gramschmidt          5 1332511.660156            0 
         2048          2048   gramschmidt          6 1355032.078125            0 
         2048          2048   gramschmidt          7 1409169.777344            0 
         2048          2048   gramschmidt          8 1361362.548828            4 
         2048          2048   gramschmidt          9 1364096.419922            0 
# Runtime: 14.095447 s (overhead: 0.000106 %) 10 records
