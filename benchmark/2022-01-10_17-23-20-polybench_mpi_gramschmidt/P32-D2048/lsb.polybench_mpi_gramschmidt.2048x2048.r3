# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 19:33:50 2022
# Execution time and date (local): Mon Jan 10 20:33:50 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 6715468.476562            0 
         2048          2048   gramschmidt          1 1463281.222656            3 
         2048          2048   gramschmidt          2 1390093.060547            1 
         2048          2048   gramschmidt          3 1344721.613281            0 
         2048          2048   gramschmidt          4 1385085.750000            1 
         2048          2048   gramschmidt          5 1332436.009766            0 
         2048          2048   gramschmidt          6 1354908.978516            0 
         2048          2048   gramschmidt          7 1409021.628906            0 
         2048          2048   gramschmidt          8 1361220.376953            5 
         2048          2048   gramschmidt          9 1364090.107422            0 
# Runtime: 19.120377 s (overhead: 0.000052 %) 10 records
