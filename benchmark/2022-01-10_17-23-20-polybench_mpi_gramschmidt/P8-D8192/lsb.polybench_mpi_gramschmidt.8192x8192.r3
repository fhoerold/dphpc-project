# Sysname : Linux
# Nodename: eu-a6-011-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:24:14 2022
# Execution time and date (local): Mon Jan 10 17:24:14 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 1851769197.191406            0 
         8192          8192   gramschmidt          1 1367673555.453125            5 
         8192          8192   gramschmidt          2 1383072301.576172            6 
         8192          8192   gramschmidt          3 1303829562.152344            0 
         8192          8192   gramschmidt          4 1363527849.318359            5 
         8192          8192   gramschmidt          5 1265567985.048828            0 
         8192          8192   gramschmidt          6 1301939827.259766            0 
         8192          8192   gramschmidt          7 1322240572.777344            0 
         8192          8192   gramschmidt          8 1113320922.294922            5 
         8192          8192   gramschmidt          9 1376360945.855469            0 
# Runtime: 13649.302770 s (overhead: 0.000000 %) 10 records
