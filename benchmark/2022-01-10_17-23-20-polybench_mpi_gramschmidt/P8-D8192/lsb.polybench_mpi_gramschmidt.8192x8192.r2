# Sysname : Linux
# Nodename: eu-a6-011-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:24:14 2022
# Execution time and date (local): Mon Jan 10 17:24:14 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 1848612824.685547            0 
         8192          8192   gramschmidt          1 1365363497.160156            5 
         8192          8192   gramschmidt          2 1380737491.970703            5 
         8192          8192   gramschmidt          3 1301627353.611328            0 
         8192          8192   gramschmidt          4 1361225897.130859            5 
         8192          8192   gramschmidt          5 1263430690.583984            0 
         8192          8192   gramschmidt          6 1299740820.996094            0 
         8192          8192   gramschmidt          7 1320007611.324219            0 
         8192          8192   gramschmidt          8 1111440921.964844            5 
         8192          8192   gramschmidt          9 1374036759.177734            0 
# Runtime: 13626.223921 s (overhead: 0.000000 %) 10 records
