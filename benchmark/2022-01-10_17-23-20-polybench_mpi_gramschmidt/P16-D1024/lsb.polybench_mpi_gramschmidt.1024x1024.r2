# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:27:30 2022
# Execution time and date (local): Mon Jan 10 17:27:30 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 10232494.048828            0 
         1024          1024   gramschmidt          1 233848.355469            3 
         1024          1024   gramschmidt          2 225084.341797            1 
         1024          1024   gramschmidt          3 224969.324219            0 
         1024          1024   gramschmidt          4 225993.166016            1 
         1024          1024   gramschmidt          5 228410.277344            0 
         1024          1024   gramschmidt          6 224013.064453            0 
         1024          1024   gramschmidt          7 225795.552734            0 
         1024          1024   gramschmidt          8 223240.613281            1 
         1024          1024   gramschmidt          9 225927.292969            0 
# Runtime: 12.269814 s (overhead: 0.000049 %) 10 records
