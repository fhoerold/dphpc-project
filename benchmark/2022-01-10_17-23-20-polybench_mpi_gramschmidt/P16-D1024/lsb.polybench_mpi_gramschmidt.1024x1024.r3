# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:27:30 2022
# Execution time and date (local): Mon Jan 10 17:27:30 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 6223524.083984            0 
         1024          1024   gramschmidt          1 233852.814453            4 
         1024          1024   gramschmidt          2 225095.884766            1 
         1024          1024   gramschmidt          3 224995.369141            0 
         1024          1024   gramschmidt          4 225983.072266            1 
         1024          1024   gramschmidt          5 228474.900391            0 
         1024          1024   gramschmidt          6 223967.964844            0 
         1024          1024   gramschmidt          7 225806.001953            0 
         1024          1024   gramschmidt          8 223257.626953            1 
         1024          1024   gramschmidt          9 225941.886719            0 
# Runtime: 8.260939 s (overhead: 0.000085 %) 10 records
