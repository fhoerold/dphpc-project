# Sysname : Linux
# Nodename: eu-a6-008-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:24:14 2022
# Execution time and date (local): Mon Jan 10 17:24:14 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 25784907.617188            0 
         2048          2048   gramschmidt          1 8470854.140625            3 
         2048          2048   gramschmidt          2 8327919.597656            2 
         2048          2048   gramschmidt          3 8379680.253906            0 
         2048          2048   gramschmidt          4 8171681.281250            1 
         2048          2048   gramschmidt          5 8483135.912109            0 
         2048          2048   gramschmidt          6 8472143.324219            0 
         2048          2048   gramschmidt          7 8259664.259766            0 
         2048          2048   gramschmidt          8 8237325.458984            4 
         2048          2048   gramschmidt          9 8388030.091797            0 
# Runtime: 100.975386 s (overhead: 0.000010 %) 10 records
