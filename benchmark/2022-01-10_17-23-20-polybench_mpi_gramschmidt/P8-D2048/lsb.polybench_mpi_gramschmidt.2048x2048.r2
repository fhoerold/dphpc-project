# Sysname : Linux
# Nodename: eu-a6-008-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:24:14 2022
# Execution time and date (local): Mon Jan 10 17:24:14 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 25779755.822266            0 
         2048          2048   gramschmidt          1 8470191.816406            4 
         2048          2048   gramschmidt          2 8326531.980469            3 
         2048          2048   gramschmidt          3 8378984.945312            0 
         2048          2048   gramschmidt          4 8170690.496094            2 
         2048          2048   gramschmidt          5 8482136.255859            0 
         2048          2048   gramschmidt          6 8470655.046875            0 
         2048          2048   gramschmidt          7 8259108.345703            0 
         2048          2048   gramschmidt          8 8236310.615234            4 
         2048          2048   gramschmidt          9 8387001.150391            0 
# Runtime: 100.961409 s (overhead: 0.000013 %) 10 records
