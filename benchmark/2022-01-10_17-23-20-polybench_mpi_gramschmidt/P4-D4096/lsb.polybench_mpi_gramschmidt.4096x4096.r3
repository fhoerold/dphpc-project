# Sysname : Linux
# Nodename: eu-a6-002-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:23:44 2022
# Execution time and date (local): Mon Jan 10 17:23:44 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 243330910.507812            1 
         4096          4096   gramschmidt          1 235408938.900391            4 
         4096          4096   gramschmidt          2 214729860.205078            5 
         4096          4096   gramschmidt          3 233451008.230469            0 
         4096          4096   gramschmidt          4 212604756.013672            4 
         4096          4096   gramschmidt          5 212091908.910156            0 
         4096          4096   gramschmidt          6 240029102.951172            0 
         4096          4096   gramschmidt          7 210798496.656250            0 
         4096          4096   gramschmidt          8 210747181.248047            4 
         4096          4096   gramschmidt          9 217579269.277344            0 
# Runtime: 2230.771482 s (overhead: 0.000001 %) 10 records
