# Sysname : Linux
# Nodename: eu-a6-002-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:23:44 2022
# Execution time and date (local): Mon Jan 10 17:23:44 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 244652620.722656            1 
         4096          4096   gramschmidt          1 235719992.669922            4 
         4096          4096   gramschmidt          2 215014315.318359            4 
         4096          4096   gramschmidt          3 233759713.111328            0 
         4096          4096   gramschmidt          4 212885429.427734            4 
         4096          4096   gramschmidt          5 212372572.316406            0 
         4096          4096   gramschmidt          6 240346723.292969            0 
         4096          4096   gramschmidt          7 211076926.138672            0 
         4096          4096   gramschmidt          8 211026099.439453            4 
         4096          4096   gramschmidt          9 217866673.050781            0 
# Runtime: 2234.721115 s (overhead: 0.000001 %) 10 records
