# Sysname : Linux
# Nodename: eu-a6-011-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:23:44 2022
# Execution time and date (local): Mon Jan 10 17:23:44 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 4552042.007812            0 
         1024          1024   gramschmidt          1 605387.677734           12 
         1024          1024   gramschmidt          2 569128.222656            1 
         1024          1024   gramschmidt          3 473729.886719            0 
         1024          1024   gramschmidt          4 517874.542969            4 
         1024          1024   gramschmidt          5 570783.906250            0 
         1024          1024   gramschmidt          6 586328.742188            0 
         1024          1024   gramschmidt          7 663943.095703            0 
         1024          1024   gramschmidt          8 558030.640625            1 
         1024          1024   gramschmidt          9 603877.804688            0 
# Runtime: 9.701176 s (overhead: 0.000186 %) 10 records
