# Sysname : Linux
# Nodename: eu-a6-003-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 17:46:06 2022
# Execution time and date (local): Mon Jan 10 18:46:06 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 9150011.875000            0 
         2048          2048   gramschmidt          1 2271697.046875            4 
         2048          2048   gramschmidt          2 1989647.978516            1 
         2048          2048   gramschmidt          3 1920958.929688            0 
         2048          2048   gramschmidt          4 1909672.769531            4 
         2048          2048   gramschmidt          5 1924203.900391            0 
         2048          2048   gramschmidt          6 1952357.718750            0 
         2048          2048   gramschmidt          7 1941525.593750            0 
         2048          2048   gramschmidt          8 1915647.556641            5 
         2048          2048   gramschmidt          9 1911226.166016            0 
# Runtime: 26.887000 s (overhead: 0.000052 %) 10 records
