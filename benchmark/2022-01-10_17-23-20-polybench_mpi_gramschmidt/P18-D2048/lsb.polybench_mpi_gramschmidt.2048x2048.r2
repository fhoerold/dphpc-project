# Sysname : Linux
# Nodename: eu-a6-003-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 17:46:06 2022
# Execution time and date (local): Mon Jan 10 18:46:06 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 5148576.498047            0 
         2048          2048   gramschmidt          1 2271882.667969            4 
         2048          2048   gramschmidt          2 1989876.392578            3 
         2048          2048   gramschmidt          3 1921040.882812            0 
         2048          2048   gramschmidt          4 1909639.638672            1 
         2048          2048   gramschmidt          5 1924065.898438            0 
         2048          2048   gramschmidt          6 1952583.056641            0 
         2048          2048   gramschmidt          7 1941571.457031            0 
         2048          2048   gramschmidt          8 1915484.130859            5 
         2048          2048   gramschmidt          9 1911468.855469            0 
# Runtime: 22.886234 s (overhead: 0.000057 %) 10 records
