# Sysname : Linux
# Nodename: eu-a6-002-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:23:44 2022
# Execution time and date (local): Mon Jan 10 17:23:44 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 17866208.326172            0 
         2048          2048   gramschmidt          1 16799654.148438            4 
         2048          2048   gramschmidt          2 16598201.343750            4 
         2048          2048   gramschmidt          3 16652574.345703            0 
         2048          2048   gramschmidt          4 16533682.873047            6 
         2048          2048   gramschmidt          5 16243017.363281            0 
         2048          2048   gramschmidt          6 16206168.841797            0 
         2048          2048   gramschmidt          7 16327762.628906            0 
         2048          2048   gramschmidt          8 16503660.873047            6 
         2048          2048   gramschmidt          9 15951490.841797            0 
# Runtime: 165.682475 s (overhead: 0.000012 %) 10 records
