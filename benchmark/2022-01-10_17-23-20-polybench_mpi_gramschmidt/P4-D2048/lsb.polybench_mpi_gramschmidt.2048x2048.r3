# Sysname : Linux
# Nodename: eu-a6-002-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:23:44 2022
# Execution time and date (local): Mon Jan 10 17:23:44 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 22862462.662109            0 
         2048          2048   gramschmidt          1 16798542.132812            4 
         2048          2048   gramschmidt          2 16595571.382812            3 
         2048          2048   gramschmidt          3 16651638.441406            0 
         2048          2048   gramschmidt          4 16531037.183594            4 
         2048          2048   gramschmidt          5 16241375.833984            0 
         2048          2048   gramschmidt          6 16205092.982422            0 
         2048          2048   gramschmidt          7 16325230.523438            0 
         2048          2048   gramschmidt          8 16501850.679688            6 
         2048          2048   gramschmidt          9 15949737.070312            0 
# Runtime: 170.662586 s (overhead: 0.000010 %) 10 records
