# Sysname : Linux
# Nodename: eu-a6-002-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:23:44 2022
# Execution time and date (local): Mon Jan 10 17:23:44 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 1113496.830078            0 
          512           512   gramschmidt          1 108994.394531            5 
          512           512   gramschmidt          2 106430.896484            2 
          512           512   gramschmidt          3 107309.130859            0 
          512           512   gramschmidt          4 107225.746094            1 
          512           512   gramschmidt          5 106884.156250            0 
          512           512   gramschmidt          6 105625.451172            0 
          512           512   gramschmidt          7 107443.681641            0 
          512           512   gramschmidt          8 109440.363281            2 
          512           512   gramschmidt          9 107237.900391            0 
# Runtime: 2.080136 s (overhead: 0.000481 %) 10 records
