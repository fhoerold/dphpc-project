# Sysname : Linux
# Nodename: eu-a6-002-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:23:44 2022
# Execution time and date (local): Mon Jan 10 17:23:44 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 2102316.607422            0 
          512           512   gramschmidt          1 108942.060547            4 
          512           512   gramschmidt          2 106385.011719            1 
          512           512   gramschmidt          3 107248.138672            0 
          512           512   gramschmidt          4 107169.183594            1 
          512           512   gramschmidt          5 106828.962891            0 
          512           512   gramschmidt          6 105575.496094            0 
          512           512   gramschmidt          7 107394.296875            0 
          512           512   gramschmidt          8 109414.707031            1 
          512           512   gramschmidt          9 107162.076172            0 
# Runtime: 3.068481 s (overhead: 0.000228 %) 10 records
