# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 17:45:18 2022
# Execution time and date (local): Mon Jan 10 18:45:18 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 11029729.589844            0 
          512           512   gramschmidt          1 37923.619141            3 
          512           512   gramschmidt          2 36029.197266            0 
          512           512   gramschmidt          3 35738.529297            0 
          512           512   gramschmidt          4 35457.130859            0 
          512           512   gramschmidt          5 35798.175781            0 
          512           512   gramschmidt          6 35549.623047            0 
          512           512   gramschmidt          7 35744.267578            0 
          512           512   gramschmidt          8 35512.220703            0 
          512           512   gramschmidt          9 36356.880859            0 
# Runtime: 11.353860 s (overhead: 0.000026 %) 10 records
