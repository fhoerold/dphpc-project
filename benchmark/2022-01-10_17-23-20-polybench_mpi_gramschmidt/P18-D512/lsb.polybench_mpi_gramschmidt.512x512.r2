# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 17:45:18 2022
# Execution time and date (local): Mon Jan 10 18:45:18 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 10030503.185547            1 
          512           512   gramschmidt          1 37956.445312            3 
          512           512   gramschmidt          2 36000.978516            0 
          512           512   gramschmidt          3 35764.304688            0 
          512           512   gramschmidt          4 35438.082031            0 
          512           512   gramschmidt          5 35796.330078            0 
          512           512   gramschmidt          6 35544.050781            0 
          512           512   gramschmidt          7 35740.166016            0 
          512           512   gramschmidt          8 35501.072266            0 
          512           512   gramschmidt          9 36347.986328            0 
# Runtime: 10.354623 s (overhead: 0.000039 %) 10 records
