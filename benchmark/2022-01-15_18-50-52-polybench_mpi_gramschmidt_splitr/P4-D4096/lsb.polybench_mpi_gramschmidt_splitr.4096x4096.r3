# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan 15 17:51:02 2022
# Execution time and date (local): Sat Jan 15 18:51:02 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 270020895.555176            0 
         4096          4096   gramschmidt          1 273274210.621094            3 
         4096          4096   gramschmidt          2 257933338.473633            4 
         4096          4096   gramschmidt          3 291359014.406250            0 
         4096          4096   gramschmidt          4 301628487.151855            4 
         4096          4096   gramschmidt          5 213590782.458984            0 
         4096          4096   gramschmidt          6 218752594.781738            0 
         4096          4096   gramschmidt          7 217396676.208984            0 
         4096          4096   gramschmidt          8 215375308.249512            4 
         4096          4096   gramschmidt          9 224638321.179688            0 
# Runtime: 2483.969672 s (overhead: 0.000001 %) 10 records
