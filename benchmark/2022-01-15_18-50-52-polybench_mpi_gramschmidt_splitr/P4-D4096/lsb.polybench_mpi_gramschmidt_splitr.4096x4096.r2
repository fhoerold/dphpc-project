# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan 15 17:51:02 2022
# Execution time and date (local): Sat Jan 15 18:51:02 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 270008971.914551            0 
         4096          4096   gramschmidt          1 273271056.157715            4 
         4096          4096   gramschmidt          2 257930679.786621            4 
         4096          4096   gramschmidt          3 291355659.332031            0 
         4096          4096   gramschmidt          4 301625175.423340            3 
         4096          4096   gramschmidt          5 213588470.015137            0 
         4096          4096   gramschmidt          6 218750257.696777            0 
         4096          4096   gramschmidt          7 217393917.145508            0 
         4096          4096   gramschmidt          8 215373045.331055            4 
         4096          4096   gramschmidt          9 224635992.744629            0 
# Runtime: 2483.933272 s (overhead: 0.000001 %) 10 records
