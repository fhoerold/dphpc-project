# Sysname : Linux
# Nodename: eu-a6-010-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:52:19 2022
# Execution time and date (local): Wed Jan 12 02:52:19 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 20326136.177734            0 
            3          4096          4096   gramschmidt           MPI          1 20241643.958984            4 
            3          4096          4096   gramschmidt           MPI          2 20325974.164062            2 
            3          4096          4096   gramschmidt           MPI          3 20205898.267578            0 
            3          4096          4096   gramschmidt           MPI          4 20179656.994141            5 
            3          4096          4096   gramschmidt           MPI          5 19960222.824219            0 
            3          4096          4096   gramschmidt           MPI          6 20239841.359375            1 
            3          4096          4096   gramschmidt           MPI          7 20356307.205078            0 
            3          4096          4096   gramschmidt           MPI          8 19992687.015625            6 
            3          4096          4096   gramschmidt           MPI          9 20272110.156250            0 
# Runtime: 267.498112 s (overhead: 0.000007 %) 10 records
