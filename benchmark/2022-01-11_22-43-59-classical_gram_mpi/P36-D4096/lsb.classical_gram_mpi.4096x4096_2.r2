# Sysname : Linux
# Nodename: eu-a6-010-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:52:19 2022
# Execution time and date (local): Wed Jan 12 02:52:19 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 20324941.986328            0 
            2          4096          4096   gramschmidt           MPI          1 20240426.056641            6 
            2          4096          4096   gramschmidt           MPI          2 20324348.220703            2 
            2          4096          4096   gramschmidt           MPI          3 20204608.812500            2 
            2          4096          4096   gramschmidt           MPI          4 20178469.306641            7 
            2          4096          4096   gramschmidt           MPI          5 19958569.673828            1 
            2          4096          4096   gramschmidt           MPI          6 20238635.580078            2 
            2          4096          4096   gramschmidt           MPI          7 20355079.109375            0 
            2          4096          4096   gramschmidt           MPI          8 19991138.078125            7 
            2          4096          4096   gramschmidt           MPI          9 20270864.732422            0 
# Runtime: 262.503378 s (overhead: 0.000010 %) 10 records
