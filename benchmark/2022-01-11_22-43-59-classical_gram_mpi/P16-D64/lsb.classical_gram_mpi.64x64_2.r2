# Sysname : Linux
# Nodename: eu-a6-009-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:50:52 2022
# Execution time and date (local): Tue Jan 11 22:50:52 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 940.359375            0 
            2            64            64   gramschmidt           MPI          1 885.193359            2 
            2            64            64   gramschmidt           MPI          2 797.914062            0 
            2            64            64   gramschmidt           MPI          3 836.318359            0 
            2            64            64   gramschmidt           MPI          4 812.230469            0 
            2            64            64   gramschmidt           MPI          5 792.302734            0 
            2            64            64   gramschmidt           MPI          6 875.345703            0 
            2            64            64   gramschmidt           MPI          7 776.755859            0 
            2            64            64   gramschmidt           MPI          8 782.552734            0 
            2            64            64   gramschmidt           MPI          9 762.726562            0 
# Runtime: 2.007325 s (overhead: 0.000100 %) 10 records
