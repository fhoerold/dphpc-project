# Sysname : Linux
# Nodename: eu-a6-009-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:50:52 2022
# Execution time and date (local): Tue Jan 11 22:50:52 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 941.914062            0 
            3            64            64   gramschmidt           MPI          1 877.152344            3 
            3            64            64   gramschmidt           MPI          2 795.851562            0 
            3            64            64   gramschmidt           MPI          3 838.220703            0 
            3            64            64   gramschmidt           MPI          4 812.572266            0 
            3            64            64   gramschmidt           MPI          5 792.882812            0 
            3            64            64   gramschmidt           MPI          6 874.603516            0 
            3            64            64   gramschmidt           MPI          7 777.525391            0 
            3            64            64   gramschmidt           MPI          8 781.259766            0 
            3            64            64   gramschmidt           MPI          9 772.574219            0 
# Runtime: 2.004785 s (overhead: 0.000150 %) 10 records
