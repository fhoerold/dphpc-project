# Sysname : Linux
# Nodename: eu-a6-012-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 23:34:42 2022
# Execution time and date (local): Wed Jan 12 00:34:42 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 569.886719            0 
            3            32            32   gramschmidt           MPI          1 565.640625            3 
            3            32            32   gramschmidt           MPI          2 509.167969            0 
            3            32            32   gramschmidt           MPI          3 455.830078            0 
            3            32            32   gramschmidt           MPI          4 428.464844            0 
            3            32            32   gramschmidt           MPI          5 506.210938            0 
            3            32            32   gramschmidt           MPI          6 421.923828            0 
            3            32            32   gramschmidt           MPI          7 429.181641            0 
            3            32            32   gramschmidt           MPI          8 428.019531            0 
            3            32            32   gramschmidt           MPI          9 424.656250            0 
# Runtime: 26.023983 s (overhead: 0.000012 %) 10 records
