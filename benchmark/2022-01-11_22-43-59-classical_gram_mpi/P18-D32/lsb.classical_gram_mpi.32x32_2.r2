# Sysname : Linux
# Nodename: eu-a6-012-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 23:34:42 2022
# Execution time and date (local): Wed Jan 12 00:34:42 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 579.792969            0 
            2            32            32   gramschmidt           MPI          1 577.015625            2 
            2            32            32   gramschmidt           MPI          2 523.513672            0 
            2            32            32   gramschmidt           MPI          3 465.013672            0 
            2            32            32   gramschmidt           MPI          4 434.849609            0 
            2            32            32   gramschmidt           MPI          5 518.697266            0 
            2            32            32   gramschmidt           MPI          6 434.966797            0 
            2            32            32   gramschmidt           MPI          7 436.222656            0 
            2            32            32   gramschmidt           MPI          8 427.269531            0 
            2            32            32   gramschmidt           MPI          9 436.042969            0 
# Runtime: 26.004907 s (overhead: 0.000008 %) 10 records
