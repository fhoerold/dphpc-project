# Sysname : Linux
# Nodename: eu-a6-012-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:46:04 2022
# Execution time and date (local): Tue Jan 11 22:46:04 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          8192          8192   gramschmidt           MPI          0 836712323.277344            0 
            3          8192          8192   gramschmidt           MPI          1 805183479.484375            8 
            3          8192          8192   gramschmidt           MPI          2 837093840.921875            5 
            3          8192          8192   gramschmidt           MPI          3 538608660.845703            0 
            3          8192          8192   gramschmidt           MPI          4 817812312.478516            5 
            3          8192          8192   gramschmidt           MPI          5 873385152.613281            3 
            3          8192          8192   gramschmidt           MPI          6 875416760.066406            5 
            3          8192          8192   gramschmidt           MPI          7 839934700.861328            2 
            3          8192          8192   gramschmidt           MPI          8 831502622.082031           18 
            3          8192          8192   gramschmidt           MPI          9 837963983.365234            0 
# Runtime: 10691.242494 s (overhead: 0.000000 %) 10 records
