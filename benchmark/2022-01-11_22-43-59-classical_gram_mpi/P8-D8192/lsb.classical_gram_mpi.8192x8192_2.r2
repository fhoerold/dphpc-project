# Sysname : Linux
# Nodename: eu-a6-012-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:46:04 2022
# Execution time and date (local): Tue Jan 11 22:46:04 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          8192          8192   gramschmidt           MPI          0 836854460.310547            0 
            2          8192          8192   gramschmidt           MPI          1 805319353.236328            5 
            2          8192          8192   gramschmidt           MPI          2 837235838.294922            7 
            2          8192          8192   gramschmidt           MPI          3 538692898.910156            2 
            2          8192          8192   gramschmidt           MPI          4 817950959.703125            5 
            2          8192          8192   gramschmidt           MPI          5 873534159.955078            0 
            2          8192          8192   gramschmidt           MPI          6 875565069.058594            1 
            2          8192          8192   gramschmidt           MPI          7 840077897.179688            0 
            2          8192          8192   gramschmidt           MPI          8 831643156.894531           11 
            2          8192          8192   gramschmidt           MPI          9 838106740.806641            2 
# Runtime: 10693.248442 s (overhead: 0.000000 %) 10 records
