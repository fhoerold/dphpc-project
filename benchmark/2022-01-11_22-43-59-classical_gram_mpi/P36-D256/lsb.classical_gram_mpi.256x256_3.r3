# Sysname : Linux
# Nodename: eu-a6-010-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:50:46 2022
# Execution time and date (local): Wed Jan 12 02:50:46 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 20569.716797            0 
            3           256           256   gramschmidt           MPI          1 20330.195312            3 
            3           256           256   gramschmidt           MPI          2 20123.414062            0 
            3           256           256   gramschmidt           MPI          3 20719.244141            0 
            3           256           256   gramschmidt           MPI          4 20400.537109            0 
            3           256           256   gramschmidt           MPI          5 20261.632812            0 
            3           256           256   gramschmidt           MPI          6 20227.232422            0 
            3           256           256   gramschmidt           MPI          7 20241.234375            0 
            3           256           256   gramschmidt           MPI          8 20292.365234            0 
            3           256           256   gramschmidt           MPI          9 20343.265625            0 
# Runtime: 7.309939 s (overhead: 0.000041 %) 10 records
