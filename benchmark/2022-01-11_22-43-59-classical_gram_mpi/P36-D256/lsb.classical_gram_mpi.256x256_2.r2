# Sysname : Linux
# Nodename: eu-a6-010-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:50:46 2022
# Execution time and date (local): Wed Jan 12 02:50:46 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 20334.507812            0 
            2           256           256   gramschmidt           MPI          1 20155.375000            3 
            2           256           256   gramschmidt           MPI          2 20117.634766            0 
            2           256           256   gramschmidt           MPI          3 20692.248047            0 
            2           256           256   gramschmidt           MPI          4 20397.162109            0 
            2           256           256   gramschmidt           MPI          5 20257.000000            0 
            2           256           256   gramschmidt           MPI          6 20227.083984            0 
            2           256           256   gramschmidt           MPI          7 20234.978516            0 
            2           256           256   gramschmidt           MPI          8 20287.917969            0 
            2           256           256   gramschmidt           MPI          9 20337.482422            0 
# Runtime: 6.296993 s (overhead: 0.000048 %) 10 records
