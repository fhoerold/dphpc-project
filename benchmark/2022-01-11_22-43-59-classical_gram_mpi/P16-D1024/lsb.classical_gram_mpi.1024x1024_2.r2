# Sysname : Linux
# Nodename: eu-a6-004-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:52:03 2022
# Execution time and date (local): Tue Jan 11 22:52:03 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 378860.335938            0 
            2          1024          1024   gramschmidt           MPI          1 327387.326172            5 
            2          1024          1024   gramschmidt           MPI          2 322685.494141            2 
            2          1024          1024   gramschmidt           MPI          3 323904.992188            0 
            2          1024          1024   gramschmidt           MPI          4 323328.423828            2 
            2          1024          1024   gramschmidt           MPI          5 326113.013672            0 
            2          1024          1024   gramschmidt           MPI          6 340887.484375            0 
            2          1024          1024   gramschmidt           MPI          7 322777.292969            0 
            2          1024          1024   gramschmidt           MPI          8 322970.462891            2 
            2          1024          1024   gramschmidt           MPI          9 347106.117188            0 
# Runtime: 11.347263 s (overhead: 0.000097 %) 10 records
