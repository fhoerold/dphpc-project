# Sysname : Linux
# Nodename: eu-a6-004-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:52:03 2022
# Execution time and date (local): Tue Jan 11 22:52:03 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 379106.166016            0 
            3          1024          1024   gramschmidt           MPI          1 327585.001953            5 
            3          1024          1024   gramschmidt           MPI          2 322898.859375            2 
            3          1024          1024   gramschmidt           MPI          3 324143.216797            0 
            3          1024          1024   gramschmidt           MPI          4 323526.630859            2 
            3          1024          1024   gramschmidt           MPI          5 326379.718750            0 
            3          1024          1024   gramschmidt           MPI          6 341130.751953            0 
            3          1024          1024   gramschmidt           MPI          7 323002.691406            0 
            3          1024          1024   gramschmidt           MPI          8 323159.167969            2 
            3          1024          1024   gramschmidt           MPI          9 347311.871094            0 
# Runtime: 12.345749 s (overhead: 0.000089 %) 10 records
