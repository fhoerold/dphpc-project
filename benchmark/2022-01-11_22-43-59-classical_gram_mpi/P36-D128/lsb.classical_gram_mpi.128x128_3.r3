# Sysname : Linux
# Nodename: eu-a6-010-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:50:21 2022
# Execution time and date (local): Wed Jan 12 02:50:21 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 4203.279297            0 
            3           128           128   gramschmidt           MPI          1 3764.279297            3 
            3           128           128   gramschmidt           MPI          2 3741.478516            0 
            3           128           128   gramschmidt           MPI          3 3835.248047            0 
            3           128           128   gramschmidt           MPI          4 3702.552734            0 
            3           128           128   gramschmidt           MPI          5 3704.457031            0 
            3           128           128   gramschmidt           MPI          6 3746.173828            0 
            3           128           128   gramschmidt           MPI          7 3636.730469            0 
            3           128           128   gramschmidt           MPI          8 4042.441406            0 
            3           128           128   gramschmidt           MPI          9 3665.017578            0 
# Runtime: 6.047650 s (overhead: 0.000050 %) 10 records
