# Sysname : Linux
# Nodename: eu-a6-010-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:50:21 2022
# Execution time and date (local): Wed Jan 12 02:50:21 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 4205.830078            0 
            2           128           128   gramschmidt           MPI          1 3764.314453            2 
            2           128           128   gramschmidt           MPI          2 3742.259766            0 
            2           128           128   gramschmidt           MPI          3 3835.869141            0 
            2           128           128   gramschmidt           MPI          4 3703.226562            0 
            2           128           128   gramschmidt           MPI          5 3703.185547            0 
            2           128           128   gramschmidt           MPI          6 3748.443359            0 
            2           128           128   gramschmidt           MPI          7 3639.119141            0 
            2           128           128   gramschmidt           MPI          8 4042.460938            0 
            2           128           128   gramschmidt           MPI          9 3665.664062            0 
# Runtime: 15.043038 s (overhead: 0.000013 %) 10 records
