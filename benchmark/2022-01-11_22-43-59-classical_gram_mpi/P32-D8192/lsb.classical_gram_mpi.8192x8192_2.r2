# Sysname : Linux
# Nodename: eu-a6-007-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:15:56 2022
# Execution time and date (local): Wed Jan 12 02:15:56 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          8192          8192   gramschmidt           MPI          0 173342066.781250            2 
            2          8192          8192   gramschmidt           MPI          1 150852113.994141            4 
            2          8192          8192   gramschmidt           MPI          2 154689281.048828            4 
            2          8192          8192   gramschmidt           MPI          3 150333637.164062            0 
            2          8192          8192   gramschmidt           MPI          4 150366727.089844            5 
            2          8192          8192   gramschmidt           MPI          5 150441940.693359            3 
            2          8192          8192   gramschmidt           MPI          6 150775042.132812            3 
            2          8192          8192   gramschmidt           MPI          7 151066076.246094            3 
            2          8192          8192   gramschmidt           MPI          8 151194911.890625            6 
            2          8192          8192   gramschmidt           MPI          9 151336294.242188            0 
# Runtime: 2009.810285 s (overhead: 0.000001 %) 10 records
