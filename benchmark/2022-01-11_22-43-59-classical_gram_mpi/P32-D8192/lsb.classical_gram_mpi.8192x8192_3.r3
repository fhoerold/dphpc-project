# Sysname : Linux
# Nodename: eu-a6-007-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:15:56 2022
# Execution time and date (local): Wed Jan 12 02:15:56 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          8192          8192   gramschmidt           MPI          0 173361023.701172            2 
            3          8192          8192   gramschmidt           MPI          1 150869261.189453           11 
            3          8192          8192   gramschmidt           MPI          2 154706949.839844            4 
            3          8192          8192   gramschmidt           MPI          3 150350714.023438            0 
            3          8192          8192   gramschmidt           MPI          4 150383964.943359            4 
            3          8192          8192   gramschmidt           MPI          5 150459048.093750            2 
            3          8192          8192   gramschmidt           MPI          6 150792258.601562            3 
            3          8192          8192   gramschmidt           MPI          7 151083135.283203            4 
            3          8192          8192   gramschmidt           MPI          8 151212176.886719            7 
            3          8192          8192   gramschmidt           MPI          9 151353746.115234            0 
# Runtime: 2011.903376 s (overhead: 0.000002 %) 10 records
