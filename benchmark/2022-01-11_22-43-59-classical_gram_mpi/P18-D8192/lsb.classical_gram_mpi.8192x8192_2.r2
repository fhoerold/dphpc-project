# Sysname : Linux
# Nodename: eu-a6-002-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 23:45:46 2022
# Execution time and date (local): Wed Jan 12 00:45:46 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          8192          8192   gramschmidt           MPI          0 362590299.835938            0 
            2          8192          8192   gramschmidt           MPI          1 366512071.017578            9 
            2          8192          8192   gramschmidt           MPI          2 367674694.076172           10 
            2          8192          8192   gramschmidt           MPI          3 375465213.914062            2 
            2          8192          8192   gramschmidt           MPI          4 369143481.216797           12 
            2          8192          8192   gramschmidt           MPI          5 363854185.849609            5 
            2          8192          8192   gramschmidt           MPI          6 386776624.884766            0 
            2          8192          8192   gramschmidt           MPI          7 394579367.812500            5 
            2          8192          8192   gramschmidt           MPI          8 397517142.306641            9 
            2          8192          8192   gramschmidt           MPI          9 391201587.234375            0 
# Runtime: 4919.093364 s (overhead: 0.000001 %) 10 records
