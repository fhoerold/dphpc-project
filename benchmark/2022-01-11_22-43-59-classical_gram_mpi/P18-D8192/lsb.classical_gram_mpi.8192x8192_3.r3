# Sysname : Linux
# Nodename: eu-a6-002-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 23:45:46 2022
# Execution time and date (local): Wed Jan 12 00:45:46 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          8192          8192   gramschmidt           MPI          0 362610201.646484            1 
            3          8192          8192   gramschmidt           MPI          1 366532961.984375            6 
            3          8192          8192   gramschmidt           MPI          2 367695638.906250            6 
            3          8192          8192   gramschmidt           MPI          3 375485558.353516            0 
            3          8192          8192   gramschmidt           MPI          4 369164341.222656            4 
            3          8192          8192   gramschmidt           MPI          5 363874807.892578            3 
            3          8192          8192   gramschmidt           MPI          6 386798135.203125            0 
            3          8192          8192   gramschmidt           MPI          7 394601625.695312            3 
            3          8192          8192   gramschmidt           MPI          8 397539899.806641           10 
            3          8192          8192   gramschmidt           MPI          9 391222946.220703            3 
# Runtime: 4919.257308 s (overhead: 0.000001 %) 10 records
