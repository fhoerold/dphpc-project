# Sysname : Linux
# Nodename: eu-a6-001-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:44:36 2022
# Execution time and date (local): Tue Jan 11 22:44:36 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 11593.796875            0 
            3           256           256   gramschmidt           MPI          1 11393.041016            4 
            3           256           256   gramschmidt           MPI          2 11395.708984            1 
            3           256           256   gramschmidt           MPI          3 11439.654297            0 
            3           256           256   gramschmidt           MPI          4 11605.726562            1 
            3           256           256   gramschmidt           MPI          5 11218.216797            0 
            3           256           256   gramschmidt           MPI          6 11315.755859            0 
            3           256           256   gramschmidt           MPI          7 11402.595703            0 
            3           256           256   gramschmidt           MPI          8 11585.335938            1 
            3           256           256   gramschmidt           MPI          9 11158.347656            0 
# Runtime: 5.155377 s (overhead: 0.000136 %) 10 records
