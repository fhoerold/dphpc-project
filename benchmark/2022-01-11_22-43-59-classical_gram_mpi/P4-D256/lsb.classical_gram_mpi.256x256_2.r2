# Sysname : Linux
# Nodename: eu-a6-001-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:44:36 2022
# Execution time and date (local): Tue Jan 11 22:44:36 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 11696.076172            0 
            2           256           256   gramschmidt           MPI          1 11488.949219            3 
            2           256           256   gramschmidt           MPI          2 11539.183594            1 
            2           256           256   gramschmidt           MPI          3 11587.685547            0 
            2           256           256   gramschmidt           MPI          4 11716.402344            1 
            2           256           256   gramschmidt           MPI          5 11328.640625            0 
            2           256           256   gramschmidt           MPI          6 11399.757812            0 
            2           256           256   gramschmidt           MPI          7 11481.978516            0 
            2           256           256   gramschmidt           MPI          8 11727.957031            1 
            2           256           256   gramschmidt           MPI          9 11353.787109            0 
# Runtime: 6.156610 s (overhead: 0.000097 %) 10 records
