# Sysname : Linux
# Nodename: eu-a6-011-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:53:37 2022
# Execution time and date (local): Tue Jan 11 22:53:37 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 35373589.292969            0 
            3          4096          4096   gramschmidt           MPI          1 35461485.656250            6 
            3          4096          4096   gramschmidt           MPI          2 35380793.087891            2 
            3          4096          4096   gramschmidt           MPI          3 35527886.812500            2 
            3          4096          4096   gramschmidt           MPI          4 35576256.646484            6 
            3          4096          4096   gramschmidt           MPI          5 36117327.832031            0 
            3          4096          4096   gramschmidt           MPI          6 35320658.419922            0 
            3          4096          4096   gramschmidt           MPI          7 35482156.140625            0 
            3          4096          4096   gramschmidt           MPI          8 35494010.974609            6 
            3          4096          4096   gramschmidt           MPI          9 36105200.310547            0 
# Runtime: 464.153273 s (overhead: 0.000005 %) 10 records
