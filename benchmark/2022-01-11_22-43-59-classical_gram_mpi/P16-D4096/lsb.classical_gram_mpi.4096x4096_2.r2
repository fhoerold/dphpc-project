# Sysname : Linux
# Nodename: eu-a6-011-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:53:37 2022
# Execution time and date (local): Tue Jan 11 22:53:37 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 35370258.875000            0 
            2          4096          4096   gramschmidt           MPI          1 35459196.630859            8 
            2          4096          4096   gramschmidt           MPI          2 35377553.425781            1 
            2          4096          4096   gramschmidt           MPI          3 35525633.306641            0 
            2          4096          4096   gramschmidt           MPI          4 35572984.085938            5 
            2          4096          4096   gramschmidt           MPI          5 36114104.945312            0 
            2          4096          4096   gramschmidt           MPI          6 35318393.939453            0 
            2          4096          4096   gramschmidt           MPI          7 35478963.513672            0 
            2          4096          4096   gramschmidt           MPI          8 35491660.984375            4 
            2          4096          4096   gramschmidt           MPI          9 36101812.734375            0 
# Runtime: 464.135482 s (overhead: 0.000004 %) 10 records
