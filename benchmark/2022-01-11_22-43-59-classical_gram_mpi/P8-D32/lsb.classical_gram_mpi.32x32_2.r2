# Sysname : Linux
# Nodename: eu-a6-008-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:45:06 2022
# Execution time and date (local): Tue Jan 11 22:45:06 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 663.195312            2 
            2            32            32   gramschmidt           MPI          1 454.455078            3 
            2            32            32   gramschmidt           MPI          2 504.408203            0 
            2            32            32   gramschmidt           MPI          3 455.373047            0 
            2            32            32   gramschmidt           MPI          4 444.298828            0 
            2            32            32   gramschmidt           MPI          5 455.294922            0 
            2            32            32   gramschmidt           MPI          6 441.134766            0 
            2            32            32   gramschmidt           MPI          7 436.326172            0 
            2            32            32   gramschmidt           MPI          8 466.669922            0 
            2            32            32   gramschmidt           MPI          9 431.974609            0 
# Runtime: 3.004277 s (overhead: 0.000166 %) 10 records
