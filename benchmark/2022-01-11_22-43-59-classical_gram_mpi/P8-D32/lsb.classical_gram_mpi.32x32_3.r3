# Sysname : Linux
# Nodename: eu-a6-008-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:45:06 2022
# Execution time and date (local): Tue Jan 11 22:45:06 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 651.906250            0 
            3            32            32   gramschmidt           MPI          1 444.109375            4 
            3            32            32   gramschmidt           MPI          2 501.345703            0 
            3            32            32   gramschmidt           MPI          3 447.414062            0 
            3            32            32   gramschmidt           MPI          4 438.597656            0 
            3            32            32   gramschmidt           MPI          5 445.673828            0 
            3            32            32   gramschmidt           MPI          6 428.910156            0 
            3            32            32   gramschmidt           MPI          7 432.064453            0 
            3            32            32   gramschmidt           MPI          8 459.224609            0 
            3            32            32   gramschmidt           MPI          9 427.835938            0 
# Runtime: 4.019881 s (overhead: 0.000100 %) 10 records
