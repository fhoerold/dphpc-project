# Sysname : Linux
# Nodename: eu-a6-010-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:51:04 2022
# Execution time and date (local): Wed Jan 12 02:51:04 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 736931.113281            0 
            2          1024          1024   gramschmidt           MPI          1 648936.316406            5 
            2          1024          1024   gramschmidt           MPI          2 293627.257812            1 
            2          1024          1024   gramschmidt           MPI          3 287701.503906            0 
            2          1024          1024   gramschmidt           MPI          4 288729.335938            2 
            2          1024          1024   gramschmidt           MPI          5 288783.664062            0 
            2          1024          1024   gramschmidt           MPI          6 290445.541016            0 
            2          1024          1024   gramschmidt           MPI          7 288308.433594            0 
            2          1024          1024   gramschmidt           MPI          8 287858.757812            1 
            2          1024          1024   gramschmidt           MPI          9 442187.136719            0 
# Runtime: 17.994537 s (overhead: 0.000050 %) 10 records
