# Sysname : Linux
# Nodename: eu-a6-010-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:51:04 2022
# Execution time and date (local): Wed Jan 12 02:51:04 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 736976.998047            0 
            3          1024          1024   gramschmidt           MPI          1 648991.414062            5 
            3          1024          1024   gramschmidt           MPI          2 293643.927734            2 
            3          1024          1024   gramschmidt           MPI          3 287733.175781            0 
            3          1024          1024   gramschmidt           MPI          4 288744.621094            2 
            3          1024          1024   gramschmidt           MPI          5 288795.177734            0 
            3          1024          1024   gramschmidt           MPI          6 290473.277344            0 
            3          1024          1024   gramschmidt           MPI          7 288336.542969            0 
            3          1024          1024   gramschmidt           MPI          8 287894.503906            5 
            3          1024          1024   gramschmidt           MPI          9 442230.457031            0 
# Runtime: 12.999471 s (overhead: 0.000108 %) 10 records
