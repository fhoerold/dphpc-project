# Sysname : Linux
# Nodename: eu-a6-004-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:51:36 2022
# Execution time and date (local): Tue Jan 11 22:51:36 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 3249.986328            0 
            2           128           128   gramschmidt           MPI          1 3202.617188            3 
            2           128           128   gramschmidt           MPI          2 3123.289062            0 
            2           128           128   gramschmidt           MPI          3 3373.458984            0 
            2           128           128   gramschmidt           MPI          4 3154.562500            0 
            2           128           128   gramschmidt           MPI          5 5435.232422            0 
            2           128           128   gramschmidt           MPI          6 3262.587891            0 
            2           128           128   gramschmidt           MPI          7 3059.154297            0 
            2           128           128   gramschmidt           MPI          8 3013.470703            0 
            2           128           128   gramschmidt           MPI          9 3089.091797            0 
# Runtime: 2.051173 s (overhead: 0.000146 %) 10 records
