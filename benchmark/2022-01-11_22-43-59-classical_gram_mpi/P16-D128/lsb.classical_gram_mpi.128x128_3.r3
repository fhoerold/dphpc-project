# Sysname : Linux
# Nodename: eu-a6-004-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:51:36 2022
# Execution time and date (local): Tue Jan 11 22:51:36 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 3258.078125            0 
            3           128           128   gramschmidt           MPI          1 3207.687500            3 
            3           128           128   gramschmidt           MPI          2 3132.810547            0 
            3           128           128   gramschmidt           MPI          3 3379.023438            0 
            3           128           128   gramschmidt           MPI          4 3157.113281            0 
            3           128           128   gramschmidt           MPI          5 5437.025391            0 
            3           128           128   gramschmidt           MPI          6 3269.726562            0 
            3           128           128   gramschmidt           MPI          7 3059.423828            0 
            3           128           128   gramschmidt           MPI          8 3013.875000            0 
            3           128           128   gramschmidt           MPI          9 3093.455078            0 
# Runtime: 1.044739 s (overhead: 0.000287 %) 10 records
