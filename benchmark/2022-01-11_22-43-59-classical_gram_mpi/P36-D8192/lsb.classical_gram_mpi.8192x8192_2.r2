# Sysname : Linux
# Nodename: eu-a6-010-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:58:25 2022
# Execution time and date (local): Wed Jan 12 02:58:25 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          8192          8192   gramschmidt           MPI          0 181768020.027344            1 
            2          8192          8192   gramschmidt           MPI          1 185577318.259766            6 
            2          8192          8192   gramschmidt           MPI          2 180774170.656250            7 
            2          8192          8192   gramschmidt           MPI          3 179326904.751953            3 
            2          8192          8192   gramschmidt           MPI          4 182086808.707031           10 
            2          8192          8192   gramschmidt           MPI          5 181966845.556641            0 
            2          8192          8192   gramschmidt           MPI          6 183855778.880859            0 
            2          8192          8192   gramschmidt           MPI          7 179319770.099609            2 
            2          8192          8192   gramschmidt           MPI          8 175731646.531250            5 
            2          8192          8192   gramschmidt           MPI          9 177968728.330078            3 
# Runtime: 2374.160017 s (overhead: 0.000002 %) 10 records
