# Sysname : Linux
# Nodename: eu-a6-010-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:58:25 2022
# Execution time and date (local): Wed Jan 12 02:58:25 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          8192          8192   gramschmidt           MPI          0 181770440.607422            4 
            3          8192          8192   gramschmidt           MPI          1 185579585.761719           12 
            3          8192          8192   gramschmidt           MPI          2 180776210.119141           12 
            3          8192          8192   gramschmidt           MPI          3 179329008.519531            1 
            3          8192          8192   gramschmidt           MPI          4 182088947.904297            9 
            3          8192          8192   gramschmidt           MPI          5 181968943.283203            2 
            3          8192          8192   gramschmidt           MPI          6 183857756.957031            2 
            3          8192          8192   gramschmidt           MPI          7 179321632.781250            2 
            3          8192          8192   gramschmidt           MPI          8 175733565.156250            6 
            3          8192          8192   gramschmidt           MPI          9 177970533.496094            4 
# Runtime: 2377.104986 s (overhead: 0.000002 %) 10 records
