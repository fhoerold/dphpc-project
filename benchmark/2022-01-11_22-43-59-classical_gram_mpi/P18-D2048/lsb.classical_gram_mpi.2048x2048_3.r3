# Sysname : Linux
# Nodename: eu-a6-012-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 23:36:23 2022
# Execution time and date (local): Wed Jan 12 00:36:23 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 3941777.511719            0 
            3          2048          2048   gramschmidt           MPI          1 3952497.263672            4 
            3          2048          2048   gramschmidt           MPI          2 3906986.935547            3 
            3          2048          2048   gramschmidt           MPI          3 3923212.830078            0 
            3          2048          2048   gramschmidt           MPI          4 3907318.373047            1 
            3          2048          2048   gramschmidt           MPI          5 3959196.623047            0 
            3          2048          2048   gramschmidt           MPI          6 3934720.808594            1 
            3          2048          2048   gramschmidt           MPI          7 3920894.716797            0 
            3          2048          2048   gramschmidt           MPI          8 3915183.216797            4 
            3          2048          2048   gramschmidt           MPI          9 3848848.693359            0 
# Runtime: 50.929877 s (overhead: 0.000026 %) 10 records
