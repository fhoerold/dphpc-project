# Sysname : Linux
# Nodename: eu-a6-012-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 23:36:23 2022
# Execution time and date (local): Wed Jan 12 00:36:23 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 3941604.681641            0 
            2          2048          2048   gramschmidt           MPI          1 3952326.984375            5 
            2          2048          2048   gramschmidt           MPI          2 3906616.314453            6 
            2          2048          2048   gramschmidt           MPI          3 3923029.027344            0 
            2          2048          2048   gramschmidt           MPI          4 3907134.181641            2 
            2          2048          2048   gramschmidt           MPI          5 3958998.974609            0 
            2          2048          2048   gramschmidt           MPI          6 3934437.777344            0 
            2          2048          2048   gramschmidt           MPI          7 3920633.421875            0 
            2          2048          2048   gramschmidt           MPI          8 3915015.757812            5 
            2          2048          2048   gramschmidt           MPI          9 3848667.167969            0 
# Runtime: 53.936335 s (overhead: 0.000033 %) 10 records
