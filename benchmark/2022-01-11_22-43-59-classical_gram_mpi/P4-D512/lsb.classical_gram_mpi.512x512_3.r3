# Sysname : Linux
# Nodename: eu-a6-001-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:44:35 2022
# Execution time and date (local): Tue Jan 11 22:44:35 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 109110.593750            0 
            3           512           512   gramschmidt           MPI          1 109377.132812            1 
            3           512           512   gramschmidt           MPI          2 109381.130859            2 
            3           512           512   gramschmidt           MPI          3 108490.400391            0 
            3           512           512   gramschmidt           MPI          4 109753.283203            2 
            3           512           512   gramschmidt           MPI          5 113158.800781            0 
            3           512           512   gramschmidt           MPI          6 109042.343750            0 
            3           512           512   gramschmidt           MPI          7 108578.312500            0 
            3           512           512   gramschmidt           MPI          8 108739.335938            6 
            3           512           512   gramschmidt           MPI          9 109136.695312            0 
# Runtime: 1.425410 s (overhead: 0.000772 %) 10 records
