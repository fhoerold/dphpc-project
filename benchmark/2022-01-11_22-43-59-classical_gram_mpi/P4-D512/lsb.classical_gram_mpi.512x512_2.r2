# Sysname : Linux
# Nodename: eu-a6-001-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:44:35 2022
# Execution time and date (local): Tue Jan 11 22:44:35 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 109415.865234            0 
            2           512           512   gramschmidt           MPI          1 109659.654297            4 
            2           512           512   gramschmidt           MPI          2 109649.171875            1 
            2           512           512   gramschmidt           MPI          3 108762.267578            0 
            2           512           512   gramschmidt           MPI          4 110034.845703            1 
            2           512           512   gramschmidt           MPI          5 113375.570312            0 
            2           512           512   gramschmidt           MPI          6 109291.191406            0 
            2           512           512   gramschmidt           MPI          7 108841.029297            0 
            2           512           512   gramschmidt           MPI          8 109000.843750            1 
            2           512           512   gramschmidt           MPI          9 109413.972656            0 
# Runtime: 2.436111 s (overhead: 0.000287 %) 10 records
