# Sysname : Linux
# Nodename: eu-a6-012-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 23:36:06 2022
# Execution time and date (local): Wed Jan 12 00:36:06 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 72025.115234            0 
            3           512           512   gramschmidt           MPI          1 61168.070312            3 
            3           512           512   gramschmidt           MPI          2 53543.156250            1 
            3           512           512   gramschmidt           MPI          3 52668.001953            0 
            3           512           512   gramschmidt           MPI          4 53702.640625            2 
            3           512           512   gramschmidt           MPI          5 52784.146484            0 
            3           512           512   gramschmidt           MPI          6 52818.593750            0 
            3           512           512   gramschmidt           MPI          7 52747.660156            0 
            3           512           512   gramschmidt           MPI          8 52818.404297            1 
            3           512           512   gramschmidt           MPI          9 52797.574219            0 
# Runtime: 8.795861 s (overhead: 0.000080 %) 10 records
