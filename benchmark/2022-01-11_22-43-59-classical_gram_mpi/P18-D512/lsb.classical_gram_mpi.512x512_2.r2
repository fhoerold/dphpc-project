# Sysname : Linux
# Nodename: eu-a6-012-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 23:36:06 2022
# Execution time and date (local): Wed Jan 12 00:36:06 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 72318.447266            0 
            2           512           512   gramschmidt           MPI          1 61517.345703            5 
            2           512           512   gramschmidt           MPI          2 53841.007812            1 
            2           512           512   gramschmidt           MPI          3 52988.947266            0 
            2           512           512   gramschmidt           MPI          4 54002.675781            2 
            2           512           512   gramschmidt           MPI          5 53069.720703            0 
            2           512           512   gramschmidt           MPI          6 53112.998047            0 
            2           512           512   gramschmidt           MPI          7 53030.419922            0 
            2           512           512   gramschmidt           MPI          8 53133.630859            2 
            2           512           512   gramschmidt           MPI          9 53119.628906            0 
# Runtime: 8.797042 s (overhead: 0.000114 %) 10 records
