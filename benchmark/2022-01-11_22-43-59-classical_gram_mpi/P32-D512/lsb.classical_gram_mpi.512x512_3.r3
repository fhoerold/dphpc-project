# Sysname : Linux
# Nodename: eu-a6-007-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:09:48 2022
# Execution time and date (local): Wed Jan 12 02:09:48 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 90675.646484            0 
            3           512           512   gramschmidt           MPI          1 94282.865234            4 
            3           512           512   gramschmidt           MPI          2 92673.423828            1 
            3           512           512   gramschmidt           MPI          3 61117.082031            0 
            3           512           512   gramschmidt           MPI          4 44698.265625            1 
            3           512           512   gramschmidt           MPI          5 43960.873047            0 
            3           512           512   gramschmidt           MPI          6 43564.876953            0 
            3           512           512   gramschmidt           MPI          7 43679.447266            0 
            3           512           512   gramschmidt           MPI          8 43605.423828            1 
            3           512           512   gramschmidt           MPI          9 45536.621094            0 
# Runtime: 22.888073 s (overhead: 0.000031 %) 10 records
