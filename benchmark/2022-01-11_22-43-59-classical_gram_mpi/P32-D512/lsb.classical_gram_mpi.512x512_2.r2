# Sysname : Linux
# Nodename: eu-a6-007-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:09:48 2022
# Execution time and date (local): Wed Jan 12 02:09:48 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 90794.865234            0 
            2           512           512   gramschmidt           MPI          1 94426.035156            4 
            2           512           512   gramschmidt           MPI          2 92764.007812            1 
            2           512           512   gramschmidt           MPI          3 61167.349609            0 
            2           512           512   gramschmidt           MPI          4 44732.943359            2 
            2           512           512   gramschmidt           MPI          5 43996.824219            0 
            2           512           512   gramschmidt           MPI          6 43600.669922            0 
            2           512           512   gramschmidt           MPI          7 43707.150391            0 
            2           512           512   gramschmidt           MPI          8 43635.396484            1 
            2           512           512   gramschmidt           MPI          9 45732.921875            0 
# Runtime: 18.939680 s (overhead: 0.000042 %) 10 records
