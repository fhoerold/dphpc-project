# Sysname : Linux
# Nodename: eu-a6-012-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 23:35:31 2022
# Execution time and date (local): Wed Jan 12 00:35:31 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 2360.152344            0 
            3           128           128   gramschmidt           MPI          1 2372.119141            2 
            3           128           128   gramschmidt           MPI          2 2321.388672            0 
            3           128           128   gramschmidt           MPI          3 2247.341797            0 
            3           128           128   gramschmidt           MPI          4 2163.425781            0 
            3           128           128   gramschmidt           MPI          5 2178.910156            0 
            3           128           128   gramschmidt           MPI          6 2174.951172            0 
            3           128           128   gramschmidt           MPI          7 2189.414062            0 
            3           128           128   gramschmidt           MPI          8 2150.554688            0 
            3           128           128   gramschmidt           MPI          9 2171.076172            0 
# Runtime: 4.039190 s (overhead: 0.000050 %) 10 records
