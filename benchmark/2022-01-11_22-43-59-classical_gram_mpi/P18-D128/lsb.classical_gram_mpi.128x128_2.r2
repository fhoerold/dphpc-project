# Sysname : Linux
# Nodename: eu-a6-012-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 23:35:31 2022
# Execution time and date (local): Wed Jan 12 00:35:31 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 2346.646484            0 
            2           128           128   gramschmidt           MPI          1 2375.609375            3 
            2           128           128   gramschmidt           MPI          2 2321.103516            0 
            2           128           128   gramschmidt           MPI          3 2247.322266            0 
            2           128           128   gramschmidt           MPI          4 2169.281250            0 
            2           128           128   gramschmidt           MPI          5 2181.041016            0 
            2           128           128   gramschmidt           MPI          6 2176.250000            0 
            2           128           128   gramschmidt           MPI          7 2189.505859            0 
            2           128           128   gramschmidt           MPI          8 2154.632812            0 
            2           128           128   gramschmidt           MPI          9 2171.484375            0 
# Runtime: 8.042652 s (overhead: 0.000037 %) 10 records
