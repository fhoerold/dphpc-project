# Sysname : Linux
# Nodename: eu-a6-002-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 23:38:19 2022
# Execution time and date (local): Wed Jan 12 00:38:19 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 31471526.804688            0 
            3          4096          4096   gramschmidt           MPI          1 31301179.812500            4 
            3          4096          4096   gramschmidt           MPI          2 31057024.794922            2 
            3          4096          4096   gramschmidt           MPI          3 31141383.382812            0 
            3          4096          4096   gramschmidt           MPI          4 30909840.427734            6 
            3          4096          4096   gramschmidt           MPI          5 31308089.751953            2 
            3          4096          4096   gramschmidt           MPI          6 31384356.292969            0 
            3          4096          4096   gramschmidt           MPI          7 33294280.326172            2 
            3          4096          4096   gramschmidt           MPI          8 31589269.027344           15 
            3          4096          4096   gramschmidt           MPI          9 31489848.082031            0 
# Runtime: 415.398371 s (overhead: 0.000007 %) 10 records
