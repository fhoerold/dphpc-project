# Sysname : Linux
# Nodename: eu-a6-002-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 23:38:19 2022
# Execution time and date (local): Wed Jan 12 00:38:19 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 31471192.076172            0 
            2          4096          4096   gramschmidt           MPI          1 31300067.806641            4 
            2          4096          4096   gramschmidt           MPI          2 31056727.808594            2 
            2          4096          4096   gramschmidt           MPI          3 31140335.345703            0 
            2          4096          4096   gramschmidt           MPI          4 30909502.541016            6 
            2          4096          4096   gramschmidt           MPI          5 31306966.496094            0 
            2          4096          4096   gramschmidt           MPI          6 31384055.738281            0 
            2          4096          4096   gramschmidt           MPI          7 33293168.085938            0 
            2          4096          4096   gramschmidt           MPI          8 31588791.064453            8 
            2          4096          4096   gramschmidt           MPI          9 31488746.111328            1 
# Runtime: 415.414922 s (overhead: 0.000005 %) 10 records
