# Sysname : Linux
# Nodename: eu-a6-010-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:57:44 2022
# Execution time and date (local): Wed Jan 12 02:57:44 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 76037.722656            0 
            2           512           512   gramschmidt           MPI          1 102661.916016            4 
            2           512           512   gramschmidt           MPI          2 81159.843750            2 
            2           512           512   gramschmidt           MPI          3 106059.441406            0 
            2           512           512   gramschmidt           MPI          4 106383.687500            2 
            2           512           512   gramschmidt           MPI          5 80503.220703            0 
            2           512           512   gramschmidt           MPI          6 74000.085938            0 
            2           512           512   gramschmidt           MPI          7 72947.431641            0 
            2           512           512   gramschmidt           MPI          8 75393.683594            4 
            2           512           512   gramschmidt           MPI          9 72966.322266            0 
# Runtime: 8.211944 s (overhead: 0.000146 %) 10 records
