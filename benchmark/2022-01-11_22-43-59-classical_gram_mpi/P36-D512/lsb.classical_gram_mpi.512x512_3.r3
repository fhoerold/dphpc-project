# Sysname : Linux
# Nodename: eu-a6-010-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:57:44 2022
# Execution time and date (local): Wed Jan 12 02:57:44 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 76061.087891            0 
            3           512           512   gramschmidt           MPI          1 102697.847656            1 
            3           512           512   gramschmidt           MPI          2 81180.912109            2 
            3           512           512   gramschmidt           MPI          3 106092.583984            0 
            3           512           512   gramschmidt           MPI          4 106427.818359            1 
            3           512           512   gramschmidt           MPI          5 80543.857422            0 
            3           512           512   gramschmidt           MPI          6 74028.734375            0 
            3           512           512   gramschmidt           MPI          7 72965.097656            0 
            3           512           512   gramschmidt           MPI          8 75406.146484            5 
            3           512           512   gramschmidt           MPI          9 72986.017578            0 
# Runtime: 1.217901 s (overhead: 0.000739 %) 10 records
