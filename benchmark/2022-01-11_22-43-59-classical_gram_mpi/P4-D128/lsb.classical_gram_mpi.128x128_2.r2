# Sysname : Linux
# Nodename: eu-a6-001-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:44:36 2022
# Execution time and date (local): Tue Jan 11 22:44:36 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 1973.179688            3 
            2           128           128   gramschmidt           MPI          1 2010.955078            3 
            2           128           128   gramschmidt           MPI          2 1993.453125            0 
            2           128           128   gramschmidt           MPI          3 2272.484375            0 
            2           128           128   gramschmidt           MPI          4 1986.394531            0 
            2           128           128   gramschmidt           MPI          5 1928.722656            0 
            2           128           128   gramschmidt           MPI          6 1939.744141            0 
            2           128           128   gramschmidt           MPI          7 1922.648438            0 
            2           128           128   gramschmidt           MPI          8 1905.003906            0 
            2           128           128   gramschmidt           MPI          9 1907.912109            0 
# Runtime: 9.016439 s (overhead: 0.000067 %) 10 records
