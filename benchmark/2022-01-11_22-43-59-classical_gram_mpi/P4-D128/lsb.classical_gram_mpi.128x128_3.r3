# Sysname : Linux
# Nodename: eu-a6-001-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:44:36 2022
# Execution time and date (local): Tue Jan 11 22:44:36 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 1941.750000            0 
            3           128           128   gramschmidt           MPI          1 1977.382812            0 
            3           128           128   gramschmidt           MPI          2 1957.896484            0 
            3           128           128   gramschmidt           MPI          3 2244.355469            0 
            3           128           128   gramschmidt           MPI          4 1955.154297            0 
            3           128           128   gramschmidt           MPI          5 1897.546875            0 
            3           128           128   gramschmidt           MPI          6 1898.689453            0 
            3           128           128   gramschmidt           MPI          7 1890.275391            0 
            3           128           128   gramschmidt           MPI          8 1879.779297            0 
            3           128           128   gramschmidt           MPI          9 1879.300781            0 
# Runtime: 0.028046 s (overhead: 0.000000 %) 10 records
