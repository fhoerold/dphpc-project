# Sysname : Linux
# Nodename: eu-a6-010-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:57:24 2022
# Execution time and date (local): Wed Jan 12 02:57:24 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 1809.724609            0 
            3            64            64   gramschmidt           MPI          1 1668.578125            2 
            3            64            64   gramschmidt           MPI          2 1647.820312            0 
            3            64            64   gramschmidt           MPI          3 1635.384766            0 
            3            64            64   gramschmidt           MPI          4 1583.751953            0 
            3            64            64   gramschmidt           MPI          5 1590.242188            0 
            3            64            64   gramschmidt           MPI          6 1514.414062            0 
            3            64            64   gramschmidt           MPI          7 1587.845703            0 
            3            64            64   gramschmidt           MPI          8 1604.335938            0 
            3            64            64   gramschmidt           MPI          9 1504.101562            0 
# Runtime: 8.015634 s (overhead: 0.000025 %) 10 records
