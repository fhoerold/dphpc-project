# Sysname : Linux
# Nodename: eu-a6-010-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:57:24 2022
# Execution time and date (local): Wed Jan 12 02:57:24 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 1851.953125            0 
            2            64            64   gramschmidt           MPI          1 1743.625000            2 
            2            64            64   gramschmidt           MPI          2 1709.371094            0 
            2            64            64   gramschmidt           MPI          3 1682.847656            0 
            2            64            64   gramschmidt           MPI          4 1635.082031            0 
            2            64            64   gramschmidt           MPI          5 1641.228516            0 
            2            64            64   gramschmidt           MPI          6 1567.111328            0 
            2            64            64   gramschmidt           MPI          7 1627.062500            0 
            2            64            64   gramschmidt           MPI          8 1641.585938            0 
            2            64            64   gramschmidt           MPI          9 1536.089844            0 
# Runtime: 13.043829 s (overhead: 0.000015 %) 10 records
