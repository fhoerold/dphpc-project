# Sysname : Linux
# Nodename: eu-a6-001-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:45:18 2022
# Execution time and date (local): Tue Jan 11 22:45:18 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 1932.910156            0 
            2           128           128   gramschmidt           MPI          1 1782.830078            3 
            2           128           128   gramschmidt           MPI          2 1738.222656            0 
            2           128           128   gramschmidt           MPI          3 1722.978516            0 
            2           128           128   gramschmidt           MPI          4 1719.470703            0 
            2           128           128   gramschmidt           MPI          5 1711.511719            0 
            2           128           128   gramschmidt           MPI          6 1678.990234            0 
            2           128           128   gramschmidt           MPI          7 1683.968750            0 
            2           128           128   gramschmidt           MPI          8 1676.253906            0 
            2           128           128   gramschmidt           MPI          9 1684.841797            0 
# Runtime: 3.026235 s (overhead: 0.000099 %) 10 records
