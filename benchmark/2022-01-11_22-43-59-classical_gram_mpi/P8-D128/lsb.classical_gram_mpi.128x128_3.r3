# Sysname : Linux
# Nodename: eu-a6-001-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:45:18 2022
# Execution time and date (local): Tue Jan 11 22:45:18 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 1919.351562            0 
            3           128           128   gramschmidt           MPI          1 1764.658203            0 
            3           128           128   gramschmidt           MPI          2 1719.759766            0 
            3           128           128   gramschmidt           MPI          3 1709.076172            0 
            3           128           128   gramschmidt           MPI          4 1702.578125            0 
            3           128           128   gramschmidt           MPI          5 1699.105469            0 
            3           128           128   gramschmidt           MPI          6 1664.623047            0 
            3           128           128   gramschmidt           MPI          7 1668.822266            0 
            3           128           128   gramschmidt           MPI          8 1655.089844            0 
            3           128           128   gramschmidt           MPI          9 1665.142578            0 
# Runtime: 0.025603 s (overhead: 0.000000 %) 10 records
