# Sysname : Linux
# Nodename: eu-a6-010-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:49:57 2022
# Execution time and date (local): Wed Jan 12 02:49:57 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 826.318359            0 
            2            32            32   gramschmidt           MPI          1 784.667969            2 
            2            32            32   gramschmidt           MPI          2 760.693359            0 
            2            32            32   gramschmidt           MPI          3 714.865234            0 
            2            32            32   gramschmidt           MPI          4 702.814453            0 
            2            32            32   gramschmidt           MPI          5 677.445312            0 
            2            32            32   gramschmidt           MPI          6 685.728516            0 
            2            32            32   gramschmidt           MPI          7 639.142578            0 
            2            32            32   gramschmidt           MPI          8 645.527344            0 
            2            32            32   gramschmidt           MPI          9 643.773438            0 
# Runtime: 15.033231 s (overhead: 0.000013 %) 10 records
