# Sysname : Linux
# Nodename: eu-a6-010-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:49:57 2022
# Execution time and date (local): Wed Jan 12 02:49:57 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 840.005859            0 
            3            32            32   gramschmidt           MPI          1 770.189453            2 
            3            32            32   gramschmidt           MPI          2 744.972656            0 
            3            32            32   gramschmidt           MPI          3 700.455078            0 
            3            32            32   gramschmidt           MPI          4 689.546875            0 
            3            32            32   gramschmidt           MPI          5 665.707031            0 
            3            32            32   gramschmidt           MPI          6 674.042969            0 
            3            32            32   gramschmidt           MPI          7 634.892578            0 
            3            32            32   gramschmidt           MPI          8 638.996094            0 
            3            32            32   gramschmidt           MPI          9 628.550781            0 
# Runtime: 14.019752 s (overhead: 0.000014 %) 10 records
