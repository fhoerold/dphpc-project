# Sysname : Linux
# Nodename: eu-a6-009-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:51:05 2022
# Execution time and date (local): Tue Jan 11 22:51:05 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 47869.150391            0 
            2           512           512   gramschmidt           MPI          1 48150.949219            4 
            2           512           512   gramschmidt           MPI          2 45603.824219            1 
            2           512           512   gramschmidt           MPI          3 45998.544922            0 
            2           512           512   gramschmidt           MPI          4 45957.144531            1 
            2           512           512   gramschmidt           MPI          5 46393.126953            0 
            2           512           512   gramschmidt           MPI          6 44959.970703            0 
            2           512           512   gramschmidt           MPI          7 45034.082031            0 
            2           512           512   gramschmidt           MPI          8 44790.556641            1 
            2           512           512   gramschmidt           MPI          9 45524.837891            0 
# Runtime: 1.697919 s (overhead: 0.000412 %) 10 records
