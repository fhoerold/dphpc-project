# Sysname : Linux
# Nodename: eu-a6-009-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:51:05 2022
# Execution time and date (local): Tue Jan 11 22:51:05 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 47870.457031            0 
            3           512           512   gramschmidt           MPI          1 48164.619141            5 
            3           512           512   gramschmidt           MPI          2 45610.052734            1 
            3           512           512   gramschmidt           MPI          3 46003.519531            0 
            3           512           512   gramschmidt           MPI          4 45961.017578            1 
            3           512           512   gramschmidt           MPI          5 46398.197266            0 
            3           512           512   gramschmidt           MPI          6 44966.228516            0 
            3           512           512   gramschmidt           MPI          7 45035.669922            0 
            3           512           512   gramschmidt           MPI          8 44783.486328            2 
            3           512           512   gramschmidt           MPI          9 45527.164062            0 
# Runtime: 7.692013 s (overhead: 0.000117 %) 10 records
