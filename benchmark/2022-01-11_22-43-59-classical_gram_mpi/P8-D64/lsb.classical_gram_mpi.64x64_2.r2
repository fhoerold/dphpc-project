# Sysname : Linux
# Nodename: eu-a6-001-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:45:06 2022
# Execution time and date (local): Tue Jan 11 22:45:06 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 861.380859            0 
            2            64            64   gramschmidt           MPI          1 720.972656            3 
            2            64            64   gramschmidt           MPI          2 647.845703            0 
            2            64            64   gramschmidt           MPI          3 629.523438            0 
            2            64            64   gramschmidt           MPI          4 631.142578            0 
            2            64            64   gramschmidt           MPI          5 653.646484            0 
            2            64            64   gramschmidt           MPI          6 726.593750            0 
            2            64            64   gramschmidt           MPI          7 656.988281            0 
            2            64            64   gramschmidt           MPI          8 649.388672            0 
            2            64            64   gramschmidt           MPI          9 613.978516            0 
# Runtime: 1.012994 s (overhead: 0.000296 %) 10 records
