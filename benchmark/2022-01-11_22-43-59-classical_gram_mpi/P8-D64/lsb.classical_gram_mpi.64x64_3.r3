# Sysname : Linux
# Nodename: eu-a6-001-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:45:06 2022
# Execution time and date (local): Tue Jan 11 22:45:06 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 837.265625            0 
            3            64            64   gramschmidt           MPI          1 700.281250            3 
            3            64            64   gramschmidt           MPI          2 634.158203            0 
            3            64            64   gramschmidt           MPI          3 623.281250            0 
            3            64            64   gramschmidt           MPI          4 628.896484            0 
            3            64            64   gramschmidt           MPI          5 640.736328            0 
            3            64            64   gramschmidt           MPI          6 714.486328            0 
            3            64            64   gramschmidt           MPI          7 641.187500            0 
            3            64            64   gramschmidt           MPI          8 667.212891            0 
            3            64            64   gramschmidt           MPI          9 614.849609            0 
# Runtime: 3.003612 s (overhead: 0.000100 %) 10 records
