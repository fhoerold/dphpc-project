# Sysname : Linux
# Nodename: eu-a6-007-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:10:41 2022
# Execution time and date (local): Wed Jan 12 02:10:41 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 1808208.945312            0 
            3          2048          2048   gramschmidt           MPI          1 1738093.355469            5 
            3          2048          2048   gramschmidt           MPI          2 1734458.195312            2 
            3          2048          2048   gramschmidt           MPI          3 1736534.041016            0 
            3          2048          2048   gramschmidt           MPI          4 1830812.265625            8 
            3          2048          2048   gramschmidt           MPI          5 1734418.933594            0 
            3          2048          2048   gramschmidt           MPI          6 1721991.054688            0 
            3          2048          2048   gramschmidt           MPI          7 1732145.572266            1 
            3          2048          2048   gramschmidt           MPI          8 1714979.597656            3 
            3          2048          2048   gramschmidt           MPI          9 1712522.925781            0 
# Runtime: 31.310289 s (overhead: 0.000061 %) 10 records
