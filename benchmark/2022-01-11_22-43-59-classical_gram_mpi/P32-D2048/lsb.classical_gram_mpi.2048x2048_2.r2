# Sysname : Linux
# Nodename: eu-a6-007-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:10:41 2022
# Execution time and date (local): Wed Jan 12 02:10:41 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 1807955.837891            0 
            2          2048          2048   gramschmidt           MPI          1 1737842.763672            4 
            2          2048          2048   gramschmidt           MPI          2 1734251.046875            2 
            2          2048          2048   gramschmidt           MPI          3 1736256.376953            0 
            2          2048          2048   gramschmidt           MPI          4 1830533.365234            3 
            2          2048          2048   gramschmidt           MPI          5 1734160.982422            0 
            2          2048          2048   gramschmidt           MPI          6 1721756.183594            0 
            2          2048          2048   gramschmidt           MPI          7 1731909.585938            1 
            2          2048          2048   gramschmidt           MPI          8 1714804.113281            7 
            2          2048          2048   gramschmidt           MPI          9 1712284.818359            0 
# Runtime: 28.341243 s (overhead: 0.000060 %) 10 records
