# Sysname : Linux
# Nodename: eu-a6-007-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:46:05 2022
# Execution time and date (local): Tue Jan 11 22:46:05 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 7806864.294922            0 
            2          2048          2048   gramschmidt           MPI          1 7796200.056641            5 
            2          2048          2048   gramschmidt           MPI          2 7759223.605469            2 
            2          2048          2048   gramschmidt           MPI          3 7774082.515625            0 
            2          2048          2048   gramschmidt           MPI          4 7740330.414062            6 
            2          2048          2048   gramschmidt           MPI          5 7744381.248047            0 
            2          2048          2048   gramschmidt           MPI          6 7774003.248047            0 
            2          2048          2048   gramschmidt           MPI          7 7781043.003906            0 
            2          2048          2048   gramschmidt           MPI          8 7758276.681641            6 
            2          2048          2048   gramschmidt           MPI          9 7720162.929688            0 
# Runtime: 110.076771 s (overhead: 0.000017 %) 10 records
