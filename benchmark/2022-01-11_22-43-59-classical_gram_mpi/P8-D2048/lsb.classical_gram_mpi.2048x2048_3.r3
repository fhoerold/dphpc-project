# Sysname : Linux
# Nodename: eu-a6-007-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:46:05 2022
# Execution time and date (local): Tue Jan 11 22:46:05 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 7807196.996094            0 
            3          2048          2048   gramschmidt           MPI          1 7797025.556641            6 
            3          2048          2048   gramschmidt           MPI          2 7759643.216797            3 
            3          2048          2048   gramschmidt           MPI          3 7774961.300781            0 
            3          2048          2048   gramschmidt           MPI          4 7740745.326172            8 
            3          2048          2048   gramschmidt           MPI          5 7744699.488281            0 
            3          2048          2048   gramschmidt           MPI          6 7774394.640625            0 
            3          2048          2048   gramschmidt           MPI          7 7781394.736328            0 
            3          2048          2048   gramschmidt           MPI          8 7759049.078125            7 
            3          2048          2048   gramschmidt           MPI          9 7720575.111328            0 
# Runtime: 109.070121 s (overhead: 0.000022 %) 10 records
