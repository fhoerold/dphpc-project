# Sysname : Linux
# Nodename: eu-a6-010-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:51:37 2022
# Execution time and date (local): Wed Jan 12 02:51:37 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 2296692.408203            0 
            3          2048          2048   gramschmidt           MPI          1 2226590.523438            5 
            3          2048          2048   gramschmidt           MPI          2 2205504.289062            1 
            3          2048          2048   gramschmidt           MPI          3 2206199.181641            0 
            3          2048          2048   gramschmidt           MPI          4 2303795.308594            7 
            3          2048          2048   gramschmidt           MPI          5 2212555.804688            0 
            3          2048          2048   gramschmidt           MPI          6 2205527.896484            1 
            3          2048          2048   gramschmidt           MPI          7 2206477.199219            0 
            3          2048          2048   gramschmidt           MPI          8 2209873.056641            4 
            3          2048          2048   gramschmidt           MPI          9 2300642.289062            0 
# Runtime: 33.843362 s (overhead: 0.000053 %) 10 records
