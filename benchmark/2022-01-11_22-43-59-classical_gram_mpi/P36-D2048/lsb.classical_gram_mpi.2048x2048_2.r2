# Sysname : Linux
# Nodename: eu-a6-010-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:51:37 2022
# Execution time and date (local): Wed Jan 12 02:51:37 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 2297026.626953            0 
            2          2048          2048   gramschmidt           MPI          1 2226851.400391            6 
            2          2048          2048   gramschmidt           MPI          2 2205706.025391            2 
            2          2048          2048   gramschmidt           MPI          3 2206257.283203            0 
            2          2048          2048   gramschmidt           MPI          4 2304019.414062            4 
            2          2048          2048   gramschmidt           MPI          5 2212751.814453            0 
            2          2048          2048   gramschmidt           MPI          6 2205656.884766            0 
            2          2048          2048   gramschmidt           MPI          7 2206678.140625            0 
            2          2048          2048   gramschmidt           MPI          8 2210067.417969            2 
            2          2048          2048   gramschmidt           MPI          9 2300832.626953            0 
# Runtime: 34.883626 s (overhead: 0.000040 %) 10 records
