# Sysname : Linux
# Nodename: eu-a6-012-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 23:35:47 2022
# Execution time and date (local): Wed Jan 12 00:35:47 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 9542.611328            0 
            2           256           256   gramschmidt           MPI          1 9510.429688            3 
            2           256           256   gramschmidt           MPI          2 9396.757812            1 
            2           256           256   gramschmidt           MPI          3 9251.974609            0 
            2           256           256   gramschmidt           MPI          4 9326.050781            0 
            2           256           256   gramschmidt           MPI          5 9075.478516            0 
            2           256           256   gramschmidt           MPI          6 9141.021484            0 
            2           256           256   gramschmidt           MPI          7 9026.744141            0 
            2           256           256   gramschmidt           MPI          8 9076.267578            0 
            2           256           256   gramschmidt           MPI          9 9117.634766            0 
# Runtime: 9.147768 s (overhead: 0.000044 %) 10 records
