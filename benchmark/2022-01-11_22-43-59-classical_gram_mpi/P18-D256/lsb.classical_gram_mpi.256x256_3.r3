# Sysname : Linux
# Nodename: eu-a6-012-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 23:35:47 2022
# Execution time and date (local): Wed Jan 12 00:35:47 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 9554.628906            0 
            3           256           256   gramschmidt           MPI          1 9492.203125            3 
            3           256           256   gramschmidt           MPI          2 9381.404297            0 
            3           256           256   gramschmidt           MPI          3 9237.400391            0 
            3           256           256   gramschmidt           MPI          4 9319.328125            0 
            3           256           256   gramschmidt           MPI          5 9067.677734            0 
            3           256           256   gramschmidt           MPI          6 9134.773438            0 
            3           256           256   gramschmidt           MPI          7 9013.548828            0 
            3           256           256   gramschmidt           MPI          8 9059.849609            1 
            3           256           256   gramschmidt           MPI          9 9071.062500            0 
# Runtime: 9.150632 s (overhead: 0.000044 %) 10 records
