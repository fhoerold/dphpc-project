# Sysname : Linux
# Nodename: eu-a6-001-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:45:06 2022
# Execution time and date (local): Tue Jan 11 22:45:06 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 13620328.933594            0 
            2          2048          2048   gramschmidt           MPI          1 13678635.328125            6 
            2          2048          2048   gramschmidt           MPI          2 13204757.480469            2 
            2          2048          2048   gramschmidt           MPI          3 13413918.253906            0 
            2          2048          2048   gramschmidt           MPI          4 13817799.462891            5 
            2          2048          2048   gramschmidt           MPI          5 13821296.326172            0 
            2          2048          2048   gramschmidt           MPI          6 13636662.548828            0 
            2          2048          2048   gramschmidt           MPI          7 13590539.525391            0 
            2          2048          2048   gramschmidt           MPI          8 13782078.220703            6 
            2          2048          2048   gramschmidt           MPI          9 13777082.691406            0 
# Runtime: 184.225915 s (overhead: 0.000010 %) 10 records
