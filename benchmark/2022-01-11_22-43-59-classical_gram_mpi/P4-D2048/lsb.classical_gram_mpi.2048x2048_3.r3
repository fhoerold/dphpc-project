# Sysname : Linux
# Nodename: eu-a6-001-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:45:06 2022
# Execution time and date (local): Tue Jan 11 22:45:06 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 13621656.947266            0 
            3          2048          2048   gramschmidt           MPI          1 13679108.611328            5 
            3          2048          2048   gramschmidt           MPI          2 13205389.310547            3 
            3          2048          2048   gramschmidt           MPI          3 13415125.146484            0 
            3          2048          2048   gramschmidt           MPI          4 13818269.638672            2 
            3          2048          2048   gramschmidt           MPI          5 13821895.699219            1 
            3          2048          2048   gramschmidt           MPI          6 13638000.644531            0 
            3          2048          2048   gramschmidt           MPI          7 13591053.287109            0 
            3          2048          2048   gramschmidt           MPI          8 13782610.212891            5 
            3          2048          2048   gramschmidt           MPI          9 13777469.925781            0 
# Runtime: 177.212601 s (overhead: 0.000009 %) 10 records
