# Sysname : Linux
# Nodename: eu-a6-004-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:51:48 2022
# Execution time and date (local): Tue Jan 11 22:51:48 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 11764.199219            0 
            3           256           256   gramschmidt           MPI          1 17660.050781            3 
            3           256           256   gramschmidt           MPI          2 15745.011719            0 
            3           256           256   gramschmidt           MPI          3 13258.402344            0 
            3           256           256   gramschmidt           MPI          4 9990.619141            0 
            3           256           256   gramschmidt           MPI          5 9860.226562            0 
            3           256           256   gramschmidt           MPI          6 11818.544922            0 
            3           256           256   gramschmidt           MPI          7 9967.464844            0 
            3           256           256   gramschmidt           MPI          8 9757.835938            3 
            3           256           256   gramschmidt           MPI          9 10052.023438            0 
# Runtime: 8.179899 s (overhead: 0.000073 %) 10 records
