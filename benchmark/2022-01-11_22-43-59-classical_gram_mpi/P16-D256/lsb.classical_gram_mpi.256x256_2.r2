# Sysname : Linux
# Nodename: eu-a6-004-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:51:48 2022
# Execution time and date (local): Tue Jan 11 22:51:48 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 11766.376953            0 
            2           256           256   gramschmidt           MPI          1 17661.650391            3 
            2           256           256   gramschmidt           MPI          2 15746.349609            0 
            2           256           256   gramschmidt           MPI          3 13259.968750            0 
            2           256           256   gramschmidt           MPI          4 9987.267578            0 
            2           256           256   gramschmidt           MPI          5 9853.228516            0 
            2           256           256   gramschmidt           MPI          6 11832.660156            0 
            2           256           256   gramschmidt           MPI          7 9966.966797            0 
            2           256           256   gramschmidt           MPI          8 9755.505859            3 
            2           256           256   gramschmidt           MPI          9 10050.447266            0 
# Runtime: 8.183931 s (overhead: 0.000073 %) 10 records
