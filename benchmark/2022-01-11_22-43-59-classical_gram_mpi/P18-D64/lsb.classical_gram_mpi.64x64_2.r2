# Sysname : Linux
# Nodename: eu-a6-012-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 23:35:15 2022
# Execution time and date (local): Wed Jan 12 00:35:15 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 1023.753906            0 
            2            64            64   gramschmidt           MPI          1 1052.195312            2 
            2            64            64   gramschmidt           MPI          2 967.794922            0 
            2            64            64   gramschmidt           MPI          3 971.951172            0 
            2            64            64   gramschmidt           MPI          4 938.148438            0 
            2            64            64   gramschmidt           MPI          5 937.404297            0 
            2            64            64   gramschmidt           MPI          6 928.242188            0 
            2            64            64   gramschmidt           MPI          7 939.156250            0 
            2            64            64   gramschmidt           MPI          8 931.365234            0 
            2            64            64   gramschmidt           MPI          9 924.308594            0 
# Runtime: 5.005538 s (overhead: 0.000040 %) 10 records
