# Sysname : Linux
# Nodename: eu-a6-012-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 23:35:15 2022
# Execution time and date (local): Wed Jan 12 00:35:15 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 1016.205078            0 
            3            64            64   gramschmidt           MPI          1 1053.556641            3 
            3            64            64   gramschmidt           MPI          2 966.939453            0 
            3            64            64   gramschmidt           MPI          3 972.031250            0 
            3            64            64   gramschmidt           MPI          4 934.037109            0 
            3            64            64   gramschmidt           MPI          5 931.683594            0 
            3            64            64   gramschmidt           MPI          6 928.080078            0 
            3            64            64   gramschmidt           MPI          7 937.736328            0 
            3            64            64   gramschmidt           MPI          8 930.031250            0 
            3            64            64   gramschmidt           MPI          9 924.558594            0 
# Runtime: 5.017770 s (overhead: 0.000060 %) 10 records
