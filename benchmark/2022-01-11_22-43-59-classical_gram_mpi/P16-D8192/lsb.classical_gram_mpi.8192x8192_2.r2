# Sysname : Linux
# Nodename: eu-a6-011-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 22:02:05 2022
# Execution time and date (local): Tue Jan 11 23:02:05 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          8192          8192   gramschmidt           MPI          0 438106673.302734            0 
            2          8192          8192   gramschmidt           MPI          1 427816307.812500            9 
            2          8192          8192   gramschmidt           MPI          2 418495784.867188            7 
            2          8192          8192   gramschmidt           MPI          3 360023156.048828            0 
            2          8192          8192   gramschmidt           MPI          4 368370248.587891            7 
            2          8192          8192   gramschmidt           MPI          5 389879962.033203            4 
            2          8192          8192   gramschmidt           MPI          6 368080535.326172            0 
            2          8192          8192   gramschmidt           MPI          7 387934259.445312            2 
            2          8192          8192   gramschmidt           MPI          8 355845604.615234            9 
            2          8192          8192   gramschmidt           MPI          9 369286534.962891            2 
# Runtime: 5361.000429 s (overhead: 0.000001 %) 10 records
