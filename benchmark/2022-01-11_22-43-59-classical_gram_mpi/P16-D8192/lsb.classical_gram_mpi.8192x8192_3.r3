# Sysname : Linux
# Nodename: eu-a6-011-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 22:02:05 2022
# Execution time and date (local): Tue Jan 11 23:02:05 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          8192          8192   gramschmidt           MPI          0 437997948.832031            1 
            3          8192          8192   gramschmidt           MPI          1 427711043.742188            6 
            3          8192          8192   gramschmidt           MPI          2 418392871.787109            7 
            3          8192          8192   gramschmidt           MPI          3 359935448.142578            4 
            3          8192          8192   gramschmidt           MPI          4 368280468.316406            9 
            3          8192          8192   gramschmidt           MPI          5 389784670.642578            4 
            3          8192          8192   gramschmidt           MPI          6 367991405.414062            2 
            3          8192          8192   gramschmidt           MPI          7 387839131.558594            2 
            3          8192          8192   gramschmidt           MPI          8 355759039.076172           12 
            3          8192          8192   gramschmidt           MPI          9 369196456.572266            2 
# Runtime: 5373.572031 s (overhead: 0.000001 %) 10 records
