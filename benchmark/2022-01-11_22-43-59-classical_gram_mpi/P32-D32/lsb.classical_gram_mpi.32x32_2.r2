# Sysname : Linux
# Nodename: eu-a6-007-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:08:26 2022
# Execution time and date (local): Wed Jan 12 02:08:26 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 841.927734            1 
            2            32            32   gramschmidt           MPI          1 753.208984            3 
            2            32            32   gramschmidt           MPI          2 729.279297            0 
            2            32            32   gramschmidt           MPI          3 680.582031            0 
            2            32            32   gramschmidt           MPI          4 640.802734            0 
            2            32            32   gramschmidt           MPI          5 647.189453            0 
            2            32            32   gramschmidt           MPI          6 639.638672            0 
            2            32            32   gramschmidt           MPI          7 648.283203            0 
            2            32            32   gramschmidt           MPI          8 629.820312            0 
            2            32            32   gramschmidt           MPI          9 624.751953            0 
# Runtime: 6.999665 s (overhead: 0.000057 %) 10 records
