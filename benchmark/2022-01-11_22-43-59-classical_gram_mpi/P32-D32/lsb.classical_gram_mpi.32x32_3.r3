# Sysname : Linux
# Nodename: eu-a6-007-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:08:26 2022
# Execution time and date (local): Wed Jan 12 02:08:26 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 834.970703            0 
            3            32            32   gramschmidt           MPI          1 748.216797            2 
            3            32            32   gramschmidt           MPI          2 719.458984            0 
            3            32            32   gramschmidt           MPI          3 672.152344            0 
            3            32            32   gramschmidt           MPI          4 636.207031            0 
            3            32            32   gramschmidt           MPI          5 639.861328            0 
            3            32            32   gramschmidt           MPI          6 633.140625            0 
            3            32            32   gramschmidt           MPI          7 639.832031            0 
            3            32            32   gramschmidt           MPI          8 622.306641            0 
            3            32            32   gramschmidt           MPI          9 618.373047            0 
# Runtime: 6.995811 s (overhead: 0.000029 %) 10 records
