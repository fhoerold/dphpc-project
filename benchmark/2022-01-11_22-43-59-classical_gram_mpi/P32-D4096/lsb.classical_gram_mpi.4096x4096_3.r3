# Sysname : Linux
# Nodename: eu-a6-007-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:11:27 2022
# Execution time and date (local): Wed Jan 12 02:11:27 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 16711225.113281            0 
            3          4096          4096   gramschmidt           MPI          1 16282790.150391            6 
            3          4096          4096   gramschmidt           MPI          2 16238874.349609            2 
            3          4096          4096   gramschmidt           MPI          3 16508187.023438            0 
            3          4096          4096   gramschmidt           MPI          4 16546001.060547            3 
            3          4096          4096   gramschmidt           MPI          5 16178726.722656            0 
            3          4096          4096   gramschmidt           MPI          6 16616356.859375            0 
            3          4096          4096   gramschmidt           MPI          7 16622022.267578            0 
            3          4096          4096   gramschmidt           MPI          8 16253218.216797            7 
            3          4096          4096   gramschmidt           MPI          9 16140624.644531            0 
# Runtime: 230.310401 s (overhead: 0.000008 %) 10 records
