# Sysname : Linux
# Nodename: eu-a6-007-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:11:27 2022
# Execution time and date (local): Wed Jan 12 02:11:27 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 16709977.472656            0 
            2          4096          4096   gramschmidt           MPI          1 16281912.335938            6 
            2          4096          4096   gramschmidt           MPI          2 16238050.306641            2 
            2          4096          4096   gramschmidt           MPI          3 16507256.376953            1 
            2          4096          4096   gramschmidt           MPI          4 16544995.824219            7 
            2          4096          4096   gramschmidt           MPI          5 16177751.298828            0 
            2          4096          4096   gramschmidt           MPI          6 16615311.679688            0 
            2          4096          4096   gramschmidt           MPI          7 16621096.761719            0 
            2          4096          4096   gramschmidt           MPI          8 16252392.265625            4 
            2          4096          4096   gramschmidt           MPI          9 16139687.150391            0 
# Runtime: 228.319164 s (overhead: 0.000009 %) 10 records
