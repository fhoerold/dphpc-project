# Sysname : Linux
# Nodename: eu-a6-008-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:45:06 2022
# Execution time and date (local): Tue Jan 11 22:45:06 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          8192          8192   gramschmidt           MPI          0 1547932409.681641            0 
            2          8192          8192   gramschmidt           MPI          1 1519009370.429688            9 
            2          8192          8192   gramschmidt           MPI          2 1308254706.011719            5 
            2          8192          8192   gramschmidt           MPI          3 1534212561.089844            1 
            2          8192          8192   gramschmidt           MPI          4 1528753774.443359            8 
            2          8192          8192   gramschmidt           MPI          5 1537783876.109375            1 
            2          8192          8192   gramschmidt           MPI          6 1524047474.048828            2 
            2          8192          8192   gramschmidt           MPI          7 1540036175.808594            1 
            2          8192          8192   gramschmidt           MPI          8 1518577305.902344            7 
            2          8192          8192   gramschmidt           MPI          9 1522983741.326172            0 
# Runtime: 19800.343312 s (overhead: 0.000000 %) 10 records
