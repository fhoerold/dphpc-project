# Sysname : Linux
# Nodename: eu-a6-008-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:45:06 2022
# Execution time and date (local): Tue Jan 11 22:45:06 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          8192          8192   gramschmidt           MPI          0 1547976125.910156            2 
            3          8192          8192   gramschmidt           MPI          1 1519051738.583984            9 
            3          8192          8192   gramschmidt           MPI          2 1308293984.126953            8 
            3          8192          8192   gramschmidt           MPI          3 1534256629.378906            2 
            3          8192          8192   gramschmidt           MPI          4 1528797762.535156           10 
            3          8192          8192   gramschmidt           MPI          5 1537826131.679688            2 
            3          8192          8192   gramschmidt           MPI          6 1524089579.183594            2 
            3          8192          8192   gramschmidt           MPI          7 1540078532.720703            2 
            3          8192          8192   gramschmidt           MPI          8 1518621260.822266           12 
            3          8192          8192   gramschmidt           MPI          9 1523027136.207031            2 
# Runtime: 19800.507733 s (overhead: 0.000000 %) 10 records
