# Sysname : Linux
# Nodename: eu-a6-008-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:44:36 2022
# Execution time and date (local): Tue Jan 11 22:44:36 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 209.560547            0 
            2            32            32   gramschmidt           MPI          1 220.359375            2 
            2            32            32   gramschmidt           MPI          2 224.880859            0 
            2            32            32   gramschmidt           MPI          3 191.449219            0 
            2            32            32   gramschmidt           MPI          4 169.146484            0 
            2            32            32   gramschmidt           MPI          5 183.246094            0 
            2            32            32   gramschmidt           MPI          6 176.513672            0 
            2            32            32   gramschmidt           MPI          7 165.085938            0 
            2            32            32   gramschmidt           MPI          8 172.712891            0 
            2            32            32   gramschmidt           MPI          9 170.621094            0 
# Runtime: 5.002535 s (overhead: 0.000040 %) 10 records
