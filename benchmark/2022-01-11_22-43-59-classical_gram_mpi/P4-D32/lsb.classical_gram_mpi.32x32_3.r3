# Sysname : Linux
# Nodename: eu-a6-008-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:44:36 2022
# Execution time and date (local): Tue Jan 11 22:44:36 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 202.142578            0 
            3            32            32   gramschmidt           MPI          1 212.341797            2 
            3            32            32   gramschmidt           MPI          2 231.208984            0 
            3            32            32   gramschmidt           MPI          3 187.736328            0 
            3            32            32   gramschmidt           MPI          4 166.017578            0 
            3            32            32   gramschmidt           MPI          5 179.347656            0 
            3            32            32   gramschmidt           MPI          6 172.150391            0 
            3            32            32   gramschmidt           MPI          7 161.058594            0 
            3            32            32   gramschmidt           MPI          8 166.056641            0 
            3            32            32   gramschmidt           MPI          9 167.748047            0 
# Runtime: 3.986875 s (overhead: 0.000050 %) 10 records
