# Sysname : Linux
# Nodename: eu-a6-008-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:44:36 2022
# Execution time and date (local): Tue Jan 11 22:44:36 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 683.533203            0 
            2            64            64   gramschmidt           MPI          1 587.878906            3 
            2            64            64   gramschmidt           MPI          2 600.712891            0 
            2            64            64   gramschmidt           MPI          3 559.167969            0 
            2            64            64   gramschmidt           MPI          4 565.554688            0 
            2            64            64   gramschmidt           MPI          5 594.708984            0 
            2            64            64   gramschmidt           MPI          6 575.751953            0 
            2            64            64   gramschmidt           MPI          7 551.845703            0 
            2            64            64   gramschmidt           MPI          8 547.050781            0 
            2            64            64   gramschmidt           MPI          9 571.732422            0 
# Runtime: 7.010824 s (overhead: 0.000043 %) 10 records
