# Sysname : Linux
# Nodename: eu-a6-008-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:44:36 2022
# Execution time and date (local): Tue Jan 11 22:44:36 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 664.433594            0 
            3            64            64   gramschmidt           MPI          1 584.623047            4 
            3            64            64   gramschmidt           MPI          2 607.832031            0 
            3            64            64   gramschmidt           MPI          3 559.046875            0 
            3            64            64   gramschmidt           MPI          4 565.740234            0 
            3            64            64   gramschmidt           MPI          5 600.775391            0 
            3            64            64   gramschmidt           MPI          6 575.998047            0 
            3            64            64   gramschmidt           MPI          7 554.572266            0 
            3            64            64   gramschmidt           MPI          8 544.421875            0 
            3            64            64   gramschmidt           MPI          9 571.126953            0 
# Runtime: 6.997155 s (overhead: 0.000057 %) 10 records
