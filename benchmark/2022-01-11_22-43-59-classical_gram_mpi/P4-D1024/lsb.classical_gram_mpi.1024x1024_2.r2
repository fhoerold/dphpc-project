# Sysname : Linux
# Nodename: eu-a6-001-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:44:45 2022
# Execution time and date (local): Tue Jan 11 22:44:45 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 1052470.304688            0 
            2          1024          1024   gramschmidt           MPI          1 1055486.652344            5 
            2          1024          1024   gramschmidt           MPI          2 1050724.691406            4 
            2          1024          1024   gramschmidt           MPI          3 1050700.867188            0 
            2          1024          1024   gramschmidt           MPI          4 1048776.558594            2 
            2          1024          1024   gramschmidt           MPI          5 1047761.947266            0 
            2          1024          1024   gramschmidt           MPI          6 1059936.552734            0 
            2          1024          1024   gramschmidt           MPI          7 1115225.037109            0 
            2          1024          1024   gramschmidt           MPI          8 1054573.552734            4 
            2          1024          1024   gramschmidt           MPI          9 1074029.701172            0 
# Runtime: 15.789579 s (overhead: 0.000095 %) 10 records
