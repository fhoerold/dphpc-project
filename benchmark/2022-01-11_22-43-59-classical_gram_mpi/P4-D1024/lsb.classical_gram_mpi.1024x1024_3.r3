# Sysname : Linux
# Nodename: eu-a6-001-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:44:45 2022
# Execution time and date (local): Tue Jan 11 22:44:45 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 1051410.619141            0 
            3          1024          1024   gramschmidt           MPI          1 1054396.376953            5 
            3          1024          1024   gramschmidt           MPI          2 1049857.167969            4 
            3          1024          1024   gramschmidt           MPI          3 1049611.916016            0 
            3          1024          1024   gramschmidt           MPI          4 1047701.992188            2 
            3          1024          1024   gramschmidt           MPI          5 1046433.269531            0 
            3          1024          1024   gramschmidt           MPI          6 1058890.498047            0 
            3          1024          1024   gramschmidt           MPI          7 1114550.441406            0 
            3          1024          1024   gramschmidt           MPI          8 1053625.039062            5 
            3          1024          1024   gramschmidt           MPI          9 1072936.929688            0 
# Runtime: 15.788129 s (overhead: 0.000101 %) 10 records
