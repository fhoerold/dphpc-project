# Sysname : Linux
# Nodename: eu-a6-002-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 23:37:50 2022
# Execution time and date (local): Wed Jan 12 00:37:50 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 325563.802734            0 
            3          1024          1024   gramschmidt           MPI          1 305454.958984            6 
            3          1024          1024   gramschmidt           MPI          2 301678.345703            1 
            3          1024          1024   gramschmidt           MPI          3 306219.708984            0 
            3          1024          1024   gramschmidt           MPI          4 312199.150391            6 
            3          1024          1024   gramschmidt           MPI          5 304485.304688            0 
            3          1024          1024   gramschmidt           MPI          6 301685.814453            0 
            3          1024          1024   gramschmidt           MPI          7 300887.386719            0 
            3          1024          1024   gramschmidt           MPI          8 300539.035156            2 
            3          1024          1024   gramschmidt           MPI          9 304788.240234            0 
# Runtime: 10.020946 s (overhead: 0.000150 %) 10 records
