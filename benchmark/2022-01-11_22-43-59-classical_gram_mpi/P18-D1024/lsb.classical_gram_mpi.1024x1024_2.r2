# Sysname : Linux
# Nodename: eu-a6-002-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 23:37:50 2022
# Execution time and date (local): Wed Jan 12 00:37:50 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 325537.673828            0 
            2          1024          1024   gramschmidt           MPI          1 305350.943359            5 
            2          1024          1024   gramschmidt           MPI          2 301632.009766            2 
            2          1024          1024   gramschmidt           MPI          3 306074.425781            0 
            2          1024          1024   gramschmidt           MPI          4 312087.986328            3 
            2          1024          1024   gramschmidt           MPI          5 304384.134766            0 
            2          1024          1024   gramschmidt           MPI          6 301648.228516            1 
            2          1024          1024   gramschmidt           MPI          7 300814.462891            0 
            2          1024          1024   gramschmidt           MPI          8 300403.552734            2 
            2          1024          1024   gramschmidt           MPI          9 304691.625000            0 
# Runtime: 7.029882 s (overhead: 0.000185 %) 10 records
