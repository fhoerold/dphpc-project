# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:45:06 2022
# Execution time and date (local): Tue Jan 11 22:45:06 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 133870716.439453            0 
            2          4096          4096   gramschmidt           MPI          1 130079228.007812            6 
            2          4096          4096   gramschmidt           MPI          2 120406074.855469            5 
            2          4096          4096   gramschmidt           MPI          3 127490344.849609            2 
            2          4096          4096   gramschmidt           MPI          4 135163273.496094            4 
            2          4096          4096   gramschmidt           MPI          5 125091634.406250            0 
            2          4096          4096   gramschmidt           MPI          6 122122526.587891            0 
            2          4096          4096   gramschmidt           MPI          7 118864434.259766            1 
            2          4096          4096   gramschmidt           MPI          8 124384004.500000            5 
            2          4096          4096   gramschmidt           MPI          9 122593780.371094            0 
# Runtime: 1660.001100 s (overhead: 0.000001 %) 10 records
