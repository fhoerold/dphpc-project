# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:45:06 2022
# Execution time and date (local): Tue Jan 11 22:45:06 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 133865773.820312            2 
            3          4096          4096   gramschmidt           MPI          1 130074319.583984            4 
            3          4096          4096   gramschmidt           MPI          2 120401991.357422            4 
            3          4096          4096   gramschmidt           MPI          3 127486564.033203            2 
            3          4096          4096   gramschmidt           MPI          4 135157824.880859            8 
            3          4096          4096   gramschmidt           MPI          5 125087434.562500            3 
            3          4096          4096   gramschmidt           MPI          6 122118517.980469            0 
            3          4096          4096   gramschmidt           MPI          7 118860374.236328            0 
            3          4096          4096   gramschmidt           MPI          8 124379542.919922            4 
            3          4096          4096   gramschmidt           MPI          9 122589248.640625            0 
# Runtime: 1652.830740 s (overhead: 0.000002 %) 10 records
