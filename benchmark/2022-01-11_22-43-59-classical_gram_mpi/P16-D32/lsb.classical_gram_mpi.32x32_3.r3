# Sysname : Linux
# Nodename: eu-a6-009-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:50:37 2022
# Execution time and date (local): Tue Jan 11 22:50:37 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 466.412109            0 
            3            32            32   gramschmidt           MPI          1 419.099609            2 
            3            32            32   gramschmidt           MPI          2 416.654297            0 
            3            32            32   gramschmidt           MPI          3 448.677734            0 
            3            32            32   gramschmidt           MPI          4 381.314453            0 
            3            32            32   gramschmidt           MPI          5 373.953125            0 
            3            32            32   gramschmidt           MPI          6 409.041016            0 
            3            32            32   gramschmidt           MPI          7 324.226562            0 
            3            32            32   gramschmidt           MPI          8 343.460938            0 
            3            32            32   gramschmidt           MPI          9 358.316406            0 
# Runtime: 4.010200 s (overhead: 0.000050 %) 10 records
