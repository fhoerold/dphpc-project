# Sysname : Linux
# Nodename: eu-a6-009-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:50:37 2022
# Execution time and date (local): Tue Jan 11 22:50:37 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 467.851562            0 
            2            32            32   gramschmidt           MPI          1 421.621094            2 
            2            32            32   gramschmidt           MPI          2 423.322266            0 
            2            32            32   gramschmidt           MPI          3 457.664062            0 
            2            32            32   gramschmidt           MPI          4 385.628906            0 
            2            32            32   gramschmidt           MPI          5 381.369141            0 
            2            32            32   gramschmidt           MPI          6 415.421875            0 
            2            32            32   gramschmidt           MPI          7 331.734375            0 
            2            32            32   gramschmidt           MPI          8 346.207031            0 
            2            32            32   gramschmidt           MPI          9 359.369141            0 
# Runtime: 4.003450 s (overhead: 0.000050 %) 10 records
