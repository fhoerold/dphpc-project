# Sysname : Linux
# Nodename: eu-a6-012-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:45:35 2022
# Execution time and date (local): Tue Jan 11 22:45:35 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 586192.691406            0 
            3          1024          1024   gramschmidt           MPI          1 566634.544922            4 
            3          1024          1024   gramschmidt           MPI          2 577092.439453            1 
            3          1024          1024   gramschmidt           MPI          3 551374.216797            0 
            3          1024          1024   gramschmidt           MPI          4 557379.529297            2 
            3          1024          1024   gramschmidt           MPI          5 562720.169922            0 
            3          1024          1024   gramschmidt           MPI          6 555518.373047            0 
            3          1024          1024   gramschmidt           MPI          7 552785.707031            0 
            3          1024          1024   gramschmidt           MPI          8 570303.755859            2 
            3          1024          1024   gramschmidt           MPI          9 562057.964844            0 
# Runtime: 14.345867 s (overhead: 0.000063 %) 10 records
