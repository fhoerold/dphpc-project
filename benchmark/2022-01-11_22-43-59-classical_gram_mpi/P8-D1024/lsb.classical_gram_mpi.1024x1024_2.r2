# Sysname : Linux
# Nodename: eu-a6-012-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:45:35 2022
# Execution time and date (local): Tue Jan 11 22:45:35 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 585881.966797            0 
            2          1024          1024   gramschmidt           MPI          1 566362.097656            5 
            2          1024          1024   gramschmidt           MPI          2 576808.707031            2 
            2          1024          1024   gramschmidt           MPI          3 551055.996094            0 
            2          1024          1024   gramschmidt           MPI          4 557108.667969            2 
            2          1024          1024   gramschmidt           MPI          5 562449.402344            0 
            2          1024          1024   gramschmidt           MPI          6 555202.126953            0 
            2          1024          1024   gramschmidt           MPI          7 552508.130859            0 
            2          1024          1024   gramschmidt           MPI          8 570015.964844            1 
            2          1024          1024   gramschmidt           MPI          9 561778.589844            0 
# Runtime: 14.344278 s (overhead: 0.000070 %) 10 records
