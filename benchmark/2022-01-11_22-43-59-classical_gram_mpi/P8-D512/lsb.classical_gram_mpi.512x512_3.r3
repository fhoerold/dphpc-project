# Sysname : Linux
# Nodename: eu-a6-008-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:45:19 2022
# Execution time and date (local): Tue Jan 11 22:45:19 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 71510.730469            0 
            3           512           512   gramschmidt           MPI          1 72103.367188            2 
            3           512           512   gramschmidt           MPI          2 72060.617188            2 
            3           512           512   gramschmidt           MPI          3 71622.712891            1 
            3           512           512   gramschmidt           MPI          4 71792.517578            2 
            3           512           512   gramschmidt           MPI          5 71769.320312            0 
            3           512           512   gramschmidt           MPI          6 71578.617188            0 
            3           512           512   gramschmidt           MPI          7 71433.498047            0 
            3           512           512   gramschmidt           MPI          8 76237.796875            4 
            3           512           512   gramschmidt           MPI          9 75669.519531            0 
# Runtime: 0.966085 s (overhead: 0.001139 %) 10 records
