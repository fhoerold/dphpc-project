# Sysname : Linux
# Nodename: eu-a6-008-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:45:19 2022
# Execution time and date (local): Tue Jan 11 22:45:19 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 71427.373047            0 
            2           512           512   gramschmidt           MPI          1 72000.128906            7 
            2           512           512   gramschmidt           MPI          2 71964.464844            2 
            2           512           512   gramschmidt           MPI          3 71541.187500            0 
            2           512           512   gramschmidt           MPI          4 71692.433594            2 
            2           512           512   gramschmidt           MPI          5 71676.527344            0 
            2           512           512   gramschmidt           MPI          6 71490.371094            0 
            2           512           512   gramschmidt           MPI          7 71330.724609            0 
            2           512           512   gramschmidt           MPI          8 76144.812500            2 
            2           512           512   gramschmidt           MPI          9 75570.949219            0 
# Runtime: 1.957966 s (overhead: 0.000664 %) 10 records
