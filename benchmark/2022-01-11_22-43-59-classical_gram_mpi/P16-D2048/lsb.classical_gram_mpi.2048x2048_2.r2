# Sysname : Linux
# Nodename: eu-a6-004-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:52:23 2022
# Execution time and date (local): Tue Jan 11 22:52:23 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 2786212.935547            0 
            2          2048          2048   gramschmidt           MPI          1 2793969.996094            4 
            2          2048          2048   gramschmidt           MPI          2 2839512.859375            5 
            2          2048          2048   gramschmidt           MPI          3 2778246.347656            0 
            2          2048          2048   gramschmidt           MPI          4 2760890.628906            2 
            2          2048          2048   gramschmidt           MPI          5 2779906.238281            0 
            2          2048          2048   gramschmidt           MPI          6 2784755.449219            0 
            2          2048          2048   gramschmidt           MPI          7 2838332.474609            0 
            2          2048          2048   gramschmidt           MPI          8 2776486.214844            6 
            2          2048          2048   gramschmidt           MPI          9 2765144.759766            0 
# Runtime: 42.605822 s (overhead: 0.000040 %) 10 records
