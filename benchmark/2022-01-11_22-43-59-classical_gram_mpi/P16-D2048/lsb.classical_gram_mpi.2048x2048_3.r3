# Sysname : Linux
# Nodename: eu-a6-004-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:52:23 2022
# Execution time and date (local): Tue Jan 11 22:52:23 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 2786454.294922            0 
            3          2048          2048   gramschmidt           MPI          1 2794404.472656            4 
            3          2048          2048   gramschmidt           MPI          2 2839726.585938            2 
            3          2048          2048   gramschmidt           MPI          3 2778369.195312            0 
            3          2048          2048   gramschmidt           MPI          4 2761099.765625            1 
            3          2048          2048   gramschmidt           MPI          5 2780088.353516            0 
            3          2048          2048   gramschmidt           MPI          6 2785175.128906            3 
            3          2048          2048   gramschmidt           MPI          7 2838504.285156            0 
            3          2048          2048   gramschmidt           MPI          8 2776637.330078            5 
            3          2048          2048   gramschmidt           MPI          9 2765327.693359            0 
# Runtime: 44.602342 s (overhead: 0.000034 %) 10 records
