# Sysname : Linux
# Nodename: eu-a6-007-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:09:32 2022
# Execution time and date (local): Wed Jan 12 02:09:32 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 13067.046875            2 
            3           256           256   gramschmidt           MPI          1 15504.035156            4 
            3           256           256   gramschmidt           MPI          2 11636.857422            1 
            3           256           256   gramschmidt           MPI          3 10926.015625            0 
            3           256           256   gramschmidt           MPI          4 10680.003906            1 
            3           256           256   gramschmidt           MPI          5 10769.734375            0 
            3           256           256   gramschmidt           MPI          6 10860.923828            0 
            3           256           256   gramschmidt           MPI          7 10572.421875            0 
            3           256           256   gramschmidt           MPI          8 10519.050781            1 
            3           256           256   gramschmidt           MPI          9 10593.212891            0 
# Runtime: 8.210421 s (overhead: 0.000110 %) 10 records
