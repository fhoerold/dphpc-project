# Sysname : Linux
# Nodename: eu-a6-007-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:09:32 2022
# Execution time and date (local): Wed Jan 12 02:09:32 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 13056.386719            0 
            2           256           256   gramschmidt           MPI          1 15514.265625            4 
            2           256           256   gramschmidt           MPI          2 11635.962891            1 
            2           256           256   gramschmidt           MPI          3 10930.349609            0 
            2           256           256   gramschmidt           MPI          4 10683.750000            1 
            2           256           256   gramschmidt           MPI          5 10774.015625            0 
            2           256           256   gramschmidt           MPI          6 10860.511719            0 
            2           256           256   gramschmidt           MPI          7 10576.121094            0 
            2           256           256   gramschmidt           MPI          8 10505.404297            1 
            2           256           256   gramschmidt           MPI          9 10605.968750            0 
# Runtime: 3.230266 s (overhead: 0.000217 %) 10 records
