# Sysname : Linux
# Nodename: eu-a6-007-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:09:02 2022
# Execution time and date (local): Wed Jan 12 02:09:02 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 3322.869141            0 
            3           128           128   gramschmidt           MPI          1 3102.628906            3 
            3           128           128   gramschmidt           MPI          2 3067.033203            0 
            3           128           128   gramschmidt           MPI          3 3026.564453            0 
            3           128           128   gramschmidt           MPI          4 3023.458984            0 
            3           128           128   gramschmidt           MPI          5 3033.289062            0 
            3           128           128   gramschmidt           MPI          6 3005.148438            0 
            3           128           128   gramschmidt           MPI          7 3066.714844            0 
            3           128           128   gramschmidt           MPI          8 3028.943359            0 
            3           128           128   gramschmidt           MPI          9 2942.458984            0 
# Runtime: 20.041347 s (overhead: 0.000015 %) 10 records
