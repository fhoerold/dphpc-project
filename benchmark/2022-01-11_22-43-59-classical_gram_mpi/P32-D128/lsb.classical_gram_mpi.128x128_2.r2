# Sysname : Linux
# Nodename: eu-a6-007-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:09:02 2022
# Execution time and date (local): Wed Jan 12 02:09:02 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 3321.355469            0 
            2           128           128   gramschmidt           MPI          1 3100.013672            3 
            2           128           128   gramschmidt           MPI          2 3065.933594            0 
            2           128           128   gramschmidt           MPI          3 3023.853516            0 
            2           128           128   gramschmidt           MPI          4 3022.228516            0 
            2           128           128   gramschmidt           MPI          5 3032.835938            0 
            2           128           128   gramschmidt           MPI          6 3002.447266            0 
            2           128           128   gramschmidt           MPI          7 3064.271484            0 
            2           128           128   gramschmidt           MPI          8 3027.880859            0 
            2           128           128   gramschmidt           MPI          9 2941.302734            0 
# Runtime: 20.040309 s (overhead: 0.000015 %) 10 records
