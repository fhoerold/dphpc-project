# Sysname : Linux
# Nodename: eu-a6-008-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:46:04 2022
# Execution time and date (local): Tue Jan 11 22:46:04 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 71677509.330078            0 
            2          4096          4096   gramschmidt           MPI          1 72148080.771484            5 
            2          4096          4096   gramschmidt           MPI          2 73006743.734375            4 
            2          4096          4096   gramschmidt           MPI          3 73574013.304688            0 
            2          4096          4096   gramschmidt           MPI          4 71956225.064453            8 
            2          4096          4096   gramschmidt           MPI          5 70174586.158203            2 
            2          4096          4096   gramschmidt           MPI          6 72823044.289062            2 
            2          4096          4096   gramschmidt           MPI          7 73607434.355469            2 
            2          4096          4096   gramschmidt           MPI          8 67154365.283203            7 
            2          4096          4096   gramschmidt           MPI          9 69912420.339844            0 
# Runtime: 918.405927 s (overhead: 0.000003 %) 10 records
