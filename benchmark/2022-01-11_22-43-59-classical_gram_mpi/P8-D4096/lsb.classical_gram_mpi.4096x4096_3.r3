# Sysname : Linux
# Nodename: eu-a6-008-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:46:04 2022
# Execution time and date (local): Tue Jan 11 22:46:04 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 71696360.113281            0 
            3          4096          4096   gramschmidt           MPI          1 72167386.056641            7 
            3          4096          4096   gramschmidt           MPI          2 73026169.330078            5 
            3          4096          4096   gramschmidt           MPI          3 73593482.572266            0 
            3          4096          4096   gramschmidt           MPI          4 71975440.103516            6 
            3          4096          4096   gramschmidt           MPI          5 70194189.191406            3 
            3          4096          4096   gramschmidt           MPI          6 72843090.798828            3 
            3          4096          4096   gramschmidt           MPI          7 73627795.400391            3 
            3          4096          4096   gramschmidt           MPI          8 67172350.662109            9 
            3          4096          4096   gramschmidt           MPI          9 69931635.734375            3 
# Runtime: 916.622097 s (overhead: 0.000004 %) 10 records
