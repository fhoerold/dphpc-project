# Sysname : Linux
# Nodename: eu-a6-007-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:10:18 2022
# Execution time and date (local): Wed Jan 12 02:10:18 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 398172.712891            0 
            2          1024          1024   gramschmidt           MPI          1 384213.091797            5 
            2          1024          1024   gramschmidt           MPI          2 380274.792969            5 
            2          1024          1024   gramschmidt           MPI          3 377012.146484            1 
            2          1024          1024   gramschmidt           MPI          4 398306.179688            2 
            2          1024          1024   gramschmidt           MPI          5 377101.357422            0 
            2          1024          1024   gramschmidt           MPI          6 375043.095703            0 
            2          1024          1024   gramschmidt           MPI          7 385020.722656            0 
            2          1024          1024   gramschmidt           MPI          8 374354.273438            2 
            2          1024          1024   gramschmidt           MPI          9 371389.832031            0 
# Runtime: 11.159422 s (overhead: 0.000134 %) 10 records
