# Sysname : Linux
# Nodename: eu-a6-007-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:10:18 2022
# Execution time and date (local): Wed Jan 12 02:10:18 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 398453.886719            0 
            3          1024          1024   gramschmidt           MPI          1 384449.464844            5 
            3          1024          1024   gramschmidt           MPI          2 380571.691406            2 
            3          1024          1024   gramschmidt           MPI          3 377403.328125            0 
            3          1024          1024   gramschmidt           MPI          4 398736.205078            2 
            3          1024          1024   gramschmidt           MPI          5 377331.591797            0 
            3          1024          1024   gramschmidt           MPI          6 375424.527344            0 
            3          1024          1024   gramschmidt           MPI          7 385157.335938            0 
            3          1024          1024   gramschmidt           MPI          8 374784.691406            5 
            3          1024          1024   gramschmidt           MPI          9 371970.664062            0 
# Runtime: 15.143142 s (overhead: 0.000092 %) 10 records
