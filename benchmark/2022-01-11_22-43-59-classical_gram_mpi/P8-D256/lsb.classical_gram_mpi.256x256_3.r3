# Sysname : Linux
# Nodename: eu-a6-008-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:45:35 2022
# Execution time and date (local): Tue Jan 11 22:45:35 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 9327.695312            0 
            3           256           256   gramschmidt           MPI          1 9514.908203            4 
            3           256           256   gramschmidt           MPI          2 9975.365234            1 
            3           256           256   gramschmidt           MPI          3 10006.537109            0 
            3           256           256   gramschmidt           MPI          4 10252.267578            2 
            3           256           256   gramschmidt           MPI          5 9330.162109            1 
            3           256           256   gramschmidt           MPI          6 9510.332031            0 
            3           256           256   gramschmidt           MPI          7 9080.699219            0 
            3           256           256   gramschmidt           MPI          8 9219.269531            1 
            3           256           256   gramschmidt           MPI          9 9063.972656            0 
# Runtime: 7.106214 s (overhead: 0.000127 %) 10 records
