# Sysname : Linux
# Nodename: eu-a6-008-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 21:45:35 2022
# Execution time and date (local): Tue Jan 11 22:45:35 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 9350.542969            0 
            2           256           256   gramschmidt           MPI          1 9546.236328            5 
            2           256           256   gramschmidt           MPI          2 10004.675781            1 
            2           256           256   gramschmidt           MPI          3 10044.501953            0 
            2           256           256   gramschmidt           MPI          4 10294.128906            2 
            2           256           256   gramschmidt           MPI          5 9376.863281            0 
            2           256           256   gramschmidt           MPI          6 9556.187500            0 
            2           256           256   gramschmidt           MPI          7 9116.101562            0 
            2           256           256   gramschmidt           MPI          8 9262.521484            1 
            2           256           256   gramschmidt           MPI          9 9111.109375            0 
# Runtime: 1.133500 s (overhead: 0.000794 %) 10 records
