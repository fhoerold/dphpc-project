# Sysname : Linux
# Nodename: eu-a6-007-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:08:44 2022
# Execution time and date (local): Wed Jan 12 02:08:44 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 1564.970703            0 
            3            64            64   gramschmidt           MPI          1 1579.617188            2 
            3            64            64   gramschmidt           MPI          2 1399.146484            0 
            3            64            64   gramschmidt           MPI          3 1345.574219            0 
            3            64            64   gramschmidt           MPI          4 1324.208984            0 
            3            64            64   gramschmidt           MPI          5 1277.798828            0 
            3            64            64   gramschmidt           MPI          6 1292.968750            0 
            3            64            64   gramschmidt           MPI          7 1295.126953            0 
            3            64            64   gramschmidt           MPI          8 1253.880859            0 
            3            64            64   gramschmidt           MPI          9 1265.298828            0 
# Runtime: 3.036507 s (overhead: 0.000066 %) 10 records
