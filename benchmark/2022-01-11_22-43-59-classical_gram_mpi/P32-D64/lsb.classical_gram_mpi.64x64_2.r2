# Sysname : Linux
# Nodename: eu-a6-007-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 01:08:44 2022
# Execution time and date (local): Wed Jan 12 02:08:44 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 1627.173828            0 
            2            64            64   gramschmidt           MPI          1 1615.240234            3 
            2            64            64   gramschmidt           MPI          2 1451.439453            0 
            2            64            64   gramschmidt           MPI          3 1404.429688            0 
            2            64            64   gramschmidt           MPI          4 1352.359375            0 
            2            64            64   gramschmidt           MPI          5 1340.867188            0 
            2            64            64   gramschmidt           MPI          6 1326.744141            0 
            2            64            64   gramschmidt           MPI          7 1330.308594            0 
            2            64            64   gramschmidt           MPI          8 1277.878906            0 
            2            64            64   gramschmidt           MPI          9 1295.699219            0 
# Runtime: 9.049990 s (overhead: 0.000033 %) 10 records
