Invocation: ./benchmark.sh -d 5 7 9 11 13 15 -p 32 36 ./build/gramschmidt_mpi
Benchmark 'gramschmidt_mpi' for
> Dimensions:		32, 128, 512, 2048, 8192, 32768, 
> Number of processors:	32, 36, 
> Use Fullnode:		false
> CPU Model:		XeonGold_6150
> Use MPI:		true

Timestamp: 2021-11-25_10-24-38
