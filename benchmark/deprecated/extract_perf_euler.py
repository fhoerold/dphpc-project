import numpy as np
import pandas as pd
import sys
import os

args = sys.argv

root_dir = args[1]
output_root_path = args[2]
if len(args) >= 4:
    for_roofline = args[3]
else:
    for_roofline = False

times_data = {}
flops_per_c_data = {}

roof_ns = [32, 128, 512, 2048]

for subdir, dirs, files in os.walk(root_dir):
    ver_name = root_dir.split('-')[-1]
    if subdir != root_dir and not "snapshot" in subdir:
        params = subdir.split('/')[1].split('-', 1)
        p = params[0][1:]
        n = params[1][1:]
        n = 2**int(n)

        # print("P=" + p + ", N=" + str(n) + ", ver=" + ver_name)
        for file_name in files:
            if "lsb." in file_name:
                filepath = os.path.join(os.getcwd(), subdir, file_name)
                df = pd.read_csv(filepath, comment='#', sep='\s+')
                size = df["size"][0]
                if size != n:
                    raise os.error("Size is not equal")
                
                times = df["time"]
                mean_time = np.mean(times)

                if not (ver_name, p) in times_data:
                    times_data[(ver_name, p)] = {}

                if not n in times_data[(ver_name, p)] or times_data[(ver_name, p)][n] < mean_time:
                    times_data[(ver_name, p)][n] = mean_time

# print(times_data)

for (ver_name, p) in times_data:
    val = times_data[(ver_name, p)]
    out_file_name = ver_name + "_" + p + "_roof.csv"
    out_file_path = os.path.join(os.getcwd(), output_root_path, out_file_name)
    # print(out_file_path)

    index = sorted(val)
    perf_list = []
    for idx in index:
        n = int(idx)
        W = (n**3) / 3
        maxT = val[idx]
        perf = (W/maxT) / 1000 # W is in [flop], T is in [microseconds] => to get GFLOPS divide by 1000
        perf_list.append(perf)

    if not os.path.exists(out_file_path):
        output_df = pd.DataFrame(perf_list, columns=["perf"])
        output_df.index = index
        output_df.index.name = "n"
    else:
        output_df = pd.read_csv(out_file_path, index_col=0)
        # print(output_df)
        output_df['perf'] = perf_list

    # print(output_df)

    output_df.to_csv(out_file_path)

# dir_list = os.listdir(dir)
# dir_list = sorted(dir_list)
# k = 0
# for filename in dir_list:
#     filepath = os.path.join(dir, filename)
#     df = pd.read_csv(filepath, comment='#', sep='\s+')
#     n = df["size"][0]

#     if not for_roofline or n in roof_ns:
#         # discard first two runs
#         times = df["time"][2:]
#         # print(times)

#         mean_time = np.mean(times)
#         # print("Mean Time = " + str(mean_time))

#         if n in times_data:
#             times_data[n] += mean_time
#         else:
#             times_data[n] = mean_time
#     k += 1

# for n in times_data:
#     if not for_roofline or n in roof_ns:
#         # for now use (n^3)/3, but for more accurate results should count flops
#         flops = (n**3) / 3

#         mean_time = times_data[n]

#         # convert to cycles, clock speed = [GHz], mean_time = [microseconds] ==> factor of 1000
#         cycles = mean_time * CLOCK_SPEED * 1000
#         # print("Cycles = " + str(cycles))

#         # calculate flops/cycle
#         flops_per_c = flops / cycles
#         # print("Flops / Cycle = " + str(flops_per_c))

#         flops_per_c_data[n] = flops_per_c



# output_path = os.path.join(os.getcwd(), output_file_name)
# if os.path.exists(output_path):
#     output_df = pd.read_csv(output_path, index_col=0)

#     index = sorted(flops_per_c_data)
#     flops_per_c_list = []
#     for idx in index:
#         flops_per_c_list.append(flops_per_c_data[idx])

#     output_df['perf'] = flops_per_c_list
# else:
#     output_df = pd.DataFrame.from_dict(flops_per_c_data, orient='index', columns=["perf"])
#     output_df.index.name="n"
#     output_df = output_df.sort_index()

# output_df.to_csv(output_path)
