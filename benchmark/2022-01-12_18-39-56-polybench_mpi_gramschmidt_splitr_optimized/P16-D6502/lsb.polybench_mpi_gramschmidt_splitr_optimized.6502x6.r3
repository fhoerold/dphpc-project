# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 17:40:34 2022
# Execution time and date (local): Wed Jan 12 18:40:34 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         6502          6502   gramschmidt          0 95115768.996094            0 
         6502          6502   gramschmidt          1 92473760.082031            5 
         6502          6502   gramschmidt          2 93169620.029297            6 
         6502          6502   gramschmidt          3 92637286.646484            0 
         6502          6502   gramschmidt          4 92896203.085938            6 
         6502          6502   gramschmidt          5 92700380.501953            0 
         6502          6502   gramschmidt          6 92844396.503906            0 
         6502          6502   gramschmidt          7 93008284.074219            0 
         6502          6502   gramschmidt          8 92901362.460938            5 
         6502          6502   gramschmidt          9 92897946.841797            0 
# Runtime: 930.645079 s (overhead: 0.000002 %) 10 records
