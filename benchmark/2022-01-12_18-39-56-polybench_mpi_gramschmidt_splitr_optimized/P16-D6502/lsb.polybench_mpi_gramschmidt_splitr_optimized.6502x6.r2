# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 17:40:34 2022
# Execution time and date (local): Wed Jan 12 18:40:34 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         6502          6502   gramschmidt          0 99118571.044922            0 
         6502          6502   gramschmidt          1 92476334.269531            6 
         6502          6502   gramschmidt          2 93171957.367188            4 
         6502          6502   gramschmidt          3 92639819.617188            0 
         6502          6502   gramschmidt          4 92898183.708984            7 
         6502          6502   gramschmidt          5 92703080.982422            0 
         6502          6502   gramschmidt          6 92846420.273438            0 
         6502          6502   gramschmidt          7 93010515.367188            0 
         6502          6502   gramschmidt          8 92903771.515625            5 
         6502          6502   gramschmidt          9 92900376.597656            2 
# Runtime: 934.669094 s (overhead: 0.000003 %) 10 records
