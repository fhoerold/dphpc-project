# Sysname : Linux
# Nodename: eu-a6-012-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 11:49:21 2022
# Execution time and date (local): Wed Jan 12 12:49:21 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 234930375.818359            0 
         8192          8192   gramschmidt          1 224271672.503906            6 
         8192          8192   gramschmidt          2 219672733.865234            5 
         8192          8192   gramschmidt          3 215687625.728516            0 
         8192          8192   gramschmidt          4 230993322.394531            5 
         8192          8192   gramschmidt          5 248868668.664062            0 
         8192          8192   gramschmidt          6 245793151.056641            0 
         8192          8192   gramschmidt          7 214417621.908203            0 
         8192          8192   gramschmidt          8 213557664.941406            5 
         8192          8192   gramschmidt          9 210132998.310547            0 
# Runtime: 2258.325900 s (overhead: 0.000001 %) 10 records
