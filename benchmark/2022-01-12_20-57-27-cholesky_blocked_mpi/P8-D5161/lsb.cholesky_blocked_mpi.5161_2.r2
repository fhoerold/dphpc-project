# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 20:13:57 2022
# Execution time and date (local): Wed Jan 12 21:13:57 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          5161            64           MPI          0 5038168.722656            0 
            2          5161            64           MPI          1 4999179.615234            1 
            2          5161            64           MPI          2 5007348.476562            2 
            2          5161            64           MPI          3 4979358.464844            0 
            2          5161            64           MPI          4 4987929.861328            8 
# Runtime: 40.119748 s (overhead: 0.000027 %) 5 records
