# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 20:13:57 2022
# Execution time and date (local): Wed Jan 12 21:13:57 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          5161            64           MPI          0 5037982.628906            0 
            3          5161            64           MPI          1 4998755.791016            3 
            3          5161            64           MPI          2 5007170.677734            2 
            3          5161            64           MPI          3 4979019.101562            0 
            3          5161            64           MPI          4 4987495.617188            2 
# Runtime: 40.120809 s (overhead: 0.000017 %) 5 records
