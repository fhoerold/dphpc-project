# Sysname : Linux
# Nodename: eu-a6-008-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:59:24 2022
# Execution time and date (local): Wed Jan 12 10:59:24 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2896          2896   gramschmidt           MPI          0 25311775.154297            0 
            3          2896          2896   gramschmidt           MPI          1 25680879.781250           10 
            3          2896          2896   gramschmidt           MPI          2 25536848.904297            4 
            3          2896          2896   gramschmidt           MPI          3 25599721.943359            0 
            3          2896          2896   gramschmidt           MPI          4 25365800.968750            4 
            3          2896          2896   gramschmidt           MPI          5 25335916.011719            0 
            3          2896          2896   gramschmidt           MPI          6 25201260.083984            0 
            3          2896          2896   gramschmidt           MPI          7 25335214.203125            0 
            3          2896          2896   gramschmidt           MPI          8 25222901.429688            7 
            3          2896          2896   gramschmidt           MPI          9 25381097.013672            0 
# Runtime: 338.431850 s (overhead: 0.000007 %) 10 records
