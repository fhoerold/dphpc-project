# Sysname : Linux
# Nodename: eu-a6-008-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:59:24 2022
# Execution time and date (local): Wed Jan 12 10:59:24 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2896          2896   gramschmidt           MPI          0 25304339.861328            0 
            2          2896          2896   gramschmidt           MPI          1 25675234.939453            7 
            2          2896          2896   gramschmidt           MPI          2 25529589.515625            3 
            2          2896          2896   gramschmidt           MPI          3 25592370.816406            0 
            2          2896          2896   gramschmidt           MPI          4 25358497.927734            7 
            2          2896          2896   gramschmidt           MPI          5 25328764.949219            2 
            2          2896          2896   gramschmidt           MPI          6 25194140.611328            0 
            2          2896          2896   gramschmidt           MPI          7 25329693.103516            0 
            2          2896          2896   gramschmidt           MPI          8 25215754.199219            5 
            2          2896          2896   gramschmidt           MPI          9 25373865.873047            0 
# Runtime: 330.378280 s (overhead: 0.000007 %) 10 records
