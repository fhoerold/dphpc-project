# Sysname : Linux
# Nodename: eu-a6-001-06
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 09:28:19 2022
# Execution time and date (local): Wed Jan  5 10:28:19 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 3047.949219            0 
            2           128           MPI          1 2409.490234            5 
            2           128           MPI          2 2247.292969            0 
            2           128           MPI          3 1969.171875            0 
            2           128           MPI          4 2148.402344            0 
            2           128           MPI          5 2094.095703            0 
            2           128           MPI          6 5612.681641            0 
            2           128           MPI          7 1736.800781            0 
            2           128           MPI          8 1579.667969            0 
            2           128           MPI          9 1577.513672            0 
# Runtime: 7.004130 s (overhead: 0.000071 %) 10 records
