# Sysname : Linux
# Nodename: eu-a6-001-06
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 09:28:19 2022
# Execution time and date (local): Wed Jan  5 10:28:19 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 3064.294922            0 
            3           128           MPI          1 2412.851562            5 
            3           128           MPI          2 2249.097656            0 
            3           128           MPI          3 1987.269531            0 
            3           128           MPI          4 2148.802734            0 
            3           128           MPI          5 2095.197266            0 
            3           128           MPI          6 5613.537109            0 
            3           128           MPI          7 1734.980469            0 
            3           128           MPI          8 1570.697266            0 
            3           128           MPI          9 1583.349609            0 
# Runtime: 8.002202 s (overhead: 0.000062 %) 10 records
