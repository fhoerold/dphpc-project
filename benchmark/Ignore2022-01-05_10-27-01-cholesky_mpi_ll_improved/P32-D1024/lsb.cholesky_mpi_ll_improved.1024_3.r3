# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 07:05:06 2022
# Execution time and date (local): Thu Jan  6 08:05:06 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 374355.669922            0 
            3          1024           MPI          1 376323.630859            5 
            3          1024           MPI          2 380443.406250            1 
            3          1024           MPI          3 377975.601562            0 
            3          1024           MPI          4 379533.466797            2 
            3          1024           MPI          5 379364.101562            0 
            3          1024           MPI          6 384178.298828            0 
            3          1024           MPI          7 382778.222656            0 
            3          1024           MPI          8 385522.708984            7 
            3          1024           MPI          9 384796.542969            0 
# Runtime: 9.510978 s (overhead: 0.000158 %) 10 records
