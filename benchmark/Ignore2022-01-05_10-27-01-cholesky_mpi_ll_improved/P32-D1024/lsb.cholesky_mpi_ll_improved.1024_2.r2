# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 07:05:06 2022
# Execution time and date (local): Thu Jan  6 08:05:06 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 374388.966797            0 
            2          1024           MPI          1 376327.019531            5 
            2          1024           MPI          2 380446.318359            1 
            2          1024           MPI          3 377981.537109            0 
            2          1024           MPI          4 379536.466797            1 
            2          1024           MPI          5 379366.269531            0 
            2          1024           MPI          6 384179.675781            0 
            2          1024           MPI          7 382787.417969            0 
            2          1024           MPI          8 385530.476562            6 
            2          1024           MPI          9 384801.291016            0 
# Runtime: 9.514680 s (overhead: 0.000137 %) 10 records
