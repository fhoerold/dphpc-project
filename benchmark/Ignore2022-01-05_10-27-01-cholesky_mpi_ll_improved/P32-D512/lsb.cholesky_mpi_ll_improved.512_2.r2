# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 07:04:45 2022
# Execution time and date (local): Thu Jan  6 08:04:45 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 64214.361328            0 
            2           512           MPI          1 60300.308594            4 
            2           512           MPI          2 60620.183594            1 
            2           512           MPI          3 61337.595703            0 
            2           512           MPI          4 59933.958984            1 
            2           512           MPI          5 60606.126953            0 
            2           512           MPI          6 60031.242188            0 
            2           512           MPI          7 60499.791016            0 
            2           512           MPI          8 60651.927734            1 
            2           512           MPI          9 60525.761719            0 
# Runtime: 10.717231 s (overhead: 0.000065 %) 10 records
