# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 07:04:45 2022
# Execution time and date (local): Thu Jan  6 08:04:45 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 64272.507812            0 
            3           512           MPI          1 60369.587891            1 
            3           512           MPI          2 60697.220703            1 
            3           512           MPI          3 61409.199219            0 
            3           512           MPI          4 60005.183594            1 
            3           512           MPI          5 60679.156250            2 
            3           512           MPI          6 60100.167969            0 
            3           512           MPI          7 60581.332031            0 
            3           512           MPI          8 60723.308594            6 
            3           512           MPI          9 60595.197266            0 
# Runtime: 0.733872 s (overhead: 0.001499 %) 10 records
