# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 07:03:23 2022
# Execution time and date (local): Thu Jan  6 08:03:23 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 5242.421875            0 
            2            32           MPI          1 2101.572266            2 
            2            32           MPI          2 2068.564453            2 
            2            32           MPI          3 2088.816406            0 
            2            32           MPI          4 2058.318359            0 
            2            32           MPI          5 2074.960938            0 
            2            32           MPI          6 2063.589844            0 
            2            32           MPI          7 2094.677734            0 
            2            32           MPI          8 2068.082031            0 
            2            32           MPI          9 2097.462891            0 
# Runtime: 15.037463 s (overhead: 0.000027 %) 10 records
