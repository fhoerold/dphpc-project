# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 07:03:23 2022
# Execution time and date (local): Thu Jan  6 08:03:23 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 5384.210938            0 
            3            32           MPI          1 2103.234375            3 
            3            32           MPI          2 2066.507812            0 
            3            32           MPI          3 2086.691406            0 
            3            32           MPI          4 2056.800781            0 
            3            32           MPI          5 2073.263672            0 
            3            32           MPI          6 2061.728516            0 
            3            32           MPI          7 2092.683594            0 
            3            32           MPI          8 2066.833984            0 
            3            32           MPI          9 2096.015625            0 
# Runtime: 14.016261 s (overhead: 0.000021 %) 10 records
