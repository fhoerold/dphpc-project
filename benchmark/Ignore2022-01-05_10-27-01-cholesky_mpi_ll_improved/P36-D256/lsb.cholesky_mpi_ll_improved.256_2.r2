# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:11:49 2022
# Execution time and date (local): Fri Jan  7 08:11:49 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 21718.005859            0 
            2           256           MPI          1 18815.804688            4 
            2           256           MPI          2 17515.218750            0 
            2           256           MPI          3 18274.568359            0 
            2           256           MPI          4 17586.281250            0 
            2           256           MPI          5 17608.453125            0 
            2           256           MPI          6 18231.806641            0 
            2           256           MPI          7 17253.292969            0 
            2           256           MPI          8 17073.927734            0 
            2           256           MPI          9 17121.642578            0 
# Runtime: 14.275991 s (overhead: 0.000028 %) 10 records
