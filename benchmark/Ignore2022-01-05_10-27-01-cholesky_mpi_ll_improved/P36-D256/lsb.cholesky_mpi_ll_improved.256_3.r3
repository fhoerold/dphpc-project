# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:11:49 2022
# Execution time and date (local): Fri Jan  7 08:11:49 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 21708.835938            0 
            3           256           MPI          1 18818.656250            4 
            3           256           MPI          2 17518.664062            0 
            3           256           MPI          3 18276.960938            0 
            3           256           MPI          4 17586.423828            0 
            3           256           MPI          5 17610.273438            0 
            3           256           MPI          6 18232.812500            0 
            3           256           MPI          7 17257.855469            0 
            3           256           MPI          8 17075.806641            0 
            3           256           MPI          9 17124.271484            0 
# Runtime: 10.235488 s (overhead: 0.000039 %) 10 records
