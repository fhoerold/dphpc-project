# Sysname : Linux
# Nodename: eu-a6-008-12
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 09:28:04 2022
# Execution time and date (local): Wed Jan  5 10:28:04 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            64           MPI          0 715.982422            0 
            3            64           MPI          1 625.392578            2 
            3            64           MPI          2 686.076172            0 
            3            64           MPI          3 601.707031            0 
            3            64           MPI          4 583.183594            0 
            3            64           MPI          5 601.218750            0 
            3            64           MPI          6 610.345703            0 
            3            64           MPI          7 629.398438            0 
            3            64           MPI          8 599.925781            0 
            3            64           MPI          9 578.890625            0 
# Runtime: 15.011366 s (overhead: 0.000013 %) 10 records
