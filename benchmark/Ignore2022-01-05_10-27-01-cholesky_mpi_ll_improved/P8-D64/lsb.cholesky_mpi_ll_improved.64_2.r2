# Sysname : Linux
# Nodename: eu-a6-008-12
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 09:28:04 2022
# Execution time and date (local): Wed Jan  5 10:28:04 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            64           MPI          0 732.585938            0 
            2            64           MPI          1 629.943359            0 
            2            64           MPI          2 678.056641            0 
            2            64           MPI          3 601.339844            0 
            2            64           MPI          4 583.253906            0 
            2            64           MPI          5 601.203125            0 
            2            64           MPI          6 618.267578            0 
            2            64           MPI          7 626.324219            0 
            2            64           MPI          8 599.048828            0 
            2            64           MPI          9 578.974609            0 
# Runtime: 0.009520 s (overhead: 0.000000 %) 10 records
