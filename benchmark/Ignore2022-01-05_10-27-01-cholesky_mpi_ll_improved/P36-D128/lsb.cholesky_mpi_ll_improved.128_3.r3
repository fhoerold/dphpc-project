# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:11:32 2022
# Execution time and date (local): Fri Jan  7 08:11:32 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 12511.044922            0 
            3           128           MPI          1 7282.253906            3 
            3           128           MPI          2 8536.603516            0 
            3           128           MPI          3 8018.089844            0 
            3           128           MPI          4 6671.626953            0 
            3           128           MPI          5 7181.859375            0 
            3           128           MPI          6 7431.060547            0 
            3           128           MPI          7 7513.501953            0 
            3           128           MPI          8 6919.964844            0 
            3           128           MPI          9 6655.714844            0 
# Runtime: 7.103108 s (overhead: 0.000042 %) 10 records
