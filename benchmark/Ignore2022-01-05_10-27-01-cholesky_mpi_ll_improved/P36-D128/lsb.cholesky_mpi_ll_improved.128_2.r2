# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:11:32 2022
# Execution time and date (local): Fri Jan  7 08:11:32 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 12197.882812            0 
            2           128           MPI          1 7282.412109            3 
            2           128           MPI          2 8532.296875            0 
            2           128           MPI          3 8015.681641            0 
            2           128           MPI          4 6666.871094            0 
            2           128           MPI          5 7179.808594            0 
            2           128           MPI          6 7424.466797            0 
            2           128           MPI          7 7318.500000            0 
            2           128           MPI          8 6917.814453            1 
            2           128           MPI          9 6651.130859            0 
# Runtime: 10.149087 s (overhead: 0.000039 %) 10 records
