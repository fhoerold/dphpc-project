# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 07:06:14 2022
# Execution time and date (local): Thu Jan  6 08:06:14 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 2672774.787109            0 
            2          2048           MPI          1 2687224.439453            6 
            2          2048           MPI          2 2704780.259766            1 
            2          2048           MPI          3 2693401.785156            0 
            2          2048           MPI          4 2707846.447266            1 
            2          2048           MPI          5 2703883.412109            0 
            2          2048           MPI          6 2710905.671875            0 
            2          2048           MPI          7 2721556.929688            0 
            2          2048           MPI          8 2721697.789062            2 
            2          2048           MPI          9 2721076.406250            0 
# Runtime: 37.964444 s (overhead: 0.000026 %) 10 records
