# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 07:06:14 2022
# Execution time and date (local): Thu Jan  6 08:06:14 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 2672722.779297            0 
            3          2048           MPI          1 2687206.763672            6 
            3          2048           MPI          2 2704760.181641            1 
            3          2048           MPI          3 2693380.927734            0 
            3          2048           MPI          4 2707834.292969            1 
            3          2048           MPI          5 2703866.355469            0 
            3          2048           MPI          6 2710887.687500            0 
            3          2048           MPI          7 2721534.019531            0 
            3          2048           MPI          8 2721679.480469            1 
            3          2048           MPI          9 2721065.957031            0 
# Runtime: 39.965161 s (overhead: 0.000023 %) 10 records
