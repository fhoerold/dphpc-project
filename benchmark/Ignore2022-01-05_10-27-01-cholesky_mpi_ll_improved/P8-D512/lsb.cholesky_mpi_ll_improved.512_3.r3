# Sysname : Linux
# Nodename: eu-a6-006-16
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 09:29:05 2022
# Execution time and date (local): Wed Jan  5 10:29:05 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 29824.363281            0 
            3           512           MPI          1 27156.662109            3 
            3           512           MPI          2 27299.197266            0 
            3           512           MPI          3 27644.980469            0 
            3           512           MPI          4 27953.503906            1 
            3           512           MPI          5 28325.564453            0 
            3           512           MPI          6 28541.542969            0 
            3           512           MPI          7 29106.048828            0 
            3           512           MPI          8 29399.988281            1 
            3           512           MPI          9 29706.169922            0 
# Runtime: 11.268731 s (overhead: 0.000044 %) 10 records
