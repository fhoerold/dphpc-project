# Sysname : Linux
# Nodename: eu-a6-006-16
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 09:29:05 2022
# Execution time and date (local): Wed Jan  5 10:29:05 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 29858.974609            0 
            2           512           MPI          1 27190.144531            4 
            2           512           MPI          2 27331.607422            0 
            2           512           MPI          3 27677.681641            0 
            2           512           MPI          4 27983.699219            1 
            2           512           MPI          5 28358.279297            0 
            2           512           MPI          6 28568.955078            0 
            2           512           MPI          7 29139.449219            0 
            2           512           MPI          8 29434.617188            1 
            2           512           MPI          9 29733.125000            0 
# Runtime: 12.278381 s (overhead: 0.000049 %) 10 records
