# Sysname : Linux
# Nodename: eu-a6-001-06
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 09:28:04 2022
# Execution time and date (local): Wed Jan  5 10:28:04 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 454.527344            0 
            3            32           MPI          1 414.714844            7 
            3            32           MPI          2 431.933594            0 
            3            32           MPI          3 379.121094            0 
            3            32           MPI          4 380.947266            0 
            3            32           MPI          5 281.285156            0 
            3            32           MPI          6 346.615234            0 
            3            32           MPI          7 320.060547            0 
            3            32           MPI          8 291.431641            0 
            3            32           MPI          9 280.923828            0 
# Runtime: 6.997492 s (overhead: 0.000100 %) 10 records
