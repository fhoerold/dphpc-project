# Sysname : Linux
# Nodename: eu-a6-001-06
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 09:28:04 2022
# Execution time and date (local): Wed Jan  5 10:28:04 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 451.056641            0 
            2            32           MPI          1 413.775391            5 
            2            32           MPI          2 430.708984            0 
            2            32           MPI          3 377.210938            0 
            2            32           MPI          4 377.500000            0 
            2            32           MPI          5 281.291016            0 
            2            32           MPI          6 336.232422            0 
            2            32           MPI          7 319.208984            0 
            2            32           MPI          8 291.179688            0 
            2            32           MPI          9 279.205078            0 
# Runtime: 5.025836 s (overhead: 0.000099 %) 10 records
