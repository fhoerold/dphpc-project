# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 09:27:33 2022
# Execution time and date (local): Wed Jan  5 10:27:33 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 4099.201172            0 
            3           256           MPI          1 4117.951172            1 
            3           256           MPI          2 4169.677734            0 
            3           256           MPI          3 4174.863281            0 
            3           256           MPI          4 4273.000000            0 
            3           256           MPI          5 6798.468750            0 
            3           256           MPI          6 4290.044922            0 
            3           256           MPI          7 4304.082031            0 
            3           256           MPI          8 4330.529297            0 
            3           256           MPI          9 4211.500000            0 
# Runtime: 0.055234 s (overhead: 0.001810 %) 10 records
