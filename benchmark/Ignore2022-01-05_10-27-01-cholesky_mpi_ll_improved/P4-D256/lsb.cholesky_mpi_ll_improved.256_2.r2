# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 09:27:33 2022
# Execution time and date (local): Wed Jan  5 10:27:33 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 4125.058594            1 
            2           256           MPI          1 4118.718750            3 
            2           256           MPI          2 4171.617188            0 
            2           256           MPI          3 4175.853516            0 
            2           256           MPI          4 4272.482422            0 
            2           256           MPI          5 6804.218750            0 
            2           256           MPI          6 4291.052734            0 
            2           256           MPI          7 4305.820312            0 
            2           256           MPI          8 4331.833984            0 
            2           256           MPI          9 4213.683594            0 
# Runtime: 1.067109 s (overhead: 0.000375 %) 10 records
