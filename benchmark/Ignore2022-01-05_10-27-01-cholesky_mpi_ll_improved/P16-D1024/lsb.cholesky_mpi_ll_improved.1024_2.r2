# Sysname : Linux
# Nodename: eu-a6-008-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:30:57 2022
# Execution time and date (local): Wed Jan  5 18:30:57 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 371289.218750            0 
            2          1024           MPI          1 373518.615234            8 
            2          1024           MPI          2 373775.142578            1 
            2          1024           MPI          3 378279.904297            0 
            2          1024           MPI          4 377026.250000            2 
            2          1024           MPI          5 375421.103516            0 
            2          1024           MPI          6 379796.500000            0 
            2          1024           MPI          7 373461.656250            0 
            2          1024           MPI          8 375353.195312            2 
            2          1024           MPI          9 376990.634766            0 
# Runtime: 7.647259 s (overhead: 0.000170 %) 10 records
