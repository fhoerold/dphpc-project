# Sysname : Linux
# Nodename: eu-a6-008-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:30:57 2022
# Execution time and date (local): Wed Jan  5 18:30:57 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 371304.359375            0 
            3          1024           MPI          1 373515.292969            4 
            3          1024           MPI          2 373783.898438            1 
            3          1024           MPI          3 378287.230469            0 
            3          1024           MPI          4 377029.291016            1 
            3          1024           MPI          5 375425.228516            0 
            3          1024           MPI          6 379803.101562            0 
            3          1024           MPI          7 373465.001953            0 
            3          1024           MPI          8 375356.253906            1 
            3          1024           MPI          9 376996.900391            0 
# Runtime: 7.642269 s (overhead: 0.000092 %) 10 records
