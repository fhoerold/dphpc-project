# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 09:27:34 2022
# Execution time and date (local): Wed Jan  5 10:27:34 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 32551.894531            0 
            2           512           MPI          1 30121.269531            4 
            2           512           MPI          2 30036.914062            1 
            2           512           MPI          3 30001.599609            0 
            2           512           MPI          4 30285.804688            1 
            2           512           MPI          5 30464.113281            0 
            2           512           MPI          6 30662.482422            0 
            2           512           MPI          7 30843.789062            0 
            2           512           MPI          8 30939.041016            1 
            2           512           MPI          9 30713.740234            0 
# Runtime: 3.375912 s (overhead: 0.000207 %) 10 records
