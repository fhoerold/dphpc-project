# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 09:27:34 2022
# Execution time and date (local): Wed Jan  5 10:27:34 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 32560.751953            0 
            3           512           MPI          1 30124.994141            1 
            3           512           MPI          2 30036.705078            1 
            3           512           MPI          3 30006.617188            0 
            3           512           MPI          4 30288.158203            1 
            3           512           MPI          5 30467.693359            0 
            3           512           MPI          6 30663.529297            0 
            3           512           MPI          7 30848.097656            0 
            3           512           MPI          8 30946.242188            2 
            3           512           MPI          9 30715.531250            0 
# Runtime: 0.372878 s (overhead: 0.001341 %) 10 records
