# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 23:09:51 2022
# Execution time and date (local): Fri Jan  7 00:09:51 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 2880920.505859            0 
            2          2048           MPI          1 2952105.007812            5 
            2          2048           MPI          2 2895361.349609            1 
            2          2048           MPI          3 2946915.535156            0 
            2          2048           MPI          4 2869021.578125            2 
            2          2048           MPI          5 2910628.980469            0 
            2          2048           MPI          6 2868898.316406            0 
            2          2048           MPI          7 2905401.191406            1 
            2          2048           MPI          8 2863739.355469            4 
            2          2048           MPI          9 2892497.156250            0 
# Runtime: 46.313230 s (overhead: 0.000028 %) 10 records
