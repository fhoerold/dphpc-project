# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 23:09:51 2022
# Execution time and date (local): Fri Jan  7 00:09:51 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 2880996.630859            0 
            3          2048           MPI          1 2952108.634766            4 
            3          2048           MPI          2 2895368.927734            1 
            3          2048           MPI          3 2946918.152344            0 
            3          2048           MPI          4 2869031.558594            1 
            3          2048           MPI          5 2910635.314453            0 
            3          2048           MPI          6 2868573.912109            0 
            3          2048           MPI          7 2905409.648438            2 
            3          2048           MPI          8 2863752.210938            4 
            3          2048           MPI          9 2892507.052734            0 
# Runtime: 44.316008 s (overhead: 0.000027 %) 10 records
