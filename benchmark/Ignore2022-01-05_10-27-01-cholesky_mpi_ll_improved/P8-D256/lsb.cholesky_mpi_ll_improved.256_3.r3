# Sysname : Linux
# Nodename: eu-a6-001-06
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 09:29:04 2022
# Execution time and date (local): Wed Jan  5 10:29:04 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 5210.193359            0 
            3           256           MPI          1 7068.113281            6 
            3           256           MPI          2 6706.302734            1 
            3           256           MPI          3 7190.402344            0 
            3           256           MPI          4 7910.351562            0 
            3           256           MPI          5 5009.955078            0 
            3           256           MPI          6 5030.923828            0 
            3           256           MPI          7 5434.037109            0 
            3           256           MPI          8 5223.146484            1 
            3           256           MPI          9 9650.148438            0 
# Runtime: 10.087434 s (overhead: 0.000079 %) 10 records
