# Sysname : Linux
# Nodename: eu-a6-001-06
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 09:29:04 2022
# Execution time and date (local): Wed Jan  5 10:29:04 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 5215.658203            0 
            2           256           MPI          1 7070.798828            6 
            2           256           MPI          2 6708.458984            2 
            2           256           MPI          3 7189.708984            0 
            2           256           MPI          4 7910.162109            1 
            2           256           MPI          5 5010.248047            0 
            2           256           MPI          6 5026.621094            0 
            2           256           MPI          7 5433.710938            0 
            2           256           MPI          8 5223.003906            1 
            2           256           MPI          9 9648.205078            0 
# Runtime: 11.081149 s (overhead: 0.000090 %) 10 records
