# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 23:08:40 2022
# Execution time and date (local): Fri Jan  7 00:08:40 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            64           MPI          0 7744.060547            0 
            2            64           MPI          1 3932.664062            5 
            2            64           MPI          2 3401.423828            1 
            2            64           MPI          3 3332.759766            0 
            2            64           MPI          4 3381.886719            0 
            2            64           MPI          5 3577.371094            0 
            2            64           MPI          6 3138.847656            0 
            2            64           MPI          7 3507.082031            0 
            2            64           MPI          8 3396.169922            0 
            2            64           MPI          9 4124.046875            0 
# Runtime: 11.063313 s (overhead: 0.000054 %) 10 records
