# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 23:08:40 2022
# Execution time and date (local): Fri Jan  7 00:08:40 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            64           MPI          0 7767.121094            0 
            3            64           MPI          1 3930.033203            3 
            3            64           MPI          2 3406.587891            0 
            3            64           MPI          3 3331.603516            0 
            3            64           MPI          4 3378.535156            0 
            3            64           MPI          5 3577.472656            0 
            3            64           MPI          6 3138.996094            0 
            3            64           MPI          7 3507.845703            0 
            3            64           MPI          8 3397.832031            0 
            3            64           MPI          9 4124.523438            0 
# Runtime: 12.045347 s (overhead: 0.000025 %) 10 records
