# Sysname : Linux
# Nodename: eu-a6-008-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:30:29 2022
# Execution time and date (local): Wed Jan  5 18:30:29 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            64           MPI          0 1598.578125            0 
            2            64           MPI          1 1534.048828            0 
            2            64           MPI          2 1535.810547            0 
            2            64           MPI          3 1501.960938            0 
            2            64           MPI          4 1475.414062            0 
            2            64           MPI          5 1699.458984            0 
            2            64           MPI          6 1679.039062            0 
            2            64           MPI          7 1509.712891            0 
            2            64           MPI          8 1463.498047            0 
            2            64           MPI          9 1456.958984            0 
# Runtime: 0.024061 s (overhead: 0.000000 %) 10 records
