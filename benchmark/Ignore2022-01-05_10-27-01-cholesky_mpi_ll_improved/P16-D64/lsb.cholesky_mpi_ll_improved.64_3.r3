# Sysname : Linux
# Nodename: eu-a6-008-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:30:29 2022
# Execution time and date (local): Wed Jan  5 18:30:29 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            64           MPI          0 1599.996094            0 
            3            64           MPI          1 1535.712891            2 
            3            64           MPI          2 1543.863281            0 
            3            64           MPI          3 1508.980469            0 
            3            64           MPI          4 3920.449219            0 
            3            64           MPI          5 1696.646484            0 
            3            64           MPI          6 1677.589844            0 
            3            64           MPI          7 1509.093750            0 
            3            64           MPI          8 1463.765625            0 
            3            64           MPI          9 1456.392578            0 
# Runtime: 17.000467 s (overhead: 0.000012 %) 10 records
