# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 09:27:33 2022
# Execution time and date (local): Wed Jan  5 10:27:33 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 1021.148438            0 
            3           128           MPI          1 1017.886719            2 
            3           128           MPI          2 1016.044922            0 
            3           128           MPI          3 980.650391            0 
            3           128           MPI          4 996.591797            0 
            3           128           MPI          5 997.214844            0 
            3           128           MPI          6 1011.498047            0 
            3           128           MPI          7 1013.050781            0 
            3           128           MPI          8 1002.427734            0 
            3           128           MPI          9 1031.416016            0 
# Runtime: 1.002114 s (overhead: 0.000200 %) 10 records
