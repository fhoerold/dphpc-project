# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 09:27:33 2022
# Execution time and date (local): Wed Jan  5 10:27:33 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 1019.935547            0 
            2           128           MPI          1 1015.515625            0 
            2           128           MPI          2 1018.580078            0 
            2           128           MPI          3 981.097656            0 
            2           128           MPI          4 997.818359            0 
            2           128           MPI          5 997.197266            0 
            2           128           MPI          6 1012.101562            0 
            2           128           MPI          7 1011.310547            0 
            2           128           MPI          8 1003.158203            0 
            2           128           MPI          9 1032.539062            0 
# Runtime: 0.020962 s (overhead: 0.000000 %) 10 records
