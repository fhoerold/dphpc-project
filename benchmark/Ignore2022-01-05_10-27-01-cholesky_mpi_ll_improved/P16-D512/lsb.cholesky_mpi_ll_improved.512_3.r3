# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 18:53:45 2022
# Execution time and date (local): Wed Jan  5 19:53:45 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 64946.980469            0 
            3           512           MPI          1 62582.441406            5 
            3           512           MPI          2 62225.851562            1 
            3           512           MPI          3 64069.158203            0 
            3           512           MPI          4 62570.009766            1 
            3           512           MPI          5 62858.763672            0 
            3           512           MPI          6 62656.564453            0 
            3           512           MPI          7 62842.693359            0 
            3           512           MPI          8 62550.607422            1 
            3           512           MPI          9 62609.968750            0 
# Runtime: 3.785186 s (overhead: 0.000211 %) 10 records
