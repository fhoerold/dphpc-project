# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 18:53:45 2022
# Execution time and date (local): Wed Jan  5 19:53:45 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 64882.519531            0 
            2           512           MPI          1 62521.589844            6 
            2           512           MPI          2 62162.976562            1 
            2           512           MPI          3 64004.861328            0 
            2           512           MPI          4 62507.261719            1 
            2           512           MPI          5 62795.812500            0 
            2           512           MPI          6 62577.769531            0 
            2           512           MPI          7 62762.019531            2 
            2           512           MPI          8 62487.585938            7 
            2           512           MPI          9 62545.785156            0 
# Runtime: 7.700397 s (overhead: 0.000221 %) 10 records
