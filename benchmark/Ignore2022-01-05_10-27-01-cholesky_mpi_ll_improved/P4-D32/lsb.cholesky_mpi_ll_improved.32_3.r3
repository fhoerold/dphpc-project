# Sysname : Linux
# Nodename: eu-a6-008-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 09:27:33 2022
# Execution time and date (local): Wed Jan  5 10:27:33 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 183.441406            0 
            3            32           MPI          1 167.750000            2 
            3            32           MPI          2 177.898438            0 
            3            32           MPI          3 191.378906            0 
            3            32           MPI          4 166.326172            0 
            3            32           MPI          5 168.269531            0 
            3            32           MPI          6 177.570312            0 
            3            32           MPI          7 177.546875            0 
            3            32           MPI          8 176.496094            0 
            3            32           MPI          9 191.458984            0 
# Runtime: 5.996995 s (overhead: 0.000033 %) 10 records
