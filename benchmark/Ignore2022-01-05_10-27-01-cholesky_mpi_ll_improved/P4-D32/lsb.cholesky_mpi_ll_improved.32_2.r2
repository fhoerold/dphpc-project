# Sysname : Linux
# Nodename: eu-a6-008-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 09:27:33 2022
# Execution time and date (local): Wed Jan  5 10:27:33 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 198.765625            0 
            2            32           MPI          1 169.388672            2 
            2            32           MPI          2 177.035156            0 
            2            32           MPI          3 190.437500            0 
            2            32           MPI          4 165.873047            0 
            2            32           MPI          5 167.585938            0 
            2            32           MPI          6 176.941406            0 
            2            32           MPI          7 174.611328            0 
            2            32           MPI          8 175.623047            0 
            2            32           MPI          9 190.623047            0 
# Runtime: 5.972116 s (overhead: 0.000033 %) 10 records
