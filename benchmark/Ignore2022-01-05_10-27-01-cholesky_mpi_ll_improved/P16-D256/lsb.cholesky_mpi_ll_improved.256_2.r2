# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 18:53:20 2022
# Execution time and date (local): Wed Jan  5 19:53:20 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 12373.710938            0 
            2           256           MPI          1 12388.671875            3 
            2           256           MPI          2 15305.556641            1 
            2           256           MPI          3 11875.294922            0 
            2           256           MPI          4 12178.117188            0 
            2           256           MPI          5 11951.443359            0 
            2           256           MPI          6 11746.164062            0 
            2           256           MPI          7 12052.035156            0 
            2           256           MPI          8 11908.867188            0 
            2           256           MPI          9 12205.261719            0 
# Runtime: 14.131107 s (overhead: 0.000028 %) 10 records
