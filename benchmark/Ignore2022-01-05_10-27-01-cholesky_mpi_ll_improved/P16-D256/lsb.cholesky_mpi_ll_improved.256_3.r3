# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 18:53:20 2022
# Execution time and date (local): Wed Jan  5 19:53:20 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 12479.722656            0 
            3           256           MPI          1 12386.208984            3 
            3           256           MPI          2 15305.107422            1 
            3           256           MPI          3 11872.630859            0 
            3           256           MPI          4 12168.337891            0 
            3           256           MPI          5 11950.259766            0 
            3           256           MPI          6 11748.015625            0 
            3           256           MPI          7 12058.738281            0 
            3           256           MPI          8 11908.933594            1 
            3           256           MPI          9 12205.107422            0 
# Runtime: 17.157041 s (overhead: 0.000029 %) 10 records
