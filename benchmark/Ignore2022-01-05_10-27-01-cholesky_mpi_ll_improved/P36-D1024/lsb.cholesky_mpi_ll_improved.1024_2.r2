# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:12:44 2022
# Execution time and date (local): Fri Jan  7 08:12:44 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 431025.271484            0 
            2          1024           MPI          1 427680.875000            5 
            2          1024           MPI          2 419642.162109            1 
            2          1024           MPI          3 412251.951172            0 
            2          1024           MPI          4 415995.326172            1 
            2          1024           MPI          5 427332.177734            0 
            2          1024           MPI          6 418739.691406            0 
            2          1024           MPI          7 410666.300781            0 
            2          1024           MPI          8 409675.380859            2 
            2          1024           MPI          9 420777.812500            0 
# Runtime: 15.027640 s (overhead: 0.000060 %) 10 records
