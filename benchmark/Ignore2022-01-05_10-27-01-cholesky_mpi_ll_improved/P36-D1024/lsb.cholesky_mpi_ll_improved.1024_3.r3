# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:12:44 2022
# Execution time and date (local): Fri Jan  7 08:12:44 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 431053.042969            0 
            3          1024           MPI          1 427663.726562            5 
            3          1024           MPI          2 419632.830078            1 
            3          1024           MPI          3 412248.398438            0 
            3          1024           MPI          4 415995.173828            5 
            3          1024           MPI          5 427310.570312            0 
            3          1024           MPI          6 418719.314453            0 
            3          1024           MPI          7 410653.707031            0 
            3          1024           MPI          8 409671.634766            5 
            3          1024           MPI          9 420766.742188            0 
# Runtime: 11.011111 s (overhead: 0.000145 %) 10 records
