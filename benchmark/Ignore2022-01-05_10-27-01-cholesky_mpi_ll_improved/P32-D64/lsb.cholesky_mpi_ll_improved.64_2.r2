# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 07:03:45 2022
# Execution time and date (local): Thu Jan  6 08:03:45 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            64           MPI          0 2730.943359            0 
            2            64           MPI          1 2816.615234            3 
            2            64           MPI          2 2796.953125            0 
            2            64           MPI          3 2985.060547            0 
            2            64           MPI          4 2398.449219            0 
            2            64           MPI          5 2670.597656            0 
            2            64           MPI          6 2849.791016            0 
            2            64           MPI          7 2424.906250            0 
            2            64           MPI          8 2685.392578            0 
            2            64           MPI          9 2787.185547            0 
# Runtime: 8.045676 s (overhead: 0.000037 %) 10 records
