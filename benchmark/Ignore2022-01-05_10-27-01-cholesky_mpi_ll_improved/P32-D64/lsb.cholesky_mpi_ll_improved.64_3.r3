# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 07:03:45 2022
# Execution time and date (local): Thu Jan  6 08:03:45 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            64           MPI          0 2772.947266            0 
            3            64           MPI          1 2811.318359            0 
            3            64           MPI          2 2797.837891            0 
            3            64           MPI          3 2989.208984            0 
            3            64           MPI          4 2408.222656            0 
            3            64           MPI          5 2676.734375            0 
            3            64           MPI          6 2851.986328            0 
            3            64           MPI          7 2425.437500            0 
            3            64           MPI          8 2686.431641            0 
            3            64           MPI          9 2789.902344            0 
# Runtime: 0.040557 s (overhead: 0.000000 %) 10 records
