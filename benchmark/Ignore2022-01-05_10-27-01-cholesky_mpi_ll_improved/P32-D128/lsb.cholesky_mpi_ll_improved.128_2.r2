# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 07:04:00 2022
# Execution time and date (local): Thu Jan  6 08:04:00 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 6044.882812            0 
            2           128           MPI          1 6078.814453            3 
            2           128           MPI          2 5576.166016            0 
            2           128           MPI          3 5411.228516            0 
            2           128           MPI          4 5271.582031            0 
            2           128           MPI          5 5489.802734            0 
            2           128           MPI          6 6043.455078            0 
            2           128           MPI          7 6886.349609            0 
            2           128           MPI          8 5581.773438            0 
            2           128           MPI          9 5617.218750            0 
# Runtime: 11.070979 s (overhead: 0.000027 %) 10 records
