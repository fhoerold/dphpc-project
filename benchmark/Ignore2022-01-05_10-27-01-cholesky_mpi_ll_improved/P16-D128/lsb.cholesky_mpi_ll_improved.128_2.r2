# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 18:53:06 2022
# Execution time and date (local): Wed Jan  5 19:53:06 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 4192.363281            0 
            2           128           MPI          1 4005.902344            3 
            2           128           MPI          2 3987.242188            0 
            2           128           MPI          3 4086.517578            0 
            2           128           MPI          4 3758.011719            0 
            2           128           MPI          5 4292.958984            0 
            2           128           MPI          6 3845.869141            0 
            2           128           MPI          7 3957.001953            0 
            2           128           MPI          8 3959.769531            0 
            2           128           MPI          9 3704.488281            0 
# Runtime: 6.079908 s (overhead: 0.000049 %) 10 records
