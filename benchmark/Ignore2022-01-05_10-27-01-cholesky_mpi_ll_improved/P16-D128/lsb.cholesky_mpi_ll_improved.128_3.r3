# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 18:53:06 2022
# Execution time and date (local): Wed Jan  5 19:53:06 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 4170.250000            0 
            3           128           MPI          1 4004.058594            3 
            3           128           MPI          2 3987.498047            0 
            3           128           MPI          3 4103.396484            0 
            3           128           MPI          4 3758.041016            0 
            3           128           MPI          5 4266.511719            0 
            3           128           MPI          6 3845.960938            0 
            3           128           MPI          7 3958.132812            0 
            3           128           MPI          8 3959.447266            1 
            3           128           MPI          9 3703.210938            0 
# Runtime: 7.045729 s (overhead: 0.000057 %) 10 records
