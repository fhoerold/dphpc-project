# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 23:08:17 2022
# Execution time and date (local): Fri Jan  7 00:08:17 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 1879.675781            0 
            3            32           MPI          1 2508.582031            3 
            3            32           MPI          2 2745.349609            0 
            3            32           MPI          3 2459.072266            0 
            3            32           MPI          4 2533.279297            0 
            3            32           MPI          5 2514.539062            0 
            3            32           MPI          6 2484.728516            0 
            3            32           MPI          7 2495.718750            0 
            3            32           MPI          8 2489.787109            0 
            3            32           MPI          9 2519.544922            0 
# Runtime: 12.045376 s (overhead: 0.000025 %) 10 records
