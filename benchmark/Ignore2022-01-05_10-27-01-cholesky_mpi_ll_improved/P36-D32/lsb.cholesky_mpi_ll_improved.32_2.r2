# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 23:08:17 2022
# Execution time and date (local): Fri Jan  7 00:08:17 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 1723.205078            0 
            2            32           MPI          1 2555.341797            3 
            2            32           MPI          2 2744.421875            0 
            2            32           MPI          3 2458.189453            0 
            2            32           MPI          4 2530.576172            2 
            2            32           MPI          5 2512.755859            0 
            2            32           MPI          6 2482.986328            0 
            2            32           MPI          7 2494.431641            0 
            2            32           MPI          8 2489.126953            0 
            2            32           MPI          9 2516.726562            0 
# Runtime: 12.040711 s (overhead: 0.000042 %) 10 records
