# Sysname : Linux
# Nodename: eu-a6-008-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 09:27:33 2022
# Execution time and date (local): Wed Jan  5 10:27:33 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            64           MPI          0 442.556641            0 
            2            64           MPI          1 425.068359            0 
            2            64           MPI          2 383.859375            0 
            2            64           MPI          3 411.669922            0 
            2            64           MPI          4 396.580078            0 
            2            64           MPI          5 400.035156            0 
            2            64           MPI          6 382.134766            0 
            2            64           MPI          7 374.560547            0 
            2            64           MPI          8 381.054688            0 
            2            64           MPI          9 412.945312            0 
# Runtime: 0.006370 s (overhead: 0.000000 %) 10 records
