# Sysname : Linux
# Nodename: eu-a6-008-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 09:27:33 2022
# Execution time and date (local): Wed Jan  5 10:27:33 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            64           MPI          0 444.941406            0 
            3            64           MPI          1 424.734375            2 
            3            64           MPI          2 384.265625            0 
            3            64           MPI          3 410.343750            0 
            3            64           MPI          4 396.101562            0 
            3            64           MPI          5 399.783203            0 
            3            64           MPI          6 381.515625            0 
            3            64           MPI          7 374.082031            0 
            3            64           MPI          8 380.488281            0 
            3            64           MPI          9 412.160156            0 
# Runtime: 0.991115 s (overhead: 0.000202 %) 10 records
