# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 07:04:19 2022
# Execution time and date (local): Thu Jan  6 08:04:19 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 15404.265625            0 
            3           256           MPI          1 14827.433594            1 
            3           256           MPI          2 14215.568359            1 
            3           256           MPI          3 14467.218750            0 
            3           256           MPI          4 14329.166016            0 
            3           256           MPI          5 14095.916016            0 
            3           256           MPI          6 15269.011719            0 
            3           256           MPI          7 14164.917969            0 
            3           256           MPI          8 14087.996094            1 
            3           256           MPI          9 13874.781250            0 
# Runtime: 0.182114 s (overhead: 0.001647 %) 10 records
