# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 07:04:19 2022
# Execution time and date (local): Thu Jan  6 08:04:19 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 15435.267578            0 
            2           256           MPI          1 14808.748047            3 
            2           256           MPI          2 14208.906250            0 
            2           256           MPI          3 14465.742188            0 
            2           256           MPI          4 14327.529297            0 
            2           256           MPI          5 14094.351562            0 
            2           256           MPI          6 15261.865234            0 
            2           256           MPI          7 14158.167969            0 
            2           256           MPI          8 14086.699219            0 
            2           256           MPI          9 13873.919922            0 
# Runtime: 17.194935 s (overhead: 0.000017 %) 10 records
