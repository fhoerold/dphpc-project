# Sysname : Linux
# Nodename: eu-a6-008-12
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 09:28:31 2022
# Execution time and date (local): Wed Jan  5 10:28:31 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 218560.224609            0 
            3          1024           MPI          1 226019.630859            4 
            3          1024           MPI          2 233955.423828            1 
            3          1024           MPI          3 239536.783203            0 
            3          1024           MPI          4 236727.435547            1 
            3          1024           MPI          5 238244.962891            0 
            3          1024           MPI          6 238877.988281            0 
            3          1024           MPI          7 243193.970703            0 
            3          1024           MPI          8 240690.828125            2 
            3          1024           MPI          9 241534.082031            0 
# Runtime: 5.764100 s (overhead: 0.000139 %) 10 records
