# Sysname : Linux
# Nodename: eu-a6-008-12
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 09:28:31 2022
# Execution time and date (local): Wed Jan  5 10:28:31 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 218572.210938            0 
            2          1024           MPI          1 226025.238281            1 
            2          1024           MPI          2 233963.953125            1 
            2          1024           MPI          3 239556.687500            0 
            2          1024           MPI          4 236738.613281            1 
            2          1024           MPI          5 238254.830078            0 
            2          1024           MPI          6 238893.392578            0 
            2          1024           MPI          7 243207.269531            3 
            2          1024           MPI          8 240700.244141            4 
            2          1024           MPI          9 241541.175781            0 
# Runtime: 2.779883 s (overhead: 0.000360 %) 10 records
