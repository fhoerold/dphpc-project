# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:12:13 2022
# Execution time and date (local): Fri Jan  7 08:12:13 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 72458.173828            0 
            3           512           MPI          1 67021.966797            7 
            3           512           MPI          2 67597.550781            1 
            3           512           MPI          3 67730.333984            0 
            3           512           MPI          4 67149.861328            2 
            3           512           MPI          5 68649.144531            0 
            3           512           MPI          6 72061.218750            0 
            3           512           MPI          7 67680.535156            0 
            3           512           MPI          8 69877.533203            2 
            3           512           MPI          9 68374.984375            0 
# Runtime: 15.830498 s (overhead: 0.000076 %) 10 records
