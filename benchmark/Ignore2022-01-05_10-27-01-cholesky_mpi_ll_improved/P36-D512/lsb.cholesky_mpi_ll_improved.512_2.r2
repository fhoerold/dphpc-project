# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:12:13 2022
# Execution time and date (local): Fri Jan  7 08:12:13 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 72500.025391            0 
            2           512           MPI          1 67020.037109            4 
            2           512           MPI          2 67594.945312            1 
            2           512           MPI          3 67727.291016            0 
            2           512           MPI          4 67136.583984            1 
            2           512           MPI          5 68644.636719            0 
            2           512           MPI          6 72157.095703            0 
            2           512           MPI          7 67672.890625            0 
            2           512           MPI          8 69872.464844            1 
            2           512           MPI          9 68385.582031            0 
# Runtime: 19.831492 s (overhead: 0.000035 %) 10 records
