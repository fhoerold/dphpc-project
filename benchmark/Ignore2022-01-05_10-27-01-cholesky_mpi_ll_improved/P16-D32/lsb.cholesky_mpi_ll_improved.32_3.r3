# Sysname : Linux
# Nodename: eu-a6-008-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:30:06 2022
# Execution time and date (local): Wed Jan  5 18:30:06 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 818.966797            0 
            3            32           MPI          1 779.580078            2 
            3            32           MPI          2 841.851562            0 
            3            32           MPI          3 768.796875            0 
            3            32           MPI          4 1090.308594            0 
            3            32           MPI          5 873.001953            0 
            3            32           MPI          6 780.480469            0 
            3            32           MPI          7 672.087891            0 
            3            32           MPI          8 755.732422            0 
            3            32           MPI          9 766.767578            0 
# Runtime: 14.991271 s (overhead: 0.000013 %) 10 records
