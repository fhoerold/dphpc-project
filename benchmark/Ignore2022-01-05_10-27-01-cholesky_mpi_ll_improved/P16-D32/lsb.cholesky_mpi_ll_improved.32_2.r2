# Sysname : Linux
# Nodename: eu-a6-008-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:30:07 2022
# Execution time and date (local): Wed Jan  5 18:30:07 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 809.929688            0 
            2            32           MPI          1 782.427734            2 
            2            32           MPI          2 842.810547            0 
            2            32           MPI          3 770.054688            0 
            2            32           MPI          4 1100.345703            0 
            2            32           MPI          5 871.830078            0 
            2            32           MPI          6 778.179688            0 
            2            32           MPI          7 673.179688            0 
            2            32           MPI          8 756.619141            0 
            2            32           MPI          9 765.507812            0 
# Runtime: 16.030461 s (overhead: 0.000012 %) 10 records
