# Sysname : Linux
# Nodename: eu-a6-008-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:51:57 2022
# Execution time and date (local): Sat Jan  8 13:51:57 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 32505960.138672            0 
            2           128           128   gramschmidt           MPI          1 31136337.378906           15 
            2           128           128   gramschmidt           MPI          2 32429951.328125            2 
            2           128           128   gramschmidt           MPI          3 31778206.373047            0 
            2           128           128   gramschmidt           MPI          4 31767992.751953            5 
            2           128           128   gramschmidt           MPI          5 31138248.310547            1 
            2           128           128   gramschmidt           MPI          6 30778216.009766            0 
            2           128           128   gramschmidt           MPI          7 31588010.572266            0 
            2           128           128   gramschmidt           MPI          8 31688091.611328            2 
            2           128           128   gramschmidt           MPI          9 30968027.281250            0 
# Runtime: 414.910763 s (overhead: 0.000006 %) 10 records
