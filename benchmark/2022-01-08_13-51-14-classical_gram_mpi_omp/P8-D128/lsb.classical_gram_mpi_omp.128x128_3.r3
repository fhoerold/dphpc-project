# Sysname : Linux
# Nodename: eu-a6-008-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:51:57 2022
# Execution time and date (local): Sat Jan  8 13:51:57 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 32428080.091797            0 
            3           128           128   gramschmidt           MPI          1 31106235.054688           44 
            3           128           128   gramschmidt           MPI          2 32368105.945312            3 
            3           128           128   gramschmidt           MPI          3 31741545.595703            1 
            3           128           128   gramschmidt           MPI          4 31717115.607422           12 
            3           128           128   gramschmidt           MPI          5 31098151.777344            0 
            3           128           128   gramschmidt           MPI          6 30738174.625000            1 
            3           128           128   gramschmidt           MPI          7 31558124.654297            0 
            3           128           128   gramschmidt           MPI          8 31601471.373047            3 
            3           128           128   gramschmidt           MPI          9 30938202.230469            0 
# Runtime: 414.893572 s (overhead: 0.000015 %) 10 records
