# Sysname : Linux
# Nodename: eu-a6-010-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 02:59:23 2022
# Execution time and date (local): Sun Jan  9 03:59:23 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 413259398.041016            0 
            3          1024          1024   gramschmidt           MPI          1 415749131.748047           18 
            3          1024          1024   gramschmidt           MPI          2 412519398.611328            4 
            3          1024          1024   gramschmidt           MPI          3 411829427.810547            0 
            3          1024          1024   gramschmidt           MPI          4 413229319.058594            5 
            3          1024          1024   gramschmidt           MPI          5 411859443.861328            0 
            3          1024          1024   gramschmidt           MPI          6 411839443.375000            0 
            3          1024          1024   gramschmidt           MPI          7 413039374.125000            0 
            3          1024          1024   gramschmidt           MPI          8 412769299.935547            4 
            3          1024          1024   gramschmidt           MPI          9 413041145.740234            0 
# Runtime: 5367.053760 s (overhead: 0.000001 %) 10 records
