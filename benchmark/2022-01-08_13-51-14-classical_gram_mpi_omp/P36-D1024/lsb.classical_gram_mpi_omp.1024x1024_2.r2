# Sysname : Linux
# Nodename: eu-a6-010-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 02:59:23 2022
# Execution time and date (local): Sun Jan  9 03:59:23 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 413218833.630859            0 
            2          1024          1024   gramschmidt           MPI          1 415718486.929688           10 
            2          1024          1024   gramschmidt           MPI          2 412478816.404297            4 
            2          1024          1024   gramschmidt           MPI          3 411809001.835938            0 
            2          1024          1024   gramschmidt           MPI          4 413188762.455078            4 
            2          1024          1024   gramschmidt           MPI          5 411818905.839844            0 
            2          1024          1024   gramschmidt           MPI          6 411798951.003906            0 
            2          1024          1024   gramschmidt           MPI          7 412988981.220703            0 
            2          1024          1024   gramschmidt           MPI          8 412738746.839844            6 
            2          1024          1024   gramschmidt           MPI          9 413012612.892578            0 
# Runtime: 5366.786852 s (overhead: 0.000000 %) 10 records
