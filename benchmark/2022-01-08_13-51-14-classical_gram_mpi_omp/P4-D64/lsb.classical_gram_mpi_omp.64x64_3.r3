# Sysname : Linux
# Nodename: eu-a6-003-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:51:29 2022
# Execution time and date (local): Sat Jan  8 13:51:29 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 1653.234375            0 
            3            64            64   gramschmidt           MPI          1 1626.648438            0 
            3            64            64   gramschmidt           MPI          2 1666.923828            0 
            3            64            64   gramschmidt           MPI          3 1624.998047            0 
            3            64            64   gramschmidt           MPI          4 1599.099609            0 
            3            64            64   gramschmidt           MPI          5 1605.759766            0 
            3            64            64   gramschmidt           MPI          6 1606.144531            0 
            3            64            64   gramschmidt           MPI          7 1610.068359            0 
            3            64            64   gramschmidt           MPI          8 1614.958984            0 
            3            64            64   gramschmidt           MPI          9 1604.634766            0 
# Runtime: 0.023791 s (overhead: 0.000000 %) 10 records
