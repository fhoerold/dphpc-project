# Sysname : Linux
# Nodename: eu-a6-003-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:51:29 2022
# Execution time and date (local): Sat Jan  8 13:51:29 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 1652.404297            0 
            2            64            64   gramschmidt           MPI          1 1627.511719            2 
            2            64            64   gramschmidt           MPI          2 1661.460938            0 
            2            64            64   gramschmidt           MPI          3 1619.148438            0 
            2            64            64   gramschmidt           MPI          4 1597.638672            0 
            2            64            64   gramschmidt           MPI          5 1606.802734            0 
            2            64            64   gramschmidt           MPI          6 1610.832031            0 
            2            64            64   gramschmidt           MPI          7 1606.019531            0 
            2            64            64   gramschmidt           MPI          8 1610.621094            0 
            2            64            64   gramschmidt           MPI          9 1602.763672            0 
# Runtime: 1.023181 s (overhead: 0.000195 %) 10 records
