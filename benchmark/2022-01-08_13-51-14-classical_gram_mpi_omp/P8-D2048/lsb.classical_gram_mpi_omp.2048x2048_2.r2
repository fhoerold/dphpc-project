# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:53:27 2022
# Execution time and date (local): Sat Jan  8 13:53:27 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 631973537.625000            0 
            2          2048          2048   gramschmidt           MPI          1 630263839.988281            2 
            2          2048          2048   gramschmidt           MPI          2 632893680.880859            3 
            2          2048          2048   gramschmidt           MPI          3 626814052.626953            0 
            2          2048          2048   gramschmidt           MPI          4 626414441.351562            6 
            2          2048          2048   gramschmidt           MPI          5 637632982.972656            0 
            2          2048          2048   gramschmidt           MPI          6 630381290.410156            2 
            2          2048          2048   gramschmidt           MPI          7 633113746.509766            0 
            2          2048          2048   gramschmidt           MPI          8 632183730.830078            3 
            2          2048          2048   gramschmidt           MPI          9 632332630.378906            0 
# Runtime: 8202.780874 s (overhead: 0.000000 %) 10 records
