# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:53:27 2022
# Execution time and date (local): Sat Jan  8 13:53:27 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 631933486.650391            0 
            3          2048          2048   gramschmidt           MPI          1 630243670.548828            3 
            3          2048          2048   gramschmidt           MPI          2 632871019.109375            3 
            3          2048          2048   gramschmidt           MPI          3 626774129.253906            0 
            3          2048          2048   gramschmidt           MPI          4 626404401.757812            4 
            3          2048          2048   gramschmidt           MPI          5 637629689.675781            0 
            3          2048          2048   gramschmidt           MPI          6 630381911.027344            1 
            3          2048          2048   gramschmidt           MPI          7 633073277.958984            0 
            3          2048          2048   gramschmidt           MPI          8 632151183.162109            3 
            3          2048          2048   gramschmidt           MPI          9 632300034.964844            0 
# Runtime: 8207.214975 s (overhead: 0.000000 %) 10 records
