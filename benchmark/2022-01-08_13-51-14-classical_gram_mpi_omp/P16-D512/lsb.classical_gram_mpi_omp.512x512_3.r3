# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 13:31:21 2022
# Execution time and date (local): Sat Jan  8 14:31:21 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 175168833.777344            0 
            3           512           512   gramschmidt           MPI          1 175348824.681641            3 
            3           512           512   gramschmidt           MPI          2 175858791.574219            3 
            3           512           512   gramschmidt           MPI          3 175888808.171875            0 
            3           512           512   gramschmidt           MPI          4 176270777.619141            3 
            3           512           512   gramschmidt           MPI          5 173746934.160156            0 
            3           512           512   gramschmidt           MPI          6 175228852.105469            0 
            3           512           512   gramschmidt           MPI          7 175538830.402344            0 
            3           512           512   gramschmidt           MPI          8 174828856.619141            3 
            3           512           512   gramschmidt           MPI          9 176871372.279297            0 
# Runtime: 2284.008315 s (overhead: 0.000001 %) 10 records
