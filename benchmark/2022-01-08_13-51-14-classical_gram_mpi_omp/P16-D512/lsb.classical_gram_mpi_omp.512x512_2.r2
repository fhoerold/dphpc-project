# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 13:31:21 2022
# Execution time and date (local): Sat Jan  8 14:31:21 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 175129252.500000            0 
            2           512           512   gramschmidt           MPI          1 175339228.244141            3 
            2           512           512   gramschmidt           MPI          2 175859234.748047            3 
            2           512           512   gramschmidt           MPI          3 175859148.859375            0 
            2           512           512   gramschmidt           MPI          4 176239223.232422            3 
            2           512           512   gramschmidt           MPI          5 173729299.218750            0 
            2           512           512   gramschmidt           MPI          6 175229206.494141            0 
            2           512           512   gramschmidt           MPI          7 175549258.289062            0 
            2           512           512   gramschmidt           MPI          8 174809257.224609            4 
            2           512           512   gramschmidt           MPI          9 176841967.037109            1 
# Runtime: 2283.999343 s (overhead: 0.000001 %) 10 records
