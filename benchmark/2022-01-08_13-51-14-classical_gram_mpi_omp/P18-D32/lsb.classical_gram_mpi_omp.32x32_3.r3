# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 14:43:48 2022
# Execution time and date (local): Sat Jan  8 15:43:48 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 10357397.857422            0 
            3            32            32   gramschmidt           MPI          1 10599374.761719           28 
            3            32            32   gramschmidt           MPI          2 9999418.421875            3 
            3            32            32   gramschmidt           MPI          3 10028415.246094            0 
            3            32            32   gramschmidt           MPI          4 10230400.509766            1 
            3            32            32   gramschmidt           MPI          5 10322748.154297            0 
            3            32            32   gramschmidt           MPI          6 10576054.203125            0 
            3            32            32   gramschmidt           MPI          7 10490446.519531            0 
            3            32            32   gramschmidt           MPI          8 10428384.566406            4 
            3            32            32   gramschmidt           MPI          9 10219379.996094            0 
# Runtime: 136.643778 s (overhead: 0.000026 %) 10 records
