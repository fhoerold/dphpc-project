# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 14:43:48 2022
# Execution time and date (local): Sat Jan  8 15:43:48 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 10389398.851562            0 
            2            32            32   gramschmidt           MPI          1 10639425.236328            3 
            2            32            32   gramschmidt           MPI          2 10039401.203125            0 
            2            32            32   gramschmidt           MPI          3 10089448.353516            0 
            2            32            32   gramschmidt           MPI          4 10259385.058594            3 
            2            32            32   gramschmidt           MPI          5 10359392.716797            0 
            2            32            32   gramschmidt           MPI          6 10609378.660156            0 
            2            32            32   gramschmidt           MPI          7 10549374.367188            0 
            2            32            32   gramschmidt           MPI          8 10459393.714844            3 
            2            32            32   gramschmidt           MPI          9 10259425.074219            0 
# Runtime: 136.681649 s (overhead: 0.000007 %) 10 records
