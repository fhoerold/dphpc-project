# Sysname : Linux
# Nodename: eu-a6-005-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 15:59:26 2022
# Execution time and date (local): Sat Jan  8 16:59:26 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 358619596.435547            0 
            2          1024          1024   gramschmidt           MPI          1 358704229.630859            4 
            2          1024          1024   gramschmidt           MPI          2 357794651.275391            6 
            2          1024          1024   gramschmidt           MPI          3 359244543.701172            0 
            2          1024          1024   gramschmidt           MPI          4 359034504.843750            4 
            2          1024          1024   gramschmidt           MPI          5 356980996.648438            0 
            2          1024          1024   gramschmidt           MPI          6 359614634.488281            0 
            2          1024          1024   gramschmidt           MPI          7 355484932.496094            0 
            2          1024          1024   gramschmidt           MPI          8 356135866.882812            6 
            2          1024          1024   gramschmidt           MPI          9 356760077.523438            2 
# Runtime: 4647.918604 s (overhead: 0.000000 %) 10 records
