# Sysname : Linux
# Nodename: eu-a6-005-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 15:59:26 2022
# Execution time and date (local): Sat Jan  8 16:59:26 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 358623919.648438            0 
            3          1024          1024   gramschmidt           MPI          1 358653937.927734            3 
            3          1024          1024   gramschmidt           MPI          2 357823986.003906            3 
            3          1024          1024   gramschmidt           MPI          3 359283861.763672            0 
            3          1024          1024   gramschmidt           MPI          4 359043874.925781            3 
            3          1024          1024   gramschmidt           MPI          5 357004082.148438            0 
            3          1024          1024   gramschmidt           MPI          6 359643996.832031            0 
            3          1024          1024   gramschmidt           MPI          7 355494271.603516            0 
            3          1024          1024   gramschmidt           MPI          8 356164224.173828            4 
            3          1024          1024   gramschmidt           MPI          9 356796071.837891            0 
# Runtime: 4647.914201 s (overhead: 0.000000 %) 10 records
