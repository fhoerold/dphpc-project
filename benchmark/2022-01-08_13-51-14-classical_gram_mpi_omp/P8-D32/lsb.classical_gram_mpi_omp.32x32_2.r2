# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:51:29 2022
# Execution time and date (local): Sat Jan  8 13:51:29 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 7429513.406250            0 
            2            32            32   gramschmidt           MPI          1 7500505.435547            4 
            2            32            32   gramschmidt           MPI          2 7689525.468750            4 
            2            32            32   gramschmidt           MPI          3 7779506.964844            0 
            2            32            32   gramschmidt           MPI          4 7542903.208984            3 
            2            32            32   gramschmidt           MPI          5 7749499.591797            0 
            2            32            32   gramschmidt           MPI          6 7459518.414062            0 
            2            32            32   gramschmidt           MPI          7 7549537.046875            0 
            2            32            32   gramschmidt           MPI          8 7531517.791016            2 
            2            32            32   gramschmidt           MPI          9 7629675.244141            0 
# Runtime: 102.545953 s (overhead: 0.000013 %) 10 records
