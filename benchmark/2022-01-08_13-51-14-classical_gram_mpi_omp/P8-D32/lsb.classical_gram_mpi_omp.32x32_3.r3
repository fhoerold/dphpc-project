# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:51:29 2022
# Execution time and date (local): Sat Jan  8 13:51:29 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 7412932.193359            0 
            3            32            32   gramschmidt           MPI          1 7455987.560547            3 
            3            32            32   gramschmidt           MPI          2 7649532.771484            1 
            3            32            32   gramschmidt           MPI          3 7779520.546875            0 
            3            32            32   gramschmidt           MPI          4 7537559.558594            1 
            3            32            32   gramschmidt           MPI          5 7698543.357422            0 
            3            32            32   gramschmidt           MPI          6 7379560.625000            0 
            3            32            32   gramschmidt           MPI          7 7488529.669922            0 
            3            32            32   gramschmidt           MPI          8 7511527.433594            6 
            3            32            32   gramschmidt           MPI          9 7599524.625000            0 
# Runtime: 102.533346 s (overhead: 0.000011 %) 10 records
