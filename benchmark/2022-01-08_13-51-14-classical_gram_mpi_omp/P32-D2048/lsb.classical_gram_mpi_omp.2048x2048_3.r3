# Sysname : Linux
# Nodename: eu-a6-009-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 22:30:31 2022
# Execution time and date (local): Sat Jan  8 23:30:31 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 838257607.564453            0 
            3          2048          2048   gramschmidt           MPI          1 838517572.460938            9 
            3          2048          2048   gramschmidt           MPI          2 839725775.761719            7 
            3          2048          2048   gramschmidt           MPI          3 834697875.824219            2 
            3          2048          2048   gramschmidt           MPI          4 835947789.738281            6 
            3          2048          2048   gramschmidt           MPI          5 841925624.156250            0 
            3          2048          2048   gramschmidt           MPI          6 840565687.250000            1 
            3          2048          2048   gramschmidt           MPI          7 840167465.392578            1 
            3          2048          2048   gramschmidt           MPI          8 840145785.193359            4 
            3          2048          2048   gramschmidt           MPI          9 838406528.878906            0 
# Runtime: 10905.236271 s (overhead: 0.000000 %) 10 records
