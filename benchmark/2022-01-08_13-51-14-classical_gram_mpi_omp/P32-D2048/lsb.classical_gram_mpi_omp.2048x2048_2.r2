# Sysname : Linux
# Nodename: eu-a6-009-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 22:30:31 2022
# Execution time and date (local): Sat Jan  8 23:30:31 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 838140405.306641            0 
            2          2048          2048   gramschmidt           MPI          1 838410592.578125           12 
            2          2048          2048   gramschmidt           MPI          2 839630279.263672            5 
            2          2048          2048   gramschmidt           MPI          3 834601029.164062            0 
            2          2048          2048   gramschmidt           MPI          4 835821096.011719            4 
            2          2048          2048   gramschmidt           MPI          5 841799936.044922            0 
            2          2048          2048   gramschmidt           MPI          6 840430197.912109            0 
            2          2048          2048   gramschmidt           MPI          7 840070225.414062            0 
            2          2048          2048   gramschmidt           MPI          8 840030275.392578            4 
            2          2048          2048   gramschmidt           MPI          9 838297586.191406            0 
# Runtime: 10904.992005 s (overhead: 0.000000 %) 10 records
