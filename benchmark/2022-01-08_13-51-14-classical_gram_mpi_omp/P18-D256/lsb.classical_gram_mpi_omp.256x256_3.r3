# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 15:00:38 2022
# Execution time and date (local): Sat Jan  8 16:00:38 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 87534435.054688            0 
            3           256           256   gramschmidt           MPI          1 88924311.265625            3 
            3           256           256   gramschmidt           MPI          2 87384423.330078            1 
            3           256           256   gramschmidt           MPI          3 87334369.642578            0 
            3           256           256   gramschmidt           MPI          4 87844392.894531            3 
            3           256           256   gramschmidt           MPI          5 86544560.123047            0 
            3           256           256   gramschmidt           MPI          6 86944492.919922            0 
            3           256           256   gramschmidt           MPI          7 87344485.179688            0 
            3           256           256   gramschmidt           MPI          8 86724419.078125            4 
            3           256           256   gramschmidt           MPI          9 87884422.478516            1 
# Runtime: 1139.416993 s (overhead: 0.000001 %) 10 records
