# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 15:00:38 2022
# Execution time and date (local): Sat Jan  8 16:00:38 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 87644543.753906            0 
            2           256           256   gramschmidt           MPI          1 89034439.164062           10 
            2           256           256   gramschmidt           MPI          2 87454712.447266            4 
            2           256           256   gramschmidt           MPI          3 87444529.617188            0 
            2           256           256   gramschmidt           MPI          4 87794642.648438            4 
            2           256           256   gramschmidt           MPI          5 86662584.257812            0 
            2           256           256   gramschmidt           MPI          6 87024578.916016            0 
            2           256           256   gramschmidt           MPI          7 87394725.308594            0 
            2           256           256   gramschmidt           MPI          8 86774591.914062            3 
            2           256           256   gramschmidt           MPI          9 87934526.511719            0 
# Runtime: 1139.467701 s (overhead: 0.000002 %) 10 records
