# Sysname : Linux
# Nodename: eu-a6-008-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 14:09:51 2022
# Execution time and date (local): Sat Jan  8 15:09:51 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 754179883.517578            0 
            3          2048          2048   gramschmidt           MPI          1 749190649.398438            3 
            3          2048          2048   gramschmidt           MPI          2 744071705.839844            3 
            3          2048          2048   gramschmidt           MPI          3 745109389.433594            0 
            3          2048          2048   gramschmidt           MPI          4 748021109.847656            4 
            3          2048          2048   gramschmidt           MPI          5 746299120.593750            0 
            3          2048          2048   gramschmidt           MPI          6 744059735.447266            0 
            3          2048          2048   gramschmidt           MPI          7 745871678.667969            0 
            3          2048          2048   gramschmidt           MPI          8 746401645.632812            4 
            3          2048          2048   gramschmidt           MPI          9 744551043.720703            0 
# Runtime: 9760.881462 s (overhead: 0.000000 %) 10 records
