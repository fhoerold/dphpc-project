# Sysname : Linux
# Nodename: eu-a6-008-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 14:09:51 2022
# Execution time and date (local): Sat Jan  8 15:09:51 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 754222518.703125            0 
            2          2048          2048   gramschmidt           MPI          1 749183108.677734            3 
            2          2048          2048   gramschmidt           MPI          2 744074206.238281            3 
            2          2048          2048   gramschmidt           MPI          3 745114110.115234            0 
            2          2048          2048   gramschmidt           MPI          4 748013685.556641            4 
            2          2048          2048   gramschmidt           MPI          5 746263950.056641            0 
            2          2048          2048   gramschmidt           MPI          6 744064391.896484            0 
            2          2048          2048   gramschmidt           MPI          7 745864248.003906            0 
            2          2048          2048   gramschmidt           MPI          8 746444127.085938            3 
            2          2048          2048   gramschmidt           MPI          9 744550964.966797            0 
# Runtime: 9762.112299 s (overhead: 0.000000 %) 10 records
