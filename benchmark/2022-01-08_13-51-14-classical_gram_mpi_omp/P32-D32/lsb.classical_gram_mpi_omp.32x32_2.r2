# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 19:33:16 2022
# Execution time and date (local): Sat Jan  8 20:33:16 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 11809348.197266            0 
            2            32            32   gramschmidt           MPI          1 11719374.164062            4 
            2            32            32   gramschmidt           MPI          2 11899335.480469            0 
            2            32            32   gramschmidt           MPI          3 11779357.376953            0 
            2            32            32   gramschmidt           MPI          4 11729354.072266            2 
            2            32            32   gramschmidt           MPI          5 11669349.845703            0 
            2            32            32   gramschmidt           MPI          6 11779356.955078            0 
            2            32            32   gramschmidt           MPI          7 11649366.925781            0 
            2            32            32   gramschmidt           MPI          8 11719371.515625            3 
            2            32            32   gramschmidt           MPI          9 11629366.152344            0 
# Runtime: 153.322263 s (overhead: 0.000006 %) 10 records
