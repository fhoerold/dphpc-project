# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 19:33:16 2022
# Execution time and date (local): Sat Jan  8 20:33:16 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 11769137.255859            0 
            3            32            32   gramschmidt           MPI          1 11669124.269531           20 
            3            32            32   gramschmidt           MPI          2 11879135.347656            0 
            3            32            32   gramschmidt           MPI          3 11713545.232422            0 
            3            32            32   gramschmidt           MPI          4 11689167.628906            3 
            3            32            32   gramschmidt           MPI          5 11629170.328125            0 
            3            32            32   gramschmidt           MPI          6 11769151.392578            0 
            3            32            32   gramschmidt           MPI          7 11619160.490234            0 
            3            32            32   gramschmidt           MPI          8 11709158.494141            1 
            3            32            32   gramschmidt           MPI          9 11599196.273438            0 
# Runtime: 157.324392 s (overhead: 0.000015 %) 10 records
