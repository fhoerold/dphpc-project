# Sysname : Linux
# Nodename: eu-a6-008-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 16:52:48 2022
# Execution time and date (local): Sat Jan  8 17:52:48 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 739610044.007812            0 
            2          2048          2048   gramschmidt           MPI          1 734959524.750000            5 
            2          2048          2048   gramschmidt           MPI          2 735999056.664062            3 
            2          2048          2048   gramschmidt           MPI          3 733939326.017578            0 
            2          2048          2048   gramschmidt           MPI          4 735669094.750000            3 
            2          2048          2048   gramschmidt           MPI          5 733418377.328125            0 
            2          2048          2048   gramschmidt           MPI          6 734620198.941406            0 
            2          2048          2048   gramschmidt           MPI          7 737738818.828125            0 
            2          2048          2048   gramschmidt           MPI          8 732927435.304688            3 
            2          2048          2048   gramschmidt           MPI          9 738461135.974609            0 
# Runtime: 9583.615845 s (overhead: 0.000000 %) 10 records
