# Sysname : Linux
# Nodename: eu-a6-008-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 16:52:48 2022
# Execution time and date (local): Sat Jan  8 17:52:48 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 739553195.345703            0 
            3          2048          2048   gramschmidt           MPI          1 734854183.931641            4 
            3          2048          2048   gramschmidt           MPI          2 735924845.816406            4 
            3          2048          2048   gramschmidt           MPI          3 733905060.062500            0 
            3          2048          2048   gramschmidt           MPI          4 735593660.255859            3 
            3          2048          2048   gramschmidt           MPI          5 733343153.671875            0 
            3          2048          2048   gramschmidt           MPI          6 734574829.056641            0 
            3          2048          2048   gramschmidt           MPI          7 737612184.181641            0 
            3          2048          2048   gramschmidt           MPI          8 732854316.923828            3 
            3          2048          2048   gramschmidt           MPI          9 738397672.109375            0 
# Runtime: 9585.578885 s (overhead: 0.000000 %) 10 records
