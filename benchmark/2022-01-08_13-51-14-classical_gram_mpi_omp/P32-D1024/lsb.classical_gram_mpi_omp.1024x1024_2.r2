# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 20:58:05 2022
# Execution time and date (local): Sat Jan  8 21:58:05 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 409301579.914062            1 
            2          1024          1024   gramschmidt           MPI          1 408472820.861328           20 
            2          1024          1024   gramschmidt           MPI          2 408309489.138672            4 
            2          1024          1024   gramschmidt           MPI          3 409571757.777344            0 
            2          1024          1024   gramschmidt           MPI          4 406661904.849609            5 
            2          1024          1024   gramschmidt           MPI          5 408671707.833984            0 
            2          1024          1024   gramschmidt           MPI          6 407791684.710938            0 
            2          1024          1024   gramschmidt           MPI          7 406911882.322266            0 
            2          1024          1024   gramschmidt           MPI          8 407681832.771484            4 
            2          1024          1024   gramschmidt           MPI          9 408850849.445312            0 
# Runtime: 5308.275320 s (overhead: 0.000001 %) 10 records
