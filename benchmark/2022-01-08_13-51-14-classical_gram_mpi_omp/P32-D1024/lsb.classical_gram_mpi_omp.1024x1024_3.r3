# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 20:58:05 2022
# Execution time and date (local): Sat Jan  8 21:58:05 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 409297831.050781            0 
            3          1024          1024   gramschmidt           MPI          1 408468883.384766            7 
            3          1024          1024   gramschmidt           MPI          2 408296891.046875            3 
            3          1024          1024   gramschmidt           MPI          3 409577778.662109            0 
            3          1024          1024   gramschmidt           MPI          4 406658016.707031            4 
            3          1024          1024   gramschmidt           MPI          5 408657855.488281            0 
            3          1024          1024   gramschmidt           MPI          6 407807935.400391            0 
            3          1024          1024   gramschmidt           MPI          7 406907992.736328            0 
            3          1024          1024   gramschmidt           MPI          8 407677931.878906            4 
            3          1024          1024   gramschmidt           MPI          9 408832754.201172            0 
# Runtime: 5307.228510 s (overhead: 0.000000 %) 10 records
