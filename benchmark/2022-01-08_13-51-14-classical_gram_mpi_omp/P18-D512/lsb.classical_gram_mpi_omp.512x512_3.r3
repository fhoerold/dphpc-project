# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 15:20:18 2022
# Execution time and date (local): Sat Jan  8 16:20:18 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 177429311.955078            0 
            3           512           512   gramschmidt           MPI          1 176750363.501953            9 
            3           512           512   gramschmidt           MPI          2 178368293.500000            6 
            3           512           512   gramschmidt           MPI          3 177419326.818359            0 
            3           512           512   gramschmidt           MPI          4 177569326.697266            4 
            3           512           512   gramschmidt           MPI          5 178369275.572266            0 
            3           512           512   gramschmidt           MPI          6 178209272.728516            0 
            3           512           512   gramschmidt           MPI          7 177709302.816406            0 
            3           512           512   gramschmidt           MPI          8 178039270.962891            6 
            3           512           512   gramschmidt           MPI          9 176471422.628906            0 
# Runtime: 2308.863131 s (overhead: 0.000001 %) 10 records
