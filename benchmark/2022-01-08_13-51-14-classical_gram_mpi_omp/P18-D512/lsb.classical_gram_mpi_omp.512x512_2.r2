# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 15:20:18 2022
# Execution time and date (local): Sat Jan  8 16:20:18 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 177429616.705078            0 
            2           512           512   gramschmidt           MPI          1 176729701.667969            8 
            2           512           512   gramschmidt           MPI          2 178379628.550781            4 
            2           512           512   gramschmidt           MPI          3 177419545.974609            0 
            2           512           512   gramschmidt           MPI          4 177569641.707031            4 
            2           512           512   gramschmidt           MPI          5 178349595.714844            0 
            2           512           512   gramschmidt           MPI          6 178169611.361328            0 
            2           512           512   gramschmidt           MPI          7 177709614.089844            0 
            2           512           512   gramschmidt           MPI          8 177999590.189453            4 
            2           512           512   gramschmidt           MPI          9 176480428.798828            0 
# Runtime: 2308.871623 s (overhead: 0.000001 %) 10 records
