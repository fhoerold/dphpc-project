# Sysname : Linux
# Nodename: eu-a6-008-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:51:29 2022
# Execution time and date (local): Sat Jan  8 13:51:29 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 749.253906            0 
            3            32            32   gramschmidt           MPI          1 736.933594            2 
            3            32            32   gramschmidt           MPI          2 781.898438            0 
            3            32            32   gramschmidt           MPI          3 736.068359            0 
            3            32            32   gramschmidt           MPI          4 750.277344            0 
            3            32            32   gramschmidt           MPI          5 725.455078            0 
            3            32            32   gramschmidt           MPI          6 640.095703            0 
            3            32            32   gramschmidt           MPI          7 727.033203            0 
            3            32            32   gramschmidt           MPI          8 743.648438            0 
            3            32            32   gramschmidt           MPI          9 652.031250            0 
# Runtime: 2.003473 s (overhead: 0.000100 %) 10 records
