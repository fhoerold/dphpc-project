# Sysname : Linux
# Nodename: eu-a6-008-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:51:29 2022
# Execution time and date (local): Sat Jan  8 13:51:29 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 752.458984            0 
            2            32            32   gramschmidt           MPI          1 742.607422           18 
            2            32            32   gramschmidt           MPI          2 788.171875            0 
            2            32            32   gramschmidt           MPI          3 738.048828            0 
            2            32            32   gramschmidt           MPI          4 753.996094            0 
            2            32            32   gramschmidt           MPI          5 727.728516            0 
            2            32            32   gramschmidt           MPI          6 641.968750            0 
            2            32            32   gramschmidt           MPI          7 732.076172            0 
            2            32            32   gramschmidt           MPI          8 746.818359            0 
            2            32            32   gramschmidt           MPI          9 657.048828            0 
# Runtime: 2.003629 s (overhead: 0.000898 %) 10 records
