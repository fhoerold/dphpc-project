# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:51:29 2022
# Execution time and date (local): Sat Jan  8 13:51:29 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 4706.550781            0 
            2           128           128   gramschmidt           MPI          1 4670.505859            7 
            2           128           128   gramschmidt           MPI          2 4736.855469            0 
            2           128           128   gramschmidt           MPI          3 4640.328125            0 
            2           128           128   gramschmidt           MPI          4 4657.996094            0 
            2           128           128   gramschmidt           MPI          5 4664.458984            0 
            2           128           128   gramschmidt           MPI          6 4664.216797            0 
            2           128           128   gramschmidt           MPI          7 4628.007812            0 
            2           128           128   gramschmidt           MPI          8 4727.179688            0 
            2           128           128   gramschmidt           MPI          9 4644.380859            0 
# Runtime: 5.064648 s (overhead: 0.000138 %) 10 records
