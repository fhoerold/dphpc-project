# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:51:29 2022
# Execution time and date (local): Sat Jan  8 13:51:29 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 4686.265625           10 
            3           128           128   gramschmidt           MPI          1 4648.253906           11 
            3           128           128   gramschmidt           MPI          2 4708.433594            1 
            3           128           128   gramschmidt           MPI          3 4621.326172            0 
            3           128           128   gramschmidt           MPI          4 4643.150391            0 
            3           128           128   gramschmidt           MPI          5 4649.941406            0 
            3           128           128   gramschmidt           MPI          6 4637.890625            0 
            3           128           128   gramschmidt           MPI          7 4612.273438            0 
            3           128           128   gramschmidt           MPI          8 4704.927734            1 
            3           128           128   gramschmidt           MPI          9 4621.568359            0 
# Runtime: 2.064340 s (overhead: 0.001114 %) 10 records
