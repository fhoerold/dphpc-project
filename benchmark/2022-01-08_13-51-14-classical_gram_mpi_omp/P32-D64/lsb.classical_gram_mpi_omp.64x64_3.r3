# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 19:36:00 2022
# Execution time and date (local): Sat Jan  8 20:36:00 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 24328313.722656            0 
            3            64            64   gramschmidt           MPI          1 23898337.072266            3 
            3            64            64   gramschmidt           MPI          2 24088304.201172            1 
            3            64            64   gramschmidt           MPI          3 24128257.535156            0 
            3            64            64   gramschmidt           MPI          4 24348296.761719            1 
            3            64            64   gramschmidt           MPI          5 24428299.658203            0 
            3            64            64   gramschmidt           MPI          6 24248300.404297            0 
            3            64            64   gramschmidt           MPI          7 24158303.335938            0 
            3            64            64   gramschmidt           MPI          8 24081431.765625            4 
            3            64            64   gramschmidt           MPI          9 24028323.462891            0 
# Runtime: 318.536905 s (overhead: 0.000003 %) 10 records
