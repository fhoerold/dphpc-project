# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 19:36:00 2022
# Execution time and date (local): Sat Jan  8 20:36:00 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 24297962.679688            1 
            2            64            64   gramschmidt           MPI          1 23917984.566406            5 
            2            64            64   gramschmidt           MPI          2 24036945.076172            1 
            2            64            64   gramschmidt           MPI          3 24147959.035156            0 
            2            64            64   gramschmidt           MPI          4 24327932.449219            1 
            2            64            64   gramschmidt           MPI          5 24457931.939453            0 
            2            64            64   gramschmidt           MPI          6 24217951.050781            0 
            2            64            64   gramschmidt           MPI          7 24227940.652344            0 
            2            64            64   gramschmidt           MPI          8 24107931.753906            3 
            2            64            64   gramschmidt           MPI          9 24027973.564453            0 
# Runtime: 316.492515 s (overhead: 0.000003 %) 10 records
