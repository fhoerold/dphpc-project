# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 20:03:09 2022
# Execution time and date (local): Sat Jan  8 21:03:09 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 201605199.361328            0 
            3           512           512   gramschmidt           MPI          1 201675185.820312            2 
            3           512           512   gramschmidt           MPI          2 200505269.642578            3 
            3           512           512   gramschmidt           MPI          3 201255214.689453            0 
            3           512           512   gramschmidt           MPI          4 200235292.986328            3 
            3           512           512   gramschmidt           MPI          5 201685184.400391            0 
            3           512           512   gramschmidt           MPI          6 201215205.468750            0 
            3           512           512   gramschmidt           MPI          7 200705240.953125            0 
            3           512           512   gramschmidt           MPI          8 199965309.000000            3 
            3           512           512   gramschmidt           MPI          9 201526918.136719            0 
# Runtime: 2612.359639 s (overhead: 0.000000 %) 10 records
