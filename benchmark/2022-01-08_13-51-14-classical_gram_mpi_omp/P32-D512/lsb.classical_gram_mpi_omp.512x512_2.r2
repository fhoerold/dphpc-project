# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 20:03:09 2022
# Execution time and date (local): Sat Jan  8 21:03:09 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 201605821.642578            0 
            2           512           512   gramschmidt           MPI          1 201705816.492188            2 
            2           512           512   gramschmidt           MPI          2 200505815.203125            3 
            2           512           512   gramschmidt           MPI          3 201265838.166016            1 
            2           512           512   gramschmidt           MPI          4 200235864.080078            2 
            2           512           512   gramschmidt           MPI          5 201655813.179688            0 
            2           512           512   gramschmidt           MPI          6 201185849.613281            0 
            2           512           512   gramschmidt           MPI          7 200686898.552734            0 
            2           512           512   gramschmidt           MPI          8 199965934.529297            2 
            2           512           512   gramschmidt           MPI          9 201517896.773438            0 
# Runtime: 2612.384354 s (overhead: 0.000000 %) 10 records
