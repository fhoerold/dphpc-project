# Sysname : Linux
# Nodename: eu-a6-004-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:51:29 2022
# Execution time and date (local): Sat Jan  8 13:51:29 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 18970.384766            0 
            2           256           256   gramschmidt           MPI          1 18610.328125            5 
            2           256           256   gramschmidt           MPI          2 18548.773438            1 
            2           256           256   gramschmidt           MPI          3 18543.136719            0 
            2           256           256   gramschmidt           MPI          4 18459.914062            1 
            2           256           256   gramschmidt           MPI          5 18824.152344            0 
            2           256           256   gramschmidt           MPI          6 18494.498047            0 
            2           256           256   gramschmidt           MPI          7 18459.128906            0 
            2           256           256   gramschmidt           MPI          8 18464.322266            2 
            2           256           256   gramschmidt           MPI          9 18513.363281            0 
# Runtime: 7.249832 s (overhead: 0.000124 %) 10 records
