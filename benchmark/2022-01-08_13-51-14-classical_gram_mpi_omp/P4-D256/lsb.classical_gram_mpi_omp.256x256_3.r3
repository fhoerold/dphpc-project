# Sysname : Linux
# Nodename: eu-a6-004-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:51:29 2022
# Execution time and date (local): Sat Jan  8 13:51:29 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 18322.835938            0 
            3           256           256   gramschmidt           MPI          1 17943.365234            6 
            3           256           256   gramschmidt           MPI          2 17837.375000            1 
            3           256           256   gramschmidt           MPI          3 17847.289062            0 
            3           256           256   gramschmidt           MPI          4 17790.837891            1 
            3           256           256   gramschmidt           MPI          5 18127.613281            0 
            3           256           256   gramschmidt           MPI          6 17824.076172            0 
            3           256           256   gramschmidt           MPI          7 17721.896484            0 
            3           256           256   gramschmidt           MPI          8 17718.646484            1 
            3           256           256   gramschmidt           MPI          9 17811.230469            0 
# Runtime: 7.250079 s (overhead: 0.000124 %) 10 records
