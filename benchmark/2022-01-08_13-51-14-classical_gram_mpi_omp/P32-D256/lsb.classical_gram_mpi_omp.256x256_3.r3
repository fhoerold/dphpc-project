# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 19:41:27 2022
# Execution time and date (local): Sat Jan  8 20:41:27 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 99022921.919922            0 
            3           256           256   gramschmidt           MPI          1 99562941.412109            3 
            3           256           256   gramschmidt           MPI          2 99342859.162109            2 
            3           256           256   gramschmidt           MPI          3 98992931.416016            0 
            3           256           256   gramschmidt           MPI          4 99032874.427734            4 
            3           256           256   gramschmidt           MPI          5 99402921.789062            1 
            3           256           256   gramschmidt           MPI          6 99672895.962891            0 
            3           256           256   gramschmidt           MPI          7 99232863.369141            0 
            3           256           256   gramschmidt           MPI          8 99087942.070312            3 
            3           256           256   gramschmidt           MPI          9 99322915.701172            0 
# Runtime: 1294.665802 s (overhead: 0.000001 %) 10 records
