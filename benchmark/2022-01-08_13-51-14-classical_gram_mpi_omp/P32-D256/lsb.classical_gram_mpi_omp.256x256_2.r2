# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 19:41:27 2022
# Execution time and date (local): Sat Jan  8 20:41:27 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 99050829.281250            1 
            2           256           256   gramschmidt           MPI          1 99650732.611328            4 
            2           256           256   gramschmidt           MPI          2 99300945.785156            3 
            2           256           256   gramschmidt           MPI          3 99020939.619141            2 
            2           256           256   gramschmidt           MPI          4 99000937.937500            4 
            2           256           256   gramschmidt           MPI          5 99431066.949219            0 
            2           256           256   gramschmidt           MPI          6 99770732.244141            0 
            2           256           256   gramschmidt           MPI          7 99340763.041016            0 
            2           256           256   gramschmidt           MPI          8 99190781.664062            3 
            2           256           256   gramschmidt           MPI          9 99350900.933594            0 
# Runtime: 1291.628989 s (overhead: 0.000001 %) 10 records
