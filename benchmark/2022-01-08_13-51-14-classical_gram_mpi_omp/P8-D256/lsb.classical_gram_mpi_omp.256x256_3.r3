# Sysname : Linux
# Nodename: eu-a6-006-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:51:57 2022
# Execution time and date (local): Sat Jan  8 13:51:57 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 64566161.386719            3 
            3           256           256   gramschmidt           MPI          1 63346237.123047            4 
            3           256           256   gramschmidt           MPI          2 65096075.542969            5 
            3           256           256   gramschmidt           MPI          3 65006016.201172            0 
            3           256           256   gramschmidt           MPI          4 64866107.718750            1 
            3           256           256   gramschmidt           MPI          5 65756106.201172            0 
            3           256           256   gramschmidt           MPI          6 64986111.691406            0 
            3           256           256   gramschmidt           MPI          7 64648091.294922            0 
            3           256           256   gramschmidt           MPI          8 63886128.060547            4 
            3           256           256   gramschmidt           MPI          9 65006096.304688            0 
# Runtime: 841.148899 s (overhead: 0.000002 %) 10 records
