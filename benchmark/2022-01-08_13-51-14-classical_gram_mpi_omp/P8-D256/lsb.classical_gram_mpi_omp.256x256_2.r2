# Sysname : Linux
# Nodename: eu-a6-006-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:51:57 2022
# Execution time and date (local): Sat Jan  8 13:51:57 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 64666163.511719            0 
            2           256           256   gramschmidt           MPI          1 63426248.867188            3 
            2           256           256   gramschmidt           MPI          2 65166302.398438            4 
            2           256           256   gramschmidt           MPI          3 65166118.650391            0 
            2           256           256   gramschmidt           MPI          4 64884280.443359            1 
            2           256           256   gramschmidt           MPI          5 65806111.341797            1 
            2           256           256   gramschmidt           MPI          6 65084100.167969            0 
            2           256           256   gramschmidt           MPI          7 64726298.062500            0 
            2           256           256   gramschmidt           MPI          8 64016187.996094            3 
            2           256           256   gramschmidt           MPI          9 65046148.398438            0 
# Runtime: 840.188900 s (overhead: 0.000001 %) 10 records
