# Sysname : Linux
# Nodename: eu-a6-010-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 02:36:56 2022
# Execution time and date (local): Sun Jan  9 03:36:56 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 99832456.792969            1 
            3           256           256   gramschmidt           MPI          1 100012473.910156            5 
            3           256           256   gramschmidt           MPI          2 100982419.181641            3 
            3           256           256   gramschmidt           MPI          3 100172432.941406            0 
            3           256           256   gramschmidt           MPI          4 99372489.166016            4 
            3           256           256   gramschmidt           MPI          5 100432460.015625            0 
            3           256           256   gramschmidt           MPI          6 100922393.126953            0 
            3           256           256   gramschmidt           MPI          7 101191321.109375            0 
            3           256           256   gramschmidt           MPI          8 100222407.515625            3 
            3           256           256   gramschmidt           MPI          9 101542365.289062            0 
# Runtime: 1312.850295 s (overhead: 0.000001 %) 10 records
