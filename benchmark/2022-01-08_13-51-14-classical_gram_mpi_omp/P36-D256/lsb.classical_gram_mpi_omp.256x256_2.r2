# Sysname : Linux
# Nodename: eu-a6-010-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 02:36:56 2022
# Execution time and date (local): Sun Jan  9 03:36:56 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 99873007.156250            0 
            2           256           256   gramschmidt           MPI          1 100132946.115234            6 
            2           256           256   gramschmidt           MPI          2 101013044.169922            3 
            2           256           256   gramschmidt           MPI          3 100213065.146484            0 
            2           256           256   gramschmidt           MPI          4 99413083.972656            2 
            2           256           256   gramschmidt           MPI          5 100562939.544922            0 
            2           256           256   gramschmidt           MPI          6 101012909.716797            0 
            2           256           256   gramschmidt           MPI          7 101153085.505859            0 
            2           256           256   gramschmidt           MPI          8 100183081.414062            3 
            2           256           256   gramschmidt           MPI          9 101622847.992188            0 
# Runtime: 1313.926142 s (overhead: 0.000001 %) 10 records
