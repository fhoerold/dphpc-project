# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:59:28 2022
# Execution time and date (local): Sat Jan  8 13:59:28 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 10259379.681641            0 
            3            32            32   gramschmidt           MPI          1 10386491.208984            4 
            3            32            32   gramschmidt           MPI          2 10089394.416016            1 
            3            32            32   gramschmidt           MPI          3 10579365.578125            0 
            3            32            32   gramschmidt           MPI          4 10262192.466797            3 
            3            32            32   gramschmidt           MPI          5 10386402.369141            0 
            3            32            32   gramschmidt           MPI          6 10436450.888672            0 
            3            32            32   gramschmidt           MPI          7 10089396.140625            0 
            3            32            32   gramschmidt           MPI          8 10137388.880859            4 
            3            32            32   gramschmidt           MPI          9 9889413.335938            0 
# Runtime: 134.971573 s (overhead: 0.000009 %) 10 records
