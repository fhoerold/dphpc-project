# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:59:28 2022
# Execution time and date (local): Sat Jan  8 13:59:28 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 10259369.017578            0 
            2            32            32   gramschmidt           MPI          1 10419380.093750            4 
            2            32            32   gramschmidt           MPI          2 10109378.283203            1 
            2            32            32   gramschmidt           MPI          3 10619367.884766            0 
            2            32            32   gramschmidt           MPI          4 10339389.583984            3 
            2            32            32   gramschmidt           MPI          5 10419354.650391            0 
            2            32            32   gramschmidt           MPI          6 10469398.146484            0 
            2            32            32   gramschmidt           MPI          7 10169372.873047            0 
            2            32            32   gramschmidt           MPI          8 10179386.910156            3 
            2            32            32   gramschmidt           MPI          9 9929408.582031            0 
# Runtime: 137.020050 s (overhead: 0.000008 %) 10 records
