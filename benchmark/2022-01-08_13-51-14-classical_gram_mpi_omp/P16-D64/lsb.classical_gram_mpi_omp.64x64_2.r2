# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 13:20:00 2022
# Execution time and date (local): Sat Jan  8 14:20:00 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 20888807.953125            0 
            2            64            64   gramschmidt           MPI          1 21758754.390625           15 
            2            64            64   gramschmidt           MPI          2 21408732.453125            1 
            2            64            64   gramschmidt           MPI          3 21768689.212891            0 
            2            64            64   gramschmidt           MPI          4 21548728.361328            1 
            2            64            64   gramschmidt           MPI          5 21007801.871094            0 
            2            64            64   gramschmidt           MPI          6 21298747.080078            0 
            2            64            64   gramschmidt           MPI          7 21438741.888672            0 
            2            64            64   gramschmidt           MPI          8 21508741.787109            4 
            2            64            64   gramschmidt           MPI          9 21179028.826172            0 
# Runtime: 279.363679 s (overhead: 0.000008 %) 10 records
