# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 13:20:00 2022
# Execution time and date (local): Sat Jan  8 14:20:00 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 20847798.316406            0 
            3            64            64   gramschmidt           MPI          1 21748723.917969           11 
            3            64            64   gramschmidt           MPI          2 21298785.195312            4 
            3            64            64   gramschmidt           MPI          3 21721808.728516            0 
            3            64            64   gramschmidt           MPI          4 21497425.554688            1 
            3            64            64   gramschmidt           MPI          5 21048778.574219            0 
            3            64            64   gramschmidt           MPI          6 21238788.402344            0 
            3            64            64   gramschmidt           MPI          7 21388767.238281            0 
            3            64            64   gramschmidt           MPI          8 21478767.763672            4 
            3            64            64   gramschmidt           MPI          9 21168762.095703            2 
# Runtime: 279.360118 s (overhead: 0.000008 %) 10 records
