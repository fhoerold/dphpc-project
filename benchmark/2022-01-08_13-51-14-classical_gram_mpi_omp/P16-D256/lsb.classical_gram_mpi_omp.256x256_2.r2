# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 13:11:18 2022
# Execution time and date (local): Sat Jan  8 14:11:18 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 87654689.250000            0 
            2           256           256   gramschmidt           MPI          1 87514644.666016            3 
            2           256           256   gramschmidt           MPI          2 86644849.693359            3 
            2           256           256   gramschmidt           MPI          3 87374674.785156            0 
            2           256           256   gramschmidt           MPI          4 87434776.761719            4 
            2           256           256   gramschmidt           MPI          5 86684979.986328            0 
            2           256           256   gramschmidt           MPI          6 87684661.218750            0 
            2           256           256   gramschmidt           MPI          7 86944696.998047            0 
            2           256           256   gramschmidt           MPI          8 86934700.439453            3 
            2           256           256   gramschmidt           MPI          9 86504753.699219            0 
# Runtime: 1136.420512 s (overhead: 0.000001 %) 10 records
