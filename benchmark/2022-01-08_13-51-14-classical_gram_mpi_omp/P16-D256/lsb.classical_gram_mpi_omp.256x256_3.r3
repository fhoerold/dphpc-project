# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 13:11:18 2022
# Execution time and date (local): Sat Jan  8 14:11:18 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 87608828.978516            0 
            3           256           256   gramschmidt           MPI          1 87418829.078125            3 
            3           256           256   gramschmidt           MPI          2 86678868.728516            4 
            3           256           256   gramschmidt           MPI          3 87248859.824219            0 
            3           256           256   gramschmidt           MPI          4 87468779.687500            3 
            3           256           256   gramschmidt           MPI          5 86638955.277344            0 
            3           256           256   gramschmidt           MPI          6 87588829.427734            0 
            3           256           256   gramschmidt           MPI          7 86868919.429688            0 
            3           256           256   gramschmidt           MPI          8 86808904.027344            5 
            3           256           256   gramschmidt           MPI          9 86396543.917969            0 
# Runtime: 1137.284206 s (overhead: 0.000001 %) 10 records
