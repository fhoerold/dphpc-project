# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 14:46:13 2022
# Execution time and date (local): Sat Jan  8 15:46:13 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 21308805.955078            0 
            2            64            64   gramschmidt           MPI          1 21358810.941406           14 
            2            64            64   gramschmidt           MPI          2 20890854.150391            4 
            2            64            64   gramschmidt           MPI          3 20487805.773438            0 
            2            64            64   gramschmidt           MPI          4 21038827.150391            3 
            2            64            64   gramschmidt           MPI          5 21397780.779297            0 
            2            64            64   gramschmidt           MPI          6 21408794.933594            0 
            2            64            64   gramschmidt           MPI          7 21198798.746094            0 
            2            64            64   gramschmidt           MPI          8 21208819.658203            3 
            2            64            64   gramschmidt           MPI          9 21258809.906250            0 
# Runtime: 278.503574 s (overhead: 0.000009 %) 10 records
