# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 14:46:13 2022
# Execution time and date (local): Sat Jan  8 15:46:13 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 21298764.769531            0 
            3            64            64   gramschmidt           MPI          1 21398785.824219            9 
            3            64            64   gramschmidt           MPI          2 20855465.960938            1 
            3            64            64   gramschmidt           MPI          3 20438817.203125            0 
            3            64            64   gramschmidt           MPI          4 21047785.818359            1 
            3            64            64   gramschmidt           MPI          5 21388791.533203            1 
            3            64            64   gramschmidt           MPI          6 21438758.146484            0 
            3            64            64   gramschmidt           MPI          7 21168783.025391            0 
            3            64            64   gramschmidt           MPI          8 21238794.974609            6 
            3            64            64   gramschmidt           MPI          9 21238779.980469            0 
# Runtime: 276.453431 s (overhead: 0.000007 %) 10 records
