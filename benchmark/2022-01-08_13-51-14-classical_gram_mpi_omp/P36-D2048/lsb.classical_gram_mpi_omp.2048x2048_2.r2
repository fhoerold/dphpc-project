# Sysname : Linux
# Nodename: eu-a6-012-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 04:30:49 2022
# Execution time and date (local): Sun Jan  9 05:30:49 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 844725310.384766            0 
            2          2048          2048   gramschmidt           MPI          1 843724472.484375           14 
            2          2048          2048   gramschmidt           MPI          2 848148112.636719            4 
            2          2048          2048   gramschmidt           MPI          3 845418528.093750            0 
            2          2048          2048   gramschmidt           MPI          4 846216369.589844            4 
            2          2048          2048   gramschmidt           MPI          5 845964702.662109            2 
            2          2048          2048   gramschmidt           MPI          6 846304645.707031            2 
            2          2048          2048   gramschmidt           MPI          7 847474484.982422            0 
            2          2048          2048   gramschmidt           MPI          8 844824538.867188            5 
            2          2048          2048   gramschmidt           MPI          9 845791518.890625            0 
# Runtime: 10993.376811 s (overhead: 0.000000 %) 10 records
