# Sysname : Linux
# Nodename: eu-a6-012-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 04:30:49 2022
# Execution time and date (local): Sun Jan  9 05:30:49 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 844716594.951172            0 
            3          2048          2048   gramschmidt           MPI          1 843716761.142578            4 
            3          2048          2048   gramschmidt           MPI          2 848156632.853516            3 
            3          2048          2048   gramschmidt           MPI          3 845446735.710938            0 
            3          2048          2048   gramschmidt           MPI          4 846304299.980469            4 
            3          2048          2048   gramschmidt           MPI          5 845976962.058594            0 
            3          2048          2048   gramschmidt           MPI          6 846354578.496094            0 
            3          2048          2048   gramschmidt           MPI          7 847466790.884766            0 
            3          2048          2048   gramschmidt           MPI          8 844833749.218750            4 
            3          2048          2048   gramschmidt           MPI          9 845796251.326172            0 
# Runtime: 10994.569540 s (overhead: 0.000000 %) 10 records
