# Sysname : Linux
# Nodename: eu-a6-004-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:51:29 2022
# Execution time and date (local): Sat Jan  8 13:51:29 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 742553.046875            0 
            3          1024          1024   gramschmidt           MPI          1 601672.755859            4 
            3          1024          1024   gramschmidt           MPI          2 594095.005859            2 
            3          1024          1024   gramschmidt           MPI          3 722629.246094            0 
            3          1024          1024   gramschmidt           MPI          4 776033.574219            2 
            3          1024          1024   gramschmidt           MPI          5 766612.189453            1 
            3          1024          1024   gramschmidt           MPI          6 767709.052734            0 
            3          1024          1024   gramschmidt           MPI          7 787372.513672            0 
            3          1024          1024   gramschmidt           MPI          8 817230.117188            5 
            3          1024          1024   gramschmidt           MPI          9 785464.410156            0 
# Runtime: 16.644698 s (overhead: 0.000084 %) 10 records
