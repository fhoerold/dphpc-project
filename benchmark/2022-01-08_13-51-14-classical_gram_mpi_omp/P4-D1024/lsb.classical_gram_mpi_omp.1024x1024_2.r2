# Sysname : Linux
# Nodename: eu-a6-004-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:51:29 2022
# Execution time and date (local): Sat Jan  8 13:51:29 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 742947.980469            0 
            2          1024          1024   gramschmidt           MPI          1 602374.423828            4 
            2          1024          1024   gramschmidt           MPI          2 594777.998047            1 
            2          1024          1024   gramschmidt           MPI          3 723878.101562            0 
            2          1024          1024   gramschmidt           MPI          4 776818.761719            4 
            2          1024          1024   gramschmidt           MPI          5 767226.097656            0 
            2          1024          1024   gramschmidt           MPI          6 768130.005859            0 
            2          1024          1024   gramschmidt           MPI          7 787949.757812            0 
            2          1024          1024   gramschmidt           MPI          8 817870.839844            2 
            2          1024          1024   gramschmidt           MPI          9 785888.113281            0 
# Runtime: 9.643905 s (overhead: 0.000114 %) 10 records
