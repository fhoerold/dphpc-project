# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 13:24:49 2022
# Execution time and date (local): Sat Jan  8 14:24:49 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 361099601.878906            0 
            2          1024          1024   gramschmidt           MPI          1 362609595.267578           11 
            2          1024          1024   gramschmidt           MPI          2 361829607.867188            4 
            2          1024          1024   gramschmidt           MPI          3 362989538.933594            2 
            2          1024          1024   gramschmidt           MPI          4 359909740.582031            5 
            2          1024          1024   gramschmidt           MPI          5 362299595.441406            0 
            2          1024          1024   gramschmidt           MPI          6 362179623.720703            0 
            2          1024          1024   gramschmidt           MPI          7 360599735.937500            0 
            2          1024          1024   gramschmidt           MPI          8 365199399.089844            4 
            2          1024          1024   gramschmidt           MPI          9 363013365.861328            0 
# Runtime: 4712.001998 s (overhead: 0.000001 %) 10 records
