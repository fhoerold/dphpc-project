# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 13:24:49 2022
# Execution time and date (local): Sat Jan  8 14:24:49 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 361118320.521484            0 
            3          1024          1024   gramschmidt           MPI          1 362588259.519531           18 
            3          1024          1024   gramschmidt           MPI          2 361879273.443359            4 
            3          1024          1024   gramschmidt           MPI          3 363017204.568359            0 
            3          1024          1024   gramschmidt           MPI          4 359928388.662109            6 
            3          1024          1024   gramschmidt           MPI          5 362328279.708984            1 
            3          1024          1024   gramschmidt           MPI          6 362218227.517578            1 
            3          1024          1024   gramschmidt           MPI          7 360608369.226562            1 
            3          1024          1024   gramschmidt           MPI          8 365218101.574219            5 
            3          1024          1024   gramschmidt           MPI          9 363040213.673828            1 
# Runtime: 4709.979146 s (overhead: 0.000001 %) 10 records
