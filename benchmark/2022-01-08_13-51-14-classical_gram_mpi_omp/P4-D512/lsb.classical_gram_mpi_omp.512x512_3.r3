# Sysname : Linux
# Nodename: eu-a6-005-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:51:29 2022
# Execution time and date (local): Sat Jan  8 13:51:29 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 81510.478516            0 
            3           512           512   gramschmidt           MPI          1 81824.136719            4 
            3           512           512   gramschmidt           MPI          2 79049.597656            0 
            3           512           512   gramschmidt           MPI          3 81841.464844            0 
            3           512           512   gramschmidt           MPI          4 81713.417969            1 
            3           512           512   gramschmidt           MPI          5 77425.951172            0 
            3           512           512   gramschmidt           MPI          6 77697.859375            0 
            3           512           512   gramschmidt           MPI          7 77959.255859            0 
            3           512           512   gramschmidt           MPI          8 81037.263672            0 
            3           512           512   gramschmidt           MPI          9 76525.503906            0 
# Runtime: 3.077790 s (overhead: 0.000162 %) 10 records
