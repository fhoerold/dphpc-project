# Sysname : Linux
# Nodename: eu-a6-005-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:51:29 2022
# Execution time and date (local): Sat Jan  8 13:51:29 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 81617.355469            0 
            2           512           512   gramschmidt           MPI          1 81930.960938            9 
            2           512           512   gramschmidt           MPI          2 79160.048828            0 
            2           512           512   gramschmidt           MPI          3 81951.216797            0 
            2           512           512   gramschmidt           MPI          4 81817.322266            0 
            2           512           512   gramschmidt           MPI          5 77531.498047            0 
            2           512           512   gramschmidt           MPI          6 77804.820312            0 
            2           512           512   gramschmidt           MPI          7 78068.458984            0 
            2           512           512   gramschmidt           MPI          8 81095.720703            0 
            2           512           512   gramschmidt           MPI          9 76652.351562            0 
# Runtime: 2.077702 s (overhead: 0.000433 %) 10 records
