# Sysname : Linux
# Nodename: eu-a6-010-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 01:35:47 2022
# Execution time and date (local): Sun Jan  9 02:35:47 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 202495438.259766            0 
            2           512           512   gramschmidt           MPI          1 203855302.652344            3 
            2           512           512   gramschmidt           MPI          2 203405312.542969            3 
            2           512           512   gramschmidt           MPI          3 202631320.568359            0 
            2           512           512   gramschmidt           MPI          4 203455251.955078            3 
            2           512           512   gramschmidt           MPI          5 204135263.769531            0 
            2           512           512   gramschmidt           MPI          6 203095333.623047            0 
            2           512           512   gramschmidt           MPI          7 202485361.472656            0 
            2           512           512   gramschmidt           MPI          8 203505310.326172            2 
            2           512           512   gramschmidt           MPI          9 203657479.267578            0 
# Runtime: 2648.263037 s (overhead: 0.000000 %) 10 records
