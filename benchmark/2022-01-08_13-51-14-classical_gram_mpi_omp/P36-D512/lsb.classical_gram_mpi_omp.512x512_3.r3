# Sysname : Linux
# Nodename: eu-a6-010-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 01:35:47 2022
# Execution time and date (local): Sun Jan  9 02:35:47 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 202498679.126953            1 
            3           512           512   gramschmidt           MPI          1 203858568.335938           10 
            3           512           512   gramschmidt           MPI          2 203408605.138672            3 
            3           512           512   gramschmidt           MPI          3 202688633.714844            0 
            3           512           512   gramschmidt           MPI          4 203488578.593750            3 
            3           512           512   gramschmidt           MPI          5 204138534.453125            0 
            3           512           512   gramschmidt           MPI          6 203128610.738281            0 
            3           512           512   gramschmidt           MPI          7 202518633.414062            0 
            3           512           512   gramschmidt           MPI          8 203548575.855469            3 
            3           512           512   gramschmidt           MPI          9 203650358.533203            0 
# Runtime: 2643.183666 s (overhead: 0.000001 %) 10 records
