# Sysname : Linux
# Nodename: eu-a6-006-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:52:59 2022
# Execution time and date (local): Sat Jan  8 13:52:59 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 272974646.298828            0 
            3          1024          1024   gramschmidt           MPI          1 270184804.429688            3 
            3          1024          1024   gramschmidt           MPI          2 265875043.535156            3 
            3          1024          1024   gramschmidt           MPI          3 263925149.093750            0 
            3          1024          1024   gramschmidt           MPI          4 254765667.767578            3 
            3          1024          1024   gramschmidt           MPI          5 263265193.699219            0 
            3          1024          1024   gramschmidt           MPI          6 263995154.742188            0 
            3          1024          1024   gramschmidt           MPI          7 263255197.255859            0 
            3          1024          1024   gramschmidt           MPI          8 271114746.410156            4 
            3          1024          1024   gramschmidt           MPI          9 262911384.091797            0 
# Runtime: 3450.683964 s (overhead: 0.000000 %) 10 records
