# Sysname : Linux
# Nodename: eu-a6-006-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:52:59 2022
# Execution time and date (local): Sat Jan  8 13:52:59 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 272970707.128906            0 
            2          1024          1024   gramschmidt           MPI          1 270180994.039062            6 
            2          1024          1024   gramschmidt           MPI          2 265881363.906250            4 
            2          1024          1024   gramschmidt           MPI          3 263951624.343750            1 
            2          1024          1024   gramschmidt           MPI          4 254742337.367188            5 
            2          1024          1024   gramschmidt           MPI          5 263261689.816406            0 
            2          1024          1024   gramschmidt           MPI          6 263991649.769531            0 
            2          1024          1024   gramschmidt           MPI          7 263241654.082031            0 
            2          1024          1024   gramschmidt           MPI          8 271090983.419922            4 
            2          1024          1024   gramschmidt           MPI          9 262876335.166016            0 
# Runtime: 3453.630443 s (overhead: 0.000001 %) 10 records
