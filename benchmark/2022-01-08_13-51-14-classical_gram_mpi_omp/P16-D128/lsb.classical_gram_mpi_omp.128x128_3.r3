# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 13:01:52 2022
# Execution time and date (local): Sat Jan  8 14:01:52 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 43257478.267578            0 
            3           128           128   gramschmidt           MPI          1 42647549.402344            4 
            3           128           128   gramschmidt           MPI          2 43057518.082031            4 
            3           128           128   gramschmidt           MPI          3 42237523.873047            0 
            3           128           128   gramschmidt           MPI          4 42677517.425781            3 
            3           128           128   gramschmidt           MPI          5 43037470.476562            0 
            3           128           128   gramschmidt           MPI          6 41797548.589844            0 
            3           128           128   gramschmidt           MPI          7 43717481.945312            0 
            3           128           128   gramschmidt           MPI          8 42220315.695312            5 
            3           128           128   gramschmidt           MPI          9 42783172.308594            0 
# Runtime: 559.030665 s (overhead: 0.000003 %) 10 records
