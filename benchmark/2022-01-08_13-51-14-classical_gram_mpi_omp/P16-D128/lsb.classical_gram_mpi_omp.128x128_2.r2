# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 13:01:52 2022
# Execution time and date (local): Sat Jan  8 14:01:52 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 43287532.517578            0 
            2           128           128   gramschmidt           MPI          1 42657664.585938            3 
            2           128           128   gramschmidt           MPI          2 43077422.287109            3 
            2           128           128   gramschmidt           MPI          3 42287677.369141            0 
            2           128           128   gramschmidt           MPI          4 42727666.226562            4 
            2           128           128   gramschmidt           MPI          5 43067533.228516            0 
            2           128           128   gramschmidt           MPI          6 41847478.853516            0 
            2           128           128   gramschmidt           MPI          7 43777403.326172            0 
            2           128           128   gramschmidt           MPI          8 42287568.515625            5 
            2           128           128   gramschmidt           MPI          9 42833463.023438            0 
# Runtime: 559.034238 s (overhead: 0.000003 %) 10 records
