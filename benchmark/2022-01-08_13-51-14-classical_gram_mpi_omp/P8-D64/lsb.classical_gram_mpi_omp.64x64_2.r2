# Sysname : Linux
# Nodename: eu-a6-012-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:51:29 2022
# Execution time and date (local): Sat Jan  8 13:51:29 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 14489533.517578            0 
            2            64            64   gramschmidt           MPI          1 15979473.527344            3 
            2            64            64   gramschmidt           MPI          2 15252864.707031            1 
            2            64            64   gramschmidt           MPI          3 15638475.164062            0 
            2            64            64   gramschmidt           MPI          4 14848499.240234            1 
            2            64            64   gramschmidt           MPI          5 15872721.705078            3 
            2            64            64   gramschmidt           MPI          6 16222784.703125            0 
            2            64            64   gramschmidt           MPI          7 15148469.955078            0 
            2            64            64   gramschmidt           MPI          8 15162831.921875            4 
            2            64            64   gramschmidt           MPI          9 15792873.328125            0 
# Runtime: 202.535980 s (overhead: 0.000006 %) 10 records
