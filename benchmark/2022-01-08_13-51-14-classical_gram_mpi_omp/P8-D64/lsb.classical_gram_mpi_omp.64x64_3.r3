# Sysname : Linux
# Nodename: eu-a6-012-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:51:29 2022
# Execution time and date (local): Sat Jan  8 13:51:29 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 14519184.097656            0 
            3            64            64   gramschmidt           MPI          1 15987065.482422            4 
            3            64            64   gramschmidt           MPI          2 15279120.511719            4 
            3            64            64   gramschmidt           MPI          3 15669100.656250            0 
            3            64            64   gramschmidt           MPI          4 14882369.945312            1 
            3            64            64   gramschmidt           MPI          5 15799085.994141            0 
            3            64            64   gramschmidt           MPI          6 16258068.851562            0 
            3            64            64   gramschmidt           MPI          7 15209116.000000            0 
            3            64            64   gramschmidt           MPI          8 15159124.595703            4 
            3            64            64   gramschmidt           MPI          9 15719117.789062            0 
# Runtime: 205.514440 s (overhead: 0.000006 %) 10 records
