# Sysname : Linux
# Nodename: eu-a6-010-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 02:26:00 2022
# Execution time and date (local): Sun Jan  9 03:26:00 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 49486426.173828            0 
            3           128           128   gramschmidt           MPI          1 49816376.976562           17 
            3           128           128   gramschmidt           MPI          2 49246379.605469            4 
            3           128           128   gramschmidt           MPI          3 49226386.771484            0 
            3           128           128   gramschmidt           MPI          4 49112639.386719            3 
            3           128           128   gramschmidt           MPI          5 49636334.394531            0 
            3           128           128   gramschmidt           MPI          6 49276395.875000            0 
            3           128           128   gramschmidt           MPI          7 49366384.689453            0 
            3           128           128   gramschmidt           MPI          8 50146320.119141            4 
            3           128           128   gramschmidt           MPI          9 49756307.482422            0 
# Runtime: 644.390140 s (overhead: 0.000004 %) 10 records
