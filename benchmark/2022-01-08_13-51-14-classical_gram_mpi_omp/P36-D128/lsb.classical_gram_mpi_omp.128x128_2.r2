# Sysname : Linux
# Nodename: eu-a6-010-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 02:26:00 2022
# Execution time and date (local): Sun Jan  9 03:26:00 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 49503294.187500            0 
            2           128           128   gramschmidt           MPI          1 49813149.484375            2 
            2           128           128   gramschmidt           MPI          2 49275213.417969            1 
            2           128           128   gramschmidt           MPI          3 49303193.388672            0 
            2           128           128   gramschmidt           MPI          4 49173404.667969            3 
            2           128           128   gramschmidt           MPI          5 49653132.224609            0 
            2           128           128   gramschmidt           MPI          6 49323179.900391            0 
            2           128           128   gramschmidt           MPI          7 49413192.199219            0 
            2           128           128   gramschmidt           MPI          8 50163167.966797            4 
            2           128           128   gramschmidt           MPI          9 49761145.351562            0 
# Runtime: 645.395621 s (overhead: 0.000002 %) 10 records
