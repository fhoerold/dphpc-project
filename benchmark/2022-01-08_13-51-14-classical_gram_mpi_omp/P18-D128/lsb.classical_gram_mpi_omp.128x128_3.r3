# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 14:50:59 2022
# Execution time and date (local): Sat Jan  8 15:50:59 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 42997813.855469            0 
            3           128           128   gramschmidt           MPI          1 43287747.310547           18 
            3           128           128   gramschmidt           MPI          2 43857770.986328            4 
            3           128           128   gramschmidt           MPI          3 43647741.111328            0 
            3           128           128   gramschmidt           MPI          4 43026780.865234            4 
            3           128           128   gramschmidt           MPI          5 43217812.841797            0 
            3           128           128   gramschmidt           MPI          6 43210896.400391            0 
            3           128           128   gramschmidt           MPI          7 42787828.201172            0 
            3           128           128   gramschmidt           MPI          8 43147801.330078            5 
            3           128           128   gramschmidt           MPI          9 42170049.283203            0 
# Runtime: 562.650589 s (overhead: 0.000006 %) 10 records
