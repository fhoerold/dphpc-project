# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 14:50:59 2022
# Execution time and date (local): Sat Jan  8 15:50:59 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 43077751.828125            0 
            2           128           128   gramschmidt           MPI          1 43375257.212891           10 
            2           128           128   gramschmidt           MPI          2 43897696.193359            3 
            2           128           128   gramschmidt           MPI          3 43667594.091797            0 
            2           128           128   gramschmidt           MPI          4 43117829.804688            3 
            2           128           128   gramschmidt           MPI          5 43257549.031250            0 
            2           128           128   gramschmidt           MPI          6 43287662.314453            0 
            2           128           128   gramschmidt           MPI          7 42867602.648438            0 
            2           128           128   gramschmidt           MPI          8 43187768.837891            4 
            2           128           128   gramschmidt           MPI          9 42217683.736328            2 
# Runtime: 569.712163 s (overhead: 0.000004 %) 10 records
