# Sysname : Linux
# Nodename: eu-a6-004-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:51:29 2022
# Execution time and date (local): Sat Jan  8 13:51:29 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 5246340.638672            0 
            3          2048          2048   gramschmidt           MPI          1 5263837.511719            5 
            3          2048          2048   gramschmidt           MPI          2 5266745.195312            2 
            3          2048          2048   gramschmidt           MPI          3 5237613.888672            0 
            3          2048          2048   gramschmidt           MPI          4 5296844.703125            5 
            3          2048          2048   gramschmidt           MPI          5 5240028.826172            0 
            3          2048          2048   gramschmidt           MPI          6 5262475.044922            0 
            3          2048          2048   gramschmidt           MPI          7 5227670.873047            0 
            3          2048          2048   gramschmidt           MPI          8 5234078.130859            4 
            3          2048          2048   gramschmidt           MPI          9 5293231.787109            0 
# Runtime: 68.388531 s (overhead: 0.000023 %) 10 records
