# Sysname : Linux
# Nodename: eu-a6-004-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:51:29 2022
# Execution time and date (local): Sat Jan  8 13:51:29 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 5244322.667969            0 
            2          2048          2048   gramschmidt           MPI          1 5262481.605469            4 
            2          2048          2048   gramschmidt           MPI          2 5265431.310547            1 
            2          2048          2048   gramschmidt           MPI          3 5235666.146484            0 
            2          2048          2048   gramschmidt           MPI          4 5295577.683594            5 
            2          2048          2048   gramschmidt           MPI          5 5238758.351562            0 
            2          2048          2048   gramschmidt           MPI          6 5261140.384766            0 
            2          2048          2048   gramschmidt           MPI          7 5226391.050781            0 
            2          2048          2048   gramschmidt           MPI          8 5232748.294922            1 
            2          2048          2048   gramschmidt           MPI          9 5291273.001953            0 
# Runtime: 71.386782 s (overhead: 0.000015 %) 10 records
