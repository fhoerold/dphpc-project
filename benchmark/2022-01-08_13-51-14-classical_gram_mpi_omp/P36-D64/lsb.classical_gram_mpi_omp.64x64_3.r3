# Sysname : Linux
# Nodename: eu-a6-010-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 02:20:25 2022
# Execution time and date (local): Sun Jan  9 03:20:25 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 24878248.271484            0 
            3            64            64   gramschmidt           MPI          1 24128271.708984            3 
            3            64            64   gramschmidt           MPI          2 24148305.451172            3 
            3            64            64   gramschmidt           MPI          3 24838227.005859            0 
            3            64            64   gramschmidt           MPI          4 24304775.156250            0 
            3            64            64   gramschmidt           MPI          5 24388257.828125            0 
            3            64            64   gramschmidt           MPI          6 24428278.101562            0 
            3            64            64   gramschmidt           MPI          7 24988217.849609            0 
            3            64            64   gramschmidt           MPI          8 24554843.048828            3 
            3            64            64   gramschmidt           MPI          9 24498489.849609            0 
# Runtime: 327.436280 s (overhead: 0.000003 %) 10 records
