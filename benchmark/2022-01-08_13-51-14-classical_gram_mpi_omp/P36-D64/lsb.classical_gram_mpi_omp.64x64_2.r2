# Sysname : Linux
# Nodename: eu-a6-010-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 02:20:25 2022
# Execution time and date (local): Sun Jan  9 03:20:25 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 24876807.417969            0 
            2            64            64   gramschmidt           MPI          1 24126925.580078           16 
            2            64            64   gramschmidt           MPI          2 24216872.285156            1 
            2            64            64   gramschmidt           MPI          3 24836822.359375            0 
            2            64            64   gramschmidt           MPI          4 24306874.697266            4 
            2            64            64   gramschmidt           MPI          5 24386860.917969            0 
            2            64            64   gramschmidt           MPI          6 24362366.882812            0 
            2            64            64   gramschmidt           MPI          7 24946799.156250            0 
            2            64            64   gramschmidt           MPI          8 24546836.978516            5 
            2            64            64   gramschmidt           MPI          9 24480122.683594            0 
# Runtime: 327.423328 s (overhead: 0.000008 %) 10 records
