# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 20:47:21 2022
# Execution time and date (local): Sat Jan  8 21:47:21 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 49096523.230469            0 
            2           128           128   gramschmidt           MPI          1 48706491.083984            6 
            2           128           128   gramschmidt           MPI          2 49176417.146484            1 
            2           128           128   gramschmidt           MPI          3 48806556.025391            0 
            2           128           128   gramschmidt           MPI          4 48836619.162109            3 
            2           128           128   gramschmidt           MPI          5 48756478.896484            0 
            2           128           128   gramschmidt           MPI          6 48306527.410156            0 
            2           128           128   gramschmidt           MPI          7 49006425.330078            0 
            2           128           128   gramschmidt           MPI          8 49376462.937500            3 
            2           128           128   gramschmidt           MPI          9 48536915.933594            0 
# Runtime: 636.523064 s (overhead: 0.000002 %) 10 records
