# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 20:47:21 2022
# Execution time and date (local): Sat Jan  8 21:47:21 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 49056670.683594            0 
            3           128           128   gramschmidt           MPI          1 48666695.931641            8 
            3           128           128   gramschmidt           MPI          2 49126591.179688            1 
            3           128           128   gramschmidt           MPI          3 48766673.625000            0 
            3           128           128   gramschmidt           MPI          4 48736671.988281            3 
            3           128           128   gramschmidt           MPI          5 48756660.007812            0 
            3           128           128   gramschmidt           MPI          6 48256677.269531            0 
            3           128           128   gramschmidt           MPI          7 48926599.554688            0 
            3           128           128   gramschmidt           MPI          8 49316632.136719            3 
            3           128           128   gramschmidt           MPI          9 48546686.021484            0 
# Runtime: 637.549687 s (overhead: 0.000002 %) 10 records
