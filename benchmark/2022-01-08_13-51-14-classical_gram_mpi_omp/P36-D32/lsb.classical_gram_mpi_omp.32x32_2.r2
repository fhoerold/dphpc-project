# Sysname : Linux
# Nodename: eu-a6-010-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 01:32:57 2022
# Execution time and date (local): Sun Jan  9 02:32:57 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 12089372.906250            0 
            2            32            32   gramschmidt           MPI          1 11939356.689453           13 
            2            32            32   gramschmidt           MPI          2 11989349.669922            2 
            2            32            32   gramschmidt           MPI          3 11899370.615234            0 
            2            32            32   gramschmidt           MPI          4 11869363.970703            3 
            2            32            32   gramschmidt           MPI          5 11989363.693359            0 
            2            32            32   gramschmidt           MPI          6 11839359.498047            0 
            2            32            32   gramschmidt           MPI          7 12029357.515625            0 
            2            32            32   gramschmidt           MPI          8 11969356.431641            1 
            2            32            32   gramschmidt           MPI          9 11939371.207031            0 
# Runtime: 156.861363 s (overhead: 0.000012 %) 10 records
