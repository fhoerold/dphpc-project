# Sysname : Linux
# Nodename: eu-a6-010-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sun Jan  9 01:32:57 2022
# Execution time and date (local): Sun Jan  9 02:32:57 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 12039140.646484            0 
            3            32            32   gramschmidt           MPI          1 11899128.333984            3 
            3            32            32   gramschmidt           MPI          2 11969126.013672            1 
            3            32            32   gramschmidt           MPI          3 11899144.339844            0 
            3            32            32   gramschmidt           MPI          4 11799587.742188            3 
            3            32            32   gramschmidt           MPI          5 11959130.087891            0 
            3            32            32   gramschmidt           MPI          6 11801832.130859            0 
            3            32            32   gramschmidt           MPI          7 11999133.974609            0 
            3            32            32   gramschmidt           MPI          8 11949122.027344            5 
            3            32            32   gramschmidt           MPI          9 11899131.671875            0 
# Runtime: 163.849834 s (overhead: 0.000007 %) 10 records
