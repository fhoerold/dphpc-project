# Sysname : Linux
# Nodename: eu-a6-006-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:51:57 2022
# Execution time and date (local): Sat Jan  8 13:51:57 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 130129561.812500            0 
            3           512           512   gramschmidt           MPI          1 126879787.587891            2 
            3           512           512   gramschmidt           MPI          2 128949628.816406            3 
            3           512           512   gramschmidt           MPI          3 125439902.480469            0 
            3           512           512   gramschmidt           MPI          4 129529578.708984            3 
            3           512           512   gramschmidt           MPI          5 128879595.478516            0 
            3           512           512   gramschmidt           MPI          6 128079692.777344            0 
            3           512           512   gramschmidt           MPI          7 126599809.277344            0 
            3           512           512   gramschmidt           MPI          8 127209745.046875            4 
            3           512           512   gramschmidt           MPI          9 128771403.980469            0 
# Runtime: 1665.207729 s (overhead: 0.000001 %) 10 records
