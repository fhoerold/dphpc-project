# Sysname : Linux
# Nodename: eu-a6-006-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 12:51:57 2022
# Execution time and date (local): Sat Jan  8 13:51:57 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 130151285.589844            0 
            2           512           512   gramschmidt           MPI          1 126881492.371094            3 
            2           512           512   gramschmidt           MPI          2 128991342.583984            3 
            2           512           512   gramschmidt           MPI          3 125441524.117188            0 
            2           512           512   gramschmidt           MPI          4 129511347.494141            3 
            2           512           512   gramschmidt           MPI          5 128861352.181641            0 
            2           512           512   gramschmidt           MPI          6 128091436.621094            0 
            2           512           512   gramschmidt           MPI          7 126601444.744141            0 
            2           512           512   gramschmidt           MPI          8 127221496.107422            4 
            2           512           512   gramschmidt           MPI          9 128762000.310547            0 
# Runtime: 1665.244416 s (overhead: 0.000001 %) 10 records
