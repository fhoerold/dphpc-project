# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 17:40:03 2022
# Execution time and date (local): Wed Jan 12 18:40:03 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         5161          5161   gramschmidt          0 113211569.695312            0 
         5161          5161   gramschmidt          1 116933715.056641            5 
         5161          5161   gramschmidt          2 116730057.644531            5 
         5161          5161   gramschmidt          3 116654994.025391            0 
         5161          5161   gramschmidt          4 116886399.216797            5 
         5161          5161   gramschmidt          5 116883936.291016            0 
         5161          5161   gramschmidt          6 116658853.384766            0 
         5161          5161   gramschmidt          7 116686563.925781            0 
         5161          5161   gramschmidt          8 108911794.351562            5 
         5161          5161   gramschmidt          9 105931996.603516            0 
# Runtime: 1145.489943 s (overhead: 0.000002 %) 10 records
