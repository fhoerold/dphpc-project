# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 17:40:03 2022
# Execution time and date (local): Wed Jan 12 18:40:03 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         5161          5161   gramschmidt          0 118156993.685547            0 
         5161          5161   gramschmidt          1 116888533.667969            5 
         5161          5161   gramschmidt          2 116684882.939453            5 
         5161          5161   gramschmidt          3 116609866.878906            0 
         5161          5161   gramschmidt          4 116841227.082031            5 
         5161          5161   gramschmidt          5 116838796.410156            0 
         5161          5161   gramschmidt          6 116614198.884766            0 
         5161          5161   gramschmidt          7 116641246.757812            0 
         5161          5161   gramschmidt          8 108870822.445312            4 
         5161          5161   gramschmidt          9 105891432.595703            0 
# Runtime: 1150.038060 s (overhead: 0.000002 %) 10 records
