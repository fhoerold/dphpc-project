Invocation: ./gramschmidt/benchmark.sh -o ./benchmark --mpi --linear-dim -d 5161 -p 8 ./build/polybench_mpi_gramschmidt_splitr_optimized
Benchmark 'polybench_mpi_gramschmidt_splitr_optimized' for
> Dimensions:		5161, 
> Number of processors:	8, 
> CPU Model:		XeonGold_6150
> Use MPI:		true
> Use OpenMP:		false

Timestamp: 2022-01-12_18-39-27
