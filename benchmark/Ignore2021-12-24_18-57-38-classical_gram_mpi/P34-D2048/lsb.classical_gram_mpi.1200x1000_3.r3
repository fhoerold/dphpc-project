# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 03:20:45 2021
# Execution time and date (local): Sat Dec 25 04:20:45 2021
# MPI execution on rank 3 with 34 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 4398756.798828            2 
            3          1200          1000   gramschmidt           MPI          1 4474326.945312            6 
            3          1200          1000   gramschmidt           MPI          2 4420970.748047            2 
            3          1200          1000   gramschmidt           MPI          3 4406924.962891            2 
            3          1200          1000   gramschmidt           MPI          4 4517777.753906            6 
            3          1200          1000   gramschmidt           MPI          5 4390462.126953            0 
            3          1200          1000   gramschmidt           MPI          6 4371459.267578            0 
            3          1200          1000   gramschmidt           MPI          7 4412250.595703            0 
            3          1200          1000   gramschmidt           MPI          8 4463481.144531            6 
            3          1200          1000   gramschmidt           MPI          9 4426089.837891            0 
# Runtime: 61.912979 s (overhead: 0.000039 %) 10 records
