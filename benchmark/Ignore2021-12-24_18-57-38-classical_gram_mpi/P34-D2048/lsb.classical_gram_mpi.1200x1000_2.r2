# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 03:20:45 2021
# Execution time and date (local): Sat Dec 25 04:20:45 2021
# MPI execution on rank 2 with 34 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 4398496.113281            0 
            2          1200          1000   gramschmidt           MPI          1 4474168.562500            7 
            2          1200          1000   gramschmidt           MPI          2 4420855.929688            2 
            2          1200          1000   gramschmidt           MPI          3 4406640.322266            2 
            2          1200          1000   gramschmidt           MPI          4 4517652.792969            6 
            2          1200          1000   gramschmidt           MPI          5 4390306.845703            0 
            2          1200          1000   gramschmidt           MPI          6 4371298.734375            0 
            2          1200          1000   gramschmidt           MPI          7 4411974.556641            3 
            2          1200          1000   gramschmidt           MPI          8 4463291.984375            5 
            2          1200          1000   gramschmidt           MPI          9 4425929.656250            0 
# Runtime: 57.909462 s (overhead: 0.000043 %) 10 records
