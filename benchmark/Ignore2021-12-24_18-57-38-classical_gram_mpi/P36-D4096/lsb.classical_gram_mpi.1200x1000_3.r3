# Sysname : Linux
# Nodename: eu-a6-012-05
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 05:53:39 2021
# Execution time and date (local): Sat Dec 25 06:53:39 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 49762640.578125            0 
            3          1200          1000   gramschmidt           MPI          1 49458047.556641            6 
            3          1200          1000   gramschmidt           MPI          2 50641274.716797            5 
            3          1200          1000   gramschmidt           MPI          3 50910724.214844            3 
            3          1200          1000   gramschmidt           MPI          4 50196984.179688            6 
            3          1200          1000   gramschmidt           MPI          5 49990500.537109            2 
            3          1200          1000   gramschmidt           MPI          6 50973930.339844            4 
            3          1200          1000   gramschmidt           MPI          7 50828898.044922            0 
            3          1200          1000   gramschmidt           MPI          8 51696204.050781            4 
            3          1200          1000   gramschmidt           MPI          9 50001651.332031            2 
# Runtime: 667.075931 s (overhead: 0.000005 %) 10 records
