# Sysname : Linux
# Nodename: eu-a6-012-05
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 05:53:39 2021
# Execution time and date (local): Sat Dec 25 06:53:39 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 49745507.730469            0 
            2          1200          1000   gramschmidt           MPI          1 49441069.521484            7 
            2          1200          1000   gramschmidt           MPI          2 50623937.191406            3 
            2          1200          1000   gramschmidt           MPI          3 50893214.041016            0 
            2          1200          1000   gramschmidt           MPI          4 50180164.736328           11 
            2          1200          1000   gramschmidt           MPI          5 49973769.755859            3 
            2          1200          1000   gramschmidt           MPI          6 50956376.796875            3 
            2          1200          1000   gramschmidt           MPI          7 50811501.683594            3 
            2          1200          1000   gramschmidt           MPI          8 51678468.210938            6 
            2          1200          1000   gramschmidt           MPI          9 49984494.765625            2 
# Runtime: 670.849025 s (overhead: 0.000006 %) 10 records
