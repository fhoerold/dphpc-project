# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:05:42 2021
# Execution time and date (local): Fri Dec 24 19:05:42 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 5737.687500            0 
            2          1200          1000   gramschmidt           MPI          1 5743.070312            3 
            2          1200          1000   gramschmidt           MPI          2 5718.828125            0 
            2          1200          1000   gramschmidt           MPI          3 5727.207031            0 
            2          1200          1000   gramschmidt           MPI          4 5663.988281            0 
            2          1200          1000   gramschmidt           MPI          5 5719.439453            0 
            2          1200          1000   gramschmidt           MPI          6 5680.283203            0 
            2          1200          1000   gramschmidt           MPI          7 6252.876953            0 
            2          1200          1000   gramschmidt           MPI          8 5699.621094            0 
            2          1200          1000   gramschmidt           MPI          9 5690.898438            0 
# Runtime: 5.078470 s (overhead: 0.000059 %) 10 records
