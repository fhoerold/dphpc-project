# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:05:42 2021
# Execution time and date (local): Fri Dec 24 19:05:42 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 5723.072266            0 
            3          1200          1000   gramschmidt           MPI          1 5715.281250            4 
            3          1200          1000   gramschmidt           MPI          2 5692.699219            0 
            3          1200          1000   gramschmidt           MPI          3 5696.974609            0 
            3          1200          1000   gramschmidt           MPI          4 5630.953125            0 
            3          1200          1000   gramschmidt           MPI          5 5685.818359            0 
            3          1200          1000   gramschmidt           MPI          6 5638.455078            0 
            3          1200          1000   gramschmidt           MPI          7 6237.085938            0 
            3          1200          1000   gramschmidt           MPI          8 5655.845703            0 
            3          1200          1000   gramschmidt           MPI          9 5670.074219            0 
# Runtime: 6.076008 s (overhead: 0.000066 %) 10 records
