# Sysname : Linux
# Nodename: eu-a6-006-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 19:01:22 2021
# Execution time and date (local): Fri Dec 24 20:01:22 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 1411842988.037109            0 
            3          1200          1000   gramschmidt           MPI          1 1389605945.685547            9 
            3          1200          1000   gramschmidt           MPI          2 1383898432.560547            6 
            3          1200          1000   gramschmidt           MPI          3 1377008986.701172            2 
            3          1200          1000   gramschmidt           MPI          4 1347516059.925781            7 
            3          1200          1000   gramschmidt           MPI          5 1344596848.240234            0 
            3          1200          1000   gramschmidt           MPI          6 1349490640.859375            0 
            3          1200          1000   gramschmidt           MPI          7 1357117830.085938            0 
            3          1200          1000   gramschmidt           MPI          8 1346874887.367188            6 
            3          1200          1000   gramschmidt           MPI          9 1351727098.238281            2 
# Runtime: 17922.839779 s (overhead: 0.000000 %) 10 records
