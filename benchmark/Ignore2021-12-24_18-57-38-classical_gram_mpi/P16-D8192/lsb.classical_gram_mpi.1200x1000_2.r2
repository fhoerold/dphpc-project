# Sysname : Linux
# Nodename: eu-a6-006-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 19:01:22 2021
# Execution time and date (local): Fri Dec 24 20:01:22 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 1413645048.986328            0 
            2          1200          1000   gramschmidt           MPI          1 1391379949.746094            9 
            2          1200          1000   gramschmidt           MPI          2 1385664651.517578            5 
            2          1200          1000   gramschmidt           MPI          3 1378766059.265625            0 
            2          1200          1000   gramschmidt           MPI          4 1349235819.001953            8 
            2          1200          1000   gramschmidt           MPI          5 1346312826.947266            3 
            2          1200          1000   gramschmidt           MPI          6 1351212917.611328            3 
            2          1200          1000   gramschmidt           MPI          7 1358849477.748047            3 
            2          1200          1000   gramschmidt           MPI          8 1348593266.226562           12 
            2          1200          1000   gramschmidt           MPI          9 1353452382.607422            4 
# Runtime: 17947.837105 s (overhead: 0.000000 %) 10 records
