# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 03:19:49 2021
# Execution time and date (local): Sat Dec 25 04:19:49 2021
# MPI execution on rank 2 with 34 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 24625.318359            0 
            2          1200          1000   gramschmidt           MPI          1 24488.962891            3 
            2          1200          1000   gramschmidt           MPI          2 24353.462891            1 
            2          1200          1000   gramschmidt           MPI          3 24184.613281            0 
            2          1200          1000   gramschmidt           MPI          4 24979.980469            0 
            2          1200          1000   gramschmidt           MPI          5 24358.015625            0 
            2          1200          1000   gramschmidt           MPI          6 24266.314453            0 
            2          1200          1000   gramschmidt           MPI          7 24141.855469            0 
            2          1200          1000   gramschmidt           MPI          8 24148.958984            0 
            2          1200          1000   gramschmidt           MPI          9 24212.699219            0 
# Runtime: 11.378520 s (overhead: 0.000035 %) 10 records
