# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 03:19:49 2021
# Execution time and date (local): Sat Dec 25 04:19:49 2021
# MPI execution on rank 3 with 34 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 24644.224609            0 
            3          1200          1000   gramschmidt           MPI          1 24717.554688            3 
            3          1200          1000   gramschmidt           MPI          2 24353.714844            1 
            3          1200          1000   gramschmidt           MPI          3 24183.669922            0 
            3          1200          1000   gramschmidt           MPI          4 24977.726562            0 
            3          1200          1000   gramschmidt           MPI          5 24362.003906            0 
            3          1200          1000   gramschmidt           MPI          6 24275.832031            0 
            3          1200          1000   gramschmidt           MPI          7 24138.226562            0 
            3          1200          1000   gramschmidt           MPI          8 24147.228516            0 
            3          1200          1000   gramschmidt           MPI          9 24219.478516            0 
# Runtime: 11.376440 s (overhead: 0.000035 %) 10 records
