# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 03:22:24 2021
# Execution time and date (local): Sat Dec 25 04:22:24 2021
# MPI execution on rank 2 with 34 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 88974.787109            0 
            2          1200          1000   gramschmidt           MPI          1 97063.470703            5 
            2          1200          1000   gramschmidt           MPI          2 71838.511719            1 
            2          1200          1000   gramschmidt           MPI          3 71689.107422            0 
            2          1200          1000   gramschmidt           MPI          4 71341.910156            2 
            2          1200          1000   gramschmidt           MPI          5 92996.291016            0 
            2          1200          1000   gramschmidt           MPI          6 116995.656250            0 
            2          1200          1000   gramschmidt           MPI          7 117293.814453            0 
            2          1200          1000   gramschmidt           MPI          8 118752.132812            1 
            2          1200          1000   gramschmidt           MPI          9 102975.582031            0 
# Runtime: 12.383837 s (overhead: 0.000073 %) 10 records
