# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 03:22:24 2021
# Execution time and date (local): Sat Dec 25 04:22:24 2021
# MPI execution on rank 3 with 34 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 88544.244141            0 
            3          1200          1000   gramschmidt           MPI          1 94054.525391            4 
            3          1200          1000   gramschmidt           MPI          2 71366.253906            1 
            3          1200          1000   gramschmidt           MPI          3 71211.689453            0 
            3          1200          1000   gramschmidt           MPI          4 70820.841797            1 
            3          1200          1000   gramschmidt           MPI          5 92774.042969            0 
            3          1200          1000   gramschmidt           MPI          6 116591.554688            0 
            3          1200          1000   gramschmidt           MPI          7 117274.066406            0 
            3          1200          1000   gramschmidt           MPI          8 118320.375000            2 
            3          1200          1000   gramschmidt           MPI          9 102962.548828            0 
# Runtime: 9.393822 s (overhead: 0.000085 %) 10 records
