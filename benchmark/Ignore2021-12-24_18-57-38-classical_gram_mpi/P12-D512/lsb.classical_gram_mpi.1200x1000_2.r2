# Sysname : Linux
# Nodename: eu-a6-001-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:59:48 2021
# Execution time and date (local): Fri Dec 24 18:59:48 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 133804.423828            0 
            2          1200          1000   gramschmidt           MPI          1 127006.148438            4 
            2          1200          1000   gramschmidt           MPI          2 126976.265625            1 
            2          1200          1000   gramschmidt           MPI          3 126397.265625            0 
            2          1200          1000   gramschmidt           MPI          4 127813.156250            1 
            2          1200          1000   gramschmidt           MPI          5 127599.628906            0 
            2          1200          1000   gramschmidt           MPI          6 127580.990234            0 
            2          1200          1000   gramschmidt           MPI          7 126040.886719            0 
            2          1200          1000   gramschmidt           MPI          8 126291.798828            5 
            2          1200          1000   gramschmidt           MPI          9 125560.414062            0 
# Runtime: 8.679671 s (overhead: 0.000127 %) 10 records
