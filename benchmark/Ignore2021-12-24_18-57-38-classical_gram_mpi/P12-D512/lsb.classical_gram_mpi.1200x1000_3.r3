# Sysname : Linux
# Nodename: eu-a6-001-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:59:48 2021
# Execution time and date (local): Fri Dec 24 18:59:48 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 134161.054688            0 
            3          1200          1000   gramschmidt           MPI          1 126976.384766            4 
            3          1200          1000   gramschmidt           MPI          2 126943.126953            1 
            3          1200          1000   gramschmidt           MPI          3 126356.900391            0 
            3          1200          1000   gramschmidt           MPI          4 127767.398438            2 
            3          1200          1000   gramschmidt           MPI          5 127556.656250            0 
            3          1200          1000   gramschmidt           MPI          6 127540.078125            0 
            3          1200          1000   gramschmidt           MPI          7 126003.492188            0 
            3          1200          1000   gramschmidt           MPI          8 126245.091797            2 
            3          1200          1000   gramschmidt           MPI          9 125506.343750            0 
# Runtime: 5.661672 s (overhead: 0.000159 %) 10 records
