# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:06:16 2021
# Execution time and date (local): Fri Dec 24 19:06:16 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 3429958.390625            0 
            2          1200          1000   gramschmidt           MPI          1 3378452.619141            4 
            2          1200          1000   gramschmidt           MPI          2 3402228.689453            3 
            2          1200          1000   gramschmidt           MPI          3 3388075.783203            0 
            2          1200          1000   gramschmidt           MPI          4 3379033.966797            1 
            2          1200          1000   gramschmidt           MPI          5 3389251.121094            0 
            2          1200          1000   gramschmidt           MPI          6 3386356.255859            0 
            2          1200          1000   gramschmidt           MPI          7 3375983.183594            0 
            2          1200          1000   gramschmidt           MPI          8 3374890.796875            4 
            2          1200          1000   gramschmidt           MPI          9 3376741.177734            0 
# Runtime: 47.097785 s (overhead: 0.000025 %) 10 records
