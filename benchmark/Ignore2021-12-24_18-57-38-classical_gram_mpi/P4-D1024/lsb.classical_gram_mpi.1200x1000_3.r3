# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:06:16 2021
# Execution time and date (local): Fri Dec 24 19:06:16 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 3426329.861328            0 
            3          1200          1000   gramschmidt           MPI          1 3375096.013672            5 
            3          1200          1000   gramschmidt           MPI          2 3398643.591797            4 
            3          1200          1000   gramschmidt           MPI          3 3384487.955078            0 
            3          1200          1000   gramschmidt           MPI          4 3375466.951172            1 
            3          1200          1000   gramschmidt           MPI          5 3385902.169922            0 
            3          1200          1000   gramschmidt           MPI          6 3382774.617188            0 
            3          1200          1000   gramschmidt           MPI          7 3372437.601562            0 
            3          1200          1000   gramschmidt           MPI          8 3371326.400391            5 
            3          1200          1000   gramschmidt           MPI          9 3373172.269531            0 
# Runtime: 44.032610 s (overhead: 0.000034 %) 10 records
