# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 00:48:48 2021
# Execution time and date (local): Sat Dec 25 01:48:48 2021
# MPI execution on rank 3 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 4748510.312500            0 
            3          1200          1000   gramschmidt           MPI          1 4760684.529297            6 
            3          1200          1000   gramschmidt           MPI          2 4848910.187500           11 
            3          1200          1000   gramschmidt           MPI          3 4736006.228516            0 
            3          1200          1000   gramschmidt           MPI          4 4762644.460938            2 
            3          1200          1000   gramschmidt           MPI          5 4740732.228516            0 
            3          1200          1000   gramschmidt           MPI          6 4804715.513672            0 
            3          1200          1000   gramschmidt           MPI          7 4770257.537109            0 
            3          1200          1000   gramschmidt           MPI          8 4775835.310547            6 
            3          1200          1000   gramschmidt           MPI          9 4735220.041016            0 
# Runtime: 68.914142 s (overhead: 0.000036 %) 10 records
