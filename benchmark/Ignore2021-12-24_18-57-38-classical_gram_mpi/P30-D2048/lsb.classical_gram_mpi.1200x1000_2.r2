# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 00:48:48 2021
# Execution time and date (local): Sat Dec 25 01:48:48 2021
# MPI execution on rank 2 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 4748480.171875            2 
            2          1200          1000   gramschmidt           MPI          1 4760581.425781            6 
            2          1200          1000   gramschmidt           MPI          2 4848931.138672           20 
            2          1200          1000   gramschmidt           MPI          3 4736033.150391            0 
            2          1200          1000   gramschmidt           MPI          4 4762658.443359            6 
            2          1200          1000   gramschmidt           MPI          5 4740585.013672            0 
            2          1200          1000   gramschmidt           MPI          6 4804672.757812            0 
            2          1200          1000   gramschmidt           MPI          7 4770280.232422            0 
            2          1200          1000   gramschmidt           MPI          8 4775743.880859            6 
            2          1200          1000   gramschmidt           MPI          9 4735193.439453            0 
# Runtime: 61.926843 s (overhead: 0.000065 %) 10 records
