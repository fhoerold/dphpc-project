# Sysname : Linux
# Nodename: eu-a6-006-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:00:45 2021
# Execution time and date (local): Fri Dec 24 19:00:45 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 111983835.832031            2 
            2          1200          1000   gramschmidt           MPI          1 104567287.130859            6 
            2          1200          1000   gramschmidt           MPI          2 105012854.732422            6 
            2          1200          1000   gramschmidt           MPI          3 105455623.277344            0 
            2          1200          1000   gramschmidt           MPI          4 104669554.865234            9 
            2          1200          1000   gramschmidt           MPI          5 104356385.033203            4 
            2          1200          1000   gramschmidt           MPI          6 104354239.062500            4 
            2          1200          1000   gramschmidt           MPI          7 117473971.273438            4 
            2          1200          1000   gramschmidt           MPI          8 104867988.171875            6 
            2          1200          1000   gramschmidt           MPI          9 106248569.964844            0 
# Runtime: 1398.466606 s (overhead: 0.000003 %) 10 records
