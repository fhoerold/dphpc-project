# Sysname : Linux
# Nodename: eu-a6-006-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:00:45 2021
# Execution time and date (local): Fri Dec 24 19:00:45 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 111979427.537109            2 
            3          1200          1000   gramschmidt           MPI          1 104563126.683594            4 
            3          1200          1000   gramschmidt           MPI          2 105008704.892578            7 
            3          1200          1000   gramschmidt           MPI          3 105451419.876953            0 
            3          1200          1000   gramschmidt           MPI          4 104665227.250000            8 
            3          1200          1000   gramschmidt           MPI          5 104351960.705078            2 
            3          1200          1000   gramschmidt           MPI          6 104350200.603516            2 
            3          1200          1000   gramschmidt           MPI          7 117468870.210938            1 
            3          1200          1000   gramschmidt           MPI          8 104863892.005859            6 
            3          1200          1000   gramschmidt           MPI          9 106244263.832031            2 
# Runtime: 1393.365684 s (overhead: 0.000002 %) 10 records
