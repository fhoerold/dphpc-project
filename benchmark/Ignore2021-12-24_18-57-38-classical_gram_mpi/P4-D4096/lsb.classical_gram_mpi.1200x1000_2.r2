# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:07:18 2021
# Execution time and date (local): Fri Dec 24 19:07:18 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 346062735.484375            0 
            2          1200          1000   gramschmidt           MPI          1 350152505.615234            7 
            2          1200          1000   gramschmidt           MPI          2 350262049.425781            7 
            2          1200          1000   gramschmidt           MPI          3 349910366.775391            0 
            2          1200          1000   gramschmidt           MPI          4 348503825.519531            5 
            2          1200          1000   gramschmidt           MPI          5 647420597.189453            0 
            2          1200          1000   gramschmidt           MPI          6 378105493.107422            0 
            2          1200          1000   gramschmidt           MPI          7 366635749.207031            1 
            2          1200          1000   gramschmidt           MPI          8 403251779.570312            6 
            2          1200          1000   gramschmidt           MPI          9 349920950.328125            1 
# Runtime: 4953.538798 s (overhead: 0.000001 %) 10 records
