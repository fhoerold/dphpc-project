# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:07:18 2021
# Execution time and date (local): Fri Dec 24 19:07:18 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 345895229.492188            0 
            3          1200          1000   gramschmidt           MPI          1 349982291.468750            6 
            3          1200          1000   gramschmidt           MPI          2 350092782.701172            7 
            3          1200          1000   gramschmidt           MPI          3 349741712.783203            3 
            3          1200          1000   gramschmidt           MPI          4 348335192.419922            6 
            3          1200          1000   gramschmidt           MPI          5 647100849.097656            1 
            3          1200          1000   gramschmidt           MPI          6 377922048.951172            1 
            3          1200          1000   gramschmidt           MPI          7 366458191.351562            1 
            3          1200          1000   gramschmidt           MPI          8 403055463.792969            4 
            3          1200          1000   gramschmidt           MPI          9 349751946.779297            0 
# Runtime: 4954.028695 s (overhead: 0.000001 %) 10 records
