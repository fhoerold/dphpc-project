# Sysname : Linux
# Nodename: eu-a6-012-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:58:45 2021
# Execution time and date (local): Fri Dec 24 18:58:45 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 165667181.966797            1 
            2          1200          1000   gramschmidt           MPI          1 164696926.433594            5 
            2          1200          1000   gramschmidt           MPI          2 167320546.960938            6 
            2          1200          1000   gramschmidt           MPI          3 168849581.832031            2 
            2          1200          1000   gramschmidt           MPI          4 169057930.212891            7 
            2          1200          1000   gramschmidt           MPI          5 166278874.292969            0 
            2          1200          1000   gramschmidt           MPI          6 171453138.128906            0 
            2          1200          1000   gramschmidt           MPI          7 167965955.164062            0 
            2          1200          1000   gramschmidt           MPI          8 169633448.386719            4 
            2          1200          1000   gramschmidt           MPI          9 170666136.556641            0 
# Runtime: 2191.493253 s (overhead: 0.000001 %) 10 records
