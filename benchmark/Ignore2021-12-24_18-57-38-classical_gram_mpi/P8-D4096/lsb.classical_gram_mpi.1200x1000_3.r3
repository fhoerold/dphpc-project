# Sysname : Linux
# Nodename: eu-a6-012-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:58:45 2021
# Execution time and date (local): Fri Dec 24 18:58:45 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 165660294.123047            0 
            3          1200          1000   gramschmidt           MPI          1 164691388.597656            7 
            3          1200          1000   gramschmidt           MPI          2 167314714.734375            6 
            3          1200          1000   gramschmidt           MPI          3 168843650.001953           13 
            3          1200          1000   gramschmidt           MPI          4 169052042.343750            7 
            3          1200          1000   gramschmidt           MPI          5 166272832.193359            3 
            3          1200          1000   gramschmidt           MPI          6 171446852.630859            4 
            3          1200          1000   gramschmidt           MPI          7 167960237.921875            4 
            3          1200          1000   gramschmidt           MPI          8 169627332.228516            7 
            3          1200          1000   gramschmidt           MPI          9 170660153.121094            0 
# Runtime: 2204.344977 s (overhead: 0.000002 %) 10 records
