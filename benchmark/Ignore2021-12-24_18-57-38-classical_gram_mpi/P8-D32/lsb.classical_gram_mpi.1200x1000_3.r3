# Sysname : Linux
# Nodename: eu-a6-008-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:58:14 2021
# Execution time and date (local): Fri Dec 24 18:58:14 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 544.935547            0 
            3          1200          1000   gramschmidt           MPI          1 548.589844            2 
            3          1200          1000   gramschmidt           MPI          2 532.601562            0 
            3          1200          1000   gramschmidt           MPI          3 551.572266            0 
            3          1200          1000   gramschmidt           MPI          4 570.654297            0 
            3          1200          1000   gramschmidt           MPI          5 528.054688            0 
            3          1200          1000   gramschmidt           MPI          6 527.449219            0 
            3          1200          1000   gramschmidt           MPI          7 526.697266            0 
            3          1200          1000   gramschmidt           MPI          8 506.388672            0 
            3          1200          1000   gramschmidt           MPI          9 523.794922            0 
# Runtime: 9.038294 s (overhead: 0.000022 %) 10 records
