# Sysname : Linux
# Nodename: eu-a6-008-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:58:14 2021
# Execution time and date (local): Fri Dec 24 18:58:14 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 547.431641            0 
            2          1200          1000   gramschmidt           MPI          1 552.541016            2 
            2          1200          1000   gramschmidt           MPI          2 534.654297            0 
            2          1200          1000   gramschmidt           MPI          3 554.585938            0 
            2          1200          1000   gramschmidt           MPI          4 572.841797            0 
            2          1200          1000   gramschmidt           MPI          5 532.968750            0 
            2          1200          1000   gramschmidt           MPI          6 530.376953            0 
            2          1200          1000   gramschmidt           MPI          7 528.009766            0 
            2          1200          1000   gramschmidt           MPI          8 510.427734            0 
            2          1200          1000   gramschmidt           MPI          9 526.193359            0 
# Runtime: 7.039341 s (overhead: 0.000028 %) 10 records
