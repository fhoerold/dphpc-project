# Sysname : Linux
# Nodename: eu-a6-012-05
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 05:51:16 2021
# Execution time and date (local): Sat Dec 25 06:51:16 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 703184.167969            0 
            3          1200          1000   gramschmidt           MPI          1 729236.634766            4 
            3          1200          1000   gramschmidt           MPI          2 728571.972656            2 
            3          1200          1000   gramschmidt           MPI          3 728849.748047            0 
            3          1200          1000   gramschmidt           MPI          4 722586.671875            2 
            3          1200          1000   gramschmidt           MPI          5 727831.183594            0 
            3          1200          1000   gramschmidt           MPI          6 726959.974609            0 
            3          1200          1000   gramschmidt           MPI          7 725435.720703            0 
            3          1200          1000   gramschmidt           MPI          8 721132.792969            1 
            3          1200          1000   gramschmidt           MPI          9 721828.376953            0 
# Runtime: 28.631344 s (overhead: 0.000031 %) 10 records
