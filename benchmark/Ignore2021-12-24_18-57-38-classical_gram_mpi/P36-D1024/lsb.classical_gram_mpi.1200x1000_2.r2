# Sysname : Linux
# Nodename: eu-a6-012-05
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 05:51:16 2021
# Execution time and date (local): Sat Dec 25 06:51:16 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 703437.035156            0 
            2          1200          1000   gramschmidt           MPI          1 729266.546875            5 
            2          1200          1000   gramschmidt           MPI          2 728627.076172            2 
            2          1200          1000   gramschmidt           MPI          3 728875.773438            0 
            2          1200          1000   gramschmidt           MPI          4 722612.875000            1 
            2          1200          1000   gramschmidt           MPI          5 727852.812500            0 
            2          1200          1000   gramschmidt           MPI          6 726975.656250            0 
            2          1200          1000   gramschmidt           MPI          7 725744.601562            0 
            2          1200          1000   gramschmidt           MPI          8 721169.603516            2 
            2          1200          1000   gramschmidt           MPI          9 721840.970703            0 
# Runtime: 28.643911 s (overhead: 0.000035 %) 10 records
