# Sysname : Linux
# Nodename: eu-a6-006-16
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:58:46 2021
# Execution time and date (local): Fri Dec 24 18:58:46 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 18989682.695312            0 
            2          1200          1000   gramschmidt           MPI          1 17787761.253906            6 
            2          1200          1000   gramschmidt           MPI          2 17903045.554688            2 
            2          1200          1000   gramschmidt           MPI          3 17632030.582031            0 
            2          1200          1000   gramschmidt           MPI          4 19398055.703125            4 
            2          1200          1000   gramschmidt           MPI          5 17732425.750000            0 
            2          1200          1000   gramschmidt           MPI          6 17473168.287109            0 
            2          1200          1000   gramschmidt           MPI          7 17803426.279297            0 
            2          1200          1000   gramschmidt           MPI          8 17530134.166016            4 
            2          1200          1000   gramschmidt           MPI          9 17655256.125000            0 
# Runtime: 236.335611 s (overhead: 0.000007 %) 10 records
