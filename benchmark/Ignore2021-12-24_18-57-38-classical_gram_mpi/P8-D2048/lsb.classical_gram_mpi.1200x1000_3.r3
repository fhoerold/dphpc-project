# Sysname : Linux
# Nodename: eu-a6-006-16
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:58:46 2021
# Execution time and date (local): Fri Dec 24 18:58:46 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 18975304.816406            0 
            3          1200          1000   gramschmidt           MPI          1 17774353.750000            8 
            3          1200          1000   gramschmidt           MPI          2 17889125.023438            2 
            3          1200          1000   gramschmidt           MPI          3 17618253.429688            0 
            3          1200          1000   gramschmidt           MPI          4 19383327.226562            7 
            3          1200          1000   gramschmidt           MPI          5 17718683.085938            0 
            3          1200          1000   gramschmidt           MPI          6 17459636.767578            0 
            3          1200          1000   gramschmidt           MPI          7 17789496.289062            0 
            3          1200          1000   gramschmidt           MPI          8 17516834.931641            8 
            3          1200          1000   gramschmidt           MPI          9 17641488.654297            0 
# Runtime: 233.137740 s (overhead: 0.000011 %) 10 records
