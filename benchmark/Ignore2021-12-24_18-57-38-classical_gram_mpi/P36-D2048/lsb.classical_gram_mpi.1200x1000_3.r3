# Sysname : Linux
# Nodename: eu-a6-012-05
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 05:51:56 2021
# Execution time and date (local): Sat Dec 25 06:51:56 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 6572325.119141            0 
            3          1200          1000   gramschmidt           MPI          1 6425095.146484            5 
            3          1200          1000   gramschmidt           MPI          2 6556034.425781            2 
            3          1200          1000   gramschmidt           MPI          3 6405568.351562            0 
            3          1200          1000   gramschmidt           MPI          4 6409195.701172            6 
            3          1200          1000   gramschmidt           MPI          5 6588479.314453            0 
            3          1200          1000   gramschmidt           MPI          6 6447243.082031            0 
            3          1200          1000   gramschmidt           MPI          7 6398164.882812            0 
            3          1200          1000   gramschmidt           MPI          8 6430629.613281            2 
            3          1200          1000   gramschmidt           MPI          9 6413577.769531            0 
# Runtime: 90.848246 s (overhead: 0.000017 %) 10 records
