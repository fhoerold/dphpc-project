# Sysname : Linux
# Nodename: eu-a6-012-05
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 05:51:56 2021
# Execution time and date (local): Sat Dec 25 06:51:56 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 6572150.748047            0 
            2          1200          1000   gramschmidt           MPI          1 6424825.378906            8 
            2          1200          1000   gramschmidt           MPI          2 6555830.990234            2 
            2          1200          1000   gramschmidt           MPI          3 6405403.623047            0 
            2          1200          1000   gramschmidt           MPI          4 6408906.093750            6 
            2          1200          1000   gramschmidt           MPI          5 6588246.539062            0 
            2          1200          1000   gramschmidt           MPI          6 6447058.707031            0 
            2          1200          1000   gramschmidt           MPI          7 6397953.238281            0 
            2          1200          1000   gramschmidt           MPI          8 6430404.281250            5 
            2          1200          1000   gramschmidt           MPI          9 6413327.445312            0 
# Runtime: 84.897558 s (overhead: 0.000025 %) 10 records
