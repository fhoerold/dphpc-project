# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 00:50:46 2021
# Execution time and date (local): Sat Dec 25 01:50:46 2021
# MPI execution on rank 2 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 631493.583984            0 
            2          1200          1000   gramschmidt           MPI          1 549296.851562            5 
            2          1200          1000   gramschmidt           MPI          2 549069.964844            2 
            2          1200          1000   gramschmidt           MPI          3 548445.296875            0 
            2          1200          1000   gramschmidt           MPI          4 549508.802734            5 
            2          1200          1000   gramschmidt           MPI          5 553712.634766            0 
            2          1200          1000   gramschmidt           MPI          6 546783.939453            0 
            2          1200          1000   gramschmidt           MPI          7 547873.695312            0 
            2          1200          1000   gramschmidt           MPI          8 550419.441406            2 
            2          1200          1000   gramschmidt           MPI          9 633596.595703            0 
# Runtime: 14.547477 s (overhead: 0.000096 %) 10 records
