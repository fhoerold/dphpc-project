# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 00:50:46 2021
# Execution time and date (local): Sat Dec 25 01:50:46 2021
# MPI execution on rank 3 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 631557.027344            0 
            3          1200          1000   gramschmidt           MPI          1 549526.658203            5 
            3          1200          1000   gramschmidt           MPI          2 549144.423828            2 
            3          1200          1000   gramschmidt           MPI          3 548527.353516            0 
            3          1200          1000   gramschmidt           MPI          4 549622.660156            2 
            3          1200          1000   gramschmidt           MPI          5 553817.804688            0 
            3          1200          1000   gramschmidt           MPI          6 546842.859375            0 
            3          1200          1000   gramschmidt           MPI          7 547958.453125            0 
            3          1200          1000   gramschmidt           MPI          8 550556.724609            4 
            3          1200          1000   gramschmidt           MPI          9 633722.275391            0 
# Runtime: 12.542693 s (overhead: 0.000104 %) 10 records
