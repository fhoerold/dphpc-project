# Sysname : Linux
# Nodename: eu-a6-005-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:59:40 2021
# Execution time and date (local): Fri Dec 24 18:59:40 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 1180211.763672            0 
            3          1200          1000   gramschmidt           MPI          1 1176920.753906            5 
            3          1200          1000   gramschmidt           MPI          2 1178728.876953            2 
            3          1200          1000   gramschmidt           MPI          3 1180047.982422            0 
            3          1200          1000   gramschmidt           MPI          4 1192396.472656            3 
            3          1200          1000   gramschmidt           MPI          5 1185595.388672            1 
            3          1200          1000   gramschmidt           MPI          6 1190432.923828            0 
            3          1200          1000   gramschmidt           MPI          7 1180810.845703            0 
            3          1200          1000   gramschmidt           MPI          8 1190639.958984            5 
            3          1200          1000   gramschmidt           MPI          9 1184522.328125            0 
# Runtime: 22.526117 s (overhead: 0.000071 %) 10 records
