# Sysname : Linux
# Nodename: eu-a6-005-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:59:40 2021
# Execution time and date (local): Fri Dec 24 18:59:40 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 1181089.105469            0 
            2          1200          1000   gramschmidt           MPI          1 1177830.878906            4 
            2          1200          1000   gramschmidt           MPI          2 1179657.912109            2 
            2          1200          1000   gramschmidt           MPI          3 1180991.410156            1 
            2          1200          1000   gramschmidt           MPI          4 1193318.546875            2 
            2          1200          1000   gramschmidt           MPI          5 1186602.546875            0 
            2          1200          1000   gramschmidt           MPI          6 1191376.263672            0 
            2          1200          1000   gramschmidt           MPI          7 1181777.318359            0 
            2          1200          1000   gramschmidt           MPI          8 1191537.214844            6 
            2          1200          1000   gramschmidt           MPI          9 1185479.597656            0 
# Runtime: 23.525629 s (overhead: 0.000064 %) 10 records
