# Sysname : Linux
# Nodename: eu-a6-012-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:58:14 2021
# Execution time and date (local): Fri Dec 24 18:58:14 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 22310.369141            0 
            2          1200          1000   gramschmidt           MPI          1 22265.941406            4 
            2          1200          1000   gramschmidt           MPI          2 23186.609375            1 
            2          1200          1000   gramschmidt           MPI          3 22608.537109            0 
            2          1200          1000   gramschmidt           MPI          4 22306.666016            1 
            2          1200          1000   gramschmidt           MPI          5 21619.003906            0 
            2          1200          1000   gramschmidt           MPI          6 21451.863281            0 
            2          1200          1000   gramschmidt           MPI          7 21438.804688            0 
            2          1200          1000   gramschmidt           MPI          8 21881.476562            1 
            2          1200          1000   gramschmidt           MPI          9 20744.970703            0 
# Runtime: 6.286989 s (overhead: 0.000111 %) 10 records
