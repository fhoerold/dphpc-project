# Sysname : Linux
# Nodename: eu-a6-012-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:58:14 2021
# Execution time and date (local): Fri Dec 24 18:58:14 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 22293.208984            0 
            3          1200          1000   gramschmidt           MPI          1 22251.410156            4 
            3          1200          1000   gramschmidt           MPI          2 23171.931641            1 
            3          1200          1000   gramschmidt           MPI          3 22592.955078            0 
            3          1200          1000   gramschmidt           MPI          4 22288.033203            1 
            3          1200          1000   gramschmidt           MPI          5 21606.449219            0 
            3          1200          1000   gramschmidt           MPI          6 21439.220703            0 
            3          1200          1000   gramschmidt           MPI          7 21417.642578            0 
            3          1200          1000   gramschmidt           MPI          8 21762.367188            1 
            3          1200          1000   gramschmidt           MPI          9 20723.609375            0 
# Runtime: 6.285313 s (overhead: 0.000111 %) 10 records
