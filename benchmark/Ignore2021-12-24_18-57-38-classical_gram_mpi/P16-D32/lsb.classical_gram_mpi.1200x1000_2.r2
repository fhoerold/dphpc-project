# Sysname : Linux
# Nodename: eu-a6-004-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:35:50 2021
# Execution time and date (local): Fri Dec 24 19:35:50 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 631.583984            0 
            2          1200          1000   gramschmidt           MPI          1 540.632812            2 
            2          1200          1000   gramschmidt           MPI          2 503.287109            0 
            2          1200          1000   gramschmidt           MPI          3 498.880859            0 
            2          1200          1000   gramschmidt           MPI          4 447.125000            0 
            2          1200          1000   gramschmidt           MPI          5 461.853516            0 
            2          1200          1000   gramschmidt           MPI          6 442.916016            0 
            2          1200          1000   gramschmidt           MPI          7 440.527344            0 
            2          1200          1000   gramschmidt           MPI          8 475.591797            0 
            2          1200          1000   gramschmidt           MPI          9 412.033203            0 
# Runtime: 9.020399 s (overhead: 0.000022 %) 10 records
