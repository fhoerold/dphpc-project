# Sysname : Linux
# Nodename: eu-a6-004-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:35:50 2021
# Execution time and date (local): Fri Dec 24 19:35:50 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 626.722656            0 
            3          1200          1000   gramschmidt           MPI          1 533.412109            3 
            3          1200          1000   gramschmidt           MPI          2 501.384766            0 
            3          1200          1000   gramschmidt           MPI          3 496.136719            0 
            3          1200          1000   gramschmidt           MPI          4 441.488281            0 
            3          1200          1000   gramschmidt           MPI          5 453.050781            0 
            3          1200          1000   gramschmidt           MPI          6 436.136719            0 
            3          1200          1000   gramschmidt           MPI          7 437.371094            0 
            3          1200          1000   gramschmidt           MPI          8 473.218750            0 
            3          1200          1000   gramschmidt           MPI          9 402.296875            0 
# Runtime: 9.014766 s (overhead: 0.000033 %) 10 records
