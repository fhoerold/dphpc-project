# Sysname : Linux
# Nodename: eu-a6-001-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:59:17 2021
# Execution time and date (local): Fri Dec 24 18:59:17 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 2051.544922            0 
            3          1200          1000   gramschmidt           MPI          1 2075.439453            3 
            3          1200          1000   gramschmidt           MPI          2 1711.712891            0 
            3          1200          1000   gramschmidt           MPI          3 1687.550781            0 
            3          1200          1000   gramschmidt           MPI          4 1646.412109            0 
            3          1200          1000   gramschmidt           MPI          5 1639.460938            0 
            3          1200          1000   gramschmidt           MPI          6 1629.349609            0 
            3          1200          1000   gramschmidt           MPI          7 1599.185547            0 
            3          1200          1000   gramschmidt           MPI          8 1636.109375            0 
            3          1200          1000   gramschmidt           MPI          9 1608.683594            0 
# Runtime: 7.018390 s (overhead: 0.000043 %) 10 records
