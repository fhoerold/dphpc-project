# Sysname : Linux
# Nodename: eu-a6-001-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:59:17 2021
# Execution time and date (local): Fri Dec 24 18:59:17 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 2037.941406            1 
            2          1200          1000   gramschmidt           MPI          1 1721.347656            2 
            2          1200          1000   gramschmidt           MPI          2 1713.472656            0 
            2          1200          1000   gramschmidt           MPI          3 1688.845703            0 
            2          1200          1000   gramschmidt           MPI          4 1648.013672            0 
            2          1200          1000   gramschmidt           MPI          5 1639.679688            0 
            2          1200          1000   gramschmidt           MPI          6 1629.183594            0 
            2          1200          1000   gramschmidt           MPI          7 1600.544922            0 
            2          1200          1000   gramschmidt           MPI          8 1639.265625            0 
            2          1200          1000   gramschmidt           MPI          9 1607.605469            0 
# Runtime: 7.031240 s (overhead: 0.000043 %) 10 records
