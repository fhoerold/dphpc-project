# Sysname : Linux
# Nodename: eu-a6-006-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:40:51 2021
# Execution time and date (local): Fri Dec 24 19:40:51 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 91470874.550781            0 
            2          1200          1000   gramschmidt           MPI          1 92383334.585938            4 
            2          1200          1000   gramschmidt           MPI          2 90280546.138672            6 
            2          1200          1000   gramschmidt           MPI          3 91500120.392578            0 
            2          1200          1000   gramschmidt           MPI          4 89104270.587891            7 
            2          1200          1000   gramschmidt           MPI          5 91053995.382812            2 
            2          1200          1000   gramschmidt           MPI          6 89711183.871094            3 
            2          1200          1000   gramschmidt           MPI          7 91629882.021484            2 
            2          1200          1000   gramschmidt           MPI          8 93207129.365234            6 
            2          1200          1000   gramschmidt           MPI          9 93678879.128906            0 
# Runtime: 1187.686822 s (overhead: 0.000003 %) 10 records
