# Sysname : Linux
# Nodename: eu-a6-006-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:40:51 2021
# Execution time and date (local): Fri Dec 24 19:40:51 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 91474239.412109            0 
            3          1200          1000   gramschmidt           MPI          1 92386644.996094            6 
            3          1200          1000   gramschmidt           MPI          2 90283296.550781            4 
            3          1200          1000   gramschmidt           MPI          3 91503015.830078            2 
            3          1200          1000   gramschmidt           MPI          4 89107214.291016            8 
            3          1200          1000   gramschmidt           MPI          5 91056906.685547            0 
            3          1200          1000   gramschmidt           MPI          6 89713985.574219            2 
            3          1200          1000   gramschmidt           MPI          7 91632844.300781            2 
            3          1200          1000   gramschmidt           MPI          8 93210121.478516            8 
            3          1200          1000   gramschmidt           MPI          9 93681973.484375            4 
# Runtime: 1188.703600 s (overhead: 0.000003 %) 10 records
