# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 00:51:24 2021
# Execution time and date (local): Sat Dec 25 01:51:24 2021
# MPI execution on rank 3 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 48908848.347656            2 
            3          1200          1000   gramschmidt           MPI          1 48246274.851562            5 
            3          1200          1000   gramschmidt           MPI          2 48632419.722656            5 
            3          1200          1000   gramschmidt           MPI          3 48251627.451172            0 
            3          1200          1000   gramschmidt           MPI          4 48612169.005859            7 
            3          1200          1000   gramschmidt           MPI          5 48610131.619141            2 
            3          1200          1000   gramschmidt           MPI          6 48098933.791016            0 
            3          1200          1000   gramschmidt           MPI          7 48712082.458984            0 
            3          1200          1000   gramschmidt           MPI          8 48539597.277344            4 
            3          1200          1000   gramschmidt           MPI          9 48482198.408203            2 
# Runtime: 641.539817 s (overhead: 0.000004 %) 10 records
