# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 00:51:24 2021
# Execution time and date (local): Sat Dec 25 01:51:24 2021
# MPI execution on rank 2 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 48909989.412109            1 
            2          1200          1000   gramschmidt           MPI          1 48247304.156250            6 
            2          1200          1000   gramschmidt           MPI          2 48634011.408203            2 
            2          1200          1000   gramschmidt           MPI          3 48252644.566406            0 
            2          1200          1000   gramschmidt           MPI          4 48613213.820312            6 
            2          1200          1000   gramschmidt           MPI          5 48611073.490234            2 
            2          1200          1000   gramschmidt           MPI          6 48100133.062500            2 
            2          1200          1000   gramschmidt           MPI          7 48713960.236328            0 
            2          1200          1000   gramschmidt           MPI          8 48541168.982422            4 
            2          1200          1000   gramschmidt           MPI          9 48483619.173828            2 
# Runtime: 635.565582 s (overhead: 0.000004 %) 10 records
