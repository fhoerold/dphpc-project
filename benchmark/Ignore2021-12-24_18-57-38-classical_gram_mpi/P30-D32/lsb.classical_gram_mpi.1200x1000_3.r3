# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 00:47:25 2021
# Execution time and date (local): Sat Dec 25 01:47:25 2021
# MPI execution on rank 3 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 1430.460938            0 
            3          1200          1000   gramschmidt           MPI          1 1020.458984            2 
            3          1200          1000   gramschmidt           MPI          2 1011.488281            0 
            3          1200          1000   gramschmidt           MPI          3 966.130859            0 
            3          1200          1000   gramschmidt           MPI          4 939.023438            0 
            3          1200          1000   gramschmidt           MPI          5 967.611328            0 
            3          1200          1000   gramschmidt           MPI          6 936.916016            0 
            3          1200          1000   gramschmidt           MPI          7 925.076172            0 
            3          1200          1000   gramschmidt           MPI          8 935.332031            0 
            3          1200          1000   gramschmidt           MPI          9 922.736328            0 
# Runtime: 10.981325 s (overhead: 0.000018 %) 10 records
