# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 00:47:25 2021
# Execution time and date (local): Sat Dec 25 01:47:25 2021
# MPI execution on rank 2 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 1380.373047            0 
            2          1200          1000   gramschmidt           MPI          1 1036.271484            2 
            2          1200          1000   gramschmidt           MPI          2 1027.076172            0 
            2          1200          1000   gramschmidt           MPI          3 981.164062            0 
            2          1200          1000   gramschmidt           MPI          4 947.085938            0 
            2          1200          1000   gramschmidt           MPI          5 983.675781            0 
            2          1200          1000   gramschmidt           MPI          6 955.808594            0 
            2          1200          1000   gramschmidt           MPI          7 940.929688            0 
            2          1200          1000   gramschmidt           MPI          8 941.867188            0 
            2          1200          1000   gramschmidt           MPI          9 933.550781            0 
# Runtime: 10.021961 s (overhead: 0.000020 %) 10 records
