# Sysname : Linux
# Nodename: eu-a6-005-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:58:14 2021
# Execution time and date (local): Fri Dec 24 18:58:14 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 887.345703            0 
            2          1200          1000   gramschmidt           MPI          1 902.126953            0 
            2          1200          1000   gramschmidt           MPI          2 958.738281            0 
            2          1200          1000   gramschmidt           MPI          3 887.525391            0 
            2          1200          1000   gramschmidt           MPI          4 866.666016            0 
            2          1200          1000   gramschmidt           MPI          5 922.445312            0 
            2          1200          1000   gramschmidt           MPI          6 871.712891            0 
            2          1200          1000   gramschmidt           MPI          7 890.261719            0 
            2          1200          1000   gramschmidt           MPI          8 879.781250            0 
            2          1200          1000   gramschmidt           MPI          9 856.824219            0 
# Runtime: 0.016380 s (overhead: 0.000000 %) 10 records
