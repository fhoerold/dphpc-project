# Sysname : Linux
# Nodename: eu-a6-005-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:58:14 2021
# Execution time and date (local): Fri Dec 24 18:58:14 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 873.689453            0 
            3          1200          1000   gramschmidt           MPI          1 898.185547            0 
            3          1200          1000   gramschmidt           MPI          2 957.287109            0 
            3          1200          1000   gramschmidt           MPI          3 884.167969            0 
            3          1200          1000   gramschmidt           MPI          4 859.277344            0 
            3          1200          1000   gramschmidt           MPI          5 915.775391            0 
            3          1200          1000   gramschmidt           MPI          6 868.662109            0 
            3          1200          1000   gramschmidt           MPI          7 887.765625            0 
            3          1200          1000   gramschmidt           MPI          8 881.589844            0 
            3          1200          1000   gramschmidt           MPI          9 855.703125            0 
# Runtime: 0.013620 s (overhead: 0.000000 %) 10 records
