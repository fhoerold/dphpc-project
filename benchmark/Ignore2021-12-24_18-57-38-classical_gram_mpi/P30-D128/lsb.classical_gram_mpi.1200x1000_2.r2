# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 00:47:46 2021
# Execution time and date (local): Sat Dec 25 01:47:46 2021
# MPI execution on rank 2 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 7777.275391            0 
            2          1200          1000   gramschmidt           MPI          1 7638.621094            3 
            2          1200          1000   gramschmidt           MPI          2 4901.107422            0 
            2          1200          1000   gramschmidt           MPI          3 4864.294922            0 
            2          1200          1000   gramschmidt           MPI          4 4858.740234            0 
            2          1200          1000   gramschmidt           MPI          5 4890.740234            0 
            2          1200          1000   gramschmidt           MPI          6 4792.039062            0 
            2          1200          1000   gramschmidt           MPI          7 4819.246094            0 
            2          1200          1000   gramschmidt           MPI          8 4788.478516            0 
            2          1200          1000   gramschmidt           MPI          9 4819.150391            0 
# Runtime: 7.067116 s (overhead: 0.000042 %) 10 records
