# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 00:47:46 2021
# Execution time and date (local): Sat Dec 25 01:47:46 2021
# MPI execution on rank 3 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 7744.908203            0 
            3          1200          1000   gramschmidt           MPI          1 7636.748047            3 
            3          1200          1000   gramschmidt           MPI          2 4899.062500            0 
            3          1200          1000   gramschmidt           MPI          3 4861.623047            0 
            3          1200          1000   gramschmidt           MPI          4 4859.861328            0 
            3          1200          1000   gramschmidt           MPI          5 4887.857422            0 
            3          1200          1000   gramschmidt           MPI          6 4791.222656            0 
            3          1200          1000   gramschmidt           MPI          7 4816.626953            0 
            3          1200          1000   gramschmidt           MPI          8 4789.398438            0 
            3          1200          1000   gramschmidt           MPI          9 5132.052734            0 
# Runtime: 1.067365 s (overhead: 0.000281 %) 10 records
