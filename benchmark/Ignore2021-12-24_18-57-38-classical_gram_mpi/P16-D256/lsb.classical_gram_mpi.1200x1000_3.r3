# Sysname : Linux
# Nodename: eu-a6-002-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:37:20 2021
# Execution time and date (local): Fri Dec 24 19:37:20 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 26224.626953            0 
            3          1200          1000   gramschmidt           MPI          1 25075.642578            5 
            3          1200          1000   gramschmidt           MPI          2 24535.251953            0 
            3          1200          1000   gramschmidt           MPI          3 17318.445312            0 
            3          1200          1000   gramschmidt           MPI          4 17072.855469            0 
            3          1200          1000   gramschmidt           MPI          5 17071.615234            0 
            3          1200          1000   gramschmidt           MPI          6 17204.369141            0 
            3          1200          1000   gramschmidt           MPI          7 16970.121094            0 
            3          1200          1000   gramschmidt           MPI          8 16929.220703            0 
            3          1200          1000   gramschmidt           MPI          9 16983.019531            0 
# Runtime: 9.277436 s (overhead: 0.000054 %) 10 records
