# Sysname : Linux
# Nodename: eu-a6-002-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:37:20 2021
# Execution time and date (local): Fri Dec 24 19:37:20 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 26237.220703            0 
            2          1200          1000   gramschmidt           MPI          1 25096.115234            3 
            2          1200          1000   gramschmidt           MPI          2 24547.970703            1 
            2          1200          1000   gramschmidt           MPI          3 17320.605469            0 
            2          1200          1000   gramschmidt           MPI          4 17083.857422            1 
            2          1200          1000   gramschmidt           MPI          5 17091.425781            0 
            2          1200          1000   gramschmidt           MPI          6 17216.289062            0 
            2          1200          1000   gramschmidt           MPI          7 16983.400391            0 
            2          1200          1000   gramschmidt           MPI          8 16942.919922            1 
            2          1200          1000   gramschmidt           MPI          9 16995.640625            0 
# Runtime: 7.285976 s (overhead: 0.000082 %) 10 records
