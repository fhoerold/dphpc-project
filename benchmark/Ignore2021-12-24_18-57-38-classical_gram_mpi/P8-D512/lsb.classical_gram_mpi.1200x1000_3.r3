# Sysname : Linux
# Nodename: eu-a6-012-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:58:14 2021
# Execution time and date (local): Fri Dec 24 18:58:14 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 164324.357422            0 
            3          1200          1000   gramschmidt           MPI          1 164758.298828            1 
            3          1200          1000   gramschmidt           MPI          2 171599.250000            3 
            3          1200          1000   gramschmidt           MPI          3 165391.089844            0 
            3          1200          1000   gramschmidt           MPI          4 165188.630859            1 
            3          1200          1000   gramschmidt           MPI          5 167218.035156            0 
            3          1200          1000   gramschmidt           MPI          6 167384.435547            0 
            3          1200          1000   gramschmidt           MPI          7 167483.943359            0 
            3          1200          1000   gramschmidt           MPI          8 169750.619141            2 
            3          1200          1000   gramschmidt           MPI          9 165861.873047            0 
# Runtime: 2.174466 s (overhead: 0.000322 %) 10 records
