# Sysname : Linux
# Nodename: eu-a6-012-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:58:14 2021
# Execution time and date (local): Fri Dec 24 18:58:14 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 164229.732422            1 
            2          1200          1000   gramschmidt           MPI          1 164679.582031            5 
            2          1200          1000   gramschmidt           MPI          2 171500.349609            4 
            2          1200          1000   gramschmidt           MPI          3 165281.822266            0 
            2          1200          1000   gramschmidt           MPI          4 165091.634766            1 
            2          1200          1000   gramschmidt           MPI          5 167117.835938            0 
            2          1200          1000   gramschmidt           MPI          6 167291.308594            0 
            2          1200          1000   gramschmidt           MPI          7 167396.818359            0 
            2          1200          1000   gramschmidt           MPI          8 169654.183594            1 
            2          1200          1000   gramschmidt           MPI          9 165795.306641            0 
# Runtime: 6.179763 s (overhead: 0.000194 %) 10 records
