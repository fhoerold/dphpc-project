# Sysname : Linux
# Nodename: eu-a6-002-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:37:37 2021
# Execution time and date (local): Fri Dec 24 19:37:37 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 1128828.171875            0 
            3          1200          1000   gramschmidt           MPI          1 1133729.593750            5 
            3          1200          1000   gramschmidt           MPI          2 1126764.281250            2 
            3          1200          1000   gramschmidt           MPI          3 1122711.085938            0 
            3          1200          1000   gramschmidt           MPI          4 1125594.673828            5 
            3          1200          1000   gramschmidt           MPI          5 1136475.386719            0 
            3          1200          1000   gramschmidt           MPI          6 1127292.798828            0 
            3          1200          1000   gramschmidt           MPI          7 1127473.763672            0 
            3          1200          1000   gramschmidt           MPI          8 1144899.306641            2 
            3          1200          1000   gramschmidt           MPI          9 1156905.402344            0 
# Runtime: 20.715655 s (overhead: 0.000068 %) 10 records
