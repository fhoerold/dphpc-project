# Sysname : Linux
# Nodename: eu-a6-002-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:37:37 2021
# Execution time and date (local): Fri Dec 24 19:37:37 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 1128512.308594            0 
            2          1200          1000   gramschmidt           MPI          1 1133372.841797            5 
            2          1200          1000   gramschmidt           MPI          2 1126416.757812            2 
            2          1200          1000   gramschmidt           MPI          3 1122385.623047            0 
            2          1200          1000   gramschmidt           MPI          4 1125221.664062            2 
            2          1200          1000   gramschmidt           MPI          5 1136125.306641            0 
            2          1200          1000   gramschmidt           MPI          6 1126940.638672            0 
            2          1200          1000   gramschmidt           MPI          7 1127119.933594            0 
            2          1200          1000   gramschmidt           MPI          8 1144644.417969            4 
            2          1200          1000   gramschmidt           MPI          9 1156528.205078            0 
# Runtime: 23.716418 s (overhead: 0.000055 %) 10 records
