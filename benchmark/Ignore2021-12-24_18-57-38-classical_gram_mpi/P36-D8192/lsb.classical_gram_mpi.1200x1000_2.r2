# Sysname : Linux
# Nodename: eu-a6-004-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 06:06:58 2021
# Execution time and date (local): Sat Dec 25 07:06:58 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 541242381.986328            2 
            2          1200          1000   gramschmidt           MPI          1 514478191.994141            9 
            2          1200          1000   gramschmidt           MPI          2 532102208.041016            9 
            2          1200          1000   gramschmidt           MPI          3 537718306.939453            2 
            2          1200          1000   gramschmidt           MPI          4 542474254.273438            7 
            2          1200          1000   gramschmidt           MPI          5 526753068.951172            3 
            2          1200          1000   gramschmidt           MPI          6 523967371.656250            3 
            2          1200          1000   gramschmidt           MPI          7 542531425.921875            3 
            2          1200          1000   gramschmidt           MPI          8 513894762.000000            9 
            2          1200          1000   gramschmidt           MPI          9 547463551.201172            2 
# Runtime: 6946.360530 s (overhead: 0.000001 %) 10 records
