# Sysname : Linux
# Nodename: eu-a6-004-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 06:06:58 2021
# Execution time and date (local): Sat Dec 25 07:06:58 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 540878345.626953            1 
            3          1200          1000   gramschmidt           MPI          1 514132312.025391            7 
            3          1200          1000   gramschmidt           MPI          2 531744299.775391           12 
            3          1200          1000   gramschmidt           MPI          3 537356624.011719            3 
            3          1200          1000   gramschmidt           MPI          4 542109438.654297            7 
            3          1200          1000   gramschmidt           MPI          5 526399030.281250            4 
            3          1200          1000   gramschmidt           MPI          6 523615182.289062            4 
            3          1200          1000   gramschmidt           MPI          7 542166504.564453            2 
            3          1200          1000   gramschmidt           MPI          8 513549217.093750            7 
            3          1200          1000   gramschmidt           MPI          9 547095201.187500            1 
# Runtime: 6944.646077 s (overhead: 0.000001 %) 10 records
