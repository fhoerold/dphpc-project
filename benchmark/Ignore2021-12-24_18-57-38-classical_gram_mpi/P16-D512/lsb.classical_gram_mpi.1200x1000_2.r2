# Sysname : Linux
# Nodename: eu-a6-004-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:36:38 2021
# Execution time and date (local): Fri Dec 24 19:36:38 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 143851.232422            0 
            2          1200          1000   gramschmidt           MPI          1 121320.666016            4 
            2          1200          1000   gramschmidt           MPI          2 144555.466797            1 
            2          1200          1000   gramschmidt           MPI          3 131639.560547            0 
            2          1200          1000   gramschmidt           MPI          4 124296.824219            1 
            2          1200          1000   gramschmidt           MPI          5 111352.632812            0 
            2          1200          1000   gramschmidt           MPI          6 110207.833984            0 
            2          1200          1000   gramschmidt           MPI          7 109247.792969            0 
            2          1200          1000   gramschmidt           MPI          8 109642.832031            1 
            2          1200          1000   gramschmidt           MPI          9 109114.240234            0 
# Runtime: 5.619281 s (overhead: 0.000125 %) 10 records
