# Sysname : Linux
# Nodename: eu-a6-004-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:36:38 2021
# Execution time and date (local): Fri Dec 24 19:36:38 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 143844.966797            0 
            3          1200          1000   gramschmidt           MPI          1 121333.566406            4 
            3          1200          1000   gramschmidt           MPI          2 144565.636719            2 
            3          1200          1000   gramschmidt           MPI          3 131622.431641            0 
            3          1200          1000   gramschmidt           MPI          4 124309.462891            2 
            3          1200          1000   gramschmidt           MPI          5 111370.193359            0 
            3          1200          1000   gramschmidt           MPI          6 110184.572266            0 
            3          1200          1000   gramschmidt           MPI          7 109222.031250            0 
            3          1200          1000   gramschmidt           MPI          8 109622.746094            2 
            3          1200          1000   gramschmidt           MPI          9 109075.646484            0 
# Runtime: 9.611168 s (overhead: 0.000104 %) 10 records
