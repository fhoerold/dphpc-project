# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 01:02:22 2021
# Execution time and date (local): Sat Dec 25 02:02:22 2021
# MPI execution on rank 3 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 612650369.855469            1 
            3          1200          1000   gramschmidt           MPI          1 595111103.099609            6 
            3          1200          1000   gramschmidt           MPI          2 601124574.273438            8 
            3          1200          1000   gramschmidt           MPI          3 600937481.158203            0 
            3          1200          1000   gramschmidt           MPI          4 599126366.664062           11 
            3          1200          1000   gramschmidt           MPI          5 597908281.455078            5 
            3          1200          1000   gramschmidt           MPI          6 598611609.156250            3 
            3          1200          1000   gramschmidt           MPI          7 599560106.076172            3 
            3          1200          1000   gramschmidt           MPI          8 595969602.208984            8 
            3          1200          1000   gramschmidt           MPI          9 599117613.603516            2 
# Runtime: 7889.019285 s (overhead: 0.000001 %) 10 records
