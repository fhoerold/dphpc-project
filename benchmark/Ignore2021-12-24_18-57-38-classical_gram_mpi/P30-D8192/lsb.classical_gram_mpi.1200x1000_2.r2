# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 01:02:22 2021
# Execution time and date (local): Sat Dec 25 02:02:22 2021
# MPI execution on rank 2 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 613324710.958984            2 
            2          1200          1000   gramschmidt           MPI          1 595765937.841797            7 
            2          1200          1000   gramschmidt           MPI          2 601786165.863281            6 
            2          1200          1000   gramschmidt           MPI          3 601598460.585938            2 
            2          1200          1000   gramschmidt           MPI          4 599785329.037109            5 
            2          1200          1000   gramschmidt           MPI          5 598565888.958984            5 
            2          1200          1000   gramschmidt           MPI          6 599270584.404297            1 
            2          1200          1000   gramschmidt           MPI          7 600219903.474609            4 
            2          1200          1000   gramschmidt           MPI          8 596625301.923828            8 
            2          1200          1000   gramschmidt           MPI          9 599776988.027344            3 
# Runtime: 7885.785957 s (overhead: 0.000001 %) 10 records
