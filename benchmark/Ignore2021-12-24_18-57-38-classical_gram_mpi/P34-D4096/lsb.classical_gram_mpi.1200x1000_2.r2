# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 03:22:43 2021
# Execution time and date (local): Sat Dec 25 04:22:43 2021
# MPI execution on rank 2 with 34 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 45605272.283203            0 
            2          1200          1000   gramschmidt           MPI          1 45739915.681641            4 
            2          1200          1000   gramschmidt           MPI          2 44849256.939453            6 
            2          1200          1000   gramschmidt           MPI          3 45287485.642578            2 
            2          1200          1000   gramschmidt           MPI          4 45251943.335938            5 
            2          1200          1000   gramschmidt           MPI          5 45570098.039062            3 
            2          1200          1000   gramschmidt           MPI          6 45665900.351562            2 
            2          1200          1000   gramschmidt           MPI          7 45729853.919922            3 
            2          1200          1000   gramschmidt           MPI          8 46058674.433594            6 
            2          1200          1000   gramschmidt           MPI          9 45960954.902344            2 
# Runtime: 602.679798 s (overhead: 0.000005 %) 10 records
