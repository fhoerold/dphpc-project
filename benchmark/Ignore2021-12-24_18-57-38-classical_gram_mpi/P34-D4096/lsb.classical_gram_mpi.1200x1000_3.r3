# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 03:22:43 2021
# Execution time and date (local): Sat Dec 25 04:22:43 2021
# MPI execution on rank 3 with 34 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 45606283.884766            0 
            3          1200          1000   gramschmidt           MPI          1 45740459.464844            6 
            3          1200          1000   gramschmidt           MPI          2 44850383.349609            5 
            3          1200          1000   gramschmidt           MPI          3 45288502.867188            0 
            3          1200          1000   gramschmidt           MPI          4 45253121.078125            6 
            3          1200          1000   gramschmidt           MPI          5 45570737.382812            1 
            3          1200          1000   gramschmidt           MPI          6 45666983.765625            1 
            3          1200          1000   gramschmidt           MPI          7 45731026.232422            2 
            3          1200          1000   gramschmidt           MPI          8 46059663.142578            6 
            3          1200          1000   gramschmidt           MPI          9 45961745.251953            2 
# Runtime: 602.683503 s (overhead: 0.000005 %) 10 records
