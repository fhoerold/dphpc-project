# Sysname : Linux
# Nodename: eu-a6-001-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:00:06 2021
# Execution time and date (local): Fri Dec 24 19:00:06 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 11285524.990234            0 
            3          1200          1000   gramschmidt           MPI          1 11359143.087891            4 
            3          1200          1000   gramschmidt           MPI          2 11578569.857422            2 
            3          1200          1000   gramschmidt           MPI          3 11415998.033203            2 
            3          1200          1000   gramschmidt           MPI          4 11462896.476562            6 
            3          1200          1000   gramschmidt           MPI          5 11446560.140625            0 
            3          1200          1000   gramschmidt           MPI          6 11352399.093750            0 
            3          1200          1000   gramschmidt           MPI          7 11344427.085938            0 
            3          1200          1000   gramschmidt           MPI          8 11384477.691406            7 
            3          1200          1000   gramschmidt           MPI          9 11376379.300781            0 
# Runtime: 148.295156 s (overhead: 0.000014 %) 10 records
