# Sysname : Linux
# Nodename: eu-a6-001-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:00:06 2021
# Execution time and date (local): Fri Dec 24 19:00:06 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 11291591.490234            0 
            2          1200          1000   gramschmidt           MPI          1 11364849.037109            8 
            2          1200          1000   gramschmidt           MPI          2 11584666.916016            2 
            2          1200          1000   gramschmidt           MPI          3 11422102.927734            0 
            2          1200          1000   gramschmidt           MPI          4 11468663.960938            8 
            2          1200          1000   gramschmidt           MPI          5 11452697.525391            0 
            2          1200          1000   gramschmidt           MPI          6 11358413.687500            0 
            2          1200          1000   gramschmidt           MPI          7 11350473.699219            0 
            2          1200          1000   gramschmidt           MPI          8 11390554.939453            2 
            2          1200          1000   gramschmidt           MPI          9 11382149.003906            0 
# Runtime: 152.398906 s (overhead: 0.000013 %) 10 records
