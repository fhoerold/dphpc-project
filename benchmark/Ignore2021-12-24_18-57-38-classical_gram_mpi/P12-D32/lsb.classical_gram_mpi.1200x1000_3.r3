# Sysname : Linux
# Nodename: eu-a6-005-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:59:18 2021
# Execution time and date (local): Fri Dec 24 18:59:18 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 527.714844            0 
            3          1200          1000   gramschmidt           MPI          1 761.099609            0 
            3          1200          1000   gramschmidt           MPI          2 487.164062            0 
            3          1200          1000   gramschmidt           MPI          3 489.597656            0 
            3          1200          1000   gramschmidt           MPI          4 450.382812            0 
            3          1200          1000   gramschmidt           MPI          5 442.005859            0 
            3          1200          1000   gramschmidt           MPI          6 463.371094            0 
            3          1200          1000   gramschmidt           MPI          7 451.746094            0 
            3          1200          1000   gramschmidt           MPI          8 482.304688            0 
            3          1200          1000   gramschmidt           MPI          9 451.208984            0 
# Runtime: 0.011276 s (overhead: 0.000000 %) 10 records
