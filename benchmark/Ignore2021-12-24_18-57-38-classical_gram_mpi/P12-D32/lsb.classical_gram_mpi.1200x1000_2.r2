# Sysname : Linux
# Nodename: eu-a6-005-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:59:18 2021
# Execution time and date (local): Fri Dec 24 18:59:18 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 528.156250            0 
            2          1200          1000   gramschmidt           MPI          1 489.392578            0 
            2          1200          1000   gramschmidt           MPI          2 502.898438            0 
            2          1200          1000   gramschmidt           MPI          3 501.390625            0 
            2          1200          1000   gramschmidt           MPI          4 457.546875            0 
            2          1200          1000   gramschmidt           MPI          5 444.324219            0 
            2          1200          1000   gramschmidt           MPI          6 472.507812            0 
            2          1200          1000   gramschmidt           MPI          7 459.113281            0 
            2          1200          1000   gramschmidt           MPI          8 490.226562            0 
            2          1200          1000   gramschmidt           MPI          9 459.074219            0 
# Runtime: 0.008860 s (overhead: 0.000000 %) 10 records
