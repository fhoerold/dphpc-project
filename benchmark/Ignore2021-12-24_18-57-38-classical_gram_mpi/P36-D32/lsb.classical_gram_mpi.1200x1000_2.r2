# Sysname : Linux
# Nodename: eu-a6-012-05
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 05:50:28 2021
# Execution time and date (local): Sat Dec 25 06:50:28 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 1064.894531            0 
            2          1200          1000   gramschmidt           MPI          1 980.023438            0 
            2          1200          1000   gramschmidt           MPI          2 979.777344            0 
            2          1200          1000   gramschmidt           MPI          3 955.867188            0 
            2          1200          1000   gramschmidt           MPI          4 947.150391            0 
            2          1200          1000   gramschmidt           MPI          5 906.437500            0 
            2          1200          1000   gramschmidt           MPI          6 917.484375            0 
            2          1200          1000   gramschmidt           MPI          7 890.792969            0 
            2          1200          1000   gramschmidt           MPI          8 934.111328            0 
            2          1200          1000   gramschmidt           MPI          9 918.005859            0 
# Runtime: 0.017724 s (overhead: 0.000000 %) 10 records
