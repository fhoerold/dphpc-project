# Sysname : Linux
# Nodename: eu-a6-012-05
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 05:50:28 2021
# Execution time and date (local): Sat Dec 25 06:50:28 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 1389.751953            0 
            3          1200          1000   gramschmidt           MPI          1 1253.015625            3 
            3          1200          1000   gramschmidt           MPI          2 966.988281            0 
            3          1200          1000   gramschmidt           MPI          3 941.279297            0 
            3          1200          1000   gramschmidt           MPI          4 929.195312            0 
            3          1200          1000   gramschmidt           MPI          5 891.025391            0 
            3          1200          1000   gramschmidt           MPI          6 911.175781            0 
            3          1200          1000   gramschmidt           MPI          7 884.914062            0 
            3          1200          1000   gramschmidt           MPI          8 920.498047            0 
            3          1200          1000   gramschmidt           MPI          9 905.658203            0 
# Runtime: 13.959813 s (overhead: 0.000021 %) 10 records
