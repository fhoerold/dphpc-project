# Sysname : Linux
# Nodename: eu-a6-001-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:59:30 2021
# Execution time and date (local): Fri Dec 24 18:59:30 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 20097.210938            0 
            3          1200          1000   gramschmidt           MPI          1 20069.189453            3 
            3          1200          1000   gramschmidt           MPI          2 20662.609375            1 
            3          1200          1000   gramschmidt           MPI          3 19731.470703            0 
            3          1200          1000   gramschmidt           MPI          4 20253.111328            0 
            3          1200          1000   gramschmidt           MPI          5 19974.042969            0 
            3          1200          1000   gramschmidt           MPI          6 19788.869141            0 
            3          1200          1000   gramschmidt           MPI          7 19763.841797            0 
            3          1200          1000   gramschmidt           MPI          8 19851.615234            0 
            3          1200          1000   gramschmidt           MPI          9 19947.673828            0 
# Runtime: 5.267455 s (overhead: 0.000076 %) 10 records
