# Sysname : Linux
# Nodename: eu-a6-001-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:59:30 2021
# Execution time and date (local): Fri Dec 24 18:59:30 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 20442.322266            0 
            2          1200          1000   gramschmidt           MPI          1 20095.396484            3 
            2          1200          1000   gramschmidt           MPI          2 20677.152344            0 
            2          1200          1000   gramschmidt           MPI          3 19742.583984            0 
            2          1200          1000   gramschmidt           MPI          4 20275.210938            0 
            2          1200          1000   gramschmidt           MPI          5 19980.208984            0 
            2          1200          1000   gramschmidt           MPI          6 19814.123047            0 
            2          1200          1000   gramschmidt           MPI          7 19768.869141            0 
            2          1200          1000   gramschmidt           MPI          8 19863.318359            1 
            2          1200          1000   gramschmidt           MPI          9 19955.394531            0 
# Runtime: 12.261486 s (overhead: 0.000033 %) 10 records
