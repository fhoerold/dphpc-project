# Sysname : Linux
# Nodename: eu-a6-007-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:06:04 2021
# Execution time and date (local): Fri Dec 24 19:06:04 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 332052.814453            0 
            3          1200          1000   gramschmidt           MPI          1 325520.357422            5 
            3          1200          1000   gramschmidt           MPI          2 361328.576172            2 
            3          1200          1000   gramschmidt           MPI          3 328030.142578            0 
            3          1200          1000   gramschmidt           MPI          4 357305.500000            6 
            3          1200          1000   gramschmidt           MPI          5 326907.734375            0 
            3          1200          1000   gramschmidt           MPI          6 327794.689453            0 
            3          1200          1000   gramschmidt           MPI          7 325557.781250            0 
            3          1200          1000   gramschmidt           MPI          8 325260.841797            1 
            3          1200          1000   gramschmidt           MPI          9 325030.021484            0 
# Runtime: 10.320813 s (overhead: 0.000136 %) 10 records
