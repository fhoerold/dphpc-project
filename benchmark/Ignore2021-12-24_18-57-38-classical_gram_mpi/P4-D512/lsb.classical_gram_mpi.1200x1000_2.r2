# Sysname : Linux
# Nodename: eu-a6-007-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:06:04 2021
# Execution time and date (local): Fri Dec 24 19:06:04 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 331895.007812            0 
            2          1200          1000   gramschmidt           MPI          1 325387.669922            5 
            2          1200          1000   gramschmidt           MPI          2 361210.863281            2 
            2          1200          1000   gramschmidt           MPI          3 327907.933594            0 
            2          1200          1000   gramschmidt           MPI          4 357177.484375            2 
            2          1200          1000   gramschmidt           MPI          5 326783.980469            0 
            2          1200          1000   gramschmidt           MPI          6 327672.011719            0 
            2          1200          1000   gramschmidt           MPI          7 325448.455078            0 
            2          1200          1000   gramschmidt           MPI          8 325146.349609            2 
            2          1200          1000   gramschmidt           MPI          9 324914.945312            0 
# Runtime: 11.321053 s (overhead: 0.000097 %) 10 records
