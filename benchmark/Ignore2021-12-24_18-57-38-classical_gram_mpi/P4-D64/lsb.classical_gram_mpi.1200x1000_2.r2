# Sysname : Linux
# Nodename: eu-a6-007-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:05:47 2021
# Execution time and date (local): Fri Dec 24 19:05:47 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 1054.470703            0 
            2          1200          1000   gramschmidt           MPI          1 1047.851562            2 
            2          1200          1000   gramschmidt           MPI          2 1033.486328            0 
            2          1200          1000   gramschmidt           MPI          3 1018.498047            0 
            2          1200          1000   gramschmidt           MPI          4 1010.968750            0 
            2          1200          1000   gramschmidt           MPI          5 1024.480469            0 
            2          1200          1000   gramschmidt           MPI          6 1044.636719            0 
            2          1200          1000   gramschmidt           MPI          7 1014.521484            0 
            2          1200          1000   gramschmidt           MPI          8 1026.894531            0 
            2          1200          1000   gramschmidt           MPI          9 1014.767578            0 
# Runtime: 8.015102 s (overhead: 0.000025 %) 10 records
