# Sysname : Linux
# Nodename: eu-a6-007-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:05:47 2021
# Execution time and date (local): Fri Dec 24 19:05:47 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 1060.685547            0 
            3          1200          1000   gramschmidt           MPI          1 1075.158203            0 
            3          1200          1000   gramschmidt           MPI          2 1029.361328            0 
            3          1200          1000   gramschmidt           MPI          3 1021.613281            0 
            3          1200          1000   gramschmidt           MPI          4 1020.412109            0 
            3          1200          1000   gramschmidt           MPI          5 1034.097656            0 
            3          1200          1000   gramschmidt           MPI          6 1052.837891            0 
            3          1200          1000   gramschmidt           MPI          7 1026.310547            0 
            3          1200          1000   gramschmidt           MPI          8 1034.492188            0 
            3          1200          1000   gramschmidt           MPI          9 1024.853516            0 
# Runtime: 0.015473 s (overhead: 0.000000 %) 10 records
