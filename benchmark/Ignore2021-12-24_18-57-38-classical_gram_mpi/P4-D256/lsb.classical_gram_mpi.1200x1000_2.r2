# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:05:55 2021
# Execution time and date (local): Fri Dec 24 19:05:55 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 36807.492188            0 
            2          1200          1000   gramschmidt           MPI          1 36822.804688            4 
            2          1200          1000   gramschmidt           MPI          2 36816.259766            1 
            2          1200          1000   gramschmidt           MPI          3 37470.759766            0 
            2          1200          1000   gramschmidt           MPI          4 36956.529297            1 
            2          1200          1000   gramschmidt           MPI          5 36872.396484            0 
            2          1200          1000   gramschmidt           MPI          6 37075.398438            0 
            2          1200          1000   gramschmidt           MPI          7 36792.595703            0 
            2          1200          1000   gramschmidt           MPI          8 36882.857422            1 
            2          1200          1000   gramschmidt           MPI          9 37021.791016            0 
# Runtime: 1.475882 s (overhead: 0.000474 %) 10 records
