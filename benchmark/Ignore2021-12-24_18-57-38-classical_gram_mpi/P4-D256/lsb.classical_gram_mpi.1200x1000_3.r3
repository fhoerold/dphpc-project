# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:05:55 2021
# Execution time and date (local): Fri Dec 24 19:05:55 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 36720.330078            0 
            3          1200          1000   gramschmidt           MPI          1 36711.966797            4 
            3          1200          1000   gramschmidt           MPI          2 36708.384766            1 
            3          1200          1000   gramschmidt           MPI          3 37383.164062            0 
            3          1200          1000   gramschmidt           MPI          4 36878.089844            1 
            3          1200          1000   gramschmidt           MPI          5 36668.546875            0 
            3          1200          1000   gramschmidt           MPI          6 36942.847656            0 
            3          1200          1000   gramschmidt           MPI          7 36676.474609            0 
            3          1200          1000   gramschmidt           MPI          8 36755.279297            1 
            3          1200          1000   gramschmidt           MPI          9 36899.593750            0 
# Runtime: 1.475943 s (overhead: 0.000474 %) 10 records
