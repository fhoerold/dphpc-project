# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 03:33:23 2021
# Execution time and date (local): Sat Dec 25 04:33:23 2021
# MPI execution on rank 3 with 34 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 586573738.037109            3 
            3          1200          1000   gramschmidt           MPI          1 550445892.736328            4 
            3          1200          1000   gramschmidt           MPI          2 557275511.025391           13 
            3          1200          1000   gramschmidt           MPI          3 552510839.490234            2 
            3          1200          1000   gramschmidt           MPI          4 560605510.982422           12 
            3          1200          1000   gramschmidt           MPI          5 603809985.972656            3 
            3          1200          1000   gramschmidt           MPI          6 567052519.029297            4 
            3          1200          1000   gramschmidt           MPI          7 578085509.572266            4 
            3          1200          1000   gramschmidt           MPI          8 568054432.703125            9 
            3          1200          1000   gramschmidt           MPI          9 583384989.261719            0 
# Runtime: 7420.107465 s (overhead: 0.000001 %) 10 records
