# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 03:33:23 2021
# Execution time and date (local): Sat Dec 25 04:33:23 2021
# MPI execution on rank 2 with 34 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 586440504.457031            4 
            2          1200          1000   gramschmidt           MPI          1 550319726.244141           13 
            2          1200          1000   gramschmidt           MPI          2 557148104.890625           17 
            2          1200          1000   gramschmidt           MPI          3 552384430.212891            3 
            2          1200          1000   gramschmidt           MPI          4 560477520.287109           11 
            2          1200          1000   gramschmidt           MPI          5 603672261.933594            2 
            2          1200          1000   gramschmidt           MPI          6 566923151.816406            2 
            2          1200          1000   gramschmidt           MPI          7 577953427.255859            3 
            2          1200          1000   gramschmidt           MPI          8 567924641.695312           16 
            2          1200          1000   gramschmidt           MPI          9 583251698.904297            2 
# Runtime: 7421.460796 s (overhead: 0.000001 %) 10 records
