# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 00:48:24 2021
# Execution time and date (local): Sat Dec 25 01:48:24 2021
# MPI execution on rank 3 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 113724.269531            0 
            3          1200          1000   gramschmidt           MPI          1 140479.656250            4 
            3          1200          1000   gramschmidt           MPI          2 122842.808594            2 
            3          1200          1000   gramschmidt           MPI          3 125209.529297            0 
            3          1200          1000   gramschmidt           MPI          4 129833.259766            1 
            3          1200          1000   gramschmidt           MPI          5 110706.384766            0 
            3          1200          1000   gramschmidt           MPI          6 109039.564453            0 
            3          1200          1000   gramschmidt           MPI          7 109779.396484            0 
            3          1200          1000   gramschmidt           MPI          8 109660.835938            1 
            3          1200          1000   gramschmidt           MPI          9 109345.023438            0 
# Runtime: 16.582150 s (overhead: 0.000048 %) 10 records
