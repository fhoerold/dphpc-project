# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 00:48:24 2021
# Execution time and date (local): Sat Dec 25 01:48:24 2021
# MPI execution on rank 2 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 113747.500000            0 
            2          1200          1000   gramschmidt           MPI          1 140515.052734            1 
            2          1200          1000   gramschmidt           MPI          2 122856.099609            1 
            2          1200          1000   gramschmidt           MPI          3 125407.929688            1 
            2          1200          1000   gramschmidt           MPI          4 129848.736328            6 
            2          1200          1000   gramschmidt           MPI          5 110717.181641            0 
            2          1200          1000   gramschmidt           MPI          6 109061.478516            0 
            2          1200          1000   gramschmidt           MPI          7 109788.796875            0 
            2          1200          1000   gramschmidt           MPI          8 109697.628906            2 
            2          1200          1000   gramschmidt           MPI          9 109373.234375            0 
# Runtime: 1.594175 s (overhead: 0.000690 %) 10 records
