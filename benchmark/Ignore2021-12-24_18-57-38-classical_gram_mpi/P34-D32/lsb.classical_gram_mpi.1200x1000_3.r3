# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 03:18:48 2021
# Execution time and date (local): Sat Dec 25 04:18:48 2021
# MPI execution on rank 3 with 34 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 1277.541016            3 
            3          1200          1000   gramschmidt           MPI          1 1158.648438            3 
            3          1200          1000   gramschmidt           MPI          2 927.662109            0 
            3          1200          1000   gramschmidt           MPI          3 905.419922            0 
            3          1200          1000   gramschmidt           MPI          4 902.462891            0 
            3          1200          1000   gramschmidt           MPI          5 902.273438            0 
            3          1200          1000   gramschmidt           MPI          6 885.808594            0 
            3          1200          1000   gramschmidt           MPI          7 882.953125            0 
            3          1200          1000   gramschmidt           MPI          8 902.632812            0 
            3          1200          1000   gramschmidt           MPI          9 878.517578            0 
# Runtime: 4.016939 s (overhead: 0.000149 %) 10 records
