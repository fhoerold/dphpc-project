# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 03:18:48 2021
# Execution time and date (local): Sat Dec 25 04:18:48 2021
# MPI execution on rank 2 with 34 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 1288.318359            0 
            2          1200          1000   gramschmidt           MPI          1 948.496094            0 
            2          1200          1000   gramschmidt           MPI          2 943.865234            0 
            2          1200          1000   gramschmidt           MPI          3 915.095703            0 
            2          1200          1000   gramschmidt           MPI          4 908.011719            0 
            2          1200          1000   gramschmidt           MPI          5 907.906250            0 
            2          1200          1000   gramschmidt           MPI          6 901.380859            0 
            2          1200          1000   gramschmidt           MPI          7 895.021484            0 
            2          1200          1000   gramschmidt           MPI          8 913.394531            0 
            2          1200          1000   gramschmidt           MPI          9 891.037109            0 
# Runtime: 0.016553 s (overhead: 0.000000 %) 10 records
