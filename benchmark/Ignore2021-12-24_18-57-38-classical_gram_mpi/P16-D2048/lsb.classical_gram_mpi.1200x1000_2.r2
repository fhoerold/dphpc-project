# Sysname : Linux
# Nodename: eu-a6-002-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:38:07 2021
# Execution time and date (local): Fri Dec 24 19:38:07 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 10035778.494141            0 
            2          1200          1000   gramschmidt           MPI          1 10021456.386719            5 
            2          1200          1000   gramschmidt           MPI          2 9968425.376953            6 
            2          1200          1000   gramschmidt           MPI          3 10010045.892578            0 
            2          1200          1000   gramschmidt           MPI          4 9912379.427734            2 
            2          1200          1000   gramschmidt           MPI          5 9860583.609375            0 
            2          1200          1000   gramschmidt           MPI          6 9959871.750000            0 
            2          1200          1000   gramschmidt           MPI          7 9908718.402344            0 
            2          1200          1000   gramschmidt           MPI          8 9786180.980469            5 
            2          1200          1000   gramschmidt           MPI          9 9951648.207031            0 
# Runtime: 134.516394 s (overhead: 0.000013 %) 10 records
