# Sysname : Linux
# Nodename: eu-a6-002-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:38:07 2021
# Execution time and date (local): Fri Dec 24 19:38:07 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 10035673.160156            0 
            3          1200          1000   gramschmidt           MPI          1 10021111.761719            4 
            3          1200          1000   gramschmidt           MPI          2 9968372.394531            2 
            3          1200          1000   gramschmidt           MPI          3 10009683.869141            0 
            3          1200          1000   gramschmidt           MPI          4 9912067.302734            6 
            3          1200          1000   gramschmidt           MPI          5 9860492.876953            0 
            3          1200          1000   gramschmidt           MPI          6 9959617.789062            0 
            3          1200          1000   gramschmidt           MPI          7 9908384.837891            0 
            3          1200          1000   gramschmidt           MPI          8 9785905.970703            3 
            3          1200          1000   gramschmidt           MPI          9 9951335.107422            0 
# Runtime: 140.500516 s (overhead: 0.000011 %) 10 records
