# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 03:20:10 2021
# Execution time and date (local): Sat Dec 25 04:20:10 2021
# MPI execution on rank 3 with 34 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 663229.960938            2 
            3          1200          1000   gramschmidt           MPI          1 482822.939453            6 
            3          1200          1000   gramschmidt           MPI          2 550051.902344            1 
            3          1200          1000   gramschmidt           MPI          3 479207.728516            0 
            3          1200          1000   gramschmidt           MPI          4 475552.738281            2 
            3          1200          1000   gramschmidt           MPI          5 475217.064453            1 
            3          1200          1000   gramschmidt           MPI          6 472746.880859            0 
            3          1200          1000   gramschmidt           MPI          7 472797.708984            0 
            3          1200          1000   gramschmidt           MPI          8 474755.537109            1 
            3          1200          1000   gramschmidt           MPI          9 658845.646484            0 
# Runtime: 26.503512 s (overhead: 0.000049 %) 10 records
