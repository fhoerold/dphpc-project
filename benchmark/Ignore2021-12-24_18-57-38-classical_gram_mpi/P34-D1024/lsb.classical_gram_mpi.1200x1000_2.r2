# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 03:20:10 2021
# Execution time and date (local): Sat Dec 25 04:20:10 2021
# MPI execution on rank 2 with 34 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 664453.203125            0 
            2          1200          1000   gramschmidt           MPI          1 483704.533203            5 
            2          1200          1000   gramschmidt           MPI          2 551009.167969            7 
            2          1200          1000   gramschmidt           MPI          3 480086.632812            0 
            2          1200          1000   gramschmidt           MPI          4 476428.873047            1 
            2          1200          1000   gramschmidt           MPI          5 476100.751953            0 
            2          1200          1000   gramschmidt           MPI          6 473599.955078            0 
            2          1200          1000   gramschmidt           MPI          7 473653.201172            0 
            2          1200          1000   gramschmidt           MPI          8 475628.523438            2 
            2          1200          1000   gramschmidt           MPI          9 660045.412109            0 
# Runtime: 25.503598 s (overhead: 0.000059 %) 10 records
