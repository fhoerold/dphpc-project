# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 00:48:02 2021
# Execution time and date (local): Sat Dec 25 01:48:02 2021
# MPI execution on rank 3 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 28480.695312            0 
            3          1200          1000   gramschmidt           MPI          1 27648.732422            4 
            3          1200          1000   gramschmidt           MPI          2 27849.980469            1 
            3          1200          1000   gramschmidt           MPI          3 27855.626953            0 
            3          1200          1000   gramschmidt           MPI          4 27564.869141            1 
            3          1200          1000   gramschmidt           MPI          5 27824.226562            0 
            3          1200          1000   gramschmidt           MPI          6 27557.228516            0 
            3          1200          1000   gramschmidt           MPI          7 27426.947266            0 
            3          1200          1000   gramschmidt           MPI          8 27472.552734            1 
            3          1200          1000   gramschmidt           MPI          9 27690.669922            0 
# Runtime: 15.413873 s (overhead: 0.000045 %) 10 records
