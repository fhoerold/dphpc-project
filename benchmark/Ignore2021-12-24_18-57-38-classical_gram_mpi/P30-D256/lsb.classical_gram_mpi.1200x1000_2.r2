# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 00:48:02 2021
# Execution time and date (local): Sat Dec 25 01:48:02 2021
# MPI execution on rank 2 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 28248.146484            0 
            2          1200          1000   gramschmidt           MPI          1 27717.310547            4 
            2          1200          1000   gramschmidt           MPI          2 27864.912109            1 
            2          1200          1000   gramschmidt           MPI          3 27891.376953            0 
            2          1200          1000   gramschmidt           MPI          4 27612.966797            0 
            2          1200          1000   gramschmidt           MPI          5 27859.750000            0 
            2          1200          1000   gramschmidt           MPI          6 27585.615234            0 
            2          1200          1000   gramschmidt           MPI          7 27463.011719            0 
            2          1200          1000   gramschmidt           MPI          8 27501.560547            1 
            2          1200          1000   gramschmidt           MPI          9 27681.828125            0 
# Runtime: 3.419013 s (overhead: 0.000175 %) 10 records
