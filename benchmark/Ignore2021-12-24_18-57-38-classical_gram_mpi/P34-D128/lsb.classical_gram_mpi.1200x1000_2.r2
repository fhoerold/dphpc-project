# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 03:19:29 2021
# Execution time and date (local): Sat Dec 25 04:19:29 2021
# MPI execution on rank 2 with 34 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 7117.498047            0 
            2          1200          1000   gramschmidt           MPI          1 7121.208984            2 
            2          1200          1000   gramschmidt           MPI          2 7059.855469            0 
            2          1200          1000   gramschmidt           MPI          3 7001.917969            0 
            2          1200          1000   gramschmidt           MPI          4 7042.769531            0 
            2          1200          1000   gramschmidt           MPI          5 7027.839844            0 
            2          1200          1000   gramschmidt           MPI          6 7091.224609            0 
            2          1200          1000   gramschmidt           MPI          7 6954.130859            0 
            2          1200          1000   gramschmidt           MPI          8 6994.423828            0 
            2          1200          1000   gramschmidt           MPI          9 7021.142578            0 
# Runtime: 9.111669 s (overhead: 0.000022 %) 10 records
