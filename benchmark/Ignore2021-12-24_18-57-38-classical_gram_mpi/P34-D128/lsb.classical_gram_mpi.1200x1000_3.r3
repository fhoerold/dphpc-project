# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 03:19:29 2021
# Execution time and date (local): Sat Dec 25 04:19:29 2021
# MPI execution on rank 3 with 34 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 7117.619141            0 
            3          1200          1000   gramschmidt           MPI          1 7117.574219            3 
            3          1200          1000   gramschmidt           MPI          2 7057.556641            0 
            3          1200          1000   gramschmidt           MPI          3 7001.103516            0 
            3          1200          1000   gramschmidt           MPI          4 7039.908203            0 
            3          1200          1000   gramschmidt           MPI          5 7025.570312            0 
            3          1200          1000   gramschmidt           MPI          6 7087.562500            0 
            3          1200          1000   gramschmidt           MPI          7 6953.880859            0 
            3          1200          1000   gramschmidt           MPI          8 6994.757812            0 
            3          1200          1000   gramschmidt           MPI          9 7017.818359            0 
# Runtime: 10.107749 s (overhead: 0.000030 %) 10 records
