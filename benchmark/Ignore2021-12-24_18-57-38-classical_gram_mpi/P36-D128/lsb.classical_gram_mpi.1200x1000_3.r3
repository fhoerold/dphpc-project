# Sysname : Linux
# Nodename: eu-a6-012-05
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 06:05:31 2021
# Execution time and date (local): Sat Dec 25 07:05:31 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 6430.421875            0 
            3          1200          1000   gramschmidt           MPI          1 6310.617188            0 
            3          1200          1000   gramschmidt           MPI          2 6344.544922            0 
            3          1200          1000   gramschmidt           MPI          3 6283.974609            0 
            3          1200          1000   gramschmidt           MPI          4 6293.462891            0 
            3          1200          1000   gramschmidt           MPI          5 6264.285156            0 
            3          1200          1000   gramschmidt           MPI          6 6245.474609            0 
            3          1200          1000   gramschmidt           MPI          7 6196.771484            0 
            3          1200          1000   gramschmidt           MPI          8 6305.466797            0 
            3          1200          1000   gramschmidt           MPI          9 6240.263672            0 
# Runtime: 0.093235 s (overhead: 0.000000 %) 10 records
