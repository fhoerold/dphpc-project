# Sysname : Linux
# Nodename: eu-a6-012-05
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 06:05:31 2021
# Execution time and date (local): Sat Dec 25 07:05:31 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 6424.523438            0 
            2          1200          1000   gramschmidt           MPI          1 6305.390625            2 
            2          1200          1000   gramschmidt           MPI          2 6338.443359            0 
            2          1200          1000   gramschmidt           MPI          3 6278.267578            0 
            2          1200          1000   gramschmidt           MPI          4 6286.884766            0 
            2          1200          1000   gramschmidt           MPI          5 6257.556641            0 
            2          1200          1000   gramschmidt           MPI          6 6237.146484            0 
            2          1200          1000   gramschmidt           MPI          7 6189.974609            0 
            2          1200          1000   gramschmidt           MPI          8 6298.792969            0 
            2          1200          1000   gramschmidt           MPI          9 6231.763672            0 
# Runtime: 14.093030 s (overhead: 0.000014 %) 10 records
