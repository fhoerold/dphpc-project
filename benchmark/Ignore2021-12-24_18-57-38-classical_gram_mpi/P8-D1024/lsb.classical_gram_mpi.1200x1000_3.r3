# Sysname : Linux
# Nodename: eu-a6-002-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:58:14 2021
# Execution time and date (local): Fri Dec 24 18:58:14 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 2538442.208984            0 
            3          1200          1000   gramschmidt           MPI          1 2532767.921875            5 
            3          1200          1000   gramschmidt           MPI          2 2498040.785156            2 
            3          1200          1000   gramschmidt           MPI          3 2434092.501953            2 
            3          1200          1000   gramschmidt           MPI          4 2475638.652344            6 
            3          1200          1000   gramschmidt           MPI          5 2384903.835938            0 
            3          1200          1000   gramschmidt           MPI          6 2376646.601562            0 
            3          1200          1000   gramschmidt           MPI          7 2413573.958984            0 
            3          1200          1000   gramschmidt           MPI          8 2555969.632812            7 
            3          1200          1000   gramschmidt           MPI          9 2286833.402344            0 
# Runtime: 35.091171 s (overhead: 0.000063 %) 10 records
