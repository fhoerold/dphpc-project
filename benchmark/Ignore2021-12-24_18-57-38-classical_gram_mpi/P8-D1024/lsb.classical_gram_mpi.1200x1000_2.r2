# Sysname : Linux
# Nodename: eu-a6-002-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:58:14 2021
# Execution time and date (local): Fri Dec 24 18:58:14 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 2540259.421875            0 
            2          1200          1000   gramschmidt           MPI          1 2534348.750000            6 
            2          1200          1000   gramschmidt           MPI          2 2499604.972656            2 
            2          1200          1000   gramschmidt           MPI          3 2432931.716797            0 
            2          1200          1000   gramschmidt           MPI          4 2476993.572266            4 
            2          1200          1000   gramschmidt           MPI          5 2386197.042969            0 
            2          1200          1000   gramschmidt           MPI          6 2377848.056641            0 
            2          1200          1000   gramschmidt           MPI          7 2414963.974609            0 
            2          1200          1000   gramschmidt           MPI          8 2557971.765625            1 
            2          1200          1000   gramschmidt           MPI          9 2288313.642578            1 
# Runtime: 36.093744 s (overhead: 0.000039 %) 10 records
