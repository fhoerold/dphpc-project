# Sysname : Linux
# Nodename: eu-a6-012-05
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 05:50:50 2021
# Execution time and date (local): Sat Dec 25 06:50:50 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 2394.546875            0 
            3          1200          1000   gramschmidt           MPI          1 2348.972656            2 
            3          1200          1000   gramschmidt           MPI          2 2303.125000            0 
            3          1200          1000   gramschmidt           MPI          3 2276.033203            0 
            3          1200          1000   gramschmidt           MPI          4 2293.808594            0 
            3          1200          1000   gramschmidt           MPI          5 2260.943359            0 
            3          1200          1000   gramschmidt           MPI          6 2323.957031            0 
            3          1200          1000   gramschmidt           MPI          7 2237.310547            0 
            3          1200          1000   gramschmidt           MPI          8 2292.810547            0 
            3          1200          1000   gramschmidt           MPI          9 2244.890625            0 
# Runtime: 13.043578 s (overhead: 0.000015 %) 10 records
