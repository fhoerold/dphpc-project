# Sysname : Linux
# Nodename: eu-a6-012-05
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 05:50:50 2021
# Execution time and date (local): Sat Dec 25 06:50:50 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 2447.818359            0 
            2          1200          1000   gramschmidt           MPI          1 2410.595703            3 
            2          1200          1000   gramschmidt           MPI          2 2370.148438            0 
            2          1200          1000   gramschmidt           MPI          3 2314.837891            0 
            2          1200          1000   gramschmidt           MPI          4 2338.730469            0 
            2          1200          1000   gramschmidt           MPI          5 2301.830078            0 
            2          1200          1000   gramschmidt           MPI          6 2360.419922            0 
            2          1200          1000   gramschmidt           MPI          7 2269.804688            0 
            2          1200          1000   gramschmidt           MPI          8 2321.746094            0 
            2          1200          1000   gramschmidt           MPI          9 2271.201172            0 
# Runtime: 17.059644 s (overhead: 0.000018 %) 10 records
