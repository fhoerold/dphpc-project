# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:05:17 2021
# Execution time and date (local): Fri Dec 24 19:05:17 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 415.896484            0 
            3          1200          1000   gramschmidt           MPI          1 406.875000            2 
            3          1200          1000   gramschmidt           MPI          2 408.583984            0 
            3          1200          1000   gramschmidt           MPI          3 406.732422            0 
            3          1200          1000   gramschmidt           MPI          4 404.761719            0 
            3          1200          1000   gramschmidt           MPI          5 401.548828            0 
            3          1200          1000   gramschmidt           MPI          6 419.392578            0 
            3          1200          1000   gramschmidt           MPI          7 399.972656            0 
            3          1200          1000   gramschmidt           MPI          8 429.771484            0 
            3          1200          1000   gramschmidt           MPI          9 506.947266            0 
# Runtime: 14.009906 s (overhead: 0.000014 %) 10 records
