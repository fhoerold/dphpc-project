# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:05:17 2021
# Execution time and date (local): Fri Dec 24 19:05:17 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 421.042969            0 
            2          1200          1000   gramschmidt           MPI          1 411.093750            2 
            2          1200          1000   gramschmidt           MPI          2 411.421875            0 
            2          1200          1000   gramschmidt           MPI          3 413.656250            0 
            2          1200          1000   gramschmidt           MPI          4 406.529297            0 
            2          1200          1000   gramschmidt           MPI          5 405.076172            0 
            2          1200          1000   gramschmidt           MPI          6 422.244141            0 
            2          1200          1000   gramschmidt           MPI          7 406.031250            0 
            2          1200          1000   gramschmidt           MPI          8 430.894531            0 
            2          1200          1000   gramschmidt           MPI          9 513.419922            0 
# Runtime: 12.998333 s (overhead: 0.000015 %) 10 records
