# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 03:19:03 2021
# Execution time and date (local): Sat Dec 25 04:19:03 2021
# MPI execution on rank 2 with 34 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 2611.359375            0 
            2          1200          1000   gramschmidt           MPI          1 2515.144531            2 
            2          1200          1000   gramschmidt           MPI          2 2408.519531            0 
            2          1200          1000   gramschmidt           MPI          3 2360.453125            0 
            2          1200          1000   gramschmidt           MPI          4 2435.521484            0 
            2          1200          1000   gramschmidt           MPI          5 2332.714844            0 
            2          1200          1000   gramschmidt           MPI          6 2357.337891            0 
            2          1200          1000   gramschmidt           MPI          7 2385.201172            0 
            2          1200          1000   gramschmidt           MPI          8 2327.515625            0 
            2          1200          1000   gramschmidt           MPI          9 2293.148438            0 
# Runtime: 13.049005 s (overhead: 0.000015 %) 10 records
