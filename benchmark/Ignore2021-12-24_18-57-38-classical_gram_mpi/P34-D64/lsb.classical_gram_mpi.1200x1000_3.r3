# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 03:19:03 2021
# Execution time and date (local): Sat Dec 25 04:19:03 2021
# MPI execution on rank 3 with 34 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 2556.054688            0 
            3          1200          1000   gramschmidt           MPI          1 2457.123047            3 
            3          1200          1000   gramschmidt           MPI          2 2366.740234            0 
            3          1200          1000   gramschmidt           MPI          3 2320.402344            0 
            3          1200          1000   gramschmidt           MPI          4 2396.794922            0 
            3          1200          1000   gramschmidt           MPI          5 2305.205078            0 
            3          1200          1000   gramschmidt           MPI          6 2321.785156            0 
            3          1200          1000   gramschmidt           MPI          7 2351.439453            0 
            3          1200          1000   gramschmidt           MPI          8 2299.292969            0 
            3          1200          1000   gramschmidt           MPI          9 2262.279297            0 
# Runtime: 19.046394 s (overhead: 0.000016 %) 10 records
