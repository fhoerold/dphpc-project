# Sysname : Linux
# Nodename: eu-a6-007-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:06:45 2021
# Execution time and date (local): Fri Dec 24 19:06:45 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 42309917.228516            1 
            2          1200          1000   gramschmidt           MPI          1 42372301.546875            9 
            2          1200          1000   gramschmidt           MPI          2 42307572.365234            1 
            2          1200          1000   gramschmidt           MPI          3 42512526.291016            0 
            2          1200          1000   gramschmidt           MPI          4 42560864.867188            5 
            2          1200          1000   gramschmidt           MPI          5 42322693.982422            0 
            2          1200          1000   gramschmidt           MPI          6 41509028.261719            0 
            2          1200          1000   gramschmidt           MPI          7 41509338.488281            0 
            2          1200          1000   gramschmidt           MPI          8 41434987.648438            4 
            2          1200          1000   gramschmidt           MPI          9 41186654.210938            0 
# Runtime: 551.832961 s (overhead: 0.000004 %) 10 records
