# Sysname : Linux
# Nodename: eu-a6-007-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:06:45 2021
# Execution time and date (local): Fri Dec 24 19:06:45 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 42313621.494141            1 
            3          1200          1000   gramschmidt           MPI          1 42376051.773438            7 
            3          1200          1000   gramschmidt           MPI          2 42310164.193359            2 
            3          1200          1000   gramschmidt           MPI          3 42516153.607422            0 
            3          1200          1000   gramschmidt           MPI          4 42564447.699219            3 
            3          1200          1000   gramschmidt           MPI          5 42325255.859375            0 
            3          1200          1000   gramschmidt           MPI          6 41512631.292969            0 
            3          1200          1000   gramschmidt           MPI          7 41512878.679688            0 
            3          1200          1000   gramschmidt           MPI          8 41437766.455078            6 
            3          1200          1000   gramschmidt           MPI          9 41190166.066406            0 
# Runtime: 548.858361 s (overhead: 0.000003 %) 10 records
