# Sysname : Linux
# Nodename: eu-a6-012-05
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 06:05:53 2021
# Execution time and date (local): Sat Dec 25 07:05:53 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 37428.455078            0 
            3          1200          1000   gramschmidt           MPI          1 36482.201172            4 
            3          1200          1000   gramschmidt           MPI          2 35336.472656            1 
            3          1200          1000   gramschmidt           MPI          3 37105.837891            0 
            3          1200          1000   gramschmidt           MPI          4 35003.820312            1 
            3          1200          1000   gramschmidt           MPI          5 34526.496094            0 
            3          1200          1000   gramschmidt           MPI          6 34305.789062            0 
            3          1200          1000   gramschmidt           MPI          7 36470.568359            0 
            3          1200          1000   gramschmidt           MPI          8 34456.005859            1 
            3          1200          1000   gramschmidt           MPI          9 35441.666016            0 
# Runtime: 20.495936 s (overhead: 0.000034 %) 10 records
