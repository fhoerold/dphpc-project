# Sysname : Linux
# Nodename: eu-a6-012-05
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 06:05:53 2021
# Execution time and date (local): Sat Dec 25 07:05:53 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 37405.105469            0 
            2          1200          1000   gramschmidt           MPI          1 36472.466797            4 
            2          1200          1000   gramschmidt           MPI          2 35324.042969            1 
            2          1200          1000   gramschmidt           MPI          3 37087.396484            0 
            2          1200          1000   gramschmidt           MPI          4 34989.324219            1 
            2          1200          1000   gramschmidt           MPI          5 34511.076172            0 
            2          1200          1000   gramschmidt           MPI          6 34288.173828            0 
            2          1200          1000   gramschmidt           MPI          7 36455.904297            0 
            2          1200          1000   gramschmidt           MPI          8 34441.595703            1 
            2          1200          1000   gramschmidt           MPI          9 35423.779297            0 
# Runtime: 20.477324 s (overhead: 0.000034 %) 10 records
