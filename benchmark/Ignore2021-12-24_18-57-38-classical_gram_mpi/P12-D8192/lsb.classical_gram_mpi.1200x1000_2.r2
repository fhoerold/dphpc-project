# Sysname : Linux
# Nodename: eu-a6-012-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:03:15 2021
# Execution time and date (local): Fri Dec 24 19:03:15 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 1875598340.677734            0 
            2          1200          1000   gramschmidt           MPI          1 1892787124.906250            7 
            2          1200          1000   gramschmidt           MPI          2 1717095396.222656            6 
            2          1200          1000   gramschmidt           MPI          3 1880289546.234375            2 
            2          1200          1000   gramschmidt           MPI          4 1882229241.791016            7 
            2          1200          1000   gramschmidt           MPI          5 1874651531.240234            0 
            2          1200          1000   gramschmidt           MPI          6 1879717346.488281            2 
            2          1200          1000   gramschmidt           MPI          7 1863446787.894531            2 
            2          1200          1000   gramschmidt           MPI          8 1851334494.253906           10 
            2          1200          1000   gramschmidt           MPI          9 1888213724.310547            3 
# Runtime: 24203.355504 s (overhead: 0.000000 %) 10 records
