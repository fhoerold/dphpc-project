# Sysname : Linux
# Nodename: eu-a6-012-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:03:15 2021
# Execution time and date (local): Fri Dec 24 19:03:15 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 1875599329.302734            0 
            3          1200          1000   gramschmidt           MPI          1 1892789799.816406            8 
            3          1200          1000   gramschmidt           MPI          2 1717097511.914062            9 
            3          1200          1000   gramschmidt           MPI          3 1880290589.941406            3 
            3          1200          1000   gramschmidt           MPI          4 1882230347.958984           12 
            3          1200          1000   gramschmidt           MPI          5 1874652557.017578            2 
            3          1200          1000   gramschmidt           MPI          6 1879718170.955078            2 
            3          1200          1000   gramschmidt           MPI          7 1863448098.396484            2 
            3          1200          1000   gramschmidt           MPI          8 1851335424.470703           14 
            3          1200          1000   gramschmidt           MPI          9 1888214424.539062            4 
# Runtime: 24203.211640 s (overhead: 0.000000 %) 10 records
