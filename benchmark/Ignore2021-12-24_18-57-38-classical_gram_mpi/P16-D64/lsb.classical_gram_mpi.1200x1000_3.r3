# Sysname : Linux
# Nodename: eu-a6-004-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:36:08 2021
# Execution time and date (local): Fri Dec 24 19:36:08 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 1152.994141            0 
            3          1200          1000   gramschmidt           MPI          1 1070.181641            3 
            3          1200          1000   gramschmidt           MPI          2 1043.269531            0 
            3          1200          1000   gramschmidt           MPI          3 1057.482422            0 
            3          1200          1000   gramschmidt           MPI          4 1018.179688            0 
            3          1200          1000   gramschmidt           MPI          5 1011.000000            0 
            3          1200          1000   gramschmidt           MPI          6 973.675781            0 
            3          1200          1000   gramschmidt           MPI          7 982.955078            0 
            3          1200          1000   gramschmidt           MPI          8 995.488281            0 
            3          1200          1000   gramschmidt           MPI          9 1019.589844            0 
# Runtime: 4.016702 s (overhead: 0.000075 %) 10 records
