# Sysname : Linux
# Nodename: eu-a6-005-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:59:28 2021
# Execution time and date (local): Fri Dec 24 18:59:28 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 4170.210938            0 
            3          1200          1000   gramschmidt           MPI          1 3938.052734            3 
            3          1200          1000   gramschmidt           MPI          2 3936.550781            0 
            3          1200          1000   gramschmidt           MPI          3 3969.681641            0 
            3          1200          1000   gramschmidt           MPI          4 3947.638672            0 
            3          1200          1000   gramschmidt           MPI          5 3794.599609            0 
            3          1200          1000   gramschmidt           MPI          6 3757.503906            0 
            3          1200          1000   gramschmidt           MPI          7 3822.556641            0 
            3          1200          1000   gramschmidt           MPI          8 3780.513672            0 
            3          1200          1000   gramschmidt           MPI          9 3781.820312            0 
# Runtime: 6.042488 s (overhead: 0.000050 %) 10 records
