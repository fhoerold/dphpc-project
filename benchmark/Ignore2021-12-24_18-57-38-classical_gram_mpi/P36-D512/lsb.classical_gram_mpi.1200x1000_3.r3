# Sysname : Linux
# Nodename: eu-a6-012-05
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 06:06:22 2021
# Execution time and date (local): Sat Dec 25 07:06:22 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 98056.503906            0 
            3          1200          1000   gramschmidt           MPI          1 93821.207031            4 
            3          1200          1000   gramschmidt           MPI          2 93803.607422            1 
            3          1200          1000   gramschmidt           MPI          3 93708.169922            0 
            3          1200          1000   gramschmidt           MPI          4 92860.333984            2 
            3          1200          1000   gramschmidt           MPI          5 94684.306641            0 
            3          1200          1000   gramschmidt           MPI          6 97859.835938            0 
            3          1200          1000   gramschmidt           MPI          7 92561.710938            0 
            3          1200          1000   gramschmidt           MPI          8 90945.984375            2 
            3          1200          1000   gramschmidt           MPI          9 92366.714844            0 
# Runtime: 7.768224 s (overhead: 0.000116 %) 10 records
