# Sysname : Linux
# Nodename: eu-a6-012-05
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 06:06:22 2021
# Execution time and date (local): Sat Dec 25 07:06:22 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 97968.669922            0 
            2          1200          1000   gramschmidt           MPI          1 93648.919922            5 
            2          1200          1000   gramschmidt           MPI          2 93623.746094            1 
            2          1200          1000   gramschmidt           MPI          3 93523.671875            0 
            2          1200          1000   gramschmidt           MPI          4 92691.478516            1 
            2          1200          1000   gramschmidt           MPI          5 94658.660156            0 
            2          1200          1000   gramschmidt           MPI          6 97693.113281            0 
            2          1200          1000   gramschmidt           MPI          7 92395.269531            0 
            2          1200          1000   gramschmidt           MPI          8 90792.935547            1 
            2          1200          1000   gramschmidt           MPI          9 92220.574219            0 
# Runtime: 17.728792 s (overhead: 0.000045 %) 10 records
