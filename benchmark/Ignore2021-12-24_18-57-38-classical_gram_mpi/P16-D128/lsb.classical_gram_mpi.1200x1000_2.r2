# Sysname : Linux
# Nodename: eu-a6-004-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:36:24 2021
# Execution time and date (local): Fri Dec 24 19:36:24 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 3204.183594            0 
            2          1200          1000   gramschmidt           MPI          1 3133.964844            2 
            2          1200          1000   gramschmidt           MPI          2 3207.687500            0 
            2          1200          1000   gramschmidt           MPI          3 3014.589844            0 
            2          1200          1000   gramschmidt           MPI          4 2979.195312            0 
            2          1200          1000   gramschmidt           MPI          5 2961.564453            0 
            2          1200          1000   gramschmidt           MPI          6 2973.337891            0 
            2          1200          1000   gramschmidt           MPI          7 2934.701172            0 
            2          1200          1000   gramschmidt           MPI          8 2960.292969            0 
            2          1200          1000   gramschmidt           MPI          9 2966.539062            0 
# Runtime: 8.035674 s (overhead: 0.000025 %) 10 records
