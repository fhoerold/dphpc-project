# Sysname : Linux
# Nodename: eu-a6-004-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 18:36:24 2021
# Execution time and date (local): Fri Dec 24 19:36:24 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 3206.091797            0 
            3          1200          1000   gramschmidt           MPI          1 3137.482422            0 
            3          1200          1000   gramschmidt           MPI          2 3209.986328            0 
            3          1200          1000   gramschmidt           MPI          3 3017.460938            0 
            3          1200          1000   gramschmidt           MPI          4 2981.603516            0 
            3          1200          1000   gramschmidt           MPI          5 2961.742188            0 
            3          1200          1000   gramschmidt           MPI          6 2974.626953            0 
            3          1200          1000   gramschmidt           MPI          7 2937.976562            0 
            3          1200          1000   gramschmidt           MPI          8 2961.017578            0 
            3          1200          1000   gramschmidt           MPI          9 2967.224609            0 
# Runtime: 0.043481 s (overhead: 0.000000 %) 10 records
