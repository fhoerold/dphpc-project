# Sysname : Linux
# Nodename: eu-a6-001-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:58:15 2021
# Execution time and date (local): Fri Dec 24 18:58:15 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 3668.433594            0 
            3          1200          1000   gramschmidt           MPI          1 3634.654297            3 
            3          1200          1000   gramschmidt           MPI          2 3646.130859            0 
            3          1200          1000   gramschmidt           MPI          3 3582.740234            0 
            3          1200          1000   gramschmidt           MPI          4 3596.583984            0 
            3          1200          1000   gramschmidt           MPI          5 3608.974609            0 
            3          1200          1000   gramschmidt           MPI          6 3594.583984            0 
            3          1200          1000   gramschmidt           MPI          7 3612.955078            0 
            3          1200          1000   gramschmidt           MPI          8 3603.410156            0 
            3          1200          1000   gramschmidt           MPI          9 3556.824219            0 
# Runtime: 25.054525 s (overhead: 0.000012 %) 10 records
