# Sysname : Linux
# Nodename: eu-a6-001-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:58:15 2021
# Execution time and date (local): Fri Dec 24 18:58:15 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 3687.773438            0 
            2          1200          1000   gramschmidt           MPI          1 3654.423828            3 
            2          1200          1000   gramschmidt           MPI          2 3665.015625            0 
            2          1200          1000   gramschmidt           MPI          3 3604.748047            0 
            2          1200          1000   gramschmidt           MPI          4 3615.535156            0 
            2          1200          1000   gramschmidt           MPI          5 3635.279297            0 
            2          1200          1000   gramschmidt           MPI          6 3613.978516            0 
            2          1200          1000   gramschmidt           MPI          7 3636.904297            0 
            2          1200          1000   gramschmidt           MPI          8 3619.181641            0 
            2          1200          1000   gramschmidt           MPI          9 3575.175781            0 
# Runtime: 25.055184 s (overhead: 0.000012 %) 10 records
