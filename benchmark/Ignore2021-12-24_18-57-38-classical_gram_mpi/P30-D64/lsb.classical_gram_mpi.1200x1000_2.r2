# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 00:50:23 2021
# Execution time and date (local): Sat Dec 25 01:50:23 2021
# MPI execution on rank 2 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1200          1000   gramschmidt           MPI          0 2455.818359            0 
            2          1200          1000   gramschmidt           MPI          1 2184.341797            3 
            2          1200          1000   gramschmidt           MPI          2 2031.343750            0 
            2          1200          1000   gramschmidt           MPI          3 2077.386719            0 
            2          1200          1000   gramschmidt           MPI          4 2033.214844            0 
            2          1200          1000   gramschmidt           MPI          5 2279.285156            0 
            2          1200          1000   gramschmidt           MPI          6 2027.160156            0 
            2          1200          1000   gramschmidt           MPI          7 1966.800781            0 
            2          1200          1000   gramschmidt           MPI          8 1939.859375            0 
            2          1200          1000   gramschmidt           MPI          9 1958.109375            0 
# Runtime: 3.055284 s (overhead: 0.000098 %) 10 records
