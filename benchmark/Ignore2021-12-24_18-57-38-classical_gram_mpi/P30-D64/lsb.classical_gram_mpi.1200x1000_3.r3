# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Dec 25 00:50:23 2021
# Execution time and date (local): Sat Dec 25 01:50:23 2021
# MPI execution on rank 3 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1200          1000   gramschmidt           MPI          0 2449.156250            0 
            3          1200          1000   gramschmidt           MPI          1 2108.023438            3 
            3          1200          1000   gramschmidt           MPI          2 1957.316406            0 
            3          1200          1000   gramschmidt           MPI          3 1995.523438            0 
            3          1200          1000   gramschmidt           MPI          4 1986.074219            0 
            3          1200          1000   gramschmidt           MPI          5 2189.449219            0 
            3          1200          1000   gramschmidt           MPI          6 1992.568359            0 
            3          1200          1000   gramschmidt           MPI          7 1940.568359            0 
            3          1200          1000   gramschmidt           MPI          8 1918.658203            0 
            3          1200          1000   gramschmidt           MPI          9 1933.816406            0 
# Runtime: 8.037215 s (overhead: 0.000037 %) 10 records
