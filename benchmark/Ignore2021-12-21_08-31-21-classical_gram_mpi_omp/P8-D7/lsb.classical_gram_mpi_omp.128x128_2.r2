# Sysname : Linux
# Nodename: eu-a6-005-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:32:05 2021
# Execution time and date (local): Tue Dec 21 08:32:05 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 6069.611328            0 
            2           128           128   gramschmidt           MPI          1 6024.652344            3 
            2           128           128   gramschmidt           MPI          2 5979.734375            0 
            2           128           128   gramschmidt           MPI          3 6024.568359            0 
            2           128           128   gramschmidt           MPI          4 5997.683594            0 
            2           128           128   gramschmidt           MPI          5 5985.464844            0 
            2           128           128   gramschmidt           MPI          6 5978.085938            0 
            2           128           128   gramschmidt           MPI          7 5976.171875            0 
            2           128           128   gramschmidt           MPI          8 5980.937500            0 
            2           128           128   gramschmidt           MPI          9 5986.820312            0 
# Runtime: 3.082636 s (overhead: 0.000097 %) 10 records
