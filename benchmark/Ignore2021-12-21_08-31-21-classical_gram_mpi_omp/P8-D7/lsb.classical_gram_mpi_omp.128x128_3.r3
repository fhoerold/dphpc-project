# Sysname : Linux
# Nodename: eu-a6-005-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:32:05 2021
# Execution time and date (local): Tue Dec 21 08:32:05 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 6036.535156            0 
            3           128           128   gramschmidt           MPI          1 5997.304688            0 
            3           128           128   gramschmidt           MPI          2 5954.533203            0 
            3           128           128   gramschmidt           MPI          3 5992.880859            0 
            3           128           128   gramschmidt           MPI          4 5971.107422            0 
            3           128           128   gramschmidt           MPI          5 5966.537109            0 
            3           128           128   gramschmidt           MPI          6 5951.943359            0 
            3           128           128   gramschmidt           MPI          7 5947.146484            0 
            3           128           128   gramschmidt           MPI          8 5948.029297            0 
            3           128           128   gramschmidt           MPI          9 5952.580078            0 
# Runtime: 0.081845 s (overhead: 0.000000 %) 10 records
