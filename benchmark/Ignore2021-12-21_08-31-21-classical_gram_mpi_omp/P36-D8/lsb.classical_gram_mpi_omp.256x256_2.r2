# Sysname : Linux
# Nodename: eu-a6-002-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 23:38:32 2021
# Execution time and date (local): Thu Dec 23 00:38:32 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 174521.566406            0 
            2           256           256   gramschmidt           MPI          1 174258.785156            1 
            2           256           256   gramschmidt           MPI          2 173886.236328            1 
            2           256           256   gramschmidt           MPI          3 174816.357422            0 
            2           256           256   gramschmidt           MPI          4 174683.753906            1 
            2           256           256   gramschmidt           MPI          5 174922.511719            0 
            2           256           256   gramschmidt           MPI          6 174144.707031            0 
            2           256           256   gramschmidt           MPI          7 174626.328125            0 
            2           256           256   gramschmidt           MPI          8 175198.927734            1 
            2           256           256   gramschmidt           MPI          9 175069.027344            0 
# Runtime: 2.278304 s (overhead: 0.000176 %) 10 records
