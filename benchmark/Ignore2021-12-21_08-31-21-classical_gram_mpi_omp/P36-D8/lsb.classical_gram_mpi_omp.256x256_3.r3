# Sysname : Linux
# Nodename: eu-a6-002-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 23:38:32 2021
# Execution time and date (local): Thu Dec 23 00:38:32 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 174445.513672            0 
            3           256           256   gramschmidt           MPI          1 174188.343750            5 
            3           256           256   gramschmidt           MPI          2 173827.203125            1 
            3           256           256   gramschmidt           MPI          3 174740.646484            0 
            3           256           256   gramschmidt           MPI          4 174563.781250            1 
            3           256           256   gramschmidt           MPI          5 174804.728516            0 
            3           256           256   gramschmidt           MPI          6 174043.724609            0 
            3           256           256   gramschmidt           MPI          7 174530.275391            0 
            3           256           256   gramschmidt           MPI          8 175095.775391            2 
            3           256           256   gramschmidt           MPI          9 174953.193359            0 
# Runtime: 8.275034 s (overhead: 0.000109 %) 10 records
