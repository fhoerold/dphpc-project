# Sysname : Linux
# Nodename: eu-a6-004-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 22:51:10 2021
# Execution time and date (local): Wed Dec 22 23:51:10 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 35070.779297            0 
            3            64            64   gramschmidt           MPI          1 35538.998047            3 
            3            64            64   gramschmidt           MPI          2 35179.458984            0 
            3            64            64   gramschmidt           MPI          3 35452.417969            0 
            3            64            64   gramschmidt           MPI          4 35810.533203            0 
            3            64            64   gramschmidt           MPI          5 35195.691406            0 
            3            64            64   gramschmidt           MPI          6 35174.669922            0 
            3            64            64   gramschmidt           MPI          7 35286.376953            0 
            3            64            64   gramschmidt           MPI          8 35328.572266            0 
            3            64            64   gramschmidt           MPI          9 35014.699219            0 
# Runtime: 7.461282 s (overhead: 0.000040 %) 10 records
