# Sysname : Linux
# Nodename: eu-a6-004-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 22:51:10 2021
# Execution time and date (local): Wed Dec 22 23:51:10 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 35063.515625            0 
            2            64            64   gramschmidt           MPI          1 35545.824219            2 
            2            64            64   gramschmidt           MPI          2 35172.978516            0 
            2            64            64   gramschmidt           MPI          3 35453.365234            0 
            2            64            64   gramschmidt           MPI          4 35803.605469            0 
            2            64            64   gramschmidt           MPI          5 35189.685547            0 
            2            64            64   gramschmidt           MPI          6 35169.443359            0 
            2            64            64   gramschmidt           MPI          7 35275.976562            0 
            2            64            64   gramschmidt           MPI          8 35312.669922            0 
            2            64            64   gramschmidt           MPI          9 35014.150391            0 
# Runtime: 10.465796 s (overhead: 0.000019 %) 10 records
