# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:33:57 2021
# Execution time and date (local): Tue Dec 21 08:33:57 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 73663.392578            0 
            2           256           256   gramschmidt           MPI          1 73422.314453            4 
            2           256           256   gramschmidt           MPI          2 73571.646484            1 
            2           256           256   gramschmidt           MPI          3 73725.408203            0 
            2           256           256   gramschmidt           MPI          4 73292.082031            1 
            2           256           256   gramschmidt           MPI          5 73412.119141            0 
            2           256           256   gramschmidt           MPI          6 73726.353516            0 
            2           256           256   gramschmidt           MPI          7 73834.343750            0 
            2           256           256   gramschmidt           MPI          8 73754.119141            1 
            2           256           256   gramschmidt           MPI          9 73446.484375            0 
# Runtime: 3.964117 s (overhead: 0.000177 %) 10 records
