# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:33:57 2021
# Execution time and date (local): Tue Dec 21 08:33:57 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 73586.132812            2 
            3           256           256   gramschmidt           MPI          1 73350.392578            3 
            3           256           256   gramschmidt           MPI          2 73498.863281            0 
            3           256           256   gramschmidt           MPI          3 73654.125000            0 
            3           256           256   gramschmidt           MPI          4 73186.226562            0 
            3           256           256   gramschmidt           MPI          5 73315.654297            0 
            3           256           256   gramschmidt           MPI          6 73620.605469            0 
            3           256           256   gramschmidt           MPI          7 73732.177734            0 
            3           256           256   gramschmidt           MPI          8 73643.839844            1 
            3           256           256   gramschmidt           MPI          9 73347.400391            0 
# Runtime: 4.963099 s (overhead: 0.000121 %) 10 records
