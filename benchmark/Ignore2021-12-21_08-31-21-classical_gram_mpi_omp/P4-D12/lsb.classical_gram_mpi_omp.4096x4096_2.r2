# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:31:36 2021
# Execution time and date (local): Tue Dec 21 08:31:36 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 227414503.005859            3 
            2          4096          4096   gramschmidt           MPI          1 227269729.761719           10 
            2          4096          4096   gramschmidt           MPI          2 227769838.609375            6 
            2          4096          4096   gramschmidt           MPI          3 227005306.728516            2 
            2          4096          4096   gramschmidt           MPI          4 226580113.777344            9 
            2          4096          4096   gramschmidt           MPI          5 227478002.964844            3 
            2          4096          4096   gramschmidt           MPI          6 227899796.015625            3 
            2          4096          4096   gramschmidt           MPI          7 226738107.945312            4 
            2          4096          4096   gramschmidt           MPI          8 226486599.320312           10 
            2          4096          4096   gramschmidt           MPI          9 227783869.718750            6 
# Runtime: 2975.072724 s (overhead: 0.000002 %) 10 records
