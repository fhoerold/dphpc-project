# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:31:36 2021
# Execution time and date (local): Tue Dec 21 08:31:36 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 227442096.746094            1 
            3          4096          4096   gramschmidt           MPI          1 227297399.992188            6 
            3          4096          4096   gramschmidt           MPI          2 227797397.769531            4 
            3          4096          4096   gramschmidt           MPI          3 227032801.880859            0 
            3          4096          4096   gramschmidt           MPI          4 226607499.951172            6 
            3          4096          4096   gramschmidt           MPI          5 227505888.113281            0 
            3          4096          4096   gramschmidt           MPI          6 227927510.925781            0 
            3          4096          4096   gramschmidt           MPI          7 226765607.677734            0 
            3          4096          4096   gramschmidt           MPI          8 226514081.347656            8 
            3          4096          4096   gramschmidt           MPI          9 227811909.060547            2 
# Runtime: 2977.214545 s (overhead: 0.000001 %) 10 records
