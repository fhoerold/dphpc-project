# Sysname : Linux
# Nodename: eu-a6-003-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:31:36 2021
# Execution time and date (local): Tue Dec 21 08:31:36 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 258537.654297            0 
            3           512           512   gramschmidt           MPI          1 257817.439453            4 
            3           512           512   gramschmidt           MPI          2 255918.708984            1 
            3           512           512   gramschmidt           MPI          3 255831.943359            0 
            3           512           512   gramschmidt           MPI          4 255943.201172            1 
            3           512           512   gramschmidt           MPI          5 256756.673828            0 
            3           512           512   gramschmidt           MPI          6 256878.630859            0 
            3           512           512   gramschmidt           MPI          7 256424.724609            0 
            3           512           512   gramschmidt           MPI          8 258050.791016            1 
            3           512           512   gramschmidt           MPI          9 257378.726562            0 
# Runtime: 4.339843 s (overhead: 0.000161 %) 10 records
