# Sysname : Linux
# Nodename: eu-a6-003-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:31:36 2021
# Execution time and date (local): Tue Dec 21 08:31:36 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 258566.937500            0 
            2           512           512   gramschmidt           MPI          1 257839.041016            5 
            2           512           512   gramschmidt           MPI          2 255945.240234            1 
            2           512           512   gramschmidt           MPI          3 255861.697266            0 
            2           512           512   gramschmidt           MPI          4 255965.490234            1 
            2           512           512   gramschmidt           MPI          5 256776.951172            0 
            2           512           512   gramschmidt           MPI          6 256901.628906            0 
            2           512           512   gramschmidt           MPI          7 256452.091797            0 
            2           512           512   gramschmidt           MPI          8 258074.908203            3 
            2           512           512   gramschmidt           MPI          9 257405.884766            0 
# Runtime: 3.352565 s (overhead: 0.000298 %) 10 records
