# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:33:35 2021
# Execution time and date (local): Tue Dec 21 08:33:35 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 11218.136719            0 
            3            64            64   gramschmidt           MPI          1 11481.767578            0 
            3            64            64   gramschmidt           MPI          2 11212.509766            0 
            3            64            64   gramschmidt           MPI          3 11206.728516            0 
            3            64            64   gramschmidt           MPI          4 11198.000000            0 
            3            64            64   gramschmidt           MPI          5 11201.138672            0 
            3            64            64   gramschmidt           MPI          6 11183.261719            0 
            3            64            64   gramschmidt           MPI          7 11210.746094            0 
            3            64            64   gramschmidt           MPI          8 11322.136719            0 
            3            64            64   gramschmidt           MPI          9 11557.476562            0 
# Runtime: 0.150066 s (overhead: 0.000000 %) 10 records
