# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:33:35 2021
# Execution time and date (local): Tue Dec 21 08:33:35 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 11220.558594            0 
            2            64            64   gramschmidt           MPI          1 11479.197266            0 
            2            64            64   gramschmidt           MPI          2 11221.605469            0 
            2            64            64   gramschmidt           MPI          3 11197.894531            0 
            2            64            64   gramschmidt           MPI          4 11208.125000            0 
            2            64            64   gramschmidt           MPI          5 11189.648438            0 
            2            64            64   gramschmidt           MPI          6 11187.050781            0 
            2            64            64   gramschmidt           MPI          7 11199.316406            0 
            2            64            64   gramschmidt           MPI          8 11329.144531            0 
            2            64            64   gramschmidt           MPI          9 11547.183594            0 
# Runtime: 0.150709 s (overhead: 0.000000 %) 10 records
