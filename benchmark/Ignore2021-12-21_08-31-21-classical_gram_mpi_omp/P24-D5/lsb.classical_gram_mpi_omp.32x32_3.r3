# Sysname : Linux
# Nodename: eu-a6-009-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 19:31:55 2021
# Execution time and date (local): Tue Dec 21 20:31:55 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 9072.589844            0 
            3            32            32   gramschmidt           MPI          1 8917.703125            2 
            3            32            32   gramschmidt           MPI          2 9014.650391            0 
            3            32            32   gramschmidt           MPI          3 8944.908203            0 
            3            32            32   gramschmidt           MPI          4 9035.574219            0 
            3            32            32   gramschmidt           MPI          5 8915.337891            0 
            3            32            32   gramschmidt           MPI          6 8886.191406            0 
            3            32            32   gramschmidt           MPI          7 8917.308594            0 
            3            32            32   gramschmidt           MPI          8 9036.794922            0 
            3            32            32   gramschmidt           MPI          9 9047.876953            0 
# Runtime: 15.121612 s (overhead: 0.000013 %) 10 records
