# Sysname : Linux
# Nodename: eu-a6-009-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 19:31:55 2021
# Execution time and date (local): Tue Dec 21 20:31:55 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 9078.140625            0 
            2            32            32   gramschmidt           MPI          1 8921.058594            0 
            2            32            32   gramschmidt           MPI          2 9018.669922            0 
            2            32            32   gramschmidt           MPI          3 8952.337891            0 
            2            32            32   gramschmidt           MPI          4 9043.613281            0 
            2            32            32   gramschmidt           MPI          5 8921.101562            0 
            2            32            32   gramschmidt           MPI          6 8887.808594            0 
            2            32            32   gramschmidt           MPI          7 8920.462891            0 
            2            32            32   gramschmidt           MPI          8 9041.980469            0 
            2            32            32   gramschmidt           MPI          9 9056.873047            0 
# Runtime: 0.119363 s (overhead: 0.000000 %) 10 records
