# Sysname : Linux
# Nodename: eu-a6-003-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:31:36 2021
# Execution time and date (local): Tue Dec 21 08:31:36 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 6264.074219            0 
            2           128           128   gramschmidt           MPI          1 6265.777344            0 
            2           128           128   gramschmidt           MPI          2 6227.996094            0 
            2           128           128   gramschmidt           MPI          3 6267.699219            0 
            2           128           128   gramschmidt           MPI          4 6238.933594            0 
            2           128           128   gramschmidt           MPI          5 6217.462891            0 
            2           128           128   gramschmidt           MPI          6 6220.498047            0 
            2           128           128   gramschmidt           MPI          7 6218.076172            0 
            2           128           128   gramschmidt           MPI          8 6219.167969            0 
            2           128           128   gramschmidt           MPI          9 6214.339844            0 
# Runtime: 0.083710 s (overhead: 0.000000 %) 10 records
