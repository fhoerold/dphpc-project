# Sysname : Linux
# Nodename: eu-a6-003-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:31:36 2021
# Execution time and date (local): Tue Dec 21 08:31:36 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 6237.656250            0 
            3           128           128   gramschmidt           MPI          1 6232.197266            3 
            3           128           128   gramschmidt           MPI          2 6201.328125            0 
            3           128           128   gramschmidt           MPI          3 6247.521484            0 
            3           128           128   gramschmidt           MPI          4 6214.457031            0 
            3           128           128   gramschmidt           MPI          5 6189.080078            0 
            3           128           128   gramschmidt           MPI          6 6199.656250            0 
            3           128           128   gramschmidt           MPI          7 6189.472656            0 
            3           128           128   gramschmidt           MPI          8 6192.712891            0 
            3           128           128   gramschmidt           MPI          9 6187.679688            0 
# Runtime: 4.072924 s (overhead: 0.000074 %) 10 records
