# Sysname : Linux
# Nodename: eu-a6-007-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 05:31:22 2021
# Execution time and date (local): Wed Dec 22 06:31:22 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 141657.730469            0 
            2           256           256   gramschmidt           MPI          1 140980.478516            5 
            2           256           256   gramschmidt           MPI          2 140799.750000            1 
            2           256           256   gramschmidt           MPI          3 140791.933594            0 
            2           256           256   gramschmidt           MPI          4 140790.744141            1 
            2           256           256   gramschmidt           MPI          5 141192.406250            0 
            2           256           256   gramschmidt           MPI          6 141144.765625            0 
            2           256           256   gramschmidt           MPI          7 141176.564453            0 
            2           256           256   gramschmidt           MPI          8 141051.468750            1 
            2           256           256   gramschmidt           MPI          9 141146.949219            0 
# Runtime: 2.849597 s (overhead: 0.000281 %) 10 records
