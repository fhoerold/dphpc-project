# Sysname : Linux
# Nodename: eu-a6-007-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 05:31:22 2021
# Execution time and date (local): Wed Dec 22 06:31:22 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 141570.169922            4 
            3           256           256   gramschmidt           MPI          1 140892.933594            5 
            3           256           256   gramschmidt           MPI          2 140719.427734            2 
            3           256           256   gramschmidt           MPI          3 140713.908203            0 
            3           256           256   gramschmidt           MPI          4 140676.105469            2 
            3           256           256   gramschmidt           MPI          5 141059.160156            0 
            3           256           256   gramschmidt           MPI          6 141008.599609            0 
            3           256           256   gramschmidt           MPI          7 141055.878906            0 
            3           256           256   gramschmidt           MPI          8 140935.613281            2 
            3           256           256   gramschmidt           MPI          9 141016.972656            0 
# Runtime: 2.848502 s (overhead: 0.000527 %) 10 records
