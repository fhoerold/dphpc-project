# Sysname : Linux
# Nodename: eu-a6-004-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 22:50:55 2021
# Execution time and date (local): Wed Dec 22 23:50:55 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 17303.703125            0 
            2            32            32   gramschmidt           MPI          1 17498.871094            2 
            2            32            32   gramschmidt           MPI          2 17016.166016            0 
            2            32            32   gramschmidt           MPI          3 17014.117188            0 
            2            32            32   gramschmidt           MPI          4 17043.431641            0 
            2            32            32   gramschmidt           MPI          5 17011.373047            0 
            2            32            32   gramschmidt           MPI          6 17009.664062            0 
            2            32            32   gramschmidt           MPI          7 17573.716797            0 
            2            32            32   gramschmidt           MPI          8 17029.132812            0 
            2            32            32   gramschmidt           MPI          9 17037.326172            0 
# Runtime: 8.224931 s (overhead: 0.000024 %) 10 records
