# Sysname : Linux
# Nodename: eu-a6-004-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 22:50:55 2021
# Execution time and date (local): Wed Dec 22 23:50:55 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 17298.128906            0 
            3            32            32   gramschmidt           MPI          1 17493.162109            3 
            3            32            32   gramschmidt           MPI          2 17010.255859            0 
            3            32            32   gramschmidt           MPI          3 17005.845703            0 
            3            32            32   gramschmidt           MPI          4 17035.873047            0 
            3            32            32   gramschmidt           MPI          5 17008.623047            0 
            3            32            32   gramschmidt           MPI          6 17005.699219            0 
            3            32            32   gramschmidt           MPI          7 17569.570312            0 
            3            32            32   gramschmidt           MPI          8 17023.326172            0 
            3            32            32   gramschmidt           MPI          9 17030.597656            0 
# Runtime: 4.211629 s (overhead: 0.000071 %) 10 records
