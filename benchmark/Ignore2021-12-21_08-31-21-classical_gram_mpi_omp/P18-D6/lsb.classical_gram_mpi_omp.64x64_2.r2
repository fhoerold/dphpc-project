# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 17:35:13 2021
# Execution time and date (local): Tue Dec 21 18:35:13 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 10449.312500            0 
            2            64            64   gramschmidt           MPI          1 10470.164062            3 
            2            64            64   gramschmidt           MPI          2 10803.656250            0 
            2            64            64   gramschmidt           MPI          3 10448.964844            0 
            2            64            64   gramschmidt           MPI          4 10438.308594            0 
            2            64            64   gramschmidt           MPI          5 10449.962891            0 
            2            64            64   gramschmidt           MPI          6 10594.593750            0 
            2            64            64   gramschmidt           MPI          7 10524.138672            0 
            2            64            64   gramschmidt           MPI          8 10426.140625            0 
            2            64            64   gramschmidt           MPI          9 10445.392578            0 
# Runtime: 2.140399 s (overhead: 0.000140 %) 10 records
