# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 17:35:13 2021
# Execution time and date (local): Tue Dec 21 18:35:13 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 10455.023438            0 
            3            64            64   gramschmidt           MPI          1 10455.695312            4 
            3            64            64   gramschmidt           MPI          2 10814.470703            0 
            3            64            64   gramschmidt           MPI          3 10452.605469            0 
            3            64            64   gramschmidt           MPI          4 10450.703125            0 
            3            64            64   gramschmidt           MPI          5 10445.363281            0 
            3            64            64   gramschmidt           MPI          6 10591.535156            0 
            3            64            64   gramschmidt           MPI          7 10517.134766            0 
            3            64            64   gramschmidt           MPI          8 10433.763672            0 
            3            64            64   gramschmidt           MPI          9 10443.017578            0 
# Runtime: 2.140074 s (overhead: 0.000187 %) 10 records
