# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:33:35 2021
# Execution time and date (local): Tue Dec 21 08:33:35 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 5220.937500            0 
            2            32            32   gramschmidt           MPI          1 5157.464844            2 
            2            32            32   gramschmidt           MPI          2 5315.123047            0 
            2            32            32   gramschmidt           MPI          3 5133.423828            0 
            2            32            32   gramschmidt           MPI          4 5180.621094            0 
            2            32            32   gramschmidt           MPI          5 5143.496094            0 
            2            32            32   gramschmidt           MPI          6 5276.396484            0 
            2            32            32   gramschmidt           MPI          7 5336.019531            0 
            2            32            32   gramschmidt           MPI          8 5188.986328            0 
            2            32            32   gramschmidt           MPI          9 5154.873047            0 
# Runtime: 5.070624 s (overhead: 0.000039 %) 10 records
