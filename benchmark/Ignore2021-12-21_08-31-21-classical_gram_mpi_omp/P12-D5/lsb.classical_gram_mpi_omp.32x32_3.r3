# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:33:35 2021
# Execution time and date (local): Tue Dec 21 08:33:35 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 5217.183594            0 
            3            32            32   gramschmidt           MPI          1 5149.056641            2 
            3            32            32   gramschmidt           MPI          2 5311.542969            0 
            3            32            32   gramschmidt           MPI          3 5124.544922            0 
            3            32            32   gramschmidt           MPI          4 5175.404297            0 
            3            32            32   gramschmidt           MPI          5 5134.388672            0 
            3            32            32   gramschmidt           MPI          6 5274.947266            0 
            3            32            32   gramschmidt           MPI          7 5328.828125            0 
            3            32            32   gramschmidt           MPI          8 5185.226562            0 
            3            32            32   gramschmidt           MPI          9 5145.816406            0 
# Runtime: 5.069371 s (overhead: 0.000039 %) 10 records
