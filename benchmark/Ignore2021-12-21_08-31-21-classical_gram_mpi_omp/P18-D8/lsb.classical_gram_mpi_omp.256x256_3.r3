# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 17:28:18 2021
# Execution time and date (local): Tue Dec 21 18:28:18 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 69718.423828            0 
            3           256           256   gramschmidt           MPI          1 69829.369141            1 
            3           256           256   gramschmidt           MPI          2 69978.949219            1 
            3           256           256   gramschmidt           MPI          3 70043.958984            0 
            3           256           256   gramschmidt           MPI          4 69517.757812            1 
            3           256           256   gramschmidt           MPI          5 70381.349609            0 
            3           256           256   gramschmidt           MPI          6 70053.361328            0 
            3           256           256   gramschmidt           MPI          7 69696.869141            0 
            3           256           256   gramschmidt           MPI          8 70148.947266            2 
            3           256           256   gramschmidt           MPI          9 70190.333984            0 
# Runtime: 0.919901 s (overhead: 0.000544 %) 10 records
