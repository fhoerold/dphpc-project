# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 17:28:18 2021
# Execution time and date (local): Tue Dec 21 18:28:18 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 69819.896484            0 
            2           256           256   gramschmidt           MPI          1 69908.832031            5 
            2           256           256   gramschmidt           MPI          2 70063.587891            1 
            2           256           256   gramschmidt           MPI          3 70119.353516            0 
            2           256           256   gramschmidt           MPI          4 69648.480469            1 
            2           256           256   gramschmidt           MPI          5 70502.529297            0 
            2           256           256   gramschmidt           MPI          6 70174.117188            0 
            2           256           256   gramschmidt           MPI          7 69812.949219            0 
            2           256           256   gramschmidt           MPI          8 70258.697266            1 
            2           256           256   gramschmidt           MPI          9 70304.941406            0 
# Runtime: 8.921480 s (overhead: 0.000090 %) 10 records
