# Sysname : Linux
# Nodename: eu-a6-003-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:31:36 2021
# Execution time and date (local): Tue Dec 21 08:31:36 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 2687834.439453            0 
            2          1024          1024   gramschmidt           MPI          1 2699356.515625            4 
            2          1024          1024   gramschmidt           MPI          2 2702862.685547            2 
            2          1024          1024   gramschmidt           MPI          3 2690084.199219            0 
            2          1024          1024   gramschmidt           MPI          4 2688530.314453            2 
            2          1024          1024   gramschmidt           MPI          5 2688623.822266            0 
            2          1024          1024   gramschmidt           MPI          6 2710586.400391            0 
            2          1024          1024   gramschmidt           MPI          7 2693962.087891            0 
            2          1024          1024   gramschmidt           MPI          8 2677681.646484            6 
            2          1024          1024   gramschmidt           MPI          9 2708651.240234            0 
# Runtime: 35.109689 s (overhead: 0.000040 %) 10 records
