# Sysname : Linux
# Nodename: eu-a6-003-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:31:36 2021
# Execution time and date (local): Tue Dec 21 08:31:36 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 2686330.775391            5 
            3          1024          1024   gramschmidt           MPI          1 2697839.460938            5 
            3          1024          1024   gramschmidt           MPI          2 2701523.750000            4 
            3          1024          1024   gramschmidt           MPI          3 2688570.847656            0 
            3          1024          1024   gramschmidt           MPI          4 2687025.199219            2 
            3          1024          1024   gramschmidt           MPI          5 2687106.783203            0 
            3          1024          1024   gramschmidt           MPI          6 2709056.595703            0 
            3          1024          1024   gramschmidt           MPI          7 2692441.861328            0 
            3          1024          1024   gramschmidt           MPI          8 2676173.238281            2 
            3          1024          1024   gramschmidt           MPI          9 2707279.289062            0 
# Runtime: 41.065787 s (overhead: 0.000044 %) 10 records
