# Sysname : Linux
# Nodename: eu-a6-005-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:32:05 2021
# Execution time and date (local): Tue Dec 21 08:32:05 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 33082.343750            0 
            2           256           256   gramschmidt           MPI          1 33067.835938            3 
            2           256           256   gramschmidt           MPI          2 33004.658203            1 
            2           256           256   gramschmidt           MPI          3 33016.093750            0 
            2           256           256   gramschmidt           MPI          4 32992.195312            1 
            2           256           256   gramschmidt           MPI          5 32991.333984            0 
            2           256           256   gramschmidt           MPI          6 32981.396484            0 
            2           256           256   gramschmidt           MPI          7 33010.490234            0 
            2           256           256   gramschmidt           MPI          8 33005.636719            0 
            2           256           256   gramschmidt           MPI          9 33046.558594            0 
# Runtime: 2.434964 s (overhead: 0.000205 %) 10 records
