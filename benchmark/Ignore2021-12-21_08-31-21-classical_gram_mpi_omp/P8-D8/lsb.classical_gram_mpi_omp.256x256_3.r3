# Sysname : Linux
# Nodename: eu-a6-005-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:32:05 2021
# Execution time and date (local): Tue Dec 21 08:32:05 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 33000.634766            0 
            3           256           256   gramschmidt           MPI          1 33006.664062            4 
            3           256           256   gramschmidt           MPI          2 32953.339844            0 
            3           256           256   gramschmidt           MPI          3 32939.820312            0 
            3           256           256   gramschmidt           MPI          4 32878.482422            0 
            3           256           256   gramschmidt           MPI          5 32884.128906            0 
            3           256           256   gramschmidt           MPI          6 32880.437500            0 
            3           256           256   gramschmidt           MPI          7 32919.359375            0 
            3           256           256   gramschmidt           MPI          8 32907.634766            0 
            3           256           256   gramschmidt           MPI          9 32947.322266            0 
# Runtime: 2.434321 s (overhead: 0.000164 %) 10 records
