# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:31:55 2021
# Execution time and date (local): Tue Dec 21 08:31:55 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 1710.443359            0 
            2            64            64   gramschmidt           MPI          1 1768.822266            0 
            2            64            64   gramschmidt           MPI          2 1752.476562            0 
            2            64            64   gramschmidt           MPI          3 1703.718750            0 
            2            64            64   gramschmidt           MPI          4 1697.687500            0 
            2            64            64   gramschmidt           MPI          5 1706.224609            0 
            2            64            64   gramschmidt           MPI          6 1711.767578            0 
            2            64            64   gramschmidt           MPI          7 1723.128906            0 
            2            64            64   gramschmidt           MPI          8 1716.220703            0 
            2            64            64   gramschmidt           MPI          9 1709.291016            0 
# Runtime: 0.025567 s (overhead: 0.000000 %) 10 records
