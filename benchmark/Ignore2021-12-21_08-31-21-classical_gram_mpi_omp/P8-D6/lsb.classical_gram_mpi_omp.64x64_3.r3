# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:31:55 2021
# Execution time and date (local): Tue Dec 21 08:31:55 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 1715.826172            0 
            3            64            64   gramschmidt           MPI          1 1754.671875            0 
            3            64            64   gramschmidt           MPI          2 1750.638672            0 
            3            64            64   gramschmidt           MPI          3 1700.511719            0 
            3            64            64   gramschmidt           MPI          4 1695.486328            0 
            3            64            64   gramschmidt           MPI          5 1704.574219            0 
            3            64            64   gramschmidt           MPI          6 1707.232422            0 
            3            64            64   gramschmidt           MPI          7 1725.488281            0 
            3            64            64   gramschmidt           MPI          8 1714.773438            0 
            3            64            64   gramschmidt           MPI          9 1709.451172            0 
# Runtime: 0.024934 s (overhead: 0.000000 %) 10 records
