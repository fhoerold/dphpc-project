# Sysname : Linux
# Nodename: eu-a6-004-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 22:51:28 2021
# Execution time and date (local): Wed Dec 22 23:51:28 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 73045.255859            0 
            3           128           128   gramschmidt           MPI          1 72938.935547            4 
            3           128           128   gramschmidt           MPI          2 73900.630859            0 
            3           128           128   gramschmidt           MPI          3 73797.281250            0 
            3           128           128   gramschmidt           MPI          4 74197.634766            0 
            3           128           128   gramschmidt           MPI          5 72951.417969            0 
            3           128           128   gramschmidt           MPI          6 73522.113281            0 
            3           128           128   gramschmidt           MPI          7 73636.134766            0 
            3           128           128   gramschmidt           MPI          8 73301.585938            1 
            3           128           128   gramschmidt           MPI          9 73129.828125            0 
# Runtime: 5.957311 s (overhead: 0.000084 %) 10 records
