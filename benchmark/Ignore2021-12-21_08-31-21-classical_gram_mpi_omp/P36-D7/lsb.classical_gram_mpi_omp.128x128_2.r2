# Sysname : Linux
# Nodename: eu-a6-004-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 22:51:28 2021
# Execution time and date (local): Wed Dec 22 23:51:28 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 73080.119141            0 
            2           128           128   gramschmidt           MPI          1 72975.042969            3 
            2           128           128   gramschmidt           MPI          2 73927.015625            0 
            2           128           128   gramschmidt           MPI          3 73833.820312            0 
            2           128           128   gramschmidt           MPI          4 74235.335938            0 
            2           128           128   gramschmidt           MPI          5 72970.222656            0 
            2           128           128   gramschmidt           MPI          6 73549.220703            0 
            2           128           128   gramschmidt           MPI          7 73665.435547            0 
            2           128           128   gramschmidt           MPI          8 73322.517578            0 
            2           128           128   gramschmidt           MPI          9 73164.214844            0 
# Runtime: 5.957283 s (overhead: 0.000050 %) 10 records
