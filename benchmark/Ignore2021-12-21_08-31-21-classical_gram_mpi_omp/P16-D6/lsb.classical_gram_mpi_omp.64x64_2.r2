# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:42:25 2021
# Execution time and date (local): Tue Dec 21 16:42:25 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 10682.472656            0 
            2            64            64   gramschmidt           MPI          1 10605.021484            4 
            2            64            64   gramschmidt           MPI          2 10657.744141            0 
            2            64            64   gramschmidt           MPI          3 10625.654297            0 
            2            64            64   gramschmidt           MPI          4 10739.791016            0 
            2            64            64   gramschmidt           MPI          5 10757.810547            0 
            2            64            64   gramschmidt           MPI          6 10594.689453            0 
            2            64            64   gramschmidt           MPI          7 10847.914062            0 
            2            64            64   gramschmidt           MPI          8 10576.628906            0 
            2            64            64   gramschmidt           MPI          9 10753.718750            0 
# Runtime: 17.141228 s (overhead: 0.000023 %) 10 records
