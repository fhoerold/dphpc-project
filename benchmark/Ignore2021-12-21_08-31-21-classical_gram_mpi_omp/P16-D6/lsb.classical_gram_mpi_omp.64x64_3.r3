# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:42:25 2021
# Execution time and date (local): Tue Dec 21 16:42:25 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 10697.707031            0 
            3            64            64   gramschmidt           MPI          1 10618.054688            0 
            3            64            64   gramschmidt           MPI          2 10668.302734            0 
            3            64            64   gramschmidt           MPI          3 10625.458984            0 
            3            64            64   gramschmidt           MPI          4 10747.646484            0 
            3            64            64   gramschmidt           MPI          5 10755.960938            0 
            3            64            64   gramschmidt           MPI          6 10590.412109            0 
            3            64            64   gramschmidt           MPI          7 10854.335938            0 
            3            64            64   gramschmidt           MPI          8 10588.687500            0 
            3            64            64   gramschmidt           MPI          9 10749.638672            0 
# Runtime: 0.141912 s (overhead: 0.000000 %) 10 records
