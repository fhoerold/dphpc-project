# Sysname : Linux
# Nodename: eu-a6-003-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:46:57 2021
# Execution time and date (local): Tue Dec 21 16:46:57 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 23811.402344            0 
            2           128           128   gramschmidt           MPI          1 23640.976562            4 
            2           128           128   gramschmidt           MPI          2 23897.710938            1 
            2           128           128   gramschmidt           MPI          3 23923.291016            0 
            2           128           128   gramschmidt           MPI          4 23819.986328            1 
            2           128           128   gramschmidt           MPI          5 23559.255859            0 
            2           128           128   gramschmidt           MPI          6 23759.546875            0 
            2           128           128   gramschmidt           MPI          7 23942.375000            0 
            2           128           128   gramschmidt           MPI          8 23759.304688            1 
            2           128           128   gramschmidt           MPI          9 23676.552734            0 
# Runtime: 8.313354 s (overhead: 0.000084 %) 10 records
