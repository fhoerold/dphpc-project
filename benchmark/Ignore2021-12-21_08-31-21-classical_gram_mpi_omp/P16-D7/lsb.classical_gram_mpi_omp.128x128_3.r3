# Sysname : Linux
# Nodename: eu-a6-003-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:46:57 2021
# Execution time and date (local): Tue Dec 21 16:46:57 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 23762.023438            0 
            3           128           128   gramschmidt           MPI          1 23590.697266            1 
            3           128           128   gramschmidt           MPI          2 23841.308594            1 
            3           128           128   gramschmidt           MPI          3 23867.126953            0 
            3           128           128   gramschmidt           MPI          4 23765.707031            0 
            3           128           128   gramschmidt           MPI          5 23509.810547            0 
            3           128           128   gramschmidt           MPI          6 23714.671875            0 
            3           128           128   gramschmidt           MPI          7 23898.664062            0 
            3           128           128   gramschmidt           MPI          8 23708.503906            1 
            3           128           128   gramschmidt           MPI          9 23624.750000            0 
# Runtime: 0.313641 s (overhead: 0.000957 %) 10 records
