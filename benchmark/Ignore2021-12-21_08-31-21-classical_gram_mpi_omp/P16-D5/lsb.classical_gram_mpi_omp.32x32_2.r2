# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:41:49 2021
# Execution time and date (local): Tue Dec 21 16:41:49 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 5107.046875            0 
            2            32            32   gramschmidt           MPI          1 5004.994141            2 
            2            32            32   gramschmidt           MPI          2 5007.035156            0 
            2            32            32   gramschmidt           MPI          3 4968.761719            0 
            2            32            32   gramschmidt           MPI          4 4999.962891            0 
            2            32            32   gramschmidt           MPI          5 4952.376953            0 
            2            32            32   gramschmidt           MPI          6 5107.074219            0 
            2            32            32   gramschmidt           MPI          7 4979.046875            0 
            2            32            32   gramschmidt           MPI          8 4988.320312            0 
            2            32            32   gramschmidt           MPI          9 5321.132812            0 
# Runtime: 6.069199 s (overhead: 0.000033 %) 10 records
