# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:41:49 2021
# Execution time and date (local): Tue Dec 21 16:41:49 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 5100.093750            0 
            3            32            32   gramschmidt           MPI          1 5000.888672            0 
            3            32            32   gramschmidt           MPI          2 5002.884766            0 
            3            32            32   gramschmidt           MPI          3 4963.027344            0 
            3            32            32   gramschmidt           MPI          4 4994.947266            0 
            3            32            32   gramschmidt           MPI          5 4950.316406            0 
            3            32            32   gramschmidt           MPI          6 5105.957031            0 
            3            32            32   gramschmidt           MPI          7 4976.332031            0 
            3            32            32   gramschmidt           MPI          8 4980.591797            0 
            3            32            32   gramschmidt           MPI          9 5317.869141            0 
# Runtime: 0.067835 s (overhead: 0.000000 %) 10 records
