# Sysname : Linux
# Nodename: eu-a6-003-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:31:36 2021
# Execution time and date (local): Tue Dec 21 08:31:36 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 1672.777344            0 
            3            64            64   gramschmidt           MPI          1 1678.156250            2 
            3            64            64   gramschmidt           MPI          2 1670.980469            0 
            3            64            64   gramschmidt           MPI          3 1746.890625            0 
            3            64            64   gramschmidt           MPI          4 1757.527344            0 
            3            64            64   gramschmidt           MPI          5 1680.826172            0 
            3            64            64   gramschmidt           MPI          6 1678.802734            0 
            3            64            64   gramschmidt           MPI          7 1692.861328            0 
            3            64            64   gramschmidt           MPI          8 1655.810547            0 
            3            64            64   gramschmidt           MPI          9 1697.222656            0 
# Runtime: 1.016258 s (overhead: 0.000197 %) 10 records
