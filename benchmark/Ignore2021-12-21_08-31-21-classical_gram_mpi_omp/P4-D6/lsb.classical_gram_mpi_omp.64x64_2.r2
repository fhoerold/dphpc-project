# Sysname : Linux
# Nodename: eu-a6-003-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:31:36 2021
# Execution time and date (local): Tue Dec 21 08:31:36 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 1661.826172            0 
            2            64            64   gramschmidt           MPI          1 1688.867188            2 
            2            64            64   gramschmidt           MPI          2 1676.837891            0 
            2            64            64   gramschmidt           MPI          3 1744.468750            0 
            2            64            64   gramschmidt           MPI          4 1752.421875            0 
            2            64            64   gramschmidt           MPI          5 1680.175781            0 
            2            64            64   gramschmidt           MPI          6 1674.970703            0 
            2            64            64   gramschmidt           MPI          7 1694.271484            0 
            2            64            64   gramschmidt           MPI          8 1652.658203            0 
            2            64            64   gramschmidt           MPI          9 1693.285156            0 
# Runtime: 4.023837 s (overhead: 0.000050 %) 10 records
