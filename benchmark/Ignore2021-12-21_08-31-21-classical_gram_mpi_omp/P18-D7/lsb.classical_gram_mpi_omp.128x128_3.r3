# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 17:35:22 2021
# Execution time and date (local): Tue Dec 21 18:35:22 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 23575.005859            0 
            3           128           128   gramschmidt           MPI          1 23484.351562            5 
            3           128           128   gramschmidt           MPI          2 23417.609375            1 
            3           128           128   gramschmidt           MPI          3 23737.937500            0 
            3           128           128   gramschmidt           MPI          4 23656.384766            1 
            3           128           128   gramschmidt           MPI          5 23493.929688            0 
            3           128           128   gramschmidt           MPI          6 23445.376953            0 
            3           128           128   gramschmidt           MPI          7 23623.666016            0 
            3           128           128   gramschmidt           MPI          8 23860.091797            1 
            3           128           128   gramschmidt           MPI          9 23426.373047            0 
# Runtime: 5.311410 s (overhead: 0.000151 %) 10 records
