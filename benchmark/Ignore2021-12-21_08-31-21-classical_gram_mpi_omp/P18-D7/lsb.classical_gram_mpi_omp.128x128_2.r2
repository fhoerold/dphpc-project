# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 17:35:22 2021
# Execution time and date (local): Tue Dec 21 18:35:22 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 23607.603516            0 
            2           128           128   gramschmidt           MPI          1 23532.574219            3 
            2           128           128   gramschmidt           MPI          2 23447.480469            1 
            2           128           128   gramschmidt           MPI          3 23881.939453            0 
            2           128           128   gramschmidt           MPI          4 23697.681641            0 
            2           128           128   gramschmidt           MPI          5 23524.275391            0 
            2           128           128   gramschmidt           MPI          6 23494.820312            0 
            2           128           128   gramschmidt           MPI          7 23653.347656            0 
            2           128           128   gramschmidt           MPI          8 23887.039062            1 
            2           128           128   gramschmidt           MPI          9 23455.392578            0 
# Runtime: 2.311833 s (overhead: 0.000216 %) 10 records
