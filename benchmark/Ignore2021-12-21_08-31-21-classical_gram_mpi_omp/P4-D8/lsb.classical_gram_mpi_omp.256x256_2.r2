# Sysname : Linux
# Nodename: eu-a6-003-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:31:36 2021
# Execution time and date (local): Tue Dec 21 08:31:36 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 35971.076172            0 
            2           256           256   gramschmidt           MPI          1 35908.978516            3 
            2           256           256   gramschmidt           MPI          2 35921.115234            0 
            2           256           256   gramschmidt           MPI          3 35884.291016            0 
            2           256           256   gramschmidt           MPI          4 35805.738281            0 
            2           256           256   gramschmidt           MPI          5 35872.402344            0 
            2           256           256   gramschmidt           MPI          6 36052.095703            0 
            2           256           256   gramschmidt           MPI          7 35907.587891            0 
            2           256           256   gramschmidt           MPI          8 35975.351562            0 
            2           256           256   gramschmidt           MPI          9 35837.767578            0 
# Runtime: 2.473896 s (overhead: 0.000121 %) 10 records
