# Sysname : Linux
# Nodename: eu-a6-003-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:31:36 2021
# Execution time and date (local): Tue Dec 21 08:31:36 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 35890.757812            0 
            3           256           256   gramschmidt           MPI          1 35830.921875            3 
            3           256           256   gramschmidt           MPI          2 35852.642578            0 
            3           256           256   gramschmidt           MPI          3 35817.312500            0 
            3           256           256   gramschmidt           MPI          4 35740.216797            0 
            3           256           256   gramschmidt           MPI          5 35773.052734            0 
            3           256           256   gramschmidt           MPI          6 35944.195312            0 
            3           256           256   gramschmidt           MPI          7 35805.253906            0 
            3           256           256   gramschmidt           MPI          8 35874.199219            0 
            3           256           256   gramschmidt           MPI          9 35739.689453            0 
# Runtime: 2.473533 s (overhead: 0.000121 %) 10 records
