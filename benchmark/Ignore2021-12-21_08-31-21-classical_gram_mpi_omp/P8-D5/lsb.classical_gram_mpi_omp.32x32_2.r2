# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:31:35 2021
# Execution time and date (local): Tue Dec 21 08:31:35 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 661.970703            0 
            2            32            32   gramschmidt           MPI          1 659.427734            2 
            2            32            32   gramschmidt           MPI          2 660.812500            0 
            2            32            32   gramschmidt           MPI          3 646.542969            0 
            2            32            32   gramschmidt           MPI          4 654.919922            0 
            2            32            32   gramschmidt           MPI          5 656.134766            0 
            2            32            32   gramschmidt           MPI          6 648.287109            0 
            2            32            32   gramschmidt           MPI          7 720.771484            0 
            2            32            32   gramschmidt           MPI          8 655.990234            0 
            2            32            32   gramschmidt           MPI          9 651.771484            0 
# Runtime: 11.009576 s (overhead: 0.000018 %) 10 records
