# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:31:35 2021
# Execution time and date (local): Tue Dec 21 08:31:35 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 658.414062            0 
            3            32            32   gramschmidt           MPI          1 655.398438            0 
            3            32            32   gramschmidt           MPI          2 655.339844            0 
            3            32            32   gramschmidt           MPI          3 641.714844            0 
            3            32            32   gramschmidt           MPI          4 650.091797            0 
            3            32            32   gramschmidt           MPI          5 652.976562            0 
            3            32            32   gramschmidt           MPI          6 644.960938            0 
            3            32            32   gramschmidt           MPI          7 717.091797            0 
            3            32            32   gramschmidt           MPI          8 652.234375            0 
            3            32            32   gramschmidt           MPI          9 648.277344            0 
# Runtime: 0.010142 s (overhead: 0.000000 %) 10 records
