# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:33:46 2021
# Execution time and date (local): Tue Dec 21 08:33:46 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 25265.705078            0 
            2           128           128   gramschmidt           MPI          1 25241.445312            0 
            2           128           128   gramschmidt           MPI          2 25171.298828            0 
            2           128           128   gramschmidt           MPI          3 25723.566406            0 
            2           128           128   gramschmidt           MPI          4 25235.949219            0 
            2           128           128   gramschmidt           MPI          5 25147.255859            0 
            2           128           128   gramschmidt           MPI          6 25190.671875            0 
            2           128           128   gramschmidt           MPI          7 25773.679688            0 
            2           128           128   gramschmidt           MPI          8 25231.476562            0 
            2           128           128   gramschmidt           MPI          9 25502.796875            0 
# Runtime: 0.333435 s (overhead: 0.000000 %) 10 records
