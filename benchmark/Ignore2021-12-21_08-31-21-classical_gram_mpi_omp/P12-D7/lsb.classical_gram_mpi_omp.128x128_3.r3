# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:33:46 2021
# Execution time and date (local): Tue Dec 21 08:33:46 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 25230.089844            0 
            3           128           128   gramschmidt           MPI          1 25178.689453            0 
            3           128           128   gramschmidt           MPI          2 25147.910156            0 
            3           128           128   gramschmidt           MPI          3 25667.076172            0 
            3           128           128   gramschmidt           MPI          4 25209.205078            0 
            3           128           128   gramschmidt           MPI          5 25089.714844            0 
            3           128           128   gramschmidt           MPI          6 25169.923828            0 
            3           128           128   gramschmidt           MPI          7 25711.728516            0 
            3           128           128   gramschmidt           MPI          8 25201.810547            0 
            3           128           128   gramschmidt           MPI          9 25427.082031            0 
# Runtime: 0.333730 s (overhead: 0.000000 %) 10 records
