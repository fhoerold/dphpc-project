# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 05:19:02 2021
# Execution time and date (local): Wed Dec 22 06:19:02 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 58754.617188            0 
            3           128           128   gramschmidt           MPI          1 58693.917969            5 
            3           128           128   gramschmidt           MPI          2 58197.052734            1 
            3           128           128   gramschmidt           MPI          3 58452.402344            0 
            3           128           128   gramschmidt           MPI          4 58348.310547            1 
            3           128           128   gramschmidt           MPI          5 58036.576172            0 
            3           128           128   gramschmidt           MPI          6 58325.054688            0 
            3           128           128   gramschmidt           MPI          7 58016.281250            0 
            3           128           128   gramschmidt           MPI          8 58358.416016            1 
            3           128           128   gramschmidt           MPI          9 57972.832031            0 
# Runtime: 3.766027 s (overhead: 0.000212 %) 10 records
