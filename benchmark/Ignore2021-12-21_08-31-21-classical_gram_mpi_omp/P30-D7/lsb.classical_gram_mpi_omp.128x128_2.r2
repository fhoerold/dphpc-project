# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 05:19:02 2021
# Execution time and date (local): Wed Dec 22 06:19:02 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 58841.580078            0 
            2           128           128   gramschmidt           MPI          1 58777.210938            4 
            2           128           128   gramschmidt           MPI          2 58257.937500            1 
            2           128           128   gramschmidt           MPI          3 58515.132812            0 
            2           128           128   gramschmidt           MPI          4 58397.226562            0 
            2           128           128   gramschmidt           MPI          5 58095.091797            0 
            2           128           128   gramschmidt           MPI          6 58389.880859            0 
            2           128           128   gramschmidt           MPI          7 58093.160156            0 
            2           128           128   gramschmidt           MPI          8 58426.769531            0 
            2           128           128   gramschmidt           MPI          9 58033.648438            0 
# Runtime: 3.765873 s (overhead: 0.000133 %) 10 records
