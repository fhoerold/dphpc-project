# Sysname : Linux
# Nodename: eu-a6-009-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 19:32:19 2021
# Execution time and date (local): Tue Dec 21 20:32:19 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 19150.445312            0 
            2            64            64   gramschmidt           MPI          1 19130.853516            4 
            2            64            64   gramschmidt           MPI          2 19325.468750            0 
            2            64            64   gramschmidt           MPI          3 19083.972656            0 
            2            64            64   gramschmidt           MPI          4 19520.089844            0 
            2            64            64   gramschmidt           MPI          5 19102.640625            0 
            2            64            64   gramschmidt           MPI          6 19084.121094            0 
            2            64            64   gramschmidt           MPI          7 19268.888672            0 
            2            64            64   gramschmidt           MPI          8 19063.339844            1 
            2            64            64   gramschmidt           MPI          9 18990.613281            0 
# Runtime: 1.254619 s (overhead: 0.000399 %) 10 records
