# Sysname : Linux
# Nodename: eu-a6-009-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 19:32:19 2021
# Execution time and date (local): Tue Dec 21 20:32:19 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 19154.265625            0 
            3            64            64   gramschmidt           MPI          1 19128.638672            1 
            3            64            64   gramschmidt           MPI          2 19320.787109            1 
            3            64            64   gramschmidt           MPI          3 19085.746094            0 
            3            64            64   gramschmidt           MPI          4 19514.859375            1 
            3            64            64   gramschmidt           MPI          5 19110.195312            0 
            3            64            64   gramschmidt           MPI          6 19093.060547            0 
            3            64            64   gramschmidt           MPI          7 19272.978516            0 
            3            64            64   gramschmidt           MPI          8 19077.384766            1 
            3            64            64   gramschmidt           MPI          9 18983.902344            0 
# Runtime: 0.254527 s (overhead: 0.001572 %) 10 records
