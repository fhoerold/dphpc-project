# Sysname : Linux
# Nodename: eu-a6-003-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:47:13 2021
# Execution time and date (local): Tue Dec 21 16:47:13 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 70283.232422            0 
            2           256           256   gramschmidt           MPI          1 70463.853516            1 
            2           256           256   gramschmidt           MPI          2 70484.412109            0 
            2           256           256   gramschmidt           MPI          3 70462.121094            0 
            2           256           256   gramschmidt           MPI          4 69951.367188            1 
            2           256           256   gramschmidt           MPI          5 70214.289062            0 
            2           256           256   gramschmidt           MPI          6 70425.878906            0 
            2           256           256   gramschmidt           MPI          7 69996.507812            0 
            2           256           256   gramschmidt           MPI          8 70362.464844            1 
            2           256           256   gramschmidt           MPI          9 70608.626953            0 
# Runtime: 0.924659 s (overhead: 0.000324 %) 10 records
