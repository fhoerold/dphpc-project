# Sysname : Linux
# Nodename: eu-a6-003-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:47:13 2021
# Execution time and date (local): Tue Dec 21 16:47:13 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 70180.275391            0 
            3           256           256   gramschmidt           MPI          1 70378.703125            5 
            3           256           256   gramschmidt           MPI          2 70387.140625            1 
            3           256           256   gramschmidt           MPI          3 70361.029297            0 
            3           256           256   gramschmidt           MPI          4 69827.570312            1 
            3           256           256   gramschmidt           MPI          5 70077.449219            0 
            3           256           256   gramschmidt           MPI          6 70313.871094            0 
            3           256           256   gramschmidt           MPI          7 69876.851562            0 
            3           256           256   gramschmidt           MPI          8 70245.402344            1 
            3           256           256   gramschmidt           MPI          9 70482.746094            0 
# Runtime: 13.927306 s (overhead: 0.000057 %) 10 records
