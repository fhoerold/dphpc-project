# Sysname : Linux
# Nodename: eu-a6-009-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 19:36:24 2021
# Execution time and date (local): Tue Dec 21 20:36:24 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 41799.960938            0 
            3           128           128   gramschmidt           MPI          1 41255.679688            1 
            3           128           128   gramschmidt           MPI          2 41262.285156            1 
            3           128           128   gramschmidt           MPI          3 41686.837891            0 
            3           128           128   gramschmidt           MPI          4 41258.595703            1 
            3           128           128   gramschmidt           MPI          5 41746.816406            0 
            3           128           128   gramschmidt           MPI          6 41431.000000            0 
            3           128           128   gramschmidt           MPI          7 41511.472656            0 
            3           128           128   gramschmidt           MPI          8 41562.679688            1 
            3           128           128   gramschmidt           MPI          9 41143.400391            0 
# Runtime: 0.544359 s (overhead: 0.000735 %) 10 records
