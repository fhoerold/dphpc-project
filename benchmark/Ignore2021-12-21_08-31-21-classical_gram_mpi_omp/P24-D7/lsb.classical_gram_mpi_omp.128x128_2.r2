# Sysname : Linux
# Nodename: eu-a6-009-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 19:36:24 2021
# Execution time and date (local): Tue Dec 21 20:36:24 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 41835.318359            0 
            2           128           128   gramschmidt           MPI          1 41296.568359            5 
            2           128           128   gramschmidt           MPI          2 41307.929688            1 
            2           128           128   gramschmidt           MPI          3 41713.900391            0 
            2           128           128   gramschmidt           MPI          4 41305.681641            1 
            2           128           128   gramschmidt           MPI          5 41782.851562            0 
            2           128           128   gramschmidt           MPI          6 41478.587891            0 
            2           128           128   gramschmidt           MPI          7 41540.941406            0 
            2           128           128   gramschmidt           MPI          8 41590.640625            1 
            2           128           128   gramschmidt           MPI          9 41170.585938            0 
# Runtime: 7.544674 s (overhead: 0.000106 %) 10 records
