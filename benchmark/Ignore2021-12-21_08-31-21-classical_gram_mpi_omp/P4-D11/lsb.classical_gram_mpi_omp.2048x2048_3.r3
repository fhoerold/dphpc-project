# Sysname : Linux
# Nodename: eu-a6-003-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:31:36 2021
# Execution time and date (local): Tue Dec 21 08:31:36 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 21720323.125000            0 
            3          2048          2048   gramschmidt           MPI          1 21869135.519531            4 
            3          2048          2048   gramschmidt           MPI          2 22011575.195312            4 
            3          2048          2048   gramschmidt           MPI          3 21941500.134766            0 
            3          2048          2048   gramschmidt           MPI          4 21718040.496094            5 
            3          2048          2048   gramschmidt           MPI          5 21847297.121094            0 
            3          2048          2048   gramschmidt           MPI          6 21989841.314453            0 
            3          2048          2048   gramschmidt           MPI          7 21869063.156250            0 
            3          2048          2048   gramschmidt           MPI          8 21822379.949219            5 
            3          2048          2048   gramschmidt           MPI          9 22004537.140625            0 
# Runtime: 287.872301 s (overhead: 0.000006 %) 10 records
