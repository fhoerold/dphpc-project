# Sysname : Linux
# Nodename: eu-a6-003-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:31:36 2021
# Execution time and date (local): Tue Dec 21 08:31:36 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 21702030.953125            0 
            2          2048          2048   gramschmidt           MPI          1 21850207.169922            5 
            2          2048          2048   gramschmidt           MPI          2 21992599.099609            5 
            2          2048          2048   gramschmidt           MPI          3 21922575.148438            0 
            2          2048          2048   gramschmidt           MPI          4 21699768.224609            7 
            2          2048          2048   gramschmidt           MPI          5 21828448.869141            0 
            2          2048          2048   gramschmidt           MPI          6 21970856.652344            0 
            2          2048          2048   gramschmidt           MPI          7 21850213.871094            0 
            2          2048          2048   gramschmidt           MPI          8 21804041.025391            7 
            2          2048          2048   gramschmidt           MPI          9 21985543.871094            0 
# Runtime: 292.666399 s (overhead: 0.000008 %) 10 records
