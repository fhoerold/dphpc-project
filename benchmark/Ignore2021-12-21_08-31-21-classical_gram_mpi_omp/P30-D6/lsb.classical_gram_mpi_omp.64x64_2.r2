# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 05:18:49 2021
# Execution time and date (local): Wed Dec 22 06:18:49 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 27722.548828            0 
            2            64            64   gramschmidt           MPI          1 27703.101562            5 
            2            64            64   gramschmidt           MPI          2 27660.783203            0 
            2            64            64   gramschmidt           MPI          3 28175.955078            0 
            2            64            64   gramschmidt           MPI          4 27702.197266            1 
            2            64            64   gramschmidt           MPI          5 27642.572266            0 
            2            64            64   gramschmidt           MPI          6 28176.707031            0 
            2            64            64   gramschmidt           MPI          7 27605.343750            0 
            2            64            64   gramschmidt           MPI          8 27470.023438            1 
            2            64            64   gramschmidt           MPI          9 27439.181641            0 
# Runtime: 5.364224 s (overhead: 0.000130 %) 10 records
