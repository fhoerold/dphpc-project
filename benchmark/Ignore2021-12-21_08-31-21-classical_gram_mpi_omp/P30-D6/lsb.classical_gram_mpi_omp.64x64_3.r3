# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 05:18:49 2021
# Execution time and date (local): Wed Dec 22 06:18:49 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 27740.425781            0 
            3            64            64   gramschmidt           MPI          1 27698.080078            1 
            3            64            64   gramschmidt           MPI          2 27653.224609            0 
            3            64            64   gramschmidt           MPI          3 28169.388672            0 
            3            64            64   gramschmidt           MPI          4 27708.240234            0 
            3            64            64   gramschmidt           MPI          5 27647.388672            0 
            3            64            64   gramschmidt           MPI          6 28190.089844            0 
            3            64            64   gramschmidt           MPI          7 27611.830078            0 
            3            64            64   gramschmidt           MPI          8 27484.136719            1 
            3            64            64   gramschmidt           MPI          9 27446.039062            0 
# Runtime: 0.363273 s (overhead: 0.000551 %) 10 records
