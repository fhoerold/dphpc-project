# Sysname : Linux
# Nodename: eu-a6-009-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 19:32:27 2021
# Execution time and date (local): Tue Dec 21 20:32:27 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 104438.371094            0 
            2           256           256   gramschmidt           MPI          1 104574.720703            5 
            2           256           256   gramschmidt           MPI          2 104618.554688            1 
            2           256           256   gramschmidt           MPI          3 104729.804688            0 
            2           256           256   gramschmidt           MPI          4 104401.966797            1 
            2           256           256   gramschmidt           MPI          5 104639.029297            0 
            2           256           256   gramschmidt           MPI          6 104821.154297            0 
            2           256           256   gramschmidt           MPI          7 105215.699219            0 
            2           256           256   gramschmidt           MPI          8 105907.777344            1 
            2           256           256   gramschmidt           MPI          9 105663.050781            0 
# Runtime: 2.374645 s (overhead: 0.000337 %) 10 records
