# Sysname : Linux
# Nodename: eu-a6-009-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 19:32:27 2021
# Execution time and date (local): Tue Dec 21 20:32:27 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 104330.664062            0 
            3           256           256   gramschmidt           MPI          1 104482.810547            6 
            3           256           256   gramschmidt           MPI          2 104522.214844            1 
            3           256           256   gramschmidt           MPI          3 104647.468750            0 
            3           256           256   gramschmidt           MPI          4 104253.679688            1 
            3           256           256   gramschmidt           MPI          5 104497.621094            0 
            3           256           256   gramschmidt           MPI          6 104696.017578            0 
            3           256           256   gramschmidt           MPI          7 105078.630859            0 
            3           256           256   gramschmidt           MPI          8 105766.841797            2 
            3           256           256   gramschmidt           MPI          9 105531.572266            0 
# Runtime: 3.372649 s (overhead: 0.000297 %) 10 records
