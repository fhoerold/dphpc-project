# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 05:18:23 2021
# Execution time and date (local): Wed Dec 22 06:18:23 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 13302.787109            0 
            2            32            32   gramschmidt           MPI          1 13118.949219            3 
            2            32            32   gramschmidt           MPI          2 13116.599609            0 
            2            32            32   gramschmidt           MPI          3 13143.859375            0 
            2            32            32   gramschmidt           MPI          4 13112.138672            0 
            2            32            32   gramschmidt           MPI          5 13548.294922            0 
            2            32            32   gramschmidt           MPI          6 13124.589844            0 
            2            32            32   gramschmidt           MPI          7 13130.939453            0 
            2            32            32   gramschmidt           MPI          8 13168.765625            0 
            2            32            32   gramschmidt           MPI          9 13126.365234            0 
# Runtime: 2.174656 s (overhead: 0.000138 %) 10 records
