# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 05:18:23 2021
# Execution time and date (local): Wed Dec 22 06:18:23 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 13298.931641            0 
            3            32            32   gramschmidt           MPI          1 13113.464844            3 
            3            32            32   gramschmidt           MPI          2 13109.001953            0 
            3            32            32   gramschmidt           MPI          3 13135.320312            0 
            3            32            32   gramschmidt           MPI          4 13091.531250            0 
            3            32            32   gramschmidt           MPI          5 13534.421875            0 
            3            32            32   gramschmidt           MPI          6 13105.662109            0 
            3            32            32   gramschmidt           MPI          7 13129.156250            0 
            3            32            32   gramschmidt           MPI          8 13165.425781            0 
            3            32            32   gramschmidt           MPI          9 13120.314453            0 
# Runtime: 3.173947 s (overhead: 0.000095 %) 10 records
