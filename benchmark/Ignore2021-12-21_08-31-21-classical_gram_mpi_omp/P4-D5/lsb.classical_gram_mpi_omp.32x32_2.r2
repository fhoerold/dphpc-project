# Sysname : Linux
# Nodename: eu-a6-012-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:31:35 2021
# Execution time and date (local): Tue Dec 21 08:31:35 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 701.171875            0 
            2            32            32   gramschmidt           MPI          1 666.828125            0 
            2            32            32   gramschmidt           MPI          2 666.208984            0 
            2            32            32   gramschmidt           MPI          3 670.525391            0 
            2            32            32   gramschmidt           MPI          4 680.242188            0 
            2            32            32   gramschmidt           MPI          5 649.986328            0 
            2            32            32   gramschmidt           MPI          6 650.851562            0 
            2            32            32   gramschmidt           MPI          7 691.046875            0 
            2            32            32   gramschmidt           MPI          8 668.134766            0 
            2            32            32   gramschmidt           MPI          9 683.933594            0 
# Runtime: 0.016719 s (overhead: 0.000000 %) 10 records
