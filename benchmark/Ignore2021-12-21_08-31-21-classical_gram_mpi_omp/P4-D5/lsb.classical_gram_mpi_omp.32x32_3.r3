# Sysname : Linux
# Nodename: eu-a6-012-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:31:35 2021
# Execution time and date (local): Tue Dec 21 08:31:35 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 697.080078            0 
            3            32            32   gramschmidt           MPI          1 663.945312            2 
            3            32            32   gramschmidt           MPI          2 661.142578            0 
            3            32            32   gramschmidt           MPI          3 665.203125            0 
            3            32            32   gramschmidt           MPI          4 676.042969            0 
            3            32            32   gramschmidt           MPI          5 642.996094            0 
            3            32            32   gramschmidt           MPI          6 646.205078            0 
            3            32            32   gramschmidt           MPI          7 686.619141            0 
            3            32            32   gramschmidt           MPI          8 662.664062            0 
            3            32            32   gramschmidt           MPI          9 681.138672            0 
# Runtime: 2.002575 s (overhead: 0.000100 %) 10 records
