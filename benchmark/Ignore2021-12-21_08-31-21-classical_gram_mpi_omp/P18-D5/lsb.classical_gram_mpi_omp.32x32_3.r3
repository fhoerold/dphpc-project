# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 17:28:09 2021
# Execution time and date (local): Tue Dec 21 18:28:09 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 5174.087891            0 
            3            32            32   gramschmidt           MPI          1 4944.722656            2 
            3            32            32   gramschmidt           MPI          2 4912.658203            0 
            3            32            32   gramschmidt           MPI          3 4879.617188            0 
            3            32            32   gramschmidt           MPI          4 4892.539062            0 
            3            32            32   gramschmidt           MPI          5 4880.648438            0 
            3            32            32   gramschmidt           MPI          6 5201.703125            0 
            3            32            32   gramschmidt           MPI          7 4847.671875            0 
            3            32            32   gramschmidt           MPI          8 4885.121094            0 
            3            32            32   gramschmidt           MPI          9 4896.033203            0 
# Runtime: 2.066253 s (overhead: 0.000097 %) 10 records
