# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 17:28:09 2021
# Execution time and date (local): Tue Dec 21 18:28:09 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 5177.869141            0 
            2            32            32   gramschmidt           MPI          1 4948.328125            2 
            2            32            32   gramschmidt           MPI          2 4917.291016            0 
            2            32            32   gramschmidt           MPI          3 4885.335938            0 
            2            32            32   gramschmidt           MPI          4 4896.791016            0 
            2            32            32   gramschmidt           MPI          5 4881.943359            0 
            2            32            32   gramschmidt           MPI          6 5204.621094            0 
            2            32            32   gramschmidt           MPI          7 4852.451172            0 
            2            32            32   gramschmidt           MPI          8 4889.744141            0 
            2            32            32   gramschmidt           MPI          9 4904.103516            0 
# Runtime: 2.066858 s (overhead: 0.000097 %) 10 records
