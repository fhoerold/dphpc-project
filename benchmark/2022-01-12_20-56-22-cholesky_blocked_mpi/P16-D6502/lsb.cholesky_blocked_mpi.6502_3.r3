# Sysname : Linux
# Nodename: eu-a6-008-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 20:31:14 2022
# Execution time and date (local): Wed Jan 12 21:31:14 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          6502            64           MPI          0 5407174.355469            0 
            3          6502            64           MPI          1 5306863.697266            1 
            3          6502            64           MPI          2 5258091.148438            3 
            3          6502            64           MPI          3 5255528.433594            0 
            3          6502            64           MPI          4 5298062.158203            1 
# Runtime: 38.891142 s (overhead: 0.000013 %) 5 records
