# Sysname : Linux
# Nodename: eu-a6-008-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 20:31:14 2022
# Execution time and date (local): Wed Jan 12 21:31:14 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          6502            64           MPI          0 5406078.410156            0 
            2          6502            64           MPI          1 5305749.697266           10 
            2          6502            64           MPI          2 5256941.138672            2 
            2          6502            64           MPI          3 5254643.763672            0 
            2          6502            64           MPI          4 5296929.603516            2 
# Runtime: 50.888568 s (overhead: 0.000028 %) 5 records
