# Sysname : Linux
# Nodename: eu-a6-011-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 22:20:13 2022
# Execution time and date (local): Wed Jan 12 23:20:13 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          5161          5161   gramschmidt           MPI          0 113243511.949219            0 
            2          5161          5161   gramschmidt           MPI          1 112852234.734375            4 
            2          5161          5161   gramschmidt           MPI          2 112726244.058594            4 
            2          5161          5161   gramschmidt           MPI          3 112744672.417969            2 
            2          5161          5161   gramschmidt           MPI          4 112724222.482422            6 
            2          5161          5161   gramschmidt           MPI          5 112856786.011719            2 
            2          5161          5161   gramschmidt           MPI          6 112616686.302734            2 
            2          5161          5161   gramschmidt           MPI          7 112617385.906250            2 
            2          5161          5161   gramschmidt           MPI          8 112597801.898438            8 
            2          5161          5161   gramschmidt           MPI          9 112736788.322266            2 
# Runtime: 1473.646637 s (overhead: 0.000002 %) 10 records
