# Sysname : Linux
# Nodename: eu-a6-011-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 22:20:13 2022
# Execution time and date (local): Wed Jan 12 23:20:13 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          5161          5161   gramschmidt           MPI          0 113250844.335938            0 
            3          5161          5161   gramschmidt           MPI          1 112859716.478516            4 
            3          5161          5161   gramschmidt           MPI          2 112733659.380859            5 
            3          5161          5161   gramschmidt           MPI          3 112753455.687500            0 
            3          5161          5161   gramschmidt           MPI          4 112733052.035156            8 
            3          5161          5161   gramschmidt           MPI          5 112865905.869141            4 
            3          5161          5161   gramschmidt           MPI          6 112625538.535156            0 
            3          5161          5161   gramschmidt           MPI          7 112626183.527344            0 
            3          5161          5161   gramschmidt           MPI          8 112606564.314453            7 
            3          5161          5161   gramschmidt           MPI          9 112745958.783203            0 
# Runtime: 1468.668918 s (overhead: 0.000002 %) 10 records
