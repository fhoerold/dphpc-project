# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:31:26 2022
# Execution time and date (local): Fri Jan  7 17:31:26 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           256           256           MPI          0 4.501953            1 
            2           256           256           MPI          1 0.068359            3 
            2           256           256           MPI          2 0.041016            0 
            2           256           256           MPI          3 0.023438            0 
            2           256           256           MPI          4 0.025391            0 
# Runtime: 29.021181 s (overhead: 0.000014 %) 5 records
