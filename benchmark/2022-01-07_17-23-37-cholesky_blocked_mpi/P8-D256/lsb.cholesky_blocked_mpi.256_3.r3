# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:31:26 2022
# Execution time and date (local): Fri Jan  7 17:31:26 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           256           256           MPI          0 5.708984            1 
            3           256           256           MPI          1 0.085938            5 
            3           256           256           MPI          2 0.027344            0 
            3           256           256           MPI          3 0.019531            0 
            3           256           256           MPI          4 0.015625            0 
# Runtime: 29.009080 s (overhead: 0.000021 %) 5 records
