# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:54:29 2022
# Execution time and date (local): Fri Jan  7 19:54:29 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096           256           MPI          0 4246533.833984            0 
            2          4096           256           MPI          1 4253547.419922            2 
            2          4096           256           MPI          2 4226172.722656            1 
            2          4096           256           MPI          3 4239569.505859            0 
            2          4096           256           MPI          4 4251076.318359            1 
# Runtime: 37.567973 s (overhead: 0.000011 %) 5 records
