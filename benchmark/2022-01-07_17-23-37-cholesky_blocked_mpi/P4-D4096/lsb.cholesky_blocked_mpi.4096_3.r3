# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:54:29 2022
# Execution time and date (local): Fri Jan  7 19:54:29 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096           256           MPI          0 4246810.949219            0 
            3          4096           256           MPI          1 4253852.578125            1 
            3          4096           256           MPI          2 4226445.753906            1 
            3          4096           256           MPI          3 4239843.312500            0 
            3          4096           256           MPI          4 4251360.294922            1 
# Runtime: 32.569504 s (overhead: 0.000009 %) 5 records
