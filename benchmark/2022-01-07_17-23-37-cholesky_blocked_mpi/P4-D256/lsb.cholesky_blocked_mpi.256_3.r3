# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:31:03 2022
# Execution time and date (local): Fri Jan  7 17:31:03 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           256           256           MPI          0 9.283203            1 
            3           256           256           MPI          1 0.175781            6 
            3           256           256           MPI          2 0.128906            0 
            3           256           256           MPI          3 0.105469            0 
            3           256           256           MPI          4 0.072266            0 
# Runtime: 2.022366 s (overhead: 0.000346 %) 5 records
