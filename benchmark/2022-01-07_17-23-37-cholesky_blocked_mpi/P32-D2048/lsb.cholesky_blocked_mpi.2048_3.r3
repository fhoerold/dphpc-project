# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:32:16 2022
# Execution time and date (local): Sat Jan  8 05:32:16 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048           256           MPI          0 676870.201172            0 
            3          2048           256           MPI          1 677564.201172            3 
            3          2048           256           MPI          2 676366.623047            4 
            3          2048           256           MPI          3 678439.919922            0 
            3          2048           256           MPI          4 677569.005859            5 
# Runtime: 11.042429 s (overhead: 0.000109 %) 5 records
