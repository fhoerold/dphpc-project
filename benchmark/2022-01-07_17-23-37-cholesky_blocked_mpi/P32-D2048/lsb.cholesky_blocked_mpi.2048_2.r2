# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:32:16 2022
# Execution time and date (local): Sat Jan  8 05:32:16 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048           256           MPI          0 676890.810547            0 
            2          2048           256           MPI          1 677582.328125            9 
            2          2048           256           MPI          2 676387.937500            1 
            2          2048           256           MPI          3 678456.312500            0 
            2          2048           256           MPI          4 677590.859375            6 
# Runtime: 11.047148 s (overhead: 0.000145 %) 5 records
