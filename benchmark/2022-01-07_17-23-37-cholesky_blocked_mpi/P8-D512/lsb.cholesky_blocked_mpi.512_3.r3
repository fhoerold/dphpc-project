# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:38:02 2022
# Execution time and date (local): Fri Jan  7 19:38:02 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512           256           MPI          0 5.890625            2 
            3           512           256           MPI          1 0.216797            4 
            3           512           256           MPI          2 0.058594            0 
            3           512           256           MPI          3 0.160156            0 
            3           512           256           MPI          4 0.195312            1 
# Runtime: 6.101033 s (overhead: 0.000115 %) 5 records
