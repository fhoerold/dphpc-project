# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:38:02 2022
# Execution time and date (local): Fri Jan  7 19:38:02 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512           256           MPI          0 5.136719            1 
            2           512           256           MPI          1 0.255859            6 
            2           512           256           MPI          2 0.193359            0 
            2           512           256           MPI          3 0.183594            0 
            2           512           256           MPI          4 0.236328            0 
# Runtime: 1.103232 s (overhead: 0.000634 %) 5 records
