# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:46:02 2022
# Execution time and date (local): Fri Jan  7 17:46:02 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024           256           MPI          0 95689.974609            0 
            2          1024           256           MPI          1 93707.335938            2 
            2          1024           256           MPI          2 94017.189453            2 
            2          1024           256           MPI          3 94792.585938            0 
            2          1024           256           MPI          4 93983.027344            2 
# Runtime: 4.242426 s (overhead: 0.000141 %) 5 records
