# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:46:02 2022
# Execution time and date (local): Fri Jan  7 17:46:02 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024           256           MPI          0 95794.597656            0 
            3          1024           256           MPI          1 93801.230469            2 
            3          1024           256           MPI          2 94115.410156            2 
            3          1024           256           MPI          3 94885.966797            0 
            3          1024           256           MPI          4 94060.314453            2 
# Runtime: 1.241151 s (overhead: 0.000483 %) 5 records
