# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 02:28:43 2022
# Execution time and date (local): Sat Jan  8 03:28:43 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024           256           MPI          0 87422.580078            0 
            2          1024           256           MPI          1 87327.031250            1 
            2          1024           256           MPI          2 87562.804688            0 
            2          1024           256           MPI          3 86994.185547            0 
            2          1024           256           MPI          4 86802.089844            0 
# Runtime: 16.132624 s (overhead: 0.000006 %) 5 records
