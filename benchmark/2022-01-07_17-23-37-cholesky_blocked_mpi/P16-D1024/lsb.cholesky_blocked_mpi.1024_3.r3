# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 02:28:43 2022
# Execution time and date (local): Sat Jan  8 03:28:43 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024           256           MPI          0 87483.695312            0 
            3          1024           256           MPI          1 87383.687500            1 
            3          1024           256           MPI          2 87620.423828            0 
            3          1024           256           MPI          3 87050.443359            0 
            3          1024           256           MPI          4 86856.701172            1 
# Runtime: 14.142749 s (overhead: 0.000014 %) 5 records
