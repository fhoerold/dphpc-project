# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:31:17 2022
# Execution time and date (local): Fri Jan  7 17:31:17 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512           256           MPI          0 5.185547            0 
            2           512           256           MPI          1 0.312500            1 
            2           512           256           MPI          2 0.302734            0 
            2           512           256           MPI          3 0.394531            0 
            2           512           256           MPI          4 0.490234            1 
# Runtime: 0.119482 s (overhead: 0.001674 %) 5 records
