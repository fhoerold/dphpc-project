# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:31:17 2022
# Execution time and date (local): Fri Jan  7 17:31:17 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512           256           MPI          0 7.921875            0 
            3           512           256           MPI          1 0.539062            5 
            3           512           256           MPI          2 0.238281            3 
            3           512           256           MPI          3 0.371094            0 
            3           512           256           MPI          4 0.392578            1 
# Runtime: 3.119835 s (overhead: 0.000288 %) 5 records
