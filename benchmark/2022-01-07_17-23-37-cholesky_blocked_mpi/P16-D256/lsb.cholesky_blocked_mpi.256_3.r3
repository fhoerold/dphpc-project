# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 02:24:28 2022
# Execution time and date (local): Sat Jan  8 03:24:28 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           256           256           MPI          0 4.767578            1 
            3           256           256           MPI          1 0.080078            9 
            3           256           256           MPI          2 0.062500            0 
            3           256           256           MPI          3 0.029297            0 
            3           256           256           MPI          4 0.021484            1 
# Runtime: 16.028868 s (overhead: 0.000069 %) 5 records
