# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:28:35 2022
# Execution time and date (local): Sat Jan  8 05:28:35 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192           256           MPI          0 16915109.294922            0 
            3          8192           256           MPI          1 16897503.943359            4 
            3          8192           256           MPI          2 16925093.740234            2 
            3          8192           256           MPI          3 16899366.847656            2 
            3          8192           256           MPI          4 16883827.968750            5 
# Runtime: 126.527062 s (overhead: 0.000010 %) 5 records
