# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:32:48 2022
# Execution time and date (local): Fri Jan  7 17:32:48 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048           256           MPI          0 615377.449219            0 
            2          2048           256           MPI          1 613462.736328            1 
            2          2048           256           MPI          2 612787.259766            1 
            2          2048           256           MPI          3 612719.529297            0 
            2          2048           256           MPI          4 615446.205078            1 
# Runtime: 20.525617 s (overhead: 0.000015 %) 5 records
