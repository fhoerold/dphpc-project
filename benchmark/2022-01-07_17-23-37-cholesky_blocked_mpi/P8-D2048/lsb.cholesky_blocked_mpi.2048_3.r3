# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:32:48 2022
# Execution time and date (local): Fri Jan  7 17:32:48 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048           256           MPI          0 615392.664062            0 
            3          2048           256           MPI          1 613478.654297            1 
            3          2048           256           MPI          2 612804.083984            1 
            3          2048           256           MPI          3 612736.927734            0 
            3          2048           256           MPI          4 615465.419922            2 
# Runtime: 20.525761 s (overhead: 0.000019 %) 5 records
