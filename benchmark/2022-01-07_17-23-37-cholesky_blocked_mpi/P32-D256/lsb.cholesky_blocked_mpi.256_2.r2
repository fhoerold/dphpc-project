# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:30:56 2022
# Execution time and date (local): Sat Jan  8 05:30:56 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           256           256           MPI          0 4.750000            1 
            2           256           256           MPI          1 0.050781            5 
            2           256           256           MPI          2 0.029297            0 
            2           256           256           MPI          3 0.015625            0 
            2           256           256           MPI          4 0.025391            0 
# Runtime: 10.012598 s (overhead: 0.000060 %) 5 records
