# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 00:31:30 2022
# Execution time and date (local): Sat Jan  8 01:31:30 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512           256           MPI          0 3.919922            2 
            3           512           256           MPI          1 0.203125            3 
            3           512           256           MPI          2 0.150391            0 
            3           512           256           MPI          3 0.132812            0 
            3           512           256           MPI          4 0.130859            0 
# Runtime: 9.124130 s (overhead: 0.000055 %) 5 records
