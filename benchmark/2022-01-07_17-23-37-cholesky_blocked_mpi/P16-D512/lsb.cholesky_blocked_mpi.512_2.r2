# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 00:31:30 2022
# Execution time and date (local): Sat Jan  8 01:31:30 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512           256           MPI          0 3.769531            0 
            2           512           256           MPI          1 0.207031            5 
            2           512           256           MPI          2 0.097656            0 
            2           512           256           MPI          3 0.248047            0 
            2           512           256           MPI          4 0.238281            0 
# Runtime: 9.123343 s (overhead: 0.000055 %) 5 records
