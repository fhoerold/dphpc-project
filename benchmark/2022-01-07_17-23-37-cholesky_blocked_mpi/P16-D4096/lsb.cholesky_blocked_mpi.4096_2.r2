# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 02:38:39 2022
# Execution time and date (local): Sat Jan  8 03:38:39 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096           256           MPI          0 3549976.501953            0 
            2          4096           256           MPI          1 3532033.744141            1 
            2          4096           256           MPI          2 3536154.140625            5 
            2          4096           256           MPI          3 3533376.195312            0 
            2          4096           256           MPI          4 3531835.851562            1 
# Runtime: 36.835514 s (overhead: 0.000019 %) 5 records
