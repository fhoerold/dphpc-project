# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 02:38:39 2022
# Execution time and date (local): Sat Jan  8 03:38:39 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096           256           MPI          0 3549937.875000            0 
            3          4096           256           MPI          1 3531996.267578            2 
            3          4096           256           MPI          2 3536094.085938            5 
            3          4096           256           MPI          3 3533338.970703            0 
            3          4096           256           MPI          4 3531801.025391            3 
# Runtime: 36.833332 s (overhead: 0.000027 %) 5 records
