# Sysname : Linux
# Nodename: eu-a6-009-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 21:23:46 2022
# Execution time and date (local): Sat Jan  8 22:23:46 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192           256           MPI          0 17745015.132812            0 
            3          8192           256           MPI          1 17667089.931641            5 
            3          8192           256           MPI          2 17702744.082031            2 
            3          8192           256           MPI          3 17728171.851562            3 
            3          8192           256           MPI          4 17743285.429688            8 
# Runtime: 141.291932 s (overhead: 0.000013 %) 5 records
