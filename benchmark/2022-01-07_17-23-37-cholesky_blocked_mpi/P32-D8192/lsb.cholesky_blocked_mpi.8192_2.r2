# Sysname : Linux
# Nodename: eu-a6-009-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 21:23:46 2022
# Execution time and date (local): Sat Jan  8 22:23:46 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192           256           MPI          0 17743283.873047            0 
            2          8192           256           MPI          1 17665317.250000            6 
            2          8192           256           MPI          2 17700976.968750            2 
            2          8192           256           MPI          3 17726402.572266            3 
            2          8192           256           MPI          4 17741516.677734            7 
# Runtime: 143.282972 s (overhead: 0.000013 %) 5 records
