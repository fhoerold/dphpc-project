# Sysname : Linux
# Nodename: eu-a6-004-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 20:34:30 2022
# Execution time and date (local): Fri Jan  7 21:34:30 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192           256           MPI          0 35573361.859375            0 
            3          8192           256           MPI          1 35389426.970703            6 
            3          8192           256           MPI          2 35553981.312500            4 
            3          8192           256           MPI          3 35774968.658203            4 
            3          8192           256           MPI          4 35507663.445312            5 
# Runtime: 256.041924 s (overhead: 0.000007 %) 5 records
