# Sysname : Linux
# Nodename: eu-a6-009-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 19:21:18 2022
# Execution time and date (local): Sat Jan  8 20:21:18 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024           256           MPI          0 94179.107422            0 
            3          1024           256           MPI          1 93740.503906            1 
            3          1024           256           MPI          2 93609.931641            1 
            3          1024           256           MPI          3 93406.966797            0 
            3          1024           256           MPI          4 93965.414062            1 
# Runtime: 10.232793 s (overhead: 0.000029 %) 5 records
