# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 20:38:15 2022
# Execution time and date (local): Fri Jan  7 21:38:15 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096           256           MPI          0 3534853.539062            0 
            2          4096           256           MPI          1 3504699.224609            2 
            2          4096           256           MPI          2 3510160.898438            5 
            2          4096           256           MPI          3 3495005.097656            0 
            2          4096           256           MPI          4 3497968.181641            4 
# Runtime: 30.575570 s (overhead: 0.000036 %) 5 records
