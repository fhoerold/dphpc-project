# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:32:23 2022
# Execution time and date (local): Fri Jan  7 17:32:23 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048           256           MPI          0 730805.023438            0 
            3          2048           256           MPI          1 726733.726562            2 
            3          2048           256           MPI          2 726962.677734            2 
            3          2048           256           MPI          3 729625.888672            0 
            3          2048           256           MPI          4 728657.083984            1 
# Runtime: 6.570399 s (overhead: 0.000076 %) 5 records
