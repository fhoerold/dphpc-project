# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:31:15 2022
# Execution time and date (local): Sat Jan  8 05:31:15 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512           256           MPI          0 3.792969            0 
            2           512           256           MPI          1 0.087891            0 
            2           512           256           MPI          2 0.052734            0 
            2           512           256           MPI          3 0.033203            0 
            2           512           256           MPI          4 0.050781            0 
# Runtime: 0.150926 s (overhead: 0.000000 %) 5 records
