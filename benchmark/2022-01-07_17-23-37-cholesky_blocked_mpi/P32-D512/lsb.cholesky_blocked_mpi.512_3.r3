# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:31:15 2022
# Execution time and date (local): Sat Jan  8 05:31:15 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512           256           MPI          0 3.224609            0 
            3           512           256           MPI          1 0.115234            0 
            3           512           256           MPI          2 0.093750            0 
            3           512           256           MPI          3 0.037109            0 
            3           512           256           MPI          4 0.042969            0 
# Runtime: 0.146872 s (overhead: 0.000000 %) 5 records
