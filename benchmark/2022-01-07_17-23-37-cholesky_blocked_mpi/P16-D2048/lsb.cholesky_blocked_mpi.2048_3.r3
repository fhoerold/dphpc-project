# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 00:32:37 2022
# Execution time and date (local): Sat Jan  8 01:32:37 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048           256           MPI          0 684442.109375            0 
            3          2048           256           MPI          1 683143.693359            1 
            3          2048           256           MPI          2 682785.675781            2 
            3          2048           256           MPI          3 681849.931641            0 
            3          2048           256           MPI          4 682114.371094            2 
# Runtime: 11.136345 s (overhead: 0.000045 %) 5 records
