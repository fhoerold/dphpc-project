# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 00:32:37 2022
# Execution time and date (local): Sat Jan  8 01:32:37 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048           256           MPI          0 684890.478516            0 
            2          2048           256           MPI          1 683592.269531            2 
            2          2048           256           MPI          2 683235.992188            1 
            2          2048           256           MPI          3 682293.345703            0 
            2          2048           256           MPI          4 682565.513672            1 
# Runtime: 10.146169 s (overhead: 0.000039 %) 5 records
