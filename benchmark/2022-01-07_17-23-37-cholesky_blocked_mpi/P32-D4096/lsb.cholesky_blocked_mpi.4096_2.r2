# Sysname : Linux
# Nodename: eu-a6-009-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 19:30:20 2022
# Execution time and date (local): Sat Jan  8 20:30:20 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096           256           MPI          0 3770441.419922            0 
            2          4096           256           MPI          1 3775683.335938            2 
            2          4096           256           MPI          2 3795322.763672            2 
            2          4096           256           MPI          3 3794782.648438            0 
            2          4096           256           MPI          4 3773872.841797            7 
# Runtime: 44.706674 s (overhead: 0.000025 %) 5 records
