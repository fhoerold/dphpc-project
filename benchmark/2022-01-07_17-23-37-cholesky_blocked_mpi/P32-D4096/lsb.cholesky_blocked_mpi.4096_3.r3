# Sysname : Linux
# Nodename: eu-a6-009-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 19:30:20 2022
# Execution time and date (local): Sat Jan  8 20:30:20 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096           256           MPI          0 3770547.642578            0 
            3          4096           256           MPI          1 3775792.203125            2 
            3          4096           256           MPI          2 3795433.847656            1 
            3          4096           256           MPI          3 3794900.171875            0 
            3          4096           256           MPI          4 3773975.013672            1 
# Runtime: 37.708041 s (overhead: 0.000011 %) 5 records
