# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:38:20 2022
# Execution time and date (local): Fri Jan  7 19:38:20 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024           256           MPI          0 80763.441406            0 
            3          1024           256           MPI          1 80513.835938            0 
            3          1024           256           MPI          2 80246.140625            0 
            3          1024           256           MPI          3 80678.019531            1 
            3          1024           256           MPI          4 80453.583984            5 
# Runtime: 7.058693 s (overhead: 0.000085 %) 5 records
