# Sysname : Linux
# Nodename: eu-a6-004-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 22:11:30 2022
# Execution time and date (local): Fri Jan  7 23:11:30 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192           256           MPI          0 19989499.666016            0 
            3          8192           256           MPI          1 19839965.951172            1 
            3          8192           256           MPI          2 20022374.347656            6 
            3          8192           256           MPI          3 20001624.679688            0 
            3          8192           256           MPI          4 19873319.320312            1 
# Runtime: 146.359457 s (overhead: 0.000005 %) 5 records
