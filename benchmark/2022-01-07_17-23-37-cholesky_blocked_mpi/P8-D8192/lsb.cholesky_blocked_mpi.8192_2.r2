# Sysname : Linux
# Nodename: eu-a6-004-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 22:11:30 2022
# Execution time and date (local): Fri Jan  7 23:11:30 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192           256           MPI          0 19989405.419922            0 
            2          8192           256           MPI          1 19839873.701172            1 
            2          8192           256           MPI          2 20022280.644531            6 
            2          8192           256           MPI          3 20001526.652344            0 
            2          8192           256           MPI          4 19873227.333984            4 
# Runtime: 149.359891 s (overhead: 0.000007 %) 5 records
