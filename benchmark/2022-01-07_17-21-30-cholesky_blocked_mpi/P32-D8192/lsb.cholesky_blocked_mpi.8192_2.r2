# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 13:41:53 2022
# Execution time and date (local): Sat Jan  8 14:41:53 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192            16           MPI          0 16367642.443359            0 
            2          8192            16           MPI          1 16064569.068359            6 
            2          8192            16           MPI          2 15933914.263672            1 
            2          8192            16           MPI          3 15865718.791016            0 
            2          8192            16           MPI          4 15726576.544922            2 
# Runtime: 124.669005 s (overhead: 0.000007 %) 5 records
