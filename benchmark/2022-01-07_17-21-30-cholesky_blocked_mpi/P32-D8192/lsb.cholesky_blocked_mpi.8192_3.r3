# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 13:41:53 2022
# Execution time and date (local): Sat Jan  8 14:41:53 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192            16           MPI          0 16367666.515625            0 
            3          8192            16           MPI          1 16064595.832031            5 
            3          8192            16           MPI          2 15933938.470703            1 
            3          8192            16           MPI          3 15865742.476562            0 
            3          8192            16           MPI          4 15726599.542969            5 
# Runtime: 124.668781 s (overhead: 0.000009 %) 5 records
