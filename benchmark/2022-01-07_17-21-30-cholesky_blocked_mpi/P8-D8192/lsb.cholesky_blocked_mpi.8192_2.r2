# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 20:37:53 2022
# Execution time and date (local): Fri Jan  7 21:37:53 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192            16           MPI          0 20835646.230469            3 
            2          8192            16           MPI          1 20821655.841797            6 
            2          8192            16           MPI          2 20948239.699219            9 
            2          8192            16           MPI          3 20858017.164062            0 
            2          8192            16           MPI          4 21079344.275391            7 
# Runtime: 159.150797 s (overhead: 0.000016 %) 5 records
