# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:45:42 2022
# Execution time and date (local): Fri Jan  7 22:45:42 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096            16           MPI          0 2277510.677734            0 
            2          4096            16           MPI          1 2185361.517578            1 
            2          4096            16           MPI          2 2230136.636719            1 
            2          4096            16           MPI          3 2234837.080078            0 
            2          4096            16           MPI          4 2257003.085938            1 
# Runtime: 23.217338 s (overhead: 0.000013 %) 5 records
