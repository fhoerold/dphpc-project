# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 11:24:49 2022
# Execution time and date (local): Sat Jan  8 12:24:49 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096            16           MPI          0 2285047.486328            0 
            3          4096            16           MPI          1 2185685.773438            1 
            3          4096            16           MPI          2 2200212.033203            1 
            3          4096            16           MPI          3 2243022.998047            0 
            3          4096            16           MPI          4 2246214.853516            2 
# Runtime: 16.830661 s (overhead: 0.000024 %) 5 records
