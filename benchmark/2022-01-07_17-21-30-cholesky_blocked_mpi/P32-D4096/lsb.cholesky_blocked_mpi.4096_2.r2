# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 11:24:49 2022
# Execution time and date (local): Sat Jan  8 12:24:49 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096            16           MPI          0 2284913.417969            0 
            2          4096            16           MPI          1 2185558.195312            1 
            2          4096            16           MPI          2 2200084.443359            1 
            2          4096            16           MPI          3 2242892.730469            0 
            2          4096            16           MPI          4 2246084.246094            1 
# Runtime: 20.832537 s (overhead: 0.000014 %) 5 records
