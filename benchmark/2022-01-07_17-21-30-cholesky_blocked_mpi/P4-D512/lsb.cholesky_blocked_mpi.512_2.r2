# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:40:58 2022
# Execution time and date (local): Fri Jan  7 17:40:58 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512            16           MPI          0 9356.103516            0 
            2           512            16           MPI          1 9162.533203            1 
            2           512            16           MPI          2 9160.689453            1 
            2           512            16           MPI          3 8953.742188            0 
            2           512            16           MPI          4 9082.833984            1 
# Runtime: 0.068980 s (overhead: 0.004349 %) 5 records
