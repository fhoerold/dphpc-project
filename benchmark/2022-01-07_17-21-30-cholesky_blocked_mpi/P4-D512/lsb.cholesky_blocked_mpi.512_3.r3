# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:40:58 2022
# Execution time and date (local): Fri Jan  7 17:40:58 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512            16           MPI          0 9394.783203            0 
            3           512            16           MPI          1 9194.515625            0 
            3           512            16           MPI          2 9201.226562            0 
            3           512            16           MPI          3 8955.650391            0 
            3           512            16           MPI          4 9117.912109            0 
# Runtime: 10.075510 s (overhead: 0.000000 %) 5 records
