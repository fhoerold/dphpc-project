# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 02:10:05 2022
# Execution time and date (local): Sat Jan  8 03:10:05 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192            16           MPI          0 15672499.757812            0 
            3          8192            16           MPI          1 15892376.982422            7 
            3          8192            16           MPI          2 17531571.085938            1 
            3          8192            16           MPI          3 16738143.482422            0 
            3          8192            16           MPI          4 16350729.843750            4 
# Runtime: 127.043716 s (overhead: 0.000009 %) 5 records
