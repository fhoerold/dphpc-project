# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 02:10:05 2022
# Execution time and date (local): Sat Jan  8 03:10:05 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192            16           MPI          0 15687936.023438            0 
            2          8192            16           MPI          1 15908029.742188            4 
            2          8192            16           MPI          2 17548837.453125            1 
            2          8192            16           MPI          3 16754628.052734            0 
            2          8192            16           MPI          4 16366835.693359            3 
# Runtime: 125.178901 s (overhead: 0.000006 %) 5 records
