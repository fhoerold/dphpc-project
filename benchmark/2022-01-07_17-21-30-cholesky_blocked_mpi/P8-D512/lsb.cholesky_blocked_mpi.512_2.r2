# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:29:00 2022
# Execution time and date (local): Fri Jan  7 17:29:00 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512            16           MPI          0 7374.269531            0 
            2           512            16           MPI          1 7402.544922            1 
            2           512            16           MPI          2 7322.437500            1 
            2           512            16           MPI          3 7337.054688            0 
            2           512            16           MPI          4 7267.025391            1 
# Runtime: 6.053310 s (overhead: 0.000050 %) 5 records
