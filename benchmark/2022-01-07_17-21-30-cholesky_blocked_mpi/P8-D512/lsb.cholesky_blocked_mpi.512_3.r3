# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:29:00 2022
# Execution time and date (local): Fri Jan  7 17:29:00 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512            16           MPI          0 7403.197266            0 
            3           512            16           MPI          1 7452.220703            0 
            3           512            16           MPI          2 7372.001953            1 
            3           512            16           MPI          3 7398.259766            0 
            3           512            16           MPI          4 7342.427734            1 
# Runtime: 6.053564 s (overhead: 0.000033 %) 5 records
