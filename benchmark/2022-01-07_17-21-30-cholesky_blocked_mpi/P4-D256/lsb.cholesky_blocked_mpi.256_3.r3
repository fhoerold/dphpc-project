# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:40:45 2022
# Execution time and date (local): Fri Jan  7 17:40:45 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           256            16           MPI          0 2109.697266            0 
            3           256            16           MPI          1 1622.839844            0 
            3           256            16           MPI          2 1662.017578            0 
            3           256            16           MPI          3 1896.427734            0 
            3           256            16           MPI          4 1753.621094            0 
# Runtime: 0.015658 s (overhead: 0.000000 %) 5 records
