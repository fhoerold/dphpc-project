# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:40:45 2022
# Execution time and date (local): Fri Jan  7 17:40:45 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           256            16           MPI          0 1696.968750            0 
            2           256            16           MPI          1 1586.750000            1 
            2           256            16           MPI          2 1629.003906            1 
            2           256            16           MPI          3 1855.925781            0 
            2           256            16           MPI          4 1720.361328            0 
# Runtime: 6.024360 s (overhead: 0.000033 %) 5 records
