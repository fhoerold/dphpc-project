# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:06:09 2022
# Execution time and date (local): Fri Jan  7 22:06:09 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512            16           MPI          0 8473.095703            0 
            3           512            16           MPI          1 8301.720703            1 
            3           512            16           MPI          2 8195.910156            1 
            3           512            16           MPI          3 8107.093750            0 
            3           512            16           MPI          4 8081.626953            1 
# Runtime: 7.067758 s (overhead: 0.000042 %) 5 records
