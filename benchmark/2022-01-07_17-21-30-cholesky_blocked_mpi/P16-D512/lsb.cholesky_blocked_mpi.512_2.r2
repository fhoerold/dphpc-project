# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:06:09 2022
# Execution time and date (local): Fri Jan  7 22:06:09 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512            16           MPI          0 8475.300781            0 
            2           512            16           MPI          1 8572.853516            1 
            2           512            16           MPI          2 8197.330078            0 
            2           512            16           MPI          3 8108.548828            0 
            2           512            16           MPI          4 8083.373047            0 
# Runtime: 7.059421 s (overhead: 0.000014 %) 5 records
