# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:55:01 2022
# Execution time and date (local): Fri Jan  7 19:55:01 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096            16           MPI          0 2638743.408203            0 
            3          4096            16           MPI          1 2727358.699219            1 
            3          4096            16           MPI          2 2732583.697266            1 
            3          4096            16           MPI          3 2641600.136719            0 
            3          4096            16           MPI          4 2679441.763672            1 
# Runtime: 25.727710 s (overhead: 0.000012 %) 5 records
