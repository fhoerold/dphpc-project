# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:06:30 2022
# Execution time and date (local): Fri Jan  7 22:06:30 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024            16           MPI          0 41377.031250            0 
            2          1024            16           MPI          1 41846.107422            1 
            2          1024            16           MPI          2 42204.244141            1 
            2          1024            16           MPI          3 41595.642578            0 
            2          1024            16           MPI          4 42371.474609            1 
# Runtime: 0.312682 s (overhead: 0.000959 %) 5 records
