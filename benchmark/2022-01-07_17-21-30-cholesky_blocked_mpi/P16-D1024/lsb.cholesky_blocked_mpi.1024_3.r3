# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:06:30 2022
# Execution time and date (local): Fri Jan  7 22:06:30 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024            16           MPI          0 41372.343750            0 
            3          1024            16           MPI          1 41841.121094            1 
            3          1024            16           MPI          2 42199.107422            2 
            3          1024            16           MPI          3 41590.755859            0 
            3          1024            16           MPI          4 42366.792969            1 
# Runtime: 2.307863 s (overhead: 0.000173 %) 5 records
