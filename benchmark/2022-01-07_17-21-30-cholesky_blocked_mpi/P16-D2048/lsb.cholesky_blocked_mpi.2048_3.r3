# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:07:31 2022
# Execution time and date (local): Fri Jan  7 22:07:31 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048            16           MPI          0 324372.017578            0 
            3          2048            16           MPI          1 322627.820312            1 
            3          2048            16           MPI          2 327995.476562            1 
            3          2048            16           MPI          3 327028.613281            0 
            3          2048            16           MPI          4 334960.326172            1 
# Runtime: 7.348275 s (overhead: 0.000041 %) 5 records
