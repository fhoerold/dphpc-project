# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:07:31 2022
# Execution time and date (local): Fri Jan  7 22:07:31 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048            16           MPI          0 324379.107422            0 
            2          2048            16           MPI          1 322635.152344            1 
            2          2048            16           MPI          2 328005.613281            5 
            2          2048            16           MPI          3 327035.792969            0 
            2          2048            16           MPI          4 334968.333984            4 
# Runtime: 5.337576 s (overhead: 0.000187 %) 5 records
