# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:34:36 2022
# Execution time and date (local): Fri Jan  7 19:34:36 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048            16           MPI          0 412224.328125            0 
            2          2048            16           MPI          1 410694.384766            1 
            2          2048            16           MPI          2 412553.261719            1 
            2          2048            16           MPI          3 404810.208984            0 
            2          2048            16           MPI          4 404080.818359            1 
# Runtime: 6.927150 s (overhead: 0.000043 %) 5 records
