# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:34:36 2022
# Execution time and date (local): Fri Jan  7 19:34:36 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048            16           MPI          0 412247.964844            0 
            3          2048            16           MPI          1 410744.035156            7 
            3          2048            16           MPI          2 412585.925781            5 
            3          2048            16           MPI          3 404852.695312            0 
            3          2048            16           MPI          4 404112.880859            1 
# Runtime: 8.927716 s (overhead: 0.000146 %) 5 records
