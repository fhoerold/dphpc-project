# Sysname : Linux
# Nodename: eu-a6-011-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:08:04 2022
# Execution time and date (local): Fri Jan  7 18:08:04 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096            16           MPI          0 5596095.154297            0 
            2          4096            16           MPI          1 5590864.652344            8 
            2          4096            16           MPI          2 5556591.371094            2 
            2          4096            16           MPI          3 5546786.849609            1 
            2          4096            16           MPI          4 5638702.753906            9 
# Runtime: 45.783381 s (overhead: 0.000044 %) 5 records
