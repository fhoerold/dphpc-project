# Sysname : Linux
# Nodename: eu-a6-011-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:08:04 2022
# Execution time and date (local): Fri Jan  7 18:08:04 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096            16           MPI          0 5596643.832031            0 
            3          4096            16           MPI          1 5591410.011719            9 
            3          4096            16           MPI          2 5557122.832031            5 
            3          4096            16           MPI          3 5547359.642578            0 
            3          4096            16           MPI          4 5639243.416016            6 
# Runtime: 41.786190 s (overhead: 0.000048 %) 5 records
