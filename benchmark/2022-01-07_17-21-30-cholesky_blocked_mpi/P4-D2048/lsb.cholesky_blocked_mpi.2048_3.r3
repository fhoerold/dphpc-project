# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:29:09 2022
# Execution time and date (local): Fri Jan  7 17:29:09 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048            16           MPI          0 639722.791016            0 
            3          2048            16           MPI          1 657080.972656            1 
            3          2048            16           MPI          2 647630.527344            4 
            3          2048            16           MPI          3 641266.134766            0 
            3          2048            16           MPI          4 641546.308594            2 
# Runtime: 6.648434 s (overhead: 0.000105 %) 5 records
