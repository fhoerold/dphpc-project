# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:28:47 2022
# Execution time and date (local): Fri Jan  7 17:28:47 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           256            16           MPI          0 1655.224609            0 
            2           256            16           MPI          1 1525.183594            0 
            2           256            16           MPI          2 1482.294922            0 
            2           256            16           MPI          3 1522.072266            0 
            2           256            16           MPI          4 1471.472656            0 
# Runtime: 6.001374 s (overhead: 0.000000 %) 5 records
