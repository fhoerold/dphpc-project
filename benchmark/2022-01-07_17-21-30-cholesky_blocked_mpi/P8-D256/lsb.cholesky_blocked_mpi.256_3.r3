# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:28:47 2022
# Execution time and date (local): Fri Jan  7 17:28:47 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           256            16           MPI          0 1691.027344            0 
            3           256            16           MPI          1 1571.607422            0 
            3           256            16           MPI          2 1665.583984            0 
            3           256            16           MPI          3 1553.720703            0 
            3           256            16           MPI          4 1516.105469            0 
# Runtime: 0.014965 s (overhead: 0.000000 %) 5 records
