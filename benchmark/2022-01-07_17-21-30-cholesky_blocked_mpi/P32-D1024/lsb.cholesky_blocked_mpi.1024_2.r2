# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 11:14:22 2022
# Execution time and date (local): Sat Jan  8 12:14:22 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024            16           MPI          0 47770.636719            0 
            2          1024            16           MPI          1 47258.292969            1 
            2          1024            16           MPI          2 47410.427734            1 
            2          1024            16           MPI          3 47526.892578            0 
            2          1024            16           MPI          4 47464.162109            1 
# Runtime: 5.366240 s (overhead: 0.000056 %) 5 records
