# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 11:14:22 2022
# Execution time and date (local): Sat Jan  8 12:14:22 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024            16           MPI          0 47761.074219            0 
            3          1024            16           MPI          1 47248.867188            1 
            3          1024            16           MPI          2 47401.207031            1 
            3          1024            16           MPI          3 47517.466797            0 
            3          1024            16           MPI          4 47454.998047            1 
# Runtime: 5.362912 s (overhead: 0.000056 %) 5 records
