# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 11:15:29 2022
# Execution time and date (local): Sat Jan  8 12:15:29 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048            16           MPI          0 283024.914062            0 
            2          2048            16           MPI          1 282567.150391            1 
            2          2048            16           MPI          2 282755.533203            1 
            2          2048            16           MPI          3 282853.167969            1 
            2          2048            16           MPI          4 285101.876953            1 
# Runtime: 9.018625 s (overhead: 0.000044 %) 5 records
