# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 11:15:29 2022
# Execution time and date (local): Sat Jan  8 12:15:29 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048            16           MPI          0 282973.570312            0 
            3          2048            16           MPI          1 282516.144531            1 
            3          2048            16           MPI          2 282703.724609            1 
            3          2048            16           MPI          3 282801.505859            0 
            3          2048            16           MPI          4 285049.689453            1 
# Runtime: 11.015701 s (overhead: 0.000027 %) 5 records
