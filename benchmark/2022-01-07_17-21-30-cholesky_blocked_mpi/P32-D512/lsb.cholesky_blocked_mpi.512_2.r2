# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:19:38 2022
# Execution time and date (local): Sat Jan  8 05:19:38 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512            16           MPI          0 9843.601562            0 
            2           512            16           MPI          1 9714.494141            0 
            2           512            16           MPI          2 9586.546875            0 
            2           512            16           MPI          3 9496.166016            0 
            2           512            16           MPI          4 9188.644531            0 
# Runtime: 6.081706 s (overhead: 0.000000 %) 5 records
