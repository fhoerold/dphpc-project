# Sysname : Linux
# Nodename: eu-a6-011-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 19:08:29 2022
# Execution time and date (local): Fri Jan  7 20:08:29 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192            16           MPI          0 37053660.841797            2 
            3          8192            16           MPI          1 37084400.074219            7 
            3          8192            16           MPI          2 37048536.724609            4 
            3          8192            16           MPI          3 37097097.353516            3 
            3          8192            16           MPI          4 37101535.474609            7 
# Runtime: 270.493598 s (overhead: 0.000009 %) 5 records
