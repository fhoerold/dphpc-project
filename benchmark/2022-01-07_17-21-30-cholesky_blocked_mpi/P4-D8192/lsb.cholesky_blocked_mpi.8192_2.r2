# Sysname : Linux
# Nodename: eu-a6-011-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 19:08:29 2022
# Execution time and date (local): Fri Jan  7 20:08:29 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192            16           MPI          0 37053045.978516            1 
            2          8192            16           MPI          1 37083777.890625           11 
            2          8192            16           MPI          2 37048014.566406            8 
            2          8192            16           MPI          3 37096392.843750            3 
            2          8192            16           MPI          4 37100912.058594           10 
# Runtime: 270.488863 s (overhead: 0.000012 %) 5 records
