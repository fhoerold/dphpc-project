# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:05:52 2022
# Execution time and date (local): Fri Jan  7 22:05:52 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           256            16           MPI          0 2367.687500            0 
            3           256            16           MPI          1 2308.199219            0 
            3           256            16           MPI          2 2554.042969            0 
            3           256            16           MPI          3 2231.732422            0 
            3           256            16           MPI          4 2220.263672            0 
# Runtime: 9.030269 s (overhead: 0.000000 %) 5 records
