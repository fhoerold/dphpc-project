# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:05:52 2022
# Execution time and date (local): Fri Jan  7 22:05:52 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           256            16           MPI          0 2671.421875            0 
            2           256            16           MPI          1 2478.511719            0 
            2           256            16           MPI          2 2340.562500            0 
            2           256            16           MPI          3 2232.767578            0 
            2           256            16           MPI          4 2221.873047            0 
# Runtime: 8.023823 s (overhead: 0.000000 %) 5 records
