# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:21:54 2022
# Execution time and date (local): Fri Jan  7 17:21:54 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024            16           MPI          0 82209.738281            0 
            2          1024            16           MPI          1 82381.851562            1 
            2          1024            16           MPI          2 80369.435547            1 
            2          1024            16           MPI          3 81962.306641            0 
            2          1024            16           MPI          4 82475.511719            1 
# Runtime: 6.601250 s (overhead: 0.000045 %) 5 records
