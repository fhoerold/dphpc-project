# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:21:54 2022
# Execution time and date (local): Fri Jan  7 17:21:54 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024            16           MPI          0 82205.423828            0 
            3          1024            16           MPI          1 82385.128906            1 
            3          1024            16           MPI          2 80376.640625            1 
            3          1024            16           MPI          3 81976.349609            0 
            3          1024            16           MPI          4 82479.677734            1 
# Runtime: 0.595907 s (overhead: 0.000503 %) 5 records
