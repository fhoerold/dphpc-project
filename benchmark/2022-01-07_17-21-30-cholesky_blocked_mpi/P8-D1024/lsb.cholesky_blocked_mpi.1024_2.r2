# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:29:19 2022
# Execution time and date (local): Fri Jan  7 17:29:19 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024            16           MPI          0 49675.765625            0 
            2          1024            16           MPI          1 50224.279297            1 
            2          1024            16           MPI          2 50507.093750            1 
            2          1024            16           MPI          3 51715.748047            0 
            2          1024            16           MPI          4 52421.888672            5 
# Runtime: 4.353080 s (overhead: 0.000161 %) 5 records
