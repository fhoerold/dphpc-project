# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:42:00 2022
# Execution time and date (local): Mon Jan  3 10:42:00 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 20488878.789062            0 
            2          2048          2048   gramschmidt           MPI          1 20456879.558594            5 
            2          2048          2048   gramschmidt           MPI          2 20448954.257812            2 
            2          2048          2048   gramschmidt           MPI          3 20393427.111328            0 
            2          2048          2048   gramschmidt           MPI          4 20439967.445312            3 
            2          2048          2048   gramschmidt           MPI          5 20364614.935547            0 
            2          2048          2048   gramschmidt           MPI          6 20343676.982422            0 
            2          2048          2048   gramschmidt           MPI          7 20336930.804688            0 
            2          2048          2048   gramschmidt           MPI          8 20274097.345703            5 
            2          2048          2048   gramschmidt           MPI          9 20302611.318359            0 
# Runtime: 268.514376 s (overhead: 0.000006 %) 10 records
