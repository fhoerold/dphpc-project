# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:42:00 2022
# Execution time and date (local): Mon Jan  3 10:42:00 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 20489739.240234            0 
            3          2048          2048   gramschmidt           MPI          1 20476417.367188            5 
            3          2048          2048   gramschmidt           MPI          2 20449750.755859            1 
            3          2048          2048   gramschmidt           MPI          3 20395149.496094            0 
            3          2048          2048   gramschmidt           MPI          4 20449206.490234            3 
            3          2048          2048   gramschmidt           MPI          5 20365446.160156            0 
            3          2048          2048   gramschmidt           MPI          6 20345382.417969            0 
            3          2048          2048   gramschmidt           MPI          7 20337769.748047            0 
            3          2048          2048   gramschmidt           MPI          8 20274940.279297            4 
            3          2048          2048   gramschmidt           MPI          9 20304353.511719            0 
# Runtime: 266.505872 s (overhead: 0.000005 %) 10 records
