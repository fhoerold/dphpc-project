# Sysname : Linux
# Nodename: eu-a6-004-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:44:17 2022
# Execution time and date (local): Mon Jan  3 10:44:17 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 75667.611328            0 
            2           512           512   gramschmidt           MPI          1 101700.513672            4 
            2           512           512   gramschmidt           MPI          2 70331.433594            1 
            2           512           512   gramschmidt           MPI          3 51844.226562            0 
            2           512           512   gramschmidt           MPI          4 51148.322266            1 
            2           512           512   gramschmidt           MPI          5 40630.208984            0 
            2           512           512   gramschmidt           MPI          6 40642.660156            0 
            2           512           512   gramschmidt           MPI          7 40244.888672            0 
            2           512           512   gramschmidt           MPI          8 40260.041016            1 
            2           512           512   gramschmidt           MPI          9 40575.218750            0 
# Runtime: 5.796393 s (overhead: 0.000121 %) 10 records
