# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:41:29 2022
# Execution time and date (local): Mon Jan  3 10:41:29 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 3198.630859            0 
            3           128           128   gramschmidt           MPI          1 3254.742188            3 
            3           128           128   gramschmidt           MPI          2 3143.552734            0 
            3           128           128   gramschmidt           MPI          3 3186.714844            0 
            3           128           128   gramschmidt           MPI          4 3172.369141            0 
            3           128           128   gramschmidt           MPI          5 3195.125000            0 
            3           128           128   gramschmidt           MPI          6 3150.931641            0 
            3           128           128   gramschmidt           MPI          7 3147.455078            0 
            3           128           128   gramschmidt           MPI          8 3128.269531            0 
            3           128           128   gramschmidt           MPI          9 3110.556641            0 
# Runtime: 9.046366 s (overhead: 0.000033 %) 10 records
