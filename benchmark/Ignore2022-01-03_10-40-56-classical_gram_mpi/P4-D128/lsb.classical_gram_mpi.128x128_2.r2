# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:41:29 2022
# Execution time and date (local): Mon Jan  3 10:41:29 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 3221.402344            2 
            2           128           128   gramschmidt           MPI          1 3283.355469            3 
            2           128           128   gramschmidt           MPI          2 3199.357422            0 
            2           128           128   gramschmidt           MPI          3 3225.509766            0 
            2           128           128   gramschmidt           MPI          4 3230.712891            0 
            2           128           128   gramschmidt           MPI          5 3248.890625            0 
            2           128           128   gramschmidt           MPI          6 3181.197266            0 
            2           128           128   gramschmidt           MPI          7 3200.476562            0 
            2           128           128   gramschmidt           MPI          8 3159.931641            0 
            2           128           128   gramschmidt           MPI          9 3166.259766            0 
# Runtime: 9.030124 s (overhead: 0.000055 %) 10 records
