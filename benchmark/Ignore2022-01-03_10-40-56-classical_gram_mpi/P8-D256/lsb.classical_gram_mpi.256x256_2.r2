# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:42:12 2022
# Execution time and date (local): Mon Jan  3 10:42:12 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 8855.449219            0 
            2           256           256   gramschmidt           MPI          1 8822.720703            4 
            2           256           256   gramschmidt           MPI          2 8910.054688            1 
            2           256           256   gramschmidt           MPI          3 8892.490234            0 
            2           256           256   gramschmidt           MPI          4 8780.230469            1 
            2           256           256   gramschmidt           MPI          5 8633.943359            0 
            2           256           256   gramschmidt           MPI          6 8708.468750            0 
            2           256           256   gramschmidt           MPI          7 8694.015625            0 
            2           256           256   gramschmidt           MPI          8 8675.035156            0 
            2           256           256   gramschmidt           MPI          9 8694.121094            0 
# Runtime: 3.119543 s (overhead: 0.000192 %) 10 records
