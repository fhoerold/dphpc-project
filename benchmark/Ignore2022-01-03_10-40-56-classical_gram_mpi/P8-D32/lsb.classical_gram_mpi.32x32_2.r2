# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:42:00 2022
# Execution time and date (local): Mon Jan  3 10:42:00 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 324.166016            0 
            2            32            32   gramschmidt           MPI          1 294.585938            3 
            2            32            32   gramschmidt           MPI          2 304.396484            0 
            2            32            32   gramschmidt           MPI          3 297.130859            0 
            2            32            32   gramschmidt           MPI          4 297.199219            0 
            2            32            32   gramschmidt           MPI          5 308.312500            0 
            2            32            32   gramschmidt           MPI          6 321.546875            0 
            2            32            32   gramschmidt           MPI          7 292.376953            0 
            2            32            32   gramschmidt           MPI          8 293.441406            0 
            2            32            32   gramschmidt           MPI          9 292.839844            0 
# Runtime: 3.003945 s (overhead: 0.000100 %) 10 records
