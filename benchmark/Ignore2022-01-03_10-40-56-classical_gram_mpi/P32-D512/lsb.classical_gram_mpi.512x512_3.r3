# Sysname : Linux
# Nodename: eu-a6-005-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:18:18 2022
# Execution time and date (local): Mon Jan  3 11:18:18 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 90130.880859            0 
            3           512           512   gramschmidt           MPI          1 87791.787109            4 
            3           512           512   gramschmidt           MPI          2 65517.164062            2 
            3           512           512   gramschmidt           MPI          3 67058.562500            0 
            3           512           512   gramschmidt           MPI          4 64349.636719            2 
            3           512           512   gramschmidt           MPI          5 48504.939453            0 
            3           512           512   gramschmidt           MPI          6 49779.062500            0 
            3           512           512   gramschmidt           MPI          7 48443.476562            0 
            3           512           512   gramschmidt           MPI          8 48430.287109            5 
            3           512           512   gramschmidt           MPI          9 50957.707031            0 
# Runtime: 5.144593 s (overhead: 0.000253 %) 10 records
