# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:59:53 2022
# Execution time and date (local): Mon Jan  3 10:59:53 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 76792.015625            0 
            3           512           512   gramschmidt           MPI          1 95477.197266            4 
            3           512           512   gramschmidt           MPI          2 56910.462891            1 
            3           512           512   gramschmidt           MPI          3 80058.685547            0 
            3           512           512   gramschmidt           MPI          4 97491.908203            2 
            3           512           512   gramschmidt           MPI          5 58561.791016            0 
            3           512           512   gramschmidt           MPI          6 53637.248047            0 
            3           512           512   gramschmidt           MPI          7 53626.787109            0 
            3           512           512   gramschmidt           MPI          8 53124.185547            1 
            3           512           512   gramschmidt           MPI          9 56482.074219            0 
# Runtime: 11.938773 s (overhead: 0.000067 %) 10 records
