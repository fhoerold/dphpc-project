# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:59:53 2022
# Execution time and date (local): Mon Jan  3 10:59:53 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 76776.582031            0 
            2           512           512   gramschmidt           MPI          1 95456.140625            4 
            2           512           512   gramschmidt           MPI          2 56902.353516            1 
            2           512           512   gramschmidt           MPI          3 80047.158203            0 
            2           512           512   gramschmidt           MPI          4 97496.246094            1 
            2           512           512   gramschmidt           MPI          5 58551.400391            0 
            2           512           512   gramschmidt           MPI          6 53640.445312            0 
            2           512           512   gramschmidt           MPI          7 53617.785156            0 
            2           512           512   gramschmidt           MPI          8 53124.666016            2 
            2           512           512   gramschmidt           MPI          9 56486.062500            0 
# Runtime: 9.927956 s (overhead: 0.000081 %) 10 records
