# Sysname : Linux
# Nodename: eu-a6-011-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:41:30 2022
# Execution time and date (local): Mon Jan  3 10:41:30 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 1963586.095703            0 
            2          1024          1024   gramschmidt           MPI          1 1905953.873047            4 
            2          1024          1024   gramschmidt           MPI          2 1953714.085938            1 
            2          1024          1024   gramschmidt           MPI          3 1963215.988281            0 
            2          1024          1024   gramschmidt           MPI          4 1979783.404297            4 
            2          1024          1024   gramschmidt           MPI          5 1938625.130859            0 
            2          1024          1024   gramschmidt           MPI          6 1956881.484375            0 
            2          1024          1024   gramschmidt           MPI          7 1977050.511719            0 
            2          1024          1024   gramschmidt           MPI          8 1863621.869141            3 
            2          1024          1024   gramschmidt           MPI          9 1889151.718750            0 
# Runtime: 27.234665 s (overhead: 0.000044 %) 10 records
