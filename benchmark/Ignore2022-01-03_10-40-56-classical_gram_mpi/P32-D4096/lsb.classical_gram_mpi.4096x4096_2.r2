# Sysname : Linux
# Nodename: eu-a6-001-06
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:19:31 2022
# Execution time and date (local): Mon Jan  3 11:19:31 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 28290730.666016            3 
            2          4096          4096   gramschmidt           MPI          1 28041204.789062            5 
            2          4096          4096   gramschmidt           MPI          2 27961318.355469            2 
            2          4096          4096   gramschmidt           MPI          3 28171384.681641            0 
            2          4096          4096   gramschmidt           MPI          4 28288289.156250            4 
            2          4096          4096   gramschmidt           MPI          5 28275252.345703            0 
            2          4096          4096   gramschmidt           MPI          6 28524598.226562            2 
            2          4096          4096   gramschmidt           MPI          7 28329636.626953            0 
            2          4096          4096   gramschmidt           MPI          8 28550312.267578            6 
            2          4096          4096   gramschmidt           MPI          9 28274562.212891            0 
# Runtime: 381.089550 s (overhead: 0.000006 %) 10 records
