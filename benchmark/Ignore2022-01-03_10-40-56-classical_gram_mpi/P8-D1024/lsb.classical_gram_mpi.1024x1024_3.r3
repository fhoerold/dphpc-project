# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:42:12 2022
# Execution time and date (local): Mon Jan  3 10:42:12 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 659102.005859            0 
            3          1024          1024   gramschmidt           MPI          1 695175.083984            3 
            3          1024          1024   gramschmidt           MPI          2 674143.697266            2 
            3          1024          1024   gramschmidt           MPI          3 665550.980469            0 
            3          1024          1024   gramschmidt           MPI          4 682324.208984            1 
            3          1024          1024   gramschmidt           MPI          5 657705.828125            0 
            3          1024          1024   gramschmidt           MPI          6 673889.425781            0 
            3          1024          1024   gramschmidt           MPI          7 663152.435547            0 
            3          1024          1024   gramschmidt           MPI          8 665684.318359            5 
            3          1024          1024   gramschmidt           MPI          9 679031.564453            0 
# Runtime: 8.782846 s (overhead: 0.000125 %) 10 records
