# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:42:00 2022
# Execution time and date (local): Mon Jan  3 10:42:00 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 1982.662109            0 
            2           128           128   gramschmidt           MPI          1 1962.503906            4 
            2           128           128   gramschmidt           MPI          2 1889.761719            0 
            2           128           128   gramschmidt           MPI          3 1889.494141            0 
            2           128           128   gramschmidt           MPI          4 1925.837891            0 
            2           128           128   gramschmidt           MPI          5 1849.884766            0 
            2           128           128   gramschmidt           MPI          6 1862.298828            0 
            2           128           128   gramschmidt           MPI          7 1840.218750            0 
            2           128           128   gramschmidt           MPI          8 1859.851562            0 
            2           128           128   gramschmidt           MPI          9 1817.003906            0 
# Runtime: 4.021071 s (overhead: 0.000099 %) 10 records
