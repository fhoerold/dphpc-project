# Sysname : Linux
# Nodename: eu-a6-001-06
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:16:23 2022
# Execution time and date (local): Mon Jan  3 11:16:23 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 4352.195312            0 
            2           128           128   gramschmidt           MPI          1 4333.166016            2 
            2           128           128   gramschmidt           MPI          2 4358.378906            0 
            2           128           128   gramschmidt           MPI          3 4285.976562            0 
            2           128           128   gramschmidt           MPI          4 4353.181641            0 
            2           128           128   gramschmidt           MPI          5 4321.974609            0 
            2           128           128   gramschmidt           MPI          6 4232.375000            0 
            2           128           128   gramschmidt           MPI          7 4284.216797            0 
            2           128           128   gramschmidt           MPI          8 4257.435547            0 
            2           128           128   gramschmidt           MPI          9 4323.613281            0 
# Runtime: 7.083593 s (overhead: 0.000028 %) 10 records
