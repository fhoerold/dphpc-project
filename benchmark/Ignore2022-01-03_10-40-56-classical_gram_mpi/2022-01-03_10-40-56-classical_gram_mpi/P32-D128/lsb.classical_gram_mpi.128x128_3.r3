# Sysname : Linux
# Nodename: eu-a6-001-06
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:16:23 2022
# Execution time and date (local): Mon Jan  3 11:16:23 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 4353.753906            0 
            3           128           128   gramschmidt           MPI          1 4334.328125            2 
            3           128           128   gramschmidt           MPI          2 4361.095703            0 
            3           128           128   gramschmidt           MPI          3 4288.863281            0 
            3           128           128   gramschmidt           MPI          4 4354.203125            0 
            3           128           128   gramschmidt           MPI          5 4324.560547            0 
            3           128           128   gramschmidt           MPI          6 4232.521484            0 
            3           128           128   gramschmidt           MPI          7 4283.919922            0 
            3           128           128   gramschmidt           MPI          8 4258.216797            0 
            3           128           128   gramschmidt           MPI          9 4326.519531            0 
# Runtime: 4.065371 s (overhead: 0.000049 %) 10 records
