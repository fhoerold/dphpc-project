# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:42:00 2022
# Execution time and date (local): Mon Jan  3 10:42:00 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 783.800781            0 
            2            64            64   gramschmidt           MPI          1 821.162109            2 
            2            64            64   gramschmidt           MPI          2 800.490234            0 
            2            64            64   gramschmidt           MPI          3 763.044922            0 
            2            64            64   gramschmidt           MPI          4 826.117188            0 
            2            64            64   gramschmidt           MPI          5 899.666016            0 
            2            64            64   gramschmidt           MPI          6 792.705078            0 
            2            64            64   gramschmidt           MPI          7 769.154297            0 
            2            64            64   gramschmidt           MPI          8 779.611328            0 
            2            64            64   gramschmidt           MPI          9 772.132812            0 
# Runtime: 5.002775 s (overhead: 0.000040 %) 10 records
