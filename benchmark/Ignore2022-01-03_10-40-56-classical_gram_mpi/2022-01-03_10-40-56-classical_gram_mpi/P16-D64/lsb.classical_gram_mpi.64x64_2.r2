# Sysname : Linux
# Nodename: eu-a6-004-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:43:12 2022
# Execution time and date (local): Mon Jan  3 10:43:12 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 1061.609375            0 
            2            64            64   gramschmidt           MPI          1 977.427734            3 
            2            64            64   gramschmidt           MPI          2 1035.210938            0 
            2            64            64   gramschmidt           MPI          3 916.154297            0 
            2            64            64   gramschmidt           MPI          4 974.683594            0 
            2            64            64   gramschmidt           MPI          5 917.894531            0 
            2            64            64   gramschmidt           MPI          6 889.912109            0 
            2            64            64   gramschmidt           MPI          7 904.269531            0 
            2            64            64   gramschmidt           MPI          8 905.568359            0 
            2            64            64   gramschmidt           MPI          9 913.105469            0 
# Runtime: 8.009013 s (overhead: 0.000037 %) 10 records
