# Sysname : Linux
# Nodename: eu-a6-001-06
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:16:03 2022
# Execution time and date (local): Mon Jan  3 11:16:03 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 1058.248047            0 
            2            32            32   gramschmidt           MPI          1 1019.486328            2 
            2            32            32   gramschmidt           MPI          2 970.869141            0 
            2            32            32   gramschmidt           MPI          3 969.718750            0 
            2            32            32   gramschmidt           MPI          4 956.957031            0 
            2            32            32   gramschmidt           MPI          5 954.234375            0 
            2            32            32   gramschmidt           MPI          6 968.541016            0 
            2            32            32   gramschmidt           MPI          7 932.564453            0 
            2            32            32   gramschmidt           MPI          8 935.742188            0 
            2            32            32   gramschmidt           MPI          9 921.785156            0 
# Runtime: 7.024278 s (overhead: 0.000028 %) 10 records
