# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:34:13 2022
# Execution time and date (local): Mon Jan  3 11:34:13 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 30861105.060547            0 
            3          4096          4096   gramschmidt           MPI          1 30509094.744141            6 
            3          4096          4096   gramschmidt           MPI          2 30883969.765625            2 
            3          4096          4096   gramschmidt           MPI          3 30274828.720703            0 
            3          4096          4096   gramschmidt           MPI          4 30255072.896484            6 
            3          4096          4096   gramschmidt           MPI          5 30353585.636719            2 
            3          4096          4096   gramschmidt           MPI          6 30737636.826172            0 
            3          4096          4096   gramschmidt           MPI          7 30483425.857422            2 
            3          4096          4096   gramschmidt           MPI          8 30348831.617188            6 
            3          4096          4096   gramschmidt           MPI          9 30203940.486328            2 
# Runtime: 403.911179 s (overhead: 0.000006 %) 10 records
