# Sysname : Linux
# Nodename: eu-a6-001-06
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:19:31 2022
# Execution time and date (local): Mon Jan  3 11:19:31 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 28294559.273438            1 
            3          4096          4096   gramschmidt           MPI          1 28044492.476562            5 
            3          4096          4096   gramschmidt           MPI          2 27965084.212891            1 
            3          4096          4096   gramschmidt           MPI          3 28174714.390625            0 
            3          4096          4096   gramschmidt           MPI          4 28292101.781250            5 
            3          4096          4096   gramschmidt           MPI          5 28278605.292969            0 
            3          4096          4096   gramschmidt           MPI          6 28528368.175781            2 
            3          4096          4096   gramschmidt           MPI          7 28333008.263672            0 
            3          4096          4096   gramschmidt           MPI          8 28553625.750000            9 
            3          4096          4096   gramschmidt           MPI          9 28278219.619141            0 
# Runtime: 381.107836 s (overhead: 0.000006 %) 10 records
