# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:42:12 2022
# Execution time and date (local): Mon Jan  3 10:42:12 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 659861.246094            0 
            2          1024          1024   gramschmidt           MPI          1 695771.748047            5 
            2          1024          1024   gramschmidt           MPI          2 674894.890625            2 
            2          1024          1024   gramschmidt           MPI          3 666330.671875            0 
            2          1024          1024   gramschmidt           MPI          4 683110.757812            3 
            2          1024          1024   gramschmidt           MPI          5 658450.617188            0 
            2          1024          1024   gramschmidt           MPI          6 674530.269531            0 
            2          1024          1024   gramschmidt           MPI          7 663891.101562            0 
            2          1024          1024   gramschmidt           MPI          8 666440.343750            1 
            2          1024          1024   gramschmidt           MPI          9 679784.462891            0 
# Runtime: 8.782443 s (overhead: 0.000125 %) 10 records
