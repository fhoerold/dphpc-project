# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:01:54 2022
# Execution time and date (local): Mon Jan  3 11:01:54 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 8919.808594            0 
            3           256           256   gramschmidt           MPI          1 8817.775391            3 
            3           256           256   gramschmidt           MPI          2 8727.937500            1 
            3           256           256   gramschmidt           MPI          3 8672.455078            0 
            3           256           256   gramschmidt           MPI          4 8690.925781            0 
            3           256           256   gramschmidt           MPI          5 8655.472656            0 
            3           256           256   gramschmidt           MPI          6 8563.486328            0 
            3           256           256   gramschmidt           MPI          7 8630.904297            0 
            3           256           256   gramschmidt           MPI          8 10130.123047            1 
            3           256           256   gramschmidt           MPI          9 8656.509766            0 
# Runtime: 10.136493 s (overhead: 0.000049 %) 10 records
