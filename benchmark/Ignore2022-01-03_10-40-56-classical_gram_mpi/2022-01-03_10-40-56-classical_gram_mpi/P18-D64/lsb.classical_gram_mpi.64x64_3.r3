# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:01:01 2022
# Execution time and date (local): Mon Jan  3 11:01:01 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 1250.039062            0 
            3            64            64   gramschmidt           MPI          1 1251.386719            2 
            3            64            64   gramschmidt           MPI          2 1206.750000            0 
            3            64            64   gramschmidt           MPI          3 1216.935547            0 
            3            64            64   gramschmidt           MPI          4 1193.927734            0 
            3            64            64   gramschmidt           MPI          5 1198.291016            0 
            3            64            64   gramschmidt           MPI          6 1230.886719            0 
            3            64            64   gramschmidt           MPI          7 1182.958984            0 
            3            64            64   gramschmidt           MPI          8 1209.845703            0 
            3            64            64   gramschmidt           MPI          9 1196.503906            0 
# Runtime: 9.014335 s (overhead: 0.000022 %) 10 records
