# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:01:01 2022
# Execution time and date (local): Mon Jan  3 11:01:01 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 1250.876953            0 
            2            64            64   gramschmidt           MPI          1 1254.113281            2 
            2            64            64   gramschmidt           MPI          2 1210.867188            0 
            2            64            64   gramschmidt           MPI          3 1218.212891            0 
            2            64            64   gramschmidt           MPI          4 1195.070312            0 
            2            64            64   gramschmidt           MPI          5 1200.615234            0 
            2            64            64   gramschmidt           MPI          6 1232.781250            0 
            2            64            64   gramschmidt           MPI          7 1183.687500            0 
            2            64            64   gramschmidt           MPI          8 1209.546875            0 
            2            64            64   gramschmidt           MPI          9 1197.333984            0 
# Runtime: 10.012795 s (overhead: 0.000020 %) 10 records
