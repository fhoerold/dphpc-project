# Sysname : Linux
# Nodename: eu-a6-004-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:43:59 2022
# Execution time and date (local): Mon Jan  3 10:43:59 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 2450.869141            0 
            3           128           128   gramschmidt           MPI          1 2282.023438            2 
            3           128           128   gramschmidt           MPI          2 2285.480469            0 
            3           128           128   gramschmidt           MPI          3 2194.335938            0 
            3           128           128   gramschmidt           MPI          4 2270.552734            0 
            3           128           128   gramschmidt           MPI          5 2251.134766            0 
            3           128           128   gramschmidt           MPI          6 2243.835938            0 
            3           128           128   gramschmidt           MPI          7 2222.634766            0 
            3           128           128   gramschmidt           MPI          8 2326.755859            0 
            3           128           128   gramschmidt           MPI          9 2212.609375            0 
# Runtime: 8.044286 s (overhead: 0.000025 %) 10 records
