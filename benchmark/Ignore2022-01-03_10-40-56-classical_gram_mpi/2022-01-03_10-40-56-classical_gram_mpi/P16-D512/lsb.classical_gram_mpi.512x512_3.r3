# Sysname : Linux
# Nodename: eu-a6-004-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:44:17 2022
# Execution time and date (local): Mon Jan  3 10:44:17 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 75914.978516            0 
            3           512           512   gramschmidt           MPI          1 101960.468750            4 
            3           512           512   gramschmidt           MPI          2 70586.828125            2 
            3           512           512   gramschmidt           MPI          3 52072.132812            0 
            3           512           512   gramschmidt           MPI          4 51170.132812            2 
            3           512           512   gramschmidt           MPI          5 40640.267578            0 
            3           512           512   gramschmidt           MPI          6 40649.281250            0 
            3           512           512   gramschmidt           MPI          7 40268.273438            0 
            3           512           512   gramschmidt           MPI          8 40254.537109            2 
            3           512           512   gramschmidt           MPI          9 40574.521484            0 
# Runtime: 7.804014 s (overhead: 0.000128 %) 10 records
