# Sysname : Linux
# Nodename: eu-a6-001-06
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:17:05 2022
# Execution time and date (local): Mon Jan  3 11:17:05 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 1670647.257812            0 
            2          2048          2048   gramschmidt           MPI          1 1722820.878906            6 
            2          2048          2048   gramschmidt           MPI          2 1648645.429688            2 
            2          2048          2048   gramschmidt           MPI          3 1633775.634766            1 
            2          2048          2048   gramschmidt           MPI          4 1726463.126953            8 
            2          2048          2048   gramschmidt           MPI          5 1650177.029297            0 
            2          2048          2048   gramschmidt           MPI          6 1673845.875000            0 
            2          2048          2048   gramschmidt           MPI          7 1630060.507812            0 
            2          2048          2048   gramschmidt           MPI          8 1622435.109375            2 
            2          2048          2048   gramschmidt           MPI          9 1687949.125000            0 
# Runtime: 30.091587 s (overhead: 0.000063 %) 10 records
