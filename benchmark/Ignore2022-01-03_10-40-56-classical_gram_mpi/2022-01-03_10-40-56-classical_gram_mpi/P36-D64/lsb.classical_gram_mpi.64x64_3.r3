# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:31:18 2022
# Execution time and date (local): Mon Jan  3 11:31:18 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 2259.505859            0 
            3            64            64   gramschmidt           MPI          1 2019.470703            2 
            3            64            64   gramschmidt           MPI          2 2016.746094            0 
            3            64            64   gramschmidt           MPI          3 1980.339844            0 
            3            64            64   gramschmidt           MPI          4 2024.623047            0 
            3            64            64   gramschmidt           MPI          5 1963.875000            0 
            3            64            64   gramschmidt           MPI          6 1965.125000            0 
            3            64            64   gramschmidt           MPI          7 1944.662109            0 
            3            64            64   gramschmidt           MPI          8 1941.873047            0 
            3            64            64   gramschmidt           MPI          9 1962.068359            0 
# Runtime: 5.034958 s (overhead: 0.000040 %) 10 records
