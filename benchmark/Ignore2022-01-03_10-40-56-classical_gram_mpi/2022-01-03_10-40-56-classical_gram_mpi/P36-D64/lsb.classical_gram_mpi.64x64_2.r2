# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:31:18 2022
# Execution time and date (local): Mon Jan  3 11:31:18 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 2050.966797            0 
            2            64            64   gramschmidt           MPI          1 2092.142578            3 
            2            64            64   gramschmidt           MPI          2 2065.166016            0 
            2            64            64   gramschmidt           MPI          3 2011.115234            0 
            2            64            64   gramschmidt           MPI          4 2057.560547            0 
            2            64            64   gramschmidt           MPI          5 1996.640625            0 
            2            64            64   gramschmidt           MPI          6 1989.238281            0 
            2            64            64   gramschmidt           MPI          7 1974.052734            0 
            2            64            64   gramschmidt           MPI          8 1968.921875            0 
            2            64            64   gramschmidt           MPI          9 1987.222656            0 
# Runtime: 13.050322 s (overhead: 0.000023 %) 10 records
