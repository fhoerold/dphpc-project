# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:32:00 2022
# Execution time and date (local): Mon Jan  3 11:32:00 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 26944.900391            0 
            2           256           256   gramschmidt           MPI          1 26681.716797            3 
            2           256           256   gramschmidt           MPI          2 24415.123047            0 
            2           256           256   gramschmidt           MPI          3 24033.162109            0 
            2           256           256   gramschmidt           MPI          4 24065.408203            0 
            2           256           256   gramschmidt           MPI          5 24096.162109            0 
            2           256           256   gramschmidt           MPI          6 23982.287109            0 
            2           256           256   gramschmidt           MPI          7 23982.673828            0 
            2           256           256   gramschmidt           MPI          8 23953.402344            0 
            2           256           256   gramschmidt           MPI          9 23985.986328            0 
# Runtime: 12.380104 s (overhead: 0.000024 %) 10 records
