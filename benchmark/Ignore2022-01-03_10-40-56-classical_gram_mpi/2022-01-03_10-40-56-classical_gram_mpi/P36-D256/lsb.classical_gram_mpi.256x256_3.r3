# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:32:00 2022
# Execution time and date (local): Mon Jan  3 11:32:00 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 26947.972656            0 
            3           256           256   gramschmidt           MPI          1 26681.707031            3 
            3           256           256   gramschmidt           MPI          2 24413.681641            0 
            3           256           256   gramschmidt           MPI          3 24031.486328            0 
            3           256           256   gramschmidt           MPI          4 24068.216797            0 
            3           256           256   gramschmidt           MPI          5 24092.587891            0 
            3           256           256   gramschmidt           MPI          6 23957.195312            0 
            3           256           256   gramschmidt           MPI          7 23979.433594            0 
            3           256           256   gramschmidt           MPI          8 23953.621094            0 
            3           256           256   gramschmidt           MPI          9 24017.398438            0 
# Runtime: 17.383641 s (overhead: 0.000017 %) 10 records
