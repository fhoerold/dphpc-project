# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:00:13 2022
# Execution time and date (local): Mon Jan  3 11:00:13 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 335648.554688            0 
            2          1024          1024   gramschmidt           MPI          1 333690.875000            5 
            2          1024          1024   gramschmidt           MPI          2 334225.763672            1 
            2          1024          1024   gramschmidt           MPI          3 335358.101562            0 
            2          1024          1024   gramschmidt           MPI          4 337882.208984            2 
            2          1024          1024   gramschmidt           MPI          5 334570.953125            0 
            2          1024          1024   gramschmidt           MPI          6 335808.662109            0 
            2          1024          1024   gramschmidt           MPI          7 335529.623047            0 
            2          1024          1024   gramschmidt           MPI          8 335351.572266            2 
            2          1024          1024   gramschmidt           MPI          9 363811.953125            0 
# Runtime: 12.465926 s (overhead: 0.000080 %) 10 records
