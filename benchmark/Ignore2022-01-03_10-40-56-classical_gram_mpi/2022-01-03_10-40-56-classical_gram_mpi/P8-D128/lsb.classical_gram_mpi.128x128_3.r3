# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:42:00 2022
# Execution time and date (local): Mon Jan  3 10:42:00 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 1958.521484            2 
            3           128           128   gramschmidt           MPI          1 1947.078125            3 
            3           128           128   gramschmidt           MPI          2 1863.320312            0 
            3           128           128   gramschmidt           MPI          3 1874.703125            0 
            3           128           128   gramschmidt           MPI          4 1904.859375            0 
            3           128           128   gramschmidt           MPI          5 1837.767578            0 
            3           128           128   gramschmidt           MPI          6 1846.060547            0 
            3           128           128   gramschmidt           MPI          7 1824.179688            0 
            3           128           128   gramschmidt           MPI          8 1832.769531            0 
            3           128           128   gramschmidt           MPI          9 1802.185547            0 
# Runtime: 4.017329 s (overhead: 0.000124 %) 10 records
