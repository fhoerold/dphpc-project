# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:42:22 2022
# Execution time and date (local): Mon Jan  3 10:42:22 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 73586.371094            0 
            3           512           512   gramschmidt           MPI          1 71073.718750            3 
            3           512           512   gramschmidt           MPI          2 70267.460938            2 
            3           512           512   gramschmidt           MPI          3 70624.484375            0 
            3           512           512   gramschmidt           MPI          4 70088.582031            1 
            3           512           512   gramschmidt           MPI          5 71076.613281            0 
            3           512           512   gramschmidt           MPI          6 71146.378906            0 
            3           512           512   gramschmidt           MPI          7 70855.664062            0 
            3           512           512   gramschmidt           MPI          8 70456.919922            1 
            3           512           512   gramschmidt           MPI          9 74512.630859            0 
# Runtime: 8.936832 s (overhead: 0.000078 %) 10 records
