# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:59:31 2022
# Execution time and date (local): Mon Jan  3 10:59:31 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 679.695312            0 
            3            32            32   gramschmidt           MPI          1 671.517578            2 
            3            32            32   gramschmidt           MPI          2 601.179688            0 
            3            32            32   gramschmidt           MPI          3 617.943359            0 
            3            32            32   gramschmidt           MPI          4 625.173828            0 
            3            32            32   gramschmidt           MPI          5 577.765625            0 
            3            32            32   gramschmidt           MPI          6 583.242188            0 
            3            32            32   gramschmidt           MPI          7 608.472656            0 
            3            32            32   gramschmidt           MPI          8 594.054688            0 
            3            32            32   gramschmidt           MPI          9 580.203125            0 
# Runtime: 10.009108 s (overhead: 0.000020 %) 10 records
