# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:59:31 2022
# Execution time and date (local): Mon Jan  3 10:59:31 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 690.640625            0 
            2            32            32   gramschmidt           MPI          1 687.076172            2 
            2            32            32   gramschmidt           MPI          2 613.279297            0 
            2            32            32   gramschmidt           MPI          3 621.443359            0 
            2            32            32   gramschmidt           MPI          4 638.669922            0 
            2            32            32   gramschmidt           MPI          5 589.285156            0 
            2            32            32   gramschmidt           MPI          6 592.431641            0 
            2            32            32   gramschmidt           MPI          7 617.138672            0 
            2            32            32   gramschmidt           MPI          8 597.566406            0 
            2            32            32   gramschmidt           MPI          9 588.791016            0 
# Runtime: 10.018840 s (overhead: 0.000020 %) 10 records
