# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:42:12 2022
# Execution time and date (local): Mon Jan  3 10:42:12 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 8851.578125            0 
            3           256           256   gramschmidt           MPI          1 8806.296875            3 
            3           256           256   gramschmidt           MPI          2 8907.333984            1 
            3           256           256   gramschmidt           MPI          3 8880.056641            0 
            3           256           256   gramschmidt           MPI          4 8786.738281            1 
            3           256           256   gramschmidt           MPI          5 8619.218750            0 
            3           256           256   gramschmidt           MPI          6 8681.255859            0 
            3           256           256   gramschmidt           MPI          7 8691.517578            0 
            3           256           256   gramschmidt           MPI          8 8652.869141            0 
            3           256           256   gramschmidt           MPI          9 8675.224609            0 
# Runtime: 3.126448 s (overhead: 0.000160 %) 10 records
