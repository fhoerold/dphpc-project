# Sysname : Linux
# Nodename: eu-a6-004-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:44:54 2022
# Execution time and date (local): Mon Jan  3 10:44:54 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 6013484.218750            0 
            3          2048          2048   gramschmidt           MPI          1 5995768.134766            5 
            3          2048          2048   gramschmidt           MPI          2 5975725.773438            6 
            3          2048          2048   gramschmidt           MPI          3 5978957.451172            0 
            3          2048          2048   gramschmidt           MPI          4 5969913.599609            2 
            3          2048          2048   gramschmidt           MPI          5 6035626.386719            0 
            3          2048          2048   gramschmidt           MPI          6 6131694.960938            1 
            3          2048          2048   gramschmidt           MPI          7 6059337.478516            0 
            3          2048          2048   gramschmidt           MPI          8 6002524.310547            5 
            3          2048          2048   gramschmidt           MPI          9 6024028.812500            0 
# Runtime: 84.177836 s (overhead: 0.000023 %) 10 records
