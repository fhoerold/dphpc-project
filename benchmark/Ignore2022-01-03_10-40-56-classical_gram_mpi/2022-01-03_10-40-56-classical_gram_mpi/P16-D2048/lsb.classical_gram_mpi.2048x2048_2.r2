# Sysname : Linux
# Nodename: eu-a6-004-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:44:54 2022
# Execution time and date (local): Mon Jan  3 10:44:54 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 6014506.474609            0 
            2          2048          2048   gramschmidt           MPI          1 5996989.843750            6 
            2          2048          2048   gramschmidt           MPI          2 5976737.429688            7 
            2          2048          2048   gramschmidt           MPI          3 5980203.638672            0 
            2          2048          2048   gramschmidt           MPI          4 5971112.496094            2 
            2          2048          2048   gramschmidt           MPI          5 6036904.906250            0 
            2          2048          2048   gramschmidt           MPI          6 6133027.853516            0 
            2          2048          2048   gramschmidt           MPI          7 6060297.970703            0 
            2          2048          2048   gramschmidt           MPI          8 6003722.390625            5 
            2          2048          2048   gramschmidt           MPI          9 6025187.464844            0 
# Runtime: 81.207857 s (overhead: 0.000025 %) 10 records
