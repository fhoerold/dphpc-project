# Sysname : Linux
# Nodename: eu-a6-012-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:42:59 2022
# Execution time and date (local): Mon Jan  3 10:42:59 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 93283783.408203            2 
            2          4096          4096   gramschmidt           MPI          1 93376158.039062           10 
            2          4096          4096   gramschmidt           MPI          2 92976623.335938            4 
            2          4096          4096   gramschmidt           MPI          3 93240726.242188            0 
            2          4096          4096   gramschmidt           MPI          4 93146129.664062            4 
            2          4096          4096   gramschmidt           MPI          5 92635963.148438            0 
            2          4096          4096   gramschmidt           MPI          6 92972338.679688            0 
            2          4096          4096   gramschmidt           MPI          7 94028584.941406            0 
            2          4096          4096   gramschmidt           MPI          8 93600691.496094            8 
            2          4096          4096   gramschmidt           MPI          9 93065452.451172            0 
# Runtime: 1216.115021 s (overhead: 0.000002 %) 10 records
