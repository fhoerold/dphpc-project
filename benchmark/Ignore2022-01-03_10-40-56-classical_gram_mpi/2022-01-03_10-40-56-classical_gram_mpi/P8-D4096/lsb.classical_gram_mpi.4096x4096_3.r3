# Sysname : Linux
# Nodename: eu-a6-012-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:42:59 2022
# Execution time and date (local): Mon Jan  3 10:42:59 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 93292093.634766            0 
            3          4096          4096   gramschmidt           MPI          1 93384300.814453            4 
            3          4096          4096   gramschmidt           MPI          2 92984759.310547            4 
            3          4096          4096   gramschmidt           MPI          3 93248937.162109            0 
            3          4096          4096   gramschmidt           MPI          4 93154279.705078            4 
            3          4096          4096   gramschmidt           MPI          5 92644316.664062            2 
            3          4096          4096   gramschmidt           MPI          6 92980549.388672            3 
            3          4096          4096   gramschmidt           MPI          7 94036988.250000            2 
            3          4096          4096   gramschmidt           MPI          8 93608915.671875            9 
            3          4096          4096   gramschmidt           MPI          9 93073878.152344            3 
# Runtime: 1216.169774 s (overhead: 0.000003 %) 10 records
