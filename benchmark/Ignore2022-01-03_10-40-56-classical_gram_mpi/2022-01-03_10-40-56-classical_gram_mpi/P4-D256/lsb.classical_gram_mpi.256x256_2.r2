# Sysname : Linux
# Nodename: eu-a6-011-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:41:30 2022
# Execution time and date (local): Mon Jan  3 10:41:30 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 14110.566406            0 
            2           256           256   gramschmidt           MPI          1 14295.070312            3 
            2           256           256   gramschmidt           MPI          2 14080.794922            0 
            2           256           256   gramschmidt           MPI          3 14208.390625            0 
            2           256           256   gramschmidt           MPI          4 14076.724609            0 
            2           256           256   gramschmidt           MPI          5 14098.166016            0 
            2           256           256   gramschmidt           MPI          6 14442.896484            0 
            2           256           256   gramschmidt           MPI          7 14314.718750            0 
            2           256           256   gramschmidt           MPI          8 14230.546875            0 
            2           256           256   gramschmidt           MPI          9 14248.818359            0 
# Runtime: 7.199458 s (overhead: 0.000042 %) 10 records
