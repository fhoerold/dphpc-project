# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:01:37 2022
# Execution time and date (local): Mon Jan  3 11:01:37 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 3238.957031            0 
            2           128           128   gramschmidt           MPI          1 2977.541016            2 
            2           128           128   gramschmidt           MPI          2 2787.304688            0 
            2           128           128   gramschmidt           MPI          3 2746.650391            0 
            2           128           128   gramschmidt           MPI          4 2755.644531            0 
            2           128           128   gramschmidt           MPI          5 2747.847656            0 
            2           128           128   gramschmidt           MPI          6 2758.804688            0 
            2           128           128   gramschmidt           MPI          7 2807.173828            0 
            2           128           128   gramschmidt           MPI          8 2807.582031            0 
            2           128           128   gramschmidt           MPI          9 2805.707031            0 
# Runtime: 6.043366 s (overhead: 0.000033 %) 10 records
