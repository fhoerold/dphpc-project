# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:01:37 2022
# Execution time and date (local): Mon Jan  3 11:01:37 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 2916.123047            0 
            3           128           128   gramschmidt           MPI          1 2977.246094            3 
            3           128           128   gramschmidt           MPI          2 2785.515625            0 
            3           128           128   gramschmidt           MPI          3 2745.144531            0 
            3           128           128   gramschmidt           MPI          4 2753.380859            0 
            3           128           128   gramschmidt           MPI          5 2745.914062            0 
            3           128           128   gramschmidt           MPI          6 2757.263672            0 
            3           128           128   gramschmidt           MPI          7 2808.205078            0 
            3           128           128   gramschmidt           MPI          8 2808.970703            0 
            3           128           128   gramschmidt           MPI          9 2804.751953            0 
# Runtime: 5.017037 s (overhead: 0.000060 %) 10 records
