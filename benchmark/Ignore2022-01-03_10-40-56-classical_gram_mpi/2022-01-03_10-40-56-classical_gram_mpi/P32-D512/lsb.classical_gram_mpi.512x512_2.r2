# Sysname : Linux
# Nodename: eu-a6-005-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:18:18 2022
# Execution time and date (local): Mon Jan  3 11:18:18 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 92818.662109            0 
            2           512           512   gramschmidt           MPI          1 87964.976562            1 
            2           512           512   gramschmidt           MPI          2 65648.664062            1 
            2           512           512   gramschmidt           MPI          3 67214.095703            0 
            2           512           512   gramschmidt           MPI          4 64504.232422            1 
            2           512           512   gramschmidt           MPI          5 48628.011719            0 
            2           512           512   gramschmidt           MPI          6 49982.144531            0 
            2           512           512   gramschmidt           MPI          7 48524.447266            0 
            2           512           512   gramschmidt           MPI          8 48620.972656            5 
            2           512           512   gramschmidt           MPI          9 51126.019531            0 
# Runtime: 1.163012 s (overhead: 0.000688 %) 10 records
