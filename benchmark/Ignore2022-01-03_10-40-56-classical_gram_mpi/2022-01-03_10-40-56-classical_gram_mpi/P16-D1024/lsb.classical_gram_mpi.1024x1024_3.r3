# Sysname : Linux
# Nodename: eu-a6-004-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:44:35 2022
# Execution time and date (local): Mon Jan  3 10:44:35 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 349458.117188            0 
            3          1024          1024   gramschmidt           MPI          1 330332.591797            5 
            3          1024          1024   gramschmidt           MPI          2 347518.091797            2 
            3          1024          1024   gramschmidt           MPI          3 362670.798828            0 
            3          1024          1024   gramschmidt           MPI          4 327936.357422            2 
            3          1024          1024   gramschmidt           MPI          5 346856.820312            0 
            3          1024          1024   gramschmidt           MPI          6 328037.378906            0 
            3          1024          1024   gramschmidt           MPI          7 326340.408203            0 
            3          1024          1024   gramschmidt           MPI          8 324113.925781            2 
            3          1024          1024   gramschmidt           MPI          9 342327.039062            0 
# Runtime: 4.421032 s (overhead: 0.000249 %) 10 records
