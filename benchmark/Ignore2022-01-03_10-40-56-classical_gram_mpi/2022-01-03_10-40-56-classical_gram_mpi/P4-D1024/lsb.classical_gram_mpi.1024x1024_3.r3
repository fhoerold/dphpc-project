# Sysname : Linux
# Nodename: eu-a6-011-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:41:30 2022
# Execution time and date (local): Mon Jan  3 10:41:30 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 1960822.099609            0 
            3          1024          1024   gramschmidt           MPI          1 1903381.169922            3 
            3          1024          1024   gramschmidt           MPI          2 1951280.404297            2 
            3          1024          1024   gramschmidt           MPI          3 1960562.892578            0 
            3          1024          1024   gramschmidt           MPI          4 1976740.753906            3 
            3          1024          1024   gramschmidt           MPI          5 1935948.591797            0 
            3          1024          1024   gramschmidt           MPI          6 1954307.798828            0 
            3          1024          1024   gramschmidt           MPI          7 1974545.746094            0 
            3          1024          1024   gramschmidt           MPI          8 1861039.246094            1 
            3          1024          1024   gramschmidt           MPI          9 1886567.484375            0 
# Runtime: 25.205074 s (overhead: 0.000036 %) 10 records
