# Sysname : Linux
# Nodename: eu-a6-009-13
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:03:32 2022
# Execution time and date (local): Mon Jan  3 11:03:32 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 53245201.644531            0 
            3          4096          4096   gramschmidt           MPI          1 53423091.041016            6 
            3          4096          4096   gramschmidt           MPI          2 55732295.857422            4 
            3          4096          4096   gramschmidt           MPI          3 55534908.601562            0 
            3          4096          4096   gramschmidt           MPI          4 53887497.843750            6 
            3          4096          4096   gramschmidt           MPI          5 54083167.289062            0 
            3          4096          4096   gramschmidt           MPI          6 53715716.369141            0 
            3          4096          4096   gramschmidt           MPI          7 53372857.855469            0 
            3          4096          4096   gramschmidt           MPI          8 54927857.181641            8 
            3          4096          4096   gramschmidt           MPI          9 55418808.294922            1 
# Runtime: 720.156452 s (overhead: 0.000003 %) 10 records
