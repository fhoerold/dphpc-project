# Sysname : Linux
# Nodename: eu-a6-004-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:42:59 2022
# Execution time and date (local): Mon Jan  3 10:42:59 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 536.216797            0 
            3            32            32   gramschmidt           MPI          1 465.113281            2 
            3            32            32   gramschmidt           MPI          2 452.705078            0 
            3            32            32   gramschmidt           MPI          3 435.994141            0 
            3            32            32   gramschmidt           MPI          4 403.478516            0 
            3            32            32   gramschmidt           MPI          5 401.824219            0 
            3            32            32   gramschmidt           MPI          6 378.156250            0 
            3            32            32   gramschmidt           MPI          7 384.068359            0 
            3            32            32   gramschmidt           MPI          8 376.230469            0 
            3            32            32   gramschmidt           MPI          9 384.972656            0 
# Runtime: 0.991676 s (overhead: 0.000202 %) 10 records
