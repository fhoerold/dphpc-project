# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:42:00 2022
# Execution time and date (local): Mon Jan  3 10:42:00 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 319.496094            0 
            3            32            32   gramschmidt           MPI          1 290.597656            2 
            3            32            32   gramschmidt           MPI          2 300.960938            0 
            3            32            32   gramschmidt           MPI          3 296.152344            0 
            3            32            32   gramschmidt           MPI          4 295.582031            0 
            3            32            32   gramschmidt           MPI          5 306.957031            0 
            3            32            32   gramschmidt           MPI          6 319.701172            0 
            3            32            32   gramschmidt           MPI          7 290.382812            0 
            3            32            32   gramschmidt           MPI          8 290.408203            0 
            3            32            32   gramschmidt           MPI          9 288.132812            0 
# Runtime: 8.002826 s (overhead: 0.000025 %) 10 records
