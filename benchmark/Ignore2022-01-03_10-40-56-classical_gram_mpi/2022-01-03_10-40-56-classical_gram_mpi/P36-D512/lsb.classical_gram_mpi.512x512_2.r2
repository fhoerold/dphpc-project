# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:32:25 2022
# Execution time and date (local): Mon Jan  3 11:32:25 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 63949.378906            0 
            2           512           512   gramschmidt           MPI          1 69178.705078            5 
            2           512           512   gramschmidt           MPI          2 69541.904297            2 
            2           512           512   gramschmidt           MPI          3 68627.970703            0 
            2           512           512   gramschmidt           MPI          4 68372.681641            2 
            2           512           512   gramschmidt           MPI          5 71135.845703            0 
            2           512           512   gramschmidt           MPI          6 70269.833984            2 
            2           512           512   gramschmidt           MPI          7 68443.548828            0 
            2           512           512   gramschmidt           MPI          8 68064.167969            5 
            2           512           512   gramschmidt           MPI          9 68042.074219            0 
# Runtime: 5.177973 s (overhead: 0.000309 %) 10 records
