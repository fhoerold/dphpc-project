# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:32:25 2022
# Execution time and date (local): Mon Jan  3 11:32:25 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 63592.312500            0 
            3           512           512   gramschmidt           MPI          1 68811.017578            5 
            3           512           512   gramschmidt           MPI          2 69187.322266            1 
            3           512           512   gramschmidt           MPI          3 68284.564453            1 
            3           512           512   gramschmidt           MPI          4 68022.857422            1 
            3           512           512   gramschmidt           MPI          5 70795.824219            0 
            3           512           512   gramschmidt           MPI          6 69896.027344            0 
            3           512           512   gramschmidt           MPI          7 68078.253906            0 
            3           512           512   gramschmidt           MPI          8 67649.763672            5 
            3           512           512   gramschmidt           MPI          9 67668.718750            0 
# Runtime: 5.174507 s (overhead: 0.000251 %) 10 records
