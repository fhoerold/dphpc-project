# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:02:14 2022
# Execution time and date (local): Mon Jan  3 11:02:14 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 6184971.939453            0 
            3          2048          2048   gramschmidt           MPI          1 6150762.738281            5 
            3          2048          2048   gramschmidt           MPI          2 6140122.781250            1 
            3          2048          2048   gramschmidt           MPI          3 6211383.158203            0 
            3          2048          2048   gramschmidt           MPI          4 6202133.583984            2 
            3          2048          2048   gramschmidt           MPI          5 6192094.136719            0 
            3          2048          2048   gramschmidt           MPI          6 6168667.312500            0 
            3          2048          2048   gramschmidt           MPI          7 6245237.685547            0 
            3          2048          2048   gramschmidt           MPI          8 6208134.542969            5 
            3          2048          2048   gramschmidt           MPI          9 6191893.591797            0 
# Runtime: 95.376000 s (overhead: 0.000014 %) 10 records
