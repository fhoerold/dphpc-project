# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:02:14 2022
# Execution time and date (local): Mon Jan  3 11:02:14 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 6186242.343750            0 
            2          2048          2048   gramschmidt           MPI          1 6151768.201172            5 
            2          2048          2048   gramschmidt           MPI          2 6141332.966797            2 
            2          2048          2048   gramschmidt           MPI          3 6212592.441406            0 
            2          2048          2048   gramschmidt           MPI          4 6203335.101562            2 
            2          2048          2048   gramschmidt           MPI          5 6193110.037109            3 
            2          2048          2048   gramschmidt           MPI          6 6169882.880859            0 
            2          2048          2048   gramschmidt           MPI          7 6246456.447266            0 
            2          2048          2048   gramschmidt           MPI          8 6209421.398438            6 
            2          2048          2048   gramschmidt           MPI          9 6193136.712891            0 
# Runtime: 95.399710 s (overhead: 0.000019 %) 10 records
