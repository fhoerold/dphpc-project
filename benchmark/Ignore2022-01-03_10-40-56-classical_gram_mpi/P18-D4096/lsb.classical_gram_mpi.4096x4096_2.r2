# Sysname : Linux
# Nodename: eu-a6-009-13
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:03:32 2022
# Execution time and date (local): Mon Jan  3 11:03:32 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 53243908.712891            2 
            2          4096          4096   gramschmidt           MPI          1 53421707.378906            5 
            2          4096          4096   gramschmidt           MPI          2 55730911.703125            6 
            2          4096          4096   gramschmidt           MPI          3 55533654.697266            0 
            2          4096          4096   gramschmidt           MPI          4 53886125.484375            6 
            2          4096          4096   gramschmidt           MPI          5 54082556.279297            2 
            2          4096          4096   gramschmidt           MPI          6 53714382.587891            2 
            2          4096          4096   gramschmidt           MPI          7 53371447.580078            2 
            2          4096          4096   gramschmidt           MPI          8 54926479.765625           10 
            2          4096          4096   gramschmidt           MPI          9 55417447.457031            0 
# Runtime: 720.174572 s (overhead: 0.000005 %) 10 records
