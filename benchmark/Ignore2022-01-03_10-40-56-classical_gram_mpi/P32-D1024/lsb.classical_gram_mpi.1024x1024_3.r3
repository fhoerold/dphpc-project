# Sysname : Linux
# Nodename: eu-a6-005-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:18:40 2022
# Execution time and date (local): Mon Jan  3 11:18:40 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 543233.646484            0 
            3          1024          1024   gramschmidt           MPI          1 373460.216797            4 
            3          1024          1024   gramschmidt           MPI          2 371402.894531            2 
            3          1024          1024   gramschmidt           MPI          3 373757.009766            0 
            3          1024          1024   gramschmidt           MPI          4 346973.882812            1 
            3          1024          1024   gramschmidt           MPI          5 405468.869141            0 
            3          1024          1024   gramschmidt           MPI          6 330706.986328            0 
            3          1024          1024   gramschmidt           MPI          7 328198.750000            0 
            3          1024          1024   gramschmidt           MPI          8 334262.330078            2 
            3          1024          1024   gramschmidt           MPI          9 331927.595703            0 
# Runtime: 16.911113 s (overhead: 0.000053 %) 10 records
