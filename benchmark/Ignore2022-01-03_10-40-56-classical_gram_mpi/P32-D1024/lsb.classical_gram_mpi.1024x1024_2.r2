# Sysname : Linux
# Nodename: eu-a6-005-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:18:40 2022
# Execution time and date (local): Mon Jan  3 11:18:40 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 543429.970703            0 
            2          1024          1024   gramschmidt           MPI          1 373552.544922            6 
            2          1024          1024   gramschmidt           MPI          2 371429.251953            2 
            2          1024          1024   gramschmidt           MPI          3 373881.613281            0 
            2          1024          1024   gramschmidt           MPI          4 347150.187500            2 
            2          1024          1024   gramschmidt           MPI          5 405632.080078            0 
            2          1024          1024   gramschmidt           MPI          6 330946.121094            0 
            2          1024          1024   gramschmidt           MPI          7 328319.994141            0 
            2          1024          1024   gramschmidt           MPI          8 334377.390625            1 
            2          1024          1024   gramschmidt           MPI          9 332108.503906            0 
# Runtime: 19.924253 s (overhead: 0.000055 %) 10 records
