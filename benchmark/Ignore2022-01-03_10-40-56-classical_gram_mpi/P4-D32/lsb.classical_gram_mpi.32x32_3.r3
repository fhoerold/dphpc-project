# Sysname : Linux
# Nodename: eu-a6-002-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:41:30 2022
# Execution time and date (local): Mon Jan  3 10:41:30 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 237.083984            0 
            3            32            32   gramschmidt           MPI          1 226.255859            2 
            3            32            32   gramschmidt           MPI          2 215.843750            0 
            3            32            32   gramschmidt           MPI          3 216.273438            0 
            3            32            32   gramschmidt           MPI          4 227.769531            0 
            3            32            32   gramschmidt           MPI          5 213.771484            0 
            3            32            32   gramschmidt           MPI          6 211.300781            0 
            3            32            32   gramschmidt           MPI          7 259.376953            0 
            3            32            32   gramschmidt           MPI          8 208.898438            0 
            3            32            32   gramschmidt           MPI          9 216.736328            0 
# Runtime: 2.995897 s (overhead: 0.000067 %) 10 records
