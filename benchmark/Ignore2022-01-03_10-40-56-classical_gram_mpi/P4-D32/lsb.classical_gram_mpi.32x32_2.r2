# Sysname : Linux
# Nodename: eu-a6-002-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:41:30 2022
# Execution time and date (local): Mon Jan  3 10:41:30 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 241.603516            0 
            2            32            32   gramschmidt           MPI          1 230.312500            0 
            2            32            32   gramschmidt           MPI          2 220.037109            0 
            2            32            32   gramschmidt           MPI          3 221.984375            0 
            2            32            32   gramschmidt           MPI          4 231.939453            0 
            2            32            32   gramschmidt           MPI          5 217.027344            0 
            2            32            32   gramschmidt           MPI          6 216.105469            0 
            2            32            32   gramschmidt           MPI          7 265.791016            0 
            2            32            32   gramschmidt           MPI          8 208.876953            0 
            2            32            32   gramschmidt           MPI          9 223.730469            0 
# Runtime: 0.004479 s (overhead: 0.000000 %) 10 records
