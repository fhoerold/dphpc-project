# Sysname : Linux
# Nodename: eu-a6-004-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:43:29 2022
# Execution time and date (local): Mon Jan  3 10:43:29 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 9567.599609            0 
            3           256           256   gramschmidt           MPI          1 9412.525391            3 
            3           256           256   gramschmidt           MPI          2 9407.167969            0 
            3           256           256   gramschmidt           MPI          3 9294.814453            0 
            3           256           256   gramschmidt           MPI          4 9413.884766            1 
            3           256           256   gramschmidt           MPI          5 12183.277344            0 
            3           256           256   gramschmidt           MPI          6 10169.441406            0 
            3           256           256   gramschmidt           MPI          7 9614.398438            0 
            3           256           256   gramschmidt           MPI          8 9481.984375            1 
            3           256           256   gramschmidt           MPI          9 9692.195312            0 
# Runtime: 9.153936 s (overhead: 0.000055 %) 10 records
