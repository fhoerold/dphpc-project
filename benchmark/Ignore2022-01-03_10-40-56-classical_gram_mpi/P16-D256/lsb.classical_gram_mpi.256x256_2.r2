# Sysname : Linux
# Nodename: eu-a6-004-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:43:29 2022
# Execution time and date (local): Mon Jan  3 10:43:29 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 9570.205078            0 
            2           256           256   gramschmidt           MPI          1 9416.994141            4 
            2           256           256   gramschmidt           MPI          2 9412.962891            0 
            2           256           256   gramschmidt           MPI          3 9268.980469            0 
            2           256           256   gramschmidt           MPI          4 9415.400391            0 
            2           256           256   gramschmidt           MPI          5 12181.490234            0 
            2           256           256   gramschmidt           MPI          6 10172.970703            0 
            2           256           256   gramschmidt           MPI          7 9612.255859            0 
            2           256           256   gramschmidt           MPI          8 9473.957031            0 
            2           256           256   gramschmidt           MPI          9 9716.578125            0 
# Runtime: 8.154605 s (overhead: 0.000049 %) 10 records
