# Sysname : Linux
# Nodename: eu-a6-001-06
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:16:38 2022
# Execution time and date (local): Mon Jan  3 11:16:38 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 14410.929688            0 
            3           256           256   gramschmidt           MPI          1 14259.173828            3 
            3           256           256   gramschmidt           MPI          2 14023.837891            0 
            3           256           256   gramschmidt           MPI          3 13854.826172            0 
            3           256           256   gramschmidt           MPI          4 13935.751953            0 
            3           256           256   gramschmidt           MPI          5 13988.017578            0 
            3           256           256   gramschmidt           MPI          6 13930.875000            0 
            3           256           256   gramschmidt           MPI          7 13971.560547            0 
            3           256           256   gramschmidt           MPI          8 14164.957031            0 
            3           256           256   gramschmidt           MPI          9 13944.162109            0 
# Runtime: 12.229898 s (overhead: 0.000025 %) 10 records
