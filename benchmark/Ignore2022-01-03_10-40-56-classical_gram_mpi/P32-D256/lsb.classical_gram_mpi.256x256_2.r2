# Sysname : Linux
# Nodename: eu-a6-001-06
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:16:38 2022
# Execution time and date (local): Mon Jan  3 11:16:38 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 14407.136719            0 
            2           256           256   gramschmidt           MPI          1 14254.828125            3 
            2           256           256   gramschmidt           MPI          2 14017.685547            0 
            2           256           256   gramschmidt           MPI          3 13852.445312            0 
            2           256           256   gramschmidt           MPI          4 13934.478516            0 
            2           256           256   gramschmidt           MPI          5 13980.101562            0 
            2           256           256   gramschmidt           MPI          6 13928.105469            0 
            2           256           256   gramschmidt           MPI          7 13972.802734            0 
            2           256           256   gramschmidt           MPI          8 14160.009766            0 
            2           256           256   gramschmidt           MPI          9 13942.597656            0 
# Runtime: 1.229982 s (overhead: 0.000244 %) 10 records
