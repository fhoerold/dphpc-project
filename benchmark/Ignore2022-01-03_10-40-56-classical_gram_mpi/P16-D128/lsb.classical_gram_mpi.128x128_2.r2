# Sysname : Linux
# Nodename: eu-a6-004-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:43:59 2022
# Execution time and date (local): Mon Jan  3 10:43:59 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 2446.509766            0 
            2           128           128   gramschmidt           MPI          1 2280.351562            2 
            2           128           128   gramschmidt           MPI          2 2282.896484            0 
            2           128           128   gramschmidt           MPI          3 2190.691406            0 
            2           128           128   gramschmidt           MPI          4 2268.376953            0 
            2           128           128   gramschmidt           MPI          5 2248.791016            0 
            2           128           128   gramschmidt           MPI          6 2244.931641            0 
            2           128           128   gramschmidt           MPI          7 2222.521484            0 
            2           128           128   gramschmidt           MPI          8 2325.917969            0 
            2           128           128   gramschmidt           MPI          9 2215.289062            0 
# Runtime: 8.042734 s (overhead: 0.000025 %) 10 records
