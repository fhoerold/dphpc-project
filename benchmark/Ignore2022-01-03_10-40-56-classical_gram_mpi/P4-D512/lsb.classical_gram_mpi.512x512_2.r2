# Sysname : Linux
# Nodename: eu-a6-011-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:41:30 2022
# Execution time and date (local): Mon Jan  3 10:41:30 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 121818.064453            0 
            2           512           512   gramschmidt           MPI          1 121756.767578            1 
            2           512           512   gramschmidt           MPI          2 121363.263672            1 
            2           512           512   gramschmidt           MPI          3 121448.324219            0 
            2           512           512   gramschmidt           MPI          4 125896.429688            2 
            2           512           512   gramschmidt           MPI          5 121781.531250            0 
            2           512           512   gramschmidt           MPI          6 121757.863281            0 
            2           512           512   gramschmidt           MPI          7 121680.031250            0 
            2           512           512   gramschmidt           MPI          8 121966.144531            1 
            2           512           512   gramschmidt           MPI          9 122196.238281            0 
# Runtime: 1.591196 s (overhead: 0.000314 %) 10 records
