# Sysname : Linux
# Nodename: eu-a6-011-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:41:30 2022
# Execution time and date (local): Mon Jan  3 10:41:30 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 121663.269531            0 
            3           512           512   gramschmidt           MPI          1 121610.054688            1 
            3           512           512   gramschmidt           MPI          2 121229.367188            1 
            3           512           512   gramschmidt           MPI          3 121303.302734            0 
            3           512           512   gramschmidt           MPI          4 125808.708984            3 
            3           512           512   gramschmidt           MPI          5 121624.222656            0 
            3           512           512   gramschmidt           MPI          6 121599.611328            0 
            3           512           512   gramschmidt           MPI          7 121512.664062            0 
            3           512           512   gramschmidt           MPI          8 121782.714844            2 
            3           512           512   gramschmidt           MPI          9 122019.285156            0 
# Runtime: 1.594945 s (overhead: 0.000439 %) 10 records
