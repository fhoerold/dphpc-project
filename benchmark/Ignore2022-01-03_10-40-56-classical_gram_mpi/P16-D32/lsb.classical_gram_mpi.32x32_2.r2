# Sysname : Linux
# Nodename: eu-a6-004-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:42:59 2022
# Execution time and date (local): Mon Jan  3 10:42:59 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 557.310547            0 
            2            32            32   gramschmidt           MPI          1 469.494141            3 
            2            32            32   gramschmidt           MPI          2 459.873047            0 
            2            32            32   gramschmidt           MPI          3 439.451172            0 
            2            32            32   gramschmidt           MPI          4 407.177734            0 
            2            32            32   gramschmidt           MPI          5 404.824219            0 
            2            32            32   gramschmidt           MPI          6 381.662109            0 
            2            32            32   gramschmidt           MPI          7 388.466797            0 
            2            32            32   gramschmidt           MPI          8 380.701172            0 
            2            32            32   gramschmidt           MPI          9 388.062500            0 
# Runtime: 0.998878 s (overhead: 0.000300 %) 10 records
