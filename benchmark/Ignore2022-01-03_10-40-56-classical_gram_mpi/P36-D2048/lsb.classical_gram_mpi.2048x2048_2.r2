# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:33:18 2022
# Execution time and date (local): Mon Jan  3 11:33:18 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 3098647.771484            0 
            2          2048          2048   gramschmidt           MPI          1 3031248.306641            5 
            2          2048          2048   gramschmidt           MPI          2 3110202.421875           10 
            2          2048          2048   gramschmidt           MPI          3 3012089.154297            0 
            2          2048          2048   gramschmidt           MPI          4 3092694.880859            2 
            2          2048          2048   gramschmidt           MPI          5 3017554.929688            0 
            2          2048          2048   gramschmidt           MPI          6 3013484.884766            3 
            2          2048          2048   gramschmidt           MPI          7 3048773.140625            0 
            2          2048          2048   gramschmidt           MPI          8 3009244.375000            6 
            2          2048          2048   gramschmidt           MPI          9 3102682.267578            0 
# Runtime: 44.544328 s (overhead: 0.000058 %) 10 records
