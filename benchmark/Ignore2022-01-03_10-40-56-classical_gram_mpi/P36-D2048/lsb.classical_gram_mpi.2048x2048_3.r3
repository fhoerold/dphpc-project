# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:33:18 2022
# Execution time and date (local): Mon Jan  3 11:33:18 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 3098398.896484            0 
            3          2048          2048   gramschmidt           MPI          1 3031078.009766            6 
            3          2048          2048   gramschmidt           MPI          2 3109939.636719            1 
            3          2048          2048   gramschmidt           MPI          3 3011925.773438            0 
            3          2048          2048   gramschmidt           MPI          4 3092442.376953            2 
            3          2048          2048   gramschmidt           MPI          5 3017296.740234            4 
            3          2048          2048   gramschmidt           MPI          6 3013153.251953            0 
            3          2048          2048   gramschmidt           MPI          7 3048424.755859            0 
            3          2048          2048   gramschmidt           MPI          8 3009010.671875            6 
            3          2048          2048   gramschmidt           MPI          9 3102338.884766            0 
# Runtime: 47.538623 s (overhead: 0.000040 %) 10 records
