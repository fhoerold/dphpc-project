# Sysname : Linux
# Nodename: eu-a6-001-06
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:17:05 2022
# Execution time and date (local): Mon Jan  3 11:17:05 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 1670943.486328            0 
            3          2048          2048   gramschmidt           MPI          1 1722978.691406            6 
            3          2048          2048   gramschmidt           MPI          2 1648830.921875            2 
            3          2048          2048   gramschmidt           MPI          3 1634052.164062            0 
            3          2048          2048   gramschmidt           MPI          4 1726629.097656            2 
            3          2048          2048   gramschmidt           MPI          5 1650385.460938            0 
            3          2048          2048   gramschmidt           MPI          6 1674042.152344            0 
            3          2048          2048   gramschmidt           MPI          7 1630241.105469            0 
            3          2048          2048   gramschmidt           MPI          8 1622908.757812            5 
            3          2048          2048   gramschmidt           MPI          9 1688182.621094            0 
# Runtime: 28.082882 s (overhead: 0.000053 %) 10 records
