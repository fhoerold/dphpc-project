# Sysname : Linux
# Nodename: eu-a6-001-06
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:16:03 2022
# Execution time and date (local): Mon Jan  3 11:16:03 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 1049.597656            0 
            3            32            32   gramschmidt           MPI          1 1016.451172            2 
            3            32            32   gramschmidt           MPI          2 964.335938            0 
            3            32            32   gramschmidt           MPI          3 960.882812            0 
            3            32            32   gramschmidt           MPI          4 949.951172            0 
            3            32            32   gramschmidt           MPI          5 949.582031            0 
            3            32            32   gramschmidt           MPI          6 959.593750            0 
            3            32            32   gramschmidt           MPI          7 923.958984            0 
            3            32            32   gramschmidt           MPI          8 931.353516            0 
            3            32            32   gramschmidt           MPI          9 918.593750            0 
# Runtime: 7.036881 s (overhead: 0.000028 %) 10 records
