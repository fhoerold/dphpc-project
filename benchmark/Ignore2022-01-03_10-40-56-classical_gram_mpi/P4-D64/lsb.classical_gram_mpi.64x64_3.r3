# Sysname : Linux
# Nodename: eu-a6-002-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:41:30 2022
# Execution time and date (local): Mon Jan  3 10:41:30 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 585.923828            0 
            3            64            64   gramschmidt           MPI          1 583.615234            0 
            3            64            64   gramschmidt           MPI          2 536.666016            0 
            3            64            64   gramschmidt           MPI          3 544.382812            0 
            3            64            64   gramschmidt           MPI          4 537.746094            0 
            3            64            64   gramschmidt           MPI          5 522.320312            0 
            3            64            64   gramschmidt           MPI          6 562.273438            0 
            3            64            64   gramschmidt           MPI          7 575.812500            0 
            3            64            64   gramschmidt           MPI          8 558.486328            0 
            3            64            64   gramschmidt           MPI          9 537.916016            0 
# Runtime: 0.008890 s (overhead: 0.000000 %) 10 records
