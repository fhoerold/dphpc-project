# Sysname : Linux
# Nodename: eu-a6-002-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:41:30 2022
# Execution time and date (local): Mon Jan  3 10:41:30 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 580.160156            0 
            2            64            64   gramschmidt           MPI          1 582.642578            3 
            2            64            64   gramschmidt           MPI          2 538.894531            0 
            2            64            64   gramschmidt           MPI          3 540.923828            0 
            2            64            64   gramschmidt           MPI          4 530.503906            0 
            2            64            64   gramschmidt           MPI          5 517.369141            0 
            2            64            64   gramschmidt           MPI          6 550.724609            0 
            2            64            64   gramschmidt           MPI          7 565.173828            0 
            2            64            64   gramschmidt           MPI          8 556.025391            0 
            2            64            64   gramschmidt           MPI          9 533.160156            0 
# Runtime: 3.008373 s (overhead: 0.000100 %) 10 records
