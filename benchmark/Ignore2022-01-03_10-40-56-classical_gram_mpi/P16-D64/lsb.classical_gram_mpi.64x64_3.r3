# Sysname : Linux
# Nodename: eu-a6-004-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:43:12 2022
# Execution time and date (local): Mon Jan  3 10:43:12 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 1064.349609            0 
            3            64            64   gramschmidt           MPI          1 978.365234            0 
            3            64            64   gramschmidt           MPI          2 1035.462891            0 
            3            64            64   gramschmidt           MPI          3 917.761719            0 
            3            64            64   gramschmidt           MPI          4 974.771484            0 
            3            64            64   gramschmidt           MPI          5 917.218750            0 
            3            64            64   gramschmidt           MPI          6 891.548828            0 
            3            64            64   gramschmidt           MPI          7 906.921875            0 
            3            64            64   gramschmidt           MPI          8 906.261719            0 
            3            64            64   gramschmidt           MPI          9 914.181641            0 
# Runtime: 0.015518 s (overhead: 0.000000 %) 10 records
