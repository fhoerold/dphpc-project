# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:31:38 2022
# Execution time and date (local): Mon Jan  3 11:31:38 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 5078.943359            0 
            2           128           128   gramschmidt           MPI          1 4669.167969            3 
            2           128           128   gramschmidt           MPI          2 4605.072266            0 
            2           128           128   gramschmidt           MPI          3 4594.476562            0 
            2           128           128   gramschmidt           MPI          4 4535.830078            0 
            2           128           128   gramschmidt           MPI          5 4519.566406            0 
            2           128           128   gramschmidt           MPI          6 4518.660156            0 
            2           128           128   gramschmidt           MPI          7 4520.107422            0 
            2           128           128   gramschmidt           MPI          8 4512.533203            0 
            2           128           128   gramschmidt           MPI          9 4539.009766            0 
# Runtime: 13.086848 s (overhead: 0.000023 %) 10 records
