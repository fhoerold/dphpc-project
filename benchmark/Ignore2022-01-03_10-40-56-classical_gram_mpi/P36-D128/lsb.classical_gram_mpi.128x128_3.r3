# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:31:38 2022
# Execution time and date (local): Mon Jan  3 11:31:38 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 5071.763672            0 
            3           128           128   gramschmidt           MPI          1 4662.410156            2 
            3           128           128   gramschmidt           MPI          2 4600.802734            0 
            3           128           128   gramschmidt           MPI          3 4589.791016            0 
            3           128           128   gramschmidt           MPI          4 4528.777344            0 
            3           128           128   gramschmidt           MPI          5 4514.074219            0 
            3           128           128   gramschmidt           MPI          6 4513.250000            0 
            3           128           128   gramschmidt           MPI          7 4514.394531            0 
            3           128           128   gramschmidt           MPI          8 4505.302734            0 
            3           128           128   gramschmidt           MPI          9 4530.189453            0 
# Runtime: 15.045116 s (overhead: 0.000013 %) 10 records
