# Sysname : Linux
# Nodename: eu-a6-009-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:42:00 2022
# Execution time and date (local): Mon Jan  3 10:42:00 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 346829581.529297            0 
            2          4096          4096   gramschmidt           MPI          1 355437718.154297            6 
            2          4096          4096   gramschmidt           MPI          2 338996281.775391            9 
            2          4096          4096   gramschmidt           MPI          3 226753732.054688            0 
            2          4096          4096   gramschmidt           MPI          4 229173076.623047           12 
            2          4096          4096   gramschmidt           MPI          5 310806955.011719            3 
            2          4096          4096   gramschmidt           MPI          6 329722646.970703            5 
            2          4096          4096   gramschmidt           MPI          7 228759005.521484            6 
            2          4096          4096   gramschmidt           MPI          8 348027519.603516           10 
            2          4096          4096   gramschmidt           MPI          9 226308876.662109            3 
# Runtime: 3780.638058 s (overhead: 0.000001 %) 10 records
