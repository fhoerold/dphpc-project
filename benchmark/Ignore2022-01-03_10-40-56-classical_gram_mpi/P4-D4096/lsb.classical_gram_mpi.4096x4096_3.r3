# Sysname : Linux
# Nodename: eu-a6-009-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:42:00 2022
# Execution time and date (local): Mon Jan  3 10:42:00 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 346832273.400391            0 
            3          4096          4096   gramschmidt           MPI          1 355440498.457031            5 
            3          4096          4096   gramschmidt           MPI          2 338999109.734375            7 
            3          4096          4096   gramschmidt           MPI          3 226759021.158203            1 
            3          4096          4096   gramschmidt           MPI          4 229177827.185547            5 
            3          4096          4096   gramschmidt           MPI          5 310810430.009766            0 
            3          4096          4096   gramschmidt           MPI          6 329726607.089844            0 
            3          4096          4096   gramschmidt           MPI          7 228763863.835938            2 
            3          4096          4096   gramschmidt           MPI          8 348030295.353516            8 
            3          4096          4096   gramschmidt           MPI          9 226313435.570312            0 
# Runtime: 3775.578175 s (overhead: 0.000001 %) 10 records
