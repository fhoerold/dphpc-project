# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:01:54 2022
# Execution time and date (local): Mon Jan  3 11:01:54 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 8920.693359            0 
            2           256           256   gramschmidt           MPI          1 8828.921875            3 
            2           256           256   gramschmidt           MPI          2 8738.070312            0 
            2           256           256   gramschmidt           MPI          3 8682.271484            0 
            2           256           256   gramschmidt           MPI          4 8697.400391            0 
            2           256           256   gramschmidt           MPI          5 8663.148438            0 
            2           256           256   gramschmidt           MPI          6 8572.804688            0 
            2           256           256   gramschmidt           MPI          7 8646.701172            0 
            2           256           256   gramschmidt           MPI          8 10139.871094            0 
            2           256           256   gramschmidt           MPI          9 8683.869141            0 
# Runtime: 11.152525 s (overhead: 0.000027 %) 10 records
