# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:42:22 2022
# Execution time and date (local): Mon Jan  3 10:42:22 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 73269.162109            0 
            2           512           512   gramschmidt           MPI          1 70760.833984            5 
            2           512           512   gramschmidt           MPI          2 69948.021484            1 
            2           512           512   gramschmidt           MPI          3 70282.724609            0 
            2           512           512   gramschmidt           MPI          4 69766.912109            1 
            2           512           512   gramschmidt           MPI          5 70762.880859            0 
            2           512           512   gramschmidt           MPI          6 70819.201172            0 
            2           512           512   gramschmidt           MPI          7 70524.603516            0 
            2           512           512   gramschmidt           MPI          8 70137.031250            1 
            2           512           512   gramschmidt           MPI          9 74132.957031            0 
# Runtime: 11.926057 s (overhead: 0.000067 %) 10 records
