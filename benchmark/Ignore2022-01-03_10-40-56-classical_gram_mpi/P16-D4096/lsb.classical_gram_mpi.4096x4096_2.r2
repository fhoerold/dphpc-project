# Sysname : Linux
# Nodename: eu-a6-004-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:46:30 2022
# Execution time and date (local): Mon Jan  3 10:46:30 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 58519827.322266            0 
            2          4096          4096   gramschmidt           MPI          1 56810765.787109           11 
            2          4096          4096   gramschmidt           MPI          2 58979952.105469            7 
            2          4096          4096   gramschmidt           MPI          3 59559386.544922            0 
            2          4096          4096   gramschmidt           MPI          4 57422351.544922            6 
            2          4096          4096   gramschmidt           MPI          5 57036511.876953            1 
            2          4096          4096   gramschmidt           MPI          6 57432314.294922            1 
            2          4096          4096   gramschmidt           MPI          7 57718565.804688            1 
            2          4096          4096   gramschmidt           MPI          8 56474789.199219           12 
            2          4096          4096   gramschmidt           MPI          9 57857540.466797            1 
# Runtime: 762.276273 s (overhead: 0.000005 %) 10 records
