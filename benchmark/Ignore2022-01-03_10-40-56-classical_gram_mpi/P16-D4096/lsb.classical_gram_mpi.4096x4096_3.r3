# Sysname : Linux
# Nodename: eu-a6-004-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:46:30 2022
# Execution time and date (local): Mon Jan  3 10:46:30 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 58544720.257812            0 
            3          4096          4096   gramschmidt           MPI          1 56834782.306641            6 
            3          4096          4096   gramschmidt           MPI          2 59005197.785156            7 
            3          4096          4096   gramschmidt           MPI          3 59584175.880859            0 
            3          4096          4096   gramschmidt           MPI          4 57446367.908203            4 
            3          4096          4096   gramschmidt           MPI          5 57060321.203125            0 
            3          4096          4096   gramschmidt           MPI          6 57456398.554688            3 
            3          4096          4096   gramschmidt           MPI          7 57742615.154297            0 
            3          4096          4096   gramschmidt           MPI          8 56498412.765625            9 
            3          4096          4096   gramschmidt           MPI          9 57881510.996094            0 
# Runtime: 756.583279 s (overhead: 0.000004 %) 10 records
