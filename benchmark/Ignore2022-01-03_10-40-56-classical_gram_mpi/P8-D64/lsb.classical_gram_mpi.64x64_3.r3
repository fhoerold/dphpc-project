# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:42:00 2022
# Execution time and date (local): Mon Jan  3 10:42:00 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 779.525391            0 
            3            64            64   gramschmidt           MPI          1 817.019531            2 
            3            64            64   gramschmidt           MPI          2 795.277344            0 
            3            64            64   gramschmidt           MPI          3 763.937500            0 
            3            64            64   gramschmidt           MPI          4 821.792969            0 
            3            64            64   gramschmidt           MPI          5 896.296875            0 
            3            64            64   gramschmidt           MPI          6 783.628906            0 
            3            64            64   gramschmidt           MPI          7 764.404297            0 
            3            64            64   gramschmidt           MPI          8 775.095703            0 
            3            64            64   gramschmidt           MPI          9 766.980469            0 
# Runtime: 5.006181 s (overhead: 0.000040 %) 10 records
