# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:42:29 2022
# Execution time and date (local): Mon Jan  3 10:42:29 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 8936017.535156            2 
            3          2048          2048   gramschmidt           MPI          1 8949173.960938            8 
            3          2048          2048   gramschmidt           MPI          2 8863270.218750            2 
            3          2048          2048   gramschmidt           MPI          3 8841292.558594            0 
            3          2048          2048   gramschmidt           MPI          4 8913254.427734            3 
            3          2048          2048   gramschmidt           MPI          5 8803066.130859            0 
            3          2048          2048   gramschmidt           MPI          6 8847711.767578            0 
            3          2048          2048   gramschmidt           MPI          7 8836326.318359            0 
            3          2048          2048   gramschmidt           MPI          8 8845319.439453            1 
            3          2048          2048   gramschmidt           MPI          9 8811633.087891            0 
# Runtime: 119.704883 s (overhead: 0.000013 %) 10 records
