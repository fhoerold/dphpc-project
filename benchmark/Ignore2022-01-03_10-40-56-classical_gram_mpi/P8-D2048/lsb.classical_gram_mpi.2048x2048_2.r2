# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:42:29 2022
# Execution time and date (local): Mon Jan  3 10:42:29 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 8935283.820312            0 
            2          2048          2048   gramschmidt           MPI          1 8948726.453125            8 
            2          2048          2048   gramschmidt           MPI          2 8862768.544922            1 
            2          2048          2048   gramschmidt           MPI          3 8840801.212891            0 
            2          2048          2048   gramschmidt           MPI          4 8912566.667969            5 
            2          2048          2048   gramschmidt           MPI          5 8802573.900391            0 
            2          2048          2048   gramschmidt           MPI          6 8847174.162109            0 
            2          2048          2048   gramschmidt           MPI          7 8835862.761719            0 
            2          2048          2048   gramschmidt           MPI          8 8844859.011719            2 
            2          2048          2048   gramschmidt           MPI          9 8810901.802734            0 
# Runtime: 115.699851 s (overhead: 0.000014 %) 10 records
