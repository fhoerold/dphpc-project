# Sysname : Linux
# Nodename: eu-a6-004-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:44:35 2022
# Execution time and date (local): Mon Jan  3 10:44:35 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 349370.595703            0 
            2          1024          1024   gramschmidt           MPI          1 330250.701172            4 
            2          1024          1024   gramschmidt           MPI          2 347435.353516            2 
            2          1024          1024   gramschmidt           MPI          3 362587.580078            0 
            2          1024          1024   gramschmidt           MPI          4 327848.478516            2 
            2          1024          1024   gramschmidt           MPI          5 346792.683594            0 
            2          1024          1024   gramschmidt           MPI          6 327955.873047            0 
            2          1024          1024   gramschmidt           MPI          7 326244.728516            0 
            2          1024          1024   gramschmidt           MPI          8 324030.785156            2 
            2          1024          1024   gramschmidt           MPI          9 342230.628906            2 
# Runtime: 4.417299 s (overhead: 0.000272 %) 10 records
