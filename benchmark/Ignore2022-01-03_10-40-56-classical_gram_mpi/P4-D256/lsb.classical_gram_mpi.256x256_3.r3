# Sysname : Linux
# Nodename: eu-a6-011-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 09:41:30 2022
# Execution time and date (local): Mon Jan  3 10:41:30 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 14025.654297            0 
            3           256           256   gramschmidt           MPI          1 14180.527344            1 
            3           256           256   gramschmidt           MPI          2 13986.332031            0 
            3           256           256   gramschmidt           MPI          3 14119.814453            0 
            3           256           256   gramschmidt           MPI          4 13990.986328            0 
            3           256           256   gramschmidt           MPI          5 13981.769531            0 
            3           256           256   gramschmidt           MPI          6 14319.392578            0 
            3           256           256   gramschmidt           MPI          7 14201.712891            0 
            3           256           256   gramschmidt           MPI          8 14121.005859            1 
            3           256           256   gramschmidt           MPI          9 14069.736328            0 
# Runtime: 0.189844 s (overhead: 0.001053 %) 10 records
