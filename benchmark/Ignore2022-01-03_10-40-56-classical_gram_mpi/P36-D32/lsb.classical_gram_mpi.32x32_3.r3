# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:31:02 2022
# Execution time and date (local): Mon Jan  3 11:31:02 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 1299.593750            0 
            3            32            32   gramschmidt           MPI          1 911.400391            2 
            3            32            32   gramschmidt           MPI          2 904.000000            0 
            3            32            32   gramschmidt           MPI          3 915.740234            0 
            3            32            32   gramschmidt           MPI          4 867.757812            0 
            3            32            32   gramschmidt           MPI          5 881.650391            0 
            3            32            32   gramschmidt           MPI          6 858.466797            0 
            3            32            32   gramschmidt           MPI          7 874.970703            0 
            3            32            32   gramschmidt           MPI          8 846.027344            0 
            3            32            32   gramschmidt           MPI          9 857.738281            0 
# Runtime: 9.018623 s (overhead: 0.000022 %) 10 records
