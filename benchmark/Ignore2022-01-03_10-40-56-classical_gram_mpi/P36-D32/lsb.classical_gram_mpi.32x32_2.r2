# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:31:02 2022
# Execution time and date (local): Mon Jan  3 11:31:02 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 1027.320312            1 
            2            32            32   gramschmidt           MPI          1 928.898438            2 
            2            32            32   gramschmidt           MPI          2 912.949219            0 
            2            32            32   gramschmidt           MPI          3 930.810547            0 
            2            32            32   gramschmidt           MPI          4 875.941406            0 
            2            32            32   gramschmidt           MPI          5 890.376953            0 
            2            32            32   gramschmidt           MPI          6 865.041016            0 
            2            32            32   gramschmidt           MPI          7 882.457031            0 
            2            32            32   gramschmidt           MPI          8 854.804688            0 
            2            32            32   gramschmidt           MPI          9 863.230469            0 
# Runtime: 10.042546 s (overhead: 0.000030 %) 10 records
