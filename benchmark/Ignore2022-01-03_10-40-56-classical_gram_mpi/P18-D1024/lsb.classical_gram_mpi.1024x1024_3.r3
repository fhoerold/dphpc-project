# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:00:13 2022
# Execution time and date (local): Mon Jan  3 11:00:13 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 336442.037109            0 
            3          1024          1024   gramschmidt           MPI          1 334554.546875            5 
            3          1024          1024   gramschmidt           MPI          2 335036.123047            2 
            3          1024          1024   gramschmidt           MPI          3 336116.742188            0 
            3          1024          1024   gramschmidt           MPI          4 338662.585938            2 
            3          1024          1024   gramschmidt           MPI          5 335347.611328            0 
            3          1024          1024   gramschmidt           MPI          6 336582.437500            0 
            3          1024          1024   gramschmidt           MPI          7 336311.265625            0 
            3          1024          1024   gramschmidt           MPI          8 336236.640625            2 
            3          1024          1024   gramschmidt           MPI          9 364546.625000            0 
# Runtime: 12.433828 s (overhead: 0.000088 %) 10 records
