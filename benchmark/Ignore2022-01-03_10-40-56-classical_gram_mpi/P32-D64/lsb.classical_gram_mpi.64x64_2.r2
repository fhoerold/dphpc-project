# Sysname : Linux
# Nodename: eu-a6-005-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:18:00 2022
# Execution time and date (local): Mon Jan  3 11:18:00 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 1923.839844            0 
            2            64            64   gramschmidt           MPI          1 1889.740234            3 
            2            64            64   gramschmidt           MPI          2 1870.605469            0 
            2            64            64   gramschmidt           MPI          3 1878.818359            0 
            2            64            64   gramschmidt           MPI          4 1831.300781            0 
            2            64            64   gramschmidt           MPI          5 1858.160156            0 
            2            64            64   gramschmidt           MPI          6 1844.460938            0 
            2            64            64   gramschmidt           MPI          7 1787.261719            0 
            2            64            64   gramschmidt           MPI          8 1804.109375            0 
            2            64            64   gramschmidt           MPI          9 1791.826172            0 
# Runtime: 10.057988 s (overhead: 0.000030 %) 10 records
