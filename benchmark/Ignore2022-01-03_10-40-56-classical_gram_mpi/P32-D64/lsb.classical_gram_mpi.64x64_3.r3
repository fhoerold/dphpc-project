# Sysname : Linux
# Nodename: eu-a6-005-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:18:00 2022
# Execution time and date (local): Mon Jan  3 11:18:00 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 1876.531250            0 
            3            64            64   gramschmidt           MPI          1 1870.203125            3 
            3            64            64   gramschmidt           MPI          2 1854.693359            0 
            3            64            64   gramschmidt           MPI          3 1855.369141            0 
            3            64            64   gramschmidt           MPI          4 1817.605469            0 
            3            64            64   gramschmidt           MPI          5 1828.236328            0 
            3            64            64   gramschmidt           MPI          6 1828.644531            0 
            3            64            64   gramschmidt           MPI          7 1772.773438            0 
            3            64            64   gramschmidt           MPI          8 1787.839844            0 
            3            64            64   gramschmidt           MPI          9 1774.113281            0 
# Runtime: 3.027651 s (overhead: 0.000099 %) 10 records
