# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:34:13 2022
# Execution time and date (local): Mon Jan  3 11:34:13 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 30917472.591797            0 
            2          4096          4096   gramschmidt           MPI          1 30565155.250000            6 
            2          4096          4096   gramschmidt           MPI          2 30940251.867188           11 
            2          4096          4096   gramschmidt           MPI          3 30330440.171875            0 
            2          4096          4096   gramschmidt           MPI          4 30310275.998047            7 
            2          4096          4096   gramschmidt           MPI          5 30409351.531250            0 
            2          4096          4096   gramschmidt           MPI          6 30793774.929688            0 
            2          4096          4096   gramschmidt           MPI          7 30539503.802734            0 
            2          4096          4096   gramschmidt           MPI          8 30404311.162109           11 
            2          4096          4096   gramschmidt           MPI          9 30259391.623047            0 
# Runtime: 403.678641 s (overhead: 0.000009 %) 10 records
