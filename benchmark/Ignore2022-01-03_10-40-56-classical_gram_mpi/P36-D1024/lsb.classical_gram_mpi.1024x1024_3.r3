# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:32:48 2022
# Execution time and date (local): Mon Jan  3 11:32:48 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 480576.464844            0 
            3          1024          1024   gramschmidt           MPI          1 319218.666016            6 
            3          1024          1024   gramschmidt           MPI          2 316853.769531            1 
            3          1024          1024   gramschmidt           MPI          3 317039.697266            0 
            3          1024          1024   gramschmidt           MPI          4 318295.367188            2 
            3          1024          1024   gramschmidt           MPI          5 318226.062500            0 
            3          1024          1024   gramschmidt           MPI          6 316674.392578            0 
            3          1024          1024   gramschmidt           MPI          7 317571.808594            0 
            3          1024          1024   gramschmidt           MPI          8 317163.792969            2 
            3          1024          1024   gramschmidt           MPI          9 476559.132812            0 
# Runtime: 21.361894 s (overhead: 0.000051 %) 10 records
