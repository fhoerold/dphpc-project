# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan  3 10:32:48 2022
# Execution time and date (local): Mon Jan  3 11:32:48 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 481407.697266            0 
            2          1024          1024   gramschmidt           MPI          1 319786.406250            4 
            2          1024          1024   gramschmidt           MPI          2 317396.519531            1 
            2          1024          1024   gramschmidt           MPI          3 317580.021484            0 
            2          1024          1024   gramschmidt           MPI          4 318851.091797            2 
            2          1024          1024   gramschmidt           MPI          5 318774.410156            0 
            2          1024          1024   gramschmidt           MPI          6 317225.626953            0 
            2          1024          1024   gramschmidt           MPI          7 318140.505859            0 
            2          1024          1024   gramschmidt           MPI          8 317729.902344            2 
            2          1024          1024   gramschmidt           MPI          9 477420.087891            0 
# Runtime: 16.452404 s (overhead: 0.000055 %) 10 records
