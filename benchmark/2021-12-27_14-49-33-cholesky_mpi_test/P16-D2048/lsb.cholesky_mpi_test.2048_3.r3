# Sysname : Linux
# Nodename: eu-a6-006-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 13:53:42 2021
# Execution time and date (local): Mon Dec 27 14:53:42 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 306564.468750            0 
            3          2048           MPI          1 306950.478516            2 
            3          2048           MPI          2 305189.554688            1 
            3          2048           MPI          3 305876.732422            0 
            3          2048           MPI          4 306076.929688            0 
            3          2048           MPI          5 306704.312500            0 
            3          2048           MPI          6 306053.980469            0 
            3          2048           MPI          7 306868.560547            0 
            3          2048           MPI          8 306074.562500            1 
            3          2048           MPI          9 306445.341797            0 
# Runtime: 3.684691 s (overhead: 0.000109 %) 10 records
