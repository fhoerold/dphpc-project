# Sysname : Linux
# Nodename: eu-a6-006-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 13:53:42 2021
# Execution time and date (local): Mon Dec 27 14:53:42 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 306457.750000            0 
            2          2048           MPI          1 306853.400391            1 
            2          2048           MPI          2 305084.455078            1 
            2          2048           MPI          3 305771.933594            0 
            2          2048           MPI          4 305973.408203            1 
            2          2048           MPI          5 306598.423828            0 
            2          2048           MPI          6 305948.863281            0 
            2          2048           MPI          7 306757.714844            0 
            2          2048           MPI          8 305970.779297            1 
            2          2048           MPI          9 306342.052734            0 
# Runtime: 11.682613 s (overhead: 0.000034 %) 10 records
