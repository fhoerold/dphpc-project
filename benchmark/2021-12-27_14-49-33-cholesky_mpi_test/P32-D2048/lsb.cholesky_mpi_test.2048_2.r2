# Sysname : Linux
# Nodename: eu-a6-002-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 16:06:39 2021
# Execution time and date (local): Mon Dec 27 17:06:39 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 396027.443359            0 
            2          2048           MPI          1 395568.466797            4 
            2          2048           MPI          2 395136.087891            1 
            2          2048           MPI          3 404175.085938            0 
            2          2048           MPI          4 402712.615234            4 
            2          2048           MPI          5 396479.156250            0 
            2          2048           MPI          6 398321.142578            0 
            2          2048           MPI          7 397090.287109            0 
            2          2048           MPI          8 396119.515625            4 
            2          2048           MPI          9 396955.564453            0 
# Runtime: 20.808799 s (overhead: 0.000062 %) 10 records
