# Sysname : Linux
# Nodename: eu-a6-002-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 16:06:39 2021
# Execution time and date (local): Mon Dec 27 17:06:39 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 395998.529297            0 
            3          2048           MPI          1 395548.630859            3 
            3          2048           MPI          2 395116.076172            0 
            3          2048           MPI          3 404154.603516            0 
            3          2048           MPI          4 402692.386719            0 
            3          2048           MPI          5 396463.746094            0 
            3          2048           MPI          6 398303.853516            0 
            3          2048           MPI          7 397070.158203            0 
            3          2048           MPI          8 396103.037109            3 
            3          2048           MPI          9 396928.744141            0 
# Runtime: 17.805106 s (overhead: 0.000034 %) 10 records
