# Sysname : Linux
# Nodename: eu-a6-002-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 18:03:31 2021
# Execution time and date (local): Mon Dec 27 19:03:31 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 8424985.472656            0 
            3          8192           MPI          1 8426315.804688            7 
            3          8192           MPI          2 8339025.806641            5 
            3          8192           MPI          3 8322381.916016            0 
            3          8192           MPI          4 8364517.121094            5 
            3          8192           MPI          5 8403553.593750            0 
            3          8192           MPI          6 8401754.009766            0 
            3          8192           MPI          7 8315860.031250            0 
            3          8192           MPI          8 8307345.439453            7 
            3          8192           MPI          9 8357909.082031            0 
# Runtime: 108.487897 s (overhead: 0.000022 %) 10 records
