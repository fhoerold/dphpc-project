# Sysname : Linux
# Nodename: eu-a6-002-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 18:03:31 2021
# Execution time and date (local): Mon Dec 27 19:03:31 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 8424945.410156            0 
            2          8192           MPI          1 8426279.539062            8 
            2          8192           MPI          2 8338982.384766            0 
            2          8192           MPI          3 8322340.531250            0 
            2          8192           MPI          4 8364477.685547            4 
            2          8192           MPI          5 8403511.615234            0 
            2          8192           MPI          6 8401707.185547            0 
            2          8192           MPI          7 8315820.175781            0 
            2          8192           MPI          8 8307303.683594            9 
            2          8192           MPI          9 8357868.132812            0 
# Runtime: 104.487939 s (overhead: 0.000020 %) 10 records
