# Sysname : Linux
# Nodename: eu-a6-002-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 15:55:14 2021
# Execution time and date (local): Mon Dec 27 16:55:14 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 108641.990234            2 
            3          1024           MPI          1 108246.621094            4 
            3          1024           MPI          2 108654.876953            0 
            3          1024           MPI          3 107906.925781            0 
            3          1024           MPI          4 113412.414062            2 
            3          1024           MPI          5 107646.500000            0 
            3          1024           MPI          6 108007.824219            0 
            3          1024           MPI          7 107861.425781            0 
            3          1024           MPI          8 107765.337891            2 
            3          1024           MPI          9 108473.710938            0 
# Runtime: 19.319791 s (overhead: 0.000052 %) 10 records
