# Sysname : Linux
# Nodename: eu-a6-002-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 15:55:14 2021
# Execution time and date (local): Mon Dec 27 16:55:14 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 108722.875000            0 
            2          1024           MPI          1 108250.320312            3 
            2          1024           MPI          2 108651.156250            3 
            2          1024           MPI          3 107906.205078            0 
            2          1024           MPI          4 113411.746094            3 
            2          1024           MPI          5 107646.011719            0 
            2          1024           MPI          6 108007.210938            0 
            2          1024           MPI          7 107860.935547            0 
            2          1024           MPI          8 107764.878906            0 
            2          1024           MPI          9 108476.988281            0 
# Runtime: 19.317611 s (overhead: 0.000047 %) 10 records
