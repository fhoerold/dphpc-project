# Sysname : Linux
# Nodename: eu-a6-006-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 13:57:49 2021
# Execution time and date (local): Mon Dec 27 14:57:49 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 3584123.998047            0 
            3          4096           MPI          1 3587841.736328            1 
            3          4096           MPI          2 3583479.691406            1 
            3          4096           MPI          3 3589556.656250            0 
            3          4096           MPI          4 3580258.072266            3 
            3          4096           MPI          5 3587992.962891            0 
            3          4096           MPI          6 3585411.505859            0 
            3          4096           MPI          7 3585950.119141            0 
            3          4096           MPI          8 3587622.373047            5 
            3          4096           MPI          9 3584663.902344            0 
# Runtime: 43.027313 s (overhead: 0.000023 %) 10 records
