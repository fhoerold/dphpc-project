# Sysname : Linux
# Nodename: eu-a6-006-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 13:57:49 2021
# Execution time and date (local): Mon Dec 27 14:57:49 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 3579263.503906            0 
            2          4096           MPI          1 3582943.419922            1 
            2          4096           MPI          2 3578289.046875            3 
            2          4096           MPI          3 3584355.812500            0 
            2          4096           MPI          4 3575065.525391            1 
            2          4096           MPI          5 3582792.820312            0 
            2          4096           MPI          6 3580214.869141            0 
            2          4096           MPI          7 3580752.486328            0 
            2          4096           MPI          8 3582423.335938            7 
            2          4096           MPI          9 3579467.708984            0 
# Runtime: 47.953690 s (overhead: 0.000025 %) 10 records
