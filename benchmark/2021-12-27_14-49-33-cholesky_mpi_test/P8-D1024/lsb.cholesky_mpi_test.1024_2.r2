# Sysname : Linux
# Nodename: eu-a6-006-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 13:50:19 2021
# Execution time and date (local): Mon Dec 27 14:50:19 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 59887.279297            1 
            2          1024           MPI          1 59479.574219            3 
            2          1024           MPI          2 59480.818359            0 
            2          1024           MPI          3 59490.244141            0 
            2          1024           MPI          4 59355.890625            1 
            2          1024           MPI          5 59279.894531            0 
            2          1024           MPI          6 59612.039062            0 
            2          1024           MPI          7 59491.685547            0 
            2          1024           MPI          8 59482.017578            0 
            2          1024           MPI          9 59124.390625            0 
# Runtime: 25.707379 s (overhead: 0.000019 %) 10 records
