# Sysname : Linux
# Nodename: eu-a6-006-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 13:50:19 2021
# Execution time and date (local): Mon Dec 27 14:50:19 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 59628.416016            0 
            3          1024           MPI          1 59433.142578            2 
            3          1024           MPI          2 59487.679688            0 
            3          1024           MPI          3 59504.630859            0 
            3          1024           MPI          4 59360.591797            0 
            3          1024           MPI          5 59284.048828            0 
            3          1024           MPI          6 59617.808594            0 
            3          1024           MPI          7 59495.173828            0 
            3          1024           MPI          8 59485.990234            0 
            3          1024           MPI          9 59128.369141            0 
# Runtime: 25.707085 s (overhead: 0.000008 %) 10 records
