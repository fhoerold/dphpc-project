# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 13:50:15 2021
# Execution time and date (local): Mon Dec 27 14:50:15 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 9427.087891            1 
            2           512           MPI          1 9017.634766            5 
            2           512           MPI          2 8990.009766            0 
            2           512           MPI          3 8974.513672            0 
            2           512           MPI          4 8975.802734            2 
            2           512           MPI          5 8971.701172            0 
            2           512           MPI          6 8930.712891            0 
            2           512           MPI          7 9019.630859            0 
            2           512           MPI          8 8966.986328            0 
            2           512           MPI          9 8992.912109            0 
# Runtime: 6.114197 s (overhead: 0.000131 %) 10 records
