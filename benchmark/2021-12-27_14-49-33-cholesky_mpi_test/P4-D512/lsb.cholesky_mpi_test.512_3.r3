# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 13:50:15 2021
# Execution time and date (local): Mon Dec 27 14:50:15 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 9063.921875            0 
            3           512           MPI          1 9017.945312            0 
            3           512           MPI          2 8990.490234            0 
            3           512           MPI          3 8974.841797            0 
            3           512           MPI          4 8976.091797            0 
            3           512           MPI          5 8971.744141            0 
            3           512           MPI          6 8931.093750            0 
            3           512           MPI          7 9019.931641            0 
            3           512           MPI          8 8967.599609            0 
            3           512           MPI          9 8992.896484            0 
# Runtime: 0.110564 s (overhead: 0.000000 %) 10 records
