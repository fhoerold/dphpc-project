# Sysname : Linux
# Nodename: eu-a6-006-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 13:52:43 2021
# Execution time and date (local): Mon Dec 27 14:52:43 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 26984.347656            0 
            2           512           MPI          1 26894.355469            3 
            2           512           MPI          2 26858.128906            0 
            2           512           MPI          3 26742.232422            0 
            2           512           MPI          4 26852.013672            3 
            2           512           MPI          5 26835.414062            0 
            2           512           MPI          6 27162.589844            0 
            2           512           MPI          7 27193.550781            0 
            2           512           MPI          8 27034.644531            0 
            2           512           MPI          9 27192.097656            0 
# Runtime: 4.332104 s (overhead: 0.000139 %) 10 records
