# Sysname : Linux
# Nodename: eu-a6-006-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 13:52:43 2021
# Execution time and date (local): Mon Dec 27 14:52:43 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 26941.685547            0 
            3           512           MPI          1 26897.300781            0 
            3           512           MPI          2 26861.080078            0 
            3           512           MPI          3 26745.111328            0 
            3           512           MPI          4 26853.507812            0 
            3           512           MPI          5 26838.892578            0 
            3           512           MPI          6 27165.685547            0 
            3           512           MPI          7 27196.406250            0 
            3           512           MPI          8 27036.982422            0 
            3           512           MPI          9 27200.748047            0 
# Runtime: 0.330690 s (overhead: 0.000000 %) 10 records
