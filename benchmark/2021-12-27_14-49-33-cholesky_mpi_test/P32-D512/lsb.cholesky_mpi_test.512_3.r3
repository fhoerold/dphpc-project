# Sysname : Linux
# Nodename: eu-a6-002-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 15:54:51 2021
# Execution time and date (local): Mon Dec 27 16:54:51 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 35834.169922            1 
            3           512           MPI          1 35717.625000            3 
            3           512           MPI          2 35306.339844            0 
            3           512           MPI          3 35177.201172            0 
            3           512           MPI          4 35377.181641            2 
            3           512           MPI          5 35235.261719            0 
            3           512           MPI          6 35356.962891            0 
            3           512           MPI          7 35336.619141            0 
            3           512           MPI          8 35329.712891            2 
            3           512           MPI          9 36089.982422            0 
# Runtime: 9.455748 s (overhead: 0.000085 %) 10 records
