# Sysname : Linux
# Nodename: eu-a6-002-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 15:54:51 2021
# Execution time and date (local): Mon Dec 27 16:54:51 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 35895.367188            1 
            2           512           MPI          1 35684.304688            3 
            2           512           MPI          2 35281.603516            0 
            2           512           MPI          3 35155.599609            0 
            2           512           MPI          4 35355.996094            0 
            2           512           MPI          5 35208.064453            0 
            2           512           MPI          6 35333.212891            0 
            2           512           MPI          7 35310.757812            0 
            2           512           MPI          8 35310.841797            2 
            2           512           MPI          9 36065.542969            0 
# Runtime: 10.434811 s (overhead: 0.000057 %) 10 records
