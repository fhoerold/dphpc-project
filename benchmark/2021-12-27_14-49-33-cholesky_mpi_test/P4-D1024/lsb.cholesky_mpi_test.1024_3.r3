# Sysname : Linux
# Nodename: eu-a6-006-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 13:50:18 2021
# Execution time and date (local): Mon Dec 27 14:50:18 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 65392.949219            0 
            3          1024           MPI          1 65504.773438            1 
            3          1024           MPI          2 65301.748047            0 
            3          1024           MPI          3 65440.650391            0 
            3          1024           MPI          4 65204.453125            1 
            3          1024           MPI          5 64850.353516            0 
            3          1024           MPI          6 65093.107422            0 
            3          1024           MPI          7 64982.121094            0 
            3          1024           MPI          8 65073.181641            1 
            3          1024           MPI          9 65075.691406            0 
# Runtime: 0.785564 s (overhead: 0.000382 %) 10 records
