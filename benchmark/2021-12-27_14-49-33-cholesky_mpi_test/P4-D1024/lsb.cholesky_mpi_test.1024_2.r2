# Sysname : Linux
# Nodename: eu-a6-006-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 13:50:18 2021
# Execution time and date (local): Mon Dec 27 14:50:18 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 65737.062500            0 
            2          1024           MPI          1 65476.314453            4 
            2          1024           MPI          2 65266.089844            0 
            2          1024           MPI          3 65412.671875            0 
            2          1024           MPI          4 65176.296875            0 
            2          1024           MPI          5 64818.750000            0 
            2          1024           MPI          6 65064.796875            0 
            2          1024           MPI          7 64954.507812            0 
            2          1024           MPI          8 65045.550781            0 
            2          1024           MPI          9 65047.769531            0 
# Runtime: 2.779196 s (overhead: 0.000144 %) 10 records
