# Sysname : Linux
# Nodename: eu-a6-012-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 15:52:41 2021
# Execution time and date (local): Mon Dec 27 16:52:41 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 8251378.082031            0 
            2          8192           MPI          1 8268548.128906            1 
            2          8192           MPI          2 8246781.642578            1 
            2          8192           MPI          3 8340697.339844            0 
            2          8192           MPI          4 8267339.406250            5 
            2          8192           MPI          5 8268642.369141            0 
            2          8192           MPI          6 8267081.593750            0 
            2          8192           MPI          7 8268594.503906            0 
            2          8192           MPI          8 8279561.445312            1 
            2          8192           MPI          9 8251611.369141            0 
# Runtime: 102.238671 s (overhead: 0.000008 %) 10 records
