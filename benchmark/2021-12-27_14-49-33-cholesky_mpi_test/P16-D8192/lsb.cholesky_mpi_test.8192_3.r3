# Sysname : Linux
# Nodename: eu-a6-012-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 15:52:41 2021
# Execution time and date (local): Mon Dec 27 16:52:41 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 8251600.976562            0 
            3          8192           MPI          1 8268749.988281            1 
            3          8192           MPI          2 8247003.341797            1 
            3          8192           MPI          3 8340919.951172            0 
            3          8192           MPI          4 8267561.830078            8 
            3          8192           MPI          5 8268863.931641            0 
            3          8192           MPI          6 8267302.822266            0 
            3          8192           MPI          7 8268810.519531            0 
            3          8192           MPI          8 8279783.644531            6 
            3          8192           MPI          9 8251833.027344            0 
# Runtime: 100.240977 s (overhead: 0.000016 %) 10 records
