# Sysname : Linux
# Nodename: eu-a6-002-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 14:03:10 2021
# Execution time and date (local): Mon Dec 27 15:03:10 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 1228369.441406            0 
            3          4096           MPI          1 1226773.185547            1 
            3          4096           MPI          2 1226112.023438            0 
            3          4096           MPI          3 1225111.648438            0 
            3          4096           MPI          4 1226868.322266            0 
            3          4096           MPI          5 1224543.529297            0 
            3          4096           MPI          6 1227944.986328            0 
            3          4096           MPI          7 1228214.013672            0 
            3          4096           MPI          8 1227436.585938            0 
            3          4096           MPI          9 1228506.636719            0 
# Runtime: 17.738561 s (overhead: 0.000006 %) 10 records
