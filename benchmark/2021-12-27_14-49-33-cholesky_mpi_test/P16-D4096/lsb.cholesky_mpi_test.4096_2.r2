# Sysname : Linux
# Nodename: eu-a6-002-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 14:03:10 2021
# Execution time and date (local): Mon Dec 27 15:03:10 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 1228362.837891            0 
            2          4096           MPI          1 1226759.658203            1 
            2          4096           MPI          2 1226099.082031            0 
            2          4096           MPI          3 1225098.355469            0 
            2          4096           MPI          4 1226855.277344            1 
            2          4096           MPI          5 1224528.423828            0 
            2          4096           MPI          6 1227931.707031            0 
            2          4096           MPI          7 1228201.820312            0 
            2          4096           MPI          8 1227423.900391            1 
            2          4096           MPI          9 1228493.236328            0 
# Runtime: 17.740131 s (overhead: 0.000017 %) 10 records
