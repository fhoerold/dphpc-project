# Sysname : Linux
# Nodename: eu-a6-006-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 13:51:01 2021
# Execution time and date (local): Mon Dec 27 14:51:01 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 459068.794922            0 
            3          2048           MPI          1 465517.376953            1 
            3          2048           MPI          2 474731.074219            1 
            3          2048           MPI          3 472282.054688            0 
            3          2048           MPI          4 471506.871094            1 
            3          2048           MPI          5 474502.736328            0 
            3          2048           MPI          6 474119.761719            0 
            3          2048           MPI          7 478019.601562            0 
            3          2048           MPI          8 474352.591797            1 
            3          2048           MPI          9 473508.525391            0 
# Runtime: 5.638777 s (overhead: 0.000071 %) 10 records
