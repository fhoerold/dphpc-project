# Sysname : Linux
# Nodename: eu-a6-006-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 13:51:01 2021
# Execution time and date (local): Mon Dec 27 14:51:01 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 459294.990234            0 
            2          2048           MPI          1 466149.044922            1 
            2          2048           MPI          2 474959.556641            0 
            2          2048           MPI          3 472509.474609            0 
            2          2048           MPI          4 471733.792969            0 
            2          2048           MPI          5 474730.917969            0 
            2          2048           MPI          6 474348.175781            0 
            2          2048           MPI          7 478248.509766            0 
            2          2048           MPI          8 474580.388672            0 
            2          2048           MPI          9 473737.181641            0 
# Runtime: 8.655422 s (overhead: 0.000012 %) 10 records
