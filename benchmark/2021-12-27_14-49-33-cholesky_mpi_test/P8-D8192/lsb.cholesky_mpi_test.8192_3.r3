# Sysname : Linux
# Nodename: eu-a6-002-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 15:25:21 2021
# Execution time and date (local): Mon Dec 27 16:25:21 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 15703429.628906            0 
            3          8192           MPI          1 15666152.750000            5 
            3          8192           MPI          2 15650216.771484            0 
            3          8192           MPI          3 15728108.683594            0 
            3          8192           MPI          4 15729292.853516            5 
            3          8192           MPI          5 15675642.080078            0 
            3          8192           MPI          6 15683686.435547            0 
            3          8192           MPI          7 15772691.167969            0 
            3          8192           MPI          8 15639796.427734            5 
            3          8192           MPI          9 14893255.044922            0 
# Runtime: 191.237346 s (overhead: 0.000008 %) 10 records
