# Sysname : Linux
# Nodename: eu-a6-002-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 15:25:21 2021
# Execution time and date (local): Mon Dec 27 16:25:21 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 15703327.212891            0 
            2          8192           MPI          1 15666020.994141            5 
            2          8192           MPI          2 15650085.833984            5 
            2          8192           MPI          3 15727976.458984            0 
            2          8192           MPI          4 15729161.232422            5 
            2          8192           MPI          5 15675510.222656            0 
            2          8192           MPI          6 15683554.962891            0 
            2          8192           MPI          7 15772559.392578            0 
            2          8192           MPI          8 15639660.951172            8 
            2          8192           MPI          9 14893129.757812            0 
# Runtime: 191.235546 s (overhead: 0.000012 %) 10 records
