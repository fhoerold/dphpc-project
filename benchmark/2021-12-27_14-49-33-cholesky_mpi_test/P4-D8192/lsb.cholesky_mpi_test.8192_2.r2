# Sysname : Linux
# Nodename: eu-a6-006-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 15:29:23 2021
# Execution time and date (local): Mon Dec 27 16:29:23 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 29544788.970703            1 
            2          8192           MPI          1 29499230.960938            6 
            2          8192           MPI          2 29521486.089844            3 
            2          8192           MPI          3 29542082.759766            0 
            2          8192           MPI          4 29453068.433594            6 
            2          8192           MPI          5 29475900.654297            0 
            2          8192           MPI          6 29520892.384766            1 
            2          8192           MPI          7 29682752.724609            0 
            2          8192           MPI          8 29714445.392578            5 
            2          8192           MPI          9 29481576.460938            0 
# Runtime: 357.661015 s (overhead: 0.000006 %) 10 records
