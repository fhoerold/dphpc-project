# Sysname : Linux
# Nodename: eu-a6-006-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 15:29:23 2021
# Execution time and date (local): Mon Dec 27 16:29:23 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 29532246.392578            1 
            3          8192           MPI          1 29487063.789062            8 
            3          8192           MPI          2 29509309.109375            4 
            3          8192           MPI          3 29529897.048828            0 
            3          8192           MPI          4 29440919.896484            3 
            3          8192           MPI          5 29463741.769531            0 
            3          8192           MPI          6 29508714.892578            0 
            3          8192           MPI          7 29670508.554688            0 
            3          8192           MPI          8 29702189.337891            3 
            3          8192           MPI          9 29469416.146484            0 
# Runtime: 357.510164 s (overhead: 0.000005 %) 10 records
