# Sysname : Linux
# Nodename: eu-a6-002-20
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 13:51:29 2021
# Execution time and date (local): Mon Dec 27 14:51:29 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 257496.265625            0 
            3          2048           MPI          1 257252.675781            1 
            3          2048           MPI          2 259014.447266            0 
            3          2048           MPI          3 263905.982422            0 
            3          2048           MPI          4 258016.537109            0 
            3          2048           MPI          5 256645.021484            0 
            3          2048           MPI          6 257216.771484            0 
            3          2048           MPI          7 257096.988281            0 
            3          2048           MPI          8 257333.375000            0 
            3          2048           MPI          9 257185.064453            0 
# Runtime: 20.091589 s (overhead: 0.000005 %) 10 records
