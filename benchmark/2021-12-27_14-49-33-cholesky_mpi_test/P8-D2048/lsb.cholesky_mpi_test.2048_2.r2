# Sysname : Linux
# Nodename: eu-a6-002-20
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 13:51:29 2021
# Execution time and date (local): Mon Dec 27 14:51:29 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 257539.148438            0 
            2          2048           MPI          1 257250.273438            1 
            2          2048           MPI          2 259012.833984            0 
            2          2048           MPI          3 263628.962891            0 
            2          2048           MPI          4 258014.636719            0 
            2          2048           MPI          5 256643.462891            0 
            2          2048           MPI          6 257214.267578            0 
            2          2048           MPI          7 257095.132812            0 
            2          2048           MPI          8 257331.787109            1 
            2          2048           MPI          9 257179.085938            0 
# Runtime: 20.092664 s (overhead: 0.000010 %) 10 records
