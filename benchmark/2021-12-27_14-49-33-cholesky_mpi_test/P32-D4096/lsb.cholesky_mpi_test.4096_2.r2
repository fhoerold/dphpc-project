# Sysname : Linux
# Nodename: eu-a6-002-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 16:05:03 2021
# Execution time and date (local): Mon Dec 27 17:05:03 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 1699941.152344            5 
            2          4096           MPI          1 1705944.490234            4 
            2          4096           MPI          2 1699733.720703            0 
            2          4096           MPI          3 1709540.003906            0 
            2          4096           MPI          4 1693563.203125            8 
            2          4096           MPI          5 1688293.357422            0 
            2          4096           MPI          6 1688859.097656            0 
            2          4096           MPI          7 1693140.083984            0 
            2          4096           MPI          8 1692839.482422            2 
            2          4096           MPI          9 1696793.398438            0 
# Runtime: 27.427083 s (overhead: 0.000069 %) 10 records
