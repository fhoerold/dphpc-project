# Sysname : Linux
# Nodename: eu-a6-002-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 16:05:03 2021
# Execution time and date (local): Mon Dec 27 17:05:03 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 1699942.580078            0 
            3          4096           MPI          1 1705951.865234            4 
            3          4096           MPI          2 1699732.666016            0 
            3          4096           MPI          3 1709547.037109            0 
            3          4096           MPI          4 1693570.242188            1 
            3          4096           MPI          5 1688301.330078            0 
            3          4096           MPI          6 1688871.904297            0 
            3          4096           MPI          7 1693145.982422            0 
            3          4096           MPI          8 1692847.361328            1 
            3          4096           MPI          9 1696799.611328            0 
# Runtime: 29.428896 s (overhead: 0.000020 %) 10 records
