# Sysname : Linux
# Nodename: eu-a6-002-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 13:59:39 2021
# Execution time and date (local): Mon Dec 27 14:59:39 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 1893274.582031            0 
            2          4096           MPI          1 1891586.988281           10 
            2          4096           MPI          2 1918167.111328            1 
            2          4096           MPI          3 1893049.365234            0 
            2          4096           MPI          4 1892406.417969            7 
            2          4096           MPI          5 1891641.652344            0 
            2          4096           MPI          6 1892034.667969            0 
            2          4096           MPI          7 1889699.710938            0 
            2          4096           MPI          8 1892035.751953            1 
            2          4096           MPI          9 1890388.246094            0 
# Runtime: 22.734212 s (overhead: 0.000084 %) 10 records
