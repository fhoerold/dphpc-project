# Sysname : Linux
# Nodename: eu-a6-002-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 13:59:39 2021
# Execution time and date (local): Mon Dec 27 14:59:39 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 1893267.794922            0 
            3          4096           MPI          1 1891584.677734            4 
            3          4096           MPI          2 1918164.537109            1 
            3          4096           MPI          3 1893046.109375            0 
            3          4096           MPI          4 1892397.808594            1 
            3          4096           MPI          5 1891641.849609            0 
            3          4096           MPI          6 1892032.330078            0 
            3          4096           MPI          7 1889697.505859            0 
            3          4096           MPI          8 1892032.667969            1 
            3          4096           MPI          9 1890385.078125            0 
# Runtime: 30.734591 s (overhead: 0.000023 %) 10 records
