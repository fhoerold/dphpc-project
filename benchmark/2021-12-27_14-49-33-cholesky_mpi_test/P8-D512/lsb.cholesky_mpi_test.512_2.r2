# Sysname : Linux
# Nodename: eu-a6-006-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 13:50:15 2021
# Execution time and date (local): Mon Dec 27 14:50:15 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 7481.429688            0 
            2           512           MPI          1 7729.775391            0 
            2           512           MPI          2 7378.861328            0 
            2           512           MPI          3 7397.123047            0 
            2           512           MPI          4 7376.917969            0 
            2           512           MPI          5 7386.482422            0 
            2           512           MPI          6 7394.826172            0 
            2           512           MPI          7 7372.218750            0 
            2           512           MPI          8 7384.324219            0 
            2           512           MPI          9 7416.437500            0 
# Runtime: 0.097833 s (overhead: 0.000000 %) 10 records
