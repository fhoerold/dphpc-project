# Sysname : Linux
# Nodename: eu-a6-006-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 13:50:15 2021
# Execution time and date (local): Mon Dec 27 14:50:15 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 7489.806641            0 
            3           512           MPI          1 7421.226562            0 
            3           512           MPI          2 7379.236328            0 
            3           512           MPI          3 7401.568359            0 
            3           512           MPI          4 7377.414062            0 
            3           512           MPI          5 7386.638672            0 
            3           512           MPI          6 7395.314453            0 
            3           512           MPI          7 7372.857422            0 
            3           512           MPI          8 7384.945312            0 
            3           512           MPI          9 7417.089844            0 
# Runtime: 0.092504 s (overhead: 0.000000 %) 10 records
