# Sysname : Linux
# Nodename: eu-a6-002-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 13:54:17 2021
# Execution time and date (local): Mon Dec 27 14:54:17 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 39224.673828            1 
            3          1024           MPI          1 38766.232422            3 
            3          1024           MPI          2 38789.921875            0 
            3          1024           MPI          3 39025.291016            0 
            3          1024           MPI          4 38708.945312            3 
            3          1024           MPI          5 38961.810547            0 
            3          1024           MPI          6 38717.406250            0 
            3          1024           MPI          7 38844.558594            0 
            3          1024           MPI          8 38976.681641            2 
            3          1024           MPI          9 38703.414062            0 
# Runtime: 6.477825 s (overhead: 0.000139 %) 10 records
