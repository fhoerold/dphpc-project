# Sysname : Linux
# Nodename: eu-a6-002-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Dec 27 13:54:17 2021
# Execution time and date (local): Mon Dec 27 14:54:17 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 39454.554688            0 
            2          1024           MPI          1 38760.263672            3 
            2          1024           MPI          2 38783.751953            0 
            2          1024           MPI          3 39018.882812            0 
            2          1024           MPI          4 38703.099609            0 
            2          1024           MPI          5 38955.599609            0 
            2          1024           MPI          6 38711.177734            0 
            2          1024           MPI          7 38838.636719            0 
            2          1024           MPI          8 38970.136719            3 
            2          1024           MPI          9 38701.914062            0 
# Runtime: 7.476010 s (overhead: 0.000080 %) 10 records
