# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:36:36 2022
# Execution time and date (local): Tue Jan 11 02:36:36 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 8032321.867188            0 
          512           512   gramschmidt          1 34832.259766            4 
          512           512   gramschmidt          2 36219.859375            1 
          512           512   gramschmidt          3 35208.304688            0 
          512           512   gramschmidt          4 34205.835938            3 
          512           512   gramschmidt          5 33169.359375            0 
          512           512   gramschmidt          6 33428.966797            0 
          512           512   gramschmidt          7 32911.218750            0 
          512           512   gramschmidt          8 32830.486328            0 
          512           512   gramschmidt          9 33778.580078            0 
# Runtime: 8.338957 s (overhead: 0.000096 %) 10 records
