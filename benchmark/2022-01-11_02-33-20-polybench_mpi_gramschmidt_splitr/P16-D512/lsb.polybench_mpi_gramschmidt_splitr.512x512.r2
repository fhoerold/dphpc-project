# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:36:36 2022
# Execution time and date (local): Tue Jan 11 02:36:36 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 6031639.769531            0 
          512           512   gramschmidt          1 34838.974609            4 
          512           512   gramschmidt          2 36213.873047            1 
          512           512   gramschmidt          3 35212.611328            0 
          512           512   gramschmidt          4 34200.611328            1 
          512           512   gramschmidt          5 33175.107422            0 
          512           512   gramschmidt          6 33420.632812            0 
          512           512   gramschmidt          7 32914.968750            0 
          512           512   gramschmidt          8 32828.574219            1 
          512           512   gramschmidt          9 33777.376953            0 
# Runtime: 6.338265 s (overhead: 0.000110 %) 10 records
