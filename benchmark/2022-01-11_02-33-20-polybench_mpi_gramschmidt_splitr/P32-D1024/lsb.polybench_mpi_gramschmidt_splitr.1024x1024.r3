# Sysname : Linux
# Nodename: eu-a6-009-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 04:54:59 2022
# Execution time and date (local): Tue Jan 11 05:54:59 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 4169541.410156            0 
         1024          1024   gramschmidt          1 141968.281250            5 
         1024          1024   gramschmidt          2 142075.345703            1 
         1024          1024   gramschmidt          3 141058.339844            0 
         1024          1024   gramschmidt          4 141103.421875            1 
         1024          1024   gramschmidt          5 140865.763672            0 
         1024          1024   gramschmidt          6 146833.707031            0 
         1024          1024   gramschmidt          7 143817.552734            0 
         1024          1024   gramschmidt          8 142136.105469            4 
         1024          1024   gramschmidt          9 146931.853516            0 
# Runtime: 5.456374 s (overhead: 0.000202 %) 10 records
