# Sysname : Linux
# Nodename: eu-a6-009-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 04:54:59 2022
# Execution time and date (local): Tue Jan 11 05:54:59 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 7171501.914062            0 
         1024          1024   gramschmidt          1 141983.458984            4 
         1024          1024   gramschmidt          2 142085.648438            1 
         1024          1024   gramschmidt          3 141057.234375            0 
         1024          1024   gramschmidt          4 141100.203125            1 
         1024          1024   gramschmidt          5 140866.542969            0 
         1024          1024   gramschmidt          6 146821.943359            0 
         1024          1024   gramschmidt          7 143833.599609            0 
         1024          1024   gramschmidt          8 142129.767578            0 
         1024          1024   gramschmidt          9 146936.509766            0 
# Runtime: 8.458355 s (overhead: 0.000071 %) 10 records
