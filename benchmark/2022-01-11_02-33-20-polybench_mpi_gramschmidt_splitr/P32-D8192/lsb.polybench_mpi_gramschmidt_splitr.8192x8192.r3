# Sysname : Linux
# Nodename: eu-a6-009-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 04:58:58 2022
# Execution time and date (local): Tue Jan 11 05:58:58 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 213887555.712891            0 
         8192          8192   gramschmidt          1 230157430.781250            6 
         8192          8192   gramschmidt          2 213288365.554688            4 
         8192          8192   gramschmidt          3 222578760.097656            0 
         8192          8192   gramschmidt          4 228307130.097656            5 
         8192          8192   gramschmidt          5 251402225.326172            0 
         8192          8192   gramschmidt          6 207977462.169922            0 
         8192          8192   gramschmidt          7 202166524.623047            0 
         8192          8192   gramschmidt          8 221436491.101562            5 
         8192          8192   gramschmidt          9 206489762.771484            0 
# Runtime: 2197.691769 s (overhead: 0.000001 %) 10 records
