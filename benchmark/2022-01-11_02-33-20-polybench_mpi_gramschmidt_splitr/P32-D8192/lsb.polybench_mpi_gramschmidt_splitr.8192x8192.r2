# Sysname : Linux
# Nodename: eu-a6-009-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 04:58:58 2022
# Execution time and date (local): Tue Jan 11 05:58:58 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 213903377.445312            0 
         8192          8192   gramschmidt          1 230164805.324219            4 
         8192          8192   gramschmidt          2 213295368.589844            5 
         8192          8192   gramschmidt          3 222586321.265625            0 
         8192          8192   gramschmidt          4 228314627.187500            5 
         8192          8192   gramschmidt          5 251410342.992188            0 
         8192          8192   gramschmidt          6 207984366.755859            0 
         8192          8192   gramschmidt          7 202173134.128906            0 
         8192          8192   gramschmidt          8 221443712.056641            5 
         8192          8192   gramschmidt          9 206496735.476562            0 
# Runtime: 2197.772857 s (overhead: 0.000001 %) 10 records
