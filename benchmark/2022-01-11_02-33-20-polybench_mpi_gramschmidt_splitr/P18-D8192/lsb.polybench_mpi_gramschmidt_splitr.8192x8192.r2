# Sysname : Linux
# Nodename: eu-a6-008-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 02:58:08 2022
# Execution time and date (local): Tue Jan 11 03:58:08 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 872441102.339844            0 
         8192          8192   gramschmidt          1 706191513.578125            6 
         8192          8192   gramschmidt          2 738785323.044922            5 
         8192          8192   gramschmidt          3 683256018.671875            0 
         8192          8192   gramschmidt          4 674369779.810547            5 
         8192          8192   gramschmidt          5 671776668.042969            1 
         8192          8192   gramschmidt          6 641278852.966797            0 
         8192          8192   gramschmidt          7 647708679.312500            0 
         8192          8192   gramschmidt          8 636093763.521484           14 
         8192          8192   gramschmidt          9 687856777.412109            0 
# Runtime: 6959.758564 s (overhead: 0.000000 %) 10 records
