# Sysname : Linux
# Nodename: eu-a6-008-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 02:58:08 2022
# Execution time and date (local): Tue Jan 11 03:58:08 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 870524220.951172            0 
         8192          8192   gramschmidt          1 705452434.560547            6 
         8192          8192   gramschmidt          2 738011698.105469            7 
         8192          8192   gramschmidt          3 682541019.685547            1 
         8192          8192   gramschmidt          4 673663770.337891            6 
         8192          8192   gramschmidt          5 671073720.482422            2 
         8192          8192   gramschmidt          6 640607267.259766            0 
         8192          8192   gramschmidt          7 647031334.269531            0 
         8192          8192   gramschmidt          8 635428144.857422           27 
         8192          8192   gramschmidt          9 687136795.451172            0 
# Runtime: 6951.470515 s (overhead: 0.000001 %) 10 records
