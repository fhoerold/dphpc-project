# Sysname : Linux
# Nodename: eu-a6-012-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:33:36 2022
# Execution time and date (local): Tue Jan 11 02:33:36 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 3260531334.041016            0 
         8192          8192   gramschmidt          1 2685416043.767578            4 
         8192          8192   gramschmidt          2 2380086960.597656            4 
         8192          8192   gramschmidt          3 2357367717.855469            1 
         8192          8192   gramschmidt          4 2365585419.207031           13 
         8192          8192   gramschmidt          5 2363713261.835938            0 
         8192          8192   gramschmidt          6 2493986007.744141            0 
         8192          8192   gramschmidt          7 2501425029.169922            0 
         8192          8192   gramschmidt          8 2486278598.972656            4 
         8192          8192   gramschmidt          9 2345445134.529297            0 
# Runtime: 25239.835576 s (overhead: 0.000000 %) 10 records
