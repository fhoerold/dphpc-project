# Sysname : Linux
# Nodename: eu-a6-008-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:34:06 2022
# Execution time and date (local): Tue Jan 11 02:34:06 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 11814090.251953            0 
         2048          2048   gramschmidt          1 10577906.566406            5 
         2048          2048   gramschmidt          2 10371738.529297            6 
         2048          2048   gramschmidt          3 10274580.316406            0 
         2048          2048   gramschmidt          4 10175101.955078            6 
         2048          2048   gramschmidt          5 10283677.535156            0 
         2048          2048   gramschmidt          6 10278024.449219            0 
         2048          2048   gramschmidt          7 10178677.533203            0 
         2048          2048   gramschmidt          8 10184259.365234            7 
         2048          2048   gramschmidt          9 10192706.703125            0 
# Runtime: 104.330825 s (overhead: 0.000023 %) 10 records
