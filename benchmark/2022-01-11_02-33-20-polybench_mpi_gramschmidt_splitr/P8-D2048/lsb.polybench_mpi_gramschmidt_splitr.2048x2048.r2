# Sysname : Linux
# Nodename: eu-a6-008-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:34:06 2022
# Execution time and date (local): Tue Jan 11 02:34:06 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 11819070.099609            0 
         2048          2048   gramschmidt          1 10580519.832031            4 
         2048          2048   gramschmidt          2 10374292.109375            4 
         2048          2048   gramschmidt          3 10277097.458984            0 
         2048          2048   gramschmidt          4 10177642.851562            5 
         2048          2048   gramschmidt          5 10286200.132812            0 
         2048          2048   gramschmidt          6 10280481.027344            0 
         2048          2048   gramschmidt          7 10181266.693359            0 
         2048          2048   gramschmidt          8 10186726.472656            6 
         2048          2048   gramschmidt          9 10195153.205078            0 
# Runtime: 104.358503 s (overhead: 0.000018 %) 10 records
