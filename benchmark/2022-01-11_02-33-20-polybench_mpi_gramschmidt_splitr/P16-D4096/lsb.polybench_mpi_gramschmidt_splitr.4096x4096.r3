# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:38:34 2022
# Execution time and date (local): Tue Jan 11 02:38:34 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 47147079.833984            0 
         4096          4096   gramschmidt          1 41250752.138672            4 
         4096          4096   gramschmidt          2 41806689.457031            5 
         4096          4096   gramschmidt          3 40231700.695312            0 
         4096          4096   gramschmidt          4 37427302.509766            4 
         4096          4096   gramschmidt          5 37326634.531250            0 
         4096          4096   gramschmidt          6 40212514.224609            0 
         4096          4096   gramschmidt          7 39847060.634766            0 
         4096          4096   gramschmidt          8 39372329.978516            6 
         4096          4096   gramschmidt          9 41835376.851562            0 
# Runtime: 406.457509 s (overhead: 0.000005 %) 10 records
