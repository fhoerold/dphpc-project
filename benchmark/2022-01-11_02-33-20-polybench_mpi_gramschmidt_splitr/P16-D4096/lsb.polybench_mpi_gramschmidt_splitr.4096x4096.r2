# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:38:34 2022
# Execution time and date (local): Tue Jan 11 02:38:34 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 46108411.552734            0 
         4096          4096   gramschmidt          1 41283702.980469            5 
         4096          4096   gramschmidt          2 41808212.164062            6 
         4096          4096   gramschmidt          3 40221388.718750            0 
         4096          4096   gramschmidt          4 37446748.808594            5 
         4096          4096   gramschmidt          5 37324170.802734            0 
         4096          4096   gramschmidt          6 40181459.636719            0 
         4096          4096   gramschmidt          7 39886395.330078            0 
         4096          4096   gramschmidt          8 39369594.027344            6 
         4096          4096   gramschmidt          9 41806161.855469            0 
# Runtime: 405.436306 s (overhead: 0.000005 %) 10 records
