# Sysname : Linux
# Nodename: eu-a6-012-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:33:36 2022
# Execution time and date (local): Tue Jan 11 02:33:36 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 4105375.488281            0 
          512           512   gramschmidt          1 110037.380859            5 
          512           512   gramschmidt          2 108996.169922            1 
          512           512   gramschmidt          3 108258.871094            0 
          512           512   gramschmidt          4 107186.273438            1 
          512           512   gramschmidt          5 107887.966797            0 
          512           512   gramschmidt          6 112980.259766            0 
          512           512   gramschmidt          7 112687.830078            0 
          512           512   gramschmidt          8 115999.722656            5 
          512           512   gramschmidt          9 113944.947266            0 
# Runtime: 5.103405 s (overhead: 0.000235 %) 10 records
