# Sysname : Linux
# Nodename: eu-a6-012-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:33:36 2022
# Execution time and date (local): Tue Jan 11 02:33:36 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 5100646.500000            0 
          512           512   gramschmidt          1 109973.925781            3 
          512           512   gramschmidt          2 108928.142578            1 
          512           512   gramschmidt          3 108167.283203            0 
          512           512   gramschmidt          4 107115.650391            1 
          512           512   gramschmidt          5 107810.535156            0 
          512           512   gramschmidt          6 112901.421875            0 
          512           512   gramschmidt          7 112606.904297            0 
          512           512   gramschmidt          8 115918.322266            0 
          512           512   gramschmidt          9 113871.644531            0 
# Runtime: 6.097977 s (overhead: 0.000082 %) 10 records
