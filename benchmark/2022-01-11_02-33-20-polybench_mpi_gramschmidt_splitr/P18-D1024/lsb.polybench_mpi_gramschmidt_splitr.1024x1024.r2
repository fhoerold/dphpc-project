# Sysname : Linux
# Nodename: eu-a6-008-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 02:47:58 2022
# Execution time and date (local): Tue Jan 11 03:47:58 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 4299161.337891            0 
         1024          1024   gramschmidt          1 289019.488281            4 
         1024          1024   gramschmidt          2 288889.042969            1 
         1024          1024   gramschmidt          3 283850.945312            0 
         1024          1024   gramschmidt          4 288359.740234            4 
         1024          1024   gramschmidt          5 278485.730469            0 
         1024          1024   gramschmidt          6 287308.451172            0 
         1024          1024   gramschmidt          7 279567.794922            0 
         1024          1024   gramschmidt          8 279012.582031            1 
         1024          1024   gramschmidt          9 272048.431641            0 
# Runtime: 6.845755 s (overhead: 0.000146 %) 10 records
