# Sysname : Linux
# Nodename: eu-a6-008-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 02:48:12 2022
# Execution time and date (local): Tue Jan 11 03:48:12 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 8996281.253906            0 
         2048          2048   gramschmidt          1 3811527.076172            5 
         2048          2048   gramschmidt          2 3939675.103516            4 
         2048          2048   gramschmidt          3 3647279.888672            0 
         2048          2048   gramschmidt          4 3657708.873047            4 
         2048          2048   gramschmidt          5 3824905.248047            0 
         2048          2048   gramschmidt          6 3729214.123047            0 
         2048          2048   gramschmidt          7 3752775.304688            0 
         2048          2048   gramschmidt          8 3493310.681641            5 
         2048          2048   gramschmidt          9 3359935.597656            0 
# Runtime: 42.212659 s (overhead: 0.000043 %) 10 records
