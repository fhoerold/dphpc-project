# Sysname : Linux
# Nodename: eu-a6-008-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 02:48:12 2022
# Execution time and date (local): Tue Jan 11 03:48:12 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 9971950.242188            0 
         2048          2048   gramschmidt          1 3807718.103516            4 
         2048          2048   gramschmidt          2 3935486.359375            1 
         2048          2048   gramschmidt          3 3643533.117188            0 
         2048          2048   gramschmidt          4 3653972.123047            4 
         2048          2048   gramschmidt          5 3820961.806641            0 
         2048          2048   gramschmidt          6 3725386.458984            0 
         2048          2048   gramschmidt          7 3748967.310547            3 
         2048          2048   gramschmidt          8 3489685.437500            5 
         2048          2048   gramschmidt          9 3356497.583984            0 
# Runtime: 43.154211 s (overhead: 0.000039 %) 10 records
