# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:36:52 2022
# Execution time and date (local): Tue Jan 11 02:36:52 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 6797449.484375            0 
         2048          2048   gramschmidt          1 4292719.148438            3 
         2048          2048   gramschmidt          2 4090300.083984            4 
         2048          2048   gramschmidt          3 4134538.640625            0 
         2048          2048   gramschmidt          4 3965151.484375            3 
         2048          2048   gramschmidt          5 4054632.958984            0 
         2048          2048   gramschmidt          6 3835803.318359            0 
         2048          2048   gramschmidt          7 3759253.884766            0 
         2048          2048   gramschmidt          8 4051522.134766            4 
         2048          2048   gramschmidt          9 3749949.468750            0 
# Runtime: 42.731379 s (overhead: 0.000033 %) 10 records
