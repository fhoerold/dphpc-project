# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:36:52 2022
# Execution time and date (local): Tue Jan 11 02:36:52 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 6786764.535156            0 
         2048          2048   gramschmidt          1 4302157.429688            5 
         2048          2048   gramschmidt          2 4090490.763672            4 
         2048          2048   gramschmidt          3 4134731.576172            0 
         2048          2048   gramschmidt          4 3965279.705078            5 
         2048          2048   gramschmidt          5 4054838.033203            0 
         2048          2048   gramschmidt          6 3835956.619141            0 
         2048          2048   gramschmidt          7 3759391.695312            0 
         2048          2048   gramschmidt          8 4051702.224609            6 
         2048          2048   gramschmidt          9 3750122.333984            0 
# Runtime: 42.731488 s (overhead: 0.000047 %) 10 records
