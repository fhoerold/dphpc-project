# Sysname : Linux
# Nodename: eu-a6-008-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 02:47:40 2022
# Execution time and date (local): Tue Jan 11 03:47:40 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 10040199.419922            2 
          512           512   gramschmidt          1 40816.580078            5 
          512           512   gramschmidt          2 39791.642578            0 
          512           512   gramschmidt          3 38831.521484            0 
          512           512   gramschmidt          4 39200.156250            1 
          512           512   gramschmidt          5 39632.384766            0 
          512           512   gramschmidt          6 39892.041016            0 
          512           512   gramschmidt          7 39273.783203            0 
          512           512   gramschmidt          8 39485.228516            1 
          512           512   gramschmidt          9 38479.300781            0 
# Runtime: 10.395641 s (overhead: 0.000087 %) 10 records
