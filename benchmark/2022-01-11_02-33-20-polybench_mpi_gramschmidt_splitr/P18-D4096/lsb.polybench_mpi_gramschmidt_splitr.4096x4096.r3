# Sysname : Linux
# Nodename: eu-a6-008-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 02:49:39 2022
# Execution time and date (local): Tue Jan 11 03:49:39 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 56867289.214844            0 
         4096          4096   gramschmidt          1 48922637.753906            5 
         4096          4096   gramschmidt          2 48675930.869141            6 
         4096          4096   gramschmidt          3 48227187.783203            0 
         4096          4096   gramschmidt          4 47660042.056641            8 
         4096          4096   gramschmidt          5 47511398.779297            0 
         4096          4096   gramschmidt          6 46909728.119141            1 
         4096          4096   gramschmidt          7 46679381.937500            0 
         4096          4096   gramschmidt          8 46404587.332031            8 
         4096          4096   gramschmidt          9 42405664.943359            1 
# Runtime: 480.263905 s (overhead: 0.000006 %) 10 records
