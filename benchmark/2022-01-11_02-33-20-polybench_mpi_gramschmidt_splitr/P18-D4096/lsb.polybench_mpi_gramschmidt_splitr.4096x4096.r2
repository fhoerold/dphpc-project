# Sysname : Linux
# Nodename: eu-a6-008-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 02:49:39 2022
# Execution time and date (local): Tue Jan 11 03:49:39 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 57868895.238281            0 
         4096          4096   gramschmidt          1 48923679.933594            6 
         4096          4096   gramschmidt          2 48676826.593750            5 
         4096          4096   gramschmidt          3 48228333.771484            0 
         4096          4096   gramschmidt          4 47660881.667969            7 
         4096          4096   gramschmidt          5 47512397.880859            2 
         4096          4096   gramschmidt          6 46910441.654297            0 
         4096          4096   gramschmidt          7 46680262.107422            3 
         4096          4096   gramschmidt          8 46405693.269531            8 
         4096          4096   gramschmidt          9 42406542.177734            0 
# Runtime: 481.274027 s (overhead: 0.000006 %) 10 records
