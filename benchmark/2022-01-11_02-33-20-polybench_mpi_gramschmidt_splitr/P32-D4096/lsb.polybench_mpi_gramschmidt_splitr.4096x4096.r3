# Sysname : Linux
# Nodename: eu-a6-009-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 04:55:59 2022
# Execution time and date (local): Tue Jan 11 05:55:59 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 24229898.427734            0 
         4096          4096   gramschmidt          1 12749274.341797            4 
         4096          4096   gramschmidt          2 12898543.000000            2 
         4096          4096   gramschmidt          3 13274542.144531            0 
         4096          4096   gramschmidt          4 12831765.914062            5 
         4096          4096   gramschmidt          5 12624172.271484            0 
         4096          4096   gramschmidt          6 12777488.609375            0 
         4096          4096   gramschmidt          7 12437076.457031            0 
         4096          4096   gramschmidt          8 12401820.906250            5 
         4096          4096   gramschmidt          9 12313589.972656            0 
# Runtime: 138.538235 s (overhead: 0.000012 %) 10 records
