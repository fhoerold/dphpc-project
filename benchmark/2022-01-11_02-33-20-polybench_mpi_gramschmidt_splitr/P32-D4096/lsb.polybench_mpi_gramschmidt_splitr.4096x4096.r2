# Sysname : Linux
# Nodename: eu-a6-009-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 04:55:59 2022
# Execution time and date (local): Tue Jan 11 05:55:59 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 22297038.917969            0 
         4096          4096   gramschmidt          1 12761482.482422            5 
         4096          4096   gramschmidt          2 12911103.896484            2 
         4096          4096   gramschmidt          3 13287099.500000            0 
         4096          4096   gramschmidt          4 12844302.498047            6 
         4096          4096   gramschmidt          5 12636146.757812            0 
         4096          4096   gramschmidt          6 12789785.710938            0 
         4096          4096   gramschmidt          7 12449008.943359            0 
         4096          4096   gramschmidt          8 12413789.146484            6 
         4096          4096   gramschmidt          9 12325319.025391            0 
# Runtime: 136.715139 s (overhead: 0.000014 %) 10 records
