# Sysname : Linux
# Nodename: eu-a6-004-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 04:54:28 2022
# Execution time and date (local): Tue Jan 11 05:54:28 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 10033482.820312            0 
          512           512   gramschmidt          1 25303.519531            3 
          512           512   gramschmidt          2 24712.236328            1 
          512           512   gramschmidt          3 24061.523438            0 
          512           512   gramschmidt          4 23951.884766            1 
          512           512   gramschmidt          5 25482.109375            0 
          512           512   gramschmidt          6 24291.462891            0 
          512           512   gramschmidt          7 23700.763672            0 
          512           512   gramschmidt          8 23704.292969            1 
          512           512   gramschmidt          9 23580.703125            0 
# Runtime: 10.252304 s (overhead: 0.000059 %) 10 records
