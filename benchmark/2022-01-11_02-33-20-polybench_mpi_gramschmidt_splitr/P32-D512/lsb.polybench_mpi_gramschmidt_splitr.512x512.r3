# Sysname : Linux
# Nodename: eu-a6-004-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 04:54:28 2022
# Execution time and date (local): Tue Jan 11 05:54:28 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 5034680.712891            0 
          512           512   gramschmidt          1 25301.785156            3 
          512           512   gramschmidt          2 24706.750000            0 
          512           512   gramschmidt          3 24062.833984            0 
          512           512   gramschmidt          4 23964.722656            0 
          512           512   gramschmidt          5 25473.640625            0 
          512           512   gramschmidt          6 24291.689453            0 
          512           512   gramschmidt          7 23704.494141            0 
          512           512   gramschmidt          8 23706.625000            1 
          512           512   gramschmidt          9 23575.628906            0 
# Runtime: 5.253506 s (overhead: 0.000076 %) 10 records
