# Sysname : Linux
# Nodename: eu-a6-012-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:34:35 2022
# Execution time and date (local): Tue Jan 11 02:34:35 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 167101669.574219            0 
         4096          4096   gramschmidt          1 135020823.146484            6 
         4096          4096   gramschmidt          2 127442759.871094            4 
         4096          4096   gramschmidt          3 131665831.603516            0 
         4096          4096   gramschmidt          4 129008425.460938            5 
         4096          4096   gramschmidt          5 132273892.458984            0 
         4096          4096   gramschmidt          6 125153440.513672            0 
         4096          4096   gramschmidt          7 132411984.480469            0 
         4096          4096   gramschmidt          8 128691854.455078            4 
         4096          4096   gramschmidt          9 127742499.388672            0 
# Runtime: 1336.513232 s (overhead: 0.000001 %) 10 records
