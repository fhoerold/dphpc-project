# Sysname : Linux
# Nodename: eu-a6-012-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:34:35 2022
# Execution time and date (local): Tue Jan 11 02:34:35 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 167106529.117188            0 
         4096          4096   gramschmidt          1 135021447.587891            5 
         4096          4096   gramschmidt          2 127443587.355469            5 
         4096          4096   gramschmidt          3 131666428.617188            0 
         4096          4096   gramschmidt          4 129009234.312500            6 
         4096          4096   gramschmidt          5 132274670.373047            0 
         4096          4096   gramschmidt          6 125154429.533203            0 
         4096          4096   gramschmidt          7 132412704.869141            0 
         4096          4096   gramschmidt          8 128692825.958984            5 
         4096          4096   gramschmidt          9 127742920.880859            0 
# Runtime: 1336.524834 s (overhead: 0.000002 %) 10 records
