# Sysname : Linux
# Nodename: eu-a6-008-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:33:36 2022
# Execution time and date (local): Tue Jan 11 02:33:36 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 10538737.765625            0 
         1024          1024   gramschmidt          1 535307.343750            5 
         1024          1024   gramschmidt          2 537344.619141            1 
         1024          1024   gramschmidt          3 529377.023438            0 
         1024          1024   gramschmidt          4 531852.718750            2 
         1024          1024   gramschmidt          5 535538.029297            0 
         1024          1024   gramschmidt          6 550304.746094            0 
         1024          1024   gramschmidt          7 550860.150391            0 
         1024          1024   gramschmidt          8 535002.666016            6 
         1024          1024   gramschmidt          9 546318.417969            0 
# Runtime: 15.390694 s (overhead: 0.000091 %) 10 records
