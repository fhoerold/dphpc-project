# Sysname : Linux
# Nodename: eu-a6-008-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:33:36 2022
# Execution time and date (local): Tue Jan 11 02:33:36 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 12536514.767578            0 
         1024          1024   gramschmidt          1 535259.216797            5 
         1024          1024   gramschmidt          2 537310.855469            5 
         1024          1024   gramschmidt          3 529311.042969            0 
         1024          1024   gramschmidt          4 531804.671875            2 
         1024          1024   gramschmidt          5 535496.216797            0 
         1024          1024   gramschmidt          6 550218.509766            0 
         1024          1024   gramschmidt          7 550838.880859            0 
         1024          1024   gramschmidt          8 534934.671875            1 
         1024          1024   gramschmidt          9 546283.931641            0 
# Runtime: 17.388019 s (overhead: 0.000075 %) 10 records
