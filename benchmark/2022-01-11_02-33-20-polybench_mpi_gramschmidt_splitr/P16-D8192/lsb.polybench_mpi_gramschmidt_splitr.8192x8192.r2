# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:45:35 2022
# Execution time and date (local): Tue Jan 11 02:45:35 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 657575891.947266            0 
         8192          8192   gramschmidt          1 703142243.017578            5 
         8192          8192   gramschmidt          2 541757910.583984            5 
         8192          8192   gramschmidt          3 654689259.982422            0 
         8192          8192   gramschmidt          4 721218857.287109            5 
         8192          8192   gramschmidt          5 717789414.800781            0 
         8192          8192   gramschmidt          6 592111029.382812            0 
         8192          8192   gramschmidt          7 490199642.072266            2 
         8192          8192   gramschmidt          8 653531355.888672            5 
         8192          8192   gramschmidt          9 559496513.962891            0 
# Runtime: 6291.512176 s (overhead: 0.000000 %) 10 records
