# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:45:35 2022
# Execution time and date (local): Tue Jan 11 02:45:35 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 654643970.677734            0 
         8192          8192   gramschmidt          1 703178874.035156            4 
         8192          8192   gramschmidt          2 541785973.345703            5 
         8192          8192   gramschmidt          3 654723413.546875            0 
         8192          8192   gramschmidt          4 721256764.710938            5 
         8192          8192   gramschmidt          5 717826673.740234            0 
         8192          8192   gramschmidt          6 592141516.146484            0 
         8192          8192   gramschmidt          7 490223537.787109            0 
         8192          8192   gramschmidt          8 653566125.988281            5 
         8192          8192   gramschmidt          9 559525372.210938            0 
# Runtime: 6288.872274 s (overhead: 0.000000 %) 10 records
