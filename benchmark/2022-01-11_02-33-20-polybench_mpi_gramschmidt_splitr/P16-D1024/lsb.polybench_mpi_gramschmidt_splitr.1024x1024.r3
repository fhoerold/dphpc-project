# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:38:04 2022
# Execution time and date (local): Tue Jan 11 02:38:04 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 9271259.263672            0 
         1024          1024   gramschmidt          1 254476.644531            5 
         1024          1024   gramschmidt          2 249951.285156            1 
         1024          1024   gramschmidt          3 250959.277344            0 
         1024          1024   gramschmidt          4 247660.740234            2 
         1024          1024   gramschmidt          5 249466.554688            0 
         1024          1024   gramschmidt          6 249621.291016            0 
         1024          1024   gramschmidt          7 281137.148438            0 
         1024          1024   gramschmidt          8 246330.259766            1 
         1024          1024   gramschmidt          9 246419.695312            0 
# Runtime: 11.547330 s (overhead: 0.000078 %) 10 records
