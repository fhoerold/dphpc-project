# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:38:04 2022
# Execution time and date (local): Tue Jan 11 02:38:04 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 9273666.978516            0 
         1024          1024   gramschmidt          1 254569.359375            5 
         1024          1024   gramschmidt          2 249869.683594            1 
         1024          1024   gramschmidt          3 250957.445312            0 
         1024          1024   gramschmidt          4 247652.066406            1 
         1024          1024   gramschmidt          5 249470.675781            0 
         1024          1024   gramschmidt          6 249613.560547            0 
         1024          1024   gramschmidt          7 281130.267578            0 
         1024          1024   gramschmidt          8 246348.449219            2 
         1024          1024   gramschmidt          9 246408.476562            0 
# Runtime: 11.549746 s (overhead: 0.000078 %) 10 records
