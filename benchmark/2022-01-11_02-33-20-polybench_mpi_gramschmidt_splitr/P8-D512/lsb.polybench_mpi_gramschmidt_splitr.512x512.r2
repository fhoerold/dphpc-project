# Sysname : Linux
# Nodename: eu-a6-012-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:33:36 2022
# Execution time and date (local): Tue Jan 11 02:33:36 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 5055064.925781            0 
          512           512   gramschmidt          1 57846.791016            4 
          512           512   gramschmidt          2 58514.796875            1 
          512           512   gramschmidt          3 58171.570312            0 
          512           512   gramschmidt          4 57116.095703            1 
          512           512   gramschmidt          5 57783.246094            0 
          512           512   gramschmidt          6 57471.902344            0 
          512           512   gramschmidt          7 57193.150391            0 
          512           512   gramschmidt          8 57575.949219            1 
          512           512   gramschmidt          9 57404.847656            0 
# Runtime: 5.574181 s (overhead: 0.000126 %) 10 records
