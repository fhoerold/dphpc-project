# Sysname : Linux
# Nodename: eu-a6-012-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:33:36 2022
# Execution time and date (local): Tue Jan 11 02:33:36 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          512           512   gramschmidt          0 7066581.175781            0 
          512           512   gramschmidt          1 57857.748047            3 
          512           512   gramschmidt          2 58522.792969            1 
          512           512   gramschmidt          3 58182.984375            0 
          512           512   gramschmidt          4 57143.804688            0 
          512           512   gramschmidt          5 57826.468750            0 
          512           512   gramschmidt          6 57496.572266            0 
          512           512   gramschmidt          7 57210.472656            0 
          512           512   gramschmidt          8 57603.220703            2 
          512           512   gramschmidt          9 57395.537109            0 
# Runtime: 7.585848 s (overhead: 0.000079 %) 10 records
