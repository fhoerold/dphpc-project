# Sysname : Linux
# Nodename: eu-a6-008-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:33:36 2022
# Execution time and date (local): Tue Jan 11 02:33:36 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 17833371.947266            1 
         2048          2048   gramschmidt          1 15326145.576172            4 
         2048          2048   gramschmidt          2 15357921.316406            5 
         2048          2048   gramschmidt          3 15367888.236328            1 
         2048          2048   gramschmidt          4 15325053.851562            4 
         2048          2048   gramschmidt          5 15297196.687500            1 
         2048          2048   gramschmidt          6 15292124.652344            0 
         2048          2048   gramschmidt          7 15262181.666016            0 
         2048          2048   gramschmidt          8 15281786.072266            5 
         2048          2048   gramschmidt          9 15246680.294922            1 
# Runtime: 155.590396 s (overhead: 0.000014 %) 10 records
