# Sysname : Linux
# Nodename: eu-a6-008-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:33:36 2022
# Execution time and date (local): Tue Jan 11 02:33:36 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 21787053.796875            0 
         2048          2048   gramschmidt          1 15310641.755859            4 
         2048          2048   gramschmidt          2 15343734.808594            3 
         2048          2048   gramschmidt          3 15351871.408203            0 
         2048          2048   gramschmidt          4 15332287.347656            6 
         2048          2048   gramschmidt          5 15259901.923828            0 
         2048          2048   gramschmidt          6 15300238.437500            0 
         2048          2048   gramschmidt          7 15247601.296875            0 
         2048          2048   gramschmidt          8 15266116.916016            3 
         2048          2048   gramschmidt          9 15214549.314453            0 
# Runtime: 159.414060 s (overhead: 0.000010 %) 10 records
