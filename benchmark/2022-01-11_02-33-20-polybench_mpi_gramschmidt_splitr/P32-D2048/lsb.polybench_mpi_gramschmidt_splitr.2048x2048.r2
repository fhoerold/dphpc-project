# Sysname : Linux
# Nodename: eu-a6-009-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 04:55:14 2022
# Execution time and date (local): Tue Jan 11 05:55:14 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 12155573.439453            0 
         2048          2048   gramschmidt          1 1091056.958984            5 
         2048          2048   gramschmidt          2 1102792.599609            2 
         2048          2048   gramschmidt          3 1131582.808594            0 
         2048          2048   gramschmidt          4 1083178.232422            1 
         2048          2048   gramschmidt          5 1141142.328125            0 
         2048          2048   gramschmidt          6 1171503.089844            0 
         2048          2048   gramschmidt          7 1088348.496094            0 
         2048          2048   gramschmidt          8 1134980.234375            5 
         2048          2048   gramschmidt          9 1133929.175781            0 
# Runtime: 22.234141 s (overhead: 0.000058 %) 10 records
