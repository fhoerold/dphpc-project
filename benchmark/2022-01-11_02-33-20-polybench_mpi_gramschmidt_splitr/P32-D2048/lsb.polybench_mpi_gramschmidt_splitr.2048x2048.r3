# Sysname : Linux
# Nodename: eu-a6-009-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 04:55:14 2022
# Execution time and date (local): Tue Jan 11 05:55:14 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2048          2048   gramschmidt          0 12150298.498047            0 
         2048          2048   gramschmidt          1 1090765.925781            4 
         2048          2048   gramschmidt          2 1102447.869141            4 
         2048          2048   gramschmidt          3 1131248.277344            0 
         2048          2048   gramschmidt          4 1082864.798828            1 
         2048          2048   gramschmidt          5 1140817.666016            0 
         2048          2048   gramschmidt          6 1171178.525391            0 
         2048          2048   gramschmidt          7 1088025.888672            0 
         2048          2048   gramschmidt          8 1134639.818359            1 
         2048          2048   gramschmidt          9 1133629.001953            0 
# Runtime: 22.225966 s (overhead: 0.000045 %) 10 records
