# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:33:36 2022
# Execution time and date (local): Tue Jan 11 02:33:36 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 14709938.119141            0 
         1024          1024   gramschmidt          1 2246681.572266            5 
         1024          1024   gramschmidt          2 2227712.421875            1 
         1024          1024   gramschmidt          3 2218629.796875            0 
         1024          1024   gramschmidt          4 2505355.457031            2 
         1024          1024   gramschmidt          5 2448032.365234            0 
         1024          1024   gramschmidt          6 2129052.472656            0 
         1024          1024   gramschmidt          7 2417155.363281            0 
         1024          1024   gramschmidt          8 2118544.105469            5 
         1024          1024   gramschmidt          9 2201639.933594            0 
# Runtime: 35.222792 s (overhead: 0.000037 %) 10 records
