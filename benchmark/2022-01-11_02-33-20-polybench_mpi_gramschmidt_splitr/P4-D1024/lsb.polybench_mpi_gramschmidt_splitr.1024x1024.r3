# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:33:36 2022
# Execution time and date (local): Tue Jan 11 02:33:36 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 14711208.464844            1 
         1024          1024   gramschmidt          1 2246788.365234            4 
         1024          1024   gramschmidt          2 2227862.818359            1 
         1024          1024   gramschmidt          3 2218802.320312            0 
         1024          1024   gramschmidt          4 2505560.380859            1 
         1024          1024   gramschmidt          5 2453455.841797            0 
         1024          1024   gramschmidt          6 2123910.027344            0 
         1024          1024   gramschmidt          7 2417255.943359            0 
         1024          1024   gramschmidt          8 2118709.953125            4 
         1024          1024   gramschmidt          9 2201746.103516            0 
# Runtime: 35.225345 s (overhead: 0.000031 %) 10 records
