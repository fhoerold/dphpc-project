# Sysname : Linux
# Nodename: eu-a6-012-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:33:36 2022
# Execution time and date (local): Tue Jan 11 02:33:36 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 418613643.666016            2 
         4096          4096   gramschmidt          1 387523760.240234           10 
         4096          4096   gramschmidt          2 420022464.234375            6 
         4096          4096   gramschmidt          3 403906933.591797            2 
         4096          4096   gramschmidt          4 302467212.683594            8 
         4096          4096   gramschmidt          5 300768460.128906            2 
         4096          4096   gramschmidt          6 315634248.181641            0 
         4096          4096   gramschmidt          7 322312216.345703            0 
         4096          4096   gramschmidt          8 341761041.572266            5 
         4096          4096   gramschmidt          9 343676942.630859            0 
# Runtime: 3556.686988 s (overhead: 0.000001 %) 10 records
