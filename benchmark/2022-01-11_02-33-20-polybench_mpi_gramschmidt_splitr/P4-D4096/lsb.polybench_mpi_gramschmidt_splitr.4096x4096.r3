# Sysname : Linux
# Nodename: eu-a6-012-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:33:36 2022
# Execution time and date (local): Tue Jan 11 02:33:36 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 416472968.755859            0 
         4096          4096   gramschmidt          1 387398775.246094            5 
         4096          4096   gramschmidt          2 419887113.896484            4 
         4096          4096   gramschmidt          3 403776131.375000            0 
         4096          4096   gramschmidt          4 302369990.345703            4 
         4096          4096   gramschmidt          5 300671136.039062            0 
         4096          4096   gramschmidt          6 315532590.236328            0 
         4096          4096   gramschmidt          7 322208279.146484            0 
         4096          4096   gramschmidt          8 341651214.037109            4 
         4096          4096   gramschmidt          9 343566173.500000            0 
# Runtime: 3553.534428 s (overhead: 0.000000 %) 10 records
