# Sysname : Linux
# Nodename: eu-a6-012-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:34:35 2022
# Execution time and date (local): Tue Jan 11 02:34:35 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 1140145845.613281            0 
         8192          8192   gramschmidt          1 1198328825.580078            5 
         8192          8192   gramschmidt          2 1154190947.400391            5 
         8192          8192   gramschmidt          3 1106456558.017578            0 
         8192          8192   gramschmidt          4 1149310679.064453            4 
         8192          8192   gramschmidt          5 1149472812.093750            0 
         8192          8192   gramschmidt          6 1179879279.380859            0 
         8192          8192   gramschmidt          7 1149238672.650391            0 
         8192          8192   gramschmidt          8 1197372199.718750            4 
         8192          8192   gramschmidt          9 1147316631.662109            0 
# Runtime: 11571.712531 s (overhead: 0.000000 %) 10 records
