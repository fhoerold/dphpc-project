# Sysname : Linux
# Nodename: eu-a6-012-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 01:34:35 2022
# Execution time and date (local): Tue Jan 11 02:34:35 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         8192          8192   gramschmidt          0 1141575873.376953            0 
         8192          8192   gramschmidt          1 1197722583.691406            4 
         8192          8192   gramschmidt          2 1153607786.550781            5 
         8192          8192   gramschmidt          3 1105896153.572266            0 
         8192          8192   gramschmidt          4 1148729321.472656            5 
         8192          8192   gramschmidt          5 1148891333.566406            0 
         8192          8192   gramschmidt          6 1179282384.966797            0 
         8192          8192   gramschmidt          7 1148657424.052734            0 
         8192          8192   gramschmidt          8 1196766449.740234            5 
         8192          8192   gramschmidt          9 1146736254.757812            0 
# Runtime: 11567.865623 s (overhead: 0.000000 %) 10 records
