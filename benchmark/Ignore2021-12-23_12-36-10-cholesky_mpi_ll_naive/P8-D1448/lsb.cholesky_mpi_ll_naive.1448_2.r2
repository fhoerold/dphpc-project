# Sysname : Linux
# Nodename: eu-a6-007-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 11:36:34 2021
# Execution time and date (local): Thu Dec 23 12:36:34 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1448           MPI          0 105513.083984            0 
            2          1448           MPI          1 105288.585938            1 
            2          1448           MPI          2 103999.541016            1 
            2          1448           MPI          3 103140.361328            0 
            2          1448           MPI          4 104960.791016            1 
            2          1448           MPI          5 104616.359375            0 
            2          1448           MPI          6 105212.826172            0 
            2          1448           MPI          7 105026.501953            0 
            2          1448           MPI          8 105118.933594            1 
            2          1448           MPI          9 105254.474609            0 
# Runtime: 1.269125 s (overhead: 0.000315 %) 10 records
