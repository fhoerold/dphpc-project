# Sysname : Linux
# Nodename: eu-a6-007-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 11:36:34 2021
# Execution time and date (local): Thu Dec 23 12:36:34 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1448           MPI          0 105520.927734            0 
            3          1448           MPI          1 105263.953125            0 
            3          1448           MPI          2 104003.634766            0 
            3          1448           MPI          3 103143.984375            0 
            3          1448           MPI          4 104964.316406            0 
            3          1448           MPI          5 104620.023438            0 
            3          1448           MPI          6 105216.337891            0 
            3          1448           MPI          7 105030.431641            0 
            3          1448           MPI          8 105122.681641            0 
            3          1448           MPI          9 105258.138672            0 
# Runtime: 1.268201 s (overhead: 0.000000 %) 10 records
