# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 11:39:49 2021
# Execution time and date (local): Thu Dec 23 12:39:49 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2896           MPI          0 620998.740234            0 
            3          2896           MPI          1 620893.123047            0 
            3          2896           MPI          2 619253.519531            0 
            3          2896           MPI          3 620757.542969            0 
            3          2896           MPI          4 620246.208984            0 
            3          2896           MPI          5 620207.478516            0 
            3          2896           MPI          6 620149.507812            0 
            3          2896           MPI          7 620291.683594            0 
            3          2896           MPI          8 619467.794922            0 
            3          2896           MPI          9 619401.066406            0 
# Runtime: 13.455114 s (overhead: 0.000000 %) 10 records
