# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 11:39:49 2021
# Execution time and date (local): Thu Dec 23 12:39:49 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2896           MPI          0 621153.917969            0 
            2          2896           MPI          1 621037.638672            1 
            2          2896           MPI          2 619406.898438            1 
            2          2896           MPI          3 620903.917969            0 
            2          2896           MPI          4 620392.443359            1 
            2          2896           MPI          5 620353.283203            0 
            2          2896           MPI          6 620295.162109            0 
            2          2896           MPI          7 620438.437500            0 
            2          2896           MPI          8 619613.542969            1 
            2          2896           MPI          9 619546.810547            0 
# Runtime: 7.464272 s (overhead: 0.000054 %) 10 records
