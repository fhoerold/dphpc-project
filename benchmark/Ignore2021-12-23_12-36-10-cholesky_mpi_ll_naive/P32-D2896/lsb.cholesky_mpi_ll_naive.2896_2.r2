# Sysname : Linux
# Nodename: eu-a6-009-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 13:01:59 2021
# Execution time and date (local): Thu Dec 23 14:01:59 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2896           MPI          0 866832.988281            0 
            2          2896           MPI          1 864910.699219            4 
            2          2896           MPI          2 872301.710938            1 
            2          2896           MPI          3 868634.037109            0 
            2          2896           MPI          4 865818.314453            0 
            2          2896           MPI          5 864481.822266            0 
            2          2896           MPI          6 866733.812500            0 
            2          2896           MPI          7 865085.589844            0 
            2          2896           MPI          8 863859.630859            0 
            2          2896           MPI          9 871749.521484            0 
# Runtime: 15.431985 s (overhead: 0.000032 %) 10 records
