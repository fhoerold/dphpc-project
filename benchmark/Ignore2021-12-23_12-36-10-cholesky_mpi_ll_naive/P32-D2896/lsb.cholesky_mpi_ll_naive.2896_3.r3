# Sysname : Linux
# Nodename: eu-a6-009-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 13:01:59 2021
# Execution time and date (local): Thu Dec 23 14:01:59 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2896           MPI          0 866836.490234            0 
            3          2896           MPI          1 864920.482422            4 
            3          2896           MPI          2 872311.560547            0 
            3          2896           MPI          3 868645.111328            0 
            3          2896           MPI          4 865828.148438            0 
            3          2896           MPI          5 864491.587891            0 
            3          2896           MPI          6 866743.644531            0 
            3          2896           MPI          7 865095.398438            0 
            3          2896           MPI          8 863869.542969            4 
            3          2896           MPI          9 871759.498047            0 
# Runtime: 15.433480 s (overhead: 0.000052 %) 10 records
