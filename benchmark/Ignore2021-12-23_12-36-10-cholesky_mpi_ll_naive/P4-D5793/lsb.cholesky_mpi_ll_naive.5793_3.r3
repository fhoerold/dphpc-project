# Sysname : Linux
# Nodename: eu-a6-007-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 11:57:26 2021
# Execution time and date (local): Thu Dec 23 12:57:26 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          5793           MPI          0 11014984.312500            0 
            3          5793           MPI          1 11038295.734375            4 
            3          5793           MPI          2 11031995.732422            1 
            3          5793           MPI          3 11063085.925781            2 
            3          5793           MPI          4 11029579.222656            4 
            3          5793           MPI          5 11009171.035156            0 
            3          5793           MPI          6 11011677.195312            0 
            3          5793           MPI          7 11010520.320312            0 
            3          5793           MPI          8 11019274.720703            4 
            3          5793           MPI          9 11052602.335938            4 
# Runtime: 133.349731 s (overhead: 0.000014 %) 10 records
