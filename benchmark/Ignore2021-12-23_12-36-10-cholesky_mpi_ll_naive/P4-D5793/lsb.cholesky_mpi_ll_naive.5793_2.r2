# Sysname : Linux
# Nodename: eu-a6-007-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 11:57:26 2021
# Execution time and date (local): Thu Dec 23 12:57:26 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          5793           MPI          0 11004289.763672            2 
            2          5793           MPI          1 11027555.703125            4 
            2          5793           MPI          2 11020925.058594            1 
            2          5793           MPI          3 11052322.044922            0 
            2          5793           MPI          4 11018847.466797            5 
            2          5793           MPI          5 10998451.824219            0 
            2          5793           MPI          6 11000963.615234            0 
            2          5793           MPI          7 10999808.542969            0 
            2          5793           MPI          8 11008557.185547            5 
            2          5793           MPI          9 11041845.845703            0 
# Runtime: 139.209540 s (overhead: 0.000012 %) 10 records
