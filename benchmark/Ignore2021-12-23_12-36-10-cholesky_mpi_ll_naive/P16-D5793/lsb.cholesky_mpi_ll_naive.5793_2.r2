# Sysname : Linux
# Nodename: eu-a6-007-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 12:08:07 2021
# Execution time and date (local): Thu Dec 23 13:08:07 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          5793           MPI          0 3975009.035156            0 
            2          5793           MPI          1 3953800.775391            8 
            2          5793           MPI          2 3905838.857422            6 
            2          5793           MPI          3 3960722.906250            0 
            2          5793           MPI          4 4002752.000000            2 
            2          5793           MPI          5 4003031.583984            0 
            2          5793           MPI          6 3958559.789062            0 
            2          5793           MPI          7 3944632.996094            0 
            2          5793           MPI          8 4020968.021484            6 
            2          5793           MPI          9 4014269.910156            0 
# Runtime: 52.862078 s (overhead: 0.000042 %) 10 records
