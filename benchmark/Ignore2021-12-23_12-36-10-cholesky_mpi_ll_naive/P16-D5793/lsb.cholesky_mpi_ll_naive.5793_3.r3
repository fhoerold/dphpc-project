# Sysname : Linux
# Nodename: eu-a6-007-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 12:08:07 2021
# Execution time and date (local): Thu Dec 23 13:08:07 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          5793           MPI          0 3972815.228516            4 
            3          5793           MPI          1 3951582.798828            9 
            3          5793           MPI          2 3903663.591797            1 
            3          5793           MPI          3 3958518.626953            0 
            3          5793           MPI          4 4000523.384766            1 
            3          5793           MPI          5 4000802.849609            0 
            3          5793           MPI          6 3956362.253906            0 
            3          5793           MPI          7 3942437.417969            0 
            3          5793           MPI          8 4018729.154297            4 
            3          5793           MPI          9 4012035.607422            0 
# Runtime: 55.829824 s (overhead: 0.000034 %) 10 records
