# Sysname : Linux
# Nodename: eu-a6-007-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 11:36:33 2021
# Execution time and date (local): Thu Dec 23 12:36:33 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1448           MPI          0 189111.310547            0 
            3          1448           MPI          1 189352.781250            4 
            3          1448           MPI          2 187024.824219            1 
            3          1448           MPI          3 185705.761719            0 
            3          1448           MPI          4 185390.042969            1 
            3          1448           MPI          5 188687.833984            0 
            3          1448           MPI          6 189380.255859            0 
            3          1448           MPI          7 188998.314453            0 
            3          1448           MPI          8 190678.269531            0 
            3          1448           MPI          9 188455.537109            0 
# Runtime: 7.268425 s (overhead: 0.000083 %) 10 records
