# Sysname : Linux
# Nodename: eu-a6-007-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 11:36:33 2021
# Execution time and date (local): Thu Dec 23 12:36:33 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1448           MPI          0 189129.468750            0 
            2          1448           MPI          1 189780.330078            1 
            2          1448           MPI          2 187024.015625            1 
            2          1448           MPI          3 185712.755859            0 
            2          1448           MPI          4 185404.607422            3 
            2          1448           MPI          5 188695.664062            0 
            2          1448           MPI          6 189388.201172            0 
            2          1448           MPI          7 189006.318359            0 
            2          1448           MPI          8 190685.570312            1 
            2          1448           MPI          9 188462.837891            0 
# Runtime: 2.267604 s (overhead: 0.000265 %) 10 records
