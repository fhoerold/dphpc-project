# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 13:35:15 2021
# Execution time and date (local): Thu Dec 23 14:35:15 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          5793           MPI          0 4560899.326172            0 
            3          5793           MPI          1 4558121.654297            5 
            3          5793           MPI          2 4560223.220703            5 
            3          5793           MPI          3 4573179.814453            0 
            3          5793           MPI          4 4570730.460938            6 
            3          5793           MPI          5 4575864.501953            0 
            3          5793           MPI          6 4581609.271484            0 
            3          5793           MPI          7 4603022.845703            0 
            3          5793           MPI          8 4629876.955078            5 
            3          5793           MPI          9 4595536.419922            0 
# Runtime: 61.340228 s (overhead: 0.000034 %) 10 records
