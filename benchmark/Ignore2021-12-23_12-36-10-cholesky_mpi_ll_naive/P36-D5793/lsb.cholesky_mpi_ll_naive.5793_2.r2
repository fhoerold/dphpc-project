# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 13:35:15 2021
# Execution time and date (local): Thu Dec 23 14:35:15 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          5793           MPI          0 4559063.994141            3 
            2          5793           MPI          1 4556236.830078            8 
            2          5793           MPI          2 4558340.564453           11 
            2          5793           MPI          3 4571294.275391            0 
            2          5793           MPI          4 4568837.220703            1 
            2          5793           MPI          5 4573985.164062            0 
            2          5793           MPI          6 4579722.234375            0 
            2          5793           MPI          7 4601117.964844            0 
            2          5793           MPI          8 4627967.875000            7 
            2          5793           MPI          9 4593643.800781            0 
# Runtime: 61.313082 s (overhead: 0.000049 %) 10 records
