# Sysname : Linux
# Nodename: eu-a6-006-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 12:12:39 2021
# Execution time and date (local): Thu Dec 23 13:12:39 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1448           MPI          0 201358.322266            2 
            3          1448           MPI          1 201519.669922            3 
            3          1448           MPI          2 205226.476562            5 
            3          1448           MPI          3 204432.785156            0 
            3          1448           MPI          4 205325.136719            3 
            3          1448           MPI          5 205257.117188            0 
            3          1448           MPI          6 205113.087891            0 
            3          1448           MPI          7 204935.873047            0 
            3          1448           MPI          8 205179.626953            0 
            3          1448           MPI          9 205285.064453            0 
# Runtime: 5.487358 s (overhead: 0.000237 %) 10 records
