# Sysname : Linux
# Nodename: eu-a6-006-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 12:12:39 2021
# Execution time and date (local): Thu Dec 23 13:12:39 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1448           MPI          0 201534.937500            2 
            2          1448           MPI          1 201678.107422            3 
            2          1448           MPI          2 205389.312500            0 
            2          1448           MPI          3 204591.439453            0 
            2          1448           MPI          4 205487.966797            5 
            2          1448           MPI          5 205420.117188            0 
            2          1448           MPI          6 205275.623047            0 
            2          1448           MPI          7 205098.466797            0 
            2          1448           MPI          8 205341.570312            0 
            2          1448           MPI          9 205451.843750            0 
# Runtime: 2.503390 s (overhead: 0.000399 %) 10 records
