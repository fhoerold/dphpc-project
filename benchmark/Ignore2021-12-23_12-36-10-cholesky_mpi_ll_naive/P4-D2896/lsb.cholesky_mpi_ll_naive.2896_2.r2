# Sysname : Linux
# Nodename: eu-a6-007-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 11:38:46 2021
# Execution time and date (local): Thu Dec 23 12:38:46 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2896           MPI          0 1346871.525391            0 
            2          2896           MPI          1 1346017.710938            5 
            2          2896           MPI          2 1342363.453125            1 
            2          2896           MPI          3 1345552.115234            0 
            2          2896           MPI          4 1344864.193359            0 
            2          2896           MPI          5 1342577.105469            0 
            2          2896           MPI          6 1343842.136719            0 
            2          2896           MPI          7 1342976.892578            0 
            2          2896           MPI          8 1345355.400391            1 
            2          2896           MPI          9 1344488.585938            0 
# Runtime: 21.125360 s (overhead: 0.000033 %) 10 records
