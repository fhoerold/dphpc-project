# Sysname : Linux
# Nodename: eu-a6-007-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 11:38:46 2021
# Execution time and date (local): Thu Dec 23 12:38:46 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2896           MPI          0 1346969.402344            0 
            3          2896           MPI          1 1346144.117188            4 
            3          2896           MPI          2 1342492.000000            1 
            3          2896           MPI          3 1345683.068359            0 
            3          2896           MPI          4 1344992.720703            0 
            3          2896           MPI          5 1342706.296875            0 
            3          2896           MPI          6 1343971.109375            0 
            3          2896           MPI          7 1343105.349609            0 
            3          2896           MPI          8 1345484.523438            1 
            3          2896           MPI          9 1344617.300781            0 
# Runtime: 22.133659 s (overhead: 0.000027 %) 10 records
