# Sysname : Linux
# Nodename: eu-a6-006-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 12:37:32 2021
# Execution time and date (local): Thu Dec 23 13:37:32 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          5793           MPI          0 4304886.796875            0 
            3          5793           MPI          1 4302868.029297            1 
            3          5793           MPI          2 4279862.371094            1 
            3          5793           MPI          3 4283659.830078            0 
            3          5793           MPI          4 4293809.210938            9 
            3          5793           MPI          5 4319115.650391            0 
            3          5793           MPI          6 4320677.449219            0 
            3          5793           MPI          7 4292798.617188            0 
            3          5793           MPI          8 4286338.320312            1 
            3          5793           MPI          9 4281184.373047            0 
# Runtime: 70.900211 s (overhead: 0.000017 %) 10 records
