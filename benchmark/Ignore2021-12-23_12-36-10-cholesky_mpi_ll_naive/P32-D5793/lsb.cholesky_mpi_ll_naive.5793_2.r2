# Sysname : Linux
# Nodename: eu-a6-006-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 12:37:32 2021
# Execution time and date (local): Thu Dec 23 13:37:32 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          5793           MPI          0 4305756.531250            0 
            2          5793           MPI          1 4303731.132812            1 
            2          5793           MPI          2 4280723.785156            1 
            2          5793           MPI          3 4284516.380859            0 
            2          5793           MPI          4 4294678.832031            8 
            2          5793           MPI          5 4319985.402344            0 
            2          5793           MPI          6 4321554.597656            0 
            2          5793           MPI          7 4293666.087891            0 
            2          5793           MPI          8 4287205.603516            6 
            2          5793           MPI          9 4282050.634766            0 
# Runtime: 70.924672 s (overhead: 0.000023 %) 10 records
