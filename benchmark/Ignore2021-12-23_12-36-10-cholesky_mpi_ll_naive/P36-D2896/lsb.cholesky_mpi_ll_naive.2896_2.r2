# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 13:07:56 2021
# Execution time and date (local): Thu Dec 23 14:07:56 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2896           MPI          0 993368.613281            0 
            2          2896           MPI          1 994881.966797            1 
            2          2896           MPI          2 996697.781250            1 
            2          2896           MPI          3 995951.650391            0 
            2          2896           MPI          4 994194.187500            2 
            2          2896           MPI          5 993435.300781            0 
            2          2896           MPI          6 991045.789062            0 
            2          2896           MPI          7 991865.779297            0 
            2          2896           MPI          8 989881.244141            7 
            2          2896           MPI          9 995471.056641            0 
# Runtime: 18.988225 s (overhead: 0.000058 %) 10 records
