# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 13:07:56 2021
# Execution time and date (local): Thu Dec 23 14:07:56 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2896           MPI          0 993512.113281            0 
            3          2896           MPI          1 995009.146484            1 
            3          2896           MPI          2 996820.306641            1 
            3          2896           MPI          3 996079.923828            0 
            3          2896           MPI          4 994322.208984            0 
            3          2896           MPI          5 993563.457031            0 
            3          2896           MPI          6 991166.314453            0 
            3          2896           MPI          7 991992.765625            0 
            3          2896           MPI          8 990008.939453            1 
            3          2896           MPI          9 995608.921875            0 
# Runtime: 18.972562 s (overhead: 0.000016 %) 10 records
