# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 11:37:01 2021
# Execution time and date (local): Thu Dec 23 12:37:01 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1448           MPI          0 99552.990234            0 
            3          1448           MPI          1 99234.417969            3 
            3          1448           MPI          2 99242.203125            0 
            3          1448           MPI          3 98932.808594            0 
            3          1448           MPI          4 98922.771484            0 
            3          1448           MPI          5 98691.953125            0 
            3          1448           MPI          6 98856.910156            0 
            3          1448           MPI          7 99041.556641            0 
            3          1448           MPI          8 99203.027344            0 
            3          1448           MPI          9 98914.560547            0 
# Runtime: 8.212247 s (overhead: 0.000037 %) 10 records
