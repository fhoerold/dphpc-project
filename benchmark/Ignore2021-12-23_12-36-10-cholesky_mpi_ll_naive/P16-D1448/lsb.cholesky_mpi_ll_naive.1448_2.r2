# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 11:37:01 2021
# Execution time and date (local): Thu Dec 23 12:37:01 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1448           MPI          0 99631.466797            2 
            2          1448           MPI          1 99230.910156            3 
            2          1448           MPI          2 99238.593750            0 
            2          1448           MPI          3 98929.072266            0 
            2          1448           MPI          4 98919.451172            0 
            2          1448           MPI          5 98691.343750            0 
            2          1448           MPI          6 98853.455078            0 
            2          1448           MPI          7 99037.863281            0 
            2          1448           MPI          8 99199.308594            0 
            2          1448           MPI          9 98911.066406            0 
# Runtime: 8.208284 s (overhead: 0.000061 %) 10 records
