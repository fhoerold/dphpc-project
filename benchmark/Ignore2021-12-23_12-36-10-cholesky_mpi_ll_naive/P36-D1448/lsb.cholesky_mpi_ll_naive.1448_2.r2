# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 13:05:12 2021
# Execution time and date (local): Thu Dec 23 14:05:12 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1448           MPI          0 241409.173828            0 
            2          1448           MPI          1 239562.884766            4 
            2          1448           MPI          2 237081.380859            1 
            2          1448           MPI          3 236988.591797            0 
            2          1448           MPI          4 235640.382812            0 
            2          1448           MPI          5 236753.517578            0 
            2          1448           MPI          6 237226.466797            0 
            2          1448           MPI          7 237363.650391            0 
            2          1448           MPI          8 237354.939453            0 
            2          1448           MPI          9 238338.464844            0 
# Runtime: 5.878345 s (overhead: 0.000085 %) 10 records
