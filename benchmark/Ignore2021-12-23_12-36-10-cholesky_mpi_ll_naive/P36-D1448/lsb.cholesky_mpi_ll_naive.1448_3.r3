# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 13:05:12 2021
# Execution time and date (local): Thu Dec 23 14:05:12 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1448           MPI          0 241418.363281            4 
            3          1448           MPI          1 239567.335938            3 
            3          1448           MPI          2 237080.158203            3 
            3          1448           MPI          3 236996.298828            0 
            3          1448           MPI          4 235646.505859            0 
            3          1448           MPI          5 236759.349609            0 
            3          1448           MPI          6 237233.136719            0 
            3          1448           MPI          7 237370.425781            0 
            3          1448           MPI          8 237360.884766            0 
            3          1448           MPI          9 238344.343750            0 
# Runtime: 13.876209 s (overhead: 0.000072 %) 10 records
