# Sysname : Linux
# Nodename: eu-a6-009-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 11:38:53 2021
# Execution time and date (local): Thu Dec 23 12:38:53 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2896           MPI          0 859387.462891            0 
            3          2896           MPI          1 864544.021484            1 
            3          2896           MPI          2 858606.359375            0 
            3          2896           MPI          3 861809.626953            0 
            3          2896           MPI          4 867080.640625            1 
            3          2896           MPI          5 863704.539062            0 
            3          2896           MPI          6 860169.218750            0 
            3          2896           MPI          7 859814.683594            0 
            3          2896           MPI          8 860522.871094            1 
            3          2896           MPI          9 860758.986328            0 
# Runtime: 17.350125 s (overhead: 0.000017 %) 10 records
