# Sysname : Linux
# Nodename: eu-a6-009-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 11:38:53 2021
# Execution time and date (local): Thu Dec 23 12:38:53 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2896           MPI          0 859607.384766            0 
            2          2896           MPI          1 864552.191406            1 
            2          2896           MPI          2 858351.921875            2 
            2          2896           MPI          3 861810.271484            0 
            2          2896           MPI          4 867079.468750            1 
            2          2896           MPI          5 863704.560547            0 
            2          2896           MPI          6 860167.855469            0 
            2          2896           MPI          7 859814.255859            0 
            2          2896           MPI          8 860522.830078            2 
            2          2896           MPI          9 860757.724609            0 
# Runtime: 10.351345 s (overhead: 0.000058 %) 10 records
