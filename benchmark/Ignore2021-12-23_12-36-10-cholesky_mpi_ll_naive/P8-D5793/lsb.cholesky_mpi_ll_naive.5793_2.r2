# Sysname : Linux
# Nodename: eu-a6-009-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 12:01:23 2021
# Execution time and date (local): Thu Dec 23 13:01:23 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          5793           MPI          0 6930931.613281            0 
            2          5793           MPI          1 6900709.876953            2 
            2          5793           MPI          2 6900518.287109            6 
            2          5793           MPI          3 6913474.074219            0 
            2          5793           MPI          4 6901469.382812            2 
            2          5793           MPI          5 6918515.695312            0 
            2          5793           MPI          6 6926867.291016            0 
            2          5793           MPI          7 6988621.009766            0 
            2          5793           MPI          8 6927775.451172            2 
            2          5793           MPI          9 6909454.169922            0 
# Runtime: 84.163905 s (overhead: 0.000014 %) 10 records
