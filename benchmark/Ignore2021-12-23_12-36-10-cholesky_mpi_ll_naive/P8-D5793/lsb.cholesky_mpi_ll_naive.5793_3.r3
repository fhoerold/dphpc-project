# Sysname : Linux
# Nodename: eu-a6-009-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 12:01:23 2021
# Execution time and date (local): Thu Dec 23 13:01:23 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          5793           MPI          0 6930917.986328            0 
            3          5793           MPI          1 6900705.179688            2 
            3          5793           MPI          2 6900514.021484            7 
            3          5793           MPI          3 6913472.683594            0 
            3          5793           MPI          4 6901464.427734            1 
            3          5793           MPI          5 6918512.023438            0 
            3          5793           MPI          6 6926866.408203            0 
            3          5793           MPI          7 6988618.111328            0 
            3          5793           MPI          8 6927771.392578            2 
            3          5793           MPI          9 6909450.619141            0 
# Runtime: 84.163961 s (overhead: 0.000014 %) 10 records
