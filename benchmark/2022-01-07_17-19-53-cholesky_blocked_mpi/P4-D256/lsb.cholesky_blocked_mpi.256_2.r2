# Sysname : Linux
# Nodename: eu-a6-004-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:20:21 2022
# Execution time and date (local): Fri Jan  7 17:20:21 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           256             1           MPI          0 6133.177734            0 
            2           256             1           MPI          1 5783.205078            1 
            2           256             1           MPI          2 5793.787109            1 
            2           256             1           MPI          3 5755.210938            0 
            2           256             1           MPI          4 6006.849609            1 
# Runtime: 13.045398 s (overhead: 0.000023 %) 5 records
