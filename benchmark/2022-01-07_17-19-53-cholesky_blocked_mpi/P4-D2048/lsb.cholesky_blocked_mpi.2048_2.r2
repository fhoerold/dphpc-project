# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:21:07 2022
# Execution time and date (local): Fri Jan  7 17:21:07 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048             1           MPI          0 1915753.904297            0 
            2          2048             1           MPI          1 1887571.056641            2 
            2          2048             1           MPI          2 2004768.273438            2 
            2          2048             1           MPI          3 2205529.417969            0 
            2          2048             1           MPI          4 2026113.031250            1 
# Runtime: 18.436943 s (overhead: 0.000027 %) 5 records
