# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:21:07 2022
# Execution time and date (local): Fri Jan  7 17:21:07 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048             1           MPI          0 1915688.478516            0 
            3          2048             1           MPI          1 1887368.507812            1 
            3          2048             1           MPI          2 2004685.789062            1 
            3          2048             1           MPI          3 2205435.746094            0 
            3          2048             1           MPI          4 2026375.826172            1 
# Runtime: 14.432428 s (overhead: 0.000021 %) 5 records
