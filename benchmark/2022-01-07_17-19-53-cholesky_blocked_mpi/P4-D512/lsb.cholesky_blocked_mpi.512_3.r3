# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:20:22 2022
# Execution time and date (local): Fri Jan  7 17:20:22 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512             1           MPI          0 41253.234375            0 
            3           512             1           MPI          1 40114.644531            1 
            3           512             1           MPI          2 39356.396484            1 
            3           512             1           MPI          3 40671.224609            0 
            3           512             1           MPI          4 40596.433594            1 
# Runtime: 4.283018 s (overhead: 0.000070 %) 5 records
