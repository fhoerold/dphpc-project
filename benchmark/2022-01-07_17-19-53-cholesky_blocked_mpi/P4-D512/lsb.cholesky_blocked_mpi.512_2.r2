# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:20:22 2022
# Execution time and date (local): Fri Jan  7 17:20:22 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512             1           MPI          0 41242.156250            3 
            2           512             1           MPI          1 40478.279297            1 
            2           512             1           MPI          2 39355.152344            1 
            2           512             1           MPI          3 40670.072266            0 
            2           512             1           MPI          4 40595.015625            1 
# Runtime: 6.283586 s (overhead: 0.000095 %) 5 records
