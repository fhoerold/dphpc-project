# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:20:29 2022
# Execution time and date (local): Fri Jan  7 22:20:29 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024             1           MPI          0 240260.191406            0 
            2          1024             1           MPI          1 242135.095703            3 
            2          1024             1           MPI          2 238875.671875            3 
            2          1024             1           MPI          3 239463.111328            0 
            2          1024             1           MPI          4 238384.017578            3 
# Runtime: 5.683814 s (overhead: 0.000158 %) 5 records
