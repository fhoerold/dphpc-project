# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:20:29 2022
# Execution time and date (local): Fri Jan  7 22:20:29 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024             1           MPI          0 240484.955078            0 
            3          1024             1           MPI          1 242241.660156            5 
            3          1024             1           MPI          2 238977.099609            3 
            3          1024             1           MPI          3 239391.320312            0 
            3          1024             1           MPI          4 238485.240234            3 
# Runtime: 1.681679 s (overhead: 0.000654 %) 5 records
