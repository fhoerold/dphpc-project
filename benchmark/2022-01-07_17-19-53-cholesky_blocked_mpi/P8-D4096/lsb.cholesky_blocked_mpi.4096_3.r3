# Sysname : Linux
# Nodename: eu-a6-009-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:29:43 2022
# Execution time and date (local): Fri Jan  7 17:29:43 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096             1           MPI          0 28638190.833984            0 
            3          4096             1           MPI          1 28731194.111328           11 
            3          4096             1           MPI          2 28855949.515625            9 
            3          4096             1           MPI          3 29214929.625000            0 
            3          4096             1           MPI          4 28603143.234375            9 
# Runtime: 210.416744 s (overhead: 0.000014 %) 5 records
