# Sysname : Linux
# Nodename: eu-a6-009-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:29:43 2022
# Execution time and date (local): Fri Jan  7 17:29:43 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096             1           MPI          0 28663130.359375            0 
            2          4096             1           MPI          1 28756234.941406            9 
            2          4096             1           MPI          2 28881087.750000            8 
            2          4096             1           MPI          3 29240381.564453            6 
            2          4096             1           MPI          4 28628054.599609           12 
# Runtime: 207.608053 s (overhead: 0.000017 %) 5 records
