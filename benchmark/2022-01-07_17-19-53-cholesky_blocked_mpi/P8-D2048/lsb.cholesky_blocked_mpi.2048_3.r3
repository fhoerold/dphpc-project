# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:22:24 2022
# Execution time and date (local): Fri Jan  7 17:22:24 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048             1           MPI          0 2003103.800781            0 
            3          2048             1           MPI          1 2023451.962891            1 
            3          2048             1           MPI          2 2021098.328125            1 
            3          2048             1           MPI          3 1995881.089844            0 
            3          2048             1           MPI          4 2008297.677734            1 
# Runtime: 16.188378 s (overhead: 0.000019 %) 5 records
