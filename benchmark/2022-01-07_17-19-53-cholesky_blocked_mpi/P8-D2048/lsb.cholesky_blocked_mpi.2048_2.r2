# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:22:24 2022
# Execution time and date (local): Fri Jan  7 17:22:24 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048             1           MPI          0 2004015.798828            0 
            2          2048             1           MPI          1 2024368.439453            2 
            2          2048             1           MPI          2 2022018.167969            1 
            2          2048             1           MPI          3 1996789.306641            0 
            2          2048             1           MPI          4 2009208.503906            2 
# Runtime: 15.198644 s (overhead: 0.000033 %) 5 records
