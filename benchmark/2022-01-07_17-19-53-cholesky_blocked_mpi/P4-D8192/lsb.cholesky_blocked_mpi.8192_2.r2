# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:03:48 2022
# Execution time and date (local): Fri Jan  7 19:03:48 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192             1           MPI          0 131015577.939453            0 
            2          8192             1           MPI          1 137757006.728516            9 
            2          8192             1           MPI          2 220779448.300781           10 
            2          8192             1           MPI          3 233507416.652344            4 
            2          8192             1           MPI          4 196359881.390625            8 
# Runtime: 1207.912196 s (overhead: 0.000003 %) 5 records
