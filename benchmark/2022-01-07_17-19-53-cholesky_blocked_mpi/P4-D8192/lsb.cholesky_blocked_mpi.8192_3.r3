# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:03:48 2022
# Execution time and date (local): Fri Jan  7 19:03:48 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192             1           MPI          0 131022842.101562            0 
            3          8192             1           MPI          1 137764652.972656            9 
            3          8192             1           MPI          2 220791694.345703            9 
            3          8192             1           MPI          3 233520370.066406            3 
            3          8192             1           MPI          4 196370775.521484            8 
# Runtime: 1209.979134 s (overhead: 0.000002 %) 5 records
