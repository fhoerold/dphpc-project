# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 06:59:19 2022
# Execution time and date (local): Sat Jan  8 07:59:19 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192             1           MPI          0 211276111.515625            2 
            2          8192             1           MPI          1 212232137.873047            6 
            2          8192             1           MPI          2 211589473.441406            8 
            2          8192             1           MPI          3 210911945.376953            2 
            2          8192             1           MPI          4 212183348.794922            7 
# Runtime: 1499.005373 s (overhead: 0.000002 %) 5 records
