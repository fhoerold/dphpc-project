# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 06:59:19 2022
# Execution time and date (local): Sat Jan  8 07:59:19 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192             1           MPI          0 211301897.455078            0 
            3          8192             1           MPI          1 212258040.222656            8 
            3          8192             1           MPI          2 211615299.271484            9 
            3          8192             1           MPI          3 210937686.720703            3 
            3          8192             1           MPI          4 212209247.552734            8 
# Runtime: 1494.176532 s (overhead: 0.000002 %) 5 records
