# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:27:30 2022
# Execution time and date (local): Fri Jan  7 17:27:30 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096             1           MPI          0 17907071.148438            0 
            3          4096             1           MPI          1 17613994.402344            7 
            3          4096             1           MPI          2 17715289.949219            5 
            3          4096             1           MPI          3 18336115.056641            0 
            3          4096             1           MPI          4 17572015.865234           15 
# Runtime: 124.209494 s (overhead: 0.000022 %) 5 records
