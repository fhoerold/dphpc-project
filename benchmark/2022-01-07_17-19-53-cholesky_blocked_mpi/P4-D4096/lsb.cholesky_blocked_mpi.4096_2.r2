# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:27:30 2022
# Execution time and date (local): Fri Jan  7 17:27:30 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096             1           MPI          0 17906919.937500            0 
            2          4096             1           MPI          1 17613824.093750            8 
            2          4096             1           MPI          2 17715121.285156            6 
            2          4096             1           MPI          3 18335940.060547            0 
            2          4096             1           MPI          4 17571844.501953           18 
# Runtime: 124.208931 s (overhead: 0.000026 %) 5 records
