# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:08:19 2022
# Execution time and date (local): Fri Jan  7 19:08:19 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192             1           MPI          0 218568828.542969            0 
            2          8192             1           MPI          1 206176110.851562            6 
            2          8192             1           MPI          2 181117757.019531            9 
            2          8192             1           MPI          3 190621035.218750            4 
            2          8192             1           MPI          4 199345546.349609            9 
# Runtime: 1415.738087 s (overhead: 0.000002 %) 5 records
