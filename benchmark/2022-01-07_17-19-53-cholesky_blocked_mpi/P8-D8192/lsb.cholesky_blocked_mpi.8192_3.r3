# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:08:19 2022
# Execution time and date (local): Fri Jan  7 19:08:19 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192             1           MPI          0 218581634.212891            0 
            3          8192             1           MPI          1 206188206.898438            6 
            3          8192             1           MPI          2 181128376.775391            8 
            3          8192             1           MPI          3 190632201.904297            3 
            3          8192             1           MPI          4 199357231.345703            8 
# Runtime: 1413.820803 s (overhead: 0.000002 %) 5 records
