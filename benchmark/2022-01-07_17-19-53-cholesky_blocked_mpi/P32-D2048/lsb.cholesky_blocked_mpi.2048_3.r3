# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:16:25 2022
# Execution time and date (local): Sat Jan  8 05:16:25 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048             1           MPI          0 2666053.080078            0 
            3          2048             1           MPI          1 2682683.267578            1 
            3          2048             1           MPI          2 2694943.179688            2 
            3          2048             1           MPI          3 2683643.011719            0 
            3          2048             1           MPI          4 2681672.703125            1 
# Runtime: 28.273901 s (overhead: 0.000014 %) 5 records
