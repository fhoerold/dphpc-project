# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:16:25 2022
# Execution time and date (local): Sat Jan  8 05:16:25 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048             1           MPI          0 2666171.130859            0 
            2          2048             1           MPI          1 2682800.425781            4 
            2          2048             1           MPI          2 2695069.751953            5 
            2          2048             1           MPI          3 2683753.267578            0 
            2          2048             1           MPI          4 2681782.457031            6 
# Runtime: 25.272208 s (overhead: 0.000059 %) 5 records
