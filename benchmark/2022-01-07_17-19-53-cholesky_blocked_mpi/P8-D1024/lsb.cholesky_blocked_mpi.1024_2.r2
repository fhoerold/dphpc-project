# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:21:25 2022
# Execution time and date (local): Fri Jan  7 17:21:25 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024             1           MPI          0 258893.947266            0 
            2          1024             1           MPI          1 251909.136719            1 
            2          1024             1           MPI          2 242353.441406            2 
            2          1024             1           MPI          3 257603.175781            0 
            2          1024             1           MPI          4 264057.519531            1 
# Runtime: 3.821982 s (overhead: 0.000105 %) 5 records
