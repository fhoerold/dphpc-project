# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:21:25 2022
# Execution time and date (local): Fri Jan  7 17:21:25 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024             1           MPI          0 258624.414062            0 
            3          1024             1           MPI          1 251347.957031            1 
            3          1024             1           MPI          2 241824.332031            1 
            3          1024             1           MPI          3 257031.037109            0 
            3          1024             1           MPI          4 263476.544922            1 
# Runtime: 1.803046 s (overhead: 0.000166 %) 5 records
