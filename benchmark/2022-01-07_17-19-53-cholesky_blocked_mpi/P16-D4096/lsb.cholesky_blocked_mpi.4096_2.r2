# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:30:48 2022
# Execution time and date (local): Fri Jan  7 22:30:48 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096             1           MPI          0 20831794.542969            0 
            2          4096             1           MPI          1 20801183.361328            6 
            2          4096             1           MPI          2 20950039.187500            4 
            2          4096             1           MPI          3 20857303.550781            4 
            2          4096             1           MPI          4 20909398.490234            6 
# Runtime: 150.083962 s (overhead: 0.000013 %) 5 records
