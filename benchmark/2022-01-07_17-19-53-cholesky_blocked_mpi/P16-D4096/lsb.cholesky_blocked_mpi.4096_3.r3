# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:30:48 2022
# Execution time and date (local): Fri Jan  7 22:30:48 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096             1           MPI          0 20837070.906250            0 
            3          4096             1           MPI          1 20806452.964844            6 
            3          4096             1           MPI          2 20955346.009766            4 
            3          4096             1           MPI          3 20862583.046875            2 
            3          4096             1           MPI          4 20914685.281250            6 
# Runtime: 150.131572 s (overhead: 0.000012 %) 5 records
