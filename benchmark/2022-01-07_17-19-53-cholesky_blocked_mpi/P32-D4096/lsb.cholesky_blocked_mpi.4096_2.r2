# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:41:20 2022
# Execution time and date (local): Sat Jan  8 05:41:20 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096             1           MPI          0 26395213.572266            0 
            2          4096             1           MPI          1 26348434.193359            6 
            2          4096             1           MPI          2 26262524.048828            5 
            2          4096             1           MPI          3 26390287.640625            4 
            2          4096             1           MPI          4 26441957.867188            7 
# Runtime: 199.956992 s (overhead: 0.000011 %) 5 records
