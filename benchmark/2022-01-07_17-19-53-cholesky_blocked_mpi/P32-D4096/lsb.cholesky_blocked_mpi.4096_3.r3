# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:41:20 2022
# Execution time and date (local): Sat Jan  8 05:41:20 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096             1           MPI          0 26395574.738281            3 
            3          4096             1           MPI          1 26348786.707031            7 
            3          4096             1           MPI          2 26262890.859375            7 
            3          4096             1           MPI          3 26390633.443359            0 
            3          4096             1           MPI          4 26442329.781250            2 
# Runtime: 202.960087 s (overhead: 0.000009 %) 5 records
