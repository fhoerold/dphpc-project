# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:14:59 2022
# Execution time and date (local): Sat Jan  8 05:14:59 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           256             1           MPI          0 13650.689453            0 
            3           256             1           MPI          1 11566.742188            1 
            3           256             1           MPI          2 11491.126953            0 
            3           256             1           MPI          3 11279.855469            0 
            3           256             1           MPI          4 12097.201172            0 
# Runtime: 13.086884 s (overhead: 0.000008 %) 5 records
