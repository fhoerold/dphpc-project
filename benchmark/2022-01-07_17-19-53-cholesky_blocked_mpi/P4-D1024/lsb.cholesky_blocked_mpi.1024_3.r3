# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:20:25 2022
# Execution time and date (local): Fri Jan  7 17:20:25 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024             1           MPI          0 258563.615234            0 
            3          1024             1           MPI          1 253356.785156            1 
            3          1024             1           MPI          2 255540.263672            1 
            3          1024             1           MPI          3 252843.675781            0 
            3          1024             1           MPI          4 258505.941406            1 
# Runtime: 6.815921 s (overhead: 0.000044 %) 5 records
