# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:20:25 2022
# Execution time and date (local): Fri Jan  7 17:20:25 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024             1           MPI          0 258540.935547            0 
            2          1024             1           MPI          1 253335.464844            2 
            2          1024             1           MPI          2 255521.875000            2 
            2          1024             1           MPI          3 252825.496094            0 
            2          1024             1           MPI          4 258487.103516            1 
# Runtime: 1.805278 s (overhead: 0.000277 %) 5 records
