# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:33:05 2022
# Execution time and date (local): Sat Jan  8 05:33:05 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024             1           MPI          0 361313.666016            1 
            2          1024             1           MPI          1 356010.832031            4 
            2          1024             1           MPI          2 358737.847656            3 
            2          1024             1           MPI          3 353754.996094            1 
            2          1024             1           MPI          4 353996.208984            3 
# Runtime: 10.551830 s (overhead: 0.000114 %) 5 records
