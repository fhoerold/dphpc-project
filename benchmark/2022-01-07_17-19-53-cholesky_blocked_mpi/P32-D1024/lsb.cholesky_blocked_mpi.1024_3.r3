# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:33:05 2022
# Execution time and date (local): Sat Jan  8 05:33:05 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024             1           MPI          0 361303.486328            0 
            3          1024             1           MPI          1 356012.437500            1 
            3          1024             1           MPI          2 358741.591797            1 
            3          1024             1           MPI          3 353758.113281            0 
            3          1024             1           MPI          4 353999.853516            1 
# Runtime: 7.549783 s (overhead: 0.000040 %) 5 records
