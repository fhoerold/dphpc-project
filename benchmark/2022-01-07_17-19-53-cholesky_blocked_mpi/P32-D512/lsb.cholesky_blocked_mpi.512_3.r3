# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:15:21 2022
# Execution time and date (local): Sat Jan  8 05:15:21 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512             1           MPI          0 53695.664062            0 
            3           512             1           MPI          1 53532.402344            1 
            3           512             1           MPI          2 53005.238281            1 
            3           512             1           MPI          3 53118.570312            0 
            3           512             1           MPI          4 53035.062500            1 
# Runtime: 0.386031 s (overhead: 0.000777 %) 5 records
