# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:15:21 2022
# Execution time and date (local): Sat Jan  8 05:15:21 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512             1           MPI          0 53803.300781            0 
            2           512             1           MPI          1 53542.468750            1 
            2           512             1           MPI          2 53006.982422            1 
            2           512             1           MPI          3 53119.539062            0 
            2           512             1           MPI          4 53037.767578            1 
# Runtime: 2.395670 s (overhead: 0.000125 %) 5 records
