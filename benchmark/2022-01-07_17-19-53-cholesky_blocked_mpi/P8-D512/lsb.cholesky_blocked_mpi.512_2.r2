# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:20:52 2022
# Execution time and date (local): Fri Jan  7 17:20:52 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512             1           MPI          0 34380.460938            0 
            2           512             1           MPI          1 35047.310547            1 
            2           512             1           MPI          2 35043.687500            1 
            2           512             1           MPI          3 34063.539062            0 
            2           512             1           MPI          4 33963.496094            1 
# Runtime: 1.246115 s (overhead: 0.000241 %) 5 records
