# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:20:52 2022
# Execution time and date (local): Fri Jan  7 17:20:52 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512             1           MPI          0 34723.998047            0 
            3           512             1           MPI          1 34686.173828            1 
            3           512             1           MPI          2 34701.841797            1 
            3           512             1           MPI          3 34063.728516            0 
            3           512             1           MPI          4 33968.210938            1 
# Runtime: 1.245541 s (overhead: 0.000241 %) 5 records
