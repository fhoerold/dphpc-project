# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 23:56:15 2022
# Execution time and date (local): Sat Jan  8 00:56:15 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192             1           MPI          0 160647518.005859            0 
            3          8192             1           MPI          1 158790741.808594            7 
            3          8192             1           MPI          2 160559850.097656            7 
            3          8192             1           MPI          3 159346946.675781            2 
            3          8192             1           MPI          4 159534051.687500            9 
# Runtime: 1133.460961 s (overhead: 0.000002 %) 5 records
