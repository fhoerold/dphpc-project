# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 23:56:15 2022
# Execution time and date (local): Sat Jan  8 00:56:15 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192             1           MPI          0 160648176.794922            0 
            2          8192             1           MPI          1 158791393.570312            7 
            2          8192             1           MPI          2 160560509.582031            7 
            2          8192             1           MPI          3 159347601.460938            5 
            2          8192             1           MPI          4 159534707.341797            8 
# Runtime: 1133.472026 s (overhead: 0.000002 %) 5 records
