# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:20:52 2022
# Execution time and date (local): Fri Jan  7 17:20:52 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           256             1           MPI          0 7466.947266            0 
            2           256             1           MPI          1 7668.085938            1 
            2           256             1           MPI          2 7449.148438            0 
            2           256             1           MPI          3 7398.144531            0 
            2           256             1           MPI          4 7273.810547            0 
# Runtime: 0.056287 s (overhead: 0.001777 %) 5 records
