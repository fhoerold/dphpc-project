# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:20:52 2022
# Execution time and date (local): Fri Jan  7 17:20:52 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           256             1           MPI          0 7776.378906            0 
            3           256             1           MPI          1 7355.267578            1 
            3           256             1           MPI          2 7451.539062            0 
            3           256             1           MPI          3 7398.947266            0 
            3           256             1           MPI          4 7279.248047            0 
# Runtime: 1.057318 s (overhead: 0.000095 %) 5 records
