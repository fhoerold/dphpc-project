# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 20:42:45 2022
# Execution time and date (local): Fri Jan  7 21:42:45 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512             1           MPI          0 38870.871094            0 
            3           512             1           MPI          1 39117.734375            1 
            3           512             1           MPI          2 38264.496094            1 
            3           512             1           MPI          3 37768.011719            0 
            3           512             1           MPI          4 38181.775391            1 
# Runtime: 0.280015 s (overhead: 0.001071 %) 5 records
