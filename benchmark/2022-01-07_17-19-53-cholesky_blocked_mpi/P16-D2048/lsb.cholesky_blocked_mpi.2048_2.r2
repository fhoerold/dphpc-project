# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 20:43:45 2022
# Execution time and date (local): Fri Jan  7 21:43:45 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048             1           MPI          0 2958820.138672            0 
            2          2048             1           MPI          1 2559384.171875            3 
            2          2048             1           MPI          2 2520617.066406            4 
            2          2048             1           MPI          3 2502008.164062            0 
            2          2048             1           MPI          4 2518880.611328            5 
# Runtime: 22.418807 s (overhead: 0.000054 %) 5 records
