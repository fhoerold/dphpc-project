# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 20:43:45 2022
# Execution time and date (local): Fri Jan  7 21:43:45 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048             1           MPI          0 2958369.761719            0 
            3          2048             1           MPI          1 2558993.712891            1 
            3          2048             1           MPI          2 2520232.433594            1 
            3          2048             1           MPI          3 2501626.623047            0 
            3          2048             1           MPI          4 2518496.208984            1 
# Runtime: 25.414953 s (overhead: 0.000012 %) 5 records
