# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan 13 10:19:56 2022
# Execution time and date (local): Thu Jan 13 11:19:56 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096            16           MPI          0 4715841.490234            0 
            2          4096            16           MPI          1 4737467.003906            2 
            2          4096            16           MPI          2 4749719.470703            8 
            2          4096            16           MPI          3 4756942.669922            0 
            2          4096            16           MPI          4 4752117.201172            6 
# Runtime: 34.416537 s (overhead: 0.000046 %) 5 records
