# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan 13 10:19:56 2022
# Execution time and date (local): Thu Jan 13 11:19:56 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096            16           MPI          0 4716178.082031            0 
            3          4096            16           MPI          1 4737815.927734            6 
            3          4096            16           MPI          2 4750060.710938            6 
            3          4096            16           MPI          3 4757281.853516            0 
            3          4096            16           MPI          4 4752449.644531            6 
# Runtime: 38.427995 s (overhead: 0.000047 %) 5 records
