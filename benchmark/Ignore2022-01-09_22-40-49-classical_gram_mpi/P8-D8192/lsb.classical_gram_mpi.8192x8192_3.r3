# Sysname : Linux
# Nodename: eu-a6-012-05
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 08:22:13 2022
# Execution time and date (local): Mon Jan 10 09:22:13 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          8192          8192   gramschmidt           MPI          0 1864547302.853516            3 
            3          8192          8192   gramschmidt           MPI          1 1776309280.847656           15 
            3          8192          8192   gramschmidt           MPI          2 1492191071.281250            9 
            3          8192          8192   gramschmidt           MPI          3 1674167972.968750            3 
            3          8192          8192   gramschmidt           MPI          4 1843862411.529297           11 
            3          8192          8192   gramschmidt           MPI          5 1787114658.802734            3 
            3          8192          8192   gramschmidt           MPI          6 1805124219.228516            3 
            3          8192          8192   gramschmidt           MPI          7 1789256542.925781            3 
            3          8192          8192   gramschmidt           MPI          8 1814819311.486328           11 
            3          8192          8192   gramschmidt           MPI          9 1789684903.441406            3 
# Runtime: 23308.930297 s (overhead: 0.000000 %) 10 records
