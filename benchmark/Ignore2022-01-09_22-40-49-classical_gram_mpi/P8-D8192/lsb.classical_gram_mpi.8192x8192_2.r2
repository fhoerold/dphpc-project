# Sysname : Linux
# Nodename: eu-a6-012-05
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 08:22:13 2022
# Execution time and date (local): Mon Jan 10 09:22:13 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          8192          8192   gramschmidt           MPI          0 1864545236.771484            2 
            2          8192          8192   gramschmidt           MPI          1 1776309080.833984           10 
            2          8192          8192   gramschmidt           MPI          2 1492188063.939453            6 
            2          8192          8192   gramschmidt           MPI          3 1674163938.867188            2 
            2          8192          8192   gramschmidt           MPI          4 1843860246.232422           11 
            2          8192          8192   gramschmidt           MPI          5 1787112188.507812            3 
            2          8192          8192   gramschmidt           MPI          6 1805122991.937500            3 
            2          8192          8192   gramschmidt           MPI          7 1789254121.437500            3 
            2          8192          8192   gramschmidt           MPI          8 1814816369.941406            9 
            2          8192          8192   gramschmidt           MPI          9 1789681931.271484            2 
# Runtime: 23309.157314 s (overhead: 0.000000 %) 10 records
