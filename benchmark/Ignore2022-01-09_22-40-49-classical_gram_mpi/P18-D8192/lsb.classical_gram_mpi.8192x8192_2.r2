# Sysname : Linux
# Nodename: eu-a6-007-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 09:16:34 2022
# Execution time and date (local): Tue Jan 11 10:16:34 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          8192          8192   gramschmidt           MPI          0 659214961.123047            2 
            2          8192          8192   gramschmidt           MPI          1 656100360.037109            6 
            2          8192          8192   gramschmidt           MPI          2 658305563.158203            9 
            2          8192          8192   gramschmidt           MPI          3 661243882.613281            3 
            2          8192          8192   gramschmidt           MPI          4 642705283.158203            7 
            2          8192          8192   gramschmidt           MPI          5 641564612.279297            5 
            2          8192          8192   gramschmidt           MPI          6 641455188.535156            4 
            2          8192          8192   gramschmidt           MPI          7 642456955.218750            4 
            2          8192          8192   gramschmidt           MPI          8 640919277.273438           10 
            2          8192          8192   gramschmidt           MPI          9 645248021.929688            3 
# Runtime: 8476.905774 s (overhead: 0.000001 %) 10 records
