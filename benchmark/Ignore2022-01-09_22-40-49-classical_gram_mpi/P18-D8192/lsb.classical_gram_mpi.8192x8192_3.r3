# Sysname : Linux
# Nodename: eu-a6-007-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 09:16:34 2022
# Execution time and date (local): Tue Jan 11 10:16:34 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          8192          8192   gramschmidt           MPI          0 658899173.414062            2 
            3          8192          8192   gramschmidt           MPI          1 655786071.505859            6 
            3          8192          8192   gramschmidt           MPI          2 657990599.527344            4 
            3          8192          8192   gramschmidt           MPI          3 660927408.970703            0 
            3          8192          8192   gramschmidt           MPI          4 642397553.511719            6 
            3          8192          8192   gramschmidt           MPI          5 641257302.890625            2 
            3          8192          8192   gramschmidt           MPI          6 641148251.421875            2 
            3          8192          8192   gramschmidt           MPI          7 642149237.070312            2 
            3          8192          8192   gramschmidt           MPI          8 640612711.519531           10 
            3          8192          8192   gramschmidt           MPI          9 644938600.640625            3 
# Runtime: 8470.738305 s (overhead: 0.000000 %) 10 records
