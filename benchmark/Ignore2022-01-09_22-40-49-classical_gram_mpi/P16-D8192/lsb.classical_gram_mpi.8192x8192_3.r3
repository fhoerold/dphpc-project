# Sysname : Linux
# Nodename: eu-a6-012-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan 11 07:02:40 2022
# Execution time and date (local): Tue Jan 11 08:02:40 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          8192          8192   gramschmidt           MPI          0 619190997.826172            1 
            3          8192          8192   gramschmidt           MPI          1 617122223.189453            7 
            3          8192          8192   gramschmidt           MPI          2 613689617.226562            7 
            3          8192          8192   gramschmidt           MPI          3 614103997.078125            2 
            3          8192          8192   gramschmidt           MPI          4 603492368.937500            9 
            3          8192          8192   gramschmidt           MPI          5 615543015.224609            2 
            3          8192          8192   gramschmidt           MPI          6 604260252.640625            2 
            3          8192          8192   gramschmidt           MPI          7 620678840.566406            3 
            3          8192          8192   gramschmidt           MPI          8 606104555.275391            9 
            3          8192          8192   gramschmidt           MPI          9 612871588.214844            3 
# Runtime: 8003.169770 s (overhead: 0.000001 %) 10 records
