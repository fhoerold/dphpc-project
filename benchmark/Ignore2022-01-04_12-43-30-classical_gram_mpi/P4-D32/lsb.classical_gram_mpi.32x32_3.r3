# Sysname : Linux
# Nodename: eu-a6-006-08
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:12 2022
# Execution time and date (local): Tue Jan  4 12:44:12 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 133.890625            0 
            3            32            32   gramschmidt           MPI          1 115.001953            2 
            3            32            32   gramschmidt           MPI          2 151.445312            0 
            3            32            32   gramschmidt           MPI          3 125.560547            0 
            3            32            32   gramschmidt           MPI          4 108.763672            0 
            3            32            32   gramschmidt           MPI          5 105.869141            0 
            3            32            32   gramschmidt           MPI          6 92.435547            0 
            3            32            32   gramschmidt           MPI          7 96.500000            0 
            3            32            32   gramschmidt           MPI          8 136.333984            0 
            3            32            32   gramschmidt           MPI          9 93.316406            0 
# Runtime: 0.990719 s (overhead: 0.000202 %) 10 records
