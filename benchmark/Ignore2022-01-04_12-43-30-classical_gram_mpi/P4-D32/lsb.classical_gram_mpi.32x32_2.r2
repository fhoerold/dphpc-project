# Sysname : Linux
# Nodename: eu-a6-006-08
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:12 2022
# Execution time and date (local): Tue Jan  4 12:44:12 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 139.324219            0 
            2            32            32   gramschmidt           MPI          1 120.966797            2 
            2            32            32   gramschmidt           MPI          2 152.998047            0 
            2            32            32   gramschmidt           MPI          3 128.562500            0 
            2            32            32   gramschmidt           MPI          4 110.644531            0 
            2            32            32   gramschmidt           MPI          5 109.511719            0 
            2            32            32   gramschmidt           MPI          6 95.349609            0 
            2            32            32   gramschmidt           MPI          7 98.466797            0 
            2            32            32   gramschmidt           MPI          8 136.382812            0 
            2            32            32   gramschmidt           MPI          9 96.644531            0 
# Runtime: 2.996182 s (overhead: 0.000067 %) 10 records
