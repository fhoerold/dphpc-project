# Sysname : Linux
# Nodename: eu-a6-003-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:41 2022
# Execution time and date (local): Tue Jan  4 12:44:41 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 450.228516            0 
            3            64            64   gramschmidt           MPI          1 347.648438            2 
            3            64            64   gramschmidt           MPI          2 322.230469            0 
            3            64            64   gramschmidt           MPI          3 303.439453            0 
            3            64            64   gramschmidt           MPI          4 315.652344            0 
            3            64            64   gramschmidt           MPI          5 316.519531            0 
            3            64            64   gramschmidt           MPI          6 318.308594            0 
            3            64            64   gramschmidt           MPI          7 318.287109            0 
            3            64            64   gramschmidt           MPI          8 294.103516            0 
            3            64            64   gramschmidt           MPI          9 305.833984            0 
# Runtime: 13.007624 s (overhead: 0.000015 %) 10 records
