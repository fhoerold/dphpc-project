# Sysname : Linux
# Nodename: eu-a6-003-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:41 2022
# Execution time and date (local): Tue Jan  4 12:44:41 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 458.017578            0 
            2            64            64   gramschmidt           MPI          1 349.697266            2 
            2            64            64   gramschmidt           MPI          2 324.181641            0 
            2            64            64   gramschmidt           MPI          3 306.093750            0 
            2            64            64   gramschmidt           MPI          4 321.294922            0 
            2            64            64   gramschmidt           MPI          5 317.738281            0 
            2            64            64   gramschmidt           MPI          6 323.257812            0 
            2            64            64   gramschmidt           MPI          7 323.931641            0 
            2            64            64   gramschmidt           MPI          8 297.542969            0 
            2            64            64   gramschmidt           MPI          9 307.625000            0 
# Runtime: 13.005301 s (overhead: 0.000015 %) 10 records
