# Sysname : Linux
# Nodename: eu-a6-003-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:00:07 2022
# Execution time and date (local): Tue Jan  4 13:00:07 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 10084.445312            0 
            2           256           256   gramschmidt           MPI          1 6234.781250            3 
            2           256           256   gramschmidt           MPI          2 5723.664062            0 
            2           256           256   gramschmidt           MPI          3 5710.958984            0 
            2           256           256   gramschmidt           MPI          4 5673.937500            0 
            2           256           256   gramschmidt           MPI          5 5542.976562            0 
            2           256           256   gramschmidt           MPI          6 5419.466797            0 
            2           256           256   gramschmidt           MPI          7 6544.074219            0 
            2           256           256   gramschmidt           MPI          8 5628.050781            0 
            2           256           256   gramschmidt           MPI          9 5493.394531            0 
# Runtime: 8.103909 s (overhead: 0.000037 %) 10 records
