# Sysname : Linux
# Nodename: eu-a6-003-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:00:07 2022
# Execution time and date (local): Tue Jan  4 13:00:07 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 10061.349609            0 
            3           256           256   gramschmidt           MPI          1 6230.957031            3 
            3           256           256   gramschmidt           MPI          2 5720.570312            1 
            3           256           256   gramschmidt           MPI          3 5701.855469            0 
            3           256           256   gramschmidt           MPI          4 5673.546875            0 
            3           256           256   gramschmidt           MPI          5 5537.757812            0 
            3           256           256   gramschmidt           MPI          6 5412.537109            0 
            3           256           256   gramschmidt           MPI          7 6539.623047            0 
            3           256           256   gramschmidt           MPI          8 5614.691406            1 
            3           256           256   gramschmidt           MPI          9 5464.402344            0 
# Runtime: 11.116820 s (overhead: 0.000045 %) 10 records
