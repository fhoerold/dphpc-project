# Sysname : Linux
# Nodename: eu-a6-003-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:59:41 2022
# Execution time and date (local): Tue Jan  4 12:59:41 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 1966.927734            0 
            3           128           128   gramschmidt           MPI          1 1964.623047            2 
            3           128           128   gramschmidt           MPI          2 1951.230469            0 
            3           128           128   gramschmidt           MPI          3 1794.134766            0 
            3           128           128   gramschmidt           MPI          4 1815.312500            0 
            3           128           128   gramschmidt           MPI          5 1819.558594            0 
            3           128           128   gramschmidt           MPI          6 1775.619141            0 
            3           128           128   gramschmidt           MPI          7 1770.351562            0 
            3           128           128   gramschmidt           MPI          8 1780.980469            0 
            3           128           128   gramschmidt           MPI          9 1749.316406            0 
# Runtime: 4.028804 s (overhead: 0.000050 %) 10 records
