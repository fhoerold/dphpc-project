# Sysname : Linux
# Nodename: eu-a6-003-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:59:41 2022
# Execution time and date (local): Tue Jan  4 12:59:41 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 1980.740234            0 
            2           128           128   gramschmidt           MPI          1 1962.416016            3 
            2           128           128   gramschmidt           MPI          2 1956.031250            0 
            2           128           128   gramschmidt           MPI          3 1799.867188            0 
            2           128           128   gramschmidt           MPI          4 1821.171875            0 
            2           128           128   gramschmidt           MPI          5 1819.300781            0 
            2           128           128   gramschmidt           MPI          6 1782.556641            0 
            2           128           128   gramschmidt           MPI          7 1774.996094            0 
            2           128           128   gramschmidt           MPI          8 1794.503906            0 
            2           128           128   gramschmidt           MPI          9 1755.384766            0 
# Runtime: 13.031550 s (overhead: 0.000023 %) 10 records
