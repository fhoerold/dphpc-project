# Sysname : Linux
# Nodename: eu-a6-003-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:46:07 2022
# Execution time and date (local): Tue Jan  4 12:46:07 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 4571.339844            0 
            3           256           256   gramschmidt           MPI          1 4293.267578            3 
            3           256           256   gramschmidt           MPI          2 4133.324219            0 
            3           256           256   gramschmidt           MPI          3 4038.011719            0 
            3           256           256   gramschmidt           MPI          4 3982.255859            0 
            3           256           256   gramschmidt           MPI          5 3943.873047            0 
            3           256           256   gramschmidt           MPI          6 3928.652344            0 
            3           256           256   gramschmidt           MPI          7 3857.207031            0 
            3           256           256   gramschmidt           MPI          8 3859.630859            0 
            3           256           256   gramschmidt           MPI          9 4028.619141            0 
# Runtime: 7.082931 s (overhead: 0.000042 %) 10 records
