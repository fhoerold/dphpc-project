# Sysname : Linux
# Nodename: eu-a6-011-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:07:27 2022
# Execution time and date (local): Tue Jan  4 13:07:27 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 323951.134766            0 
            3          1024          1024   gramschmidt           MPI          1 316958.078125            5 
            3          1024          1024   gramschmidt           MPI          2 331049.726562            2 
            3          1024          1024   gramschmidt           MPI          3 321751.187500            0 
            3          1024          1024   gramschmidt           MPI          4 308791.572266            1 
            3          1024          1024   gramschmidt           MPI          5 298303.490234            2 
            3          1024          1024   gramschmidt           MPI          6 282884.687500            0 
            3          1024          1024   gramschmidt           MPI          7 282305.240234            0 
            3          1024          1024   gramschmidt           MPI          8 283011.658203            5 
            3          1024          1024   gramschmidt           MPI          9 284628.955078            0 
# Runtime: 6.111476 s (overhead: 0.000245 %) 10 records
