# Sysname : Linux
# Nodename: eu-a6-011-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:07:27 2022
# Execution time and date (local): Tue Jan  4 13:07:27 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 323469.156250            0 
            2          1024          1024   gramschmidt           MPI          1 316863.458984            4 
            2          1024          1024   gramschmidt           MPI          2 330687.289062            3 
            2          1024          1024   gramschmidt           MPI          3 321497.369141            0 
            2          1024          1024   gramschmidt           MPI          4 306589.357422            2 
            2          1024          1024   gramschmidt           MPI          5 296158.291016            0 
            2          1024          1024   gramschmidt           MPI          6 280686.103516            0 
            2          1024          1024   gramschmidt           MPI          7 280185.917969            0 
            2          1024          1024   gramschmidt           MPI          8 280865.414062            2 
            2          1024          1024   gramschmidt           MPI          9 282411.011719            0 
# Runtime: 6.127427 s (overhead: 0.000180 %) 10 records
