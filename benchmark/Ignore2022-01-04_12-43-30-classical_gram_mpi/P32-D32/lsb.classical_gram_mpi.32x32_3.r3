# Sysname : Linux
# Nodename: eu-a6-011-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:06:12 2022
# Execution time and date (local): Tue Jan  4 13:06:12 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 617.230469            2 
            3            32            32   gramschmidt           MPI          1 549.083984            3 
            3            32            32   gramschmidt           MPI          2 536.550781            0 
            3            32            32   gramschmidt           MPI          3 506.240234            0 
            3            32            32   gramschmidt           MPI          4 492.761719            0 
            3            32            32   gramschmidt           MPI          5 475.833984            0 
            3            32            32   gramschmidt           MPI          6 455.630859            0 
            3            32            32   gramschmidt           MPI          7 460.488281            0 
            3            32            32   gramschmidt           MPI          8 446.328125            0 
            3            32            32   gramschmidt           MPI          9 444.347656            0 
# Runtime: 4.015915 s (overhead: 0.000125 %) 10 records
