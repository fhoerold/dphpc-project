# Sysname : Linux
# Nodename: eu-a6-011-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:06:12 2022
# Execution time and date (local): Tue Jan  4 13:06:12 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 618.789062            0 
            2            32            32   gramschmidt           MPI          1 557.181641            0 
            2            32            32   gramschmidt           MPI          2 543.076172            0 
            2            32            32   gramschmidt           MPI          3 510.089844            0 
            2            32            32   gramschmidt           MPI          4 501.501953            0 
            2            32            32   gramschmidt           MPI          5 481.554688            0 
            2            32            32   gramschmidt           MPI          6 466.904297            0 
            2            32            32   gramschmidt           MPI          7 465.441406            0 
            2            32            32   gramschmidt           MPI          8 452.291016            0 
            2            32            32   gramschmidt           MPI          9 454.046875            0 
# Runtime: 0.024324 s (overhead: 0.000000 %) 10 records
