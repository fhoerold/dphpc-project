# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:16:12 2022
# Execution time and date (local): Tue Jan  4 13:16:12 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 540.710938            0 
            3            32            32   gramschmidt           MPI          1 482.144531            2 
            3            32            32   gramschmidt           MPI          2 472.773438            0 
            3            32            32   gramschmidt           MPI          3 455.152344            0 
            3            32            32   gramschmidt           MPI          4 449.751953            0 
            3            32            32   gramschmidt           MPI          5 424.164062            0 
            3            32            32   gramschmidt           MPI          6 407.814453            0 
            3            32            32   gramschmidt           MPI          7 393.996094            0 
            3            32            32   gramschmidt           MPI          8 392.500000            0 
            3            32            32   gramschmidt           MPI          9 395.599609            0 
# Runtime: 5.026120 s (overhead: 0.000040 %) 10 records
