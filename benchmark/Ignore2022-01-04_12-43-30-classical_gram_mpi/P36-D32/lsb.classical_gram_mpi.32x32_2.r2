# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:16:12 2022
# Execution time and date (local): Tue Jan  4 13:16:12 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 530.105469            0 
            2            32            32   gramschmidt           MPI          1 494.720703            2 
            2            32            32   gramschmidt           MPI          2 477.937500            0 
            2            32            32   gramschmidt           MPI          3 466.966797            0 
            2            32            32   gramschmidt           MPI          4 464.623047            0 
            2            32            32   gramschmidt           MPI          5 435.753906            0 
            2            32            32   gramschmidt           MPI          6 409.210938            0 
            2            32            32   gramschmidt           MPI          7 400.394531            0 
            2            32            32   gramschmidt           MPI          8 400.916016            0 
            2            32            32   gramschmidt           MPI          9 398.937500            0 
# Runtime: 9.016667 s (overhead: 0.000022 %) 10 records
