# Sysname : Linux
# Nodename: eu-a6-003-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:45:05 2022
# Execution time and date (local): Tue Jan  4 12:45:05 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 414144.818359            0 
            2          1024          1024   gramschmidt           MPI          1 412764.904297            5 
            2          1024          1024   gramschmidt           MPI          2 402520.634766            1 
            2          1024          1024   gramschmidt           MPI          3 402477.603516            0 
            2          1024          1024   gramschmidt           MPI          4 387562.949219            4 
            2          1024          1024   gramschmidt           MPI          5 406833.146484            0 
            2          1024          1024   gramschmidt           MPI          6 423849.492188            0 
            2          1024          1024   gramschmidt           MPI          7 414128.199219            0 
            2          1024          1024   gramschmidt           MPI          8 408192.945312            2 
            2          1024          1024   gramschmidt           MPI          9 411504.828125            0 
# Runtime: 5.366064 s (overhead: 0.000224 %) 10 records
