# Sysname : Linux
# Nodename: eu-a6-003-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:45:05 2022
# Execution time and date (local): Tue Jan  4 12:45:05 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 414332.906250            0 
            3          1024          1024   gramschmidt           MPI          1 412938.244141            4 
            3          1024          1024   gramschmidt           MPI          2 402694.404297            1 
            3          1024          1024   gramschmidt           MPI          3 402644.291016            0 
            3          1024          1024   gramschmidt           MPI          4 387843.759766            4 
            3          1024          1024   gramschmidt           MPI          5 406999.025391            0 
            3          1024          1024   gramschmidt           MPI          6 424007.355469            0 
            3          1024          1024   gramschmidt           MPI          7 414307.552734            0 
            3          1024          1024   gramschmidt           MPI          8 408357.531250            1 
            3          1024          1024   gramschmidt           MPI          9 411672.878906            0 
# Runtime: 5.363058 s (overhead: 0.000186 %) 10 records
