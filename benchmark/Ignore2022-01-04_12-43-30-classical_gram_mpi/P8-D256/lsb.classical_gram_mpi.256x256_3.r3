# Sysname : Linux
# Nodename: eu-a6-003-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:41 2022
# Execution time and date (local): Tue Jan  4 12:44:41 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 5353.841797            0 
            3           256           256   gramschmidt           MPI          1 5241.011719            3 
            3           256           256   gramschmidt           MPI          2 5253.072266            0 
            3           256           256   gramschmidt           MPI          3 5181.548828            0 
            3           256           256   gramschmidt           MPI          4 5260.984375            0 
            3           256           256   gramschmidt           MPI          5 5225.853516            0 
            3           256           256   gramschmidt           MPI          6 5205.738281            0 
            3           256           256   gramschmidt           MPI          7 5126.789062            0 
            3           256           256   gramschmidt           MPI          8 5203.072266            0 
            3           256           256   gramschmidt           MPI          9 5089.548828            0 
# Runtime: 1.072467 s (overhead: 0.000280 %) 10 records
