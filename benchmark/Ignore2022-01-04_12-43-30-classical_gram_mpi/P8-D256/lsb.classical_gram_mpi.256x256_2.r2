# Sysname : Linux
# Nodename: eu-a6-003-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:41 2022
# Execution time and date (local): Tue Jan  4 12:44:41 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 5360.710938            0 
            2           256           256   gramschmidt           MPI          1 5248.167969            3 
            2           256           256   gramschmidt           MPI          2 5256.302734            0 
            2           256           256   gramschmidt           MPI          3 5183.025391            0 
            2           256           256   gramschmidt           MPI          4 5269.519531            0 
            2           256           256   gramschmidt           MPI          5 5230.263672            0 
            2           256           256   gramschmidt           MPI          6 5215.271484            0 
            2           256           256   gramschmidt           MPI          7 5139.417969            0 
            2           256           256   gramschmidt           MPI          8 5207.871094            0 
            2           256           256   gramschmidt           MPI          9 5095.738281            0 
# Runtime: 7.064944 s (overhead: 0.000042 %) 10 records
