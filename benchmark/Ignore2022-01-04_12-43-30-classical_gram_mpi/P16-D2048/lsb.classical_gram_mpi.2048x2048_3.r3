# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:47:28 2022
# Execution time and date (local): Tue Jan  4 12:47:28 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 2098838.386719            0 
            3          2048          2048   gramschmidt           MPI          1 2145466.664062            4 
            3          2048          2048   gramschmidt           MPI          2 2138347.695312            2 
            3          2048          2048   gramschmidt           MPI          3 2114695.376953            0 
            3          2048          2048   gramschmidt           MPI          4 2122698.154297            4 
            3          2048          2048   gramschmidt           MPI          5 2099069.937500            0 
            3          2048          2048   gramschmidt           MPI          6 2075333.849609            0 
            3          2048          2048   gramschmidt           MPI          7 2100798.187500            0 
            3          2048          2048   gramschmidt           MPI          8 2101332.509766            2 
            3          2048          2048   gramschmidt           MPI          9 2125441.794922            0 
# Runtime: 31.637168 s (overhead: 0.000038 %) 10 records
