# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:47:28 2022
# Execution time and date (local): Tue Jan  4 12:47:28 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 2098362.300781            0 
            2          2048          2048   gramschmidt           MPI          1 2144979.458984            5 
            2          2048          2048   gramschmidt           MPI          2 2137801.978516            3 
            2          2048          2048   gramschmidt           MPI          3 2113946.037109            0 
            2          2048          2048   gramschmidt           MPI          4 2122219.802734            1 
            2          2048          2048   gramschmidt           MPI          5 2098526.693359            0 
            2          2048          2048   gramschmidt           MPI          6 2074853.156250            0 
            2          2048          2048   gramschmidt           MPI          7 2100273.900391            0 
            2          2048          2048   gramschmidt           MPI          8 2100897.066406            4 
            2          2048          2048   gramschmidt           MPI          9 2125029.142578            0 
# Runtime: 33.633056 s (overhead: 0.000039 %) 10 records
