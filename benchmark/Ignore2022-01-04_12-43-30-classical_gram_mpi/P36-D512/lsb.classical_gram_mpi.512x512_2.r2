# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:17:05 2022
# Execution time and date (local): Tue Jan  4 13:17:05 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 72279.080078            0 
            2           512           512   gramschmidt           MPI          1 35021.572266            4 
            2           512           512   gramschmidt           MPI          2 34197.228516            1 
            2           512           512   gramschmidt           MPI          3 34093.728516            0 
            2           512           512   gramschmidt           MPI          4 33916.240234            1 
            2           512           512   gramschmidt           MPI          5 33704.320312            0 
            2           512           512   gramschmidt           MPI          6 33831.617188            0 
            2           512           512   gramschmidt           MPI          7 37977.628906            0 
            2           512           512   gramschmidt           MPI          8 35491.533203            1 
            2           512           512   gramschmidt           MPI          9 34003.369141            0 
# Runtime: 16.809921 s (overhead: 0.000042 %) 10 records
