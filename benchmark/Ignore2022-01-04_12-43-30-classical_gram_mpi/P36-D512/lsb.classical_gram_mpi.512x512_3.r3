# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:17:05 2022
# Execution time and date (local): Tue Jan  4 13:17:05 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 71988.400391            0 
            3           512           512   gramschmidt           MPI          1 35017.232422            4 
            3           512           512   gramschmidt           MPI          2 34170.007812            1 
            3           512           512   gramschmidt           MPI          3 34092.419922            0 
            3           512           512   gramschmidt           MPI          4 33902.634766            1 
            3           512           512   gramschmidt           MPI          5 33700.259766            0 
            3           512           512   gramschmidt           MPI          6 33826.111328            0 
            3           512           512   gramschmidt           MPI          7 37913.976562            0 
            3           512           512   gramschmidt           MPI          8 35491.126953            1 
            3           512           512   gramschmidt           MPI          9 33995.728516            0 
# Runtime: 8.814535 s (overhead: 0.000079 %) 10 records
