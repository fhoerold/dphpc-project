# Sysname : Linux
# Nodename: eu-a6-010-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:11 2022
# Execution time and date (local): Tue Jan  4 12:44:11 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 1177.626953            0 
            3           128           128   gramschmidt           MPI          1 1159.951172            2 
            3           128           128   gramschmidt           MPI          2 1151.023438            0 
            3           128           128   gramschmidt           MPI          3 1146.265625            0 
            3           128           128   gramschmidt           MPI          4 1111.994141            0 
            3           128           128   gramschmidt           MPI          5 1101.248047            0 
            3           128           128   gramschmidt           MPI          6 1105.931641            0 
            3           128           128   gramschmidt           MPI          7 1128.054688            0 
            3           128           128   gramschmidt           MPI          8 1164.558594            0 
            3           128           128   gramschmidt           MPI          9 1191.218750            0 
# Runtime: 6.017329 s (overhead: 0.000033 %) 10 records
