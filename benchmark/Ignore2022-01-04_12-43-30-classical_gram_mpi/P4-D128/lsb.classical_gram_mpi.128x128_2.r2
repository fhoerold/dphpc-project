# Sysname : Linux
# Nodename: eu-a6-010-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:11 2022
# Execution time and date (local): Tue Jan  4 12:44:11 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 1189.742188            0 
            2           128           128   gramschmidt           MPI          1 1192.185547            8 
            2           128           128   gramschmidt           MPI          2 1180.861328            0 
            2           128           128   gramschmidt           MPI          3 1168.240234            0 
            2           128           128   gramschmidt           MPI          4 1130.248047            0 
            2           128           128   gramschmidt           MPI          5 1129.101562            0 
            2           128           128   gramschmidt           MPI          6 1123.273438            0 
            2           128           128   gramschmidt           MPI          7 1145.634766            0 
            2           128           128   gramschmidt           MPI          8 1186.962891            0 
            2           128           128   gramschmidt           MPI          9 1212.933594            0 
# Runtime: 6.013389 s (overhead: 0.000133 %) 10 records
