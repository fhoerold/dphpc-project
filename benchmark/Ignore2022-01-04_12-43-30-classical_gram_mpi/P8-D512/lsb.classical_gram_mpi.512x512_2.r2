# Sysname : Linux
# Nodename: eu-a6-010-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:54 2022
# Execution time and date (local): Tue Jan  4 12:44:54 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 45976.394531            0 
            2           512           512   gramschmidt           MPI          1 45392.332031            4 
            2           512           512   gramschmidt           MPI          2 45841.496094            1 
            2           512           512   gramschmidt           MPI          3 45522.365234            0 
            2           512           512   gramschmidt           MPI          4 45488.320312            1 
            2           512           512   gramschmidt           MPI          5 45973.728516            0 
            2           512           512   gramschmidt           MPI          6 45713.927734            0 
            2           512           512   gramschmidt           MPI          7 46166.841797            0 
            2           512           512   gramschmidt           MPI          8 45879.707031            2 
            2           512           512   gramschmidt           MPI          9 46110.490234            0 
# Runtime: 19.610595 s (overhead: 0.000041 %) 10 records
