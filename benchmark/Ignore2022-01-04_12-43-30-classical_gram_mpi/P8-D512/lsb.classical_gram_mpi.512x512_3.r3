# Sysname : Linux
# Nodename: eu-a6-010-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:54 2022
# Execution time and date (local): Tue Jan  4 12:44:54 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 46049.972656            0 
            3           512           512   gramschmidt           MPI          1 45449.320312            1 
            3           512           512   gramschmidt           MPI          2 45905.716797            2 
            3           512           512   gramschmidt           MPI          3 45588.869141            0 
            3           512           512   gramschmidt           MPI          4 45570.208984            1 
            3           512           512   gramschmidt           MPI          5 46070.800781            0 
            3           512           512   gramschmidt           MPI          6 45797.070312            0 
            3           512           512   gramschmidt           MPI          7 46236.511719            0 
            3           512           512   gramschmidt           MPI          8 45951.748047            1 
            3           512           512   gramschmidt           MPI          9 46187.242188            0 
# Runtime: 0.604896 s (overhead: 0.000827 %) 10 records
