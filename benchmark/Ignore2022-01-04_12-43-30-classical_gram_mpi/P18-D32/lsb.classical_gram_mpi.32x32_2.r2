# Sysname : Linux
# Nodename: eu-a6-012-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:57:41 2022
# Execution time and date (local): Tue Jan  4 12:57:41 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 402.228516            1 
            2            32            32   gramschmidt           MPI          1 374.181641            2 
            2            32            32   gramschmidt           MPI          2 343.982422            0 
            2            32            32   gramschmidt           MPI          3 315.529297            0 
            2            32            32   gramschmidt           MPI          4 288.533203            0 
            2            32            32   gramschmidt           MPI          5 278.632812            0 
            2            32            32   gramschmidt           MPI          6 338.808594            0 
            2            32            32   gramschmidt           MPI          7 271.164062            0 
            2            32            32   gramschmidt           MPI          8 262.740234            0 
            2            32            32   gramschmidt           MPI          9 263.826172            0 
# Runtime: 9.001442 s (overhead: 0.000033 %) 10 records
