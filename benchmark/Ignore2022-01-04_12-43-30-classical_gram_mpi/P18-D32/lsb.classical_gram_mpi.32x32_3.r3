# Sysname : Linux
# Nodename: eu-a6-012-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:57:41 2022
# Execution time and date (local): Tue Jan  4 12:57:41 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 393.355469            0 
            3            32            32   gramschmidt           MPI          1 357.699219            2 
            3            32            32   gramschmidt           MPI          2 328.517578            0 
            3            32            32   gramschmidt           MPI          3 305.486328            0 
            3            32            32   gramschmidt           MPI          4 279.421875            0 
            3            32            32   gramschmidt           MPI          5 267.767578            0 
            3            32            32   gramschmidt           MPI          6 330.214844            0 
            3            32            32   gramschmidt           MPI          7 263.486328            0 
            3            32            32   gramschmidt           MPI          8 255.287109            0 
            3            32            32   gramschmidt           MPI          9 259.886719            0 
# Runtime: 8.000345 s (overhead: 0.000025 %) 10 records
