# Sysname : Linux
# Nodename: eu-a6-010-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:48:41 2022
# Execution time and date (local): Tue Jan  4 12:48:41 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 37186116.828125            0 
            2          4096          4096   gramschmidt           MPI          1 40092202.046875            4 
            2          4096          4096   gramschmidt           MPI          2 38038033.054688            8 
            2          4096          4096   gramschmidt           MPI          3 38637486.378906            0 
            2          4096          4096   gramschmidt           MPI          4 38350244.720703            9 
            2          4096          4096   gramschmidt           MPI          5 38629272.421875            4 
            2          4096          4096   gramschmidt           MPI          6 37808474.521484            0 
            2          4096          4096   gramschmidt           MPI          7 38667538.154297            3 
            2          4096          4096   gramschmidt           MPI          8 37586730.882812            6 
            2          4096          4096   gramschmidt           MPI          9 37761847.080078            2 
# Runtime: 509.036637 s (overhead: 0.000007 %) 10 records
