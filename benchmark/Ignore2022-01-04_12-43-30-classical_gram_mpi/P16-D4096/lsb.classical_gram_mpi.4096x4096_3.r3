# Sysname : Linux
# Nodename: eu-a6-010-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:48:41 2022
# Execution time and date (local): Tue Jan  4 12:48:41 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 37215465.781250            0 
            3          4096          4096   gramschmidt           MPI          1 40122886.818359            4 
            3          4096          4096   gramschmidt           MPI          2 38067896.142578            1 
            3          4096          4096   gramschmidt           MPI          3 38667875.210938            5 
            3          4096          4096   gramschmidt           MPI          4 38379663.398438            4 
            3          4096          4096   gramschmidt           MPI          5 38659596.439453            0 
            3          4096          4096   gramschmidt           MPI          6 37838321.781250            0 
            3          4096          4096   gramschmidt           MPI          7 38701320.259766            0 
            3          4096          4096   gramschmidt           MPI          8 37616428.542969            6 
            3          4096          4096   gramschmidt           MPI          9 37791539.607422            0 
# Runtime: 498.412691 s (overhead: 0.000004 %) 10 records
