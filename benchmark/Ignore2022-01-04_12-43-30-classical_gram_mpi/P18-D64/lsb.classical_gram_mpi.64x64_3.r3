# Sysname : Linux
# Nodename: eu-a6-012-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:58:00 2022
# Execution time and date (local): Tue Jan  4 12:58:00 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 701.232422            0 
            3            64            64   gramschmidt           MPI          1 629.552734            2 
            3            64            64   gramschmidt           MPI          2 616.662109            0 
            3            64            64   gramschmidt           MPI          3 612.966797            0 
            3            64            64   gramschmidt           MPI          4 590.419922            0 
            3            64            64   gramschmidt           MPI          5 520.115234            0 
            3            64            64   gramschmidt           MPI          6 515.324219            0 
            3            64            64   gramschmidt           MPI          7 490.828125            0 
            3            64            64   gramschmidt           MPI          8 506.023438            0 
            3            64            64   gramschmidt           MPI          9 491.148438            0 
# Runtime: 11.984904 s (overhead: 0.000017 %) 10 records
