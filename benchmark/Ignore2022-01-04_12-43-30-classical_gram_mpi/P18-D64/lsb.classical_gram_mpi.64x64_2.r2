# Sysname : Linux
# Nodename: eu-a6-012-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:58:00 2022
# Execution time and date (local): Tue Jan  4 12:58:00 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 701.744141            0 
            2            64            64   gramschmidt           MPI          1 630.849609            2 
            2            64            64   gramschmidt           MPI          2 618.191406            0 
            2            64            64   gramschmidt           MPI          3 614.632812            0 
            2            64            64   gramschmidt           MPI          4 591.556641            0 
            2            64            64   gramschmidt           MPI          5 521.330078            0 
            2            64            64   gramschmidt           MPI          6 517.896484            0 
            2            64            64   gramschmidt           MPI          7 492.021484            0 
            2            64            64   gramschmidt           MPI          8 505.921875            0 
            2            64            64   gramschmidt           MPI          9 491.082031            0 
# Runtime: 11.003186 s (overhead: 0.000018 %) 10 records
