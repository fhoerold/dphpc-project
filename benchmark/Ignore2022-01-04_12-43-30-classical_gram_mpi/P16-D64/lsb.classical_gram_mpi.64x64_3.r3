# Sysname : Linux
# Nodename: eu-a6-003-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:45:53 2022
# Execution time and date (local): Tue Jan  4 12:45:53 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 571.562500            0 
            3            64            64   gramschmidt           MPI          1 508.988281            2 
            3            64            64   gramschmidt           MPI          2 455.525391            0 
            3            64            64   gramschmidt           MPI          3 514.884766            0 
            3            64            64   gramschmidt           MPI          4 474.906250            0 
            3            64            64   gramschmidt           MPI          5 483.185547            0 
            3            64            64   gramschmidt           MPI          6 454.041016            0 
            3            64            64   gramschmidt           MPI          7 526.480469            0 
            3            64            64   gramschmidt           MPI          8 415.117188            0 
            3            64            64   gramschmidt           MPI          9 451.197266            0 
# Runtime: 6.003092 s (overhead: 0.000033 %) 10 records
