# Sysname : Linux
# Nodename: eu-a6-003-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:45:53 2022
# Execution time and date (local): Tue Jan  4 12:45:53 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 574.478516            0 
            2            64            64   gramschmidt           MPI          1 508.660156            0 
            2            64            64   gramschmidt           MPI          2 453.417969            0 
            2            64            64   gramschmidt           MPI          3 517.005859            0 
            2            64            64   gramschmidt           MPI          4 473.875000            0 
            2            64            64   gramschmidt           MPI          5 484.126953            0 
            2            64            64   gramschmidt           MPI          6 453.369141            0 
            2            64            64   gramschmidt           MPI          7 525.957031            0 
            2            64            64   gramschmidt           MPI          8 414.660156            0 
            2            64            64   gramschmidt           MPI          9 452.070312            0 
# Runtime: 0.009302 s (overhead: 0.000000 %) 10 records
