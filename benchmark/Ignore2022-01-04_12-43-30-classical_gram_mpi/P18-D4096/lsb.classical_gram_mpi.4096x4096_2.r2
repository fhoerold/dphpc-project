# Sysname : Linux
# Nodename: eu-a6-003-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:00:44 2022
# Execution time and date (local): Tue Jan  4 13:00:44 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 19992787.703125            4 
            2          4096          4096   gramschmidt           MPI          1 19131779.955078            6 
            2          4096          4096   gramschmidt           MPI          2 21802361.107422            3 
            2          4096          4096   gramschmidt           MPI          3 20819080.878906            0 
            2          4096          4096   gramschmidt           MPI          4 20672898.794922            3 
            2          4096          4096   gramschmidt           MPI          5 21230475.312500            0 
            2          4096          4096   gramschmidt           MPI          6 28296433.130859            0 
            2          4096          4096   gramschmidt           MPI          7 21689033.695312            4 
            2          4096          4096   gramschmidt           MPI          8 20609324.408203            8 
            2          4096          4096   gramschmidt           MPI          9 23583188.947266            0 
# Runtime: 288.577785 s (overhead: 0.000010 %) 10 records
