# Sysname : Linux
# Nodename: eu-a6-003-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:00:44 2022
# Execution time and date (local): Tue Jan  4 13:00:44 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 19963376.093750            2 
            3          4096          4096   gramschmidt           MPI          1 19102846.853516            6 
            3          4096          4096   gramschmidt           MPI          2 21770524.099609            3 
            3          4096          4096   gramschmidt           MPI          3 20788031.580078            0 
            3          4096          4096   gramschmidt           MPI          4 20642051.365234            3 
            3          4096          4096   gramschmidt           MPI          5 21199513.605469            1 
            3          4096          4096   gramschmidt           MPI          6 28253709.958984            0 
            3          4096          4096   gramschmidt           MPI          7 21656720.615234            5 
            3          4096          4096   gramschmidt           MPI          8 20579636.757812            8 
            3          4096          4096   gramschmidt           MPI          9 23547972.048828            0 
# Runtime: 288.114374 s (overhead: 0.000010 %) 10 records
