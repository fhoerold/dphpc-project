# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:23:41 2022
# Execution time and date (local): Tue Jan  4 13:23:41 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 1291.298828            0 
            3            64            64   gramschmidt           MPI          1 1123.298828            2 
            3            64            64   gramschmidt           MPI          2 993.583984            0 
            3            64            64   gramschmidt           MPI          3 981.787109            0 
            3            64            64   gramschmidt           MPI          4 926.257812            0 
            3            64            64   gramschmidt           MPI          5 913.152344            0 
            3            64            64   gramschmidt           MPI          6 923.812500            0 
            3            64            64   gramschmidt           MPI          7 890.757812            0 
            3            64            64   gramschmidt           MPI          8 872.003906            0 
            3            64            64   gramschmidt           MPI          9 889.875000            0 
# Runtime: 11.027093 s (overhead: 0.000018 %) 10 records
