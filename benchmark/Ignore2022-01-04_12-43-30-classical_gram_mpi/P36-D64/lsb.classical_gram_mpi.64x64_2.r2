# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:23:41 2022
# Execution time and date (local): Tue Jan  4 13:23:41 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 1037.964844            0 
            2            64            64   gramschmidt           MPI          1 1165.615234            3 
            2            64            64   gramschmidt           MPI          2 1044.294922            0 
            2            64            64   gramschmidt           MPI          3 1026.658203            0 
            2            64            64   gramschmidt           MPI          4 962.769531            0 
            2            64            64   gramschmidt           MPI          5 950.371094            0 
            2            64            64   gramschmidt           MPI          6 948.929688            0 
            2            64            64   gramschmidt           MPI          7 921.361328            0 
            2            64            64   gramschmidt           MPI          8 897.771484            0 
            2            64            64   gramschmidt           MPI          9 920.119141            0 
# Runtime: 8.003686 s (overhead: 0.000037 %) 10 records
