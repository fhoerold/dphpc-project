# Sysname : Linux
# Nodename: eu-a6-010-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:11 2022
# Execution time and date (local): Tue Jan  4 12:44:11 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 7463.933594            0 
            3           256           256   gramschmidt           MPI          1 7492.416016            3 
            3           256           256   gramschmidt           MPI          2 7474.208984            0 
            3           256           256   gramschmidt           MPI          3 7680.359375            0 
            3           256           256   gramschmidt           MPI          4 7340.068359            0 
            3           256           256   gramschmidt           MPI          5 7396.730469            0 
            3           256           256   gramschmidt           MPI          6 7363.945312            0 
            3           256           256   gramschmidt           MPI          7 7357.740234            0 
            3           256           256   gramschmidt           MPI          8 7307.564453            0 
            3           256           256   gramschmidt           MPI          9 7423.476562            0 
# Runtime: 5.085720 s (overhead: 0.000059 %) 10 records
