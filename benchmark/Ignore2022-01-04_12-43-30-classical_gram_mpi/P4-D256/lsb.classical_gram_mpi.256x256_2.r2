# Sysname : Linux
# Nodename: eu-a6-010-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:11 2022
# Execution time and date (local): Tue Jan  4 12:44:11 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 7557.972656            0 
            2           256           256   gramschmidt           MPI          1 7583.750000            0 
            2           256           256   gramschmidt           MPI          2 7544.021484            0 
            2           256           256   gramschmidt           MPI          3 7798.144531            0 
            2           256           256   gramschmidt           MPI          4 7438.818359            0 
            2           256           256   gramschmidt           MPI          5 7525.906250            0 
            2           256           256   gramschmidt           MPI          6 7476.957031            0 
            2           256           256   gramschmidt           MPI          7 7450.154297            0 
            2           256           256   gramschmidt           MPI          8 7396.417969            0 
            2           256           256   gramschmidt           MPI          9 7492.894531            0 
# Runtime: 0.102504 s (overhead: 0.000000 %) 10 records
