# Sysname : Linux
# Nodename: eu-a6-011-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:08:11 2022
# Execution time and date (local): Tue Jan  4 13:08:11 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 2242.015625            0 
            3           128           128   gramschmidt           MPI          1 2227.949219            2 
            3           128           128   gramschmidt           MPI          2 2245.033203            0 
            3           128           128   gramschmidt           MPI          3 2171.658203            0 
            3           128           128   gramschmidt           MPI          4 2107.830078            0 
            3           128           128   gramschmidt           MPI          5 2174.246094            0 
            3           128           128   gramschmidt           MPI          6 2156.253906            0 
            3           128           128   gramschmidt           MPI          7 2119.322266            0 
            3           128           128   gramschmidt           MPI          8 2079.667969            0 
            3           128           128   gramschmidt           MPI          9 2073.861328            0 
# Runtime: 4.033121 s (overhead: 0.000050 %) 10 records
