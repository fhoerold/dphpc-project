# Sysname : Linux
# Nodename: eu-a6-011-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:08:11 2022
# Execution time and date (local): Tue Jan  4 13:08:11 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 2239.486328            0 
            2           128           128   gramschmidt           MPI          1 2226.603516            2 
            2           128           128   gramschmidt           MPI          2 2243.746094            0 
            2           128           128   gramschmidt           MPI          3 2170.642578            0 
            2           128           128   gramschmidt           MPI          4 2104.896484            0 
            2           128           128   gramschmidt           MPI          5 2172.640625            0 
            2           128           128   gramschmidt           MPI          6 2155.890625            0 
            2           128           128   gramschmidt           MPI          7 2117.007812            0 
            2           128           128   gramschmidt           MPI          8 2078.966797            0 
            2           128           128   gramschmidt           MPI          9 2071.865234            0 
# Runtime: 3.027374 s (overhead: 0.000066 %) 10 records
