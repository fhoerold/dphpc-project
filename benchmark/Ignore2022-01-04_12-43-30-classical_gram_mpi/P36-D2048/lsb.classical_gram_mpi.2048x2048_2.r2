# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:17:29 2022
# Execution time and date (local): Tue Jan  4 13:17:29 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 2065070.615234            0 
            2          2048          2048   gramschmidt           MPI          1 2020354.777344            7 
            2          2048          2048   gramschmidt           MPI          2 1936511.503906            2 
            2          2048          2048   gramschmidt           MPI          3 1998480.341797            0 
            2          2048          2048   gramschmidt           MPI          4 2103773.380859            3 
            2          2048          2048   gramschmidt           MPI          5 1991626.589844            0 
            2          2048          2048   gramschmidt           MPI          6 2052683.230469            0 
            2          2048          2048   gramschmidt           MPI          7 2103710.511719            0 
            2          2048          2048   gramschmidt           MPI          8 2126903.919922           10 
            2          2048          2048   gramschmidt           MPI          9 2086419.457031            0 
# Runtime: 37.985590 s (overhead: 0.000058 %) 10 records
