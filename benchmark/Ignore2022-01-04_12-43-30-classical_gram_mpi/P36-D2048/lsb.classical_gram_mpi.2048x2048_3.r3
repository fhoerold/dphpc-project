# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:17:29 2022
# Execution time and date (local): Tue Jan  4 13:17:29 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 2067874.685547            0 
            3          2048          2048   gramschmidt           MPI          1 2022952.150391           11 
            3          2048          2048   gramschmidt           MPI          2 1938940.097656            1 
            3          2048          2048   gramschmidt           MPI          3 2001082.564453            0 
            3          2048          2048   gramschmidt           MPI          4 2106312.039062            2 
            3          2048          2048   gramschmidt           MPI          5 1994045.658203            0 
            3          2048          2048   gramschmidt           MPI          6 2055241.779297            0 
            3          2048          2048   gramschmidt           MPI          7 2106444.007812            0 
            3          2048          2048   gramschmidt           MPI          8 2129532.681641            6 
            3          2048          2048   gramschmidt           MPI          9 2089065.636719            0 
# Runtime: 38.022233 s (overhead: 0.000053 %) 10 records
