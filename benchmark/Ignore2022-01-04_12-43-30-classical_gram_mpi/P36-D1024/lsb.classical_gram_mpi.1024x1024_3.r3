# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:24:17 2022
# Execution time and date (local): Tue Jan  4 13:24:17 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 585979.605469            0 
            3          1024          1024   gramschmidt           MPI          1 393557.558594            4 
            3          1024          1024   gramschmidt           MPI          2 353639.150391            1 
            3          1024          1024   gramschmidt           MPI          3 181204.662109            0 
            3          1024          1024   gramschmidt           MPI          4 361913.880859            1 
            3          1024          1024   gramschmidt           MPI          5 181958.300781            0 
            3          1024          1024   gramschmidt           MPI          6 180285.955078            0 
            3          1024          1024   gramschmidt           MPI          7 180727.880859            0 
            3          1024          1024   gramschmidt           MPI          8 179758.580078            1 
            3          1024          1024   gramschmidt           MPI          9 180261.169922            0 
# Runtime: 18.265294 s (overhead: 0.000038 %) 10 records
