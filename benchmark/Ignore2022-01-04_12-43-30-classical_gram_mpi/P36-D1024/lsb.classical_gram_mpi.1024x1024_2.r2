# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:24:17 2022
# Execution time and date (local): Tue Jan  4 13:24:17 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 585775.878906            0 
            2          1024          1024   gramschmidt           MPI          1 393421.466797            4 
            2          1024          1024   gramschmidt           MPI          2 353508.917969            1 
            2          1024          1024   gramschmidt           MPI          3 181140.695312            0 
            2          1024          1024   gramschmidt           MPI          4 361783.066406            2 
            2          1024          1024   gramschmidt           MPI          5 181887.660156            0 
            2          1024          1024   gramschmidt           MPI          6 180217.181641            0 
            2          1024          1024   gramschmidt           MPI          7 180651.933594            0 
            2          1024          1024   gramschmidt           MPI          8 179676.832031            1 
            2          1024          1024   gramschmidt           MPI          9 180213.197266            0 
# Runtime: 12.263812 s (overhead: 0.000065 %) 10 records
