# Sysname : Linux
# Nodename: eu-a6-007-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:00:12 2022
# Execution time and date (local): Tue Jan  4 13:00:12 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 84551.873047            0 
            2           512           512   gramschmidt           MPI          1 61884.818359            4 
            2           512           512   gramschmidt           MPI          2 58426.958984            1 
            2           512           512   gramschmidt           MPI          3 46034.957031            0 
            2           512           512   gramschmidt           MPI          4 45781.123047            1 
            2           512           512   gramschmidt           MPI          5 45423.195312            0 
            2           512           512   gramschmidt           MPI          6 45501.009766            0 
            2           512           512   gramschmidt           MPI          7 45554.267578            0 
            2           512           512   gramschmidt           MPI          8 45361.404297            1 
            2           512           512   gramschmidt           MPI          9 45673.806641            0 
# Runtime: 8.775057 s (overhead: 0.000080 %) 10 records
