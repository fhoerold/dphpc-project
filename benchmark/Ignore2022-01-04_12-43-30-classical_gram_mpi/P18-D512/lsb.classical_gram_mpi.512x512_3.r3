# Sysname : Linux
# Nodename: eu-a6-007-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:00:12 2022
# Execution time and date (local): Tue Jan  4 13:00:12 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 84598.916016            0 
            3           512           512   gramschmidt           MPI          1 61912.849609            4 
            3           512           512   gramschmidt           MPI          2 58454.123047            1 
            3           512           512   gramschmidt           MPI          3 46066.542969            0 
            3           512           512   gramschmidt           MPI          4 45805.507812            1 
            3           512           512   gramschmidt           MPI          5 45436.115234            0 
            3           512           512   gramschmidt           MPI          6 45506.792969            0 
            3           512           512   gramschmidt           MPI          7 45566.236328            0 
            3           512           512   gramschmidt           MPI          8 45363.787109            2 
            3           512           512   gramschmidt           MPI          9 45690.128906            0 
# Runtime: 10.787077 s (overhead: 0.000074 %) 10 records
