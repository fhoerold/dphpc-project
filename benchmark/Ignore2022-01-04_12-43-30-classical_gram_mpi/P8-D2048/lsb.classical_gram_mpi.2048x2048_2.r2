# Sysname : Linux
# Nodename: eu-a6-011-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:45:11 2022
# Execution time and date (local): Tue Jan  4 12:45:11 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 5145180.773438            2 
            2          2048          2048   gramschmidt           MPI          1 4513382.070312            5 
            2          2048          2048   gramschmidt           MPI          2 6852127.501953            2 
            2          2048          2048   gramschmidt           MPI          3 4985252.025391            0 
            2          2048          2048   gramschmidt           MPI          4 5281411.837891            4 
            2          2048          2048   gramschmidt           MPI          5 4516609.171875            0 
            2          2048          2048   gramschmidt           MPI          6 5059527.646484            0 
            2          2048          2048   gramschmidt           MPI          7 5538397.011719            0 
            2          2048          2048   gramschmidt           MPI          8 6887924.810547            1 
            2          2048          2048   gramschmidt           MPI          9 5815289.769531            0 
# Runtime: 84.676039 s (overhead: 0.000017 %) 10 records
