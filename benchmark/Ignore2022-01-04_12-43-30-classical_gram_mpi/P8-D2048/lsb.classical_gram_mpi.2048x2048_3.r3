# Sysname : Linux
# Nodename: eu-a6-011-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:45:11 2022
# Execution time and date (local): Tue Jan  4 12:45:11 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 5147822.029297            0 
            3          2048          2048   gramschmidt           MPI          1 4516164.535156            6 
            3          2048          2048   gramschmidt           MPI          2 6855282.042969            2 
            3          2048          2048   gramschmidt           MPI          3 4988120.042969            0 
            3          2048          2048   gramschmidt           MPI          4 5284344.744141            3 
            3          2048          2048   gramschmidt           MPI          5 4519646.128906            2 
            3          2048          2048   gramschmidt           MPI          6 5062245.560547            0 
            3          2048          2048   gramschmidt           MPI          7 5541155.083984            0 
            3          2048          2048   gramschmidt           MPI          8 6891045.023438            6 
            3          2048          2048   gramschmidt           MPI          9 5818402.015625            0 
# Runtime: 80.710168 s (overhead: 0.000024 %) 10 records
