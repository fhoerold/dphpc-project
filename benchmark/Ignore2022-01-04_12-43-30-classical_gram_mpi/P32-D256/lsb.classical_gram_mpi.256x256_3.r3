# Sysname : Linux
# Nodename: eu-a6-011-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:08:33 2022
# Execution time and date (local): Tue Jan  4 13:08:33 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 11914.423828            0 
            3           256           256   gramschmidt           MPI          1 9271.404297            3 
            3           256           256   gramschmidt           MPI          2 12560.878906            0 
            3           256           256   gramschmidt           MPI          3 10393.136719            0 
            3           256           256   gramschmidt           MPI          4 9017.179688            0 
            3           256           256   gramschmidt           MPI          5 8989.042969            0 
            3           256           256   gramschmidt           MPI          6 8896.599609            0 
            3           256           256   gramschmidt           MPI          7 9003.792969            0 
            3           256           256   gramschmidt           MPI          8 8918.189453            0 
            3           256           256   gramschmidt           MPI          9 8973.169922            0 
# Runtime: 11.175652 s (overhead: 0.000027 %) 10 records
