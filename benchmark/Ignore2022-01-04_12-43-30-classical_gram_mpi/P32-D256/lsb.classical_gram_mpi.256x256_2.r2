# Sysname : Linux
# Nodename: eu-a6-011-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:08:33 2022
# Execution time and date (local): Tue Jan  4 13:08:33 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 11913.216797            0 
            2           256           256   gramschmidt           MPI          1 9257.716797            3 
            2           256           256   gramschmidt           MPI          2 12538.322266            1 
            2           256           256   gramschmidt           MPI          3 10383.759766            0 
            2           256           256   gramschmidt           MPI          4 9014.949219            0 
            2           256           256   gramschmidt           MPI          5 8983.296875            0 
            2           256           256   gramschmidt           MPI          6 8896.142578            0 
            2           256           256   gramschmidt           MPI          7 8976.630859            0 
            2           256           256   gramschmidt           MPI          8 8912.541016            1 
            2           256           256   gramschmidt           MPI          9 8969.966797            0 
# Runtime: 3.184065 s (overhead: 0.000157 %) 10 records
