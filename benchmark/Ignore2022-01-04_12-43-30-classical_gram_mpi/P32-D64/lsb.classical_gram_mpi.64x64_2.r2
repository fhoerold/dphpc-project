# Sysname : Linux
# Nodename: eu-a6-011-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:06:28 2022
# Execution time and date (local): Tue Jan  4 13:06:28 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 1104.656250            0 
            2            64            64   gramschmidt           MPI          1 994.460938            2 
            2            64            64   gramschmidt           MPI          2 1028.412109            0 
            2            64            64   gramschmidt           MPI          3 948.744141            0 
            2            64            64   gramschmidt           MPI          4 933.808594            0 
            2            64            64   gramschmidt           MPI          5 944.703125            0 
            2            64            64   gramschmidt           MPI          6 919.550781            0 
            2            64            64   gramschmidt           MPI          7 903.048828            0 
            2            64            64   gramschmidt           MPI          8 886.865234            0 
            2            64            64   gramschmidt           MPI          9 886.394531            0 
# Runtime: 14.037653 s (overhead: 0.000014 %) 10 records
