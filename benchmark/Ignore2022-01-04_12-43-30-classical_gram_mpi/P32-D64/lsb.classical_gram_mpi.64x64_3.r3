# Sysname : Linux
# Nodename: eu-a6-011-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:06:28 2022
# Execution time and date (local): Tue Jan  4 13:06:28 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 1071.136719            0 
            3            64            64   gramschmidt           MPI          1 965.779297            2 
            3            64            64   gramschmidt           MPI          2 998.587891            0 
            3            64            64   gramschmidt           MPI          3 931.046875            0 
            3            64            64   gramschmidt           MPI          4 920.625000            0 
            3            64            64   gramschmidt           MPI          5 923.882812            0 
            3            64            64   gramschmidt           MPI          6 904.236328            0 
            3            64            64   gramschmidt           MPI          7 882.134766            0 
            3            64            64   gramschmidt           MPI          8 868.279297            0 
            3            64            64   gramschmidt           MPI          9 865.574219            0 
# Runtime: 14.035214 s (overhead: 0.000014 %) 10 records
