# Sysname : Linux
# Nodename: eu-a6-011-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:08:53 2022
# Execution time and date (local): Tue Jan  4 13:08:53 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 1341399.041016            0 
            2          2048          2048   gramschmidt           MPI          1 1259732.482422            4 
            2          2048          2048   gramschmidt           MPI          2 1326524.535156            2 
            2          2048          2048   gramschmidt           MPI          3 1255950.041016            0 
            2          2048          2048   gramschmidt           MPI          4 1260814.775391            2 
            2          2048          2048   gramschmidt           MPI          5 1274381.937500            2 
            2          2048          2048   gramschmidt           MPI          6 1328160.330078            0 
            2          2048          2048   gramschmidt           MPI          7 1338037.318359            0 
            2          2048          2048   gramschmidt           MPI          8 1257535.787109            6 
            2          2048          2048   gramschmidt           MPI          9 1323003.201172            0 
# Runtime: 17.762047 s (overhead: 0.000090 %) 10 records
