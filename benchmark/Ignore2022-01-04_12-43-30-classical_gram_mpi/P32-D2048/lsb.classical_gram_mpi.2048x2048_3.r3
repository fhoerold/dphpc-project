# Sysname : Linux
# Nodename: eu-a6-011-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:08:53 2022
# Execution time and date (local): Tue Jan  4 13:08:53 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 1341283.261719            0 
            3          2048          2048   gramschmidt           MPI          1 1259714.796875            6 
            3          2048          2048   gramschmidt           MPI          2 1326424.013672            2 
            3          2048          2048   gramschmidt           MPI          3 1255859.027344            0 
            3          2048          2048   gramschmidt           MPI          4 1260712.720703            2 
            3          2048          2048   gramschmidt           MPI          5 1274365.925781            4 
            3          2048          2048   gramschmidt           MPI          6 1328014.066406            0 
            3          2048          2048   gramschmidt           MPI          7 1337947.580078            0 
            3          2048          2048   gramschmidt           MPI          8 1257424.300781            6 
            3          2048          2048   gramschmidt           MPI          9 1322934.455078            0 
# Runtime: 27.750508 s (overhead: 0.000072 %) 10 records
