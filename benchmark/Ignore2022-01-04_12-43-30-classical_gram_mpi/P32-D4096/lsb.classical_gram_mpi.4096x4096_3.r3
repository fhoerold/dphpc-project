# Sysname : Linux
# Nodename: eu-a6-011-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:09:40 2022
# Execution time and date (local): Tue Jan  4 13:09:40 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 20445803.267578            0 
            3          4096          4096   gramschmidt           MPI          1 20336637.050781            4 
            3          4096          4096   gramschmidt           MPI          2 20581569.716797            3 
            3          4096          4096   gramschmidt           MPI          3 20377790.832031            0 
            3          4096          4096   gramschmidt           MPI          4 20277142.527344           14 
            3          4096          4096   gramschmidt           MPI          5 20600137.976562            0 
            3          4096          4096   gramschmidt           MPI          6 20408598.513672            3 
            3          4096          4096   gramschmidt           MPI          7 20261916.626953            0 
            3          4096          4096   gramschmidt           MPI          8 20586778.255859            7 
            3          4096          4096   gramschmidt           MPI          9 20355855.505859            4 
# Runtime: 273.722929 s (overhead: 0.000013 %) 10 records
