# Sysname : Linux
# Nodename: eu-a6-011-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:09:40 2022
# Execution time and date (local): Tue Jan  4 13:09:40 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 20442208.650391            0 
            2          4096          4096   gramschmidt           MPI          1 20333355.189453            6 
            2          4096          4096   gramschmidt           MPI          2 20578250.455078            2 
            2          4096          4096   gramschmidt           MPI          3 20374091.082031            0 
            2          4096          4096   gramschmidt           MPI          4 20273897.763672            7 
            2          4096          4096   gramschmidt           MPI          5 20596879.666016            0 
            2          4096          4096   gramschmidt           MPI          6 20405316.677734            0 
            2          4096          4096   gramschmidt           MPI          7 20258790.501953            0 
            2          4096          4096   gramschmidt           MPI          8 20583174.865234            7 
            2          4096          4096   gramschmidt           MPI          9 20352668.873047            2 
# Runtime: 279.701479 s (overhead: 0.000009 %) 10 records
