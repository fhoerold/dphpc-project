# Sysname : Linux
# Nodename: eu-a6-003-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:41 2022
# Execution time and date (local): Tue Jan  4 12:44:41 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 1323.966797            0 
            2           128           128   gramschmidt           MPI          1 1142.449219            3 
            2           128           128   gramschmidt           MPI          2 1111.171875            0 
            2           128           128   gramschmidt           MPI          3 1113.251953            0 
            2           128           128   gramschmidt           MPI          4 1061.568359            0 
            2           128           128   gramschmidt           MPI          5 1087.990234            0 
            2           128           128   gramschmidt           MPI          6 1067.787109            0 
            2           128           128   gramschmidt           MPI          7 1092.312500            0 
            2           128           128   gramschmidt           MPI          8 1050.335938            0 
            2           128           128   gramschmidt           MPI          9 1092.085938            0 
# Runtime: 10.022734 s (overhead: 0.000030 %) 10 records
