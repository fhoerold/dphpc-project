# Sysname : Linux
# Nodename: eu-a6-003-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:41 2022
# Execution time and date (local): Tue Jan  4 12:44:41 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 1311.765625            0 
            3           128           128   gramschmidt           MPI          1 1112.968750            3 
            3           128           128   gramschmidt           MPI          2 1080.687500            0 
            3           128           128   gramschmidt           MPI          3 1078.371094            0 
            3           128           128   gramschmidt           MPI          4 1037.675781            0 
            3           128           128   gramschmidt           MPI          5 1070.589844            0 
            3           128           128   gramschmidt           MPI          6 1057.861328            0 
            3           128           128   gramschmidt           MPI          7 1066.167969            0 
            3           128           128   gramschmidt           MPI          8 1054.339844            0 
            3           128           128   gramschmidt           MPI          9 1050.759766            0 
# Runtime: 15.001159 s (overhead: 0.000020 %) 10 records
