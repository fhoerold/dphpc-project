# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:47:12 2022
# Execution time and date (local): Tue Jan  4 12:47:12 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 246476.677734            0 
            2          1024          1024   gramschmidt           MPI          1 237927.945312            4 
            2          1024          1024   gramschmidt           MPI          2 225630.964844            2 
            2          1024          1024   gramschmidt           MPI          3 213256.636719            0 
            2          1024          1024   gramschmidt           MPI          4 214931.867188            2 
            2          1024          1024   gramschmidt           MPI          5 234059.689453            0 
            2          1024          1024   gramschmidt           MPI          6 227202.355469            3 
            2          1024          1024   gramschmidt           MPI          7 213160.283203            0 
            2          1024          1024   gramschmidt           MPI          8 213439.269531            4 
            2          1024          1024   gramschmidt           MPI          9 237224.859375            0 
# Runtime: 8.938046 s (overhead: 0.000168 %) 10 records
