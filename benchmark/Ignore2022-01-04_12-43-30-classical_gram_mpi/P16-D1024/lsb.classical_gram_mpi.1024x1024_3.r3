# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:47:12 2022
# Execution time and date (local): Tue Jan  4 12:47:12 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 246772.248047            0 
            3          1024          1024   gramschmidt           MPI          1 238167.648438            4 
            3          1024          1024   gramschmidt           MPI          2 225837.535156            2 
            3          1024          1024   gramschmidt           MPI          3 213497.396484            0 
            3          1024          1024   gramschmidt           MPI          4 215202.994141            2 
            3          1024          1024   gramschmidt           MPI          5 234240.175781            0 
            3          1024          1024   gramschmidt           MPI          6 227322.783203            0 
            3          1024          1024   gramschmidt           MPI          7 213432.804688            0 
            3          1024          1024   gramschmidt           MPI          8 213706.187500            5 
            3          1024          1024   gramschmidt           MPI          9 237617.259766            0 
# Runtime: 8.938275 s (overhead: 0.000145 %) 10 records
