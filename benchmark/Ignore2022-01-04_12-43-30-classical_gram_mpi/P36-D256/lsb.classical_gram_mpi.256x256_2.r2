# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:16:43 2022
# Execution time and date (local): Tue Jan  4 13:16:43 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 16642.904297            0 
            2           256           256   gramschmidt           MPI          1 16022.515625            3 
            2           256           256   gramschmidt           MPI          2 15871.832031            0 
            2           256           256   gramschmidt           MPI          3 16153.382812            0 
            2           256           256   gramschmidt           MPI          4 16291.292969            0 
            2           256           256   gramschmidt           MPI          5 16331.304688            0 
            2           256           256   gramschmidt           MPI          6 16404.914062            0 
            2           256           256   gramschmidt           MPI          7 16461.484375            0 
            2           256           256   gramschmidt           MPI          8 16299.376953            0 
            2           256           256   gramschmidt           MPI          9 16409.843750            0 
# Runtime: 12.262840 s (overhead: 0.000024 %) 10 records
