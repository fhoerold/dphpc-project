# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:16:43 2022
# Execution time and date (local): Tue Jan  4 13:16:43 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 16708.240234            0 
            3           256           256   gramschmidt           MPI          1 16107.998047            4 
            3           256           256   gramschmidt           MPI          2 15958.048828            0 
            3           256           256   gramschmidt           MPI          3 16233.580078            0 
            3           256           256   gramschmidt           MPI          4 16362.074219            1 
            3           256           256   gramschmidt           MPI          5 16421.775391            0 
            3           256           256   gramschmidt           MPI          6 16484.390625            0 
            3           256           256   gramschmidt           MPI          7 16546.082031            0 
            3           256           256   gramschmidt           MPI          8 16384.449219            1 
            3           256           256   gramschmidt           MPI          9 16500.107422            0 
# Runtime: 13.242925 s (overhead: 0.000045 %) 10 records
