# Sysname : Linux
# Nodename: eu-a6-012-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:58:19 2022
# Execution time and date (local): Tue Jan  4 12:58:19 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 241961.142578            0 
            2          1024          1024   gramschmidt           MPI          1 222666.392578            4 
            2          1024          1024   gramschmidt           MPI          2 224776.328125            2 
            2          1024          1024   gramschmidt           MPI          3 223654.640625            0 
            2          1024          1024   gramschmidt           MPI          4 222635.365234            1 
            2          1024          1024   gramschmidt           MPI          5 222834.718750            0 
            2          1024          1024   gramschmidt           MPI          6 224587.078125            0 
            2          1024          1024   gramschmidt           MPI          7 223096.726562            0 
            2          1024          1024   gramschmidt           MPI          8 223350.021484            1 
            2          1024          1024   gramschmidt           MPI          9 263205.621094            0 
# Runtime: 16.034656 s (overhead: 0.000050 %) 10 records
