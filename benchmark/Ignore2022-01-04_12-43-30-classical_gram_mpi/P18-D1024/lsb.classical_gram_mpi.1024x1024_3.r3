# Sysname : Linux
# Nodename: eu-a6-012-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:58:19 2022
# Execution time and date (local): Tue Jan  4 12:58:19 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 242036.636719            0 
            3          1024          1024   gramschmidt           MPI          1 222748.431641            4 
            3          1024          1024   gramschmidt           MPI          2 224867.140625            2 
            3          1024          1024   gramschmidt           MPI          3 223740.640625            0 
            3          1024          1024   gramschmidt           MPI          4 222720.128906            1 
            3          1024          1024   gramschmidt           MPI          5 222926.796875            0 
            3          1024          1024   gramschmidt           MPI          6 224722.080078            0 
            3          1024          1024   gramschmidt           MPI          7 223183.138672            0 
            3          1024          1024   gramschmidt           MPI          8 223436.062500            1 
            3          1024          1024   gramschmidt           MPI          9 263294.419922            0 
# Runtime: 16.030433 s (overhead: 0.000050 %) 10 records
