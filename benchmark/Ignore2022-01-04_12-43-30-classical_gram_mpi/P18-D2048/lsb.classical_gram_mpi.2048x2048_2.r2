# Sysname : Linux
# Nodename: eu-a6-012-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:58:46 2022
# Execution time and date (local): Tue Jan  4 12:58:46 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 4240976.156250            0 
            2          2048          2048   gramschmidt           MPI          1 4202821.060547            9 
            2          2048          2048   gramschmidt           MPI          2 4221499.488281            2 
            2          2048          2048   gramschmidt           MPI          3 4238784.945312            1 
            2          2048          2048   gramschmidt           MPI          4 4183457.289062            2 
            2          2048          2048   gramschmidt           MPI          5 4180597.126953            2 
            2          2048          2048   gramschmidt           MPI          6 4247386.060547            0 
            2          2048          2048   gramschmidt           MPI          7 4290339.505859            0 
            2          2048          2048   gramschmidt           MPI          8 4316775.771484            6 
            2          2048          2048   gramschmidt           MPI          9 4265647.263672            0 
# Runtime: 55.261897 s (overhead: 0.000040 %) 10 records
