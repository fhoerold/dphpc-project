# Sysname : Linux
# Nodename: eu-a6-012-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:58:46 2022
# Execution time and date (local): Tue Jan  4 12:58:46 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 4238024.410156            0 
            3          2048          2048   gramschmidt           MPI          1 4199819.064453            6 
            3          2048          2048   gramschmidt           MPI          2 4218563.626953            2 
            3          2048          2048   gramschmidt           MPI          3 4235990.722656            0 
            3          2048          2048   gramschmidt           MPI          4 4180497.369141            6 
            3          2048          2048   gramschmidt           MPI          5 4177682.101562            0 
            3          2048          2048   gramschmidt           MPI          6 4244475.886719            0 
            3          2048          2048   gramschmidt           MPI          7 4287332.699219            0 
            3          2048          2048   gramschmidt           MPI          8 4313743.554688            3 
            3          2048          2048   gramschmidt           MPI          9 4262664.179688            0 
# Runtime: 67.207076 s (overhead: 0.000025 %) 10 records
