# Sysname : Linux
# Nodename: eu-a6-010-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:11 2022
# Execution time and date (local): Tue Jan  4 12:44:11 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 78365.306641            0 
            3           512           512   gramschmidt           MPI          1 78393.640625            3 
            3           512           512   gramschmidt           MPI          2 78363.181641            1 
            3           512           512   gramschmidt           MPI          3 79478.398438            0 
            3           512           512   gramschmidt           MPI          4 78283.265625            0 
            3           512           512   gramschmidt           MPI          5 78757.962891            0 
            3           512           512   gramschmidt           MPI          6 84700.617188            0 
            3           512           512   gramschmidt           MPI          7 77958.462891            0 
            3           512           512   gramschmidt           MPI          8 79292.718750            1 
            3           512           512   gramschmidt           MPI          9 78241.558594            0 
# Runtime: 3.032637 s (overhead: 0.000165 %) 10 records
