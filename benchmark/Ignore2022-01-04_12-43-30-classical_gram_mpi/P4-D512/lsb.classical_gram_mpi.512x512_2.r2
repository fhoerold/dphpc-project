# Sysname : Linux
# Nodename: eu-a6-010-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:11 2022
# Execution time and date (local): Tue Jan  4 12:44:11 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 78624.224609            0 
            2           512           512   gramschmidt           MPI          1 78661.205078            3 
            2           512           512   gramschmidt           MPI          2 78620.873047            1 
            2           512           512   gramschmidt           MPI          3 79737.310547            0 
            2           512           512   gramschmidt           MPI          4 78547.009766            1 
            2           512           512   gramschmidt           MPI          5 79015.208984            0 
            2           512           512   gramschmidt           MPI          6 84971.232422            0 
            2           512           512   gramschmidt           MPI          7 78213.853516            0 
            2           512           512   gramschmidt           MPI          8 79502.285156            1 
            2           512           512   gramschmidt           MPI          9 78495.759766            0 
# Runtime: 4.044268 s (overhead: 0.000148 %) 10 records
