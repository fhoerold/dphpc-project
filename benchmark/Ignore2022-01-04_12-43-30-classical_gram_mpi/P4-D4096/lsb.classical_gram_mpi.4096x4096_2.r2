# Sysname : Linux
# Nodename: eu-a6-010-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:12 2022
# Execution time and date (local): Tue Jan  4 12:44:12 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 157786585.435547            0 
            2          4096          4096   gramschmidt           MPI          1 156701706.066406            7 
            2          4096          4096   gramschmidt           MPI          2 157112894.558594            8 
            2          4096          4096   gramschmidt           MPI          3 156692736.947266            0 
            2          4096          4096   gramschmidt           MPI          4 160246218.925781            4 
            2          4096          4096   gramschmidt           MPI          5 157494939.910156            0 
            2          4096          4096   gramschmidt           MPI          6 156182380.955078            0 
            2          4096          4096   gramschmidt           MPI          7 157098687.441406            0 
            2          4096          4096   gramschmidt           MPI          8 156311065.589844            4 
            2          4096          4096   gramschmidt           MPI          9 156384053.082031            0 
# Runtime: 2083.546772 s (overhead: 0.000001 %) 10 records
