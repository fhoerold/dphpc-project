# Sysname : Linux
# Nodename: eu-a6-010-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:12 2022
# Execution time and date (local): Tue Jan  4 12:44:12 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 157796853.203125            0 
            3          4096          4096   gramschmidt           MPI          1 156712215.330078            6 
            3          4096          4096   gramschmidt           MPI          2 157123362.417969            8 
            3          4096          4096   gramschmidt           MPI          3 156703151.335938            0 
            3          4096          4096   gramschmidt           MPI          4 160256485.597656            8 
            3          4096          4096   gramschmidt           MPI          5 157505471.878906            2 
            3          4096          4096   gramschmidt           MPI          6 156192866.921875            2 
            3          4096          4096   gramschmidt           MPI          7 157108946.783203            3 
            3          4096          4096   gramschmidt           MPI          8 156321381.548828            6 
            3          4096          4096   gramschmidt           MPI          9 156394311.025391            0 
# Runtime: 2083.587066 s (overhead: 0.000002 %) 10 records
