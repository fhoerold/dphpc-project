# Sysname : Linux
# Nodename: eu-a6-010-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:12 2022
# Execution time and date (local): Tue Jan  4 12:44:12 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 13428441.412109            0 
            3          2048          2048   gramschmidt           MPI          1 13275789.716797            4 
            3          2048          2048   gramschmidt           MPI          2 13192988.839844            2 
            3          2048          2048   gramschmidt           MPI          3 13231784.419922            0 
            3          2048          2048   gramschmidt           MPI          4 13132266.730469            2 
            3          2048          2048   gramschmidt           MPI          5 13294407.730469            0 
            3          2048          2048   gramschmidt           MPI          6 13166749.738281            0 
            3          2048          2048   gramschmidt           MPI          7 13089697.271484            0 
            3          2048          2048   gramschmidt           MPI          8 13265351.177734            6 
            3          2048          2048   gramschmidt           MPI          9 13123922.578125            2 
# Runtime: 181.280885 s (overhead: 0.000009 %) 10 records
