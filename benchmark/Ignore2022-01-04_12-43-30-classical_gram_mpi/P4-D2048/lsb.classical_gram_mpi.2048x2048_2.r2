# Sysname : Linux
# Nodename: eu-a6-010-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:12 2022
# Execution time and date (local): Tue Jan  4 12:44:12 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 13429763.005859            0 
            2          2048          2048   gramschmidt           MPI          1 13276436.152344            4 
            2          2048          2048   gramschmidt           MPI          2 13194352.841797            2 
            2          2048          2048   gramschmidt           MPI          3 13233182.412109            0 
            2          2048          2048   gramschmidt           MPI          4 13132913.888672            1 
            2          2048          2048   gramschmidt           MPI          5 13295817.232422            0 
            2          2048          2048   gramschmidt           MPI          6 13168134.613281            0 
            2          2048          2048   gramschmidt           MPI          7 13091147.074219            0 
            2          2048          2048   gramschmidt           MPI          8 13266855.052734            5 
            2          2048          2048   gramschmidt           MPI          9 13124610.312500            0 
# Runtime: 180.312866 s (overhead: 0.000007 %) 10 records
