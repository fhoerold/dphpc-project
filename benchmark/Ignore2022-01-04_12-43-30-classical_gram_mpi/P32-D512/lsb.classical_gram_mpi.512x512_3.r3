# Sysname : Linux
# Nodename: eu-a6-011-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:06:53 2022
# Execution time and date (local): Tue Jan  4 13:06:53 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 36497.869141            0 
            3           512           512   gramschmidt           MPI          1 34042.191406            3 
            3           512           512   gramschmidt           MPI          2 34156.326172            1 
            3           512           512   gramschmidt           MPI          3 52103.466797            0 
            3           512           512   gramschmidt           MPI          4 33830.257812            1 
            3           512           512   gramschmidt           MPI          5 33335.169922            0 
            3           512           512   gramschmidt           MPI          6 33381.193359            0 
            3           512           512   gramschmidt           MPI          7 33376.439453            0 
            3           512           512   gramschmidt           MPI          8 33340.976562            1 
            3           512           512   gramschmidt           MPI          9 34409.009766            0 
# Runtime: 7.814284 s (overhead: 0.000077 %) 10 records
