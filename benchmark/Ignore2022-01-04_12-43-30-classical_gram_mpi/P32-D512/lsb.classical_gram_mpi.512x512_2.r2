# Sysname : Linux
# Nodename: eu-a6-011-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:06:53 2022
# Execution time and date (local): Tue Jan  4 13:06:53 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 36536.091797            0 
            2           512           512   gramschmidt           MPI          1 34037.353516            5 
            2           512           512   gramschmidt           MPI          2 34167.560547            2 
            2           512           512   gramschmidt           MPI          3 52115.472656            0 
            2           512           512   gramschmidt           MPI          4 33817.849609            1 
            2           512           512   gramschmidt           MPI          5 33318.996094            0 
            2           512           512   gramschmidt           MPI          6 33367.621094            0 
            2           512           512   gramschmidt           MPI          7 33359.761719            0 
            2           512           512   gramschmidt           MPI          8 33330.992188            1 
            2           512           512   gramschmidt           MPI          9 34391.482422            0 
# Runtime: 12.810918 s (overhead: 0.000070 %) 10 records
