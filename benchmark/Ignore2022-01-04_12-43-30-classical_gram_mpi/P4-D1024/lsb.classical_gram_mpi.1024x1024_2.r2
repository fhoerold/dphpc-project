# Sysname : Linux
# Nodename: eu-a6-010-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:11 2022
# Execution time and date (local): Tue Jan  4 12:44:11 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 884177.855469            0 
            2          1024          1024   gramschmidt           MPI          1 881220.371094            5 
            2          1024          1024   gramschmidt           MPI          2 883477.099609            1 
            2          1024          1024   gramschmidt           MPI          3 880421.777344            0 
            2          1024          1024   gramschmidt           MPI          4 880700.855469            1 
            2          1024          1024   gramschmidt           MPI          5 888449.191406            0 
            2          1024          1024   gramschmidt           MPI          6 880807.296875            0 
            2          1024          1024   gramschmidt           MPI          7 879849.808594            0 
            2          1024          1024   gramschmidt           MPI          8 880420.710938            6 
            2          1024          1024   gramschmidt           MPI          9 880502.060547            0 
# Runtime: 15.495116 s (overhead: 0.000084 %) 10 records
