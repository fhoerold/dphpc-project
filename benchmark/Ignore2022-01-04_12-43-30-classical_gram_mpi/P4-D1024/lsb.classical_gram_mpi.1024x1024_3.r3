# Sysname : Linux
# Nodename: eu-a6-010-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:11 2022
# Execution time and date (local): Tue Jan  4 12:44:11 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 883087.816406            0 
            3          1024          1024   gramschmidt           MPI          1 880172.087891            4 
            3          1024          1024   gramschmidt           MPI          2 882403.294922            1 
            3          1024          1024   gramschmidt           MPI          3 879336.972656            0 
            3          1024          1024   gramschmidt           MPI          4 879622.298828            3 
            3          1024          1024   gramschmidt           MPI          5 887519.822266            0 
            3          1024          1024   gramschmidt           MPI          6 879718.080078            0 
            3          1024          1024   gramschmidt           MPI          7 878763.150391            0 
            3          1024          1024   gramschmidt           MPI          8 879325.974609            1 
            3          1024          1024   gramschmidt           MPI          9 879428.304688            0 
# Runtime: 11.478332 s (overhead: 0.000078 %) 10 records
