# Sysname : Linux
# Nodename: eu-a6-003-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:45:42 2022
# Execution time and date (local): Tue Jan  4 12:45:42 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 324.855469            0 
            3            32            32   gramschmidt           MPI          1 291.673828            2 
            3            32            32   gramschmidt           MPI          2 245.369141            0 
            3            32            32   gramschmidt           MPI          3 301.937500            0 
            3            32            32   gramschmidt           MPI          4 269.988281            0 
            3            32            32   gramschmidt           MPI          5 254.130859            0 
            3            32            32   gramschmidt           MPI          6 264.408203            0 
            3            32            32   gramschmidt           MPI          7 217.816406            0 
            3            32            32   gramschmidt           MPI          8 210.216797            0 
            3            32            32   gramschmidt           MPI          9 200.128906            0 
# Runtime: 1.004831 s (overhead: 0.000199 %) 10 records
