# Sysname : Linux
# Nodename: eu-a6-003-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:45:42 2022
# Execution time and date (local): Tue Jan  4 12:45:42 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 328.585938            0 
            2            32            32   gramschmidt           MPI          1 294.982422            2 
            2            32            32   gramschmidt           MPI          2 251.830078            0 
            2            32            32   gramschmidt           MPI          3 307.593750            0 
            2            32            32   gramschmidt           MPI          4 277.025391            0 
            2            32            32   gramschmidt           MPI          5 262.318359            0 
            2            32            32   gramschmidt           MPI          6 269.093750            0 
            2            32            32   gramschmidt           MPI          7 222.304688            0 
            2            32            32   gramschmidt           MPI          8 214.021484            0 
            2            32            32   gramschmidt           MPI          9 201.287109            0 
# Runtime: 3.995633 s (overhead: 0.000050 %) 10 records
