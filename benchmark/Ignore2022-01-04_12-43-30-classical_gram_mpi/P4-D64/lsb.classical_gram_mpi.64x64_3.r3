# Sysname : Linux
# Nodename: eu-a6-006-08
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:12 2022
# Execution time and date (local): Tue Jan  4 12:44:12 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 310.703125            0 
            3            64            64   gramschmidt           MPI          1 304.724609            2 
            3            64            64   gramschmidt           MPI          2 285.902344            0 
            3            64            64   gramschmidt           MPI          3 256.992188            0 
            3            64            64   gramschmidt           MPI          4 291.708984            0 
            3            64            64   gramschmidt           MPI          5 262.560547            0 
            3            64            64   gramschmidt           MPI          6 258.900391            0 
            3            64            64   gramschmidt           MPI          7 266.337891            0 
            3            64            64   gramschmidt           MPI          8 272.978516            0 
            3            64            64   gramschmidt           MPI          9 239.679688            0 
# Runtime: 9.006078 s (overhead: 0.000022 %) 10 records
