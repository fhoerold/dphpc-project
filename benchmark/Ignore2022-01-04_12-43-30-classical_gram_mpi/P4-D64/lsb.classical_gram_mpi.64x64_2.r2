# Sysname : Linux
# Nodename: eu-a6-006-08
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:12 2022
# Execution time and date (local): Tue Jan  4 12:44:12 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 309.531250            0 
            2            64            64   gramschmidt           MPI          1 300.296875            2 
            2            64            64   gramschmidt           MPI          2 277.111328            0 
            2            64            64   gramschmidt           MPI          3 248.726562            0 
            2            64            64   gramschmidt           MPI          4 281.560547            0 
            2            64            64   gramschmidt           MPI          5 252.400391            0 
            2            64            64   gramschmidt           MPI          6 249.074219            0 
            2            64            64   gramschmidt           MPI          7 258.919922            0 
            2            64            64   gramschmidt           MPI          8 269.578125            0 
            2            64            64   gramschmidt           MPI          9 230.845703            0 
# Runtime: 11.019736 s (overhead: 0.000018 %) 10 records
