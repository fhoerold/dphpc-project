# Sysname : Linux
# Nodename: eu-a6-010-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:41 2022
# Execution time and date (local): Tue Jan  4 12:44:41 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 284.882812            0 
            3            32            32   gramschmidt           MPI          1 204.439453            2 
            3            32            32   gramschmidt           MPI          2 216.324219            0 
            3            32            32   gramschmidt           MPI          3 187.281250            0 
            3            32            32   gramschmidt           MPI          4 192.734375            0 
            3            32            32   gramschmidt           MPI          5 174.974609            0 
            3            32            32   gramschmidt           MPI          6 170.261719            0 
            3            32            32   gramschmidt           MPI          7 169.617188            0 
            3            32            32   gramschmidt           MPI          8 158.687500            0 
            3            32            32   gramschmidt           MPI          9 153.802734            0 
# Runtime: 6.007002 s (overhead: 0.000033 %) 10 records
