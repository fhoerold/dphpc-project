# Sysname : Linux
# Nodename: eu-a6-010-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:44:41 2022
# Execution time and date (local): Tue Jan  4 12:44:41 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 285.599609            0 
            2            32            32   gramschmidt           MPI          1 206.611328            2 
            2            32            32   gramschmidt           MPI          2 222.931641            0 
            2            32            32   gramschmidt           MPI          3 193.722656            0 
            2            32            32   gramschmidt           MPI          4 200.029297            0 
            2            32            32   gramschmidt           MPI          5 175.009766            0 
            2            32            32   gramschmidt           MPI          6 170.859375            0 
            2            32            32   gramschmidt           MPI          7 175.917969            0 
            2            32            32   gramschmidt           MPI          8 161.751953            0 
            2            32            32   gramschmidt           MPI          9 157.486328            0 
# Runtime: 6.010716 s (overhead: 0.000033 %) 10 records
