# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:24:00 2022
# Execution time and date (local): Tue Jan  4 13:24:00 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 3014.410156            0 
            3           128           128   gramschmidt           MPI          1 2347.503906            3 
            3           128           128   gramschmidt           MPI          2 2272.228516            0 
            3           128           128   gramschmidt           MPI          3 2257.345703            0 
            3           128           128   gramschmidt           MPI          4 2242.414062            0 
            3           128           128   gramschmidt           MPI          5 2215.074219            0 
            3           128           128   gramschmidt           MPI          6 2185.029297            0 
            3           128           128   gramschmidt           MPI          7 2220.707031            0 
            3           128           128   gramschmidt           MPI          8 2201.208984            0 
            3           128           128   gramschmidt           MPI          9 2204.693359            0 
# Runtime: 10.031964 s (overhead: 0.000030 %) 10 records
