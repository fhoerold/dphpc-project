# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:24:00 2022
# Execution time and date (local): Tue Jan  4 13:24:00 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 2469.732422            0 
            2           128           128   gramschmidt           MPI          1 2350.386719            3 
            2           128           128   gramschmidt           MPI          2 2273.412109            0 
            2           128           128   gramschmidt           MPI          3 2257.076172            0 
            2           128           128   gramschmidt           MPI          4 2243.134766            0 
            2           128           128   gramschmidt           MPI          5 2215.658203            0 
            2           128           128   gramschmidt           MPI          6 2185.662109            0 
            2           128           128   gramschmidt           MPI          7 2220.982422            0 
            2           128           128   gramschmidt           MPI          8 2200.851562            0 
            2           128           128   gramschmidt           MPI          9 2203.533203            0 
# Runtime: 7.050039 s (overhead: 0.000043 %) 10 records
