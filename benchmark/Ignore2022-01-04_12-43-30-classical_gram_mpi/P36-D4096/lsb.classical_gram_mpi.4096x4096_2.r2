# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:18:14 2022
# Execution time and date (local): Tue Jan  4 13:18:14 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 21348800.560547            0 
            2          4096          4096   gramschmidt           MPI          1 21186408.183594            5 
            2          4096          4096   gramschmidt           MPI          2 21262718.714844           12 
            2          4096          4096   gramschmidt           MPI          3 21129186.855469            0 
            2          4096          4096   gramschmidt           MPI          4 20848906.871094            2 
            2          4096          4096   gramschmidt           MPI          5 21345679.654297            1 
            2          4096          4096   gramschmidt           MPI          6 21190036.425781            0 
            2          4096          4096   gramschmidt           MPI          7 21457526.175781            0 
            2          4096          4096   gramschmidt           MPI          8 21132085.531250            9 
            2          4096          4096   gramschmidt           MPI          9 21066228.767578            0 
# Runtime: 281.328131 s (overhead: 0.000010 %) 10 records
