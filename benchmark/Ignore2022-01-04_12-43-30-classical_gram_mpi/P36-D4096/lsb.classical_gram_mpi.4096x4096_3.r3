# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 12:18:14 2022
# Execution time and date (local): Tue Jan  4 13:18:14 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 21350003.449219            0 
            3          4096          4096   gramschmidt           MPI          1 21187091.908203            6 
            3          4096          4096   gramschmidt           MPI          2 21263947.898438           11 
            3          4096          4096   gramschmidt           MPI          3 21129981.976562            0 
            3          4096          4096   gramschmidt           MPI          4 20849716.828125            2 
            3          4096          4096   gramschmidt           MPI          5 21346858.195312            0 
            3          4096          4096   gramschmidt           MPI          6 21190832.009766            0 
            3          4096          4096   gramschmidt           MPI          7 21458304.923828            3 
            3          4096          4096   gramschmidt           MPI          8 21133342.128906            6 
            3          4096          4096   gramschmidt           MPI          9 21067021.529297            0 
# Runtime: 282.306807 s (overhead: 0.000010 %) 10 records
