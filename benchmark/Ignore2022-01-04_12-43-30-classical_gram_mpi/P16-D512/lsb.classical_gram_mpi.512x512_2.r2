# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:46:56 2022
# Execution time and date (local): Tue Jan  4 12:46:56 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 25638.025391            0 
            2           512           512   gramschmidt           MPI          1 25065.466797            4 
            2           512           512   gramschmidt           MPI          2 24901.201172            1 
            2           512           512   gramschmidt           MPI          3 24834.572266            0 
            2           512           512   gramschmidt           MPI          4 24719.236328            1 
            2           512           512   gramschmidt           MPI          5 45392.091797            0 
            2           512           512   gramschmidt           MPI          6 24847.851562            0 
            2           512           512   gramschmidt           MPI          7 24315.593750            0 
            2           512           512   gramschmidt           MPI          8 24390.652344            2 
            2           512           512   gramschmidt           MPI          9 24985.970703            0 
# Runtime: 8.451380 s (overhead: 0.000095 %) 10 records
