# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:46:56 2022
# Execution time and date (local): Tue Jan  4 12:46:56 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 25638.003906            0 
            3           512           512   gramschmidt           MPI          1 25075.408203            5 
            3           512           512   gramschmidt           MPI          2 24900.775391            2 
            3           512           512   gramschmidt           MPI          3 24760.994141            0 
            3           512           512   gramschmidt           MPI          4 24677.445312            1 
            3           512           512   gramschmidt           MPI          5 45384.115234            0 
            3           512           512   gramschmidt           MPI          6 24842.720703            0 
            3           512           512   gramschmidt           MPI          7 24318.550781            0 
            3           512           512   gramschmidt           MPI          8 24377.525391            1 
            3           512           512   gramschmidt           MPI          9 24990.437500            0 
# Runtime: 9.441453 s (overhead: 0.000095 %) 10 records
