# Sysname : Linux
# Nodename: eu-a6-001-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:45:11 2022
# Execution time and date (local): Tue Jan  4 12:45:11 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 67248490.857422            0 
            3          4096          4096   gramschmidt           MPI          1 63672213.376953            9 
            3          4096          4096   gramschmidt           MPI          2 64778352.738281            5 
            3          4096          4096   gramschmidt           MPI          3 64909301.820312            0 
            3          4096          4096   gramschmidt           MPI          4 64402158.921875            8 
            3          4096          4096   gramschmidt           MPI          5 65141083.777344            4 
            3          4096          4096   gramschmidt           MPI          6 66526938.197266            2 
            3          4096          4096   gramschmidt           MPI          7 64808166.820312            0 
            3          4096          4096   gramschmidt           MPI          8 64750807.476562            9 
            3          4096          4096   gramschmidt           MPI          9 64400341.443359            2 
# Runtime: 851.120198 s (overhead: 0.000005 %) 10 records
