# Sysname : Linux
# Nodename: eu-a6-001-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:45:11 2022
# Execution time and date (local): Tue Jan  4 12:45:11 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 67244595.910156            0 
            2          4096          4096   gramschmidt           MPI          1 63668137.609375            6 
            2          4096          4096   gramschmidt           MPI          2 64773716.429688            5 
            2          4096          4096   gramschmidt           MPI          3 64905313.007812            0 
            2          4096          4096   gramschmidt           MPI          4 64398231.773438            4 
            2          4096          4096   gramschmidt           MPI          5 65137362.230469            0 
            2          4096          4096   gramschmidt           MPI          6 66523250.433594            2 
            2          4096          4096   gramschmidt           MPI          7 64803749.443359            2 
            2          4096          4096   gramschmidt           MPI          8 64746621.660156            4 
            2          4096          4096   gramschmidt           MPI          9 64396534.441406            0 
# Runtime: 853.107857 s (overhead: 0.000003 %) 10 records
