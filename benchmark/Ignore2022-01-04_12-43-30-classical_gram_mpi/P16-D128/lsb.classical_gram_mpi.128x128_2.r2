# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:46:41 2022
# Execution time and date (local): Tue Jan  4 12:46:41 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 1307.544922            0 
            2           128           128   gramschmidt           MPI          1 1221.539062            2 
            2           128           128   gramschmidt           MPI          2 1193.265625            0 
            2           128           128   gramschmidt           MPI          3 1263.685547            0 
            2           128           128   gramschmidt           MPI          4 1213.466797            0 
            2           128           128   gramschmidt           MPI          5 1155.287109            0 
            2           128           128   gramschmidt           MPI          6 1136.666016            0 
            2           128           128   gramschmidt           MPI          7 1137.998047            0 
            2           128           128   gramschmidt           MPI          8 1064.951172            0 
            2           128           128   gramschmidt           MPI          9 1093.708984            0 
# Runtime: 4.017369 s (overhead: 0.000050 %) 10 records
