# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 11:46:41 2022
# Execution time and date (local): Tue Jan  4 12:46:41 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 1309.273438            0 
            3           128           128   gramschmidt           MPI          1 1221.460938            2 
            3           128           128   gramschmidt           MPI          2 1192.792969            0 
            3           128           128   gramschmidt           MPI          3 1265.568359            0 
            3           128           128   gramschmidt           MPI          4 1214.599609            0 
            3           128           128   gramschmidt           MPI          5 1155.904297            0 
            3           128           128   gramschmidt           MPI          6 1139.376953            0 
            3           128           128   gramschmidt           MPI          7 1141.328125            0 
            3           128           128   gramschmidt           MPI          8 1067.365234            0 
            3           128           128   gramschmidt           MPI          9 1095.460938            0 
# Runtime: 4.011793 s (overhead: 0.000050 %) 10 records
