# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:22:48 2022
# Execution time and date (local): Fri Jan  7 10:22:48 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 2128.654297            0 
            3           256           MPI          1 1686.830078            0 
            3           256           MPI          2 1566.695312            0 
            3           256           MPI          3 1555.900391            0 
            3           256           MPI          4 1607.691406            0 
# Runtime: 0.015384 s (overhead: 0.000000 %) 5 records
