# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:22:48 2022
# Execution time and date (local): Fri Jan  7 10:22:48 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 1719.494141            0 
            2           256           MPI          1 1643.828125            3 
            2           256           MPI          2 1528.326172            0 
            2           256           MPI          3 1509.187500            0 
            2           256           MPI          4 1556.652344            0 
# Runtime: 8.017605 s (overhead: 0.000037 %) 5 records
