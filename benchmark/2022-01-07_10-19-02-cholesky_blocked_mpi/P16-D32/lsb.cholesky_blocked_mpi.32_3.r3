# Sysname : Linux
# Nodename: eu-a6-004-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:39:16 2022
# Execution time and date (local): Fri Jan  7 10:39:16 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 4.712891            0 
            3            32           MPI          1 0.058594            2 
            3            32           MPI          2 0.027344            0 
            3            32           MPI          3 0.035156            0 
            3            32           MPI          4 0.017578            0 
# Runtime: 4.000051 s (overhead: 0.000050 %) 5 records
