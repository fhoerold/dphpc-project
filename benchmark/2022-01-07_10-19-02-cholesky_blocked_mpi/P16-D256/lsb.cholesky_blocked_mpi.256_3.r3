# Sysname : Linux
# Nodename: eu-a6-012-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:41:29 2022
# Execution time and date (local): Fri Jan  7 10:41:29 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 2614.580078            0 
            3           256           MPI          1 2656.751953            3 
            3           256           MPI          2 2973.787109            0 
            3           256           MPI          3 2491.335938            0 
            3           256           MPI          4 2543.804688            0 
# Runtime: 10.023481 s (overhead: 0.000030 %) 5 records
