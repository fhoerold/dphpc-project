# Sysname : Linux
# Nodename: eu-a6-012-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:41:29 2022
# Execution time and date (local): Fri Jan  7 10:41:29 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 2606.443359            0 
            2           256           MPI          1 2886.978516            3 
            2           256           MPI          2 2640.855469            0 
            2           256           MPI          3 2492.855469            0 
            2           256           MPI          4 2545.287109            0 
# Runtime: 17.023872 s (overhead: 0.000018 %) 5 records
