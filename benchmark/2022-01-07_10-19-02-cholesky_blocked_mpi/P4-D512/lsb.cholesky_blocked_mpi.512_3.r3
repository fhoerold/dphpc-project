# Sysname : Linux
# Nodename: eu-a6-002-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:21:18 2022
# Execution time and date (local): Fri Jan  7 10:21:18 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 11662.339844            0 
            3           512           MPI          1 11996.804688            4 
            3           512           MPI          2 11182.367188            1 
            3           512           MPI          3 11073.042969            0 
            3           512           MPI          4 12191.646484            1 
# Runtime: 1.087567 s (overhead: 0.000552 %) 5 records
