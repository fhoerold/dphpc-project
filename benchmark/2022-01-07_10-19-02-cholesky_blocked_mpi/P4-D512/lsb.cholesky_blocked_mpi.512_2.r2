# Sysname : Linux
# Nodename: eu-a6-002-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:21:18 2022
# Execution time and date (local): Fri Jan  7 10:21:18 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 11727.777344            0 
            2           512           MPI          1 11500.613281            1 
            2           512           MPI          2 11135.099609            1 
            2           512           MPI          3 11068.742188            0 
            2           512           MPI          4 12126.537109            1 
# Runtime: 0.087134 s (overhead: 0.003443 %) 5 records
