# Sysname : Linux
# Nodename: eu-a6-004-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:39:27 2022
# Execution time and date (local): Fri Jan  7 10:39:27 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 961.705078            0 
            3           128           MPI          1 935.240234            2 
            3           128           MPI          2 899.099609            0 
            3           128           MPI          3 572.945312            0 
            3           128           MPI          4 568.306641            0 
# Runtime: 14.007875 s (overhead: 0.000014 %) 5 records
