# Sysname : Linux
# Nodename: eu-a6-004-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:39:27 2022
# Execution time and date (local): Fri Jan  7 10:39:27 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 665.945312            0 
            2           128           MPI          1 939.853516            2 
            2           128           MPI          2 902.583984            0 
            2           128           MPI          3 573.841797            0 
            2           128           MPI          4 569.292969            0 
# Runtime: 12.013156 s (overhead: 0.000017 %) 5 records
