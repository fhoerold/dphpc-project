# Sysname : Linux
# Nodename: eu-a6-012-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:50:28 2022
# Execution time and date (local): Fri Jan  7 10:50:28 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 2591059.810547            0 
            2          4096           MPI          1 2469530.890625            1 
            2          4096           MPI          2 2523091.621094            1 
            2          4096           MPI          3 2545707.103516            0 
            2          4096           MPI          4 2562702.634766            6 
# Runtime: 31.358181 s (overhead: 0.000026 %) 5 records
