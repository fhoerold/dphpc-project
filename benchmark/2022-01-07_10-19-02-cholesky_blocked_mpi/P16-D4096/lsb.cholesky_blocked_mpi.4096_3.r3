# Sysname : Linux
# Nodename: eu-a6-012-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:50:28 2022
# Execution time and date (local): Fri Jan  7 10:50:28 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 2591091.964844            0 
            3          4096           MPI          1 2469560.873047            2 
            3          4096           MPI          2 2523119.599609            1 
            3          4096           MPI          3 2545734.609375            0 
            3          4096           MPI          4 2562734.068359            1 
# Runtime: 26.357922 s (overhead: 0.000015 %) 5 records
