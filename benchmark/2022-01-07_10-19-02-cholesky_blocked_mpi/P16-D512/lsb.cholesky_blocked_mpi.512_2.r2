# Sysname : Linux
# Nodename: eu-a6-012-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:41:54 2022
# Execution time and date (local): Fri Jan  7 10:41:54 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 8307.800781            0 
            2           512           MPI          1 8090.298828            3 
            2           512           MPI          2 8319.599609            0 
            2           512           MPI          3 8159.333984            0 
            2           512           MPI          4 8049.707031            0 
# Runtime: 8.072248 s (overhead: 0.000037 %) 5 records
