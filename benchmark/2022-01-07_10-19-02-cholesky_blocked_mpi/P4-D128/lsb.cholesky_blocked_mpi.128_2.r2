# Sysname : Linux
# Nodename: eu-a6-012-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:20:13 2022
# Execution time and date (local): Fri Jan  7 10:20:13 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 675.048828            0 
            2           128           MPI          1 364.943359            0 
            2           128           MPI          2 328.382812            0 
            2           128           MPI          3 317.001953            0 
            2           128           MPI          4 310.210938            0 
# Runtime: 0.006282 s (overhead: 0.000000 %) 5 records
