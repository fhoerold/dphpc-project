# Sysname : Linux
# Nodename: eu-a6-012-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:20:13 2022
# Execution time and date (local): Fri Jan  7 10:20:13 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 720.662109            0 
            3           128           MPI          1 397.566406            0 
            3           128           MPI          2 677.306641            0 
            3           128           MPI          3 349.011719            0 
            3           128           MPI          4 344.369141            0 
# Runtime: 0.004542 s (overhead: 0.000000 %) 5 records
