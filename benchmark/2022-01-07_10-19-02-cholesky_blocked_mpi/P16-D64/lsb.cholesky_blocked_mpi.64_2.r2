# Sysname : Linux
# Nodename: eu-a6-012-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:41:16 2022
# Execution time and date (local): Fri Jan  7 10:41:16 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            64           MPI          0 121.798828            0 
            2            64           MPI          1 185.730469            0 
            2            64           MPI          2 374.949219            0 
            2            64           MPI          3 84.330078            0 
            2            64           MPI          4 82.839844            0 
# Runtime: 0.003990 s (overhead: 0.000000 %) 5 records
