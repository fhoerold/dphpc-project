# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:15:48 2022
# Execution time and date (local): Fri Jan  7 12:15:48 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            64           MPI          0 101.990234            0 
            2            64           MPI          1 88.802734            2 
            2            64           MPI          2 46.509766            0 
            2            64           MPI          3 41.853516            0 
            2            64           MPI          4 40.888672            0 
# Runtime: 13.033903 s (overhead: 0.000015 %) 5 records
