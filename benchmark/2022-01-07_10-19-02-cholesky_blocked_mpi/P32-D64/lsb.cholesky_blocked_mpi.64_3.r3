# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:15:48 2022
# Execution time and date (local): Fri Jan  7 12:15:48 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            64           MPI          0 24.193359            0 
            3            64           MPI          1 48.453125            2 
            3            64           MPI          2 43.886719            0 
            3            64           MPI          3 39.261719            0 
            3            64           MPI          4 38.230469            0 
# Runtime: 13.018419 s (overhead: 0.000015 %) 5 records
