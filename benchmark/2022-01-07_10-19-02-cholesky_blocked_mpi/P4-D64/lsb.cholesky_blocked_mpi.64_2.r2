# Sysname : Linux
# Nodename: eu-a6-012-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:20:05 2022
# Execution time and date (local): Fri Jan  7 10:20:05 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            64           MPI          0 68.353516            1 
            2            64           MPI          1 355.160156            2 
            2            64           MPI          2 57.298828            0 
            2            64           MPI          3 62.152344            0 
            2            64           MPI          4 57.068359            0 
# Runtime: 0.996016 s (overhead: 0.000301 %) 5 records
