# Sysname : Linux
# Nodename: eu-a6-012-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:20:05 2022
# Execution time and date (local): Fri Jan  7 10:20:05 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            64           MPI          0 65.367188            0 
            3            64           MPI          1 42.992188            2 
            3            64           MPI          2 59.353516            0 
            3            64           MPI          3 65.902344            0 
            3            64           MPI          4 59.166016            0 
# Runtime: 1.000469 s (overhead: 0.000200 %) 5 records
