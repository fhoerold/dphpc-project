# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:23:47 2022
# Execution time and date (local): Fri Jan  7 10:23:47 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 377294.525391            0 
            2          2048           MPI          1 376224.345703            1 
            2          2048           MPI          2 376938.257812            1 
            2          2048           MPI          3 374135.685547            0 
            2          2048           MPI          4 374939.673828            1 
# Runtime: 4.699650 s (overhead: 0.000064 %) 5 records
