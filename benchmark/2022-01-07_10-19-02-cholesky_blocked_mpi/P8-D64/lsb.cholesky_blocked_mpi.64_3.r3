# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:28:40 2022
# Execution time and date (local): Fri Jan  7 10:28:40 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            64           MPI          0 87.435547            0 
            3            64           MPI          1 78.365234            2 
            3            64           MPI          2 84.082031            0 
            3            64           MPI          3 70.144531            0 
            3            64           MPI          4 65.912109            0 
# Runtime: 2.997425 s (overhead: 0.000067 %) 5 records
