# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:28:40 2022
# Execution time and date (local): Fri Jan  7 10:28:40 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            64           MPI          0 84.099609            0 
            2            64           MPI          1 78.683594            3 
            2            64           MPI          2 83.486328            0 
            2            64           MPI          3 66.777344            0 
            2            64           MPI          4 64.933594            0 
# Runtime: 1.999659 s (overhead: 0.000150 %) 5 records
