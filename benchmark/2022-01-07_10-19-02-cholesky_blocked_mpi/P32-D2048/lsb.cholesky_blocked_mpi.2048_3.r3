# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:18:05 2022
# Execution time and date (local): Fri Jan  7 12:18:05 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 298583.503906            0 
            3          2048           MPI          1 298456.306641            4 
            3          2048           MPI          2 298674.220703            1 
            3          2048           MPI          3 297948.447266            0 
            3          2048           MPI          4 298315.414062            1 
# Runtime: 10.135948 s (overhead: 0.000059 %) 5 records
