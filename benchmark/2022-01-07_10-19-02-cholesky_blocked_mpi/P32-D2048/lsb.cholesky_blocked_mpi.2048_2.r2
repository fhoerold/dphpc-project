# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:18:05 2022
# Execution time and date (local): Fri Jan  7 12:18:05 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 298563.298828            0 
            2          2048           MPI          1 298432.400391            4 
            2          2048           MPI          2 298650.707031            1 
            2          2048           MPI          3 297925.431641            0 
            2          2048           MPI          4 298292.498047            1 
# Runtime: 13.137437 s (overhead: 0.000046 %) 5 records
