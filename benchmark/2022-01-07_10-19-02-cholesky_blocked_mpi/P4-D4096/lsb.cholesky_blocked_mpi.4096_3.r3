# Sysname : Linux
# Nodename: eu-a6-001-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:29:24 2022
# Execution time and date (local): Fri Jan  7 10:29:24 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 4596961.128906            0 
            3          4096           MPI          1 4489437.070312            6 
            3          4096           MPI          2 4517219.794922            1 
            3          4096           MPI          3 4501414.466797            0 
            3          4096           MPI          4 4694915.410156            6 
# Runtime: 38.219636 s (overhead: 0.000034 %) 5 records
