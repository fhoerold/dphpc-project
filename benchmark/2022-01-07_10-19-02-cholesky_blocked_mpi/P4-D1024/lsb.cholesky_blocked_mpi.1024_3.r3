# Sysname : Linux
# Nodename: eu-a6-002-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:21:32 2022
# Execution time and date (local): Fri Jan  7 10:21:32 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 79979.089844            0 
            3          1024           MPI          1 80441.572266            5 
            3          1024           MPI          2 80109.298828            1 
            3          1024           MPI          3 77368.589844            0 
            3          1024           MPI          4 77875.982422            1 
# Runtime: 4.570812 s (overhead: 0.000153 %) 5 records
