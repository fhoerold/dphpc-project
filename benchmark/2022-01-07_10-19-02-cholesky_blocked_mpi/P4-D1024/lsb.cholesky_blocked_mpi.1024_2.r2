# Sysname : Linux
# Nodename: eu-a6-002-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:21:32 2022
# Execution time and date (local): Fri Jan  7 10:21:32 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 79979.312500            3 
            2          1024           MPI          1 80442.646484            6 
            2          1024           MPI          2 80097.080078            1 
            2          1024           MPI          3 77366.263672            0 
            2          1024           MPI          4 77871.988281            1 
# Runtime: 4.571243 s (overhead: 0.000241 %) 5 records
