# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:39:02 2022
# Execution time and date (local): Fri Jan  7 12:39:02 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 2515885.781250            0 
            3          4096           MPI          1 2415376.142578            1 
            3          4096           MPI          2 2455249.960938            1 
            3          4096           MPI          3 2448224.691406            0 
            3          4096           MPI          4 2469848.931641            1 
# Runtime: 20.487141 s (overhead: 0.000015 %) 5 records
