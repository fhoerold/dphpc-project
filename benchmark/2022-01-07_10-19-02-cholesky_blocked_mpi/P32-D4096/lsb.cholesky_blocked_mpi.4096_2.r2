# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:39:02 2022
# Execution time and date (local): Fri Jan  7 12:39:02 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 2515854.208984            0 
            2          4096           MPI          1 2415347.603516            1 
            2          4096           MPI          2 2455220.988281            1 
            2          4096           MPI          3 2448195.626953            0 
            2          4096           MPI          4 2469819.246094            1 
# Runtime: 26.489849 s (overhead: 0.000011 %) 5 records
