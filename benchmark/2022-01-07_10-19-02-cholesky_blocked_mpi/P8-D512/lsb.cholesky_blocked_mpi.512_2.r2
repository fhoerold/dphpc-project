# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:29:12 2022
# Execution time and date (local): Fri Jan  7 10:29:12 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 7928.980469            0 
            2           512           MPI          1 7792.517578            4 
            2           512           MPI          2 7598.556641            0 
            2           512           MPI          3 7811.203125            0 
            2           512           MPI          4 7640.132812            0 
# Runtime: 4.063956 s (overhead: 0.000098 %) 5 records
