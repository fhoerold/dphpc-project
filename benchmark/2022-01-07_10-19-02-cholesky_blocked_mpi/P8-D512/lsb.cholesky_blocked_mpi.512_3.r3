# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:29:12 2022
# Execution time and date (local): Fri Jan  7 10:29:12 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 8294.525391            0 
            3           512           MPI          1 7856.152344            4 
            3           512           MPI          2 7645.726562            0 
            3           512           MPI          3 7864.353516            0 
            3           512           MPI          4 7684.404297            1 
# Runtime: 4.064494 s (overhead: 0.000123 %) 5 records
