# Sysname : Linux
# Nodename: eu-a6-012-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:21:07 2022
# Execution time and date (local): Fri Jan  7 10:21:07 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 513662.380859            0 
            3          2048           MPI          1 517703.914062            1 
            3          2048           MPI          2 515977.541016            1 
            3          2048           MPI          3 512952.816406            0 
            3          2048           MPI          4 517226.654297            1 
# Runtime: 3.706438 s (overhead: 0.000081 %) 5 records
