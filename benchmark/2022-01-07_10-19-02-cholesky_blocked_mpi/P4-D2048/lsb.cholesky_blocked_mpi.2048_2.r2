# Sysname : Linux
# Nodename: eu-a6-012-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:21:07 2022
# Execution time and date (local): Fri Jan  7 10:21:07 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 513905.478516            0 
            2          2048           MPI          1 517943.470703            1 
            2          2048           MPI          2 516219.185547            1 
            2          2048           MPI          3 513188.623047            0 
            2          2048           MPI          4 517466.302734            2 
# Runtime: 3.716926 s (overhead: 0.000108 %) 5 records
