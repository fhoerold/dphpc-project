# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:16:34 2022
# Execution time and date (local): Fri Jan  7 12:16:34 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 3094.421875            0 
            3           256           MPI          1 3214.964844            2 
            3           256           MPI          2 2893.820312            0 
            3           256           MPI          3 2819.087891            0 
            3           256           MPI          4 2721.224609            0 
# Runtime: 12.005239 s (overhead: 0.000017 %) 5 records
