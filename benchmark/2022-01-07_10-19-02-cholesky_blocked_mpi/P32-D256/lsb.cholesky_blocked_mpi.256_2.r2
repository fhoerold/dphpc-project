# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:16:34 2022
# Execution time and date (local): Fri Jan  7 12:16:34 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 3406.101562            0 
            2           256           MPI          1 3243.833984            3 
            2           256           MPI          2 2895.111328            0 
            2           256           MPI          3 2822.802734            0 
            2           256           MPI          4 2722.523438            0 
# Runtime: 12.019334 s (overhead: 0.000025 %) 5 records
