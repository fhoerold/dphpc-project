# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:30:10 2022
# Execution time and date (local): Fri Jan  7 12:30:10 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 52110.125000            0 
            2          1024           MPI          1 51458.558594            3 
            2          1024           MPI          2 51684.199219            1 
            2          1024           MPI          3 51555.947266            0 
            2          1024           MPI          4 51806.939453            1 
# Runtime: 6.384201 s (overhead: 0.000078 %) 5 records
