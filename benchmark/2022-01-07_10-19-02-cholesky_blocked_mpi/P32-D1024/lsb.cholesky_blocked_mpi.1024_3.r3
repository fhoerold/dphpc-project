# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:30:10 2022
# Execution time and date (local): Fri Jan  7 12:30:10 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 52109.042969            0 
            3          1024           MPI          1 51457.242188            4 
            3          1024           MPI          2 51682.296875            1 
            3          1024           MPI          3 51554.369141            0 
            3          1024           MPI          4 51805.572266            1 
# Runtime: 6.381931 s (overhead: 0.000094 %) 5 records
