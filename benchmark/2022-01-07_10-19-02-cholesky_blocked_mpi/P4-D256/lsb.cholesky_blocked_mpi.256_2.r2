# Sysname : Linux
# Nodename: eu-a6-002-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:21:04 2022
# Execution time and date (local): Fri Jan  7 10:21:04 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 1816.646484            0 
            2           256           MPI          1 1769.837891            3 
            2           256           MPI          2 1789.669922            0 
            2           256           MPI          3 1918.632812            0 
            2           256           MPI          4 2015.294922            0 
# Runtime: 3.017467 s (overhead: 0.000099 %) 5 records
