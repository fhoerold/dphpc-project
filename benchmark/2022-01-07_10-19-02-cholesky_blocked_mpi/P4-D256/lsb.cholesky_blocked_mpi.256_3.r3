# Sysname : Linux
# Nodename: eu-a6-002-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:21:04 2022
# Execution time and date (local): Fri Jan  7 10:21:04 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 2618.578125            0 
            3           256           MPI          1 1811.408203            3 
            3           256           MPI          2 1820.871094            0 
            3           256           MPI          3 1960.175781            0 
            3           256           MPI          4 2052.621094            0 
# Runtime: 3.016581 s (overhead: 0.000099 %) 5 records
