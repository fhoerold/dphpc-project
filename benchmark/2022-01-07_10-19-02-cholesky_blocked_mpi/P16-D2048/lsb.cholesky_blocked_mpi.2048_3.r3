# Sysname : Linux
# Nodename: eu-a6-004-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:40:36 2022
# Execution time and date (local): Fri Jan  7 10:40:36 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 292020.744141            0 
            3          2048           MPI          1 303397.453125            1 
            3          2048           MPI          2 297433.328125            1 
            3          2048           MPI          3 309148.707031            0 
            3          2048           MPI          4 304558.369141            1 
# Runtime: 7.159371 s (overhead: 0.000042 %) 5 records
