# Sysname : Linux
# Nodename: eu-a6-004-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:40:36 2022
# Execution time and date (local): Fri Jan  7 10:40:36 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 292018.814453            0 
            2          2048           MPI          1 303395.001953            1 
            2          2048           MPI          2 297430.529297            1 
            2          2048           MPI          3 309145.490234            0 
            2          2048           MPI          4 304555.808594            1 
# Runtime: 4.158375 s (overhead: 0.000072 %) 5 records
