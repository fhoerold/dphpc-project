# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:29:29 2022
# Execution time and date (local): Fri Jan  7 10:29:29 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 54577.000000            0 
            3          1024           MPI          1 55380.427734            4 
            3          1024           MPI          2 56385.115234            1 
            3          1024           MPI          3 56896.554688            0 
            3          1024           MPI          4 57219.363281            1 
# Runtime: 6.401163 s (overhead: 0.000094 %) 5 records
