# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:29:29 2022
# Execution time and date (local): Fri Jan  7 10:29:29 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 54560.167969            0 
            2          1024           MPI          1 55316.994141            5 
            2          1024           MPI          2 55874.972656            5 
            2          1024           MPI          3 56750.376953            0 
            2          1024           MPI          4 57198.732422            6 
# Runtime: 4.400975 s (overhead: 0.000364 %) 5 records
