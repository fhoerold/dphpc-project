# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:29:49 2022
# Execution time and date (local): Fri Jan  7 12:29:49 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 5.613281            0 
            3            32           MPI          1 0.087891            5 
            3            32           MPI          2 0.058594            0 
            3            32           MPI          3 0.015625            0 
            3            32           MPI          4 0.021484            0 
# Runtime: 9.016278 s (overhead: 0.000055 %) 5 records
