# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:29:49 2022
# Execution time and date (local): Fri Jan  7 12:29:49 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 3.511719            0 
            2            32           MPI          1 0.095703            0 
            2            32           MPI          2 0.048828            0 
            2            32           MPI          3 0.039062            0 
            2            32           MPI          4 0.019531            0 
# Runtime: 0.001035 s (overhead: 0.000000 %) 5 records
