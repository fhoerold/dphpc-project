# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:16:11 2022
# Execution time and date (local): Fri Jan  7 12:16:11 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 1316.345703            0 
            2           128           MPI          1 884.689453            2 
            2           128           MPI          2 809.296875            0 
            2           128           MPI          3 740.285156            0 
            2           128           MPI          4 738.947266            0 
# Runtime: 15.016151 s (overhead: 0.000013 %) 5 records
