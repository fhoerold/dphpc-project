# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:16:11 2022
# Execution time and date (local): Fri Jan  7 12:16:11 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 1108.333984            0 
            3           128           MPI          1 881.099609            2 
            3           128           MPI          2 804.548828            0 
            3           128           MPI          3 739.576172            0 
            3           128           MPI          4 736.380859            0 
# Runtime: 16.007272 s (overhead: 0.000012 %) 5 records
