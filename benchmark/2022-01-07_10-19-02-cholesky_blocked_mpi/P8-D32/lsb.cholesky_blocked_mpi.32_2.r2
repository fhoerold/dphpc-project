# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:22:33 2022
# Execution time and date (local): Fri Jan  7 10:22:33 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 5.582031            1 
            2            32           MPI          1 0.074219            3 
            2            32           MPI          2 0.033203            1 
            2            32           MPI          3 0.015625            0 
            2            32           MPI          4 0.015625            0 
# Runtime: 2.002777 s (overhead: 0.000250 %) 5 records
