# Sysname : Linux
# Nodename: eu-a6-012-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:42:15 2022
# Execution time and date (local): Fri Jan  7 10:42:15 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 40768.736328            0 
            3          1024           MPI          1 40298.667969            3 
            3          1024           MPI          2 39638.318359            1 
            3          1024           MPI          3 39865.183594            0 
            3          1024           MPI          4 39500.451172            1 
# Runtime: 4.299806 s (overhead: 0.000116 %) 5 records
