# Sysname : Linux
# Nodename: eu-a6-012-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:42:15 2022
# Execution time and date (local): Fri Jan  7 10:42:15 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 40773.441406            0 
            2          1024           MPI          1 40302.970703            3 
            2          1024           MPI          2 39641.886719            1 
            2          1024           MPI          3 39869.207031            0 
            2          1024           MPI          4 39504.363281            1 
# Runtime: 6.304327 s (overhead: 0.000079 %) 5 records
