# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:16:58 2022
# Execution time and date (local): Fri Jan  7 12:16:58 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 10442.724609            0 
            2           512           MPI          1 10150.806641            3 
            2           512           MPI          2 10038.681641            0 
            2           512           MPI          3 9960.787109            0 
            2           512           MPI          4 9974.011719            0 
# Runtime: 9.094092 s (overhead: 0.000033 %) 5 records
