# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 10:54:41 2022
# Execution time and date (local): Fri Jan  7 11:54:41 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 37944735.960938            0 
            2          8192           MPI          1 37222961.593750            5 
            2          8192           MPI          2 37130362.263672            6 
            2          8192           MPI          3 36932240.507812            0 
            2          8192           MPI          4 36955473.708984            5 
# Runtime: 269.040880 s (overhead: 0.000006 %) 5 records
