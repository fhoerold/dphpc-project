# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 10:54:41 2022
# Execution time and date (local): Fri Jan  7 11:54:41 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 37943237.476562            0 
            3          8192           MPI          1 37221448.302734            3 
            3          8192           MPI          2 37128851.150391            3 
            3          8192           MPI          3 36930725.283203            0 
            3          8192           MPI          4 36953964.304688            4 
# Runtime: 274.030058 s (overhead: 0.000004 %) 5 records
