# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:20:04 2022
# Execution time and date (local): Fri Jan  7 10:20:04 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 5.689453            0 
            3            32           MPI          1 0.166016            0 
            3            32           MPI          2 0.042969            0 
            3            32           MPI          3 0.017578            0 
            3            32           MPI          4 0.017578            0 
# Runtime: 0.000584 s (overhead: 0.000000 %) 5 records
