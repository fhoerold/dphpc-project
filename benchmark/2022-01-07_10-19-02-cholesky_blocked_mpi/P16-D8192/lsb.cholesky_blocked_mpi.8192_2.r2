# Sysname : Linux
# Nodename: eu-a6-009-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:36:26 2022
# Execution time and date (local): Fri Jan  7 12:36:26 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 18884438.814453            0 
            2          8192           MPI          1 18908379.101562            5 
            2          8192           MPI          2 20504841.544922            5 
            2          8192           MPI          3 19849176.197266            0 
            2          8192           MPI          4 18869831.203125            6 
# Runtime: 147.991208 s (overhead: 0.000011 %) 5 records
