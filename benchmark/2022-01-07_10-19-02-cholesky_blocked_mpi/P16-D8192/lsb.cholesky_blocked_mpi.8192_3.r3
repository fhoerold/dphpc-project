# Sysname : Linux
# Nodename: eu-a6-009-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:36:26 2022
# Execution time and date (local): Fri Jan  7 12:36:26 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 18884611.972656            0 
            3          8192           MPI          1 18908552.855469            7 
            3          8192           MPI          2 20505027.785156            8 
            3          8192           MPI          3 19849358.716797            0 
            3          8192           MPI          4 18870004.933594            2 
# Runtime: 147.992045 s (overhead: 0.000011 %) 5 records
