# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:10:40 2022
# Execution time and date (local): Fri Jan  7 12:10:40 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 20684873.185547            0 
            2          8192           MPI          1 20727222.064453            8 
            2          8192           MPI          2 20695105.042969            7 
            2          8192           MPI          3 20690765.972656            0 
            2          8192           MPI          4 20842025.613281            1 
# Runtime: 156.670801 s (overhead: 0.000010 %) 5 records
