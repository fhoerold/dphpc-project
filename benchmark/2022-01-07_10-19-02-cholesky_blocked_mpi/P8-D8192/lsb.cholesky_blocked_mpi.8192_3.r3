# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:10:40 2022
# Execution time and date (local): Fri Jan  7 12:10:40 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 20671653.855469            0 
            3          8192           MPI          1 20713959.566406            4 
            3          8192           MPI          2 20681867.748047            4 
            3          8192           MPI          3 20677533.054688            0 
            3          8192           MPI          4 20828703.339844            4 
# Runtime: 156.565429 s (overhead: 0.000008 %) 5 records
