# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:28:52 2022
# Execution time and date (local): Fri Jan  7 10:28:52 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 581.550781            0 
            2           128           MPI          1 509.279297            3 
            2           128           MPI          2 493.464844            0 
            2           128           MPI          3 474.486328            0 
            2           128           MPI          4 431.814453            0 
# Runtime: 14.007991 s (overhead: 0.000021 %) 5 records
