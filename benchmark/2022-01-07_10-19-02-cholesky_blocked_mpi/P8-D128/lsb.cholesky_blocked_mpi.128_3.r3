# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:28:52 2022
# Execution time and date (local): Fri Jan  7 10:28:52 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 915.695312            0 
            3           128           MPI          1 535.271484            2 
            3           128           MPI          2 526.949219            0 
            3           128           MPI          3 506.880859            0 
            3           128           MPI          4 482.039062            0 
# Runtime: 7.006247 s (overhead: 0.000029 %) 5 records
