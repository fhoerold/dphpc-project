# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:38:29 2022
# Execution time and date (local): Fri Jan  7 10:38:29 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 2837630.492188            2 
            3          4096           MPI          1 2959104.527344            7 
            3          4096           MPI          2 2914275.806641            3 
            3          4096           MPI          3 2847512.291016            0 
            3          4096           MPI          4 2912495.185547            5 
# Runtime: 23.945655 s (overhead: 0.000071 %) 5 records
