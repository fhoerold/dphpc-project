# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:38:29 2022
# Execution time and date (local): Fri Jan  7 10:38:29 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 2837613.271484            3 
            2          4096           MPI          1 2959060.310547            5 
            2          4096           MPI          2 2914220.861328            5 
            2          4096           MPI          3 2847451.976562            0 
            2          4096           MPI          4 2912423.041016            1 
# Runtime: 23.946765 s (overhead: 0.000058 %) 5 records
