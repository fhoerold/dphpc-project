Invocation: ../cholesky/benchmark.sh -o /cluster/home/forstesa/repos/dphpc-project/benchmark --cachegrind --mpi -d 5 7 9 11 -p 32 ../cholesky/build/cholesky_mpi_ll_improved
Benchmark 'cholesky_mpi_ll_improved' for
> Dimensions:		32, 128, 512, 2048, 
> Number of processors:	32, 
> CPU Model:		XeonGold_6150
> Use MPI:		true
> Use OpenMP:		false

Timestamp: 2022-01-10_12-12-27
