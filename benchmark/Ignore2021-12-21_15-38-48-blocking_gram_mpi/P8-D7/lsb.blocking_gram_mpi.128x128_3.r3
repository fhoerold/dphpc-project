# Sysname : Linux
# Nodename: eu-a6-009-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:40:06 2021
# Execution time and date (local): Tue Dec 21 15:40:06 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 583.851562            0 
            3           128           128   gramschmidt           MPI          1 559.056641            3 
            3           128           128   gramschmidt           MPI          2 545.531250            0 
            3           128           128   gramschmidt           MPI          3 540.474609            0 
            3           128           128   gramschmidt           MPI          4 565.623047            0 
            3           128           128   gramschmidt           MPI          5 566.259766            0 
            3           128           128   gramschmidt           MPI          6 530.667969            0 
            3           128           128   gramschmidt           MPI          7 513.283203            0 
            3           128           128   gramschmidt           MPI          8 500.994141            0 
            3           128           128   gramschmidt           MPI          9 505.320312            0 
# Runtime: 9.022274 s (overhead: 0.000033 %) 10 records
