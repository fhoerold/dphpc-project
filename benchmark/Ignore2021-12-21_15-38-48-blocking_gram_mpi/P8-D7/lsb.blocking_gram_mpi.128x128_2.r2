# Sysname : Linux
# Nodename: eu-a6-009-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:40:06 2021
# Execution time and date (local): Tue Dec 21 15:40:06 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 483.033203            0 
            2           128           128   gramschmidt           MPI          1 443.498047            1 
            2           128           128   gramschmidt           MPI          2 432.058594            0 
            2           128           128   gramschmidt           MPI          3 422.488281            0 
            2           128           128   gramschmidt           MPI          4 450.376953            0 
            2           128           128   gramschmidt           MPI          5 451.671875            0 
            2           128           128   gramschmidt           MPI          6 405.646484            0 
            2           128           128   gramschmidt           MPI          7 398.685547            0 
            2           128           128   gramschmidt           MPI          8 383.341797            0 
            2           128           128   gramschmidt           MPI          9 392.697266            0 
# Runtime: 0.027846 s (overhead: 0.003591 %) 10 records
