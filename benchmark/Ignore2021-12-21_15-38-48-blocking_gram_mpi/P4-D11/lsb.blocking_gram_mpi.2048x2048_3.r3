# Sysname : Linux
# Nodename: eu-a6-007-01
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:39:57 2021
# Execution time and date (local): Tue Dec 21 15:39:57 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 41095154.262695            0 
            3          2048          2048   gramschmidt           MPI          1 41367290.497070            4 
            3          2048          2048   gramschmidt           MPI          2 41371757.314453            5 
            3          2048          2048   gramschmidt           MPI          3 41355572.658203            0 
            3          2048          2048   gramschmidt           MPI          4 41577391.686523            6 
            3          2048          2048   gramschmidt           MPI          5 41720584.082031            0 
            3          2048          2048   gramschmidt           MPI          6 41541057.594727            0 
            3          2048          2048   gramschmidt           MPI          7 41521536.654297            0 
            3          2048          2048   gramschmidt           MPI          8 41739918.552734            4 
            3          2048          2048   gramschmidt           MPI          9 41527343.784180            0 
# Runtime: 1118.317246 s (overhead: 0.000002 %) 10 records
