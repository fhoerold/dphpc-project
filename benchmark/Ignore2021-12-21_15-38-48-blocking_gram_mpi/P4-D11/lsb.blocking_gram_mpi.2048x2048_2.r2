# Sysname : Linux
# Nodename: eu-a6-007-01
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:39:57 2021
# Execution time and date (local): Tue Dec 21 15:39:57 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 29464177.890625            0 
            2          2048          2048   gramschmidt           MPI          1 29578403.387695            6 
            2          2048          2048   gramschmidt           MPI          2 29736861.739258            5 
            2          2048          2048   gramschmidt           MPI          3 29763393.508789            0 
            2          2048          2048   gramschmidt           MPI          4 30015418.973633            5 
            2          2048          2048   gramschmidt           MPI          5 30064021.872070            0 
            2          2048          2048   gramschmidt           MPI          6 29894455.028320            0 
            2          2048          2048   gramschmidt           MPI          7 29767112.689453            0 
            2          2048          2048   gramschmidt           MPI          8 29763715.793945            6 
            2          2048          2048   gramschmidt           MPI          9 29476101.700195            0 
# Runtime: 1106.278055 s (overhead: 0.000002 %) 10 records
