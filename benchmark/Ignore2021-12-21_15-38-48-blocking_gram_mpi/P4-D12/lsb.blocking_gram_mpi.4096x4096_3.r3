# Sysname : Linux
# Nodename: eu-a6-007-01
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:39:57 2021
# Execution time and date (local): Tue Dec 21 15:39:57 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 497313469.905273            0 
            3          4096          4096   gramschmidt           MPI          1 502045693.928711            5 
            3          4096          4096   gramschmidt           MPI          2 402231570.881836            6 
            3          4096          4096   gramschmidt           MPI          3 421725590.591797            0 
            3          4096          4096   gramschmidt           MPI          4 434273935.659180            4 
            3          4096          4096   gramschmidt           MPI          5 422113887.694336            0 
            3          4096          4096   gramschmidt           MPI          6 422455184.491211            0 
            3          4096          4096   gramschmidt           MPI          7 424937835.004883            0 
            3          4096          4096   gramschmidt           MPI          8 409548357.684570            4 
            3          4096          4096   gramschmidt           MPI          9 422037565.222656            0 
# Runtime: 11587.532001 s (overhead: 0.000000 %) 10 records
