# Sysname : Linux
# Nodename: eu-a6-007-01
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:39:57 2021
# Execution time and date (local): Tue Dec 21 15:39:57 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 355687965.277344            1 
            2          4096          4096   gramschmidt           MPI          1 357777808.185547            6 
            2          4096          4096   gramschmidt           MPI          2 286323252.697266            5 
            2          4096          4096   gramschmidt           MPI          3 302960980.423828            0 
            2          4096          4096   gramschmidt           MPI          4 311864631.161133            7 
            2          4096          4096   gramschmidt           MPI          5 300367208.295898            0 
            2          4096          4096   gramschmidt           MPI          6 304265698.541016            1 
            2          4096          4096   gramschmidt           MPI          7 309902438.734375            0 
            2          4096          4096   gramschmidt           MPI          8 289770144.657227            5 
            2          4096          4096   gramschmidt           MPI          9 304355169.545898            0 
# Runtime: 11447.247141 s (overhead: 0.000000 %) 10 records
