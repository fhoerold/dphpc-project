# Sysname : Linux
# Nodename: eu-a6-006-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:39:27 2021
# Execution time and date (local): Tue Dec 21 15:39:27 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 1892.716797            0 
            3           128           128   gramschmidt           MPI          1 1778.093750            0 
            3           128           128   gramschmidt           MPI          2 1748.519531            0 
            3           128           128   gramschmidt           MPI          3 1755.080078            0 
            3           128           128   gramschmidt           MPI          4 1743.990234            0 
            3           128           128   gramschmidt           MPI          5 1797.406250            0 
            3           128           128   gramschmidt           MPI          6 1886.804688            0 
            3           128           128   gramschmidt           MPI          7 1723.578125            0 
            3           128           128   gramschmidt           MPI          8 1737.167969            0 
            3           128           128   gramschmidt           MPI          9 1763.785156            0 
# Runtime: 0.038827 s (overhead: 0.000000 %) 10 records
