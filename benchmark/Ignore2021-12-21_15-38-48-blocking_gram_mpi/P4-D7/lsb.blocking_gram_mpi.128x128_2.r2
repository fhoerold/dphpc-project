# Sysname : Linux
# Nodename: eu-a6-006-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:39:27 2021
# Execution time and date (local): Tue Dec 21 15:39:27 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 1396.689453            3 
            2           128           128   gramschmidt           MPI          1 1285.785156            3 
            2           128           128   gramschmidt           MPI          2 1276.789062            0 
            2           128           128   gramschmidt           MPI          3 1262.369141            0 
            2           128           128   gramschmidt           MPI          4 1278.865234            0 
            2           128           128   gramschmidt           MPI          5 1327.771484            0 
            2           128           128   gramschmidt           MPI          6 1371.375000            0 
            2           128           128   gramschmidt           MPI          7 1272.648438            0 
            2           128           128   gramschmidt           MPI          8 1268.707031            0 
            2           128           128   gramschmidt           MPI          9 1293.380859            0 
# Runtime: 3.050839 s (overhead: 0.000197 %) 10 records
