# Sysname : Linux
# Nodename: eu-a6-006-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:40:27 2021
# Execution time and date (local): Tue Dec 21 15:40:27 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 219.501953            0 
            3            64            64   gramschmidt           MPI          1 215.250000            3 
            3            64            64   gramschmidt           MPI          2 54.810547            0 
            3            64            64   gramschmidt           MPI          3 119.125000            0 
            3            64            64   gramschmidt           MPI          4 113.974609            0 
            3            64            64   gramschmidt           MPI          5 125.816406            0 
            3            64            64   gramschmidt           MPI          6 113.078125            0 
            3            64            64   gramschmidt           MPI          7 103.994141            0 
            3            64            64   gramschmidt           MPI          8 107.773438            2 
            3            64            64   gramschmidt           MPI          9 104.429688            0 
# Runtime: 6.989451 s (overhead: 0.000072 %) 10 records
