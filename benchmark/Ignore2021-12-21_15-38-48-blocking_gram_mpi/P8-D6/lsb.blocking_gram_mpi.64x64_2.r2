# Sysname : Linux
# Nodename: eu-a6-006-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:40:27 2021
# Execution time and date (local): Tue Dec 21 15:40:27 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 60.837891            0 
            2            64            64   gramschmidt           MPI          1 411.777344            3 
            2            64            64   gramschmidt           MPI          2 111.185547            0 
            2            64            64   gramschmidt           MPI          3 102.015625            0 
            2            64            64   gramschmidt           MPI          4 99.074219            0 
            2            64            64   gramschmidt           MPI          5 109.919922            0 
            2            64            64   gramschmidt           MPI          6 97.201172            0 
            2            64            64   gramschmidt           MPI          7 88.726562            0 
            2            64            64   gramschmidt           MPI          8 93.009766            0 
            2            64            64   gramschmidt           MPI          9 90.292969            0 
# Runtime: 8.005238 s (overhead: 0.000037 %) 10 records
