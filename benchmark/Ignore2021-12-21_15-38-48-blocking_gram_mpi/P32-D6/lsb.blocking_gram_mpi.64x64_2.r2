# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 02:58:35 2021
# Execution time and date (local): Wed Dec 22 03:58:35 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 583.023438            0 
            2            64            64   gramschmidt           MPI          1 218.033203            3 
            2            64            64   gramschmidt           MPI          2 193.478516            0 
            2            64            64   gramschmidt           MPI          3 189.535156            0 
            2            64            64   gramschmidt           MPI          4 182.255859            0 
            2            64            64   gramschmidt           MPI          5 181.720703            0 
            2            64            64   gramschmidt           MPI          6 186.847656            0 
            2            64            64   gramschmidt           MPI          7 152.048828            0 
            2            64            64   gramschmidt           MPI          8 170.974609            0 
            2            64            64   gramschmidt           MPI          9 163.251953            0 
# Runtime: 10.006664 s (overhead: 0.000030 %) 10 records
