# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 02:58:35 2021
# Execution time and date (local): Wed Dec 22 03:58:35 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 273.859375            0 
            3            64            64   gramschmidt           MPI          1 219.335938            3 
            3            64            64   gramschmidt           MPI          2 191.013672            0 
            3            64            64   gramschmidt           MPI          3 189.351562            0 
            3            64            64   gramschmidt           MPI          4 178.837891            0 
            3            64            64   gramschmidt           MPI          5 178.835938            0 
            3            64            64   gramschmidt           MPI          6 191.808594            0 
            3            64            64   gramschmidt           MPI          7 159.560547            0 
            3            64            64   gramschmidt           MPI          8 166.740234            0 
            3            64            64   gramschmidt           MPI          9 166.951172            0 
# Runtime: 10.024731 s (overhead: 0.000030 %) 10 records
