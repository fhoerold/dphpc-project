# Sysname : Linux
# Nodename: eu-a6-009-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:39:58 2021
# Execution time and date (local): Tue Dec 21 15:39:58 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 87.417969            0 
            3            32            32   gramschmidt           MPI          1 36.310547            3 
            3            32            32   gramschmidt           MPI          2 47.421875            0 
            3            32            32   gramschmidt           MPI          3 48.248047            0 
            3            32            32   gramschmidt           MPI          4 40.796875            0 
            3            32            32   gramschmidt           MPI          5 42.562500            0 
            3            32            32   gramschmidt           MPI          6 42.648438            0 
            3            32            32   gramschmidt           MPI          7 40.232422            0 
            3            32            32   gramschmidt           MPI          8 40.457031            0 
            3            32            32   gramschmidt           MPI          9 35.968750            0 
# Runtime: 1.005356 s (overhead: 0.000298 %) 10 records
