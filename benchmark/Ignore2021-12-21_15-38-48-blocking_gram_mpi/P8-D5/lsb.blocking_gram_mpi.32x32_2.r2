# Sysname : Linux
# Nodename: eu-a6-009-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:39:58 2021
# Execution time and date (local): Tue Dec 21 15:39:58 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 100.892578            0 
            2            32            32   gramschmidt           MPI          1 67.115234            3 
            2            32            32   gramschmidt           MPI          2 43.654297            0 
            2            32            32   gramschmidt           MPI          3 47.644531            0 
            2            32            32   gramschmidt           MPI          4 38.705078            0 
            2            32            32   gramschmidt           MPI          5 39.925781            0 
            2            32            32   gramschmidt           MPI          6 39.607422            0 
            2            32            32   gramschmidt           MPI          7 39.585938            0 
            2            32            32   gramschmidt           MPI          8 36.509766            0 
            2            32            32   gramschmidt           MPI          9 33.769531            0 
# Runtime: 1.003239 s (overhead: 0.000299 %) 10 records
