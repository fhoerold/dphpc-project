# Sysname : Linux
# Nodename: eu-a6-010-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:24:46 2021
# Execution time and date (local): Tue Dec 21 16:24:46 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 75809.310547            0 
            3          1024          1024   gramschmidt           MPI          1 75168.339844            4 
            3          1024          1024   gramschmidt           MPI          2 76305.662109            4 
            3          1024          1024   gramschmidt           MPI          3 75042.433594            0 
            3          1024          1024   gramschmidt           MPI          4 74638.755859            1 
            3          1024          1024   gramschmidt           MPI          5 74427.486328            0 
            3          1024          1024   gramschmidt           MPI          6 75669.072266            0 
            3          1024          1024   gramschmidt           MPI          7 75229.488281            0 
            3          1024          1024   gramschmidt           MPI          8 76363.494141            5 
            3          1024          1024   gramschmidt           MPI          9 73571.414062            0 
# Runtime: 74.753237 s (overhead: 0.000019 %) 10 records
