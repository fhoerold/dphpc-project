# Sysname : Linux
# Nodename: eu-a6-010-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:24:46 2021
# Execution time and date (local): Tue Dec 21 16:24:46 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 57537.914062            0 
            2          1024          1024   gramschmidt           MPI          1 56235.687500            4 
            2          1024          1024   gramschmidt           MPI          2 57409.324219            3 
            2          1024          1024   gramschmidt           MPI          3 56269.382812            0 
            2          1024          1024   gramschmidt           MPI          4 56081.677734            1 
            2          1024          1024   gramschmidt           MPI          5 55838.023438            0 
            2          1024          1024   gramschmidt           MPI          6 57175.820312            0 
            2          1024          1024   gramschmidt           MPI          7 56584.507812            0 
            2          1024          1024   gramschmidt           MPI          8 57818.853516            5 
            2          1024          1024   gramschmidt           MPI          9 55752.171875            0 
# Runtime: 72.790299 s (overhead: 0.000018 %) 10 records
