# Sysname : Linux
# Nodename: eu-a6-009-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:41:28 2021
# Execution time and date (local): Tue Dec 21 15:41:28 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 7063654.773438            0 
            3          2048          2048   gramschmidt           MPI          1 7095723.541016            4 
            3          2048          2048   gramschmidt           MPI          2 7007798.666016            5 
            3          2048          2048   gramschmidt           MPI          3 6962171.181641            0 
            3          2048          2048   gramschmidt           MPI          4 6952067.751953            8 
            3          2048          2048   gramschmidt           MPI          5 7005545.169922            0 
            3          2048          2048   gramschmidt           MPI          6 7229027.222656            2 
            3          2048          2048   gramschmidt           MPI          7 7208583.585938            2 
            3          2048          2048   gramschmidt           MPI          8 7213630.462891            7 
            3          2048          2048   gramschmidt           MPI          9 6881372.693359            2 
# Runtime: 824.269855 s (overhead: 0.000004 %) 10 records
