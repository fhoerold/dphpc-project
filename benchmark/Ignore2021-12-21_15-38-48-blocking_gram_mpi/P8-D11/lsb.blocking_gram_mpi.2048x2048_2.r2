# Sysname : Linux
# Nodename: eu-a6-009-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:41:28 2021
# Execution time and date (local): Tue Dec 21 15:41:28 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 5110512.169922            0 
            2          2048          2048   gramschmidt           MPI          1 5079640.632812            4 
            2          2048          2048   gramschmidt           MPI          2 5018952.583984            4 
            2          2048          2048   gramschmidt           MPI          3 4987474.609375            0 
            2          2048          2048   gramschmidt           MPI          4 5034472.916016            4 
            2          2048          2048   gramschmidt           MPI          5 5054165.066406            0 
            2          2048          2048   gramschmidt           MPI          6 5229822.152344            0 
            2          2048          2048   gramschmidt           MPI          7 5212621.529297            0 
            2          2048          2048   gramschmidt           MPI          8 5190510.910156            5 
            2          2048          2048   gramschmidt           MPI          9 5032755.238281            0 
# Runtime: 822.466675 s (overhead: 0.000002 %) 10 records
