# Sysname : Linux
# Nodename: eu-a6-010-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:40:57 2021
# Execution time and date (local): Tue Dec 21 15:40:57 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 326714.892578            0 
            2          1024          1024   gramschmidt           MPI          1 327511.013672            5 
            2          1024          1024   gramschmidt           MPI          2 317696.906250            2 
            2          1024          1024   gramschmidt           MPI          3 338629.376953            0 
            2          1024          1024   gramschmidt           MPI          4 303936.279297            2 
            2          1024          1024   gramschmidt           MPI          5 310508.210938            0 
            2          1024          1024   gramschmidt           MPI          6 314684.406250            0 
            2          1024          1024   gramschmidt           MPI          7 314076.488281            0 
            2          1024          1024   gramschmidt           MPI          8 317470.003906            5 
            2          1024          1024   gramschmidt           MPI          9 317122.152344            0 
# Runtime: 86.986771 s (overhead: 0.000016 %) 10 records
