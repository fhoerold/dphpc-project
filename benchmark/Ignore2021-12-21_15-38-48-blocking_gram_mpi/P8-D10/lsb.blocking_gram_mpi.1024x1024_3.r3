# Sysname : Linux
# Nodename: eu-a6-010-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:40:57 2021
# Execution time and date (local): Tue Dec 21 15:40:57 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 458191.210938            0 
            3          1024          1024   gramschmidt           MPI          1 460106.058594            6 
            3          1024          1024   gramschmidt           MPI          2 450187.257812            5 
            3          1024          1024   gramschmidt           MPI          3 469198.029297            0 
            3          1024          1024   gramschmidt           MPI          4 435115.972656            8 
            3          1024          1024   gramschmidt           MPI          5 442660.648438            0 
            3          1024          1024   gramschmidt           MPI          6 444887.259766            0 
            3          1024          1024   gramschmidt           MPI          7 446016.781250            0 
            3          1024          1024   gramschmidt           MPI          8 448470.527344            2 
            3          1024          1024   gramschmidt           MPI          9 445656.613281            0 
# Runtime: 88.109376 s (overhead: 0.000024 %) 10 records
