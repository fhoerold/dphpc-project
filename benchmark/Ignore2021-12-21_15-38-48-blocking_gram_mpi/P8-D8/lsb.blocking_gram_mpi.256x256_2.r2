# Sysname : Linux
# Nodename: eu-a6-006-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:40:42 2021
# Execution time and date (local): Tue Dec 21 15:40:42 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 3245.279297            0 
            2           256           256   gramschmidt           MPI          1 3081.423828            5 
            2           256           256   gramschmidt           MPI          2 3256.425781            0 
            2           256           256   gramschmidt           MPI          3 2965.703125            0 
            2           256           256   gramschmidt           MPI          4 2987.500000            0 
            2           256           256   gramschmidt           MPI          5 2953.976562            0 
            2           256           256   gramschmidt           MPI          6 3002.343750            0 
            2           256           256   gramschmidt           MPI          7 2952.333984            0 
            2           256           256   gramschmidt           MPI          8 3127.355469            1 
            2           256           256   gramschmidt           MPI          9 3135.425781            0 
# Runtime: 6.297855 s (overhead: 0.000095 %) 10 records
