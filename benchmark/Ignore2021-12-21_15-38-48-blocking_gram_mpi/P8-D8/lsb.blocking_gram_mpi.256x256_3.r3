# Sysname : Linux
# Nodename: eu-a6-006-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:40:42 2021
# Execution time and date (local): Tue Dec 21 15:40:42 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 4296.820312            0 
            3           256           256   gramschmidt           MPI          1 4133.937500            2 
            3           256           256   gramschmidt           MPI          2 4338.666016            0 
            3           256           256   gramschmidt           MPI          3 4024.648438            0 
            3           256           256   gramschmidt           MPI          4 4033.617188            0 
            3           256           256   gramschmidt           MPI          5 4054.130859            0 
            3           256           256   gramschmidt           MPI          6 4105.115234            0 
            3           256           256   gramschmidt           MPI          7 4042.000000            0 
            3           256           256   gramschmidt           MPI          8 4249.732422            0 
            3           256           256   gramschmidt           MPI          9 4232.525391            0 
# Runtime: 7.281241 s (overhead: 0.000027 %) 10 records
