# Sysname : Linux
# Nodename: eu-a6-010-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:10:46 2021
# Execution time and date (local): Tue Dec 21 16:10:46 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 69.066406            1 
            3            64            64   gramschmidt           MPI          1 124.033203            2 
            3            64            64   gramschmidt           MPI          2 85.449219            0 
            3            64            64   gramschmidt           MPI          3 108.326172            0 
            3            64            64   gramschmidt           MPI          4 115.716797            0 
            3            64            64   gramschmidt           MPI          5 93.462891            0 
            3            64            64   gramschmidt           MPI          6 99.033203            0 
            3            64            64   gramschmidt           MPI          7 90.978516            0 
            3            64            64   gramschmidt           MPI          8 96.476562            0 
            3            64            64   gramschmidt           MPI          9 76.484375            0 
# Runtime: 10.006097 s (overhead: 0.000030 %) 10 records
