# Sysname : Linux
# Nodename: eu-a6-010-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:10:46 2021
# Execution time and date (local): Tue Dec 21 16:10:46 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 89.021484            0 
            2            64            64   gramschmidt           MPI          1 115.562500            2 
            2            64            64   gramschmidt           MPI          2 263.681641            1 
            2            64            64   gramschmidt           MPI          3 99.611328            0 
            2            64            64   gramschmidt           MPI          4 107.710938            0 
            2            64            64   gramschmidt           MPI          5 81.423828            0 
            2            64            64   gramschmidt           MPI          6 93.894531            0 
            2            64            64   gramschmidt           MPI          7 86.431641            0 
            2            64            64   gramschmidt           MPI          8 93.496094            0 
            2            64            64   gramschmidt           MPI          9 71.306641            0 
# Runtime: 6.008714 s (overhead: 0.000050 %) 10 records
