# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 02:44:31 2021
# Execution time and date (local): Wed Dec 22 03:44:31 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 1158.658203            0 
            3           256           256   gramschmidt           MPI          1 1019.716797            3 
            3           256           256   gramschmidt           MPI          2 1000.775391            0 
            3           256           256   gramschmidt           MPI          3 980.306641            0 
            3           256           256   gramschmidt           MPI          4 946.865234            0 
            3           256           256   gramschmidt           MPI          5 887.644531            0 
            3           256           256   gramschmidt           MPI          6 945.667969            0 
            3           256           256   gramschmidt           MPI          7 918.007812            0 
            3           256           256   gramschmidt           MPI          8 858.021484            0 
            3           256           256   gramschmidt           MPI          9 878.535156            0 
# Runtime: 19.160722 s (overhead: 0.000016 %) 10 records
