# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 02:44:31 2021
# Execution time and date (local): Wed Dec 22 03:44:31 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 1080.914062            1 
            2           256           256   gramschmidt           MPI          1 962.613281            3 
            2           256           256   gramschmidt           MPI          2 921.646484            0 
            2           256           256   gramschmidt           MPI          3 915.593750            0 
            2           256           256   gramschmidt           MPI          4 877.960938            0 
            2           256           256   gramschmidt           MPI          5 812.597656            0 
            2           256           256   gramschmidt           MPI          6 853.648438            0 
            2           256           256   gramschmidt           MPI          7 848.013672            0 
            2           256           256   gramschmidt           MPI          8 783.791016            0 
            2           256           256   gramschmidt           MPI          9 797.619141            0 
# Runtime: 18.182780 s (overhead: 0.000022 %) 10 records
