# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 02:58:55 2021
# Execution time and date (local): Wed Dec 22 03:58:55 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 401.216797            0 
            3           128           128   gramschmidt           MPI          1 351.419922            3 
            3           128           128   gramschmidt           MPI          2 348.203125            0 
            3           128           128   gramschmidt           MPI          3 336.929688            0 
            3           128           128   gramschmidt           MPI          4 306.154297            0 
            3           128           128   gramschmidt           MPI          5 327.335938            0 
            3           128           128   gramschmidt           MPI          6 322.750000            0 
            3           128           128   gramschmidt           MPI          7 304.701172            0 
            3           128           128   gramschmidt           MPI          8 308.933594            0 
            3           128           128   gramschmidt           MPI          9 295.935547            0 
# Runtime: 9.011791 s (overhead: 0.000033 %) 10 records
