# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 02:58:55 2021
# Execution time and date (local): Wed Dec 22 03:58:55 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 393.150391            0 
            2           128           128   gramschmidt           MPI          1 327.640625            4 
            2           128           128   gramschmidt           MPI          2 329.714844            0 
            2           128           128   gramschmidt           MPI          3 323.677734            0 
            2           128           128   gramschmidt           MPI          4 291.976562            0 
            2           128           128   gramschmidt           MPI          5 316.042969            0 
            2           128           128   gramschmidt           MPI          6 309.509766            0 
            2           128           128   gramschmidt           MPI          7 290.708984            0 
            2           128           128   gramschmidt           MPI          8 294.619141            0 
            2           128           128   gramschmidt           MPI          9 281.917969            0 
# Runtime: 9.049547 s (overhead: 0.000044 %) 10 records
