# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 02:59:12 2021
# Execution time and date (local): Wed Dec 22 03:59:12 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 23495.654297            0 
            3          1024          1024   gramschmidt           MPI          1 23548.179688            4 
            3          1024          1024   gramschmidt           MPI          2 23024.490234            0 
            3          1024          1024   gramschmidt           MPI          3 23210.025391            0 
            3          1024          1024   gramschmidt           MPI          4 22348.603516            1 
            3          1024          1024   gramschmidt           MPI          5 23233.312500            0 
            3          1024          1024   gramschmidt           MPI          6 23293.111328            0 
            3          1024          1024   gramschmidt           MPI          7 22517.896484            0 
            3          1024          1024   gramschmidt           MPI          8 23385.714844            5 
            3          1024          1024   gramschmidt           MPI          9 22589.617188            0 
# Runtime: 79.494747 s (overhead: 0.000013 %) 10 records
