# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 02:59:12 2021
# Execution time and date (local): Wed Dec 22 03:59:12 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 18971.916016            0 
            2          1024          1024   gramschmidt           MPI          1 19090.644531            4 
            2          1024          1024   gramschmidt           MPI          2 18558.701172            2 
            2          1024          1024   gramschmidt           MPI          3 18711.744141            0 
            2          1024          1024   gramschmidt           MPI          4 17683.289062            1 
            2          1024          1024   gramschmidt           MPI          5 18695.675781            0 
            2          1024          1024   gramschmidt           MPI          6 18682.248047            0 
            2          1024          1024   gramschmidt           MPI          7 17976.964844            0 
            2          1024          1024   gramschmidt           MPI          8 18827.474609            5 
            2          1024          1024   gramschmidt           MPI          9 18127.724609            0 
# Runtime: 79.488267 s (overhead: 0.000015 %) 10 records
