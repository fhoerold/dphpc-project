# Sysname : Linux
# Nodename: eu-a6-006-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:39:27 2021
# Execution time and date (local): Tue Dec 21 15:39:27 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 249.068359            0 
            3            64            64   gramschmidt           MPI          1 232.548828            2 
            3            64            64   gramschmidt           MPI          2 222.869141            0 
            3            64            64   gramschmidt           MPI          3 222.353516            0 
            3            64            64   gramschmidt           MPI          4 227.324219            0 
            3            64            64   gramschmidt           MPI          5 218.578125            0 
            3            64            64   gramschmidt           MPI          6 212.623047            0 
            3            64            64   gramschmidt           MPI          7 205.884766            0 
            3            64            64   gramschmidt           MPI          8 215.375000            0 
            3            64            64   gramschmidt           MPI          9 196.982422            0 
# Runtime: 4.011141 s (overhead: 0.000050 %) 10 records
