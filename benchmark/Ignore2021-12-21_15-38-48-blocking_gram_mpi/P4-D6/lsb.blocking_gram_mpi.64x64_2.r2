# Sysname : Linux
# Nodename: eu-a6-006-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:39:27 2021
# Execution time and date (local): Tue Dec 21 15:39:27 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 199.117188            1 
            2            64            64   gramschmidt           MPI          1 181.623047            2 
            2            64            64   gramschmidt           MPI          2 173.496094            0 
            2            64            64   gramschmidt           MPI          3 173.335938            0 
            2            64            64   gramschmidt           MPI          4 178.699219            0 
            2            64            64   gramschmidt           MPI          5 171.671875            0 
            2            64            64   gramschmidt           MPI          6 164.294922            0 
            2            64            64   gramschmidt           MPI          7 157.740234            0 
            2            64            64   gramschmidt           MPI          8 167.158203            0 
            2            64            64   gramschmidt           MPI          9 149.181641            0 
# Runtime: 4.012394 s (overhead: 0.000075 %) 10 records
