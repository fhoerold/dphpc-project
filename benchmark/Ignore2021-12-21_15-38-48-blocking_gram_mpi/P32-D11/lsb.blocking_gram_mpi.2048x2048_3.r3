# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 02:45:24 2021
# Execution time and date (local): Wed Dec 22 03:45:24 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 278025.550781            0 
            3          2048          2048   gramschmidt           MPI          1 275037.882812            5 
            3          2048          2048   gramschmidt           MPI          2 279506.300781            5 
            3          2048          2048   gramschmidt           MPI          3 275722.673828            0 
            3          2048          2048   gramschmidt           MPI          4 282297.996094            6 
            3          2048          2048   gramschmidt           MPI          5 277539.964844            0 
            3          2048          2048   gramschmidt           MPI          6 285545.660156            0 
            3          2048          2048   gramschmidt           MPI          7 281039.824219            0 
            3          2048          2048   gramschmidt           MPI          8 290182.791016            5 
            3          2048          2048   gramschmidt           MPI          9 275703.771484            0 
# Runtime: 719.867600 s (overhead: 0.000003 %) 10 records
