# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 02:45:24 2021
# Execution time and date (local): Wed Dec 22 03:45:24 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 207433.859375            0 
            2          2048          2048   gramschmidt           MPI          1 204360.132812            5 
            2          2048          2048   gramschmidt           MPI          2 207467.927734            5 
            2          2048          2048   gramschmidt           MPI          3 206280.281250            0 
            2          2048          2048   gramschmidt           MPI          4 211154.353516            5 
            2          2048          2048   gramschmidt           MPI          5 208263.132812            0 
            2          2048          2048   gramschmidt           MPI          6 212611.923828            0 
            2          2048          2048   gramschmidt           MPI          7 210643.572266            0 
            2          2048          2048   gramschmidt           MPI          8 216971.490234            6 
            2          2048          2048   gramschmidt           MPI          9 207512.978516            0 
# Runtime: 720.811220 s (overhead: 0.000003 %) 10 records
