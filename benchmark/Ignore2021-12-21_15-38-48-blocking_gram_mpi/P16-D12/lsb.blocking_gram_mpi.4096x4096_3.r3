# Sysname : Linux
# Nodename: eu-a6-010-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:26:31 2021
# Execution time and date (local): Tue Dec 21 16:26:31 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 18003358.353516            0 
            3          4096          4096   gramschmidt           MPI          1 18062414.613281            7 
            3          4096          4096   gramschmidt           MPI          2 18051277.898438            7 
            3          4096          4096   gramschmidt           MPI          3 18132827.173828            0 
            3          4096          4096   gramschmidt           MPI          4 18006645.941406            7 
            3          4096          4096   gramschmidt           MPI          5 17947787.371094            1 
            3          4096          4096   gramschmidt           MPI          6 18106800.085938            1 
            3          4096          4096   gramschmidt           MPI          7 18083260.304688            0 
            3          4096          4096   gramschmidt           MPI          8 18109378.064453            7 
            3          4096          4096   gramschmidt           MPI          9 17988055.228516            1 
# Runtime: 7161.582289 s (overhead: 0.000000 %) 10 records
