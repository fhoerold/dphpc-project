# Sysname : Linux
# Nodename: eu-a6-010-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:26:31 2021
# Execution time and date (local): Tue Dec 21 16:26:31 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 12879522.785156            0 
            2          4096          4096   gramschmidt           MPI          1 12983469.289062            8 
            2          4096          4096   gramschmidt           MPI          2 12958679.925781            7 
            2          4096          4096   gramschmidt           MPI          3 13013197.851562            2 
            2          4096          4096   gramschmidt           MPI          4 12938875.060547            8 
            2          4096          4096   gramschmidt           MPI          5 12872114.308594            0 
            2          4096          4096   gramschmidt           MPI          6 12947415.294922            0 
            2          4096          4096   gramschmidt           MPI          7 13045703.554688            0 
            2          4096          4096   gramschmidt           MPI          8 12970876.369141            9 
            2          4096          4096   gramschmidt           MPI          9 13064405.005859            0 
# Runtime: 7157.435186 s (overhead: 0.000000 %) 10 records
