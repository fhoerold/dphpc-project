# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 02:44:06 2021
# Execution time and date (local): Wed Dec 22 03:44:06 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 223.710938            0 
            2            32            32   gramschmidt           MPI          1 154.613281            3 
            2            32            32   gramschmidt           MPI          2 372.666016            0 
            2            32            32   gramschmidt           MPI          3 155.818359            0 
            2            32            32   gramschmidt           MPI          4 137.962891            0 
            2            32            32   gramschmidt           MPI          5 134.294922            0 
            2            32            32   gramschmidt           MPI          6 138.525391            0 
            2            32            32   gramschmidt           MPI          7 134.962891            0 
            2            32            32   gramschmidt           MPI          8 134.824219            0 
            2            32            32   gramschmidt           MPI          9 158.896484            0 
# Runtime: 15.995253 s (overhead: 0.000019 %) 10 records
