# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 02:44:06 2021
# Execution time and date (local): Wed Dec 22 03:44:06 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 183.375000            0 
            3            32            32   gramschmidt           MPI          1 138.212891            3 
            3            32            32   gramschmidt           MPI          2 69.767578            0 
            3            32            32   gramschmidt           MPI          3 151.490234            0 
            3            32            32   gramschmidt           MPI          4 140.662109            0 
            3            32            32   gramschmidt           MPI          5 130.636719            0 
            3            32            32   gramschmidt           MPI          6 132.636719            0 
            3            32            32   gramschmidt           MPI          7 125.796875            0 
            3            32            32   gramschmidt           MPI          8 125.513672            0 
            3            32            32   gramschmidt           MPI          9 155.207031            0 
# Runtime: 15.978343 s (overhead: 0.000019 %) 10 records
