# Sysname : Linux
# Nodename: eu-a6-010-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:11:27 2021
# Execution time and date (local): Tue Dec 21 16:11:27 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 9004.140625            0 
            3           512           512   gramschmidt           MPI          1 8890.767578            3 
            3           512           512   gramschmidt           MPI          2 8756.162109            0 
            3           512           512   gramschmidt           MPI          3 8639.912109            0 
            3           512           512   gramschmidt           MPI          4 8877.910156            0 
            3           512           512   gramschmidt           MPI          5 8658.453125            0 
            3           512           512   gramschmidt           MPI          6 8664.490234            0 
            3           512           512   gramschmidt           MPI          7 8610.140625            0 
            3           512           512   gramschmidt           MPI          8 8653.744141            3 
            3           512           512   gramschmidt           MPI          9 8530.750000            0 
# Runtime: 9.708971 s (overhead: 0.000062 %) 10 records
