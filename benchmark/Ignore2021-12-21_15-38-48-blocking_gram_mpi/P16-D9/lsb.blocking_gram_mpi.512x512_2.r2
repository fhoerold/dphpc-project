# Sysname : Linux
# Nodename: eu-a6-010-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:11:27 2021
# Execution time and date (local): Tue Dec 21 16:11:27 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 6833.294922            0 
            2           512           512   gramschmidt           MPI          1 6627.171875            3 
            2           512           512   gramschmidt           MPI          2 6498.041016            4 
            2           512           512   gramschmidt           MPI          3 6395.789062            0 
            2           512           512   gramschmidt           MPI          4 6649.384766            1 
            2           512           512   gramschmidt           MPI          5 6508.570312            0 
            2           512           512   gramschmidt           MPI          6 6457.783203            0 
            2           512           512   gramschmidt           MPI          7 6381.236328            0 
            2           512           512   gramschmidt           MPI          8 6389.892578            1 
            2           512           512   gramschmidt           MPI          9 6343.541016            0 
# Runtime: 11.717672 s (overhead: 0.000077 %) 10 records
