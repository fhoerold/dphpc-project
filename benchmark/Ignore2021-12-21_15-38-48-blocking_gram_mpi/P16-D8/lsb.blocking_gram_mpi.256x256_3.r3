# Sysname : Linux
# Nodename: eu-a6-010-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:11:08 2021
# Execution time and date (local): Tue Dec 21 16:11:08 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 1427.007812            0 
            3           256           256   gramschmidt           MPI          1 1358.355469            3 
            3           256           256   gramschmidt           MPI          2 1313.662109            0 
            3           256           256   gramschmidt           MPI          3 1331.095703            0 
            3           256           256   gramschmidt           MPI          4 1262.953125            0 
            3           256           256   gramschmidt           MPI          5 1257.576172            0 
            3           256           256   gramschmidt           MPI          6 1296.863281            0 
            3           256           256   gramschmidt           MPI          7 1169.943359            0 
            3           256           256   gramschmidt           MPI          8 1199.781250            0 
            3           256           256   gramschmidt           MPI          9 1181.400391            0 
# Runtime: 10.187911 s (overhead: 0.000029 %) 10 records
