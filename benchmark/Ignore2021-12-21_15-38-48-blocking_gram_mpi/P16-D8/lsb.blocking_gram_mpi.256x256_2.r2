# Sysname : Linux
# Nodename: eu-a6-010-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:11:08 2021
# Execution time and date (local): Tue Dec 21 16:11:08 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 1203.425781            0 
            2           256           256   gramschmidt           MPI          1 1104.871094            3 
            2           256           256   gramschmidt           MPI          2 1060.289062            0 
            2           256           256   gramschmidt           MPI          3 1077.492188            0 
            2           256           256   gramschmidt           MPI          4 1001.242188            0 
            2           256           256   gramschmidt           MPI          5 1006.208984            0 
            2           256           256   gramschmidt           MPI          6 1023.013672            0 
            2           256           256   gramschmidt           MPI          7 918.015625            0 
            2           256           256   gramschmidt           MPI          8 947.000000            0 
            2           256           256   gramschmidt           MPI          9 925.761719            0 
# Runtime: 13.200395 s (overhead: 0.000023 %) 10 records
