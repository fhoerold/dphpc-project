# Sysname : Linux
# Nodename: eu-a6-010-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:11:48 2021
# Execution time and date (local): Tue Dec 21 16:11:48 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 1387687.107422            0 
            2          2048          2048   gramschmidt           MPI          1 1391861.601562            6 
            2          2048          2048   gramschmidt           MPI          2 1392838.265625            6 
            2          2048          2048   gramschmidt           MPI          3 1398047.855469            0 
            2          2048          2048   gramschmidt           MPI          4 1401383.496094            6 
            2          2048          2048   gramschmidt           MPI          5 1407880.759766            0 
            2          2048          2048   gramschmidt           MPI          6 1407874.833984            0 
            2          2048          2048   gramschmidt           MPI          7 1401322.898438            0 
            2          2048          2048   gramschmidt           MPI          8 1400604.218750            5 
            2          2048          2048   gramschmidt           MPI          9 1387247.117188            0 
# Runtime: 682.029839 s (overhead: 0.000003 %) 10 records
