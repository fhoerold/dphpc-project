# Sysname : Linux
# Nodename: eu-a6-010-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:11:48 2021
# Execution time and date (local): Tue Dec 21 16:11:48 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 1934657.710938            0 
            3          2048          2048   gramschmidt           MPI          1 1941288.900391            4 
            3          2048          2048   gramschmidt           MPI          2 1942249.072266            5 
            3          2048          2048   gramschmidt           MPI          3 1945368.041016            0 
            3          2048          2048   gramschmidt           MPI          4 1952253.421875            6 
            3          2048          2048   gramschmidt           MPI          5 1957352.535156            0 
            3          2048          2048   gramschmidt           MPI          6 1956712.498047            2 
            3          2048          2048   gramschmidt           MPI          7 1951037.505859            2 
            3          2048          2048   gramschmidt           MPI          8 1949031.945312            7 
            3          2048          2048   gramschmidt           MPI          9 1919815.740234            0 
# Runtime: 687.143212 s (overhead: 0.000004 %) 10 records
