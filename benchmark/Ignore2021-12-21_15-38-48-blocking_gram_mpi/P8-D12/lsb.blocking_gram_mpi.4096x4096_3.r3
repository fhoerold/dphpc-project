# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:42:27 2021
# Execution time and date (local): Tue Dec 21 15:42:27 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 125111775.068359            0 
            3          4096          4096   gramschmidt           MPI          1 122083320.734375            4 
            3          4096          4096   gramschmidt           MPI          2 119896887.044922            5 
            3          4096          4096   gramschmidt           MPI          3 122268478.943359            0 
            3          4096          4096   gramschmidt           MPI          4 120667122.132812            4 
            3          4096          4096   gramschmidt           MPI          5 124506212.197266            0 
            3          4096          4096   gramschmidt           MPI          6 119055702.515625            0 
            3          4096          4096   gramschmidt           MPI          7 123614713.421875            0 
            3          4096          4096   gramschmidt           MPI          8 130686432.789062            6 
            3          4096          4096   gramschmidt           MPI          9 132697202.880859            3 
# Runtime: 9185.619429 s (overhead: 0.000000 %) 10 records
