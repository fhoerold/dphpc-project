# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:42:27 2021
# Execution time and date (local): Tue Dec 21 15:42:27 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 81182561.189453            0 
            2          4096          4096   gramschmidt           MPI          1 78392694.269531            5 
            2          4096          4096   gramschmidt           MPI          2 77206636.082031            5 
            2          4096          4096   gramschmidt           MPI          3 80390383.464844            0 
            2          4096          4096   gramschmidt           MPI          4 77532181.912109            5 
            2          4096          4096   gramschmidt           MPI          5 80997458.500000            0 
            2          4096          4096   gramschmidt           MPI          6 77921491.699219            0 
            2          4096          4096   gramschmidt           MPI          7 79254721.408203            0 
            2          4096          4096   gramschmidt           MPI          8 84933037.972656            5 
            2          4096          4096   gramschmidt           MPI          9 87305634.494141            0 
# Runtime: 9141.118565 s (overhead: 0.000000 %) 10 records
