# Sysname : Linux
# Nodename: eu-a6-003-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 03:01:04 2021
# Execution time and date (local): Wed Dec 22 04:01:04 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 3272644.283203            0 
            2          4096          4096   gramschmidt           MPI          1 3229952.066406            8 
            2          4096          4096   gramschmidt           MPI          2 3329251.580078            9 
            2          4096          4096   gramschmidt           MPI          3 3259184.488281            0 
            2          4096          4096   gramschmidt           MPI          4 3247065.207031            9 
            2          4096          4096   gramschmidt           MPI          5 3209310.562500            0 
            2          4096          4096   gramschmidt           MPI          6 3233079.166016            0 
            2          4096          4096   gramschmidt           MPI          7 3205553.343750            0 
            2          4096          4096   gramschmidt           MPI          8 3251937.740234            8 
            2          4096          4096   gramschmidt           MPI          9 3223200.529297            0 
# Runtime: 8550.105877 s (overhead: 0.000000 %) 10 records
