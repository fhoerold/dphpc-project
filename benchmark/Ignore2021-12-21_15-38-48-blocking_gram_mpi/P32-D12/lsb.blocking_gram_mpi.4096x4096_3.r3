# Sysname : Linux
# Nodename: eu-a6-003-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 03:01:04 2021
# Execution time and date (local): Wed Dec 22 04:01:04 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 4383872.447266            0 
            3          4096          4096   gramschmidt           MPI          1 4347250.849609            9 
            3          4096          4096   gramschmidt           MPI          2 4443844.126953            6 
            3          4096          4096   gramschmidt           MPI          3 4388834.619141            0 
            3          4096          4096   gramschmidt           MPI          4 4377657.863281            7 
            3          4096          4096   gramschmidt           MPI          5 4390121.146484            0 
            3          4096          4096   gramschmidt           MPI          6 4400494.378906            0 
            3          4096          4096   gramschmidt           MPI          7 4319468.529297            0 
            3          4096          4096   gramschmidt           MPI          8 4375971.994141            6 
            3          4096          4096   gramschmidt           MPI          9 4297705.519531            0 
# Runtime: 8553.311613 s (overhead: 0.000000 %) 10 records
