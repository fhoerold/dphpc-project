# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:39:28 2021
# Execution time and date (local): Tue Dec 21 15:39:28 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 44.361328            0 
            2            32            32   gramschmidt           MPI          1 34.285156            3 
            2            32            32   gramschmidt           MPI          2 38.675781            0 
            2            32            32   gramschmidt           MPI          3 45.089844            0 
            2            32            32   gramschmidt           MPI          4 46.460938            0 
            2            32            32   gramschmidt           MPI          5 42.294922            0 
            2            32            32   gramschmidt           MPI          6 39.964844            0 
            2            32            32   gramschmidt           MPI          7 43.994141            0 
            2            32            32   gramschmidt           MPI          8 32.087891            0 
            2            32            32   gramschmidt           MPI          9 34.269531            0 
# Runtime: 7.993764 s (overhead: 0.000038 %) 10 records
