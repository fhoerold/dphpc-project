# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:39:28 2021
# Execution time and date (local): Tue Dec 21 15:39:28 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 54.150391            0 
            3            32            32   gramschmidt           MPI          1 40.710938            0 
            3            32            32   gramschmidt           MPI          2 44.546875            0 
            3            32            32   gramschmidt           MPI          3 55.056641            0 
            3            32            32   gramschmidt           MPI          4 52.837891            0 
            3            32            32   gramschmidt           MPI          5 48.806641            0 
            3            32            32   gramschmidt           MPI          6 46.619141            0 
            3            32            32   gramschmidt           MPI          7 61.359375            0 
            3            32            32   gramschmidt           MPI          8 39.765625            0 
            3            32            32   gramschmidt           MPI          9 41.628906            0 
# Runtime: 0.001540 s (overhead: 0.000000 %) 10 records
