# Sysname : Linux
# Nodename: eu-a6-006-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:39:27 2021
# Execution time and date (local): Tue Dec 21 15:39:27 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 134127.257812            0 
            3           512           512   gramschmidt           MPI          1 144719.312500            5 
            3           512           512   gramschmidt           MPI          2 124749.505859            2 
            3           512           512   gramschmidt           MPI          3 125676.134766            0 
            3           512           512   gramschmidt           MPI          4 125504.085938            1 
            3           512           512   gramschmidt           MPI          5 125068.912109            0 
            3           512           512   gramschmidt           MPI          6 124384.837891            0 
            3           512           512   gramschmidt           MPI          7 124623.607422            0 
            3           512           512   gramschmidt           MPI          8 125286.125000            2 
            3           512           512   gramschmidt           MPI          9 124603.378906            0 
# Runtime: 19.639120 s (overhead: 0.000051 %) 10 records
