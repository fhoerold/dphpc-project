# Sysname : Linux
# Nodename: eu-a6-006-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:39:27 2021
# Execution time and date (local): Tue Dec 21 15:39:27 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 95384.837891            0 
            2           512           512   gramschmidt           MPI          1 93825.232422            5 
            2           512           512   gramschmidt           MPI          2 87797.988281            1 
            2           512           512   gramschmidt           MPI          3 88490.599609            0 
            2           512           512   gramschmidt           MPI          4 90763.199219            1 
            2           512           512   gramschmidt           MPI          5 90258.119141            0 
            2           512           512   gramschmidt           MPI          6 88493.603516            0 
            2           512           512   gramschmidt           MPI          7 89917.400391            0 
            2           512           512   gramschmidt           MPI          8 89580.160156            4 
            2           512           512   gramschmidt           MPI          9 89028.339844            0 
# Runtime: 6.600928 s (overhead: 0.000167 %) 10 records
