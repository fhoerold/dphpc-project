# Sysname : Linux
# Nodename: eu-a6-010-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:10:31 2021
# Execution time and date (local): Tue Dec 21 16:10:31 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 80.460938            0 
            2            32            32   gramschmidt           MPI          1 87.027344            2 
            2            32            32   gramschmidt           MPI          2 329.371094            0 
            2            32            32   gramschmidt           MPI          3 67.435547            0 
            2            32            32   gramschmidt           MPI          4 65.533203            0 
            2            32            32   gramschmidt           MPI          5 57.134766            0 
            2            32            32   gramschmidt           MPI          6 60.619141            0 
            2            32            32   gramschmidt           MPI          7 57.929688            0 
            2            32            32   gramschmidt           MPI          8 57.177734            0 
            2            32            32   gramschmidt           MPI          9 72.921875            0 
# Runtime: 7.014703 s (overhead: 0.000029 %) 10 records
