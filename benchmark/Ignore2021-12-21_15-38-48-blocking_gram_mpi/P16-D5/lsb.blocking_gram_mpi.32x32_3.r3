# Sysname : Linux
# Nodename: eu-a6-010-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:10:31 2021
# Execution time and date (local): Tue Dec 21 16:10:31 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 82.048828            0 
            3            32            32   gramschmidt           MPI          1 91.330078            2 
            3            32            32   gramschmidt           MPI          2 32.990234            0 
            3            32            32   gramschmidt           MPI          3 70.886719            0 
            3            32            32   gramschmidt           MPI          4 63.167969            0 
            3            32            32   gramschmidt           MPI          5 60.968750            0 
            3            32            32   gramschmidt           MPI          6 58.214844            0 
            3            32            32   gramschmidt           MPI          7 60.333984            0 
            3            32            32   gramschmidt           MPI          8 56.767578            0 
            3            32            32   gramschmidt           MPI          9 72.625000            0 
# Runtime: 5.984940 s (overhead: 0.000033 %) 10 records
