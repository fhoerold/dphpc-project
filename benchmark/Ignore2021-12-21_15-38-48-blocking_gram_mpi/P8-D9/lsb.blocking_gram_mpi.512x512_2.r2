# Sysname : Linux
# Nodename: eu-a6-009-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:40:26 2021
# Execution time and date (local): Tue Dec 21 15:40:26 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 22391.765625            0 
            2           512           512   gramschmidt           MPI          1 22004.193359            4 
            2           512           512   gramschmidt           MPI          2 22137.562500            1 
            2           512           512   gramschmidt           MPI          3 21921.810547            0 
            2           512           512   gramschmidt           MPI          4 21823.658203            1 
            2           512           512   gramschmidt           MPI          5 21934.492188            0 
            2           512           512   gramschmidt           MPI          6 21921.458984            0 
            2           512           512   gramschmidt           MPI          7 21752.783203            0 
            2           512           512   gramschmidt           MPI          8 22213.576172            4 
            2           512           512   gramschmidt           MPI          9 21735.134766            0 
# Runtime: 6.102936 s (overhead: 0.000164 %) 10 records
