# Sysname : Linux
# Nodename: eu-a6-009-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:40:26 2021
# Execution time and date (local): Tue Dec 21 15:40:26 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 30721.929688            0 
            3           512           512   gramschmidt           MPI          1 30272.810547            3 
            3           512           512   gramschmidt           MPI          2 30383.980469            3 
            3           512           512   gramschmidt           MPI          3 30215.654297            0 
            3           512           512   gramschmidt           MPI          4 30107.250000            1 
            3           512           512   gramschmidt           MPI          5 30255.363281            0 
            3           512           512   gramschmidt           MPI          6 30179.482422            0 
            3           512           512   gramschmidt           MPI          7 30028.720703            0 
            3           512           512   gramschmidt           MPI          8 30493.550781            1 
            3           512           512   gramschmidt           MPI          9 29863.652344            0 
# Runtime: 8.098846 s (overhead: 0.000099 %) 10 records
