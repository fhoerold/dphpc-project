# Sysname : Linux
# Nodename: eu-a6-010-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:24:30 2021
# Execution time and date (local): Tue Dec 21 16:24:30 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 397.228516            0 
            3           128           128   gramschmidt           MPI          1 341.707031            3 
            3           128           128   gramschmidt           MPI          2 347.248047            0 
            3           128           128   gramschmidt           MPI          3 323.531250            0 
            3           128           128   gramschmidt           MPI          4 310.906250            0 
            3           128           128   gramschmidt           MPI          5 290.021484            0 
            3           128           128   gramschmidt           MPI          6 318.064453            0 
            3           128           128   gramschmidt           MPI          7 272.363281            0 
            3           128           128   gramschmidt           MPI          8 281.650391            2 
            3           128           128   gramschmidt           MPI          9 271.126953            0 
# Runtime: 6.025159 s (overhead: 0.000083 %) 10 records
