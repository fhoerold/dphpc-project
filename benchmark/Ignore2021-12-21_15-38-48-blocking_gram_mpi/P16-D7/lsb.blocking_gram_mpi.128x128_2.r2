# Sysname : Linux
# Nodename: eu-a6-010-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:24:30 2021
# Execution time and date (local): Tue Dec 21 16:24:30 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 347.015625            0 
            2           128           128   gramschmidt           MPI          1 303.412109            2 
            2           128           128   gramschmidt           MPI          2 305.625000            0 
            2           128           128   gramschmidt           MPI          3 282.068359            0 
            2           128           128   gramschmidt           MPI          4 272.150391            0 
            2           128           128   gramschmidt           MPI          5 251.257812            0 
            2           128           128   gramschmidt           MPI          6 273.037109            0 
            2           128           128   gramschmidt           MPI          7 233.291016            0 
            2           128           128   gramschmidt           MPI          8 241.105469            0 
            2           128           128   gramschmidt           MPI          9 232.783203            0 
# Runtime: 6.033786 s (overhead: 0.000033 %) 10 records
