# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:39:43 2021
# Execution time and date (local): Tue Dec 21 15:39:43 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 2465921.447266            0 
            2          1024          1024   gramschmidt           MPI          1 2467037.191406            4 
            2          1024          1024   gramschmidt           MPI          2 2469591.234375            1 
            2          1024          1024   gramschmidt           MPI          3 2482908.232422            0 
            2          1024          1024   gramschmidt           MPI          4 2481502.351562            4 
            2          1024          1024   gramschmidt           MPI          5 2489031.996094            0 
            2          1024          1024   gramschmidt           MPI          6 2480839.001953            0 
            2          1024          1024   gramschmidt           MPI          7 2494469.275391            0 
            2          1024          1024   gramschmidt           MPI          8 2464639.703125            2 
            2          1024          1024   gramschmidt           MPI          9 2475343.164062            0 
# Runtime: 103.656711 s (overhead: 0.000011 %) 10 records
