# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:39:43 2021
# Execution time and date (local): Tue Dec 21 15:39:43 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 3452164.380859            0 
            3          1024          1024   gramschmidt           MPI          1 3409144.988281            5 
            3          1024          1024   gramschmidt           MPI          2 3442349.949219            2 
            3          1024          1024   gramschmidt           MPI          3 3410516.570312            0 
            3          1024          1024   gramschmidt           MPI          4 3443626.494141            4 
            3          1024          1024   gramschmidt           MPI          5 3469400.003906            0 
            3          1024          1024   gramschmidt           MPI          6 3450479.931641            0 
            3          1024          1024   gramschmidt           MPI          7 3481663.556641            0 
            3          1024          1024   gramschmidt           MPI          8 3425291.806641            2 
            3          1024          1024   gramschmidt           MPI          9 3412097.214844            0 
# Runtime: 117.595348 s (overhead: 0.000011 %) 10 records
