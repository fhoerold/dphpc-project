# Sysname : Linux
# Nodename: eu-a6-006-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:39:27 2021
# Execution time and date (local): Tue Dec 21 15:39:27 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 11486.642578            0 
            2           256           256   gramschmidt           MPI          1 11711.916016            4 
            2           256           256   gramschmidt           MPI          2 11481.261719            0 
            2           256           256   gramschmidt           MPI          3 11283.966797            0 
            2           256           256   gramschmidt           MPI          4 11282.679688            0 
            2           256           256   gramschmidt           MPI          5 11367.023438            0 
            2           256           256   gramschmidt           MPI          6 11367.085938            0 
            2           256           256   gramschmidt           MPI          7 11398.814453            0 
            2           256           256   gramschmidt           MPI          8 11423.029297            1 
            2           256           256   gramschmidt           MPI          9 11545.396484            0 
# Runtime: 15.342428 s (overhead: 0.000033 %) 10 records
