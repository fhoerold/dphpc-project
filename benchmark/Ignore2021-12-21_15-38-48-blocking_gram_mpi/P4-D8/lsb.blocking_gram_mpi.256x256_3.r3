# Sysname : Linux
# Nodename: eu-a6-006-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 14:39:27 2021
# Execution time and date (local): Tue Dec 21 15:39:27 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 15988.052734            0 
            3           256           256   gramschmidt           MPI          1 16243.486328            4 
            3           256           256   gramschmidt           MPI          2 15927.968750            0 
            3           256           256   gramschmidt           MPI          3 15780.103516            0 
            3           256           256   gramschmidt           MPI          4 15736.773438            0 
            3           256           256   gramschmidt           MPI          5 15838.964844            0 
            3           256           256   gramschmidt           MPI          6 15834.210938            0 
            3           256           256   gramschmidt           MPI          7 15914.564453            0 
            3           256           256   gramschmidt           MPI          8 15868.695312            5 
            3           256           256   gramschmidt           MPI          9 15859.642578            0 
# Runtime: 16.311239 s (overhead: 0.000055 %) 10 records
