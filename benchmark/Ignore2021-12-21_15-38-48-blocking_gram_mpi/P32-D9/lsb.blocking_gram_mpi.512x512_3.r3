# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 02:44:57 2021
# Execution time and date (local): Wed Dec 22 03:44:57 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 4404.675781            0 
            3           512           512   gramschmidt           MPI          1 4170.794922            4 
            3           512           512   gramschmidt           MPI          2 3982.304688            0 
            3           512           512   gramschmidt           MPI          3 3794.314453            0 
            3           512           512   gramschmidt           MPI          4 3798.746094            0 
            3           512           512   gramschmidt           MPI          5 3637.041016            0 
            3           512           512   gramschmidt           MPI          6 3486.822266            0 
            3           512           512   gramschmidt           MPI          7 3459.400391            0 
            3           512           512   gramschmidt           MPI          8 3508.371094            1 
            3           512           512   gramschmidt           MPI          9 3359.783203            0 
# Runtime: 19.621350 s (overhead: 0.000025 %) 10 records
