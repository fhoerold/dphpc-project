# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 02:44:57 2021
# Execution time and date (local): Wed Dec 22 03:44:57 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 3970.566406            0 
            2           512           512   gramschmidt           MPI          1 3645.945312            4 
            2           512           512   gramschmidt           MPI          2 3457.298828            0 
            2           512           512   gramschmidt           MPI          3 3260.669922            0 
            2           512           512   gramschmidt           MPI          4 3308.726562            1 
            2           512           512   gramschmidt           MPI          5 3076.042969            0 
            2           512           512   gramschmidt           MPI          6 2931.992188            0 
            2           512           512   gramschmidt           MPI          7 2916.935547            0 
            2           512           512   gramschmidt           MPI          8 2930.509766            1 
            2           512           512   gramschmidt           MPI          9 2790.214844            0 
# Runtime: 17.601926 s (overhead: 0.000034 %) 10 records
