Invocation: ../benchmark.sh -o /cluster/home/forstesa/repos/dphpc-project/gramschmidt/benchmark --openmp -d 5 6 7 8 9 10 11 12 13 -p 1 2 4 8 16 18 ../build/polybench_openmp_gramschmidt
Benchmark 'polybench_openmp_gramschmidt' for
> Dimensions:		32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 
> Number of processors:	1, 2, 4, 8, 16, 18, 
> CPU Model:		XeonGold_6150
> Use MPI:		false
> Use OpenMP:		true

Timestamp: 2021-12-20_10-51-02
