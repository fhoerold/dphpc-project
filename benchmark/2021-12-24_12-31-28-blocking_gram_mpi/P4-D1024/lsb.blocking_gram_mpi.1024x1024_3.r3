# Sysname : Linux
# Nodename: eu-a6-004-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:31:52 2021
# Execution time and date (local): Fri Dec 24 12:31:52 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 3201482.718750            1 
            3          1024          1024   gramschmidt           MPI          1 3201237.380859            5 
            3          1024          1024   gramschmidt           MPI          2 3176094.384766            2 
            3          1024          1024   gramschmidt           MPI          3 3181314.951172            0 
            3          1024          1024   gramschmidt           MPI          4 3183559.714844            4 
            3          1024          1024   gramschmidt           MPI          5 3202747.664062            0 
            3          1024          1024   gramschmidt           MPI          6 3202404.783203            0 
            3          1024          1024   gramschmidt           MPI          7 3197108.589844            0 
            3          1024          1024   gramschmidt           MPI          8 3189346.945312            4 
            3          1024          1024   gramschmidt           MPI          9 3197170.738281            0 
# Runtime: 40.545444 s (overhead: 0.000039 %) 10 records
