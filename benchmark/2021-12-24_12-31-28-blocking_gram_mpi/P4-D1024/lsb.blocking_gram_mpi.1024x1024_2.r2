# Sysname : Linux
# Nodename: eu-a6-004-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:31:52 2021
# Execution time and date (local): Fri Dec 24 12:31:52 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 3014775.259766            0 
            2          1024          1024   gramschmidt           MPI          1 3017561.841797            3 
            2          1024          1024   gramschmidt           MPI          2 2995409.087891            1 
            2          1024          1024   gramschmidt           MPI          3 3000551.388672            0 
            2          1024          1024   gramschmidt           MPI          4 2999067.781250            4 
            2          1024          1024   gramschmidt           MPI          5 3015149.869141            0 
            2          1024          1024   gramschmidt           MPI          6 3014142.699219            0 
            2          1024          1024   gramschmidt           MPI          7 3016393.656250            0 
            2          1024          1024   gramschmidt           MPI          8 3005265.449219            4 
            2          1024          1024   gramschmidt           MPI          9 3012596.527344            0 
# Runtime: 41.383479 s (overhead: 0.000029 %) 10 records
