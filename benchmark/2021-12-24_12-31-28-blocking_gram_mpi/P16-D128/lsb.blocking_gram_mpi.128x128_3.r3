# Sysname : Linux
# Nodename: eu-a6-006-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:52:00 2021
# Execution time and date (local): Fri Dec 24 12:52:00 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 559.812500            0 
            3           128           128   gramschmidt           MPI          1 368.494141            0 
            3           128           128   gramschmidt           MPI          2 292.582031            0 
            3           128           128   gramschmidt           MPI          3 290.554688            0 
            3           128           128   gramschmidt           MPI          4 295.402344            0 
            3           128           128   gramschmidt           MPI          5 354.908203            0 
            3           128           128   gramschmidt           MPI          6 296.427734            0 
            3           128           128   gramschmidt           MPI          7 263.148438            0 
            3           128           128   gramschmidt           MPI          8 268.507812            0 
            3           128           128   gramschmidt           MPI          9 287.498047            0 
# Runtime: 0.009509 s (overhead: 0.000000 %) 10 records
