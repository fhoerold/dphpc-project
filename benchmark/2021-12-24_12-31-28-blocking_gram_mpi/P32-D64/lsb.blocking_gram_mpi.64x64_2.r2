# Sysname : Linux
# Nodename: eu-a6-007-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 16:54:36 2021
# Execution time and date (local): Fri Dec 24 17:54:36 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 495.945312            0 
            2            64            64   gramschmidt           MPI          1 121.572266            3 
            2            64            64   gramschmidt           MPI          2 108.734375            0 
            2            64            64   gramschmidt           MPI          3 88.417969            0 
            2            64            64   gramschmidt           MPI          4 93.988281            0 
            2            64            64   gramschmidt           MPI          5 96.054688            0 
            2            64            64   gramschmidt           MPI          6 90.781250            0 
            2            64            64   gramschmidt           MPI          7 81.560547            0 
            2            64            64   gramschmidt           MPI          8 92.558594            0 
            2            64            64   gramschmidt           MPI          9 105.205078            0 
# Runtime: 12.979413 s (overhead: 0.000023 %) 10 records
