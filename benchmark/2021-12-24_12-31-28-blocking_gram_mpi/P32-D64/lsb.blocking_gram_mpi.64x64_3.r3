# Sysname : Linux
# Nodename: eu-a6-007-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 16:54:36 2021
# Execution time and date (local): Fri Dec 24 17:54:36 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 123.824219            0 
            3            64            64   gramschmidt           MPI          1 171.015625            3 
            3            64            64   gramschmidt           MPI          2 114.550781            0 
            3            64            64   gramschmidt           MPI          3 98.406250            0 
            3            64            64   gramschmidt           MPI          4 98.105469            0 
            3            64            64   gramschmidt           MPI          5 99.242188            0 
            3            64            64   gramschmidt           MPI          6 91.650391            0 
            3            64            64   gramschmidt           MPI          7 95.814453            0 
            3            64            64   gramschmidt           MPI          8 105.144531            0 
            3            64            64   gramschmidt           MPI          9 102.539062            0 
# Runtime: 9.951071 s (overhead: 0.000030 %) 10 records
