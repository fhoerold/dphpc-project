# Sysname : Linux
# Nodename: eu-a6-007-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 16:52:30 2021
# Execution time and date (local): Fri Dec 24 17:52:30 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 19145.962891            0 
            3           512           512   gramschmidt           MPI          1 19758.978516            5 
            3           512           512   gramschmidt           MPI          2 19131.886719            2 
            3           512           512   gramschmidt           MPI          3 19336.980469            0 
            3           512           512   gramschmidt           MPI          4 18917.527344            5 
            3           512           512   gramschmidt           MPI          5 18887.708984            0 
            3           512           512   gramschmidt           MPI          6 19122.587891            0 
            3           512           512   gramschmidt           MPI          7 18937.962891            0 
            3           512           512   gramschmidt           MPI          8 18960.550781            2 
            3           512           512   gramschmidt           MPI          9 18645.521484            0 
# Runtime: 5.414815 s (overhead: 0.000259 %) 10 records
