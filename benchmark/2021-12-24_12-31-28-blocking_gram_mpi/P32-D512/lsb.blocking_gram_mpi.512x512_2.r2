# Sysname : Linux
# Nodename: eu-a6-007-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 16:52:30 2021
# Execution time and date (local): Fri Dec 24 17:52:30 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 19141.863281            3 
            2           512           512   gramschmidt           MPI          1 19758.523438            6 
            2           512           512   gramschmidt           MPI          2 19127.806641            1 
            2           512           512   gramschmidt           MPI          3 19293.212891            0 
            2           512           512   gramschmidt           MPI          4 18935.351562            1 
            2           512           512   gramschmidt           MPI          5 18886.660156            0 
            2           512           512   gramschmidt           MPI          6 19128.332031            0 
            2           512           512   gramschmidt           MPI          7 18916.339844            0 
            2           512           512   gramschmidt           MPI          8 19011.505859            2 
            2           512           512   gramschmidt           MPI          9 18662.134766            0 
# Runtime: 5.415294 s (overhead: 0.000240 %) 10 records
