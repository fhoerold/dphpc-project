# Sysname : Linux
# Nodename: eu-a6-007-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 16:54:57 2021
# Execution time and date (local): Fri Dec 24 17:54:57 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 292.412109            0 
            3           128           128   gramschmidt           MPI          1 451.376953            3 
            3           128           128   gramschmidt           MPI          2 234.296875            0 
            3           128           128   gramschmidt           MPI          3 240.246094            0 
            3           128           128   gramschmidt           MPI          4 243.750000            0 
            3           128           128   gramschmidt           MPI          5 241.935547            0 
            3           128           128   gramschmidt           MPI          6 228.062500            0 
            3           128           128   gramschmidt           MPI          7 227.892578            0 
            3           128           128   gramschmidt           MPI          8 227.794922            0 
            3           128           128   gramschmidt           MPI          9 226.457031            0 
# Runtime: 9.013003 s (overhead: 0.000033 %) 10 records
