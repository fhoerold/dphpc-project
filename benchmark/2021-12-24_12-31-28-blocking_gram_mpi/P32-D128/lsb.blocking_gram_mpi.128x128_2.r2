# Sysname : Linux
# Nodename: eu-a6-007-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 16:54:57 2021
# Execution time and date (local): Fri Dec 24 17:54:57 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 532.380859            1 
            2           128           128   gramschmidt           MPI          1 452.205078            3 
            2           128           128   gramschmidt           MPI          2 228.894531            0 
            2           128           128   gramschmidt           MPI          3 232.523438            0 
            2           128           128   gramschmidt           MPI          4 224.257812            0 
            2           128           128   gramschmidt           MPI          5 228.849609            0 
            2           128           128   gramschmidt           MPI          6 216.843750            0 
            2           128           128   gramschmidt           MPI          7 218.970703            0 
            2           128           128   gramschmidt           MPI          8 210.517578            0 
            2           128           128   gramschmidt           MPI          9 214.412109            0 
# Runtime: 11.023560 s (overhead: 0.000036 %) 10 records
