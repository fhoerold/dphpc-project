# Sysname : Linux
# Nodename: eu-a6-012-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:32:45 2021
# Execution time and date (local): Fri Dec 24 12:32:45 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 379.896484            0 
            2            64            64   gramschmidt           MPI          1 69.857422            3 
            2            64            64   gramschmidt           MPI          2 83.912109            0 
            2            64            64   gramschmidt           MPI          3 85.404297            0 
            2            64            64   gramschmidt           MPI          4 83.115234            0 
            2            64            64   gramschmidt           MPI          5 84.929688            0 
            2            64            64   gramschmidt           MPI          6 89.142578            0 
            2            64            64   gramschmidt           MPI          7 79.613281            0 
            2            64            64   gramschmidt           MPI          8 80.390625            0 
            2            64            64   gramschmidt           MPI          9 76.935547            0 
# Runtime: 2.994056 s (overhead: 0.000100 %) 10 records
