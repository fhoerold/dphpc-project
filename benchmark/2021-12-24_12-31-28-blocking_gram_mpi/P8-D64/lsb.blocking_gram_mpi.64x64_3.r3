# Sysname : Linux
# Nodename: eu-a6-012-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:32:45 2021
# Execution time and date (local): Fri Dec 24 12:32:45 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 84.597656            0 
            3            64            64   gramschmidt           MPI          1 418.894531            3 
            3            64            64   gramschmidt           MPI          2 101.259766            0 
            3            64            64   gramschmidt           MPI          3 98.992188            0 
            3            64            64   gramschmidt           MPI          4 92.687500            0 
            3            64            64   gramschmidt           MPI          5 94.673828            0 
            3            64            64   gramschmidt           MPI          6 95.253906            0 
            3            64            64   gramschmidt           MPI          7 88.027344            0 
            3            64            64   gramschmidt           MPI          8 92.460938            0 
            3            64            64   gramschmidt           MPI          9 87.636719            0 
# Runtime: 3.998485 s (overhead: 0.000075 %) 10 records
