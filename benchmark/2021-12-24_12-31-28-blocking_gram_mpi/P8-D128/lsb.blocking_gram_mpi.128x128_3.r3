# Sysname : Linux
# Nodename: eu-a6-006-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:33:16 2021
# Execution time and date (local): Fri Dec 24 12:33:16 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 683.771484            0 
            3           128           128   gramschmidt           MPI          1 538.259766            3 
            3           128           128   gramschmidt           MPI          2 654.974609            0 
            3           128           128   gramschmidt           MPI          3 644.628906            0 
            3           128           128   gramschmidt           MPI          4 630.898438            0 
            3           128           128   gramschmidt           MPI          5 642.015625            0 
            3           128           128   gramschmidt           MPI          6 620.369141            0 
            3           128           128   gramschmidt           MPI          7 627.591797            0 
            3           128           128   gramschmidt           MPI          8 634.636719            0 
            3           128           128   gramschmidt           MPI          9 625.851562            0 
# Runtime: 3.017954 s (overhead: 0.000099 %) 10 records
