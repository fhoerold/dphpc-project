# Sysname : Linux
# Nodename: eu-a6-006-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:33:16 2021
# Execution time and date (local): Fri Dec 24 12:33:16 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 770.183594            0 
            2           128           128   gramschmidt           MPI          1 448.339844            3 
            2           128           128   gramschmidt           MPI          2 563.996094            0 
            2           128           128   gramschmidt           MPI          3 559.236328            0 
            2           128           128   gramschmidt           MPI          4 551.257812            0 
            2           128           128   gramschmidt           MPI          5 577.716797            0 
            2           128           128   gramschmidt           MPI          6 537.093750            0 
            2           128           128   gramschmidt           MPI          7 541.347656            0 
            2           128           128   gramschmidt           MPI          8 554.617188            0 
            2           128           128   gramschmidt           MPI          9 547.919922            0 
# Runtime: 10.022333 s (overhead: 0.000030 %) 10 records
