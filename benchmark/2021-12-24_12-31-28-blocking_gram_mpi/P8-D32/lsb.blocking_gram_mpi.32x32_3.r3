# Sysname : Linux
# Nodename: eu-a6-011-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:32:15 2021
# Execution time and date (local): Fri Dec 24 12:32:15 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 32.955078            0 
            3            32            32   gramschmidt           MPI          1 378.050781            2 
            3            32            32   gramschmidt           MPI          2 346.494141            0 
            3            32            32   gramschmidt           MPI          3 38.050781            0 
            3            32            32   gramschmidt           MPI          4 28.376953            0 
            3            32            32   gramschmidt           MPI          5 29.722656            0 
            3            32            32   gramschmidt           MPI          6 30.292969            0 
            3            32            32   gramschmidt           MPI          7 29.208984            0 
            3            32            32   gramschmidt           MPI          8 27.210938            0 
            3            32            32   gramschmidt           MPI          9 33.751953            0 
# Runtime: 4.996721 s (overhead: 0.000040 %) 10 records
