# Sysname : Linux
# Nodename: eu-a6-011-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:32:15 2021
# Execution time and date (local): Fri Dec 24 12:32:15 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 320.861328            0 
            2            32            32   gramschmidt           MPI          1 40.640625            2 
            2            32            32   gramschmidt           MPI          2 20.531250            0 
            2            32            32   gramschmidt           MPI          3 36.212891            0 
            2            32            32   gramschmidt           MPI          4 27.369141            0 
            2            32            32   gramschmidt           MPI          5 25.730469            0 
            2            32            32   gramschmidt           MPI          6 27.595703            0 
            2            32            32   gramschmidt           MPI          7 23.041016            0 
            2            32            32   gramschmidt           MPI          8 24.060547            0 
            2            32            32   gramschmidt           MPI          9 28.326172            0 
# Runtime: 4.997049 s (overhead: 0.000040 %) 10 records
