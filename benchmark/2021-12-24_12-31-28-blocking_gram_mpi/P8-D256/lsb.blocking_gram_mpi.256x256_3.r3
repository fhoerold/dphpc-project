# Sysname : Linux
# Nodename: eu-a6-011-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:32:26 2021
# Execution time and date (local): Fri Dec 24 12:32:26 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 5081.142578            0 
            3           256           256   gramschmidt           MPI          1 5239.761719            3 
            3           256           256   gramschmidt           MPI          2 5227.537109            0 
            3           256           256   gramschmidt           MPI          3 5208.195312            0 
            3           256           256   gramschmidt           MPI          4 5175.701172            0 
            3           256           256   gramschmidt           MPI          5 5159.142578            0 
            3           256           256   gramschmidt           MPI          6 5138.431641            0 
            3           256           256   gramschmidt           MPI          7 5177.673828            0 
            3           256           256   gramschmidt           MPI          8 5090.410156            0 
            3           256           256   gramschmidt           MPI          9 5119.111328            0 
# Runtime: 2.092386 s (overhead: 0.000143 %) 10 records
