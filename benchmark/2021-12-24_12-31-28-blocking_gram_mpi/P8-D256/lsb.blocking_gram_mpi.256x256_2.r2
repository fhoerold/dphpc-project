# Sysname : Linux
# Nodename: eu-a6-011-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:32:26 2021
# Execution time and date (local): Fri Dec 24 12:32:26 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 4631.691406            0 
            2           256           256   gramschmidt           MPI          1 4130.326172            3 
            2           256           256   gramschmidt           MPI          2 4445.410156            0 
            2           256           256   gramschmidt           MPI          3 4454.441406            0 
            2           256           256   gramschmidt           MPI          4 4396.501953            0 
            2           256           256   gramschmidt           MPI          5 4426.716797            0 
            2           256           256   gramschmidt           MPI          6 4396.087891            0 
            2           256           256   gramschmidt           MPI          7 4442.404297            0 
            2           256           256   gramschmidt           MPI          8 4347.761719            0 
            2           256           256   gramschmidt           MPI          9 4375.281250            0 
# Runtime: 6.106368 s (overhead: 0.000049 %) 10 records
