# Sysname : Linux
# Nodename: eu-a6-011-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:32:40 2021
# Execution time and date (local): Fri Dec 24 12:32:40 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 102513.802734            0 
            3           512           512   gramschmidt           MPI          1 103644.648438            4 
            3           512           512   gramschmidt           MPI          2 103848.101562            1 
            3           512           512   gramschmidt           MPI          3 103890.201172            0 
            3           512           512   gramschmidt           MPI          4 103569.908203            1 
            3           512           512   gramschmidt           MPI          5 103609.697266            0 
            3           512           512   gramschmidt           MPI          6 103869.886719            0 
            3           512           512   gramschmidt           MPI          7 103525.171875            0 
            3           512           512   gramschmidt           MPI          8 103175.255859            1 
            3           512           512   gramschmidt           MPI          9 103383.765625            0 
# Runtime: 16.348881 s (overhead: 0.000043 %) 10 records
