# Sysname : Linux
# Nodename: eu-a6-011-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:32:40 2021
# Execution time and date (local): Fri Dec 24 12:32:40 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 102412.593750            0 
            2           512           512   gramschmidt           MPI          1 103344.707031            1 
            2           512           512   gramschmidt           MPI          2 103845.882812            1 
            2           512           512   gramschmidt           MPI          3 103890.761719            0 
            2           512           512   gramschmidt           MPI          4 103568.355469            1 
            2           512           512   gramschmidt           MPI          5 103554.251953            0 
            2           512           512   gramschmidt           MPI          6 103805.437500            0 
            2           512           512   gramschmidt           MPI          7 103462.494141            0 
            2           512           512   gramschmidt           MPI          8 103105.886719            4 
            2           512           512   gramschmidt           MPI          9 103362.082031            0 
# Runtime: 1.349946 s (overhead: 0.000519 %) 10 records
