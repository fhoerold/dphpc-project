# Sysname : Linux
# Nodename: eu-a6-004-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:31:45 2021
# Execution time and date (local): Fri Dec 24 12:31:45 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 230.791016            0 
            3            64            64   gramschmidt           MPI          1 183.324219            2 
            3            64            64   gramschmidt           MPI          2 221.781250            0 
            3            64            64   gramschmidt           MPI          3 213.533203            0 
            3            64            64   gramschmidt           MPI          4 215.220703            0 
            3            64            64   gramschmidt           MPI          5 211.892578            0 
            3            64            64   gramschmidt           MPI          6 214.931641            0 
            3            64            64   gramschmidt           MPI          7 210.794922            0 
            3            64            64   gramschmidt           MPI          8 197.361328            0 
            3            64            64   gramschmidt           MPI          9 199.525391            0 
# Runtime: 8.007079 s (overhead: 0.000025 %) 10 records
