# Sysname : Linux
# Nodename: eu-a6-004-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:31:45 2021
# Execution time and date (local): Fri Dec 24 12:31:45 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 196.792969            1 
            2            64            64   gramschmidt           MPI          1 460.501953            3 
            2            64            64   gramschmidt           MPI          2 187.742188            0 
            2            64            64   gramschmidt           MPI          3 176.320312            0 
            2            64            64   gramschmidt           MPI          4 176.492188            0 
            2            64            64   gramschmidt           MPI          5 176.203125            0 
            2            64            64   gramschmidt           MPI          6 172.998047            0 
            2            64            64   gramschmidt           MPI          7 169.345703            0 
            2            64            64   gramschmidt           MPI          8 162.730469            0 
            2            64            64   gramschmidt           MPI          9 160.166016            0 
# Runtime: 8.005680 s (overhead: 0.000050 %) 10 records
