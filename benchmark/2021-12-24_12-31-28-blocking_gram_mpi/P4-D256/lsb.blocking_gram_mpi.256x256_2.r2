# Sysname : Linux
# Nodename: eu-a6-004-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:31:45 2021
# Execution time and date (local): Fri Dec 24 12:31:45 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 12483.519531            0 
            2           256           256   gramschmidt           MPI          1 12448.003906            2 
            2           256           256   gramschmidt           MPI          2 12458.421875            1 
            2           256           256   gramschmidt           MPI          3 12333.906250            0 
            2           256           256   gramschmidt           MPI          4 12409.458984            0 
            2           256           256   gramschmidt           MPI          5 12450.107422            0 
            2           256           256   gramschmidt           MPI          6 12365.578125            0 
            2           256           256   gramschmidt           MPI          7 12350.873047            0 
            2           256           256   gramschmidt           MPI          8 12425.839844            1 
            2           256           256   gramschmidt           MPI          9 12434.132812            0 
# Runtime: 0.195381 s (overhead: 0.002047 %) 10 records
