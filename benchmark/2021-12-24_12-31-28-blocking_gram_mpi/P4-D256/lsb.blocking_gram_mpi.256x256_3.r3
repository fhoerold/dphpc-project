# Sysname : Linux
# Nodename: eu-a6-004-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:31:45 2021
# Execution time and date (local): Fri Dec 24 12:31:45 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 15430.951172            0 
            3           256           256   gramschmidt           MPI          1 15610.613281            3 
            3           256           256   gramschmidt           MPI          2 15410.964844            2 
            3           256           256   gramschmidt           MPI          3 15305.046875            0 
            3           256           256   gramschmidt           MPI          4 15375.777344            0 
            3           256           256   gramschmidt           MPI          5 15396.875000            0 
            3           256           256   gramschmidt           MPI          6 15295.150391            0 
            3           256           256   gramschmidt           MPI          7 15263.847656            0 
            3           256           256   gramschmidt           MPI          8 15390.341797            0 
            3           256           256   gramschmidt           MPI          9 15321.554688            0 
# Runtime: 1.198533 s (overhead: 0.000417 %) 10 records
