# Sysname : Linux
# Nodename: eu-a6-002-06
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:33:46 2021
# Execution time and date (local): Fri Dec 24 12:33:46 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 219000953.705078            4 
            2          4096          4096   gramschmidt           MPI          1 218554403.019531            6 
            2          4096          4096   gramschmidt           MPI          2 222383653.574219            3 
            2          4096          4096   gramschmidt           MPI          3 206237087.421875            3 
            2          4096          4096   gramschmidt           MPI          4 205103430.820312            4 
            2          4096          4096   gramschmidt           MPI          5 220520945.416016            2 
            2          4096          4096   gramschmidt           MPI          6 212196766.992188            2 
            2          4096          4096   gramschmidt           MPI          7 220081638.167969            2 
            2          4096          4096   gramschmidt           MPI          8 216427108.078125            6 
            2          4096          4096   gramschmidt           MPI          9 210675592.216797            0 
# Runtime: 2882.454203 s (overhead: 0.000001 %) 10 records
