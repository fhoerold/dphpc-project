# Sysname : Linux
# Nodename: eu-a6-002-06
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:33:46 2021
# Execution time and date (local): Fri Dec 24 12:33:46 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 219016989.494141            3 
            3          4096          4096   gramschmidt           MPI          1 218570004.802734            6 
            3          4096          4096   gramschmidt           MPI          2 222399824.142578            4 
            3          4096          4096   gramschmidt           MPI          3 206253054.605469            0 
            3          4096          4096   gramschmidt           MPI          4 205118985.486328            4 
            3          4096          4096   gramschmidt           MPI          5 220537011.984375            0 
            3          4096          4096   gramschmidt           MPI          6 212212665.371094            0 
            3          4096          4096   gramschmidt           MPI          7 220097627.873047            0 
            3          4096          4096   gramschmidt           MPI          8 216443176.007812            5 
            3          4096          4096   gramschmidt           MPI          9 210691311.320312            0 
# Runtime: 2886.545489 s (overhead: 0.000001 %) 10 records
