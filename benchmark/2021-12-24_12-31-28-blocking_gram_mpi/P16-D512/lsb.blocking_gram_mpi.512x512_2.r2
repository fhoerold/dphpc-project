# Sysname : Linux
# Nodename: eu-a6-006-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:52:31 2021
# Execution time and date (local): Fri Dec 24 12:52:31 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 32265.667969            0 
            2           512           512   gramschmidt           MPI          1 32294.800781            5 
            2           512           512   gramschmidt           MPI          2 32239.609375            1 
            2           512           512   gramschmidt           MPI          3 32210.816406            0 
            2           512           512   gramschmidt           MPI          4 32076.882812            1 
            2           512           512   gramschmidt           MPI          5 32297.791016            0 
            2           512           512   gramschmidt           MPI          6 32062.117188            0 
            2           512           512   gramschmidt           MPI          7 32091.066406            0 
            2           512           512   gramschmidt           MPI          8 32193.464844            2 
            2           512           512   gramschmidt           MPI          9 55851.734375            0 
# Runtime: 15.691008 s (overhead: 0.000057 %) 10 records
