# Sysname : Linux
# Nodename: eu-a6-006-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:52:31 2021
# Execution time and date (local): Fri Dec 24 12:52:31 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 33964.632812            0 
            3           512           512   gramschmidt           MPI          1 56236.529297            4 
            3           512           512   gramschmidt           MPI          2 56013.849609            1 
            3           512           512   gramschmidt           MPI          3 56077.718750            0 
            3           512           512   gramschmidt           MPI          4 56033.539062            1 
            3           512           512   gramschmidt           MPI          5 56233.830078            0 
            3           512           512   gramschmidt           MPI          6 56140.402344            0 
            3           512           512   gramschmidt           MPI          7 56089.648438            0 
            3           512           512   gramschmidt           MPI          8 56016.472656            1 
            3           512           512   gramschmidt           MPI          9 54691.185547            0 
# Runtime: 15.708268 s (overhead: 0.000045 %) 10 records
