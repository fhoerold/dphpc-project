# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:32:15 2021
# Execution time and date (local): Fri Dec 24 12:32:15 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 39845438.708984            0 
            2          2048          2048   gramschmidt           MPI          1 39809441.982422            3 
            2          2048          2048   gramschmidt           MPI          2 39749037.236328            2 
            2          2048          2048   gramschmidt           MPI          3 39730145.529297            0 
            2          2048          2048   gramschmidt           MPI          4 39758957.939453            3 
            2          2048          2048   gramschmidt           MPI          5 40029037.138672            0 
            2          2048          2048   gramschmidt           MPI          6 39778390.654297            0 
            2          2048          2048   gramschmidt           MPI          7 39829350.433594            0 
            2          2048          2048   gramschmidt           MPI          8 39741714.910156            4 
            2          2048          2048   gramschmidt           MPI          9 39725361.691406            0 
# Runtime: 553.490861 s (overhead: 0.000002 %) 10 records
