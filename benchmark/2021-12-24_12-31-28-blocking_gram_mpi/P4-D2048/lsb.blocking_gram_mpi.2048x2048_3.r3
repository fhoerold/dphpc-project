# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:32:15 2021
# Execution time and date (local): Fri Dec 24 12:32:15 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 42455223.291016            0 
            3          2048          2048   gramschmidt           MPI          1 42440972.687500            3 
            3          2048          2048   gramschmidt           MPI          2 42400588.134766            4 
            3          2048          2048   gramschmidt           MPI          3 42375879.984375            0 
            3          2048          2048   gramschmidt           MPI          4 42396517.035156            3 
            3          2048          2048   gramschmidt           MPI          5 42683753.878906            0 
            3          2048          2048   gramschmidt           MPI          6 42397797.082031            0 
            3          2048          2048   gramschmidt           MPI          7 42464788.093750            0 
            3          2048          2048   gramschmidt           MPI          8 42407907.048828            4 
            3          2048          2048   gramschmidt           MPI          9 42384899.968750            0 
# Runtime: 558.852114 s (overhead: 0.000003 %) 10 records
