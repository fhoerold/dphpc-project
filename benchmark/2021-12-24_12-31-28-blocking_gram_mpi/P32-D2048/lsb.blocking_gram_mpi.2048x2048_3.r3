# Sysname : Linux
# Nodename: eu-a6-007-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 16:53:02 2021
# Execution time and date (local): Fri Dec 24 17:53:02 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 4277468.277344            0 
            3          2048          2048   gramschmidt           MPI          1 4269458.828125            9 
            3          2048          2048   gramschmidt           MPI          2 4254569.822266            2 
            3          2048          2048   gramschmidt           MPI          3 4246749.222656            0 
            3          2048          2048   gramschmidt           MPI          4 4283160.324219            1 
            3          2048          2048   gramschmidt           MPI          5 4253625.150391            0 
            3          2048          2048   gramschmidt           MPI          6 4233460.101562            0 
            3          2048          2048   gramschmidt           MPI          7 4237016.173828            0 
            3          2048          2048   gramschmidt           MPI          8 4248105.560547            6 
            3          2048          2048   gramschmidt           MPI          9 4214452.884766            0 
# Runtime: 69.517041 s (overhead: 0.000026 %) 10 records
