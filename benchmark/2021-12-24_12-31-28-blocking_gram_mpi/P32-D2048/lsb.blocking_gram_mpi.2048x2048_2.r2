# Sysname : Linux
# Nodename: eu-a6-007-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 16:53:02 2021
# Execution time and date (local): Fri Dec 24 17:53:02 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 4277405.910156            0 
            2          2048          2048   gramschmidt           MPI          1 4269052.945312            8 
            2          2048          2048   gramschmidt           MPI          2 4254497.515625            1 
            2          2048          2048   gramschmidt           MPI          3 4246695.943359            0 
            2          2048          2048   gramschmidt           MPI          4 4283108.203125            2 
            2          2048          2048   gramschmidt           MPI          5 4253670.195312            3 
            2          2048          2048   gramschmidt           MPI          6 4233024.388672            0 
            2          2048          2048   gramschmidt           MPI          7 4236975.294922            0 
            2          2048          2048   gramschmidt           MPI          8 4248063.169922            5 
            2          2048          2048   gramschmidt           MPI          9 4214381.716797            0 
# Runtime: 64.517021 s (overhead: 0.000029 %) 10 records
