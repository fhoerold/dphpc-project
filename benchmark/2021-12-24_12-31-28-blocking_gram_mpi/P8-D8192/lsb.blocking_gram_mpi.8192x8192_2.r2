# Sysname : Linux
# Nodename: eu-a6-012-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:34:20 2021
# Execution time and date (local): Fri Dec 24 12:34:20 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          8192          8192   gramschmidt           MPI          0 2497279846.216797            1 
            2          8192          8192   gramschmidt           MPI          1 2477538554.984375            7 
            2          8192          8192   gramschmidt           MPI          2 2450308809.980469            6 
            2          8192          8192   gramschmidt           MPI          3 2474519434.146484            0 
            2          8192          8192   gramschmidt           MPI          4 2464802668.128906            3 
            2          8192          8192   gramschmidt           MPI          5 2490982844.896484            0 
            2          8192          8192   gramschmidt           MPI          6 2339164928.783203            0 
            2          8192          8192   gramschmidt           MPI          7 2254052975.298828            0 
            2          8192          8192   gramschmidt           MPI          8 2247328684.908203            5 
            2          8192          8192   gramschmidt           MPI          9 2266428129.312500            3 
# Runtime: 32298.148861 s (overhead: 0.000000 %) 10 records
