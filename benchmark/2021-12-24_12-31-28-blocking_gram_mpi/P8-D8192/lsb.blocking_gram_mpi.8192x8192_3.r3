# Sysname : Linux
# Nodename: eu-a6-012-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:34:20 2021
# Execution time and date (local): Fri Dec 24 12:34:20 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          8192          8192   gramschmidt           MPI          0 2502233385.150391            3 
            3          8192          8192   gramschmidt           MPI          1 2482452938.550781            9 
            3          8192          8192   gramschmidt           MPI          2 2455170630.878906            7 
            3          8192          8192   gramschmidt           MPI          3 2479428092.253906            1 
            3          8192          8192   gramschmidt           MPI          4 2469692620.574219            9 
            3          8192          8192   gramschmidt           MPI          5 2495926848.056641            3 
            3          8192          8192   gramschmidt           MPI          6 2343805445.802734            2 
            3          8192          8192   gramschmidt           MPI          7 2258526119.443359            2 
            3          8192          8192   gramschmidt           MPI          8 2251788809.974609            7 
            3          8192          8192   gramschmidt           MPI          9 2270925329.779297            3 
# Runtime: 32358.335671 s (overhead: 0.000000 %) 10 records
