# Sysname : Linux
# Nodename: eu-a6-006-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:52:16 2021
# Execution time and date (local): Fri Dec 24 12:52:16 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 1671.386719            0 
            2           256           256   gramschmidt           MPI          1 1977.529297            3 
            2           256           256   gramschmidt           MPI          2 1954.000000            0 
            2           256           256   gramschmidt           MPI          3 1941.636719            0 
            2           256           256   gramschmidt           MPI          4 1922.728516            0 
            2           256           256   gramschmidt           MPI          5 1906.158203            0 
            2           256           256   gramschmidt           MPI          6 1910.123047            0 
            2           256           256   gramschmidt           MPI          7 1897.904297            0 
            2           256           256   gramschmidt           MPI          8 1873.679688            0 
            2           256           256   gramschmidt           MPI          9 1876.351562            0 
# Runtime: 7.058700 s (overhead: 0.000043 %) 10 records
