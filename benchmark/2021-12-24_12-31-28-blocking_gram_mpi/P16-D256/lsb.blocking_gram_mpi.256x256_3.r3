# Sysname : Linux
# Nodename: eu-a6-006-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:52:16 2021
# Execution time and date (local): Fri Dec 24 12:52:16 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 2223.117188            0 
            3           256           256   gramschmidt           MPI          1 2151.464844            1 
            3           256           256   gramschmidt           MPI          2 2125.322266            0 
            3           256           256   gramschmidt           MPI          3 2110.234375            0 
            3           256           256   gramschmidt           MPI          4 2098.859375            0 
            3           256           256   gramschmidt           MPI          5 2120.857422            0 
            3           256           256   gramschmidt           MPI          6 2083.349609            0 
            3           256           256   gramschmidt           MPI          7 2063.373047            0 
            3           256           256   gramschmidt           MPI          8 2041.611328            0 
            3           256           256   gramschmidt           MPI          9 2052.996094            0 
# Runtime: 0.056827 s (overhead: 0.001760 %) 10 records
