# Sysname : Linux
# Nodename: eu-a6-007-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 16:52:07 2021
# Execution time and date (local): Fri Dec 24 17:52:07 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 439.320312            0 
            3            32            32   gramschmidt           MPI          1 98.107422            3 
            3            32            32   gramschmidt           MPI          2 83.326172            0 
            3            32            32   gramschmidt           MPI          3 80.503906            0 
            3            32            32   gramschmidt           MPI          4 79.880859            2 
            3            32            32   gramschmidt           MPI          5 83.400391            0 
            3            32            32   gramschmidt           MPI          6 72.833984            0 
            3            32            32   gramschmidt           MPI          7 70.125000            0 
            3            32            32   gramschmidt           MPI          8 66.591797            0 
            3            32            32   gramschmidt           MPI          9 78.128906            0 
# Runtime: 13.018357 s (overhead: 0.000038 %) 10 records
