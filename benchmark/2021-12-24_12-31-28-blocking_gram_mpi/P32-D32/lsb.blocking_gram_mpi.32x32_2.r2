# Sysname : Linux
# Nodename: eu-a6-007-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 16:52:07 2021
# Execution time and date (local): Fri Dec 24 17:52:07 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 457.884766            1 
            2            32            32   gramschmidt           MPI          1 89.421875            3 
            2            32            32   gramschmidt           MPI          2 77.417969            0 
            2            32            32   gramschmidt           MPI          3 76.914062            0 
            2            32            32   gramschmidt           MPI          4 72.710938            0 
            2            32            32   gramschmidt           MPI          5 67.695312            0 
            2            32            32   gramschmidt           MPI          6 72.308594            0 
            2            32            32   gramschmidt           MPI          7 70.359375            0 
            2            32            32   gramschmidt           MPI          8 66.380859            0 
            2            32            32   gramschmidt           MPI          9 74.080078            0 
# Runtime: 9.025576 s (overhead: 0.000044 %) 10 records
