# Sysname : Linux
# Nodename: eu-a6-007-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 16:52:42 2021
# Execution time and date (local): Fri Dec 24 17:52:42 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 316093.984375            4 
            2          1024          1024   gramschmidt           MPI          1 312087.783203            8 
            2          1024          1024   gramschmidt           MPI          2 313127.324219            2 
            2          1024          1024   gramschmidt           MPI          3 310788.097656            0 
            2          1024          1024   gramschmidt           MPI          4 311382.335938            1 
            2          1024          1024   gramschmidt           MPI          5 311941.455078            0 
            2          1024          1024   gramschmidt           MPI          6 311599.130859            0 
            2          1024          1024   gramschmidt           MPI          7 312531.425781            0 
            2          1024          1024   gramschmidt           MPI          8 310860.792969            1 
            2          1024          1024   gramschmidt           MPI          9 311214.447266            0 
# Runtime: 11.124077 s (overhead: 0.000144 %) 10 records
