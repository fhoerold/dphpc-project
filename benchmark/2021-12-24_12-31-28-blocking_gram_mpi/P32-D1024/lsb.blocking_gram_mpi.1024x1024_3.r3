# Sysname : Linux
# Nodename: eu-a6-007-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 16:52:42 2021
# Execution time and date (local): Fri Dec 24 17:52:42 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 316284.984375            1 
            3          1024          1024   gramschmidt           MPI          1 312187.312500            5 
            3          1024          1024   gramschmidt           MPI          2 313233.810547            2 
            3          1024          1024   gramschmidt           MPI          3 310887.261719            0 
            3          1024          1024   gramschmidt           MPI          4 311480.373047            2 
            3          1024          1024   gramschmidt           MPI          5 312046.408203            0 
            3          1024          1024   gramschmidt           MPI          6 311698.470703            0 
            3          1024          1024   gramschmidt           MPI          7 312642.146484            0 
            3          1024          1024   gramschmidt           MPI          8 310958.787109            3 
            3          1024          1024   gramschmidt           MPI          9 311312.955078            0 
# Runtime: 11.122657 s (overhead: 0.000117 %) 10 records
