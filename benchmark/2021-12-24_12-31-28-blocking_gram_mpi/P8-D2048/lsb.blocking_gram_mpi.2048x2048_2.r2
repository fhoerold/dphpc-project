# Sysname : Linux
# Nodename: eu-a6-006-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:33:45 2021
# Execution time and date (local): Fri Dec 24 12:33:45 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 18963885.703125            0 
            2          2048          2048   gramschmidt           MPI          1 18995804.103516            5 
            2          2048          2048   gramschmidt           MPI          2 19024281.839844            2 
            2          2048          2048   gramschmidt           MPI          3 18961634.728516            0 
            2          2048          2048   gramschmidt           MPI          4 18901163.031250            4 
            2          2048          2048   gramschmidt           MPI          5 18941485.898438            0 
            2          2048          2048   gramschmidt           MPI          6 19003399.611328            0 
            2          2048          2048   gramschmidt           MPI          7 18993943.796875            0 
            2          2048          2048   gramschmidt           MPI          8 18888825.826172            6 
            2          2048          2048   gramschmidt           MPI          9 18910519.310547            0 
# Runtime: 248.792029 s (overhead: 0.000007 %) 10 records
