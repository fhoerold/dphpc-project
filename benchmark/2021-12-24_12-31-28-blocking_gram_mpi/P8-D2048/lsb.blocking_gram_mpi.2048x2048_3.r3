# Sysname : Linux
# Nodename: eu-a6-006-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:33:45 2021
# Execution time and date (local): Fri Dec 24 12:33:45 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 18965236.589844            0 
            3          2048          2048   gramschmidt           MPI          1 18998404.230469            4 
            3          2048          2048   gramschmidt           MPI          2 19025676.833984            2 
            3          2048          2048   gramschmidt           MPI          3 18963058.613281            0 
            3          2048          2048   gramschmidt           MPI          4 18903901.275391            5 
            3          2048          2048   gramschmidt           MPI          5 18942942.812500            0 
            3          2048          2048   gramschmidt           MPI          6 19004701.462891            0 
            3          2048          2048   gramschmidt           MPI          7 18997390.707031            0 
            3          2048          2048   gramschmidt           MPI          8 18890227.880859            6 
            3          2048          2048   gramschmidt           MPI          9 18911871.001953            0 
# Runtime: 246.793370 s (overhead: 0.000007 %) 10 records
