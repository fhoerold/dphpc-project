# Sysname : Linux
# Nodename: eu-a6-006-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:55:45 2021
# Execution time and date (local): Fri Dec 24 12:55:45 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 106892754.191406            2 
            3          4096          4096   gramschmidt           MPI          1 107166806.746094           11 
            3          4096          4096   gramschmidt           MPI          2 107410207.945312            4 
            3          4096          4096   gramschmidt           MPI          3 107256445.017578            0 
            3          4096          4096   gramschmidt           MPI          4 107311286.529297            4 
            3          4096          4096   gramschmidt           MPI          5 107190977.279297            2 
            3          4096          4096   gramschmidt           MPI          6 107130081.429688            2 
            3          4096          4096   gramschmidt           MPI          7 107199828.656250            0 
            3          4096          4096   gramschmidt           MPI          8 107195643.773438            6 
            3          4096          4096   gramschmidt           MPI          9 107252714.109375            0 
# Runtime: 1397.369702 s (overhead: 0.000002 %) 10 records
