# Sysname : Linux
# Nodename: eu-a6-006-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:55:45 2021
# Execution time and date (local): Fri Dec 24 12:55:45 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 106891321.267578            5 
            2          4096          4096   gramschmidt           MPI          1 107165128.738281            6 
            2          4096          4096   gramschmidt           MPI          2 107408828.878906            5 
            2          4096          4096   gramschmidt           MPI          3 107255046.777344            2 
            2          4096          4096   gramschmidt           MPI          4 107309926.466797            6 
            2          4096          4096   gramschmidt           MPI          5 107189587.312500            2 
            2          4096          4096   gramschmidt           MPI          6 107128655.355469            2 
            2          4096          4096   gramschmidt           MPI          7 107198434.025391            2 
            2          4096          4096   gramschmidt           MPI          8 107194229.542969            8 
            2          4096          4096   gramschmidt           MPI          9 107251338.052734            2 
# Runtime: 1398.411101 s (overhead: 0.000003 %) 10 records
