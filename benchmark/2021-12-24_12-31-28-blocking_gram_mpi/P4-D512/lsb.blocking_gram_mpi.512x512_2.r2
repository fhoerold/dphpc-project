# Sysname : Linux
# Nodename: eu-a6-004-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:31:45 2021
# Execution time and date (local): Fri Dec 24 12:31:45 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 184104.646484            0 
            2           512           512   gramschmidt           MPI          1 183902.855469            3 
            2           512           512   gramschmidt           MPI          2 184177.621094            1 
            2           512           512   gramschmidt           MPI          3 182174.105469            0 
            2           512           512   gramschmidt           MPI          4 182138.294922            1 
            2           512           512   gramschmidt           MPI          5 181838.292969            0 
            2           512           512   gramschmidt           MPI          6 181880.296875            0 
            2           512           512   gramschmidt           MPI          7 181957.701172            0 
            2           512           512   gramschmidt           MPI          8 182113.937500            1 
            2           512           512   gramschmidt           MPI          9 185358.625000            0 
# Runtime: 4.385493 s (overhead: 0.000137 %) 10 records
