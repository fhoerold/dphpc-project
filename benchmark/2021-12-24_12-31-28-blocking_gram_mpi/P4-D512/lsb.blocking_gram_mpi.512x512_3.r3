# Sysname : Linux
# Nodename: eu-a6-004-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:31:45 2021
# Execution time and date (local): Fri Dec 24 12:31:45 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 184071.261719            0 
            3           512           512   gramschmidt           MPI          1 183979.259766            4 
            3           512           512   gramschmidt           MPI          2 184127.806641            1 
            3           512           512   gramschmidt           MPI          3 182125.611328            0 
            3           512           512   gramschmidt           MPI          4 182087.900391            1 
            3           512           512   gramschmidt           MPI          5 181805.244141            0 
            3           512           512   gramschmidt           MPI          6 181829.291016            0 
            3           512           512   gramschmidt           MPI          7 181908.236328            0 
            3           512           512   gramschmidt           MPI          8 182063.091797            1 
            3           512           512   gramschmidt           MPI          9 185311.484375            0 
# Runtime: 4.374048 s (overhead: 0.000160 %) 10 records
