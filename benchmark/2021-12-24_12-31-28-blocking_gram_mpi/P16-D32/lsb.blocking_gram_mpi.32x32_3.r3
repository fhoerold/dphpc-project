# Sysname : Linux
# Nodename: eu-a6-004-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:50:46 2021
# Execution time and date (local): Fri Dec 24 12:50:46 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 63.683594            0 
            3            32            32   gramschmidt           MPI          1 54.253906            3 
            3            32            32   gramschmidt           MPI          2 48.044922            0 
            3            32            32   gramschmidt           MPI          3 38.441406            0 
            3            32            32   gramschmidt           MPI          4 36.357422            0 
            3            32            32   gramschmidt           MPI          5 41.371094            0 
            3            32            32   gramschmidt           MPI          6 41.000000            0 
            3            32            32   gramschmidt           MPI          7 35.888672            0 
            3            32            32   gramschmidt           MPI          8 40.277344            0 
            3            32            32   gramschmidt           MPI          9 29.787109            0 
# Runtime: 1.994325 s (overhead: 0.000150 %) 10 records
