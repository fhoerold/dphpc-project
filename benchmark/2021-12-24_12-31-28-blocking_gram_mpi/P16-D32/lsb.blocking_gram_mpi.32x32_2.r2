# Sysname : Linux
# Nodename: eu-a6-004-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:50:46 2021
# Execution time and date (local): Fri Dec 24 12:50:46 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 45.570312            0 
            2            32            32   gramschmidt           MPI          1 53.847656            3 
            2            32            32   gramschmidt           MPI          2 41.816406            0 
            2            32            32   gramschmidt           MPI          3 42.861328            0 
            2            32            32   gramschmidt           MPI          4 37.402344            0 
            2            32            32   gramschmidt           MPI          5 36.410156            0 
            2            32            32   gramschmidt           MPI          6 31.808594            0 
            2            32            32   gramschmidt           MPI          7 42.576172            0 
            2            32            32   gramschmidt           MPI          8 31.783203            0 
            2            32            32   gramschmidt           MPI          9 29.738281            0 
# Runtime: 3.987156 s (overhead: 0.000075 %) 10 records
