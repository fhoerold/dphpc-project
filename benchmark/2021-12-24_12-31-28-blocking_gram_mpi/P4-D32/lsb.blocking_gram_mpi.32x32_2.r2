# Sysname : Linux
# Nodename: eu-a6-004-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:31:45 2021
# Execution time and date (local): Fri Dec 24 12:31:45 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 48.246094            0 
            2            32            32   gramschmidt           MPI          1 48.880859            0 
            2            32            32   gramschmidt           MPI          2 42.400391            0 
            2            32            32   gramschmidt           MPI          3 39.890625            0 
            2            32            32   gramschmidt           MPI          4 36.857422            0 
            2            32            32   gramschmidt           MPI          5 45.640625            0 
            2            32            32   gramschmidt           MPI          6 41.726562            0 
            2            32            32   gramschmidt           MPI          7 36.673828            0 
            2            32            32   gramschmidt           MPI          8 33.892578            0 
            2            32            32   gramschmidt           MPI          9 37.910156            0 
# Runtime: 0.001651 s (overhead: 0.000000 %) 10 records
