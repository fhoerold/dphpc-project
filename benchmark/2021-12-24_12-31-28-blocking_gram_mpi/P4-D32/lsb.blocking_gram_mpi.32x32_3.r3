# Sysname : Linux
# Nodename: eu-a6-004-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:31:45 2021
# Execution time and date (local): Fri Dec 24 12:31:45 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 40.740234            0 
            3            32            32   gramschmidt           MPI          1 52.730469            4 
            3            32            32   gramschmidt           MPI          2 47.722656            0 
            3            32            32   gramschmidt           MPI          3 47.007812            0 
            3            32            32   gramschmidt           MPI          4 44.777344            0 
            3            32            32   gramschmidt           MPI          5 50.404297            0 
            3            32            32   gramschmidt           MPI          6 42.144531            0 
            3            32            32   gramschmidt           MPI          7 42.025391            0 
            3            32            32   gramschmidt           MPI          8 41.130859            0 
            3            32            32   gramschmidt           MPI          9 46.658203            0 
# Runtime: 20.996318 s (overhead: 0.000019 %) 10 records
