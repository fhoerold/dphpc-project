# Sysname : Linux
# Nodename: eu-a6-007-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:07:19 2021
# Execution time and date (local): Fri Dec 24 18:07:19 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          8192          8192   gramschmidt           MPI          0 569418852.818359            4 
            2          8192          8192   gramschmidt           MPI          1 567814575.660156            7 
            2          8192          8192   gramschmidt           MPI          2 566365691.447266            9 
            2          8192          8192   gramschmidt           MPI          3 548612090.492188            4 
            2          8192          8192   gramschmidt           MPI          4 557725879.740234            5 
            2          8192          8192   gramschmidt           MPI          5 564799444.439453            0 
            2          8192          8192   gramschmidt           MPI          6 562725809.574219            0 
            2          8192          8192   gramschmidt           MPI          7 557418569.544922            0 
            2          8192          8192   gramschmidt           MPI          8 560493690.521484           12 
            2          8192          8192   gramschmidt           MPI          9 557843189.943359            2 
# Runtime: 7420.111986 s (overhead: 0.000001 %) 10 records
