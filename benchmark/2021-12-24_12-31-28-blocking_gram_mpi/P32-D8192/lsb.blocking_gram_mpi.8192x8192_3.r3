# Sysname : Linux
# Nodename: eu-a6-007-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 17:07:20 2021
# Execution time and date (local): Fri Dec 24 18:07:20 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          8192          8192   gramschmidt           MPI          0 573238408.656250            2 
            3          8192          8192   gramschmidt           MPI          1 576456033.269531            8 
            3          8192          8192   gramschmidt           MPI          2 571974536.478516            5 
            3          8192          8192   gramschmidt           MPI          3 553159195.429688            0 
            3          8192          8192   gramschmidt           MPI          4 557733679.484375           10 
            3          8192          8192   gramschmidt           MPI          5 564807319.437500            3 
            3          8192          8192   gramschmidt           MPI          6 568085408.816406            2 
            3          8192          8192   gramschmidt           MPI          7 563256740.728516            3 
            3          8192          8192   gramschmidt           MPI          8 567899377.826172            9 
            3          8192          8192   gramschmidt           MPI          9 560807285.408203            0 
# Runtime: 7413.040790 s (overhead: 0.000001 %) 10 records
