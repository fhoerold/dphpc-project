# Sysname : Linux
# Nodename: eu-a6-004-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:50:56 2021
# Execution time and date (local): Fri Dec 24 12:50:56 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 575509.617188            0 
            3          1024          1024   gramschmidt           MPI          1 597481.628906            5 
            3          1024          1024   gramschmidt           MPI          2 618466.070312            1 
            3          1024          1024   gramschmidt           MPI          3 609957.460938            0 
            3          1024          1024   gramschmidt           MPI          4 570009.816406            1 
            3          1024          1024   gramschmidt           MPI          5 570311.191406            0 
            3          1024          1024   gramschmidt           MPI          6 564414.462891            0 
            3          1024          1024   gramschmidt           MPI          7 568156.386719            0 
            3          1024          1024   gramschmidt           MPI          8 561407.628906            6 
            3          1024          1024   gramschmidt           MPI          9 558999.353516            0 
# Runtime: 10.478098 s (overhead: 0.000124 %) 10 records
