# Sysname : Linux
# Nodename: eu-a6-004-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:50:56 2021
# Execution time and date (local): Fri Dec 24 12:50:56 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 575320.632812            0 
            2          1024          1024   gramschmidt           MPI          1 597306.035156            5 
            2          1024          1024   gramschmidt           MPI          2 618294.816406            1 
            2          1024          1024   gramschmidt           MPI          3 609788.691406            0 
            2          1024          1024   gramschmidt           MPI          4 569700.962891            4 
            2          1024          1024   gramschmidt           MPI          5 570137.164062            0 
            2          1024          1024   gramschmidt           MPI          6 564239.021484            0 
            2          1024          1024   gramschmidt           MPI          7 567977.697266            0 
            2          1024          1024   gramschmidt           MPI          8 561233.914062            2 
            2          1024          1024   gramschmidt           MPI          9 558835.671875            0 
# Runtime: 7.478067 s (overhead: 0.000160 %) 10 records
