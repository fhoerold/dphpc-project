# Sysname : Linux
# Nodename: eu-a6-010-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:31:45 2021
# Execution time and date (local): Fri Dec 24 12:31:45 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 1728.029297            0 
            2           128           128   gramschmidt           MPI          1 1654.222656            1 
            2           128           128   gramschmidt           MPI          2 1666.349609            1 
            2           128           128   gramschmidt           MPI          3 1662.902344            0 
            2           128           128   gramschmidt           MPI          4 1679.824219            2 
            2           128           128   gramschmidt           MPI          5 1584.710938            0 
            2           128           128   gramschmidt           MPI          6 1556.748047            0 
            2           128           128   gramschmidt           MPI          7 1405.222656            0 
            2           128           128   gramschmidt           MPI          8 1395.929688            0 
            2           128           128   gramschmidt           MPI          9 1409.992188            0 
# Runtime: 0.024253 s (overhead: 0.016493 %) 10 records
