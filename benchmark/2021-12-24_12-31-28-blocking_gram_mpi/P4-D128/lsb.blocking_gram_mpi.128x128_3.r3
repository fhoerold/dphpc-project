# Sysname : Linux
# Nodename: eu-a6-010-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:31:45 2021
# Execution time and date (local): Fri Dec 24 12:31:45 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 1915.787109            0 
            3           128           128   gramschmidt           MPI          1 1869.207031            4 
            3           128           128   gramschmidt           MPI          2 1873.933594            0 
            3           128           128   gramschmidt           MPI          3 1874.939453            0 
            3           128           128   gramschmidt           MPI          4 1903.789062            0 
            3           128           128   gramschmidt           MPI          5 1895.697266            0 
            3           128           128   gramschmidt           MPI          6 1917.894531            0 
            3           128           128   gramschmidt           MPI          7 1744.287109            0 
            3           128           128   gramschmidt           MPI          8 1726.474609            0 
            3           128           128   gramschmidt           MPI          9 1740.953125            0 
# Runtime: 4.024888 s (overhead: 0.000099 %) 10 records
