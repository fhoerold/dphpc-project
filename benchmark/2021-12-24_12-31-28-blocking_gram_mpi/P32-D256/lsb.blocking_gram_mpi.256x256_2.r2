# Sysname : Linux
# Nodename: eu-a6-007-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 16:55:15 2021
# Execution time and date (local): Fri Dec 24 17:55:15 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 1394.462891            0 
            2           256           256   gramschmidt           MPI          1 1154.404297            4 
            2           256           256   gramschmidt           MPI          2 1153.085938            0 
            2           256           256   gramschmidt           MPI          3 1148.125000            0 
            2           256           256   gramschmidt           MPI          4 1130.908203            0 
            2           256           256   gramschmidt           MPI          5 1104.462891            0 
            2           256           256   gramschmidt           MPI          6 1133.076172            0 
            2           256           256   gramschmidt           MPI          7 1104.089844            0 
            2           256           256   gramschmidt           MPI          8 1101.041016            0 
            2           256           256   gramschmidt           MPI          9 1135.263672            0 
# Runtime: 7.039505 s (overhead: 0.000057 %) 10 records
