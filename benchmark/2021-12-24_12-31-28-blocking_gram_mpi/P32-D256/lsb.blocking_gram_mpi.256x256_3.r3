# Sysname : Linux
# Nodename: eu-a6-007-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 16:55:15 2021
# Execution time and date (local): Fri Dec 24 17:55:15 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 1381.162109            1 
            3           256           256   gramschmidt           MPI          1 1204.148438            4 
            3           256           256   gramschmidt           MPI          2 1189.095703            1 
            3           256           256   gramschmidt           MPI          3 1190.726562            0 
            3           256           256   gramschmidt           MPI          4 1170.054688            0 
            3           256           256   gramschmidt           MPI          5 1150.644531            0 
            3           256           256   gramschmidt           MPI          6 1173.710938            0 
            3           256           256   gramschmidt           MPI          7 1171.410156            0 
            3           256           256   gramschmidt           MPI          8 1140.757812            0 
            3           256           256   gramschmidt           MPI          9 1193.515625            0 
# Runtime: 5.038969 s (overhead: 0.000119 %) 10 records
