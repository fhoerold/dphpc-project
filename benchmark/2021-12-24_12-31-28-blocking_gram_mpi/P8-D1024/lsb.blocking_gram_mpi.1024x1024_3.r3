# Sysname : Linux
# Nodename: eu-a6-002-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:33:16 2021
# Execution time and date (local): Fri Dec 24 12:33:16 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 1272344.425781            0 
            3          1024          1024   gramschmidt           MPI          1 1266747.705078            6 
            3          1024          1024   gramschmidt           MPI          2 1304422.875000            1 
            3          1024          1024   gramschmidt           MPI          3 1391857.332031            0 
            3          1024          1024   gramschmidt           MPI          4 1387753.833984            5 
            3          1024          1024   gramschmidt           MPI          5 1391286.826172            0 
            3          1024          1024   gramschmidt           MPI          6 1424015.318359            0 
            3          1024          1024   gramschmidt           MPI          7 1429715.294922            0 
            3          1024          1024   gramschmidt           MPI          8 1351004.390625            2 
            3          1024          1024   gramschmidt           MPI          9 1353053.738281            0 
# Runtime: 20.257518 s (overhead: 0.000069 %) 10 records
