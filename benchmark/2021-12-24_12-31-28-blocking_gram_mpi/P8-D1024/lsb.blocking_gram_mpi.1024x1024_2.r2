# Sysname : Linux
# Nodename: eu-a6-002-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:33:16 2021
# Execution time and date (local): Fri Dec 24 12:33:16 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 1272348.759766            1 
            2          1024          1024   gramschmidt           MPI          1 1266736.943359            5 
            2          1024          1024   gramschmidt           MPI          2 1304428.968750            1 
            2          1024          1024   gramschmidt           MPI          3 1391615.392578            1 
            2          1024          1024   gramschmidt           MPI          4 1387765.031250            5 
            2          1024          1024   gramschmidt           MPI          5 1391311.542969            0 
            2          1024          1024   gramschmidt           MPI          6 1424032.972656            0 
            2          1024          1024   gramschmidt           MPI          7 1429745.425781            0 
            2          1024          1024   gramschmidt           MPI          8 1351014.513672            5 
            2          1024          1024   gramschmidt           MPI          9 1352813.843750            0 
# Runtime: 20.266956 s (overhead: 0.000089 %) 10 records
