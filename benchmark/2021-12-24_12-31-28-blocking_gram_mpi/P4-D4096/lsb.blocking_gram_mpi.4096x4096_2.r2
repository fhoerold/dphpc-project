# Sysname : Linux
# Nodename: eu-a6-010-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:32:15 2021
# Execution time and date (local): Fri Dec 24 12:32:15 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 438155898.156250            3 
            2          4096          4096   gramschmidt           MPI          1 435678718.835938            8 
            2          4096          4096   gramschmidt           MPI          2 438239594.675781            7 
            2          4096          4096   gramschmidt           MPI          3 440405666.498047            2 
            2          4096          4096   gramschmidt           MPI          4 448173438.652344            6 
            2          4096          4096   gramschmidt           MPI          5 443866541.962891            1 
            2          4096          4096   gramschmidt           MPI          6 433540802.771484            0 
            2          4096          4096   gramschmidt           MPI          7 438742165.794922            1 
            2          4096          4096   gramschmidt           MPI          8 448464779.748047            7 
            2          4096          4096   gramschmidt           MPI          9 438320186.195312            1 
# Runtime: 6055.591084 s (overhead: 0.000001 %) 10 records
