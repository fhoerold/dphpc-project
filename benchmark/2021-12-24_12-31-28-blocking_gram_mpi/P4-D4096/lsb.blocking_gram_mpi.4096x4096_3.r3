# Sysname : Linux
# Nodename: eu-a6-010-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:32:15 2021
# Execution time and date (local): Fri Dec 24 12:32:15 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 463627688.708984            2 
            3          4096          4096   gramschmidt           MPI          1 461256359.660156            8 
            3          4096          4096   gramschmidt           MPI          2 463835065.390625            5 
            3          4096          4096   gramschmidt           MPI          3 466061444.482422            0 
            3          4096          4096   gramschmidt           MPI          4 473738684.175781            5 
            3          4096          4096   gramschmidt           MPI          5 469367060.236328            0 
            3          4096          4096   gramschmidt           MPI          6 459103211.185547            0 
            3          4096          4096   gramschmidt           MPI          7 464218033.728516            0 
            3          4096          4096   gramschmidt           MPI          8 474131468.937500            5 
            3          4096          4096   gramschmidt           MPI          9 463648645.226562            0 
# Runtime: 6075.912084 s (overhead: 0.000000 %) 10 records
