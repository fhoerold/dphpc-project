# Sysname : Linux
# Nodename: eu-a6-007-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 16:55:39 2021
# Execution time and date (local): Fri Dec 24 17:55:39 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 51130604.287109            0 
            2          4096          4096   gramschmidt           MPI          1 51074027.474609            6 
            2          4096          4096   gramschmidt           MPI          2 51213955.115234            4 
            2          4096          4096   gramschmidt           MPI          3 51147927.050781            0 
            2          4096          4096   gramschmidt           MPI          4 51026728.951172            3 
            2          4096          4096   gramschmidt           MPI          5 51066824.896484            0 
            2          4096          4096   gramschmidt           MPI          6 51139142.406250            2 
            2          4096          4096   gramschmidt           MPI          7 51207441.294922            2 
            2          4096          4096   gramschmidt           MPI          8 51052591.183594            6 
            2          4096          4096   gramschmidt           MPI          9 51109368.994141            0 
# Runtime: 671.361224 s (overhead: 0.000003 %) 10 records
