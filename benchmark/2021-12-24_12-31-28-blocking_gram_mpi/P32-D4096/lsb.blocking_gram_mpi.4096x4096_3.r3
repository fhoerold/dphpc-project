# Sysname : Linux
# Nodename: eu-a6-007-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 16:55:39 2021
# Execution time and date (local): Fri Dec 24 17:55:39 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 51131272.626953            4 
            3          4096          4096   gramschmidt           MPI          1 51073296.322266            5 
            3          4096          4096   gramschmidt           MPI          2 51214512.650391            4 
            3          4096          4096   gramschmidt           MPI          3 51148409.945312            0 
            3          4096          4096   gramschmidt           MPI          4 51027334.947266            4 
            3          4096          4096   gramschmidt           MPI          5 51067237.533203            2 
            3          4096          4096   gramschmidt           MPI          6 51139731.460938            0 
            3          4096          4096   gramschmidt           MPI          7 51208016.757812            1 
            3          4096          4096   gramschmidt           MPI          8 51051865.812500            5 
            3          4096          4096   gramschmidt           MPI          9 51109949.371094            0 
# Runtime: 671.333965 s (overhead: 0.000004 %) 10 records
