# Sysname : Linux
# Nodename: eu-a6-010-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 12:19:29 2021
# Execution time and date (local): Fri Dec 24 13:19:29 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          8192          8192   gramschmidt           MPI          0 1236738739.500000            4 
            2          8192          8192   gramschmidt           MPI          1 1238310625.009766           13 
            2          8192          8192   gramschmidt           MPI          2 1238655197.593750           11 
            2          8192          8192   gramschmidt           MPI          3 1232833079.486328            3 
            2          8192          8192   gramschmidt           MPI          4 1228716301.984375           11 
            2          8192          8192   gramschmidt           MPI          5 1232032763.583984            3 
            2          8192          8192   gramschmidt           MPI          6 1236805082.490234            3 
            2          8192          8192   gramschmidt           MPI          7 1237180221.466797            3 
            2          8192          8192   gramschmidt           MPI          8 1228654474.875000           13 
            2          8192          8192   gramschmidt           MPI          9 1235700785.568359            3 
# Runtime: 16299.614770 s (overhead: 0.000000 %) 10 records
