# Sysname : Linux
# Nodename: eu-a6-010-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 12:19:29 2021
# Execution time and date (local): Fri Dec 24 13:19:29 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          8192          8192   gramschmidt           MPI          0 1236885722.011719            4 
            3          8192          8192   gramschmidt           MPI          1 1238457896.685547           13 
            3          8192          8192   gramschmidt           MPI          2 1238801698.457031           12 
            3          8192          8192   gramschmidt           MPI          3 1232979927.560547            1 
            3          8192          8192   gramschmidt           MPI          4 1228861925.031250           10 
            3          8192          8192   gramschmidt           MPI          5 1232179284.689453            2 
            3          8192          8192   gramschmidt           MPI          6 1236952308.681641            2 
            3          8192          8192   gramschmidt           MPI          7 1237327245.402344            2 
            3          8192          8192   gramschmidt           MPI          8 1228800678.208984           14 
            3          8192          8192   gramschmidt           MPI          9 1235846319.193359            4 
# Runtime: 16286.098250 s (overhead: 0.000000 %) 10 records
