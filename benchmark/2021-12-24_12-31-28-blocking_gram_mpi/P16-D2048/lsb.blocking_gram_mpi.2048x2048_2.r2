# Sysname : Linux
# Nodename: eu-a6-006-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:52:54 2021
# Execution time and date (local): Fri Dec 24 12:52:54 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 10847749.541016            0 
            2          2048          2048   gramschmidt           MPI          1 10856941.361328            6 
            2          2048          2048   gramschmidt           MPI          2 10858951.755859            1 
            2          2048          2048   gramschmidt           MPI          3 10859280.882812            0 
            2          2048          2048   gramschmidt           MPI          4 10855081.910156            2 
            2          2048          2048   gramschmidt           MPI          5 10855896.517578            0 
            2          2048          2048   gramschmidt           MPI          6 10864159.546875            0 
            2          2048          2048   gramschmidt           MPI          7 10863763.871094            0 
            2          2048          2048   gramschmidt           MPI          8 10868422.607422            7 
            2          2048          2048   gramschmidt           MPI          9 10852521.007812            0 
# Runtime: 156.762400 s (overhead: 0.000010 %) 10 records
