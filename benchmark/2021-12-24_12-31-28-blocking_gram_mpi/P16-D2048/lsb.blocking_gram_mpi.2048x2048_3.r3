# Sysname : Linux
# Nodename: eu-a6-006-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:52:54 2021
# Execution time and date (local): Fri Dec 24 12:52:54 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 10848592.472656            4 
            3          2048          2048   gramschmidt           MPI          1 10858642.138672            6 
            3          2048          2048   gramschmidt           MPI          2 10859782.171875            1 
            3          2048          2048   gramschmidt           MPI          3 10860098.093750            0 
            3          2048          2048   gramschmidt           MPI          4 10855955.851562            1 
            3          2048          2048   gramschmidt           MPI          5 10856773.929688            0 
            3          2048          2048   gramschmidt           MPI          6 10865673.876953            0 
            3          2048          2048   gramschmidt           MPI          7 10864643.269531            0 
            3          2048          2048   gramschmidt           MPI          8 10869233.927734            5 
            3          2048          2048   gramschmidt           MPI          9 10853361.611328            0 
# Runtime: 153.763841 s (overhead: 0.000011 %) 10 records
