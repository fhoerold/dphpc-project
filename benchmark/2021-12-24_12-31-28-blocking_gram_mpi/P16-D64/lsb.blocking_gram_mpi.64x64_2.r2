# Sysname : Linux
# Nodename: eu-a6-006-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:51:45 2021
# Execution time and date (local): Fri Dec 24 12:51:45 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 417.750000            0 
            2            64            64   gramschmidt           MPI          1 107.035156            3 
            2            64            64   gramschmidt           MPI          2 63.839844            0 
            2            64            64   gramschmidt           MPI          3 60.142578            0 
            2            64            64   gramschmidt           MPI          4 65.265625            0 
            2            64            64   gramschmidt           MPI          5 65.083984            0 
            2            64            64   gramschmidt           MPI          6 64.339844            0 
            2            64            64   gramschmidt           MPI          7 58.582031            0 
            2            64            64   gramschmidt           MPI          8 53.787109            0 
            2            64            64   gramschmidt           MPI          9 59.023438            0 
# Runtime: 2.006135 s (overhead: 0.000150 %) 10 records
