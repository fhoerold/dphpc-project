# Sysname : Linux
# Nodename: eu-a6-006-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 24 11:51:45 2021
# Execution time and date (local): Fri Dec 24 12:51:45 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 417.074219            0 
            3            64            64   gramschmidt           MPI          1 93.691406            0 
            3            64            64   gramschmidt           MPI          2 68.380859            0 
            3            64            64   gramschmidt           MPI          3 66.816406            0 
            3            64            64   gramschmidt           MPI          4 68.708984            0 
            3            64            64   gramschmidt           MPI          5 70.787109            0 
            3            64            64   gramschmidt           MPI          6 64.951172            0 
            3            64            64   gramschmidt           MPI          7 66.814453            0 
            3            64            64   gramschmidt           MPI          8 60.333984            0 
            3            64            64   gramschmidt           MPI          9 64.513672            0 
# Runtime: 0.003833 s (overhead: 0.000000 %) 10 records
