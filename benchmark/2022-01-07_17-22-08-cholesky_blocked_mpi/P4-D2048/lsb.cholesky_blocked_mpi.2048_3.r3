# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:45:00 2022
# Execution time and date (local): Fri Jan  7 17:45:00 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048            64           MPI          0 560359.441406            0 
            3          2048            64           MPI          1 556835.185547            2 
            3          2048            64           MPI          2 556407.777344            3 
            3          2048            64           MPI          3 557974.101562            0 
            3          2048            64           MPI          4 559524.917969            3 
# Runtime: 10.050210 s (overhead: 0.000080 %) 5 records
