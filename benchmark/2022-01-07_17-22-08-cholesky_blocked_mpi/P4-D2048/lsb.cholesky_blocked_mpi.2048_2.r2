# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:45:00 2022
# Execution time and date (local): Fri Jan  7 17:45:00 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048            64           MPI          0 560369.277344            0 
            2          2048            64           MPI          1 556770.771484            3 
            2          2048            64           MPI          2 556416.365234            4 
            2          2048            64           MPI          3 557976.675781            1 
            2          2048            64           MPI          4 559533.878906            4 
# Runtime: 6.047201 s (overhead: 0.000198 %) 5 records
