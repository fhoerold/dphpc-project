# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:43:23 2022
# Execution time and date (local): Fri Jan  7 17:43:23 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512            64           MPI          0 11573.937500            0 
            3           512            64           MPI          1 11919.433594            2 
            3           512            64           MPI          2 12064.279297            2 
            3           512            64           MPI          3 11509.681641            0 
            3           512            64           MPI          4 11711.908203            2 
# Runtime: 4.107867 s (overhead: 0.000146 %) 5 records
