# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:43:23 2022
# Execution time and date (local): Fri Jan  7 17:43:23 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512            64           MPI          0 11567.558594            0 
            2           512            64           MPI          1 11454.363281            2 
            2           512            64           MPI          2 12053.351562            1 
            2           512            64           MPI          3 11499.865234            0 
            2           512            64           MPI          4 11696.857422            3 
# Runtime: 1.107689 s (overhead: 0.000542 %) 5 records
