# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:56:36 2022
# Execution time and date (local): Fri Jan  7 22:56:36 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096            64           MPI          0 1515457.847656            0 
            3          4096            64           MPI          1 1504822.892578            1 
            3          4096            64           MPI          2 1487032.832031            1 
            3          4096            64           MPI          3 1490020.929688            0 
            3          4096            64           MPI          4 1486866.199219            1 
# Runtime: 20.174760 s (overhead: 0.000015 %) 5 records
