# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:42:36 2022
# Execution time and date (local): Fri Jan  7 19:42:36 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096            64           MPI          0 3918374.599609            0 
            2          4096            64           MPI          1 3902307.689453            2 
            2          4096            64           MPI          2 3916445.695312            2 
            2          4096            64           MPI          3 3914465.044922            4 
            2          4096            64           MPI          4 3902155.644531            7 
# Runtime: 27.916115 s (overhead: 0.000054 %) 5 records
