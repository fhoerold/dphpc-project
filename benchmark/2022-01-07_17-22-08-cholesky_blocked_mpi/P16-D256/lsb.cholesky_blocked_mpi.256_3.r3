# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 00:29:16 2022
# Execution time and date (local): Sat Jan  8 01:29:16 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           256            64           MPI          0 823.490234            0 
            3           256            64           MPI          1 628.068359            0 
            3           256            64           MPI          2 638.777344            0 
            3           256            64           MPI          3 609.992188            0 
            3           256            64           MPI          4 825.771484            0 
# Runtime: 8.021465 s (overhead: 0.000000 %) 5 records
