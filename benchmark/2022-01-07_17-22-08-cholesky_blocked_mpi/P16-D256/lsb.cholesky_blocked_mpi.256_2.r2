# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 00:29:16 2022
# Execution time and date (local): Sat Jan  8 01:29:16 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           256            64           MPI          0 825.062500            0 
            2           256            64           MPI          1 620.408203            0 
            2           256            64           MPI          2 634.046875            0 
            2           256            64           MPI          3 624.570312            0 
            2           256            64           MPI          4 743.615234            0 
# Runtime: 8.020237 s (overhead: 0.000000 %) 5 records
