# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:43:10 2022
# Execution time and date (local): Fri Jan  7 17:43:10 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           256            64           MPI          0 1551.232422            0 
            2           256            64           MPI          1 1501.943359            0 
            2           256            64           MPI          2 1548.769531            0 
            2           256            64           MPI          3 1588.255859            0 
            2           256            64           MPI          4 1472.966797            1 
# Runtime: 1.020341 s (overhead: 0.000098 %) 5 records
