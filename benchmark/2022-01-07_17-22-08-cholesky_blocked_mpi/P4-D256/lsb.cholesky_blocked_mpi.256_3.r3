# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:43:10 2022
# Execution time and date (local): Fri Jan  7 17:43:10 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           256            64           MPI          0 1556.566406            0 
            3           256            64           MPI          1 1516.601562            0 
            3           256            64           MPI          2 1564.421875            0 
            3           256            64           MPI          3 1605.263672            0 
            3           256            64           MPI          4 1487.609375            0 
# Runtime: 1.021093 s (overhead: 0.000000 %) 5 records
