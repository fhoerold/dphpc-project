# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:21:58 2022
# Execution time and date (local): Sat Jan  8 05:21:58 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024            64           MPI          0 58032.318359            0 
            2          1024            64           MPI          1 57099.208984            1 
            2          1024            64           MPI          2 56807.917969            1 
            2          1024            64           MPI          3 56990.390625            0 
            2          1024            64           MPI          4 56839.521484            0 
# Runtime: 0.500660 s (overhead: 0.000399 %) 5 records
