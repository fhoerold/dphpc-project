# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:21:58 2022
# Execution time and date (local): Sat Jan  8 05:21:58 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024            64           MPI          0 58187.871094            0 
            3          1024            64           MPI          1 57269.109375            1 
            3          1024            64           MPI          2 57197.443359            1 
            3          1024            64           MPI          3 56909.474609            0 
            3          1024            64           MPI          4 56975.992188            1 
# Runtime: 10.499421 s (overhead: 0.000029 %) 5 records
