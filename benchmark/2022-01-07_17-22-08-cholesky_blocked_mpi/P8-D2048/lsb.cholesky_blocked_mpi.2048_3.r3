# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:36:16 2022
# Execution time and date (local): Fri Jan  7 19:36:16 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048            64           MPI          0 324597.746094            0 
            3          2048            64           MPI          1 312975.703125           11 
            3          2048            64           MPI          2 309592.013672            5 
            3          2048            64           MPI          3 309840.886719            0 
            3          2048            64           MPI          4 309130.640625            6 
# Runtime: 9.388602 s (overhead: 0.000234 %) 5 records
