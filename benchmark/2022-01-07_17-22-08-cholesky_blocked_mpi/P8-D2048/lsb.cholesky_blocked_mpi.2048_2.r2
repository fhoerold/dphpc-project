# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:36:16 2022
# Execution time and date (local): Fri Jan  7 19:36:16 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048            64           MPI          0 324219.480469            0 
            2          2048            64           MPI          1 312857.531250            1 
            2          2048            64           MPI          2 309533.144531            1 
            2          2048            64           MPI          3 309360.533203            0 
            2          2048            64           MPI          4 309158.882812            2 
# Runtime: 9.389447 s (overhead: 0.000043 %) 5 records
