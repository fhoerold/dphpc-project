# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:31:07 2022
# Execution time and date (local): Fri Jan  7 17:31:07 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512            64           MPI          0 7487.330078            0 
            3           512            64           MPI          1 7275.367188            0 
            3           512            64           MPI          2 7361.376953            0 
            3           512            64           MPI          3 7237.478516            0 
            3           512            64           MPI          4 7233.871094            0 
# Runtime: 0.095449 s (overhead: 0.000000 %) 5 records
