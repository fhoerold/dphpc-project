# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:31:07 2022
# Execution time and date (local): Fri Jan  7 17:31:07 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512            64           MPI          0 7503.253906            0 
            2           512            64           MPI          1 7249.132812            0 
            2           512            64           MPI          2 7279.107422            0 
            2           512            64           MPI          3 7208.105469            0 
            2           512            64           MPI          4 7197.958984            0 
# Runtime: 2.086022 s (overhead: 0.000000 %) 5 records
