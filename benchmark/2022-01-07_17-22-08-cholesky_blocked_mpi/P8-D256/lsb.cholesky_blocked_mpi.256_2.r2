# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:30:52 2022
# Execution time and date (local): Fri Jan  7 17:30:52 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           256            64           MPI          0 732.843750            0 
            2           256            64           MPI          1 601.640625            0 
            2           256            64           MPI          2 589.292969            0 
            2           256            64           MPI          3 664.441406            0 
            2           256            64           MPI          4 632.500000            0 
# Runtime: 8.020987 s (overhead: 0.000000 %) 5 records
