# Sysname : Linux
# Nodename: eu-a6-011-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 19:34:23 2022
# Execution time and date (local): Fri Jan  7 20:34:23 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096            64           MPI          0 2319220.064453            0 
            3          4096            64           MPI          1 2294877.324219            1 
            3          4096            64           MPI          2 2319456.160156            1 
            3          4096            64           MPI          3 2351406.994141            0 
            3          4096            64           MPI          4 2352108.867188            2 
# Runtime: 23.024547 s (overhead: 0.000017 %) 5 records
