# Sysname : Linux
# Nodename: eu-a6-011-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 19:34:23 2022
# Execution time and date (local): Fri Jan  7 20:34:23 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096            64           MPI          0 2318371.474609            0 
            2          4096            64           MPI          1 2293922.892578            2 
            2          4096            64           MPI          2 2318497.121094            2 
            2          4096            64           MPI          3 2350428.726562            0 
            2          4096            64           MPI          4 2351153.017578            3 
# Runtime: 22.012948 s (overhead: 0.000032 %) 5 records
