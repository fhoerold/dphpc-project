# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:30:03 2022
# Execution time and date (local): Sat Jan  8 05:30:03 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096            64           MPI          0 1682743.382812            0 
            2          4096            64           MPI          1 1601242.724609            2 
            2          4096            64           MPI          2 1536309.240234            2 
            2          4096            64           MPI          3 1544084.039062            0 
            2          4096            64           MPI          4 1523612.826172            2 
# Runtime: 18.919050 s (overhead: 0.000032 %) 5 records
