# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:30:03 2022
# Execution time and date (local): Sat Jan  8 05:30:03 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096            64           MPI          0 1682417.531250            0 
            3          4096            64           MPI          1 1602478.431641            1 
            3          4096            64           MPI          2 1536363.093750            1 
            3          4096            64           MPI          3 1544143.230469            0 
            3          4096            64           MPI          4 1523809.109375            1 
# Runtime: 18.911738 s (overhead: 0.000016 %) 5 records
