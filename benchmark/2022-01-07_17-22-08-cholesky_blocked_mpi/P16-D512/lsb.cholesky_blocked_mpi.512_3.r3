# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:46:32 2022
# Execution time and date (local): Fri Jan  7 22:46:32 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512            64           MPI          0 8701.031250            0 
            3           512            64           MPI          1 8227.562500            0 
            3           512            64           MPI          2 8337.361328            0 
            3           512            64           MPI          3 8228.642578            0 
            3           512            64           MPI          4 8116.652344            0 
# Runtime: 0.099180 s (overhead: 0.000000 %) 5 records
