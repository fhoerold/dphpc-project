# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:46:32 2022
# Execution time and date (local): Fri Jan  7 22:46:32 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512            64           MPI          0 8724.955078            2 
            2           512            64           MPI          1 8205.681641            0 
            2           512            64           MPI          2 8244.164062            0 
            2           512            64           MPI          3 8265.669922            0 
            2           512            64           MPI          4 8079.115234            0 
# Runtime: 5.104112 s (overhead: 0.000039 %) 5 records
