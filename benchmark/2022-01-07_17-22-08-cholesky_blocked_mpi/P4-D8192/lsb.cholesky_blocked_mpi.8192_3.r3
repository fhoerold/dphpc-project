# Sysname : Linux
# Nodename: eu-a6-011-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 20:37:46 2022
# Execution time and date (local): Fri Jan  7 21:37:46 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192            64           MPI          0 32251656.628906            0 
            3          8192            64           MPI          1 31398271.080078            6 
            3          8192            64           MPI          2 30049615.103516            2 
            3          8192            64           MPI          3 30277642.923828            3 
            3          8192            64           MPI          4 30210094.675781            6 
# Runtime: 223.275732 s (overhead: 0.000008 %) 5 records
