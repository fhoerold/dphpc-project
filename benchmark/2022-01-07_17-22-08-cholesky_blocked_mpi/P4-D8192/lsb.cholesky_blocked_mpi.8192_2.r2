# Sysname : Linux
# Nodename: eu-a6-011-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 20:37:46 2022
# Execution time and date (local): Fri Jan  7 21:37:46 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192            64           MPI          0 32251178.005859            0 
            2          8192            64           MPI          1 31398239.980469            8 
            2          8192            64           MPI          2 30049579.187500            5 
            2          8192            64           MPI          3 30277609.351562            4 
            2          8192            64           MPI          4 30210064.619141           10 
# Runtime: 223.275530 s (overhead: 0.000012 %) 5 records
