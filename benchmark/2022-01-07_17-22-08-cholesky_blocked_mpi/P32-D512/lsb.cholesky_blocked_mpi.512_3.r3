# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 11:39:25 2022
# Execution time and date (local): Sat Jan  8 12:39:25 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512            64           MPI          0 9509.746094            0 
            3           512            64           MPI          1 8952.585938            0 
            3           512            64           MPI          2 8835.412109            0 
            3           512            64           MPI          3 8880.218750            0 
            3           512            64           MPI          4 8766.646484            0 
# Runtime: 4.104567 s (overhead: 0.000000 %) 5 records
