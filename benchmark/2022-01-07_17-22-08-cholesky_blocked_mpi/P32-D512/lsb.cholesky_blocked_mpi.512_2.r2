# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 11:39:25 2022
# Execution time and date (local): Sat Jan  8 12:39:25 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512            64           MPI          0 9434.792969            0 
            2           512            64           MPI          1 8916.039062            0 
            2           512            64           MPI          2 8754.179688            0 
            2           512            64           MPI          3 8838.126953            0 
            2           512            64           MPI          4 8705.302734            0 
# Runtime: 11.112130 s (overhead: 0.000000 %) 5 records
