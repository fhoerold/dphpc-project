# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 22:11:01 2022
# Execution time and date (local): Fri Jan  7 23:11:01 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192            64           MPI          0 17648899.177734            0 
            2          8192            64           MPI          1 17616795.376953            9 
            2          8192            64           MPI          2 17563332.289062            8 
            2          8192            64           MPI          3 17701852.179688            0 
            2          8192            64           MPI          4 17650715.445312            2 
# Runtime: 130.881442 s (overhead: 0.000015 %) 5 records
