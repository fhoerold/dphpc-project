# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 22:11:01 2022
# Execution time and date (local): Fri Jan  7 23:11:01 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192            64           MPI          0 17650933.810547            0 
            3          8192            64           MPI          1 17618526.103516            9 
            3          8192            64           MPI          2 17565293.580078            2 
            3          8192            64           MPI          3 17703851.513672            0 
            3          8192            64           MPI          4 17652630.648438            2 
# Runtime: 130.893876 s (overhead: 0.000010 %) 5 records
