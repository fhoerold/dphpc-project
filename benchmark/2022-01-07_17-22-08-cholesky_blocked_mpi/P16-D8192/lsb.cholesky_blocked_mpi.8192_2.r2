# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 02:33:55 2022
# Execution time and date (local): Sat Jan  8 03:33:55 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192            64           MPI          0 10542098.912109            0 
            2          8192            64           MPI          1 10547190.187500            6 
            2          8192            64           MPI          2 10462107.248047            1 
            2          8192            64           MPI          3 10464072.576172            0 
            2          8192            64           MPI          4 10473341.251953            1 
# Runtime: 87.066749 s (overhead: 0.000009 %) 5 records
