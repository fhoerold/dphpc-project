# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 02:33:55 2022
# Execution time and date (local): Sat Jan  8 03:33:55 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192            64           MPI          0 10544308.265625            0 
            3          8192            64           MPI          1 10549312.150391            1 
            3          8192            64           MPI          2 10463506.585938            4 
            3          8192            64           MPI          3 10465679.910156            0 
            3          8192            64           MPI          4 10475531.085938            6 
# Runtime: 77.068191 s (overhead: 0.000014 %) 5 records
