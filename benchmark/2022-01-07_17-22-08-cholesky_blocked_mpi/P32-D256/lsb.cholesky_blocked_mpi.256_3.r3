# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 11:39:01 2022
# Execution time and date (local): Sat Jan  8 12:39:01 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           256            64           MPI          0 845.783203            0 
            3           256            64           MPI          1 713.980469            0 
            3           256            64           MPI          2 740.070312            0 
            3           256            64           MPI          3 705.527344            0 
            3           256            64           MPI          4 652.437500            0 
# Runtime: 8.012042 s (overhead: 0.000000 %) 5 records
