# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 11:39:01 2022
# Execution time and date (local): Sat Jan  8 12:39:01 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           256            64           MPI          0 830.429688            0 
            2           256            64           MPI          1 712.691406            0 
            2           256            64           MPI          2 708.023438            0 
            2           256            64           MPI          3 714.089844            0 
            2           256            64           MPI          4 648.199219            0 
# Runtime: 11.016322 s (overhead: 0.000000 %) 5 records
