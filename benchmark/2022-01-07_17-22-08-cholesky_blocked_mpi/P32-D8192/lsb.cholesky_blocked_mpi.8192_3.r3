# Sysname : Linux
# Nodename: eu-a6-004-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 17:27:41 2022
# Execution time and date (local): Sat Jan  8 18:27:41 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192            64           MPI          0 8647116.285156            1 
            3          8192            64           MPI          1 8876853.896484            2 
            3          8192            64           MPI          2 8582645.095703            2 
            3          8192            64           MPI          3 8711957.826172            0 
            3          8192            64           MPI          4 8558078.037109            6 
# Runtime: 81.162948 s (overhead: 0.000014 %) 5 records
