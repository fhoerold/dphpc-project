# Sysname : Linux
# Nodename: eu-a6-004-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 17:27:41 2022
# Execution time and date (local): Sat Jan  8 18:27:41 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192            64           MPI          0 8646426.589844            0 
            2          8192            64           MPI          1 8876484.718750            1 
            2          8192            64           MPI          2 8578918.154297            1 
            2          8192            64           MPI          3 8709840.296875            0 
            2          8192            64           MPI          4 8555753.410156            7 
# Runtime: 81.161931 s (overhead: 0.000011 %) 5 records
