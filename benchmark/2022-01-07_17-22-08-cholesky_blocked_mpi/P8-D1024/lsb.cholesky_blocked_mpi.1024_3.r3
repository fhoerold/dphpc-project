# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:35:14 2022
# Execution time and date (local): Fri Jan  7 19:35:14 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024            64           MPI          0 51584.253906            0 
            3          1024            64           MPI          1 50495.847656            7 
            3          1024            64           MPI          2 50153.822266            4 
            3          1024            64           MPI          3 50005.474609            0 
            3          1024            64           MPI          4 50162.656250            3 
# Runtime: 7.438389 s (overhead: 0.000188 %) 5 records
