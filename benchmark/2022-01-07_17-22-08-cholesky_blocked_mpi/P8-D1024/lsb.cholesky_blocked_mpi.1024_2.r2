# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:35:14 2022
# Execution time and date (local): Fri Jan  7 19:35:14 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024            64           MPI          0 51516.890625            0 
            2          1024            64           MPI          1 50322.787109            1 
            2          1024            64           MPI          2 50236.710938            1 
            2          1024            64           MPI          3 49993.789062            1 
            2          1024            64           MPI          4 50106.316406            1 
# Runtime: 8.445508 s (overhead: 0.000047 %) 5 records
