# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:43:40 2022
# Execution time and date (local): Fri Jan  7 17:43:40 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024            64           MPI          0 77629.240234            0 
            2          1024            64           MPI          1 79724.451172            2 
            2          1024            64           MPI          2 76057.046875            3 
            2          1024            64           MPI          3 76263.021484            1 
            2          1024            64           MPI          4 77831.472656            3 
# Runtime: 9.601829 s (overhead: 0.000094 %) 5 records
