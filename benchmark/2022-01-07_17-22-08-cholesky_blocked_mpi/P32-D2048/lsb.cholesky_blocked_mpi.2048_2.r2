# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 11:40:32 2022
# Execution time and date (local): Sat Jan  8 12:40:32 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048            64           MPI          0 305037.642578            0 
            2          2048            64           MPI          1 304228.478516            1 
            2          2048            64           MPI          2 303747.244141            1 
            2          2048            64           MPI          3 303534.728516            0 
            2          2048            64           MPI          4 304866.199219            2 
# Runtime: 12.326487 s (overhead: 0.000032 %) 5 records
