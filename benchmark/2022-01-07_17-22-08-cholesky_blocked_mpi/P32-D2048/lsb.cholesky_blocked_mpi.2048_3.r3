# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 11:40:32 2022
# Execution time and date (local): Sat Jan  8 12:40:32 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048            64           MPI          0 305631.091797            0 
            3          2048            64           MPI          1 304289.138672            1 
            3          2048            64           MPI          2 304027.292969            1 
            3          2048            64           MPI          3 303790.888672            0 
            3          2048            64           MPI          4 305162.503906            1 
# Runtime: 16.334034 s (overhead: 0.000018 %) 5 records
