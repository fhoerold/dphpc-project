# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 00:30:48 2022
# Execution time and date (local): Sat Jan  8 01:30:48 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048            64           MPI          0 314045.000000            0 
            2          2048            64           MPI          1 293900.724609            1 
            2          2048            64           MPI          2 288439.890625            1 
            2          2048            64           MPI          3 303937.314453            0 
            2          2048            64           MPI          4 290083.886719            2 
# Runtime: 11.323054 s (overhead: 0.000035 %) 5 records
