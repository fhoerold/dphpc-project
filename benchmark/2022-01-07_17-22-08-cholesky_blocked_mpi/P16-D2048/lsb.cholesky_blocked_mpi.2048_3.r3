# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 00:30:48 2022
# Execution time and date (local): Sat Jan  8 01:30:48 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048            64           MPI          0 313962.798828            0 
            3          2048            64           MPI          1 293957.154297            1 
            3          2048            64           MPI          2 288484.525391            1 
            3          2048            64           MPI          3 303965.888672            0 
            3          2048            64           MPI          4 290135.044922            1 
# Runtime: 11.323046 s (overhead: 0.000026 %) 5 records
