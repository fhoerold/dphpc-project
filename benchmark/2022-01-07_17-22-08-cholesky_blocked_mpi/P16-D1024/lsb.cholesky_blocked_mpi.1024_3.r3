# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 00:29:36 2022
# Execution time and date (local): Sat Jan  8 01:29:36 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024            64           MPI          0 55121.673828            0 
            3          1024            64           MPI          1 54805.671875            1 
            3          1024            64           MPI          2 54754.013672            1 
            3          1024            64           MPI          3 55045.769531            0 
            3          1024            64           MPI          4 54852.691406            1 
# Runtime: 12.487049 s (overhead: 0.000024 %) 5 records
