# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 00:29:36 2022
# Execution time and date (local): Sat Jan  8 01:29:36 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024            64           MPI          0 55139.072266            0 
            2          1024            64           MPI          1 54791.621094            1 
            2          1024            64           MPI          2 54748.892578            0 
            2          1024            64           MPI          3 55032.921875            0 
            2          1024            64           MPI          4 54781.683594            1 
# Runtime: 13.479511 s (overhead: 0.000015 %) 5 records
