Invocation: ./cholesky/benchmark.sh -o ./benchmark --mpi --linear-dim -d 6502 -p 16 ./cholesky/build/cholesky_mpi_ll_naive
Benchmark 'cholesky_mpi_ll_naive' for
> Dimensions:		6502, 
> Number of processors:	16, 
> CPU Model:		XeonGold_6150
> Use MPI:		true
> Use OpenMP:		false

Timestamp: 2022-01-12_21-05-36
