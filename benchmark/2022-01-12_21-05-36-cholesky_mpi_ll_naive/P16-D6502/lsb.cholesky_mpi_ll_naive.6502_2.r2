# Sysname : Linux
# Nodename: eu-a6-012-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 21:09:09 2022
# Execution time and date (local): Wed Jan 12 22:09:09 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          6502             1           MPI          0 218261705.794922            0 
            2          6502             1           MPI          1 219295573.894531            6 
            2          6502             1           MPI          2 218301133.824219            5 
            2          6502             1           MPI          3 217376975.257812            1 
            2          6502             1           MPI          4 217545361.746094            9 
            2          6502             1           MPI          5 216703136.078125            1 
            2          6502             1           MPI          6 216784918.652344            2 
            2          6502             1           MPI          7 216613351.220703            2 
            2          6502             1           MPI          8 216640539.365234            9 
            2          6502             1           MPI          9 217449502.658203            3 
# Runtime: 2621.762987 s (overhead: 0.000001 %) 10 records
