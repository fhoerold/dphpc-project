# Sysname : Linux
# Nodename: eu-a6-012-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 21:09:09 2022
# Execution time and date (local): Wed Jan 12 22:09:09 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          6502             1           MPI          0 218037162.554688            0 
            3          6502             1           MPI          1 219069987.753906            5 
            3          6502             1           MPI          2 218076583.691406            3 
            3          6502             1           MPI          3 217153363.962891            0 
            3          6502             1           MPI          4 217321575.585938            5 
            3          6502             1           MPI          5 216480217.570312            1 
            3          6502             1           MPI          6 216561916.873047            0 
            3          6502             1           MPI          7 216390526.048828            1 
            3          6502             1           MPI          8 216417685.439453            3 
            3          6502             1           MPI          9 217225816.185547            0 
# Runtime: 2623.056576 s (overhead: 0.000001 %) 10 records
