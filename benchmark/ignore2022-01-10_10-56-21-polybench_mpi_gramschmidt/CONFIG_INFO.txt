Invocation: ./gramschmidt/benchmark.sh -o ./benchmark --mpi -d 10 -p 8 ./build/polybench_mpi_gramschmidt
Benchmark 'polybench_mpi_gramschmidt' for
> Dimensions:		1024, 
> Number of processors:	8, 
> CPU Model:		XeonGold_6150
> Use MPI:		true
> Use OpenMP:		false

Timestamp: 2022-01-10_10-56-21
