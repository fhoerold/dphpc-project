# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 09:56:49 2022
# Execution time and date (local): Mon Jan 10 10:56:49 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 387695.935547            0 
         1024          1024   gramschmidt          1 394458.400391            1 
         1024          1024   gramschmidt          2 393127.328125            5 
         1024          1024   gramschmidt          3 389690.966797            0 
         1024          1024   gramschmidt          4 394746.521484            4 
         1024          1024   gramschmidt          5 391273.896484            0 
         1024          1024   gramschmidt          6 392244.046875            0 
         1024          1024   gramschmidt          7 393038.978516            0 
         1024          1024   gramschmidt          8 392489.810547            0 
         1024          1024   gramschmidt          9 391282.857422            0 
# Runtime: 3.920102 s (overhead: 0.000255 %) 10 records
