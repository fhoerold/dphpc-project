# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 09:56:49 2022
# Execution time and date (local): Mon Jan 10 10:56:49 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 360901.193359            0 
         1024          1024   gramschmidt          1 360414.832031            0 
         1024          1024   gramschmidt          2 375344.537109            3 
         1024          1024   gramschmidt          3 379438.876953            0 
         1024          1024   gramschmidt          4 378442.568359            2 
         1024          1024   gramschmidt          5 388604.216797            0 
         1024          1024   gramschmidt          6 389121.843750            0 
         1024          1024   gramschmidt          7 391764.591797            0 
         1024          1024   gramschmidt          8 394502.652344            1 
         1024          1024   gramschmidt          9 387628.447266            0 
# Runtime: 3.806199 s (overhead: 0.000158 %) 10 records
