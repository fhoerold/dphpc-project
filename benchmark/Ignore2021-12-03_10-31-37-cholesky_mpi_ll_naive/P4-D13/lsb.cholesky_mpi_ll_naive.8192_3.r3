# Sysname : Linux
# Nodename: eu-a6-012-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec  3 12:52:49 2021
# Execution time and date (local): Fri Dec  3 13:52:49 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 36216051.099609            3 
            3          8192           MPI          1 36145996.142578            5 
            3          8192           MPI          2 36223763.177734            4 
            3          8192           MPI          3 35980953.111328            0 
            3          8192           MPI          4 36239678.916016            3 
            3          8192           MPI          5 36193140.551758            2 
            3          8192           MPI          6 36132274.541992            0 
            3          8192           MPI          7 36190808.077148            1 
            3          8192           MPI          8 36163729.790039            5 
            3          8192           MPI          9 36223896.129883            0 
# Runtime: 434.073702 s (overhead: 0.000005 %) 10 records
