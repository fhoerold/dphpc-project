# Sysname : Linux
# Nodename: eu-a6-012-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec  3 12:52:49 2021
# Execution time and date (local): Fri Dec  3 13:52:49 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 36216254.669922            0 
            2          8192           MPI          1 36146210.854492            5 
            2          8192           MPI          2 36223972.241211            6 
            2          8192           MPI          3 35981165.166016            0 
            2          8192           MPI          4 36239890.290039            6 
            2          8192           MPI          5 36193352.051758            3 
            2          8192           MPI          6 36132487.412109            0 
            2          8192           MPI          7 36191019.653320            3 
            2          8192           MPI          8 36163941.304688            5 
            2          8192           MPI          9 36224105.858398            0 
# Runtime: 434.077441 s (overhead: 0.000006 %) 10 records
