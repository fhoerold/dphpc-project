# Sysname : Linux
# Nodename: eu-a6-009-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec  8 19:50:40 2021
# Execution time and date (local): Wed Dec  8 20:50:40 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 10904156.475586            5 
            2          8192           MPI          1 10850897.378906           13 
            2          8192           MPI          2 10852079.442383            1 
            2          8192           MPI          3 10840950.116211            3 
            2          8192           MPI          4 10871866.274414            6 
            2          8192           MPI          5 10843718.564453            0 
            2          8192           MPI          6 10898921.312500            0 
            2          8192           MPI          7 10841406.334961            0 
            2          8192           MPI          8 10848489.307617            8 
            2          8192           MPI          9 10852202.895508            3 
# Runtime: 140.571264 s (overhead: 0.000028 %) 10 records
