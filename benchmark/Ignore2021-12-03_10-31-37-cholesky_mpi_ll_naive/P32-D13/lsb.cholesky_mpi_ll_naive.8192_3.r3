# Sysname : Linux
# Nodename: eu-a6-009-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec  8 19:50:40 2021
# Execution time and date (local): Wed Dec  8 20:50:40 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 10904257.520508            0 
            3          8192           MPI          1 10851004.192383            6 
            3          8192           MPI          2 10852181.160156            1 
            3          8192           MPI          3 10841058.960938            2 
            3          8192           MPI          4 10871968.797852           25 
            3          8192           MPI          5 10843824.554688            0 
            3          8192           MPI          6 10899029.936523            0 
            3          8192           MPI          7 10841511.908203            0 
            3          8192           MPI          8 10848596.311523            6 
            3          8192           MPI          9 10852312.975586            4 
# Runtime: 140.571413 s (overhead: 0.000031 %) 10 records
