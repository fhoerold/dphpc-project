# Sysname : Linux
# Nodename: eu-a6-012-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec  3 13:18:22 2021
# Execution time and date (local): Fri Dec  3 14:18:22 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 16642192.410156            1 
            3          8192           MPI          1 17939116.123047            5 
            3          8192           MPI          2 17957271.555664            1 
            3          8192           MPI          3 17978168.141602            0 
            3          8192           MPI          4 17864974.902344            6 
            3          8192           MPI          5 18049097.689453            0 
            3          8192           MPI          6 18008318.238281            0 
            3          8192           MPI          7 18010484.421875            0 
            3          8192           MPI          8 18024484.623047            7 
            3          8192           MPI          9 18010005.020508            0 
# Runtime: 213.397608 s (overhead: 0.000009 %) 10 records
