# Sysname : Linux
# Nodename: eu-a6-012-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec  3 13:18:22 2021
# Execution time and date (local): Fri Dec  3 14:18:22 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 16625665.155273            0 
            2          8192           MPI          1 17921476.193359            6 
            2          8192           MPI          2 17939870.746094            1 
            2          8192           MPI          3 17960750.046875            0 
            2          8192           MPI          4 17847670.596680            8 
            2          8192           MPI          5 18031604.529297            0 
            2          8192           MPI          6 17990869.498047            0 
            2          8192           MPI          7 17993035.413086            0 
            2          8192           MPI          8 18007022.054688            7 
            2          8192           MPI          9 17992558.298828            0 
# Runtime: 219.181209 s (overhead: 0.000010 %) 10 records
