# Sysname : Linux
# Nodename: eu-a6-005-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec  3 23:16:14 2021
# Execution time and date (local): Sat Dec  4 00:16:14 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 10372969.665039            0 
            3          8192           MPI          1 10454186.809570            1 
            3          8192           MPI          2 10363005.370117            3 
            3          8192           MPI          3 10506445.800781            0 
            3          8192           MPI          4 10491779.240234            8 
            3          8192           MPI          5 10421901.550781            0 
            3          8192           MPI          6 10390093.816406            0 
            3          8192           MPI          7 10595869.074219            0 
            3          8192           MPI          8 10391567.911133            7 
            3          8192           MPI          9 10403013.186523            0 
# Runtime: 131.053473 s (overhead: 0.000014 %) 10 records
