# Sysname : Linux
# Nodename: eu-a6-005-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec  3 23:16:14 2021
# Execution time and date (local): Sat Dec  4 00:16:14 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 10368916.936523            0 
            2          8192           MPI          1 10450487.566406            5 
            2          8192           MPI          2 10359316.530273            2 
            2          8192           MPI          3 10502724.297852            0 
            2          8192           MPI          4 10488061.853516            1 
            2          8192           MPI          5 10418209.187500            0 
            2          8192           MPI          6 10386410.423828            0 
            2          8192           MPI          7 10592120.743164            1 
            2          8192           MPI          8 10387871.229492            8 
            2          8192           MPI          9 10399330.387695            0 
# Runtime: 134.001195 s (overhead: 0.000013 %) 10 records
