# Sysname : Linux
# Nodename: eu-a6-008-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 20:22:01 2022
# Execution time and date (local): Wed Jan 12 21:22:01 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          5161             1           MPI          0 181371713.888672            2 
            2          5161             1           MPI          1 186484204.441406            7 
            2          5161             1           MPI          2 188799787.994141            4 
            2          5161             1           MPI          3 210661446.427734            0 
            2          5161             1           MPI          4 206407149.732422            6 
            2          5161             1           MPI          5 164297488.281250            2 
            2          5161             1           MPI          6 216069937.089844            2 
            2          5161             1           MPI          7 179584442.730469            2 
            2          5161             1           MPI          8 219998919.912109            7 
            2          5161             1           MPI          9 157612857.632812            2 
# Runtime: 2336.990053 s (overhead: 0.000001 %) 10 records
