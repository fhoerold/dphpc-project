# Sysname : Linux
# Nodename: eu-a6-008-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 20:22:01 2022
# Execution time and date (local): Wed Jan 12 21:22:01 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          5161             1           MPI          0 181372052.177734            0 
            3          5161             1           MPI          1 186484607.232422            3 
            3          5161             1           MPI          2 188800186.027344            3 
            3          5161             1           MPI          3 210661899.341797            0 
            3          5161             1           MPI          4 206407589.437500            3 
            3          5161             1           MPI          5 164297843.115234            0 
            3          5161             1           MPI          6 216070396.974609            0 
            3          5161             1           MPI          7 179584829.951172            0 
            3          5161             1           MPI          8 219999388.955078            4 
            3          5161             1           MPI          9 157613196.689453            0 
# Runtime: 2336.995670 s (overhead: 0.000001 %) 10 records
