# Sysname : Linux
# Nodename: eu-a6-009-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 22:23:15 2022
# Execution time and date (local): Wed Jan 12 23:23:15 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          6502          6502   gramschmidt           MPI          0 98148555.380859            0 
            2          6502          6502   gramschmidt           MPI          1 98158910.054688            4 
            2          6502          6502   gramschmidt           MPI          2 98179636.291016            4 
            2          6502          6502   gramschmidt           MPI          3 98301529.925781            0 
            2          6502          6502   gramschmidt           MPI          4 98436202.185547            4 
            2          6502          6502   gramschmidt           MPI          5 98189662.820312            3 
            2          6502          6502   gramschmidt           MPI          6 97839431.035156            3 
            2          6502          6502   gramschmidt           MPI          7 98234120.316406            3 
            2          6502          6502   gramschmidt           MPI          8 98282883.970703            7 
            2          6502          6502   gramschmidt           MPI          9 98121155.320312            4 
# Runtime: 1283.140291 s (overhead: 0.000002 %) 10 records
