# Sysname : Linux
# Nodename: eu-a6-009-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 22:23:15 2022
# Execution time and date (local): Wed Jan 12 23:23:15 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          6502          6502   gramschmidt           MPI          0 98035797.009766            0 
            3          6502          6502   gramschmidt           MPI          1 98046091.603516            4 
            3          6502          6502   gramschmidt           MPI          2 98067005.156250            4 
            3          6502          6502   gramschmidt           MPI          3 98188718.732422            0 
            3          6502          6502   gramschmidt           MPI          4 98324133.951172            5 
            3          6502          6502   gramschmidt           MPI          5 98078207.568359            1 
            3          6502          6502   gramschmidt           MPI          6 97728215.566406            2 
            3          6502          6502   gramschmidt           MPI          7 98122527.699219            2 
            3          6502          6502   gramschmidt           MPI          8 98171138.316406            6 
            3          6502          6502   gramschmidt           MPI          9 98009680.988281            0 
# Runtime: 1288.580227 s (overhead: 0.000002 %) 10 records
