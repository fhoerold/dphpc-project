# Sysname : Linux
# Nodename: eu-a6-008-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 15:03:20 2022
# Execution time and date (local): Mon Jan 10 16:03:20 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 7696857.681641            0 
         1024          1024   gramschmidt          1 662990.615234            4 
         1024          1024   gramschmidt          2 805507.384766            2 
         1024          1024   gramschmidt          3 715378.648438            0 
         1024          1024   gramschmidt          4 616500.281250            2 
         1024          1024   gramschmidt          5 660881.992188            0 
         1024          1024   gramschmidt          6 693308.667969            1 
         1024          1024   gramschmidt          7 663502.773438            0 
         1024          1024   gramschmidt          8 543940.494141            2 
         1024          1024   gramschmidt          9 533533.812500            0 
# Runtime: 13.592454 s (overhead: 0.000081 %) 10 records
