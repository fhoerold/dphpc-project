# Sysname : Linux
# Nodename: eu-a6-008-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 15:03:20 2022
# Execution time and date (local): Mon Jan 10 16:03:20 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 6688864.105469            0 
         1024          1024   gramschmidt          1 662978.542969            5 
         1024          1024   gramschmidt          2 805319.943359            5 
         1024          1024   gramschmidt          3 715207.931641            0 
         1024          1024   gramschmidt          4 616397.962891            2 
         1024          1024   gramschmidt          5 660771.527344            0 
         1024          1024   gramschmidt          6 693193.843750            0 
         1024          1024   gramschmidt          7 663499.462891            0 
         1024          1024   gramschmidt          8 543731.289062            2 
         1024          1024   gramschmidt          9 533441.490234            0 
# Runtime: 12.583456 s (overhead: 0.000111 %) 10 records
