# Sysname : Linux
# Nodename: eu-a6-006-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan 13 14:47:22 2022
# Execution time and date (local): Thu Jan 13 15:47:22 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192            16           MPI          0 17346671.603516            0 
            2          8192            16           MPI          1 16795792.496094            8 
            2          8192            16           MPI          2 16805637.179688            1 
            2          8192            16           MPI          3 16875596.343750            0 
            2          8192            16           MPI          4 16729971.083984            6 
# Runtime: 126.557909 s (overhead: 0.000012 %) 5 records
