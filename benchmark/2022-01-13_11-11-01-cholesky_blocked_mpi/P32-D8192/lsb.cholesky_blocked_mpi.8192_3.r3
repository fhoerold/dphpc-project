# Sysname : Linux
# Nodename: eu-a6-006-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan 13 14:47:22 2022
# Execution time and date (local): Thu Jan 13 15:47:22 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192            16           MPI          0 17344210.703125            0 
            3          8192            16           MPI          1 16793408.933594            7 
            3          8192            16           MPI          2 16803251.544922            2 
            3          8192            16           MPI          3 16873204.240234            4 
            3          8192            16           MPI          4 16727590.240234            9 
# Runtime: 132.542133 s (overhead: 0.000017 %) 5 records
