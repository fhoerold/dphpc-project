Invocation: ./benchmark.sh -o ../benchmark -d 5 6 7 8 9 10 11 12 13 --sequential --time 12:00 ./build/modified_gram
Benchmark 'modified_gram' for
> Dimensions:		32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 
> Number of processors:	1, 
> CPU Model:		XeonGold_6150
> Use MPI:		false
> Use OpenMP:		false

Timestamp: 2021-12-21_15-37-26
