# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:21:30 2022
# Execution time and date (local): Fri Jan  7 17:21:30 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048             4           MPI          0 1231558.083984            0 
            3          2048             4           MPI          1 1202720.431641            5 
            3          2048             4           MPI          2 1236170.884766            7 
            3          2048             4           MPI          3 1212134.496094            0 
            3          2048             4           MPI          4 1123736.099609            6 
# Runtime: 8.655434 s (overhead: 0.000208 %) 5 records
