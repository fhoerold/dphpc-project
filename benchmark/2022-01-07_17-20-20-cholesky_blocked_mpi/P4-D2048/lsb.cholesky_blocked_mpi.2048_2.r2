# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:21:30 2022
# Execution time and date (local): Fri Jan  7 17:21:30 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048             4           MPI          0 1231548.367188            0 
            2          2048             4           MPI          1 1202728.910156            1 
            2          2048             4           MPI          2 1236178.566406            2 
            2          2048             4           MPI          3 1212141.960938            0 
            2          2048             4           MPI          4 1123743.417969            2 
# Runtime: 12.658040 s (overhead: 0.000040 %) 5 records
