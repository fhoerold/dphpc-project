# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:23:04 2022
# Execution time and date (local): Fri Jan  7 17:23:04 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024             4           MPI          0 79885.929688            0 
            2          1024             4           MPI          1 80749.845703            1 
            2          1024             4           MPI          2 81452.437500            1 
            2          1024             4           MPI          3 79766.734375            0 
            2          1024             4           MPI          4 81678.107422            1 
# Runtime: 0.568902 s (overhead: 0.000527 %) 5 records
