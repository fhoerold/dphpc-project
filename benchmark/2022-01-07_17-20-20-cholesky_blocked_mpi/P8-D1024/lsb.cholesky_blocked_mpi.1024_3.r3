# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:23:04 2022
# Execution time and date (local): Fri Jan  7 17:23:04 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024             4           MPI          0 80236.619141            0 
            3          1024             4           MPI          1 80954.423828            1 
            3          1024             4           MPI          2 81480.503906            1 
            3          1024             4           MPI          3 79795.746094            0 
            3          1024             4           MPI          4 81708.294922            1 
# Runtime: 7.567407 s (overhead: 0.000040 %) 5 records
