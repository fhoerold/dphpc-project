# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:22:22 2022
# Execution time and date (local): Fri Jan  7 17:22:22 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           256             4           MPI          0 2516.845703            0 
            2           256             4           MPI          1 2862.673828            0 
            2           256             4           MPI          2 2480.724609            0 
            2           256             4           MPI          3 2499.041016            0 
            2           256             4           MPI          4 2492.371094            1 
# Runtime: 3.015759 s (overhead: 0.000033 %) 5 records
