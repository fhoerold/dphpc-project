# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:22:22 2022
# Execution time and date (local): Fri Jan  7 17:22:22 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           256             4           MPI          0 2487.974609            0 
            3           256             4           MPI          1 2485.468750            1 
            3           256             4           MPI          2 2480.492188            1 
            3           256             4           MPI          3 2499.962891            0 
            3           256             4           MPI          4 2492.779297            0 
# Runtime: 2.017841 s (overhead: 0.000099 %) 5 records
