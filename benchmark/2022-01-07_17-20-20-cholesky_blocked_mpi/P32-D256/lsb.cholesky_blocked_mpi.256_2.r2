# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:17:00 2022
# Execution time and date (local): Sat Jan  8 05:17:00 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           256             4           MPI          0 5482.527344            0 
            2           256             4           MPI          1 4135.414062            0 
            2           256             4           MPI          2 3661.306641            0 
            2           256             4           MPI          3 3420.138672            0 
            2           256             4           MPI          4 3412.814453            0 
# Runtime: 16.034205 s (overhead: 0.000000 %) 5 records
