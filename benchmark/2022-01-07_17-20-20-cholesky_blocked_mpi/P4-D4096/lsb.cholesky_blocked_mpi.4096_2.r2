# Sysname : Linux
# Nodename: eu-a6-011-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:41:17 2022
# Execution time and date (local): Fri Jan  7 17:41:17 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096             4           MPI          0 10600864.046875            0 
            2          4096             4           MPI          1 10050496.775391            6 
            2          4096             4           MPI          2 10609243.777344            1 
            2          4096             4           MPI          3 10491099.082031            0 
            2          4096             4           MPI          4 10280602.750000            8 
# Runtime: 76.040274 s (overhead: 0.000020 %) 5 records
