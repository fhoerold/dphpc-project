# Sysname : Linux
# Nodename: eu-a6-011-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:41:17 2022
# Execution time and date (local): Fri Jan  7 17:41:17 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096             4           MPI          0 10599713.140625            0 
            3          4096             4           MPI          1 10049404.025391            5 
            3          4096             4           MPI          2 10608092.347656            1 
            3          4096             4           MPI          3 10489966.501953            3 
            3          4096             4           MPI          4 10279482.181641            8 
# Runtime: 84.031923 s (overhead: 0.000020 %) 5 records
