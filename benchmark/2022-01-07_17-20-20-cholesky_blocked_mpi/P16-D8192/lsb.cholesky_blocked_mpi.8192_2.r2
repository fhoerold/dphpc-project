# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 00:09:36 2022
# Execution time and date (local): Sat Jan  8 01:09:36 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192             4           MPI          0 46657492.849609            0 
            2          8192             4           MPI          1 51115766.009766            3 
            2          8192             4           MPI          2 48464589.847656            6 
            2          8192             4           MPI          3 50159398.703125            3 
            2          8192             4           MPI          4 54044635.210938            7 
# Runtime: 362.222591 s (overhead: 0.000005 %) 5 records
