# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:22:33 2022
# Execution time and date (local): Fri Jan  7 17:22:33 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512             4           MPI          0 19067.728516            0 
            2           512             4           MPI          1 19012.312500            2 
            2           512             4           MPI          2 19011.796875            2 
            2           512             4           MPI          3 18757.634766            0 
            2           512             4           MPI          4 18764.367188            2 
# Runtime: 2.130993 s (overhead: 0.000282 %) 5 records
