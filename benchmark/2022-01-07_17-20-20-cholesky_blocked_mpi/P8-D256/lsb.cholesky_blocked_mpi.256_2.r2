# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:21:01 2022
# Execution time and date (local): Fri Jan  7 17:21:01 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           256             4           MPI          0 2365.666016            0 
            2           256             4           MPI          1 2410.875000            0 
            2           256             4           MPI          2 2282.511719            0 
            2           256             4           MPI          3 2339.480469            0 
            2           256             4           MPI          4 2360.574219            1 
# Runtime: 0.019582 s (overhead: 0.005107 %) 5 records
