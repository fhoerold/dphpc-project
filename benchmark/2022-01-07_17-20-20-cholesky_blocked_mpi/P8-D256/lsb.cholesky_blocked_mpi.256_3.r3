# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:21:01 2022
# Execution time and date (local): Fri Jan  7 17:21:01 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           256             4           MPI          0 2692.154297            0 
            3           256             4           MPI          1 2414.501953            0 
            3           256             4           MPI          2 2285.884766            0 
            3           256             4           MPI          3 2340.025391            0 
            3           256             4           MPI          4 2360.781250            0 
# Runtime: 4.020738 s (overhead: 0.000000 %) 5 records
