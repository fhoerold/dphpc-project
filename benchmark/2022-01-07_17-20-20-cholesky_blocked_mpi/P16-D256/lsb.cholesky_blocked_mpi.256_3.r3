# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:33:27 2022
# Execution time and date (local): Fri Jan  7 22:33:27 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           256             4           MPI          0 2442.757812            0 
            3           256             4           MPI          1 2497.181641            0 
            3           256             4           MPI          2 2298.603516            0 
            3           256             4           MPI          3 2224.089844            0 
            3           256             4           MPI          4 2180.005859            0 
# Runtime: 5.016659 s (overhead: 0.000000 %) 5 records
