# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:22:48 2022
# Execution time and date (local): Fri Jan  7 17:22:48 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024             4           MPI          0 153855.476562            0 
            3          1024             4           MPI          1 150488.867188            2 
            3          1024             4           MPI          2 153571.396484            2 
            3          1024             4           MPI          3 152392.021484            0 
            3          1024             4           MPI          4 156064.677734            2 
# Runtime: 2.085603 s (overhead: 0.000288 %) 5 records
