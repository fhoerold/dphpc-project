# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:22:48 2022
# Execution time and date (local): Fri Jan  7 17:22:48 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024             4           MPI          0 153764.902344            0 
            2          1024             4           MPI          1 150712.414062            2 
            2          1024             4           MPI          2 153471.994141            2 
            2          1024             4           MPI          3 152292.726562            0 
            2          1024             4           MPI          4 155965.625000            3 
# Runtime: 5.077556 s (overhead: 0.000138 %) 5 records
