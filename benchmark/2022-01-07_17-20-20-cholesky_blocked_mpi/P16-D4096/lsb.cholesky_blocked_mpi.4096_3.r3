# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 20:54:21 2022
# Execution time and date (local): Fri Jan  7 21:54:21 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096             4           MPI          0 6469487.732422            0 
            3          4096             4           MPI          1 6401768.798828            2 
            3          4096             4           MPI          2 6356217.712891            1 
            3          4096             4           MPI          3 6404108.753906            0 
            3          4096             4           MPI          4 6549658.363281            1 
# Runtime: 45.463743 s (overhead: 0.000009 %) 5 records
