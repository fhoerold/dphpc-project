# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 20:54:21 2022
# Execution time and date (local): Fri Jan  7 21:54:21 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096             4           MPI          0 6469654.949219            0 
            2          4096             4           MPI          1 6401935.175781            1 
            2          4096             4           MPI          2 6356382.609375            1 
            2          4096             4           MPI          3 6404275.292969            0 
            2          4096             4           MPI          4 6549828.427734            1 
# Runtime: 45.463979 s (overhead: 0.000007 %) 5 records
