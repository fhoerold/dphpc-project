# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:34:47 2022
# Execution time and date (local): Fri Jan  7 22:34:47 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048             4           MPI          0 719004.074219            0 
            2          2048             4           MPI          1 717870.414062            1 
            2          2048             4           MPI          2 722502.648438            1 
            2          2048             4           MPI          3 719347.144531            0 
            2          2048             4           MPI          4 722834.164062            1 
# Runtime: 6.042360 s (overhead: 0.000050 %) 5 records
