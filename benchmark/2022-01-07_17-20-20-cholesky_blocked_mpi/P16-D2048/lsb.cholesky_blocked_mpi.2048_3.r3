# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:34:47 2022
# Execution time and date (local): Fri Jan  7 22:34:47 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048             4           MPI          0 719120.302734            0 
            3          2048             4           MPI          1 717967.955078            1 
            3          2048             4           MPI          2 722619.667969            1 
            3          2048             4           MPI          3 719463.667969            0 
            3          2048             4           MPI          4 722951.595703            1 
# Runtime: 5.053342 s (overhead: 0.000059 %) 5 records
