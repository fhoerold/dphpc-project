# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:44:58 2022
# Execution time and date (local): Sat Jan  8 05:44:58 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024             4           MPI          0 112202.300781            0 
            3          1024             4           MPI          1 111095.830078            1 
            3          1024             4           MPI          2 111502.289062            1 
            3          1024             4           MPI          3 111431.230469            0 
            3          1024             4           MPI          4 112504.455078            1 
# Runtime: 6.814152 s (overhead: 0.000044 %) 5 records
