# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:44:58 2022
# Execution time and date (local): Sat Jan  8 05:44:58 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024             4           MPI          0 112196.177734            0 
            2          1024             4           MPI          1 111094.669922            1 
            2          1024             4           MPI          2 111497.681641            1 
            2          1024             4           MPI          3 111425.761719            0 
            2          1024             4           MPI          4 112499.890625            1 
# Runtime: 5.816291 s (overhead: 0.000052 %) 5 records
