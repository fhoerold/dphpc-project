# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:24:09 2022
# Execution time and date (local): Fri Jan  7 17:24:09 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048             4           MPI          0 697719.968750            0 
            3          2048             4           MPI          1 726859.255859            1 
            3          2048             4           MPI          2 703400.533203            1 
            3          2048             4           MPI          3 715804.623047            0 
            3          2048             4           MPI          4 712051.445312            1 
# Runtime: 7.009816 s (overhead: 0.000043 %) 5 records
