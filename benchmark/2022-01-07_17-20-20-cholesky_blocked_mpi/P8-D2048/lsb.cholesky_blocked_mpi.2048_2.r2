# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:24:09 2022
# Execution time and date (local): Fri Jan  7 17:24:09 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048             4           MPI          0 698011.833984            0 
            2          2048             4           MPI          1 727164.896484            1 
            2          2048             4           MPI          2 703696.423828            1 
            2          2048             4           MPI          3 716213.888672            0 
            2          2048             4           MPI          4 712351.855469            1 
# Runtime: 5.019653 s (overhead: 0.000060 %) 5 records
