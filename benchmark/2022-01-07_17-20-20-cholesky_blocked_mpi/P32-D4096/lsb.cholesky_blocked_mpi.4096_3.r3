# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:55:20 2022
# Execution time and date (local): Sat Jan  8 05:55:20 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096             4           MPI          0 7054136.792969            0 
            3          4096             4           MPI          1 7023822.378906            7 
            3          4096             4           MPI          2 7062701.556641            1 
            3          4096             4           MPI          3 6987660.164062            1 
            3          4096             4           MPI          4 7018917.089844            1 
# Runtime: 61.398879 s (overhead: 0.000016 %) 5 records
