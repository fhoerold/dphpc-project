# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:22:48 2022
# Execution time and date (local): Fri Jan  7 17:22:48 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512             4           MPI          0 14462.841797            0 
            3           512             4           MPI          1 14738.808594            0 
            3           512             4           MPI          2 14397.472656            1 
            3           512             4           MPI          3 14306.013672            0 
            3           512             4           MPI          4 14329.468750            0 
# Runtime: 2.112216 s (overhead: 0.000047 %) 5 records
