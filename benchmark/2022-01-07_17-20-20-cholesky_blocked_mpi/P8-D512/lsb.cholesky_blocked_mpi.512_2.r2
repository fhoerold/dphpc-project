# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:22:48 2022
# Execution time and date (local): Fri Jan  7 17:22:48 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512             4           MPI          0 14428.162109            0 
            2           512             4           MPI          1 14474.390625            1 
            2           512             4           MPI          2 14362.701172            1 
            2           512             4           MPI          3 14273.009766            0 
            2           512             4           MPI          4 14280.730469            1 
# Runtime: 2.107178 s (overhead: 0.000142 %) 5 records
