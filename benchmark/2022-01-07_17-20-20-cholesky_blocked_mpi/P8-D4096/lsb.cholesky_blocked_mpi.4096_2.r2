# Sysname : Linux
# Nodename: eu-a6-009-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:41:07 2022
# Execution time and date (local): Fri Jan  7 17:41:07 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096             4           MPI          0 8221657.257812            0 
            2          4096             4           MPI          1 8899623.031250           10 
            2          4096             4           MPI          2 8391514.677734            8 
            2          4096             4           MPI          3 8824766.595703            0 
            2          4096             4           MPI          4 8923897.847656            2 
# Runtime: 71.222006 s (overhead: 0.000028 %) 5 records
