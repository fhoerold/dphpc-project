# Sysname : Linux
# Nodename: eu-a6-009-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:41:07 2022
# Execution time and date (local): Fri Jan  7 17:41:07 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096             4           MPI          0 8221101.591797            0 
            3          4096             4           MPI          1 8899019.089844            1 
            3          4096             4           MPI          2 8390955.857422            2 
            3          4096             4           MPI          3 8824184.632812            0 
            3          4096             4           MPI          4 8923306.238281            7 
# Runtime: 72.216235 s (overhead: 0.000014 %) 5 records
