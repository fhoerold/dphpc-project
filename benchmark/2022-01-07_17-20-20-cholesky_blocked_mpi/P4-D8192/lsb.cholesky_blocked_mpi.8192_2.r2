# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:24:25 2022
# Execution time and date (local): Fri Jan  7 19:24:25 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192             4           MPI          0 53928587.169922            3 
            2          8192             4           MPI          1 53864683.503906            7 
            2          8192             4           MPI          2 58582382.162109            6 
            2          8192             4           MPI          3 54880512.382812            4 
            2          8192             4           MPI          4 57385474.689453            5 
# Runtime: 420.321616 s (overhead: 0.000006 %) 5 records
