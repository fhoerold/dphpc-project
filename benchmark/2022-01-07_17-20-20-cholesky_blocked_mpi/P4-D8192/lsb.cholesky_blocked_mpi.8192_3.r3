# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:24:25 2022
# Execution time and date (local): Fri Jan  7 19:24:25 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192             4           MPI          0 53924579.417969            0 
            3          8192             4           MPI          1 53860678.292969            8 
            3          8192             4           MPI          2 58578026.689453            6 
            3          8192             4           MPI          3 54876432.265625            3 
            3          8192             4           MPI          4 57381205.457031            5 
# Runtime: 424.289966 s (overhead: 0.000005 %) 5 records
