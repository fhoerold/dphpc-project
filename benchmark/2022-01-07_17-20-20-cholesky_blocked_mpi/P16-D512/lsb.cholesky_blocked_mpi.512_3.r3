# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 20:44:20 2022
# Execution time and date (local): Fri Jan  7 21:44:20 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512             4           MPI          0 19928.552734            0 
            3           512             4           MPI          1 17309.632812            1 
            3           512             4           MPI          2 16979.796875            1 
            3           512             4           MPI          3 17288.714844            0 
            3           512             4           MPI          4 17031.259766            1 
# Runtime: 23.129973 s (overhead: 0.000013 %) 5 records
