# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 20:44:20 2022
# Execution time and date (local): Fri Jan  7 21:44:20 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512             4           MPI          0 19925.328125            0 
            2           512             4           MPI          1 17203.146484            1 
            2           512             4           MPI          2 16981.683594            1 
            2           512             4           MPI          3 17290.958984            0 
            2           512             4           MPI          4 17247.417969            1 
# Runtime: 23.131208 s (overhead: 0.000013 %) 5 records
