# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:33:43 2022
# Execution time and date (local): Fri Jan  7 22:33:43 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024             4           MPI          0 84733.259766            0 
            3          1024             4           MPI          1 82663.916016            1 
            3          1024             4           MPI          2 83965.007812            1 
            3          1024             4           MPI          3 83389.767578            0 
            3          1024             4           MPI          4 84159.779297            1 
# Runtime: 0.596191 s (overhead: 0.000503 %) 5 records
