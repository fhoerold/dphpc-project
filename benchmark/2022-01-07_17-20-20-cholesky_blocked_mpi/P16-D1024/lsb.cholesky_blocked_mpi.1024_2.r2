# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:33:43 2022
# Execution time and date (local): Fri Jan  7 22:33:43 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024             4           MPI          0 84747.443359            0 
            2          1024             4           MPI          1 82676.523438            1 
            2          1024             4           MPI          2 83977.958984            1 
            2          1024             4           MPI          3 83476.681641            0 
            2          1024             4           MPI          4 84174.007812            2 
# Runtime: 3.600651 s (overhead: 0.000111 %) 5 records
