# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:17:27 2022
# Execution time and date (local): Sat Jan  8 05:17:27 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512             4           MPI          0 24039.923828            0 
            3           512             4           MPI          1 23024.056641            1 
            3           512             4           MPI          2 23013.539062            1 
            3           512             4           MPI          3 22878.435547            0 
            3           512             4           MPI          4 22918.689453            2 
# Runtime: 6.179412 s (overhead: 0.000065 %) 5 records
