# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 09:13:36 2022
# Execution time and date (local): Sat Jan  8 10:13:36 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192             4           MPI          0 53212896.746094            0 
            3          8192             4           MPI          1 54210901.820312            5 
            3          8192             4           MPI          2 53186483.935547            6 
            3          8192             4           MPI          3 53816981.007812            0 
            3          8192             4           MPI          4 53589237.693359            7 
# Runtime: 385.280656 s (overhead: 0.000005 %) 5 records
