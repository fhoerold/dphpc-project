# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 09:13:36 2022
# Execution time and date (local): Sat Jan  8 10:13:36 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192             4           MPI          0 53214062.109375            0 
            2          8192             4           MPI          1 54212086.517578            6 
            2          8192             4           MPI          2 53187645.843750            4 
            2          8192             4           MPI          3 53818157.404297            0 
            2          8192             4           MPI          4 53590409.917969            6 
# Runtime: 385.287569 s (overhead: 0.000004 %) 5 records
