# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:46:01 2022
# Execution time and date (local): Sat Jan  8 05:46:01 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048             4           MPI          0 744908.707031            0 
            2          2048             4           MPI          1 747220.216797            1 
            2          2048             4           MPI          2 747785.074219            1 
            2          2048             4           MPI          3 745271.511719            0 
            2          2048             4           MPI          4 746084.080078            0 
# Runtime: 14.245422 s (overhead: 0.000014 %) 5 records
