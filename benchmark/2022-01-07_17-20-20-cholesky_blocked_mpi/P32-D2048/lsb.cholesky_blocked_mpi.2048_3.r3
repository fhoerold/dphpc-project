# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:46:01 2022
# Execution time and date (local): Sat Jan  8 05:46:01 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048             4           MPI          0 744854.027344            0 
            3          2048             4           MPI          1 747165.708984            1 
            3          2048             4           MPI          2 747730.166016            1 
            3          2048             4           MPI          3 745216.955078            0 
            3          2048             4           MPI          4 746029.968750            1 
# Runtime: 17.235926 s (overhead: 0.000017 %) 5 records
