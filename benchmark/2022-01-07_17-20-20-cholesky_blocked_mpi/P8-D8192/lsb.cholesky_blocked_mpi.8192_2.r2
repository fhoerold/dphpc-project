# Sysname : Linux
# Nodename: eu-a6-008-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:30:02 2022
# Execution time and date (local): Fri Jan  7 19:30:02 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192             4           MPI          0 47924538.667969            0 
            2          8192             4           MPI          1 46117656.693359            7 
            2          8192             4           MPI          2 48152882.888672            7 
            2          8192             4           MPI          3 45627440.160156            0 
            2          8192             4           MPI          4 47181782.140625            7 
# Runtime: 357.660900 s (overhead: 0.000006 %) 5 records
