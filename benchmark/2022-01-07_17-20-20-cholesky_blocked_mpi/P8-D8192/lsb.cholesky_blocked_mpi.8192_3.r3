# Sysname : Linux
# Nodename: eu-a6-008-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:30:02 2022
# Execution time and date (local): Fri Jan  7 19:30:02 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192             4           MPI          0 47987188.142578            1 
            3          8192             4           MPI          1 46177935.716797            8 
            3          8192             4           MPI          2 48215828.119141            4 
            3          8192             4           MPI          3 45687080.353516            4 
            3          8192             4           MPI          4 47243452.167969            6 
# Runtime: 346.132112 s (overhead: 0.000007 %) 5 records
