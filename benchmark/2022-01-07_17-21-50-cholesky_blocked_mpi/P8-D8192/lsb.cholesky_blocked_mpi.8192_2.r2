# Sysname : Linux
# Nodename: eu-a6-011-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:51:13 2022
# Execution time and date (local): Fri Jan  7 22:51:13 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192            32           MPI          0 18874176.455078            0 
            2          8192            32           MPI          1 18790893.601562            2 
            2          8192            32           MPI          2 18826920.083984            3 
            2          8192            32           MPI          3 18846029.425781            1 
            2          8192            32           MPI          4 18812553.423828            1 
# Runtime: 146.432975 s (overhead: 0.000005 %) 5 records
