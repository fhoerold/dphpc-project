# Sysname : Linux
# Nodename: eu-a6-011-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:51:13 2022
# Execution time and date (local): Fri Jan  7 22:51:13 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192            32           MPI          0 18890185.781250            0 
            3          8192            32           MPI          1 18807090.777344            1 
            3          8192            32           MPI          2 18843189.634766            4 
            3          8192            32           MPI          3 18862594.978516            0 
            3          8192            32           MPI          4 18828514.103516            6 
# Runtime: 139.555713 s (overhead: 0.000008 %) 5 records
