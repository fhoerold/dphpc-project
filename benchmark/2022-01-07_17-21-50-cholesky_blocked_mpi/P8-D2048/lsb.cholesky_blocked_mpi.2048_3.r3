# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:30:37 2022
# Execution time and date (local): Fri Jan  7 17:30:37 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048            32           MPI          0 301582.882812            0 
            3          2048            32           MPI          1 294856.718750            1 
            3          2048            32           MPI          2 284588.916016            1 
            3          2048            32           MPI          3 298562.576172            0 
            3          2048            32           MPI          4 288191.625000            1 
# Runtime: 2.145765 s (overhead: 0.000140 %) 5 records
