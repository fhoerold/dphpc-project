# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:30:37 2022
# Execution time and date (local): Fri Jan  7 17:30:37 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048            32           MPI          0 301592.189453            0 
            2          2048            32           MPI          1 294837.849609            1 
            2          2048            32           MPI          2 284434.160156            1 
            2          2048            32           MPI          3 298548.548828            0 
            2          2048            32           MPI          4 288193.634766            1 
# Runtime: 2.144244 s (overhead: 0.000140 %) 5 records
