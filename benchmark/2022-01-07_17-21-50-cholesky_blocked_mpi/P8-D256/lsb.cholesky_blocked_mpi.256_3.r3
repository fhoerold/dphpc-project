# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:29:30 2022
# Execution time and date (local): Fri Jan  7 17:29:30 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           256            32           MPI          0 1757.041016            0 
            3           256            32           MPI          1 1385.541016            0 
            3           256            32           MPI          2 1240.632812            0 
            3           256            32           MPI          3 1311.005859            0 
            3           256            32           MPI          4 1171.945312            0 
# Runtime: 2.021535 s (overhead: 0.000000 %) 5 records
