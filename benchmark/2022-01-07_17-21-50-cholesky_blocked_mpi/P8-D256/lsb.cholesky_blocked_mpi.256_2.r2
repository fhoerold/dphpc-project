# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:29:30 2022
# Execution time and date (local): Fri Jan  7 17:29:30 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           256            32           MPI          0 1355.896484            0 
            2           256            32           MPI          1 1331.359375            0 
            2           256            32           MPI          2 1246.023438            0 
            2           256            32           MPI          3 1199.072266            0 
            2           256            32           MPI          4 1176.451172            0 
# Runtime: 3.018405 s (overhead: 0.000000 %) 5 records
