# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 15:29:21 2022
# Execution time and date (local): Sat Jan  8 16:29:21 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192            32           MPI          0 10569147.871094            0 
            3          8192            32           MPI          1 10424945.148438            6 
            3          8192            32           MPI          2 10058892.607422            5 
            3          8192            32           MPI          3 10239184.855469            0 
            3          8192            32           MPI          4 10007925.681641            5 
# Runtime: 86.079023 s (overhead: 0.000019 %) 5 records
