# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 15:29:21 2022
# Execution time and date (local): Sat Jan  8 16:29:21 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192            32           MPI          0 10575115.410156            0 
            2          8192            32           MPI          1 10430827.457031            6 
            2          8192            32           MPI          2 10064573.332031            1 
            2          8192            32           MPI          3 10244968.105469            0 
            2          8192            32           MPI          4 10013576.746094            5 
# Runtime: 79.135432 s (overhead: 0.000015 %) 5 records
