# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:18:58 2022
# Execution time and date (local): Fri Jan  7 22:18:58 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096            32           MPI          0 1711649.470703            0 
            2          4096            32           MPI          1 1646726.707031            2 
            2          4096            32           MPI          2 1652223.416016            1 
            2          4096            32           MPI          3 1677421.472656            0 
            2          4096            32           MPI          4 1635802.277344            1 
# Runtime: 26.368085 s (overhead: 0.000015 %) 5 records
