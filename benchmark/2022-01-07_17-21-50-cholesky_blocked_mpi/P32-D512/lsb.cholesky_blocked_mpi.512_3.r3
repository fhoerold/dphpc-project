# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:20:09 2022
# Execution time and date (local): Sat Jan  8 05:20:09 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512            32           MPI          0 9134.960938            0 
            3           512            32           MPI          1 8900.230469            0 
            3           512            32           MPI          2 8525.226562            0 
            3           512            32           MPI          3 8727.146484            0 
            3           512            32           MPI          4 8760.117188            0 
# Runtime: 2.084883 s (overhead: 0.000000 %) 5 records
