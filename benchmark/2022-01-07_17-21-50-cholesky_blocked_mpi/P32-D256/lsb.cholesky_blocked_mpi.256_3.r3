# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:19:53 2022
# Execution time and date (local): Sat Jan  8 05:19:53 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           256            32           MPI          0 1873.580078            0 
            3           256            32           MPI          1 1795.689453            0 
            3           256            32           MPI          2 1699.585938            0 
            3           256            32           MPI          3 1665.251953            0 
            3           256            32           MPI          4 1581.335938            0 
# Runtime: 5.020828 s (overhead: 0.000000 %) 5 records
