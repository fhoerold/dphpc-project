# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 11:38:37 2022
# Execution time and date (local): Sat Jan  8 12:38:37 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096            32           MPI          0 1657738.667969            0 
            2          4096            32           MPI          1 1572238.332031            1 
            2          4096            32           MPI          2 1472769.431641            1 
            2          4096            32           MPI          3 1478451.935547            0 
            2          4096            32           MPI          4 1603428.347656            1 
# Runtime: 12.297881 s (overhead: 0.000024 %) 5 records
