# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 11:38:37 2022
# Execution time and date (local): Sat Jan  8 12:38:37 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096            32           MPI          0 1657859.144531            0 
            3          4096            32           MPI          1 1572329.609375            1 
            3          4096            32           MPI          2 1472873.687500            1 
            3          4096            32           MPI          3 1478557.318359            0 
            3          4096            32           MPI          4 1603175.904297            1 
# Runtime: 11.283864 s (overhead: 0.000027 %) 5 records
