# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:41:15 2022
# Execution time and date (local): Fri Jan  7 17:41:15 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           256            32           MPI          0 1414.658203            0 
            3           256            32           MPI          1 1707.625000            0 
            3           256            32           MPI          2 1364.000000            0 
            3           256            32           MPI          3 1305.787109            0 
            3           256            32           MPI          4 1325.640625            0 
# Runtime: 0.015293 s (overhead: 0.000000 %) 5 records
