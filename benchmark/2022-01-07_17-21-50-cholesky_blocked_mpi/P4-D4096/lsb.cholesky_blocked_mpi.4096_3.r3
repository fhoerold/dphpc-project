# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:36:48 2022
# Execution time and date (local): Fri Jan  7 17:36:48 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096            32           MPI          0 3828620.679688            0 
            3          4096            32           MPI          1 3828456.015625            1 
            3          4096            32           MPI          2 3829692.818359            1 
            3          4096            32           MPI          3 3826954.656250            0 
            3          4096            32           MPI          4 3825707.447266            1 
# Runtime: 27.342529 s (overhead: 0.000011 %) 5 records
