# Sysname : Linux
# Nodename: eu-a6-011-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 19:23:23 2022
# Execution time and date (local): Fri Jan  7 20:23:23 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096            32           MPI          0 2588679.587891            0 
            2          4096            32           MPI          1 2595226.750000            2 
            2          4096            32           MPI          2 2581697.726562            2 
            2          4096            32           MPI          3 2603700.589844            0 
            2          4096            32           MPI          4 2633943.148438            2 
# Runtime: 24.961789 s (overhead: 0.000024 %) 5 records
