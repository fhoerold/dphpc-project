# Sysname : Linux
# Nodename: eu-a6-011-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 19:23:23 2022
# Execution time and date (local): Fri Jan  7 20:23:23 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096            32           MPI          0 2588745.937500            0 
            3          4096            32           MPI          1 2595345.203125            2 
            3          4096            32           MPI          2 2581792.560547            1 
            3          4096            32           MPI          3 2603816.277344            0 
            3          4096            32           MPI          4 2634052.427734            1 
# Runtime: 24.961416 s (overhead: 0.000016 %) 5 records
