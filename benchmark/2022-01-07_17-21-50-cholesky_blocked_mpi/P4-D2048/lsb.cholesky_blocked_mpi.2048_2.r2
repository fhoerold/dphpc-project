# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:42:47 2022
# Execution time and date (local): Fri Jan  7 17:42:47 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048            32           MPI          0 580254.279297            0 
            2          2048            32           MPI          1 585469.927734            3 
            2          2048            32           MPI          2 581647.416016            3 
            2          2048            32           MPI          3 581900.503906            1 
            2          2048            32           MPI          4 578877.833984            3 
# Runtime: 16.209717 s (overhead: 0.000062 %) 5 records
