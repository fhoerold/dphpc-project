# Sysname : Linux
# Nodename: eu-a6-006-16
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 20:09:00 2022
# Execution time and date (local): Fri Jan  7 21:09:00 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192            32           MPI          0 31797936.980469            0 
            3          8192            32           MPI          1 31391268.285156            9 
            3          8192            32           MPI          2 31250457.994141            7 
            3          8192            32           MPI          3 31288964.050781            0 
            3          8192            32           MPI          4 31245239.890625            7 
# Runtime: 225.934790 s (overhead: 0.000010 %) 5 records
