# Sysname : Linux
# Nodename: eu-a6-006-16
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 20:09:00 2022
# Execution time and date (local): Fri Jan  7 21:09:00 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192            32           MPI          0 31759092.226562            1 
            2          8192            32           MPI          1 31352758.460938            6 
            2          8192            32           MPI          2 31212133.587891            7 
            2          8192            32           MPI          3 31250585.085938            0 
            2          8192            32           MPI          4 31206920.593750            8 
# Runtime: 229.646212 s (overhead: 0.000010 %) 5 records
