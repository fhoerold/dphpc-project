# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:34:56 2022
# Execution time and date (local): Fri Jan  7 19:34:56 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024            32           MPI          0 45188.695312            0 
            2          1024            32           MPI          1 47903.332031            7 
            2          1024            32           MPI          2 46790.322266            5 
            2          1024            32           MPI          3 46999.474609            0 
            2          1024            32           MPI          4 46086.164062            6 
# Runtime: 2.349127 s (overhead: 0.000766 %) 5 records
