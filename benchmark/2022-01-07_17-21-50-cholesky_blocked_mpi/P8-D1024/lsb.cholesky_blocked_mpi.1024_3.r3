# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:34:56 2022
# Execution time and date (local): Fri Jan  7 19:34:56 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024            32           MPI          0 45206.564453            0 
            3          1024            32           MPI          1 48022.046875            1 
            3          1024            32           MPI          2 46919.279297            1 
            3          1024            32           MPI          3 46952.134766            0 
            3          1024            32           MPI          4 46176.712891            1 
# Runtime: 8.357363 s (overhead: 0.000036 %) 5 records
