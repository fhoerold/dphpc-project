# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 02:18:55 2022
# Execution time and date (local): Sat Jan  8 03:18:55 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192            32           MPI          0 12257724.783203            0 
            2          8192            32           MPI          1 12168602.966797            1 
            2          8192            32           MPI          2 12216280.566406            4 
            2          8192            32           MPI          3 12302336.128906            0 
            2          8192            32           MPI          4 12350427.720703            1 
# Runtime: 99.354902 s (overhead: 0.000006 %) 5 records
