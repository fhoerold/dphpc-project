# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 02:18:55 2022
# Execution time and date (local): Sat Jan  8 03:18:55 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192            32           MPI          0 12257842.710938            0 
            3          8192            32           MPI          1 12168722.332031            1 
            3          8192            32           MPI          2 12216393.763672            1 
            3          8192            32           MPI          3 12302459.343750            3 
            3          8192            32           MPI          4 12350549.607422            8 
# Runtime: 91.353163 s (overhead: 0.000014 %) 5 records
