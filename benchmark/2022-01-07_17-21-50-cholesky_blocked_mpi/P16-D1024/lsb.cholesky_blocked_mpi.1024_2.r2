# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:08:09 2022
# Execution time and date (local): Fri Jan  7 22:08:09 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024            32           MPI          0 44162.960938            0 
            2          1024            32           MPI          1 43710.380859            2 
            2          1024            32           MPI          2 43286.138672            1 
            2          1024            32           MPI          3 43953.101562            0 
            2          1024            32           MPI          4 43377.814453            1 
# Runtime: 3.337171 s (overhead: 0.000120 %) 5 records
