# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:08:09 2022
# Execution time and date (local): Fri Jan  7 22:08:09 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024            32           MPI          0 44136.189453            0 
            3          1024            32           MPI          1 43706.833984            1 
            3          1024            32           MPI          2 43187.320312            1 
            3          1024            32           MPI          3 43949.113281            1 
            3          1024            32           MPI          4 43239.058594            1 
# Runtime: 3.341145 s (overhead: 0.000120 %) 5 records
