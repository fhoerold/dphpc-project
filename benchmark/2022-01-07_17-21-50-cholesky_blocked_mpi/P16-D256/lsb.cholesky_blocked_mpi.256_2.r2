# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:46:12 2022
# Execution time and date (local): Fri Jan  7 22:46:12 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           256            32           MPI          0 1627.373047            0 
            2           256            32           MPI          1 1492.222656            0 
            2           256            32           MPI          2 1533.685547            0 
            2           256            32           MPI          3 1453.466797            0 
            2           256            32           MPI          4 1447.376953            0 
# Runtime: 14.005117 s (overhead: 0.000000 %) 5 records
