# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:21:37 2022
# Execution time and date (local): Sat Jan  8 05:21:37 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048            32           MPI          0 247986.533203            0 
            2          2048            32           MPI          1 246675.726562            5 
            2          2048            32           MPI          2 245890.468750            3 
            2          2048            32           MPI          3 245759.220703            0 
            2          2048            32           MPI          4 247477.517578            4 
# Runtime: 8.795270 s (overhead: 0.000136 %) 5 records
