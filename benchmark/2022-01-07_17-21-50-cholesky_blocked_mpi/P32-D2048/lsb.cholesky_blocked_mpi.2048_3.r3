# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:21:37 2022
# Execution time and date (local): Sat Jan  8 05:21:37 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048            32           MPI          0 247711.011719            0 
            3          2048            32           MPI          1 246397.125000            2 
            3          2048            32           MPI          2 245611.742188            1 
            3          2048            32           MPI          3 245486.236328            0 
            3          2048            32           MPI          4 247201.722656            1 
# Runtime: 10.768956 s (overhead: 0.000037 %) 5 records
