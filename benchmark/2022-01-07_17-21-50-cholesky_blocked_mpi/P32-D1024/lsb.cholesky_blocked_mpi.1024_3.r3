# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:20:30 2022
# Execution time and date (local): Sat Jan  8 05:20:30 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024            32           MPI          0 47770.085938            0 
            3          1024            32           MPI          1 46983.406250            1 
            3          1024            32           MPI          2 46898.085938            0 
            3          1024            32           MPI          3 47046.914062            0 
            3          1024            32           MPI          4 46727.800781            1 
# Runtime: 5.362152 s (overhead: 0.000037 %) 5 records
