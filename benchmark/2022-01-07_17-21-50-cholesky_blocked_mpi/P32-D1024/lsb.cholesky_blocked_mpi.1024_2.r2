# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:20:30 2022
# Execution time and date (local): Sat Jan  8 05:20:30 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024            32           MPI          0 47749.578125            0 
            2          1024            32           MPI          1 46962.074219            1 
            2          1024            32           MPI          2 46877.101562            0 
            2          1024            32           MPI          3 47025.322266            0 
            2          1024            32           MPI          4 46706.986328            0 
# Runtime: 10.353834 s (overhead: 0.000010 %) 5 records
