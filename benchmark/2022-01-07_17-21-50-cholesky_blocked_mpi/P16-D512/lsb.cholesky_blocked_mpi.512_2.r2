# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 21:07:46 2022
# Execution time and date (local): Fri Jan  7 22:07:46 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512            32           MPI          0 8648.859375            0 
            2           512            32           MPI          1 8381.380859            1 
            2           512            32           MPI          2 8375.111328            0 
            2           512            32           MPI          3 8257.509766            0 
            2           512            32           MPI          4 8355.908203            0 
# Runtime: 11.075946 s (overhead: 0.000009 %) 5 records
