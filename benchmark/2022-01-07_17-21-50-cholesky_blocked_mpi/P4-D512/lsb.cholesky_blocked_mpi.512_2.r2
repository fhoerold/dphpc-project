# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:29:24 2022
# Execution time and date (local): Fri Jan  7 17:29:24 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512            32           MPI          0 9423.933594            0 
            2           512            32           MPI          1 9359.271484            0 
            2           512            32           MPI          2 9766.402344            1 
            2           512            32           MPI          3 9508.859375            0 
            2           512            32           MPI          4 9735.166016            3 
# Runtime: 10.082146 s (overhead: 0.000040 %) 5 records
