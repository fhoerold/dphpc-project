# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:29:24 2022
# Execution time and date (local): Fri Jan  7 17:29:24 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512            32           MPI          0 9435.330078            0 
            3           512            32           MPI          1 9366.519531            0 
            3           512            32           MPI          2 9830.187500            1 
            3           512            32           MPI          3 9525.832031            0 
            3           512            32           MPI          4 9755.494141            2 
# Runtime: 10.081880 s (overhead: 0.000030 %) 5 records
