# Sysname : Linux
# Nodename: eu-a6-009-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:29:40 2022
# Execution time and date (local): Fri Jan  7 17:29:40 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512            32           MPI          0 6951.324219            0 
            3           512            32           MPI          1 6935.078125            1 
            3           512            32           MPI          2 7244.525391            0 
            3           512            32           MPI          3 6749.843750            0 
            3           512            32           MPI          4 6902.402344            0 
# Runtime: 4.051035 s (overhead: 0.000025 %) 5 records
