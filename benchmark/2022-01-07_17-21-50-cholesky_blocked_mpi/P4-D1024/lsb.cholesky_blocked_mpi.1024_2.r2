# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:41:30 2022
# Execution time and date (local): Fri Jan  7 17:41:30 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024            32           MPI          0 76167.531250            0 
            2          1024            32           MPI          1 77931.150391            3 
            2          1024            32           MPI          2 79590.507812            1 
            2          1024            32           MPI          3 77783.488281            1 
            2          1024            32           MPI          4 77160.527344            2 
# Runtime: 6.584401 s (overhead: 0.000106 %) 5 records
