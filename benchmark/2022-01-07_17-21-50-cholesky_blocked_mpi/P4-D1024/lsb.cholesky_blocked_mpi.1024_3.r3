# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:41:30 2022
# Execution time and date (local): Fri Jan  7 17:41:30 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024            32           MPI          0 76631.740234            0 
            3          1024            32           MPI          1 77983.898438            4 
            3          1024            32           MPI          2 79628.316406            2 
            3          1024            32           MPI          3 77815.031250            0 
            3          1024            32           MPI          4 77188.089844            3 
# Runtime: 0.579862 s (overhead: 0.001552 %) 5 records
