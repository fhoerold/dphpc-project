# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:55:25 2022
# Execution time and date (local): Wed Jan 12 17:55:25 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2801          2801   gramschmidt          0 13407581.671875            0 
         2801          2801   gramschmidt          1 9381132.009766            4 
         2801          2801   gramschmidt          2 9352048.375000            3 
         2801          2801   gramschmidt          3 9641614.042969            2 
         2801          2801   gramschmidt          4 9832310.271484            9 
         2801          2801   gramschmidt          5 9703294.580078            0 
         2801          2801   gramschmidt          6 9814951.837891            0 
         2801          2801   gramschmidt          7 11144652.867188            0 
         2801          2801   gramschmidt          8 9462808.529297            3 
         2801          2801   gramschmidt          9 11842127.179688            0 
# Runtime: 103.582597 s (overhead: 0.000020 %) 10 records
