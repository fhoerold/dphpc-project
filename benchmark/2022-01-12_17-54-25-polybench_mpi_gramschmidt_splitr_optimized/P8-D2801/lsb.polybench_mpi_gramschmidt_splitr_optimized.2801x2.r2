# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:55:25 2022
# Execution time and date (local): Wed Jan 12 17:55:25 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2801          2801   gramschmidt          0 13405834.275391            1 
         2801          2801   gramschmidt          1 9381001.162109            6 
         2801          2801   gramschmidt          2 9352909.734375            2 
         2801          2801   gramschmidt          3 9641024.609375            1 
         2801          2801   gramschmidt          4 9832206.990234           10 
         2801          2801   gramschmidt          5 9704325.351562            0 
         2801          2801   gramschmidt          6 9813980.101562            0 
         2801          2801   gramschmidt          7 11145916.656250            0 
         2801          2801   gramschmidt          8 9462774.480469            3 
         2801          2801   gramschmidt          9 11841364.552734            0 
# Runtime: 103.581406 s (overhead: 0.000022 %) 10 records
