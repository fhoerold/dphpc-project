# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:56:53 2022
# Execution time and date (local): Wed Jan 12 17:56:53 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2801          2801   gramschmidt          0 7691148.623047            0 
         2801          2801   gramschmidt          1 6898037.187500            3 
         2801          2801   gramschmidt          2 6748662.429688            1 
         2801          2801   gramschmidt          3 6640490.636719            0 
         2801          2801   gramschmidt          4 6773893.285156            4 
         2801          2801   gramschmidt          5 6630356.554688            0 
         2801          2801   gramschmidt          6 6794716.339844            0 
         2801          2801   gramschmidt          7 6631477.474609            0 
         2801          2801   gramschmidt          8 6350172.984375            4 
         2801          2801   gramschmidt          9 6691690.804688            0 
# Runtime: 67.850693 s (overhead: 0.000018 %) 10 records
