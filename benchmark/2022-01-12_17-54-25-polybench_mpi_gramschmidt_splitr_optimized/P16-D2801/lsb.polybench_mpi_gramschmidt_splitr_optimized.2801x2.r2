# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:56:53 2022
# Execution time and date (local): Wed Jan 12 17:56:53 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2801          2801   gramschmidt          0 9689616.302734            0 
         2801          2801   gramschmidt          1 6897967.886719            5 
         2801          2801   gramschmidt          2 6748246.048828            5 
         2801          2801   gramschmidt          3 6641338.986328            0 
         2801          2801   gramschmidt          4 6772164.339844            5 
         2801          2801   gramschmidt          5 6631266.320312            0 
         2801          2801   gramschmidt          6 6794328.142578            0 
         2801          2801   gramschmidt          7 6631007.998047            0 
         2801          2801   gramschmidt          8 6348638.000000            5 
         2801          2801   gramschmidt          9 6692514.232422            0 
# Runtime: 69.847156 s (overhead: 0.000029 %) 10 records
