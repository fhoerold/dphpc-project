# Sysname : Linux
# Nodename: eu-a6-009-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:57:41 2022
# Execution time and date (local): Wed Jan 12 17:57:41 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1000          1000   gramschmidt          0 5110787.847656            0 
         1000          1000   gramschmidt          1 96104.832031            5 
         1000          1000   gramschmidt          2 96458.277344            2 
         1000          1000   gramschmidt          3 96785.216797            0 
         1000          1000   gramschmidt          4 94373.892578            2 
         1000          1000   gramschmidt          5 94397.783203            0 
         1000          1000   gramschmidt          6 94433.806641            0 
         1000          1000   gramschmidt          7 94045.681641            0 
         1000          1000   gramschmidt          8 93934.634766            1 
         1000          1000   gramschmidt          9 96157.330078            0 
# Runtime: 5.967510 s (overhead: 0.000168 %) 10 records
