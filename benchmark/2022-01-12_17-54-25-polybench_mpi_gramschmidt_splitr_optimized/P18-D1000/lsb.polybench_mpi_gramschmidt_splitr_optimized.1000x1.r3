# Sysname : Linux
# Nodename: eu-a6-009-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:57:41 2022
# Execution time and date (local): Wed Jan 12 17:57:41 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1000          1000   gramschmidt          0 5109881.226562            0 
         1000          1000   gramschmidt          1 96117.914062            4 
         1000          1000   gramschmidt          2 96489.000000            2 
         1000          1000   gramschmidt          3 96749.421875            0 
         1000          1000   gramschmidt          4 94394.677734            1 
         1000          1000   gramschmidt          5 94379.744141            0 
         1000          1000   gramschmidt          6 94440.904297            0 
         1000          1000   gramschmidt          7 94043.748047            0 
         1000          1000   gramschmidt          8 93936.332031            1 
         1000          1000   gramschmidt          9 96155.769531            0 
# Runtime: 5.966627 s (overhead: 0.000134 %) 10 records
