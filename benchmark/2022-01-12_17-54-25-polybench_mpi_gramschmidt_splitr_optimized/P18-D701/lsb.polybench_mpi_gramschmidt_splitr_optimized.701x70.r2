# Sysname : Linux
# Nodename: eu-a6-009-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:57:24 2022
# Execution time and date (local): Wed Jan 12 17:57:24 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          701           701   gramschmidt          0 6042834.404297            0 
          701           701   gramschmidt          1 37158.117188            3 
          701           701   gramschmidt          2 36529.328125            1 
          701           701   gramschmidt          3 36281.673828            0 
          701           701   gramschmidt          4 36226.951172            1 
          701           701   gramschmidt          5 36172.912109            0 
          701           701   gramschmidt          6 35962.667969            0 
          701           701   gramschmidt          7 36078.851562            0 
          701           701   gramschmidt          8 36189.794922            1 
          701           701   gramschmidt          9 36204.449219            0 
# Runtime: 6.369671 s (overhead: 0.000094 %) 10 records
