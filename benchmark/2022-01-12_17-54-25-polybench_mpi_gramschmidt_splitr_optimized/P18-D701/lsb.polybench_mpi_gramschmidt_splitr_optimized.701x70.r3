# Sysname : Linux
# Nodename: eu-a6-009-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:57:24 2022
# Execution time and date (local): Wed Jan 12 17:57:24 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          701           701   gramschmidt          0 6039235.546875            0 
          701           701   gramschmidt          1 37085.066406            4 
          701           701   gramschmidt          2 36512.025391            1 
          701           701   gramschmidt          3 36294.457031            0 
          701           701   gramschmidt          4 36222.925781            1 
          701           701   gramschmidt          5 36160.134766            0 
          701           701   gramschmidt          6 35971.955078            0 
          701           701   gramschmidt          7 36065.181641            0 
          701           701   gramschmidt          8 36189.056641            1 
          701           701   gramschmidt          9 36214.160156            0 
# Runtime: 6.365985 s (overhead: 0.000110 %) 10 records
