# Sysname : Linux
# Nodename: eu-a6-007-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 17:01:12 2022
# Execution time and date (local): Wed Jan 12 18:01:12 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2801          2801   gramschmidt          0 6538719.912109            0 
         2801          2801   gramschmidt          1 1525790.326172            5 
         2801          2801   gramschmidt          2 1494608.531250            5 
         2801          2801   gramschmidt          3 1499938.951172            0 
         2801          2801   gramschmidt          4 1520338.738281            2 
         2801          2801   gramschmidt          5 1467389.544922            0 
         2801          2801   gramschmidt          6 1471690.587891            0 
         2801          2801   gramschmidt          7 1469911.099609            0 
         2801          2801   gramschmidt          8 1511282.720703            6 
         2801          2801   gramschmidt          9 1511741.414062            0 
# Runtime: 20.011460 s (overhead: 0.000090 %) 10 records
