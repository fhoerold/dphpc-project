# Sysname : Linux
# Nodename: eu-a6-007-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 17:01:12 2022
# Execution time and date (local): Wed Jan 12 18:01:12 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2801          2801   gramschmidt          0 1523645.873047            0 
         2801          2801   gramschmidt          1 1525929.988281            5 
         2801          2801   gramschmidt          2 1494575.669922            5 
         2801          2801   gramschmidt          3 1499650.757812            0 
         2801          2801   gramschmidt          4 1520541.359375            5 
         2801          2801   gramschmidt          5 1467313.218750            0 
         2801          2801   gramschmidt          6 1471639.939453            0 
         2801          2801   gramschmidt          7 1469878.656250            2 
         2801          2801   gramschmidt          8 1511031.736328            5 
         2801          2801   gramschmidt          9 1511883.681641            0 
# Runtime: 14.996156 s (overhead: 0.000147 %) 10 records
