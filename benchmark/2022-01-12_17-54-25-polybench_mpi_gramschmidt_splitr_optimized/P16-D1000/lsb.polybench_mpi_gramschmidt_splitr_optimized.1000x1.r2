# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:56:09 2022
# Execution time and date (local): Wed Jan 12 17:56:09 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1000          1000   gramschmidt          0 6152580.857422            0 
         1000          1000   gramschmidt          1 117713.158203            5 
         1000          1000   gramschmidt          2 116626.388672            2 
         1000          1000   gramschmidt          3 116016.388672            0 
         1000          1000   gramschmidt          4 115685.994141            1 
         1000          1000   gramschmidt          5 116121.382812            0 
         1000          1000   gramschmidt          6 115169.281250            0 
         1000          1000   gramschmidt          7 119866.056641            0 
         1000          1000   gramschmidt          8 118829.072266            2 
         1000          1000   gramschmidt          9 114238.039062            0 
# Runtime: 7.202887 s (overhead: 0.000139 %) 10 records
