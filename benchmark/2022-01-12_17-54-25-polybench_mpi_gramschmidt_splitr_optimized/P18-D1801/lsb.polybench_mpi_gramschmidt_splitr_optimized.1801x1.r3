# Sysname : Linux
# Nodename: eu-a6-009-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:58:02 2022
# Execution time and date (local): Wed Jan 12 17:58:02 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1801          1801   gramschmidt          0 2526865.976562            0 
         1801          1801   gramschmidt          1 561919.935547            4 
         1801          1801   gramschmidt          2 547828.144531            1 
         1801          1801   gramschmidt          3 568704.455078            0 
         1801          1801   gramschmidt          4 553511.796875            1 
         1801          1801   gramschmidt          5 560892.244141            0 
         1801          1801   gramschmidt          6 555738.146484            0 
         1801          1801   gramschmidt          7 550303.943359            0 
         1801          1801   gramschmidt          8 600040.648438            5 
         1801          1801   gramschmidt          9 556536.671875            0 
# Runtime: 7.582393 s (overhead: 0.000145 %) 10 records
