# Sysname : Linux
# Nodename: eu-a6-009-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:58:01 2022
# Execution time and date (local): Wed Jan 12 17:58:01 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1801          1801   gramschmidt          0 560431.652344            0 
         1801          1801   gramschmidt          1 561924.408203            4 
         1801          1801   gramschmidt          2 547866.244141            2 
         1801          1801   gramschmidt          3 568734.746094            0 
         1801          1801   gramschmidt          4 553524.683594            2 
         1801          1801   gramschmidt          5 560762.033203            0 
         1801          1801   gramschmidt          6 555920.222656            0 
         1801          1801   gramschmidt          7 550322.480469            0 
         1801          1801   gramschmidt          8 600086.207031            4 
         1801          1801   gramschmidt          9 556543.546875            0 
# Runtime: 5.616168 s (overhead: 0.000214 %) 10 records
