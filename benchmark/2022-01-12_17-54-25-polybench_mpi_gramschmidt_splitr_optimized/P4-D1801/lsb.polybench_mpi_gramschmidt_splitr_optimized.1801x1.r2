# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:54:55 2022
# Execution time and date (local): Wed Jan 12 17:54:55 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1801          1801   gramschmidt          0 20640662.050781            0 
         1801          1801   gramschmidt          1 2828663.353516            5 
         1801          1801   gramschmidt          2 2854962.433594            3 
         1801          1801   gramschmidt          3 2763226.267578            0 
         1801          1801   gramschmidt          4 2792255.908203            1 
         1801          1801   gramschmidt          5 2896419.593750            0 
         1801          1801   gramschmidt          6 2804615.003906            0 
         1801          1801   gramschmidt          7 2702709.869141            0 
         1801          1801   gramschmidt          8 2848804.041016            3 
         1801          1801   gramschmidt          9 2754358.492188            0 
# Runtime: 45.886735 s (overhead: 0.000026 %) 10 records
