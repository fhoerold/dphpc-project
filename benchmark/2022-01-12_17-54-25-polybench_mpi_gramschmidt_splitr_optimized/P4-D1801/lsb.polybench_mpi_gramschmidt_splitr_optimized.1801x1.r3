# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:54:55 2022
# Execution time and date (local): Wed Jan 12 17:54:55 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1801          1801   gramschmidt          0 2685131.802734            0 
         1801          1801   gramschmidt          1 2834827.945312            3 
         1801          1801   gramschmidt          2 2858882.822266            3 
         1801          1801   gramschmidt          3 2767552.052734            0 
         1801          1801   gramschmidt          4 2796900.503906            3 
         1801          1801   gramschmidt          5 2901518.666016            0 
         1801          1801   gramschmidt          6 2808472.796875            0 
         1801          1801   gramschmidt          7 2707094.232422            0 
         1801          1801   gramschmidt          8 2853286.541016            4 
         1801          1801   gramschmidt          9 2758862.857422            0 
# Runtime: 27.972585 s (overhead: 0.000046 %) 10 records
