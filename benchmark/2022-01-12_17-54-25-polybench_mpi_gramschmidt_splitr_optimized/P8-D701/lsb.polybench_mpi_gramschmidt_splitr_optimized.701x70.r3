# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:54:54 2022
# Execution time and date (local): Wed Jan 12 17:54:54 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          701           701   gramschmidt          0 9053944.341797            0 
          701           701   gramschmidt          1 57740.634766            4 
          701           701   gramschmidt          2 55791.519531            1 
          701           701   gramschmidt          3 55817.398438            0 
          701           701   gramschmidt          4 55354.373047            1 
          701           701   gramschmidt          5 55708.042969            0 
          701           701   gramschmidt          6 55500.332031            0 
          701           701   gramschmidt          7 55747.775391            0 
          701           701   gramschmidt          8 55594.857422            1 
          701           701   gramschmidt          9 55637.701172            0 
# Runtime: 9.556874 s (overhead: 0.000073 %) 10 records
