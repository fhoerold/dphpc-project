# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:54:54 2022
# Execution time and date (local): Wed Jan 12 17:54:54 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          701           701   gramschmidt          0 12087458.542969            0 
          701           701   gramschmidt          1 57774.482422            4 
          701           701   gramschmidt          2 55821.246094            1 
          701           701   gramschmidt          3 55838.923828            0 
          701           701   gramschmidt          4 55386.287109            1 
          701           701   gramschmidt          5 55735.750000            0 
          701           701   gramschmidt          6 55526.478516            0 
          701           701   gramschmidt          7 55772.222656            0 
          701           701   gramschmidt          8 55622.595703            1 
          701           701   gramschmidt          9 55662.531250            0 
# Runtime: 12.590631 s (overhead: 0.000056 %) 10 records
