# Sysname : Linux
# Nodename: eu-a6-007-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 17:00:54 2022
# Execution time and date (local): Wed Jan 12 18:00:54 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1801          1801   gramschmidt          0 6379114.210938            0 
         1801          1801   gramschmidt          1 355110.572266            4 
         1801          1801   gramschmidt          2 344881.031250            2 
         1801          1801   gramschmidt          3 339511.119141            0 
         1801          1801   gramschmidt          4 336306.380859            1 
         1801          1801   gramschmidt          5 331290.951172            0 
         1801          1801   gramschmidt          6 336699.484375            0 
         1801          1801   gramschmidt          7 333275.337891            0 
         1801          1801   gramschmidt          8 337357.105469            2 
         1801          1801   gramschmidt          9 332033.244141            0 
# Runtime: 9.425622 s (overhead: 0.000095 %) 10 records
