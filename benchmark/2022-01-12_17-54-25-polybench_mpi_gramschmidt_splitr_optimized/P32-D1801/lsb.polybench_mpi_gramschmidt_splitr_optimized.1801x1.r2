# Sysname : Linux
# Nodename: eu-a6-007-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 17:00:54 2022
# Execution time and date (local): Wed Jan 12 18:00:54 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1801          1801   gramschmidt          0 2383310.287109            0 
         1801          1801   gramschmidt          1 355083.367188            5 
         1801          1801   gramschmidt          2 344777.972656            2 
         1801          1801   gramschmidt          3 339603.330078            0 
         1801          1801   gramschmidt          4 336276.966797            2 
         1801          1801   gramschmidt          5 331290.576172            0 
         1801          1801   gramschmidt          6 336689.199219            0 
         1801          1801   gramschmidt          7 333257.246094            0 
         1801          1801   gramschmidt          8 337359.605469            4 
         1801          1801   gramschmidt          9 331995.730469            0 
# Runtime: 5.429704 s (overhead: 0.000239 %) 10 records
