# Sysname : Linux
# Nodename: eu-a6-011-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:54:54 2022
# Execution time and date (local): Wed Jan 12 17:54:54 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2801          2801   gramschmidt          0 23208031.425781            0 
         2801          2801   gramschmidt          1 22570974.697266            3 
         2801          2801   gramschmidt          2 22561061.878906            3 
         2801          2801   gramschmidt          3 23911908.689453            0 
         2801          2801   gramschmidt          4 23198202.644531            3 
         2801          2801   gramschmidt          5 24101627.613281            0 
         2801          2801   gramschmidt          6 24630652.466797            0 
         2801          2801   gramschmidt          7 23025301.126953            0 
         2801          2801   gramschmidt          8 24604504.634766            5 
         2801          2801   gramschmidt          9 23409621.220703            0 
# Runtime: 235.221942 s (overhead: 0.000006 %) 10 records
