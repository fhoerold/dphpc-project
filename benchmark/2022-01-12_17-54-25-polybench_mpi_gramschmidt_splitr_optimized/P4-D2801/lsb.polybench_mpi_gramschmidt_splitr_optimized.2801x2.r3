# Sysname : Linux
# Nodename: eu-a6-011-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:54:54 2022
# Execution time and date (local): Wed Jan 12 17:54:54 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2801          2801   gramschmidt          0 41209759.455078            0 
         2801          2801   gramschmidt          1 22569433.136719            5 
         2801          2801   gramschmidt          2 22560029.925781            3 
         2801          2801   gramschmidt          3 23908927.892578            0 
         2801          2801   gramschmidt          4 23198507.541016            4 
         2801          2801   gramschmidt          5 24100066.419922            0 
         2801          2801   gramschmidt          6 24629497.031250            0 
         2801          2801   gramschmidt          7 23023736.058594            0 
         2801          2801   gramschmidt          8 24601745.123047            5 
         2801          2801   gramschmidt          9 23409974.710938            0 
# Runtime: 253.211724 s (overhead: 0.000007 %) 10 records
