# Sysname : Linux
# Nodename: eu-a6-009-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:58:19 2022
# Execution time and date (local): Wed Jan 12 17:58:19 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2801          2801   gramschmidt          0 14109363.660156            0 
         2801          2801   gramschmidt          1 4259906.507812            5 
         2801          2801   gramschmidt          2 4230054.113281            4 
         2801          2801   gramschmidt          3 4230581.707031            0 
         2801          2801   gramschmidt          4 4292849.666016            2 
         2801          2801   gramschmidt          5 4208668.898438            0 
         2801          2801   gramschmidt          6 4251400.736328            0 
         2801          2801   gramschmidt          7 4262834.087891            0 
         2801          2801   gramschmidt          8 4244396.533203            6 
         2801          2801   gramschmidt          9 4232460.708984            1 
# Runtime: 52.322575 s (overhead: 0.000034 %) 10 records
