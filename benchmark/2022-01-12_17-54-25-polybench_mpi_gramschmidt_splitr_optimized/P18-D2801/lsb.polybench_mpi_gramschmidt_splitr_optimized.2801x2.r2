# Sysname : Linux
# Nodename: eu-a6-009-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:58:19 2022
# Execution time and date (local): Wed Jan 12 17:58:19 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         2801          2801   gramschmidt          0 13210749.406250            2 
         2801          2801   gramschmidt          1 4259905.095703            5 
         2801          2801   gramschmidt          2 4230294.320312            4 
         2801          2801   gramschmidt          3 4230281.283203            0 
         2801          2801   gramschmidt          4 4293114.587891            2 
         2801          2801   gramschmidt          5 4208213.972656            0 
         2801          2801   gramschmidt          6 4251755.888672            0 
         2801          2801   gramschmidt          7 4262829.666016            0 
         2801          2801   gramschmidt          8 4243999.474609            5 
         2801          2801   gramschmidt          9 4232737.128906            0 
# Runtime: 51.423938 s (overhead: 0.000035 %) 10 records
