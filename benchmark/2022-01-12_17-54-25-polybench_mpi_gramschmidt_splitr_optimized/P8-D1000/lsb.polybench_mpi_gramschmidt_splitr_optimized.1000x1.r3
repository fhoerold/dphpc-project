# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:54:54 2022
# Execution time and date (local): Wed Jan 12 17:54:54 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1000          1000   gramschmidt          0 172156.513672            0 
         1000          1000   gramschmidt          1 169859.583984            1 
         1000          1000   gramschmidt          2 167543.988281            1 
         1000          1000   gramschmidt          3 168153.091797            0 
         1000          1000   gramschmidt          4 166921.861328            1 
         1000          1000   gramschmidt          5 170556.960938            0 
         1000          1000   gramschmidt          6 171088.324219            0 
         1000          1000   gramschmidt          7 167453.621094            0 
         1000          1000   gramschmidt          8 168240.478516            4 
         1000          1000   gramschmidt          9 166978.773438            0 
# Runtime: 1.688999 s (overhead: 0.000414 %) 10 records
