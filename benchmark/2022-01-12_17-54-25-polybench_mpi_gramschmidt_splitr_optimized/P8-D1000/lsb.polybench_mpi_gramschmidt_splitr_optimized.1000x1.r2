# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:54:54 2022
# Execution time and date (local): Wed Jan 12 17:54:54 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1000          1000   gramschmidt          0 5180033.189453            0 
         1000          1000   gramschmidt          1 169852.589844            4 
         1000          1000   gramschmidt          2 167535.439453            1 
         1000          1000   gramschmidt          3 168142.101562            0 
         1000          1000   gramschmidt          4 166911.964844            1 
         1000          1000   gramschmidt          5 170528.992188            1 
         1000          1000   gramschmidt          6 171084.890625            0 
         1000          1000   gramschmidt          7 167442.775391            0 
         1000          1000   gramschmidt          8 168222.275391            4 
         1000          1000   gramschmidt          9 166976.234375            0 
# Runtime: 6.696782 s (overhead: 0.000164 %) 10 records
