# Sysname : Linux
# Nodename: eu-a6-010-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:59:56 2022
# Execution time and date (local): Wed Jan 12 17:59:56 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          701           701   gramschmidt          0 13066499.332031            0 
          701           701   gramschmidt          1 38207.373047            3 
          701           701   gramschmidt          2 36194.234375            0 
          701           701   gramschmidt          3 35925.519531            0 
          701           701   gramschmidt          4 35806.718750            0 
          701           701   gramschmidt          5 36430.271484            0 
          701           701   gramschmidt          6 37274.453125            0 
          701           701   gramschmidt          7 36012.916016            0 
          701           701   gramschmidt          8 35828.851562            0 
          701           701   gramschmidt          9 35741.400391            0 
# Runtime: 13.393955 s (overhead: 0.000022 %) 10 records
