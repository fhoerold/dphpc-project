# Sysname : Linux
# Nodename: eu-a6-010-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:59:56 2022
# Execution time and date (local): Wed Jan 12 17:59:56 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          701           701   gramschmidt          0 13057163.390625            0 
          701           701   gramschmidt          1 38120.408203            3 
          701           701   gramschmidt          2 36176.314453            0 
          701           701   gramschmidt          3 35917.964844            0 
          701           701   gramschmidt          4 35800.640625            0 
          701           701   gramschmidt          5 36426.392578            0 
          701           701   gramschmidt          6 37269.375000            0 
          701           701   gramschmidt          7 36004.804688            0 
          701           701   gramschmidt          8 35822.634766            0 
          701           701   gramschmidt          9 35738.164062            0 
# Runtime: 13.384474 s (overhead: 0.000022 %) 10 records
