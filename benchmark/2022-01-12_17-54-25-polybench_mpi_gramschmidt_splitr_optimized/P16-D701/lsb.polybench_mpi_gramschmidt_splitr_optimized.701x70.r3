# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:55:53 2022
# Execution time and date (local): Wed Jan 12 17:55:53 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          701           701   gramschmidt          0 5047737.832031            0 
          701           701   gramschmidt          1 44323.806641            4 
          701           701   gramschmidt          2 44742.617188            1 
          701           701   gramschmidt          3 44222.968750            0 
          701           701   gramschmidt          4 43966.013672            0 
          701           701   gramschmidt          5 44017.593750            0 
          701           701   gramschmidt          6 44809.923828            0 
          701           701   gramschmidt          7 46495.277344            0 
          701           701   gramschmidt          8 43705.089844            1 
          701           701   gramschmidt          9 43526.154297            0 
# Runtime: 5.447586 s (overhead: 0.000110 %) 10 records
