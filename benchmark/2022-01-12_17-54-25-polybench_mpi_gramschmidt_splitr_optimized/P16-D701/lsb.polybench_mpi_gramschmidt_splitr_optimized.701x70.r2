# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:55:53 2022
# Execution time and date (local): Wed Jan 12 17:55:53 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          701           701   gramschmidt          0 1059782.890625            0 
          701           701   gramschmidt          1 44337.933594            5 
          701           701   gramschmidt          2 44757.923828            1 
          701           701   gramschmidt          3 44242.585938            0 
          701           701   gramschmidt          4 43984.968750            1 
          701           701   gramschmidt          5 44032.453125            0 
          701           701   gramschmidt          6 44826.654297            0 
          701           701   gramschmidt          7 46508.619141            0 
          701           701   gramschmidt          8 43721.324219            1 
          701           701   gramschmidt          9 43542.855469            0 
# Runtime: 1.459777 s (overhead: 0.000548 %) 10 records
