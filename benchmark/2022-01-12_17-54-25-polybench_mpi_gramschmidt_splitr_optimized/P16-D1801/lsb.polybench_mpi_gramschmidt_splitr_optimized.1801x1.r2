# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:56:27 2022
# Execution time and date (local): Wed Jan 12 17:56:27 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1801          1801   gramschmidt          0 10806253.513672            0 
         1801          1801   gramschmidt          1 773888.347656            6 
         1801          1801   gramschmidt          2 765648.367188            2 
         1801          1801   gramschmidt          3 744352.935547            0 
         1801          1801   gramschmidt          4 756437.634766            4 
         1801          1801   gramschmidt          5 744363.814453            0 
         1801          1801   gramschmidt          6 768041.814453            0 
         1801          1801   gramschmidt          7 775156.839844            0 
         1801          1801   gramschmidt          8 758320.937500            2 
         1801          1801   gramschmidt          9 759480.925781            0 
# Runtime: 17.651991 s (overhead: 0.000079 %) 10 records
