# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:56:27 2022
# Execution time and date (local): Wed Jan 12 17:56:27 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1801          1801   gramschmidt          0 12827026.406250            0 
         1801          1801   gramschmidt          1 774404.283203            4 
         1801          1801   gramschmidt          2 765792.300781            2 
         1801          1801   gramschmidt          3 744660.265625            0 
         1801          1801   gramschmidt          4 756755.093750            2 
         1801          1801   gramschmidt          5 744697.894531            0 
         1801          1801   gramschmidt          6 768533.416016            0 
         1801          1801   gramschmidt          7 775336.841797            0 
         1801          1801   gramschmidt          8 758613.712891            2 
         1801          1801   gramschmidt          9 759821.048828            0 
# Runtime: 19.675686 s (overhead: 0.000051 %) 10 records
