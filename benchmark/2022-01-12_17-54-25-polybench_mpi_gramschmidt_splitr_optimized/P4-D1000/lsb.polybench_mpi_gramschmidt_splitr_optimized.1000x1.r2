# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:54:55 2022
# Execution time and date (local): Wed Jan 12 17:54:55 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1000          1000   gramschmidt          0 322128.091797            0 
         1000          1000   gramschmidt          1 317132.400391            1 
         1000          1000   gramschmidt          2 318166.441406            3 
         1000          1000   gramschmidt          3 307571.619141            0 
         1000          1000   gramschmidt          4 306847.367188            1 
         1000          1000   gramschmidt          5 309488.523438            0 
         1000          1000   gramschmidt          6 307238.240234            0 
         1000          1000   gramschmidt          7 309055.664062            0 
         1000          1000   gramschmidt          8 309502.371094            1 
         1000          1000   gramschmidt          9 310501.000000            0 
# Runtime: 3.117677 s (overhead: 0.000192 %) 10 records
