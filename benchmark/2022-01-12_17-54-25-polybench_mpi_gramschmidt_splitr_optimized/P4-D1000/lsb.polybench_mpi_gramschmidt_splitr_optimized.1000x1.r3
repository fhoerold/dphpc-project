# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:54:55 2022
# Execution time and date (local): Wed Jan 12 17:54:55 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1000          1000   gramschmidt          0 327246.308594            0 
         1000          1000   gramschmidt          1 317125.246094            1 
         1000          1000   gramschmidt          2 318325.421875            3 
         1000          1000   gramschmidt          3 307368.330078            0 
         1000          1000   gramschmidt          4 306819.144531            1 
         1000          1000   gramschmidt          5 309488.437500            0 
         1000          1000   gramschmidt          6 307222.714844            0 
         1000          1000   gramschmidt          7 309019.208984            0 
         1000          1000   gramschmidt          8 309480.992188            1 
         1000          1000   gramschmidt          9 310484.126953            0 
# Runtime: 3.122615 s (overhead: 0.000192 %) 10 records
