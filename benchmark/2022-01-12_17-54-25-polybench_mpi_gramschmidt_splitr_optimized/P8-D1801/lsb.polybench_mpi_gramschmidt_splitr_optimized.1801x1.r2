# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:55:10 2022
# Execution time and date (local): Wed Jan 12 17:55:10 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1801          1801   gramschmidt          0 4291932.298828            0 
         1801          1801   gramschmidt          1 1193203.425781            4 
         1801          1801   gramschmidt          2 1207934.128906            1 
         1801          1801   gramschmidt          3 1171944.460938            0 
         1801          1801   gramschmidt          4 1215573.232422            3 
         1801          1801   gramschmidt          5 1195936.417969            0 
         1801          1801   gramschmidt          6 1206199.873047            0 
         1801          1801   gramschmidt          7 1190981.855469            0 
         1801          1801   gramschmidt          8 1184203.710938            4 
         1801          1801   gramschmidt          9 1188397.705078            0 
# Runtime: 15.046359 s (overhead: 0.000080 %) 10 records
