# Sysname : Linux
# Nodename: eu-a6-012-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:55:10 2022
# Execution time and date (local): Wed Jan 12 17:55:10 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1801          1801   gramschmidt          0 1256680.695312            0 
         1801          1801   gramschmidt          1 1192631.812500            4 
         1801          1801   gramschmidt          2 1207903.896484            1 
         1801          1801   gramschmidt          3 1171367.501953            0 
         1801          1801   gramschmidt          4 1215257.566406            3 
         1801          1801   gramschmidt          5 1195880.064453            1 
         1801          1801   gramschmidt          6 1205700.830078            0 
         1801          1801   gramschmidt          7 1190602.527344            0 
         1801          1801   gramschmidt          8 1183884.544922            5 
         1801          1801   gramschmidt          9 1188367.718750            0 
# Runtime: 12.008340 s (overhead: 0.000117 %) 10 records
