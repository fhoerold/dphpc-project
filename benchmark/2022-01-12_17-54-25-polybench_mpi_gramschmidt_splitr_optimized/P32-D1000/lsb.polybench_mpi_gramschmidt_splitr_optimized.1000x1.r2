# Sysname : Linux
# Nodename: eu-a6-010-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 17:00:23 2022
# Execution time and date (local): Wed Jan 12 18:00:23 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1000          1000   gramschmidt          0 9089783.902344            2 
         1000          1000   gramschmidt          1 59585.714844            6 
         1000          1000   gramschmidt          2 58493.607422            0 
         1000          1000   gramschmidt          3 60508.798828            0 
         1000          1000   gramschmidt          4 58618.720703            1 
         1000          1000   gramschmidt          5 58724.605469            0 
         1000          1000   gramschmidt          6 59117.240234            0 
         1000          1000   gramschmidt          7 58589.757812            0 
         1000          1000   gramschmidt          8 58602.792969            0 
         1000          1000   gramschmidt          9 59054.970703            0 
# Runtime: 9.621121 s (overhead: 0.000094 %) 10 records
