# Sysname : Linux
# Nodename: eu-a6-010-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 17:00:23 2022
# Execution time and date (local): Wed Jan 12 18:00:23 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1000          1000   gramschmidt          0 11074735.541016            0 
         1000          1000   gramschmidt          1 59552.230469            5 
         1000          1000   gramschmidt          2 58511.693359            1 
         1000          1000   gramschmidt          3 60481.283203            0 
         1000          1000   gramschmidt          4 58635.839844            1 
         1000          1000   gramschmidt          5 58701.281250            0 
         1000          1000   gramschmidt          6 59111.423828            0 
         1000          1000   gramschmidt          7 58615.039062            0 
         1000          1000   gramschmidt          8 58569.851562            1 
         1000          1000   gramschmidt          9 59047.699219            0 
# Runtime: 11.605992 s (overhead: 0.000069 %) 10 records
