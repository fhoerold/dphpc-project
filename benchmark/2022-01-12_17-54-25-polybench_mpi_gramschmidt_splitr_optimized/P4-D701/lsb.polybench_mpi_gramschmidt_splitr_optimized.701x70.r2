# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:54:55 2022
# Execution time and date (local): Wed Jan 12 17:54:55 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          701           701   gramschmidt          0 8102547.189453            0 
          701           701   gramschmidt          1 119239.597656            3 
          701           701   gramschmidt          2 114337.380859            1 
          701           701   gramschmidt          3 113740.587891            0 
          701           701   gramschmidt          4 113760.451172            1 
          701           701   gramschmidt          5 114197.912109            0 
          701           701   gramschmidt          6 115137.197266            0 
          701           701   gramschmidt          7 114852.953125            0 
          701           701   gramschmidt          8 116125.480469            1 
          701           701   gramschmidt          9 113888.054688            0 
# Runtime: 9.137873 s (overhead: 0.000066 %) 10 records
