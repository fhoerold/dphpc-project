# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 16:54:55 2022
# Execution time and date (local): Wed Jan 12 17:54:55 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
          701           701   gramschmidt          0 118507.619141            0 
          701           701   gramschmidt          1 119421.730469            1 
          701           701   gramschmidt          2 114529.216797            2 
          701           701   gramschmidt          3 113911.841797            0 
          701           701   gramschmidt          4 113945.431641            1 
          701           701   gramschmidt          5 114379.931641            0 
          701           701   gramschmidt          6 115344.505859            0 
          701           701   gramschmidt          7 115035.814453            1 
          701           701   gramschmidt          8 116395.115234            4 
          701           701   gramschmidt          9 113979.681641            0 
# Runtime: 1.155511 s (overhead: 0.000779 %) 10 records
