# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:36:31 2022
# Execution time and date (local): Fri Jan  7 19:36:31 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512           128           MPI          0 10180.800781            0 
            2           512           128           MPI          1 10077.886719            0 
            2           512           128           MPI          2 10063.527344            0 
            2           512           128           MPI          3 10034.429688            0 
            2           512           128           MPI          4 9943.843750            0 
# Runtime: 4.138307 s (overhead: 0.000000 %) 5 records
