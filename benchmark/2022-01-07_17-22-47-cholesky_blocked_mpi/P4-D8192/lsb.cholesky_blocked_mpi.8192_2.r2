# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 20:22:18 2022
# Execution time and date (local): Fri Jan  7 21:22:18 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192           128           MPI          0 30406694.615234            3 
            2          8192           128           MPI          1 30362953.667969            8 
            2          8192           128           MPI          2 30425727.068359            1 
            2          8192           128           MPI          3 30533291.216797            4 
            2          8192           128           MPI          4 30352943.531250            4 
# Runtime: 215.265908 s (overhead: 0.000009 %) 5 records
