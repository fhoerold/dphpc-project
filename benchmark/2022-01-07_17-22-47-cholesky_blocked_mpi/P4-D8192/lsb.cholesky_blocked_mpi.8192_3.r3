# Sysname : Linux
# Nodename: eu-a6-003-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 20:22:18 2022
# Execution time and date (local): Fri Jan  7 21:22:18 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192           128           MPI          0 30405084.658203            0 
            3          8192           128           MPI          1 30361351.189453            3 
            3          8192           128           MPI          2 30424169.236328            1 
            3          8192           128           MPI          3 30531679.537109            1 
            3          8192           128           MPI          4 30351333.865234            5 
# Runtime: 216.252159 s (overhead: 0.000005 %) 5 records
