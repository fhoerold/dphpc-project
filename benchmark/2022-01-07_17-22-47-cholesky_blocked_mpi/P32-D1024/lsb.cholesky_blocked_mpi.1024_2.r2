# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 11:41:15 2022
# Execution time and date (local): Sat Jan  8 12:41:15 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024           128           MPI          0 65711.988281            0 
            2          1024           128           MPI          1 64916.994141            1 
            2          1024           128           MPI          2 64458.789062            0 
            2          1024           128           MPI          3 64569.029297            0 
            2          1024           128           MPI          4 66276.998047            0 
# Runtime: 5.776791 s (overhead: 0.000017 %) 5 records
