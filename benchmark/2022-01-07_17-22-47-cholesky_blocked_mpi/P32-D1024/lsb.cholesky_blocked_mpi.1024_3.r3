# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 11:41:15 2022
# Execution time and date (local): Sat Jan  8 12:41:15 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024           128           MPI          0 65792.330078            0 
            3          1024           128           MPI          1 64708.164062            1 
            3          1024           128           MPI          2 64662.398438            1 
            3          1024           128           MPI          3 64671.890625            0 
            3          1024           128           MPI          4 66548.287109            1 
# Runtime: 12.771179 s (overhead: 0.000023 %) 5 records
