# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:45:17 2022
# Execution time and date (local): Fri Jan  7 17:45:17 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           256           128           MPI          0 7.986328            2 
            2           256           128           MPI          1 0.484375           14 
            2           256           128           MPI          2 0.298828            0 
            2           256           128           MPI          3 0.148438            0 
            2           256           128           MPI          4 0.308594            1 
# Runtime: 5.024349 s (overhead: 0.000338 %) 5 records
