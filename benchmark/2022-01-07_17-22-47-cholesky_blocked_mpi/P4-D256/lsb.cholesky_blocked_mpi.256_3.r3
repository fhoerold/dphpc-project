# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:45:17 2022
# Execution time and date (local): Fri Jan  7 17:45:17 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           256           128           MPI          0 8.390625            3 
            3           256           128           MPI          1 0.201172           10 
            3           256           128           MPI          2 0.220703            0 
            3           256           128           MPI          3 0.189453            0 
            3           256           128           MPI          4 0.382812            0 
# Runtime: 9.023354 s (overhead: 0.000144 %) 5 records
