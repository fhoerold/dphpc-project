# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 22:32:25 2022
# Execution time and date (local): Fri Jan  7 23:32:25 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192           128           MPI          0 17653739.218750            0 
            2          8192           128           MPI          1 18373280.341797           11 
            2          8192           128           MPI          2 18462348.275391            2 
            2          8192           128           MPI          3 18480746.375000            6 
            2          8192           128           MPI          4 18025685.517578            6 
# Runtime: 131.522157 s (overhead: 0.000019 %) 5 records
