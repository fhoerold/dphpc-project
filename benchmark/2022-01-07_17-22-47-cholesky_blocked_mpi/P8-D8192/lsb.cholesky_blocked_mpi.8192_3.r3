# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 22:32:25 2022
# Execution time and date (local): Fri Jan  7 23:32:25 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192           128           MPI          0 17653755.333984            0 
            3          8192           128           MPI          1 18373298.966797            7 
            3          8192           128           MPI          2 18462366.861328            1 
            3          8192           128           MPI          3 18480756.613281            1 
            3          8192           128           MPI          4 18025698.154297            4 
# Runtime: 130.520233 s (overhead: 0.000010 %) 5 records
