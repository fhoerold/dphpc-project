# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 00:31:07 2022
# Execution time and date (local): Sat Jan  8 01:31:07 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512           128           MPI          0 11412.882812            0 
            3           512           128           MPI          1 11159.888672            0 
            3           512           128           MPI          2 11108.498047            0 
            3           512           128           MPI          3 11061.712891            0 
            3           512           128           MPI          4 11048.181641            0 
# Runtime: 5.148529 s (overhead: 0.000000 %) 5 records
