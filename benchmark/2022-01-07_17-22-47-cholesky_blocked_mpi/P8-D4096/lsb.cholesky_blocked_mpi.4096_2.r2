# Sysname : Linux
# Nodename: eu-a6-011-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 19:44:48 2022
# Execution time and date (local): Fri Jan  7 20:44:48 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096           128           MPI          0 2699437.240234            0 
            2          4096           128           MPI          1 2672811.943359            2 
            2          4096           128           MPI          2 2683527.128906            3 
            2          4096           128           MPI          3 2668998.324219            0 
            2          4096           128           MPI          4 2707707.132812            2 
# Runtime: 19.760973 s (overhead: 0.000035 %) 5 records
