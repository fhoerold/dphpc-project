# Sysname : Linux
# Nodename: eu-a6-011-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 19:44:48 2022
# Execution time and date (local): Fri Jan  7 20:44:48 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096           128           MPI          0 2699107.660156            0 
            3          4096           128           MPI          1 2672490.347656            2 
            3          4096           128           MPI          2 2683204.648438            2 
            3          4096           128           MPI          3 2668675.800781            0 
            3          4096           128           MPI          4 2707379.275391            2 
# Runtime: 20.759058 s (overhead: 0.000029 %) 5 records
