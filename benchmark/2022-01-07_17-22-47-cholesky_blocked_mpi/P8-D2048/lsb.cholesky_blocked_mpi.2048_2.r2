# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:37:44 2022
# Execution time and date (local): Fri Jan  7 19:37:44 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048           128           MPI          0 434295.316406            0 
            2          2048           128           MPI          1 429055.486328            1 
            2          2048           128           MPI          2 426416.582031            2 
            2          2048           128           MPI          3 424091.746094            0 
            2          2048           128           MPI          4 428602.949219            2 
# Runtime: 3.378048 s (overhead: 0.000148 %) 5 records
