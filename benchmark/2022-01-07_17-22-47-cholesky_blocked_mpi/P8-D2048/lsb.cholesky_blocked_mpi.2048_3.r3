# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:37:44 2022
# Execution time and date (local): Fri Jan  7 19:37:44 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048           128           MPI          0 434303.183594            0 
            3          2048           128           MPI          1 429062.587891            1 
            3          2048           128           MPI          2 426423.638672            1 
            3          2048           128           MPI          3 424098.240234            0 
            3          2048           128           MPI          4 428611.320312            1 
# Runtime: 6.376496 s (overhead: 0.000047 %) 5 records
