# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:36:48 2022
# Execution time and date (local): Fri Jan  7 19:36:48 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024           128           MPI          0 78504.925781            0 
            3          1024           128           MPI          1 78451.031250            1 
            3          1024           128           MPI          2 78444.097656            1 
            3          1024           128           MPI          3 78490.113281            0 
            3          1024           128           MPI          4 78641.914062            1 
# Runtime: 1.709041 s (overhead: 0.000176 %) 5 records
