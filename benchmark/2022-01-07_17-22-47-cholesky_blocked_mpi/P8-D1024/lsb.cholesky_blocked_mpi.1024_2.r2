# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:36:48 2022
# Execution time and date (local): Fri Jan  7 19:36:48 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024           128           MPI          0 78497.841797            0 
            2          1024           128           MPI          1 78440.017578            1 
            2          1024           128           MPI          2 78433.044922            1 
            2          1024           128           MPI          3 78477.939453            0 
            2          1024           128           MPI          4 78631.914062            1 
# Runtime: 2.713036 s (overhead: 0.000111 %) 5 records
