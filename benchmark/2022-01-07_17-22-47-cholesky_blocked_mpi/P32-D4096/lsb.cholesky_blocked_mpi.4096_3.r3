# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 11:51:27 2022
# Execution time and date (local): Sat Jan  8 12:51:27 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096           128           MPI          0 2206042.753906            0 
            3          4096           128           MPI          1 2198455.013672            1 
            3          4096           128           MPI          2 2177997.396484            1 
            3          4096           128           MPI          3 2176022.681641            0 
            3          4096           128           MPI          4 2171662.556641            2 
# Runtime: 17.059390 s (overhead: 0.000023 %) 5 records
