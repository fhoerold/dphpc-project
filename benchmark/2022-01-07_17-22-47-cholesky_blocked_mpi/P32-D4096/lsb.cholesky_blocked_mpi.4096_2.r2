# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 11:51:27 2022
# Execution time and date (local): Sat Jan  8 12:51:27 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096           128           MPI          0 2203901.773438            0 
            2          4096           128           MPI          1 2194970.212891            2 
            2          4096           128           MPI          2 2175192.849609            1 
            2          4096           128           MPI          3 2175100.748047            0 
            2          4096           128           MPI          4 2170116.019531            1 
# Runtime: 27.057017 s (overhead: 0.000015 %) 5 records
