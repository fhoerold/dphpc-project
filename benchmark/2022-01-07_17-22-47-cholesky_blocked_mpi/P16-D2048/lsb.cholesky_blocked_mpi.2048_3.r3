# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 02:13:54 2022
# Execution time and date (local): Sat Jan  8 03:13:54 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048           128           MPI          0 454499.591797            0 
            3          2048           128           MPI          1 449692.255859            1 
            3          2048           128           MPI          2 447383.572266            1 
            3          2048           128           MPI          3 447510.343750            0 
            3          2048           128           MPI          4 448138.310547            1 
# Runtime: 3.553037 s (overhead: 0.000084 %) 5 records
