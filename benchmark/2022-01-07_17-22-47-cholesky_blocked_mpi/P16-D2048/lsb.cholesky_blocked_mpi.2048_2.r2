# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 02:13:54 2022
# Execution time and date (local): Sat Jan  8 03:13:54 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048           128           MPI          0 454485.986328            0 
            2          2048           128           MPI          1 449681.673828            1 
            2          2048           128           MPI          2 447369.839844            1 
            2          2048           128           MPI          3 447498.220703            0 
            2          2048           128           MPI          4 448127.353516            1 
# Runtime: 12.559671 s (overhead: 0.000024 %) 5 records
