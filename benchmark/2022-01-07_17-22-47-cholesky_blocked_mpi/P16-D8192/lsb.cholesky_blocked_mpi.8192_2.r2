# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:12:57 2022
# Execution time and date (local): Sat Jan  8 05:12:57 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192           128           MPI          0 12311419.636719            0 
            2          8192           128           MPI          1 12300458.818359            9 
            2          8192           128           MPI          2 12216086.996094            2 
            2          8192           128           MPI          3 12222302.441406            0 
            2          8192           128           MPI          4 12234651.603516            1 
# Runtime: 96.777942 s (overhead: 0.000012 %) 5 records
