# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:12:57 2022
# Execution time and date (local): Sat Jan  8 05:12:57 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          8192           128           MPI          0 12311992.476562            0 
            3          8192           128           MPI          1 12301031.449219            5 
            3          8192           128           MPI          2 12216656.878906            7 
            3          8192           128           MPI          3 12222873.996094            0 
            3          8192           128           MPI          4 12235221.041016            2 
# Runtime: 90.780061 s (overhead: 0.000015 %) 5 records
