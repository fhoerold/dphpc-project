# Sysname : Linux
# Nodename: eu-a6-011-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:20:57 2022
# Execution time and date (local): Fri Jan  7 18:20:57 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096           128           MPI          0 4189315.128906            0 
            2          4096           128           MPI          1 4218587.556641            2 
            2          4096           128           MPI          2 4206048.873047            2 
            2          4096           128           MPI          3 4200986.560547            0 
            2          4096           128           MPI          4 4193712.019531            1 
# Runtime: 37.315772 s (overhead: 0.000013 %) 5 records
