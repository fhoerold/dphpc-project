# Sysname : Linux
# Nodename: eu-a6-011-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:20:57 2022
# Execution time and date (local): Fri Jan  7 18:20:57 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096           128           MPI          0 4189390.482422            0 
            3          4096           128           MPI          1 4218702.458984            2 
            3          4096           128           MPI          2 4206128.363281            2 
            3          4096           128           MPI          3 4201059.681641            0 
            3          4096           128           MPI          4 4193788.177734            2 
# Runtime: 36.312824 s (overhead: 0.000017 %) 5 records
