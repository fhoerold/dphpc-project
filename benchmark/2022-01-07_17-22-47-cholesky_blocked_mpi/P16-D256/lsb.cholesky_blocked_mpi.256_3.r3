# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 02:12:51 2022
# Execution time and date (local): Sat Jan  8 03:12:51 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           256           128           MPI          0 4.882812            1 
            3           256           128           MPI          1 0.107422            4 
            3           256           128           MPI          2 0.070312            0 
            3           256           128           MPI          3 0.054688            0 
            3           256           128           MPI          4 0.048828            0 
# Runtime: 6.000982 s (overhead: 0.000083 %) 5 records
