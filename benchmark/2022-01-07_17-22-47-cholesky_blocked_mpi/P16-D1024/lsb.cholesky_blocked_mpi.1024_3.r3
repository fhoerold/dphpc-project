# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 00:31:23 2022
# Execution time and date (local): Sat Jan  8 01:31:23 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024           128           MPI          0 83942.205078            0 
            3          1024           128           MPI          1 83743.042969            1 
            3          1024           128           MPI          2 84348.867188            1 
            3          1024           128           MPI          3 85979.476562            3 
            3          1024           128           MPI          4 83994.992188            8 
# Runtime: 0.762450 s (overhead: 0.001705 %) 5 records
