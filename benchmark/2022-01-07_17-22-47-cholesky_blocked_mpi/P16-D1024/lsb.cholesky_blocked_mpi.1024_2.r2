# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 00:31:23 2022
# Execution time and date (local): Sat Jan  8 01:31:23 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024           128           MPI          0 83929.498047            0 
            2          1024           128           MPI          1 83729.861328            1 
            2          1024           128           MPI          2 84333.769531            1 
            2          1024           128           MPI          3 85989.123047            0 
            2          1024           128           MPI          4 83985.291016            4 
# Runtime: 0.759331 s (overhead: 0.000790 %) 5 records
