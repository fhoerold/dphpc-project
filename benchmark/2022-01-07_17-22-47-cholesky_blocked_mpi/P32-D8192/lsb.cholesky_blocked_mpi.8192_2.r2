# Sysname : Linux
# Nodename: eu-a6-009-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 19:19:20 2022
# Execution time and date (local): Sat Jan  8 20:19:20 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          8192           128           MPI          0 10727361.050781            2 
            2          8192           128           MPI          1 10558943.859375            9 
            2          8192           128           MPI          2 10772824.392578            8 
            2          8192           128           MPI          3 10802572.767578            0 
            2          8192           128           MPI          4 10837123.593750            6 
# Runtime: 81.719138 s (overhead: 0.000031 %) 5 records
