# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:30:46 2022
# Execution time and date (local): Fri Jan  7 17:30:46 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048           128           MPI          0 596177.802734            0 
            3          2048           128           MPI          1 584360.339844            2 
            3          2048           128           MPI          2 586306.796875            2 
            3          2048           128           MPI          3 586596.273438            1 
            3          2048           128           MPI          4 589506.996094            2 
# Runtime: 9.536785 s (overhead: 0.000073 %) 5 records
