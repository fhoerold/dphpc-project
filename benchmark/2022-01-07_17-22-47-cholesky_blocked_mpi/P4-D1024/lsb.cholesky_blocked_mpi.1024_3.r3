# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:45:40 2022
# Execution time and date (local): Fri Jan  7 17:45:40 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          1024           128           MPI          0 92459.322266            0 
            3          1024           128           MPI          1 89399.796875            3 
            3          1024           128           MPI          2 89677.859375            3 
            3          1024           128           MPI          3 89738.628906            1 
            3          1024           128           MPI          4 89830.185547            3 
# Runtime: 9.810400 s (overhead: 0.000102 %) 5 records
