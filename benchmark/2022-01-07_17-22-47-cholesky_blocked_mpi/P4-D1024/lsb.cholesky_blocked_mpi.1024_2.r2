# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:45:40 2022
# Execution time and date (local): Fri Jan  7 17:45:40 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          1024           128           MPI          0 92424.734375            0 
            2          1024           128           MPI          1 89375.583984            2 
            2          1024           128           MPI          2 89652.083984            3 
            2          1024           128           MPI          3 89714.287109            1 
            2          1024           128           MPI          4 89803.542969            3 
# Runtime: 7.816144 s (overhead: 0.000115 %) 5 records
