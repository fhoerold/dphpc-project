# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 02:23:57 2022
# Execution time and date (local): Sat Jan  8 03:23:57 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          4096           128           MPI          0 2180289.980469            0 
            3          4096           128           MPI          1 2152844.669922            2 
            3          4096           128           MPI          2 2134009.677734            3 
            3          4096           128           MPI          3 2133514.185547            0 
            3          4096           128           MPI          4 2134116.376953            1 
# Runtime: 23.083274 s (overhead: 0.000026 %) 5 records
