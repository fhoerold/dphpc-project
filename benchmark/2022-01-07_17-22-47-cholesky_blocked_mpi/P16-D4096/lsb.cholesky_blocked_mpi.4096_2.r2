# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 02:23:57 2022
# Execution time and date (local): Sat Jan  8 03:23:57 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          4096           128           MPI          0 2180263.419922            0 
            2          4096           128           MPI          1 2152817.445312            2 
            2          4096           128           MPI          2 2133979.822266            5 
            2          4096           128           MPI          3 2133487.892578            0 
            2          4096           128           MPI          4 2134087.068359            6 
# Runtime: 23.083814 s (overhead: 0.000056 %) 5 records
