# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 11:40:56 2022
# Execution time and date (local): Sat Jan  8 12:40:56 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512           128           MPI          0 4353.972656            0 
            2           512           128           MPI          1 4333.031250            0 
            2           512           128           MPI          2 4285.519531            0 
            2           512           128           MPI          3 4198.753906            0 
            2           512           128           MPI          4 4125.117188            0 
# Runtime: 7.154800 s (overhead: 0.000000 %) 5 records
