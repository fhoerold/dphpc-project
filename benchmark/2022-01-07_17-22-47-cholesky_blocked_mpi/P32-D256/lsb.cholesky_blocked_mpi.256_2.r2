# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:30:32 2022
# Execution time and date (local): Sat Jan  8 05:30:32 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           256           128           MPI          0 4.998047            3 
            2           256           128           MPI          1 0.068359            8 
            2           256           128           MPI          2 0.050781            0 
            2           256           128           MPI          3 0.013672            0 
            2           256           128           MPI          4 0.023438            2 
# Runtime: 15.021954 s (overhead: 0.000087 %) 5 records
