# Sysname : Linux
# Nodename: eu-a6-003-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 04:30:32 2022
# Execution time and date (local): Sat Jan  8 05:30:32 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           256           128           MPI          0 5.039062            1 
            3           256           128           MPI          1 0.203125            4 
            3           256           128           MPI          2 0.031250            1 
            3           256           128           MPI          3 0.013672            0 
            3           256           128           MPI          4 0.023438            3 
# Runtime: 14.995161 s (overhead: 0.000060 %) 5 records
