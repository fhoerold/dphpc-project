# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:29:41 2022
# Execution time and date (local): Fri Jan  7 17:29:41 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3           512           128           MPI          0 11590.074219            0 
            3           512           128           MPI          1 11444.861328            1 
            3           512           128           MPI          2 11428.486328            0 
            3           512           128           MPI          3 11414.865234            0 
            3           512           128           MPI          4 11434.302734            0 
# Runtime: 5.150137 s (overhead: 0.000019 %) 5 records
