# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:29:41 2022
# Execution time and date (local): Fri Jan  7 17:29:41 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2           512           128           MPI          0 11563.177734            0 
            2           512           128           MPI          1 11415.179688            1 
            2           512           128           MPI          2 11399.304688            1 
            2           512           128           MPI          3 11385.250000            0 
            2           512           128           MPI          4 11409.353516            1 
# Runtime: 0.152651 s (overhead: 0.001965 %) 5 records
