Invocation: ./cholesky/benchmark.sh -o ./benchmark --mpi -d 8 9 10 11 12 13 -p 1 2 4 8 16 32 --blocksize 128 ./cholesky/build/cholesky_blocked_mpi
Benchmark 'cholesky_blocked_mpi' for
> Dimensions:		256, 512, 1024, 2048, 4096, 8192, 
> Number of processors:	1, 2, 4, 8, 16, 32, 
> CPU Model:		XeonGold_6150
> Use MPI:		true
> Use OpenMP:		false

Timestamp: 2022-01-07_17-22-47
