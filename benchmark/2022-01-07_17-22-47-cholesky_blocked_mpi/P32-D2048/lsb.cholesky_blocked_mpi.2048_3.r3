# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 11:42:24 2022
# Execution time and date (local): Sat Jan  8 12:42:24 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            3          2048           128           MPI          0 421689.808594            0 
            3          2048           128           MPI          1 419972.144531            1 
            3          2048           128           MPI          2 422672.685547            1 
            3          2048           128           MPI          3 421192.542969            0 
            3          2048           128           MPI          4 420925.728516            1 
# Runtime: 11.666289 s (overhead: 0.000026 %) 5 records
