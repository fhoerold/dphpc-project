# Sysname : Linux
# Nodename: eu-a6-011-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 11:42:24 2022
# Execution time and date (local): Sat Jan  8 12:42:24 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size    blocksize         type       id         time     overhead 
            2          2048           128           MPI          0 420763.189453            0 
            2          2048           128           MPI          1 419449.171875            2 
            2          2048           128           MPI          2 421725.904297            2 
            2          2048           128           MPI          3 420656.199219            0 
            2          2048           128           MPI          4 420360.683594            1 
# Runtime: 11.668754 s (overhead: 0.000043 %) 5 records
