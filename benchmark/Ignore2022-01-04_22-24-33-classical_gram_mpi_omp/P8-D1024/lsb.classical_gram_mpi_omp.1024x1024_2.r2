# Sysname : Linux
# Nodename: eu-a6-008-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:18 2022
# Execution time and date (local): Tue Jan  4 22:26:18 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 522312.035156            0 
            2          1024          1024   gramschmidt           MPI          1 521452.025391            4 
            2          1024          1024   gramschmidt           MPI          2 551372.867188            1 
            2          1024          1024   gramschmidt           MPI          3 527843.242188            0 
            2          1024          1024   gramschmidt           MPI          4 523258.337891            1 
            2          1024          1024   gramschmidt           MPI          5 527874.001953            0 
            2          1024          1024   gramschmidt           MPI          6 530512.052734            0 
            2          1024          1024   gramschmidt           MPI          7 535874.072266            0 
            2          1024          1024   gramschmidt           MPI          8 522514.607422            5 
            2          1024          1024   gramschmidt           MPI          9 522840.613281            0 
# Runtime: 6.884283 s (overhead: 0.000160 %) 10 records
