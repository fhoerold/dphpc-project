# Sysname : Linux
# Nodename: eu-a6-004-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:27:03 2022
# Execution time and date (local): Tue Jan  4 22:27:03 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 8195540.830078            0 
            2          2048          2048   gramschmidt           MPI          1 8161930.796875            3 
            2          2048          2048   gramschmidt           MPI          2 8198175.996094            1 
            2          2048          2048   gramschmidt           MPI          3 8186196.853516            0 
            2          2048          2048   gramschmidt           MPI          4 8137376.914062            2 
            2          2048          2048   gramschmidt           MPI          5 8171432.619141            0 
            2          2048          2048   gramschmidt           MPI          6 8153000.783203            0 
            2          2048          2048   gramschmidt           MPI          7 8152685.333984            0 
            2          2048          2048   gramschmidt           MPI          8 8149141.486328            4 
            2          2048          2048   gramschmidt           MPI          9 8072571.087891            0 
# Runtime: 108.166961 s (overhead: 0.000009 %) 10 records
