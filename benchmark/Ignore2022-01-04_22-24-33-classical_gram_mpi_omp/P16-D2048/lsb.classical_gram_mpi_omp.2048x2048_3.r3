# Sysname : Linux
# Nodename: eu-a6-004-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:27:03 2022
# Execution time and date (local): Tue Jan  4 22:27:03 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 8197687.673828            0 
            3          2048          2048   gramschmidt           MPI          1 8163315.841797            4 
            3          2048          2048   gramschmidt           MPI          2 8200357.917969            2 
            3          2048          2048   gramschmidt           MPI          3 8187499.013672            0 
            3          2048          2048   gramschmidt           MPI          4 8138780.681641            2 
            3          2048          2048   gramschmidt           MPI          5 8172878.240234            0 
            3          2048          2048   gramschmidt           MPI          6 8155244.841797            0 
            3          2048          2048   gramschmidt           MPI          7 8154100.833984            0 
            3          2048          2048   gramschmidt           MPI          8 8150549.851562            5 
            3          2048          2048   gramschmidt           MPI          9 8073838.720703            0 
# Runtime: 109.167758 s (overhead: 0.000012 %) 10 records
