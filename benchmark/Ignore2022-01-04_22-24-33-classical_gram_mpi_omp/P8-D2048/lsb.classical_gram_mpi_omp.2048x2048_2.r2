# Sysname : Linux
# Nodename: eu-a6-008-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:32 2022
# Execution time and date (local): Tue Jan  4 22:26:32 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 8067726.259766            0 
            2          2048          2048   gramschmidt           MPI          1 8074417.775391            5 
            2          2048          2048   gramschmidt           MPI          2 8092613.306641            1 
            2          2048          2048   gramschmidt           MPI          3 8088631.289062            0 
            2          2048          2048   gramschmidt           MPI          4 8083876.001953            1 
            2          2048          2048   gramschmidt           MPI          5 8047958.248047            0 
            2          2048          2048   gramschmidt           MPI          6 8086689.978516            0 
            2          2048          2048   gramschmidt           MPI          7 8063078.097656            0 
            2          2048          2048   gramschmidt           MPI          8 8067302.136719            6 
            2          2048          2048   gramschmidt           MPI          9 8029222.335938            0 
# Runtime: 106.954135 s (overhead: 0.000012 %) 10 records
