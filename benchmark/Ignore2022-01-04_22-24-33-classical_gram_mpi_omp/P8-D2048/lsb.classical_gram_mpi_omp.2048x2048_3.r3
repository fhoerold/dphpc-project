# Sysname : Linux
# Nodename: eu-a6-008-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:32 2022
# Execution time and date (local): Tue Jan  4 22:26:32 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 8069518.056641            1 
            3          2048          2048   gramschmidt           MPI          1 8075895.621094            5 
            3          2048          2048   gramschmidt           MPI          2 8093943.257812            1 
            3          2048          2048   gramschmidt           MPI          3 8089538.476562            0 
            3          2048          2048   gramschmidt           MPI          4 8086070.857422            5 
            3          2048          2048   gramschmidt           MPI          5 8049616.269531            0 
            3          2048          2048   gramschmidt           MPI          6 8088021.335938            0 
            3          2048          2048   gramschmidt           MPI          7 8064383.984375            0 
            3          2048          2048   gramschmidt           MPI          8 8069512.326172            5 
            3          2048          2048   gramschmidt           MPI          9 8030596.666016            0 
# Runtime: 109.954564 s (overhead: 0.000015 %) 10 records
