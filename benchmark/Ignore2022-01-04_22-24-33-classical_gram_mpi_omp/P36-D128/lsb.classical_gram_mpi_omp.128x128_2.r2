# Sysname : Linux
# Nodename: eu-a6-009-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:34:22 2022
# Execution time and date (local): Tue Jan  4 23:34:22 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 29556.400391            0 
            2           128           128   gramschmidt           MPI          1 29596.962891            0 
            2           128           128   gramschmidt           MPI          2 29681.691406            0 
            2           128           128   gramschmidt           MPI          3 29028.974609            0 
            2           128           128   gramschmidt           MPI          4 29532.332031            0 
            2           128           128   gramschmidt           MPI          5 29665.025391            0 
            2           128           128   gramschmidt           MPI          6 29264.142578            0 
            2           128           128   gramschmidt           MPI          7 29112.031250            0 
            2           128           128   gramschmidt           MPI          8 29442.898438            0 
            2           128           128   gramschmidt           MPI          9 29040.001953            0 
# Runtime: 0.388062 s (overhead: 0.000000 %) 10 records
