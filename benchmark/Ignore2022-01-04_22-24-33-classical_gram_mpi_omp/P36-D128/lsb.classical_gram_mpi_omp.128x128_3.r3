# Sysname : Linux
# Nodename: eu-a6-009-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:34:22 2022
# Execution time and date (local): Tue Jan  4 23:34:22 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 29541.140625            0 
            3           128           128   gramschmidt           MPI          1 29578.386719            0 
            3           128           128   gramschmidt           MPI          2 29661.427734            0 
            3           128           128   gramschmidt           MPI          3 28999.554688            0 
            3           128           128   gramschmidt           MPI          4 29515.056641            0 
            3           128           128   gramschmidt           MPI          5 29642.544922            0 
            3           128           128   gramschmidt           MPI          6 29242.369141            0 
            3           128           128   gramschmidt           MPI          7 29082.683594            0 
            3           128           128   gramschmidt           MPI          8 29420.349609            0 
            3           128           128   gramschmidt           MPI          9 29018.759766            0 
# Runtime: 0.387126 s (overhead: 0.000000 %) 10 records
