# Sysname : Linux
# Nodename: eu-a6-004-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:32 2022
# Execution time and date (local): Tue Jan  4 22:26:32 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 2052.755859            0 
            2            32            32   gramschmidt           MPI          1 2062.705078            3 
            2            32            32   gramschmidt           MPI          2 2063.191406            0 
            2            32            32   gramschmidt           MPI          3 1997.384766            0 
            2            32            32   gramschmidt           MPI          4 1999.464844            0 
            2            32            32   gramschmidt           MPI          5 1999.781250            0 
            2            32            32   gramschmidt           MPI          6 2028.392578            0 
            2            32            32   gramschmidt           MPI          7 2028.843750            0 
            2            32            32   gramschmidt           MPI          8 2055.341797            0 
            2            32            32   gramschmidt           MPI          9 2118.603516            0 
# Runtime: 3.028101 s (overhead: 0.000099 %) 10 records
