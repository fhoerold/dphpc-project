# Sysname : Linux
# Nodename: eu-a6-001-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:12 2022
# Execution time and date (local): Tue Jan  4 22:26:12 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 62082.996094            0 
            3           512           512   gramschmidt           MPI          1 61777.890625            5 
            3           512           512   gramschmidt           MPI          2 61874.031250            1 
            3           512           512   gramschmidt           MPI          3 61792.283203            0 
            3           512           512   gramschmidt           MPI          4 61768.345703            1 
            3           512           512   gramschmidt           MPI          5 61874.302734            0 
            3           512           512   gramschmidt           MPI          6 61667.369141            0 
            3           512           512   gramschmidt           MPI          7 61712.976562            0 
            3           512           512   gramschmidt           MPI          8 61252.044922            1 
            3           512           512   gramschmidt           MPI          9 61938.783203            0 
# Runtime: 3.811071 s (overhead: 0.000210 %) 10 records
