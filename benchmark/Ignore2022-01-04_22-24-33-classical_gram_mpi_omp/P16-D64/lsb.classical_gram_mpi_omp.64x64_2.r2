# Sysname : Linux
# Nodename: eu-a6-004-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:46 2022
# Execution time and date (local): Tue Jan  4 22:26:46 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 4380.410156            0 
            2            64            64   gramschmidt           MPI          1 4191.949219            2 
            2            64            64   gramschmidt           MPI          2 4221.029297            0 
            2            64            64   gramschmidt           MPI          3 4311.193359            0 
            2            64            64   gramschmidt           MPI          4 4254.589844            0 
            2            64            64   gramschmidt           MPI          5 4142.103516            0 
            2            64            64   gramschmidt           MPI          6 4174.132812            0 
            2            64            64   gramschmidt           MPI          7 4422.093750            0 
            2            64            64   gramschmidt           MPI          8 4284.798828            0 
            2            64            64   gramschmidt           MPI          9 4272.580078            0 
# Runtime: 2.056635 s (overhead: 0.000097 %) 10 records
