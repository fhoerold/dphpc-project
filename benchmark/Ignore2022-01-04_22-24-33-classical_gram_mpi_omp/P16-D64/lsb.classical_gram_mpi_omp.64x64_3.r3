# Sysname : Linux
# Nodename: eu-a6-004-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:46 2022
# Execution time and date (local): Tue Jan  4 22:26:46 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 4378.884766            0 
            3            64            64   gramschmidt           MPI          1 4196.619141            2 
            3            64            64   gramschmidt           MPI          2 4231.197266            0 
            3            64            64   gramschmidt           MPI          3 4315.173828            0 
            3            64            64   gramschmidt           MPI          4 4264.171875            0 
            3            64            64   gramschmidt           MPI          5 4154.775391            0 
            3            64            64   gramschmidt           MPI          6 4176.714844            0 
            3            64            64   gramschmidt           MPI          7 4433.125000            0 
            3            64            64   gramschmidt           MPI          8 4290.373047            0 
            3            64            64   gramschmidt           MPI          9 4274.585938            0 
# Runtime: 2.057164 s (overhead: 0.000097 %) 10 records
