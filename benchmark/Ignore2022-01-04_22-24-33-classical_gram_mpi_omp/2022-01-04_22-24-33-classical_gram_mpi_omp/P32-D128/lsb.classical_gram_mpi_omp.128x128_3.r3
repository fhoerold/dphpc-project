# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:00:20 2022
# Execution time and date (local): Tue Jan  4 23:00:20 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 23173.603516            0 
            3           128           128   gramschmidt           MPI          1 23279.878906            1 
            3           128           128   gramschmidt           MPI          2 23235.394531            1 
            3           128           128   gramschmidt           MPI          3 23122.576172            0 
            3           128           128   gramschmidt           MPI          4 23221.796875            0 
            3           128           128   gramschmidt           MPI          5 23035.884766            0 
            3           128           128   gramschmidt           MPI          6 23348.265625            0 
            3           128           128   gramschmidt           MPI          7 23027.777344            0 
            3           128           128   gramschmidt           MPI          8 23237.167969            0 
            3           128           128   gramschmidt           MPI          9 23152.546875            0 
# Runtime: 0.304805 s (overhead: 0.000656 %) 10 records
