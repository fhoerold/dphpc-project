# Sysname : Linux
# Nodename: eu-a6-010-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:09 2022
# Execution time and date (local): Tue Jan  4 22:26:09 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 161849545.175781            1 
            3          4096          4096   gramschmidt           MPI          1 162242907.677734            8 
            3          4096          4096   gramschmidt           MPI          2 161395855.972656           10 
            3          4096          4096   gramschmidt           MPI          3 161421512.037109            1 
            3          4096          4096   gramschmidt           MPI          4 161604655.414062           10 
            3          4096          4096   gramschmidt           MPI          5 161270554.652344            3 
            3          4096          4096   gramschmidt           MPI          6 161906206.291016            3 
            3          4096          4096   gramschmidt           MPI          7 161995734.023438            3 
            3          4096          4096   gramschmidt           MPI          8 161613058.763672           10 
            3          4096          4096   gramschmidt           MPI          9 152066367.486328            0 
# Runtime: 2098.430922 s (overhead: 0.000002 %) 10 records
