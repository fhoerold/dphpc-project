# Sysname : Linux
# Nodename: eu-a6-010-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:09 2022
# Execution time and date (local): Tue Jan  4 22:26:09 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 161838350.119141            1 
            2          4096          4096   gramschmidt           MPI          1 162231413.337891            6 
            2          4096          4096   gramschmidt           MPI          2 161384135.164062            5 
            2          4096          4096   gramschmidt           MPI          3 161410026.652344            1 
            2          4096          4096   gramschmidt           MPI          4 161593495.564453            6 
            2          4096          4096   gramschmidt           MPI          5 161258687.728516            0 
            2          4096          4096   gramschmidt           MPI          6 161894869.904297            0 
            2          4096          4096   gramschmidt           MPI          7 161984763.529297            0 
            2          4096          4096   gramschmidt           MPI          8 161601659.554688            5 
            2          4096          4096   gramschmidt           MPI          9 152055286.472656            0 
# Runtime: 2098.413186 s (overhead: 0.000001 %) 10 records
