# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:31:25 2022
# Execution time and date (local): Tue Jan  4 22:31:25 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 7786593.410156            0 
            3          2048          2048   gramschmidt           MPI          1 7772771.902344            7 
            3          2048          2048   gramschmidt           MPI          2 7761113.523438            1 
            3          2048          2048   gramschmidt           MPI          3 7758177.705078            0 
            3          2048          2048   gramschmidt           MPI          4 7766072.154297            5 
            3          2048          2048   gramschmidt           MPI          5 7779947.617188            0 
            3          2048          2048   gramschmidt           MPI          6 7773614.992188            0 
            3          2048          2048   gramschmidt           MPI          7 7731253.621094            0 
            3          2048          2048   gramschmidt           MPI          8 7758165.431641            1 
            3          2048          2048   gramschmidt           MPI          9 7777238.673828            0 
# Runtime: 102.122605 s (overhead: 0.000014 %) 10 records
