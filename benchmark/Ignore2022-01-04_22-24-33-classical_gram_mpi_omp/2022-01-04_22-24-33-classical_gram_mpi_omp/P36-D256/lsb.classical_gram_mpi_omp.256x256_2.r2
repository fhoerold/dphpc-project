# Sysname : Linux
# Nodename: eu-a6-006-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 23:05:14 2022
# Execution time and date (local): Wed Jan  5 00:05:14 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 62715.005859            0 
            2           256           256   gramschmidt           MPI          1 62674.375000            3 
            2           256           256   gramschmidt           MPI          2 63186.648438            1 
            2           256           256   gramschmidt           MPI          3 63706.185547            0 
            2           256           256   gramschmidt           MPI          4 62787.835938            0 
            2           256           256   gramschmidt           MPI          5 62701.486328            0 
            2           256           256   gramschmidt           MPI          6 63132.292969            0 
            2           256           256   gramschmidt           MPI          7 62398.792969            0 
            2           256           256   gramschmidt           MPI          8 62625.994141            1 
            2           256           256   gramschmidt           MPI          9 62851.564453            0 
# Runtime: 15.822521 s (overhead: 0.000032 %) 10 records
