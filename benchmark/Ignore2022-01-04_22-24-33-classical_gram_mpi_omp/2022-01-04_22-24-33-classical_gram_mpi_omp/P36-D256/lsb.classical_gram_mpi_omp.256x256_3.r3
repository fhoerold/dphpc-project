# Sysname : Linux
# Nodename: eu-a6-006-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 23:05:14 2022
# Execution time and date (local): Wed Jan  5 00:05:14 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 62462.775391            0 
            3           256           256   gramschmidt           MPI          1 62452.273438            0 
            3           256           256   gramschmidt           MPI          2 63003.542969            0 
            3           256           256   gramschmidt           MPI          3 63439.509766            0 
            3           256           256   gramschmidt           MPI          4 62549.300781            1 
            3           256           256   gramschmidt           MPI          5 62506.951172            0 
            3           256           256   gramschmidt           MPI          6 62832.677734            0 
            3           256           256   gramschmidt           MPI          7 62162.646484            0 
            3           256           256   gramschmidt           MPI          8 62431.085938            0 
            3           256           256   gramschmidt           MPI          9 62572.281250            0 
# Runtime: 0.820399 s (overhead: 0.000122 %) 10 records
