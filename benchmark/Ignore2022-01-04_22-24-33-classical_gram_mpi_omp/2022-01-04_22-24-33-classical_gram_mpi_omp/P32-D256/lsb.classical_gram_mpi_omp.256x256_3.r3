# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:03:24 2022
# Execution time and date (local): Tue Jan  4 23:03:24 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 49832.169922            0 
            3           256           256   gramschmidt           MPI          1 50041.644531            5 
            3           256           256   gramschmidt           MPI          2 50081.640625            1 
            3           256           256   gramschmidt           MPI          3 50131.023438            0 
            3           256           256   gramschmidt           MPI          4 50150.849609            1 
            3           256           256   gramschmidt           MPI          5 50633.722656            0 
            3           256           256   gramschmidt           MPI          6 50118.515625            0 
            3           256           256   gramschmidt           MPI          7 50274.746094            0 
            3           256           256   gramschmidt           MPI          8 50268.697266            1 
            3           256           256   gramschmidt           MPI          9 50373.541016            0 
# Runtime: 2.660995 s (overhead: 0.000301 %) 10 records
