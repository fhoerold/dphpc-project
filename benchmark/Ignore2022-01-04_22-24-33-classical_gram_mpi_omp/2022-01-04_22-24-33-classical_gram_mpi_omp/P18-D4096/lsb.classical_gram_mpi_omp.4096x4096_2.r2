# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:34:02 2022
# Execution time and date (local): Tue Jan  4 22:34:02 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 117055041.539062            0 
            2          4096          4096   gramschmidt           MPI          1 117582600.566406            4 
            2          4096          4096   gramschmidt           MPI          2 118098018.925781            4 
            2          4096          4096   gramschmidt           MPI          3 117660159.949219            3 
            2          4096          4096   gramschmidt           MPI          4 117722831.535156            7 
            2          4096          4096   gramschmidt           MPI          5 116794717.310547            0 
            2          4096          4096   gramschmidt           MPI          6 118596705.945312            0 
            2          4096          4096   gramschmidt           MPI          7 116346967.298828            0 
            2          4096          4096   gramschmidt           MPI          8 117689560.500000            4 
            2          4096          4096   gramschmidt           MPI          9 118020365.781250            3 
# Runtime: 1527.663459 s (overhead: 0.000002 %) 10 records
