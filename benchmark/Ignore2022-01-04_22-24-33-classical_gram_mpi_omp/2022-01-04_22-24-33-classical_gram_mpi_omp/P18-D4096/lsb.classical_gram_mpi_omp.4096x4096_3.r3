# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:34:02 2022
# Execution time and date (local): Tue Jan  4 22:34:02 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 117062386.589844            0 
            3          4096          4096   gramschmidt           MPI          1 117589950.736328            4 
            3          4096          4096   gramschmidt           MPI          2 118105408.839844            4 
            3          4096          4096   gramschmidt           MPI          3 117667824.994141            0 
            3          4096          4096   gramschmidt           MPI          4 117730135.638672            5 
            3          4096          4096   gramschmidt           MPI          5 116802050.298828            2 
            3          4096          4096   gramschmidt           MPI          6 118604085.855469            2 
            3          4096          4096   gramschmidt           MPI          7 116354247.929688            2 
            3          4096          4096   gramschmidt           MPI          8 117696909.070312            6 
            3          4096          4096   gramschmidt           MPI          9 118027806.347656            2 
# Runtime: 1529.663240 s (overhead: 0.000002 %) 10 records
