# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:25:34 2022
# Execution time and date (local): Tue Jan  4 22:25:34 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 108267.072266            0 
            2           512           512   gramschmidt           MPI          1 95270.654297            3 
            2           512           512   gramschmidt           MPI          2 99815.679688            1 
            2           512           512   gramschmidt           MPI          3 95330.458984            0 
            2           512           512   gramschmidt           MPI          4 96987.037109            2 
            2           512           512   gramschmidt           MPI          5 95296.972656            0 
            2           512           512   gramschmidt           MPI          6 96076.445312            0 
            2           512           512   gramschmidt           MPI          7 93182.554688            0 
            2           512           512   gramschmidt           MPI          8 94624.212891            2 
            2           512           512   gramschmidt           MPI          9 92510.742188            0 
# Runtime: 2.273486 s (overhead: 0.000352 %) 10 records
