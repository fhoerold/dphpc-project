# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:25:34 2022
# Execution time and date (local): Tue Jan  4 22:25:34 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 107802.433594            0 
            3           512           512   gramschmidt           MPI          1 94814.109375            1 
            3           512           512   gramschmidt           MPI          2 99370.177734            1 
            3           512           512   gramschmidt           MPI          3 94881.554688            0 
            3           512           512   gramschmidt           MPI          4 96515.156250            1 
            3           512           512   gramschmidt           MPI          5 94832.361328            0 
            3           512           512   gramschmidt           MPI          6 95629.996094            0 
            3           512           512   gramschmidt           MPI          7 92749.072266            0 
            3           512           512   gramschmidt           MPI          8 94181.968750            5 
            3           512           512   gramschmidt           MPI          9 92068.966797            0 
# Runtime: 1.259460 s (overhead: 0.000635 %) 10 records
