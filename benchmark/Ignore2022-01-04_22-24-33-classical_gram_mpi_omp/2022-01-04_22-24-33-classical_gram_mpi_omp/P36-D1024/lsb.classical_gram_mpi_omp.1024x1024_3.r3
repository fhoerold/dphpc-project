# Sysname : Linux
# Nodename: eu-a6-009-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:34:31 2022
# Execution time and date (local): Tue Jan  4 23:34:31 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 1154045.396484            4 
            3          1024          1024   gramschmidt           MPI          1 1155971.550781            4 
            3          1024          1024   gramschmidt           MPI          2 1168176.708984            4 
            3          1024          1024   gramschmidt           MPI          3 1152183.285156            0 
            3          1024          1024   gramschmidt           MPI          4 1156576.884766            1 
            3          1024          1024   gramschmidt           MPI          5 1148097.835938            0 
            3          1024          1024   gramschmidt           MPI          6 1158055.148438            0 
            3          1024          1024   gramschmidt           MPI          7 1151300.039062            0 
            3          1024          1024   gramschmidt           MPI          8 1153606.875000            4 
            3          1024          1024   gramschmidt           MPI          9 1150223.660156            0 
# Runtime: 14.998937 s (overhead: 0.000113 %) 10 records
