# Sysname : Linux
# Nodename: eu-a6-009-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:34:31 2022
# Execution time and date (local): Tue Jan  4 23:34:31 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 1154843.228516            0 
            2          1024          1024   gramschmidt           MPI          1 1156785.878906            4 
            2          1024          1024   gramschmidt           MPI          2 1168966.673828            4 
            2          1024          1024   gramschmidt           MPI          3 1153001.232422            0 
            2          1024          1024   gramschmidt           MPI          4 1157409.130859            1 
            2          1024          1024   gramschmidt           MPI          5 1148898.923828            0 
            2          1024          1024   gramschmidt           MPI          6 1158882.951172            0 
            2          1024          1024   gramschmidt           MPI          7 1152107.863281            0 
            2          1024          1024   gramschmidt           MPI          8 1154445.263672            5 
            2          1024          1024   gramschmidt           MPI          9 1150830.621094            0 
# Runtime: 15.001547 s (overhead: 0.000093 %) 10 records
