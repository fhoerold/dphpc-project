# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:04:37 2022
# Execution time and date (local): Tue Jan  4 23:04:37 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 135083581.318359            0 
            3          4096          4096   gramschmidt           MPI          1 148242622.562500            6 
            3          4096          4096   gramschmidt           MPI          2 141650140.226562            4 
            3          4096          4096   gramschmidt           MPI          3 141767848.617188            0 
            3          4096          4096   gramschmidt           MPI          4 139894105.562500            7 
            3          4096          4096   gramschmidt           MPI          5 126436659.837891            0 
            3          4096          4096   gramschmidt           MPI          6 128557122.869141            0 
            3          4096          4096   gramschmidt           MPI          7 122798508.861328            0 
            3          4096          4096   gramschmidt           MPI          8 116579236.908203            8 
            3          4096          4096   gramschmidt           MPI          9 123746464.333984            0 
# Runtime: 1703.511485 s (overhead: 0.000001 %) 10 records
