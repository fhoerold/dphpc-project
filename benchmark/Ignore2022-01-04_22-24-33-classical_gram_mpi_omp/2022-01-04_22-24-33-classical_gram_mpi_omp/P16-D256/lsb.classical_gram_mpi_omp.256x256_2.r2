# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:29:03 2022
# Execution time and date (local): Tue Jan  4 22:29:03 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 21624.298828            0 
            2           256           256   gramschmidt           MPI          1 21466.541016            3 
            2           256           256   gramschmidt           MPI          2 21208.470703            0 
            2           256           256   gramschmidt           MPI          3 21323.787109            0 
            2           256           256   gramschmidt           MPI          4 21329.671875            0 
            2           256           256   gramschmidt           MPI          5 21510.654297            0 
            2           256           256   gramschmidt           MPI          6 21236.861328            0 
            2           256           256   gramschmidt           MPI          7 21164.060547            0 
            2           256           256   gramschmidt           MPI          8 21311.757812            0 
            2           256           256   gramschmidt           MPI          9 21361.492188            0 
# Runtime: 2.281732 s (overhead: 0.000131 %) 10 records
