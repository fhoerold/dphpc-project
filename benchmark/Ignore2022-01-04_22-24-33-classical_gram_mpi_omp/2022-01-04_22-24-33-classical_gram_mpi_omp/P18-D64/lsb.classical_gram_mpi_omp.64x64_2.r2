# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:30:12 2022
# Execution time and date (local): Tue Jan  4 22:30:12 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 4442.720703            0 
            2            64            64   gramschmidt           MPI          1 4389.658203            0 
            2            64            64   gramschmidt           MPI          2 4382.371094            0 
            2            64            64   gramschmidt           MPI          3 4372.560547            0 
            2            64            64   gramschmidt           MPI          4 4337.562500            0 
            2            64            64   gramschmidt           MPI          5 4352.443359            0 
            2            64            64   gramschmidt           MPI          6 4289.064453            0 
            2            64            64   gramschmidt           MPI          7 4306.658203            0 
            2            64            64   gramschmidt           MPI          8 4397.841797            0 
            2            64            64   gramschmidt           MPI          9 4669.251953            0 
# Runtime: 0.060330 s (overhead: 0.000000 %) 10 records
