# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:25:34 2022
# Execution time and date (local): Tue Jan  4 22:25:34 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 637.429688            0 
            2            64            64   gramschmidt           MPI          1 655.375000            3 
            2            64            64   gramschmidt           MPI          2 638.443359            0 
            2            64            64   gramschmidt           MPI          3 559.623047            0 
            2            64            64   gramschmidt           MPI          4 651.179688            0 
            2            64            64   gramschmidt           MPI          5 584.888672            0 
            2            64            64   gramschmidt           MPI          6 613.759766            0 
            2            64            64   gramschmidt           MPI          7 669.847656            0 
            2            64            64   gramschmidt           MPI          8 610.925781            0 
            2            64            64   gramschmidt           MPI          9 586.798828            0 
# Runtime: 1.006115 s (overhead: 0.000298 %) 10 records
