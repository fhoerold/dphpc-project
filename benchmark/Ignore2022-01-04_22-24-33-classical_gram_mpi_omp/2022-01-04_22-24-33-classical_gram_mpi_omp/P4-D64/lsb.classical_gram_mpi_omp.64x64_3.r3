# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:25:34 2022
# Execution time and date (local): Tue Jan  4 22:25:34 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 640.123047            0 
            3            64            64   gramschmidt           MPI          1 659.132812            0 
            3            64            64   gramschmidt           MPI          2 648.974609            0 
            3            64            64   gramschmidt           MPI          3 571.445312            0 
            3            64            64   gramschmidt           MPI          4 657.763672            0 
            3            64            64   gramschmidt           MPI          5 595.218750            0 
            3            64            64   gramschmidt           MPI          6 624.375000            0 
            3            64            64   gramschmidt           MPI          7 679.875000            0 
            3            64            64   gramschmidt           MPI          8 615.966797            0 
            3            64            64   gramschmidt           MPI          9 594.093750            0 
# Runtime: 0.009790 s (overhead: 0.000000 %) 10 records
