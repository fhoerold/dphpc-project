# Sysname : Linux
# Nodename: eu-a6-001-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:12 2022
# Execution time and date (local): Tue Jan  4 22:26:12 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 62310.972656            0 
            2           512           512   gramschmidt           MPI          1 61987.271484            4 
            2           512           512   gramschmidt           MPI          2 62088.080078            1 
            2           512           512   gramschmidt           MPI          3 62014.597656            0 
            2           512           512   gramschmidt           MPI          4 61974.462891            1 
            2           512           512   gramschmidt           MPI          5 62082.187500            0 
            2           512           512   gramschmidt           MPI          6 61897.791016            0 
            2           512           512   gramschmidt           MPI          7 61937.134766            0 
            2           512           512   gramschmidt           MPI          8 61457.345703            1 
            2           512           512   gramschmidt           MPI          9 62147.169922            0 
# Runtime: 5.811955 s (overhead: 0.000120 %) 10 records
