# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:00:05 2022
# Execution time and date (local): Tue Jan  4 23:00:05 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 5585.044922            0 
            2            32            32   gramschmidt           MPI          1 5689.228516            1 
            2            32            32   gramschmidt           MPI          2 5613.781250            0 
            2            32            32   gramschmidt           MPI          3 5549.068359            0 
            2            32            32   gramschmidt           MPI          4 5571.220703            0 
            2            32            32   gramschmidt           MPI          5 5591.658203            0 
            2            32            32   gramschmidt           MPI          6 5552.023438            0 
            2            32            32   gramschmidt           MPI          7 5556.453125            0 
            2            32            32   gramschmidt           MPI          8 5546.941406            0 
            2            32            32   gramschmidt           MPI          9 5528.234375            0 
# Runtime: 0.074484 s (overhead: 0.001343 %) 10 records
