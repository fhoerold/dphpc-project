# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:00:05 2022
# Execution time and date (local): Tue Jan  4 23:00:05 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 5577.347656            0 
            3            32            32   gramschmidt           MPI          1 5680.298828            4 
            3            32            32   gramschmidt           MPI          2 5613.085938            0 
            3            32            32   gramschmidt           MPI          3 5536.466797            0 
            3            32            32   gramschmidt           MPI          4 5567.732422            0 
            3            32            32   gramschmidt           MPI          5 5585.974609            0 
            3            32            32   gramschmidt           MPI          6 5543.353516            0 
            3            32            32   gramschmidt           MPI          7 5547.351562            0 
            3            32            32   gramschmidt           MPI          8 5541.933594            0 
            3            32            32   gramschmidt           MPI          9 5523.576172            0 
# Runtime: 7.074806 s (overhead: 0.000057 %) 10 records
