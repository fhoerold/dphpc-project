# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:03:53 2022
# Execution time and date (local): Tue Jan  4 23:03:53 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 1098167.601562            0 
            2          1024          1024   gramschmidt           MPI          1 1054034.531250            5 
            2          1024          1024   gramschmidt           MPI          2 1053655.273438            2 
            2          1024          1024   gramschmidt           MPI          3 1058433.341797            0 
            2          1024          1024   gramschmidt           MPI          4 1058624.005859            2 
            2          1024          1024   gramschmidt           MPI          5 1082520.455078            0 
            2          1024          1024   gramschmidt           MPI          6 1050494.201172            0 
            2          1024          1024   gramschmidt           MPI          7 1079399.416016            0 
            2          1024          1024   gramschmidt           MPI          8 1058515.916016            2 
            2          1024          1024   gramschmidt           MPI          9 1059646.519531            0 
# Runtime: 21.888207 s (overhead: 0.000050 %) 10 records
