# Sysname : Linux
# Nodename: eu-a6-008-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:09 2022
# Execution time and date (local): Tue Jan  4 22:26:09 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 687.857422            0 
            3            64            64   gramschmidt           MPI          1 666.783203            0 
            3            64            64   gramschmidt           MPI          2 669.693359            0 
            3            64            64   gramschmidt           MPI          3 663.775391            0 
            3            64            64   gramschmidt           MPI          4 637.296875            0 
            3            64            64   gramschmidt           MPI          5 597.832031            0 
            3            64            64   gramschmidt           MPI          6 614.189453            0 
            3            64            64   gramschmidt           MPI          7 631.972656            0 
            3            64            64   gramschmidt           MPI          8 597.554688            0 
            3            64            64   gramschmidt           MPI          9 619.818359            0 
# Runtime: 0.010437 s (overhead: 0.000000 %) 10 records
