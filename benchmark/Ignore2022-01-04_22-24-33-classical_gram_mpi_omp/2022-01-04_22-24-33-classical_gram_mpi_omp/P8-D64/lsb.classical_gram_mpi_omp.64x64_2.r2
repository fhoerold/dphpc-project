# Sysname : Linux
# Nodename: eu-a6-008-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:09 2022
# Execution time and date (local): Tue Jan  4 22:26:09 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 673.677734            0 
            2            64            64   gramschmidt           MPI          1 658.595703            3 
            2            64            64   gramschmidt           MPI          2 660.849609            0 
            2            64            64   gramschmidt           MPI          3 647.626953            0 
            2            64            64   gramschmidt           MPI          4 617.570312            0 
            2            64            64   gramschmidt           MPI          5 584.269531            0 
            2            64            64   gramschmidt           MPI          6 612.203125            0 
            2            64            64   gramschmidt           MPI          7 626.175781            0 
            2            64            64   gramschmidt           MPI          8 592.515625            0 
            2            64            64   gramschmidt           MPI          9 611.404297            0 
# Runtime: 3.011287 s (overhead: 0.000100 %) 10 records
