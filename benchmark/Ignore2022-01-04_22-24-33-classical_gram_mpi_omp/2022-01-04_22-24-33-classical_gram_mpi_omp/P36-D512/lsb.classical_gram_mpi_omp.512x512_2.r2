# Sysname : Linux
# Nodename: eu-a6-006-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 23:05:38 2022
# Execution time and date (local): Wed Jan  5 00:05:38 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 163404.515625            0 
            2           512           512   gramschmidt           MPI          1 164132.125000            4 
            2           512           512   gramschmidt           MPI          2 161966.132812            1 
            2           512           512   gramschmidt           MPI          3 161313.964844            0 
            2           512           512   gramschmidt           MPI          4 161016.275391            1 
            2           512           512   gramschmidt           MPI          5 160427.625000            0 
            2           512           512   gramschmidt           MPI          6 160476.376953            0 
            2           512           512   gramschmidt           MPI          7 160812.541016            0 
            2           512           512   gramschmidt           MPI          8 161927.015625            1 
            2           512           512   gramschmidt           MPI          9 161013.958984            0 
# Runtime: 5.111323 s (overhead: 0.000137 %) 10 records
