# Sysname : Linux
# Nodename: eu-a6-004-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:32 2022
# Execution time and date (local): Tue Jan  4 22:26:32 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 2048.117188            1 
            3            32            32   gramschmidt           MPI          1 2055.380859            2 
            3            32            32   gramschmidt           MPI          2 2059.029297            0 
            3            32            32   gramschmidt           MPI          3 1987.140625            0 
            3            32            32   gramschmidt           MPI          4 1995.544922            0 
            3            32            32   gramschmidt           MPI          5 1994.750000            0 
            3            32            32   gramschmidt           MPI          6 2023.330078            0 
            3            32            32   gramschmidt           MPI          7 2026.296875            0 
            3            32            32   gramschmidt           MPI          8 2051.931641            0 
            3            32            32   gramschmidt           MPI          9 2113.765625            0 
# Runtime: 6.018931 s (overhead: 0.000050 %) 10 records
