# Sysname : Linux
# Nodename: eu-a6-009-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:34:10 2022
# Execution time and date (local): Tue Jan  4 23:34:10 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 14359.242188            0 
            2            64            64   gramschmidt           MPI          1 14687.500000            0 
            2            64            64   gramschmidt           MPI          2 14696.753906            0 
            2            64            64   gramschmidt           MPI          3 14557.427734            0 
            2            64            64   gramschmidt           MPI          4 14388.666016            0 
            2            64            64   gramschmidt           MPI          5 14219.478516            0 
            2            64            64   gramschmidt           MPI          6 14337.980469            0 
            2            64            64   gramschmidt           MPI          7 14204.771484            0 
            2            64            64   gramschmidt           MPI          8 14838.074219            0 
            2            64            64   gramschmidt           MPI          9 14607.300781            0 
# Runtime: 0.197862 s (overhead: 0.000000 %) 10 records
