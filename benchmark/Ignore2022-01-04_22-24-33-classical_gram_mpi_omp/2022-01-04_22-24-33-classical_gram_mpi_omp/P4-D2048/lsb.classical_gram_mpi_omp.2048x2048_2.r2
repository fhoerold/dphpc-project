# Sysname : Linux
# Nodename: eu-a6-011-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:05 2022
# Execution time and date (local): Tue Jan  4 22:26:05 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 13046587.839844            0 
            2          2048          2048   gramschmidt           MPI          1 13134474.996094            7 
            2          2048          2048   gramschmidt           MPI          2 13113809.214844            3 
            2          2048          2048   gramschmidt           MPI          3 12858204.529297            0 
            2          2048          2048   gramschmidt           MPI          4 13366147.998047            8 
            2          2048          2048   gramschmidt           MPI          5 13158643.628906            0 
            2          2048          2048   gramschmidt           MPI          6 13127389.951172            0 
            2          2048          2048   gramschmidt           MPI          7 13140526.849609            0 
            2          2048          2048   gramschmidt           MPI          8 13123520.255859            7 
            2          2048          2048   gramschmidt           MPI          9 12886752.355469            0 
# Runtime: 175.365769 s (overhead: 0.000014 %) 10 records
