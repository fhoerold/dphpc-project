# Sysname : Linux
# Nodename: eu-a6-008-06
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:09 2022
# Execution time and date (local): Tue Jan  4 22:26:09 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 7637.214844            0 
            2           256           256   gramschmidt           MPI          1 7537.054688            4 
            2           256           256   gramschmidt           MPI          2 7498.269531            1 
            2           256           256   gramschmidt           MPI          3 7451.662109            0 
            2           256           256   gramschmidt           MPI          4 7525.068359            0 
            2           256           256   gramschmidt           MPI          5 7565.728516            0 
            2           256           256   gramschmidt           MPI          6 7418.019531            0 
            2           256           256   gramschmidt           MPI          7 7459.984375            0 
            2           256           256   gramschmidt           MPI          8 7515.421875            0 
            2           256           256   gramschmidt           MPI          9 7555.396484            0 
# Runtime: 13.100852 s (overhead: 0.000038 %) 10 records
