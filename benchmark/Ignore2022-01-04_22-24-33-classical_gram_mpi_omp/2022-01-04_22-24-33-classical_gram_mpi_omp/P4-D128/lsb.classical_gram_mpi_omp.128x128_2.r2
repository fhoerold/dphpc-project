# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:25:33 2022
# Execution time and date (local): Tue Jan  4 22:25:33 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 1730.546875            0 
            2           128           128   gramschmidt           MPI          1 1668.214844            0 
            2           128           128   gramschmidt           MPI          2 1669.691406            0 
            2           128           128   gramschmidt           MPI          3 1642.937500            0 
            2           128           128   gramschmidt           MPI          4 1632.835938            0 
            2           128           128   gramschmidt           MPI          5 1631.140625            0 
            2           128           128   gramschmidt           MPI          6 1615.267578            0 
            2           128           128   gramschmidt           MPI          7 1643.818359            0 
            2           128           128   gramschmidt           MPI          8 1608.613281            0 
            2           128           128   gramschmidt           MPI          9 1626.992188            0 
# Runtime: 0.024689 s (overhead: 0.000000 %) 10 records
