# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:30:21 2022
# Execution time and date (local): Tue Jan  4 22:30:21 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 8711.789062            0 
            2           128           128   gramschmidt           MPI          1 8672.503906            2 
            2           128           128   gramschmidt           MPI          2 8604.546875            0 
            2           128           128   gramschmidt           MPI          3 8556.957031            0 
            2           128           128   gramschmidt           MPI          4 8658.933594            0 
            2           128           128   gramschmidt           MPI          5 8752.287109            0 
            2           128           128   gramschmidt           MPI          6 8669.451172            0 
            2           128           128   gramschmidt           MPI          7 8628.605469            0 
            2           128           128   gramschmidt           MPI          8 8941.519531            0 
            2           128           128   gramschmidt           MPI          9 8582.121094            0 
# Runtime: 19.116204 s (overhead: 0.000010 %) 10 records
