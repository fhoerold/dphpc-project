# Sysname : Linux
# Nodename: eu-a6-009-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:35:01 2022
# Execution time and date (local): Tue Jan  4 23:35:01 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 138949516.412109            0 
            3          4096          4096   gramschmidt           MPI          1 131158120.199219            5 
            3          4096          4096   gramschmidt           MPI          2 132684053.966797            6 
            3          4096          4096   gramschmidt           MPI          3 132835803.707031            0 
            3          4096          4096   gramschmidt           MPI          4 131110395.757812            5 
            3          4096          4096   gramschmidt           MPI          5 132148550.646484            0 
            3          4096          4096   gramschmidt           MPI          6 132846990.070312            0 
            3          4096          4096   gramschmidt           MPI          7 131776154.630859            0 
            3          4096          4096   gramschmidt           MPI          8 139712176.376953            8 
            3          4096          4096   gramschmidt           MPI          9 137187727.011719            2 
# Runtime: 1743.559519 s (overhead: 0.000001 %) 10 records
