# Sysname : Linux
# Nodename: eu-a6-009-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:35:01 2022
# Execution time and date (local): Tue Jan  4 23:35:01 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 138950254.138672            0 
            2          4096          4096   gramschmidt           MPI          1 131158426.583984            4 
            2          4096          4096   gramschmidt           MPI          2 132684343.718750            7 
            2          4096          4096   gramschmidt           MPI          3 132836124.937500            0 
            2          4096          4096   gramschmidt           MPI          4 131110718.646484            6 
            2          4096          4096   gramschmidt           MPI          5 132148944.994141            2 
            2          4096          4096   gramschmidt           MPI          6 132847307.244141            2 
            2          4096          4096   gramschmidt           MPI          7 131776512.798828            2 
            2          4096          4096   gramschmidt           MPI          8 139712814.814453            6 
            2          4096          4096   gramschmidt           MPI          9 137188310.300781            0 
# Runtime: 1743.661415 s (overhead: 0.000002 %) 10 records
