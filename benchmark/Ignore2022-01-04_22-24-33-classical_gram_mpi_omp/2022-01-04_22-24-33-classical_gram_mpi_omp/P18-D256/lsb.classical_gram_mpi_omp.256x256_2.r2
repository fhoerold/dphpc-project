# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:30:48 2022
# Execution time and date (local): Tue Jan  4 22:30:48 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 21462.947266            0 
            2           256           256   gramschmidt           MPI          1 21270.941406            1 
            2           256           256   gramschmidt           MPI          2 21159.541016            0 
            2           256           256   gramschmidt           MPI          3 21169.966797            0 
            2           256           256   gramschmidt           MPI          4 21288.515625            0 
            2           256           256   gramschmidt           MPI          5 21324.605469            0 
            2           256           256   gramschmidt           MPI          6 21431.207031            0 
            2           256           256   gramschmidt           MPI          7 21322.056641            0 
            2           256           256   gramschmidt           MPI          8 21271.158203            0 
            2           256           256   gramschmidt           MPI          9 21324.412109            0 
# Runtime: 0.280914 s (overhead: 0.000356 %) 10 records
