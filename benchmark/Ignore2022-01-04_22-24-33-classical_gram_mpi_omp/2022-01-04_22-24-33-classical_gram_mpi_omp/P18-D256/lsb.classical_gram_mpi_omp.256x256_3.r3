# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:30:48 2022
# Execution time and date (local): Tue Jan  4 22:30:48 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 21188.867188            0 
            3           256           256   gramschmidt           MPI          1 21035.992188            3 
            3           256           256   gramschmidt           MPI          2 20957.167969            0 
            3           256           256   gramschmidt           MPI          3 20876.173828            0 
            3           256           256   gramschmidt           MPI          4 21045.816406            0 
            3           256           256   gramschmidt           MPI          5 21116.925781            0 
            3           256           256   gramschmidt           MPI          6 21132.871094            0 
            3           256           256   gramschmidt           MPI          7 21072.464844            0 
            3           256           256   gramschmidt           MPI          8 21040.923828            1 
            3           256           256   gramschmidt           MPI          9 21137.628906            0 
# Runtime: 3.279675 s (overhead: 0.000122 %) 10 records
