# Sysname : Linux
# Nodename: eu-a6-004-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:57 2022
# Execution time and date (local): Tue Jan  4 22:26:57 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 8955.310547            0 
            2           128           128   gramschmidt           MPI          1 8698.275391            0 
            2           128           128   gramschmidt           MPI          2 9134.402344            0 
            2           128           128   gramschmidt           MPI          3 8718.337891            0 
            2           128           128   gramschmidt           MPI          4 8941.941406            0 
            2           128           128   gramschmidt           MPI          5 9090.908203            0 
            2           128           128   gramschmidt           MPI          6 8723.113281            0 
            2           128           128   gramschmidt           MPI          7 8877.013672            0 
            2           128           128   gramschmidt           MPI          8 8956.126953            0 
            2           128           128   gramschmidt           MPI          9 8728.208984            0 
# Runtime: 0.117316 s (overhead: 0.000000 %) 10 records
