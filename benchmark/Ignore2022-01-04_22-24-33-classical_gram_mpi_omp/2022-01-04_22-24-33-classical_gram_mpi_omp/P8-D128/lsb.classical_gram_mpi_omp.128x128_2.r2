# Sysname : Linux
# Nodename: eu-a6-001-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:04 2022
# Execution time and date (local): Tue Jan  4 22:26:04 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 1852.318359            0 
            2           128           128   gramschmidt           MPI          1 1835.449219            0 
            2           128           128   gramschmidt           MPI          2 1799.828125            0 
            2           128           128   gramschmidt           MPI          3 1790.984375            0 
            2           128           128   gramschmidt           MPI          4 1756.445312            0 
            2           128           128   gramschmidt           MPI          5 1811.779297            0 
            2           128           128   gramschmidt           MPI          6 1837.285156            0 
            2           128           128   gramschmidt           MPI          7 1704.472656            0 
            2           128           128   gramschmidt           MPI          8 1797.595703            0 
            2           128           128   gramschmidt           MPI          9 1754.083984            0 
# Runtime: 0.027127 s (overhead: 0.000000 %) 10 records
