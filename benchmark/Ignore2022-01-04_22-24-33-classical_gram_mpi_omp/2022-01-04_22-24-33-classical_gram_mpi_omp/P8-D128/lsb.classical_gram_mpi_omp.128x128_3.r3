# Sysname : Linux
# Nodename: eu-a6-001-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:04 2022
# Execution time and date (local): Tue Jan  4 22:26:04 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 1817.187500            0 
            3           128           128   gramschmidt           MPI          1 1796.986328            3 
            3           128           128   gramschmidt           MPI          2 1773.720703            0 
            3           128           128   gramschmidt           MPI          3 1767.824219            0 
            3           128           128   gramschmidt           MPI          4 1751.941406            0 
            3           128           128   gramschmidt           MPI          5 1793.277344            0 
            3           128           128   gramschmidt           MPI          6 1812.070312            0 
            3           128           128   gramschmidt           MPI          7 1673.349609            0 
            3           128           128   gramschmidt           MPI          8 1713.046875            0 
            3           128           128   gramschmidt           MPI          9 1729.955078            0 
# Runtime: 1.026616 s (overhead: 0.000292 %) 10 records
