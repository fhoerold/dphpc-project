# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:31:17 2022
# Execution time and date (local): Tue Jan  4 22:31:17 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 79678.134766            0 
            2           512           512   gramschmidt           MPI          1 80388.865234            1 
            2           512           512   gramschmidt           MPI          2 79696.929688            1 
            2           512           512   gramschmidt           MPI          3 78987.150391            0 
            2           512           512   gramschmidt           MPI          4 79184.093750            1 
            2           512           512   gramschmidt           MPI          5 79423.699219            0 
            2           512           512   gramschmidt           MPI          6 79167.751953            0 
            2           512           512   gramschmidt           MPI          7 79113.214844            0 
            2           512           512   gramschmidt           MPI          8 78953.361328            1 
            2           512           512   gramschmidt           MPI          9 79129.599609            0 
# Runtime: 1.046730 s (overhead: 0.000382 %) 10 records
