# Sysname : Linux
# Nodename: eu-a6-008-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:18 2022
# Execution time and date (local): Tue Jan  4 22:26:18 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 521186.714844            0 
            3          1024          1024   gramschmidt           MPI          1 520358.738281            5 
            3          1024          1024   gramschmidt           MPI          2 550332.705078            6 
            3          1024          1024   gramschmidt           MPI          3 526612.384766            0 
            3          1024          1024   gramschmidt           MPI          4 522120.785156            2 
            3          1024          1024   gramschmidt           MPI          5 526721.392578            0 
            3          1024          1024   gramschmidt           MPI          6 529401.541016            0 
            3          1024          1024   gramschmidt           MPI          7 534767.908203            0 
            3          1024          1024   gramschmidt           MPI          8 521349.658203            2 
            3          1024          1024   gramschmidt           MPI          9 521593.888672            0 
# Runtime: 15.881783 s (overhead: 0.000094 %) 10 records
