# Sysname : Linux
# Nodename: eu-a6-008-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:09 2022
# Execution time and date (local): Tue Jan  4 22:26:09 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 291.333984            0 
            3            32            32   gramschmidt           MPI          1 282.937500            3 
            3            32            32   gramschmidt           MPI          2 331.148438            0 
            3            32            32   gramschmidt           MPI          3 277.494141            0 
            3            32            32   gramschmidt           MPI          4 286.388672            0 
            3            32            32   gramschmidt           MPI          5 261.060547            0 
            3            32            32   gramschmidt           MPI          6 263.832031            0 
            3            32            32   gramschmidt           MPI          7 266.406250            0 
            3            32            32   gramschmidt           MPI          8 269.099609            0 
            3            32            32   gramschmidt           MPI          9 256.269531            0 
# Runtime: 1.005629 s (overhead: 0.000298 %) 10 records
