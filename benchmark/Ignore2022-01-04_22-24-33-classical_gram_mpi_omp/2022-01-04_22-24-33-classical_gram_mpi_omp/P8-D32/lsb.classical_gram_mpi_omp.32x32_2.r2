# Sysname : Linux
# Nodename: eu-a6-008-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:09 2022
# Execution time and date (local): Tue Jan  4 22:26:09 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 301.726562            0 
            2            32            32   gramschmidt           MPI          1 290.429688            2 
            2            32            32   gramschmidt           MPI          2 332.113281            0 
            2            32            32   gramschmidt           MPI          3 281.835938            0 
            2            32            32   gramschmidt           MPI          4 288.425781            0 
            2            32            32   gramschmidt           MPI          5 264.509766            0 
            2            32            32   gramschmidt           MPI          6 265.275391            0 
            2            32            32   gramschmidt           MPI          7 268.378906            0 
            2            32            32   gramschmidt           MPI          8 269.091797            0 
            2            32            32   gramschmidt           MPI          9 259.789062            0 
# Runtime: 2.006080 s (overhead: 0.000100 %) 10 records
