# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:00:30 2022
# Execution time and date (local): Tue Jan  4 23:00:30 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 8641334.810547            0 
            2          2048          2048   gramschmidt           MPI          1 8891248.464844            4 
            2          2048          2048   gramschmidt           MPI          2 8826508.525391            2 
            2          2048          2048   gramschmidt           MPI          3 8921095.761719            0 
            2          2048          2048   gramschmidt           MPI          4 9146076.888672            6 
            2          2048          2048   gramschmidt           MPI          5 8908353.761719            0 
            2          2048          2048   gramschmidt           MPI          6 8949836.208984            0 
            2          2048          2048   gramschmidt           MPI          7 9126208.417969            0 
            2          2048          2048   gramschmidt           MPI          8 8858087.937500            6 
            2          2048          2048   gramschmidt           MPI          9 8913504.017578            0 
# Runtime: 116.084371 s (overhead: 0.000016 %) 10 records
