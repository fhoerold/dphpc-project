# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:25:34 2022
# Execution time and date (local): Tue Jan  4 22:25:34 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 7966.099609            0 
            2           256           256   gramschmidt           MPI          1 7859.484375            3 
            2           256           256   gramschmidt           MPI          2 7937.939453            1 
            2           256           256   gramschmidt           MPI          3 7899.271484            0 
            2           256           256   gramschmidt           MPI          4 7760.312500            0 
            2           256           256   gramschmidt           MPI          5 7745.689453            0 
            2           256           256   gramschmidt           MPI          6 7745.386719            0 
            2           256           256   gramschmidt           MPI          7 7851.244141            0 
            2           256           256   gramschmidt           MPI          8 7760.955078            1 
            2           256           256   gramschmidt           MPI          9 7759.033203            0 
# Runtime: 7.105772 s (overhead: 0.000070 %) 10 records
