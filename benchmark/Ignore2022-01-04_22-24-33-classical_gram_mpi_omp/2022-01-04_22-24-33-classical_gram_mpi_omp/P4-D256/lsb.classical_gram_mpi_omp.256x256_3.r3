# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:25:34 2022
# Execution time and date (local): Tue Jan  4 22:25:34 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 7817.458984            0 
            3           256           256   gramschmidt           MPI          1 7731.796875            4 
            3           256           256   gramschmidt           MPI          2 7825.457031            0 
            3           256           256   gramschmidt           MPI          3 7688.283203            0 
            3           256           256   gramschmidt           MPI          4 7629.082031            1 
            3           256           256   gramschmidt           MPI          5 7629.707031            0 
            3           256           256   gramschmidt           MPI          6 7592.023438            0 
            3           256           256   gramschmidt           MPI          7 7723.480469            0 
            3           256           256   gramschmidt           MPI          8 7639.136719            1 
            3           256           256   gramschmidt           MPI          9 7594.058594            0 
# Runtime: 4.081896 s (overhead: 0.000147 %) 10 records
