# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:29:23 2022
# Execution time and date (local): Tue Jan  4 22:29:23 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 79702.892578            0 
            2           512           512   gramschmidt           MPI          1 79028.982422            1 
            2           512           512   gramschmidt           MPI          2 79468.449219            1 
            2           512           512   gramschmidt           MPI          3 80976.521484            0 
            2           512           512   gramschmidt           MPI          4 80122.820312            1 
            2           512           512   gramschmidt           MPI          5 79776.894531            0 
            2           512           512   gramschmidt           MPI          6 79685.375000            0 
            2           512           512   gramschmidt           MPI          7 80082.371094            0 
            2           512           512   gramschmidt           MPI          8 80095.087891            1 
            2           512           512   gramschmidt           MPI          9 79157.533203            0 
# Runtime: 1.039483 s (overhead: 0.000385 %) 10 records
