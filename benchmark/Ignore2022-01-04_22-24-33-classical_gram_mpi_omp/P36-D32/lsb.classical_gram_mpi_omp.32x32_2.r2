# Sysname : Linux
# Nodename: eu-a6-009-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:33:47 2022
# Execution time and date (local): Tue Jan  4 23:33:47 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 7228.314453            0 
            2            32            32   gramschmidt           MPI          1 7186.537109            3 
            2            32            32   gramschmidt           MPI          2 7004.568359            0 
            2            32            32   gramschmidt           MPI          3 6989.996094            0 
            2            32            32   gramschmidt           MPI          4 7017.435547            0 
            2            32            32   gramschmidt           MPI          5 7032.621094            0 
            2            32            32   gramschmidt           MPI          6 7142.976562            0 
            2            32            32   gramschmidt           MPI          7 7035.902344            0 
            2            32            32   gramschmidt           MPI          8 7031.701172            0 
            2            32            32   gramschmidt           MPI          9 7299.826172            0 
# Runtime: 11.095739 s (overhead: 0.000027 %) 10 records
