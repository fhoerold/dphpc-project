# Sysname : Linux
# Nodename: eu-a6-009-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:33:47 2022
# Execution time and date (local): Tue Jan  4 23:33:47 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 7225.429688            0 
            3            32            32   gramschmidt           MPI          1 7156.781250            0 
            3            32            32   gramschmidt           MPI          2 6983.185547            0 
            3            32            32   gramschmidt           MPI          3 6986.498047            0 
            3            32            32   gramschmidt           MPI          4 7013.632812            0 
            3            32            32   gramschmidt           MPI          5 7026.386719            0 
            3            32            32   gramschmidt           MPI          6 7125.044922            0 
            3            32            32   gramschmidt           MPI          7 7017.595703            0 
            3            32            32   gramschmidt           MPI          8 7016.126953            0 
            3            32            32   gramschmidt           MPI          9 7288.304688            0 
# Runtime: 0.093687 s (overhead: 0.000000 %) 10 records
