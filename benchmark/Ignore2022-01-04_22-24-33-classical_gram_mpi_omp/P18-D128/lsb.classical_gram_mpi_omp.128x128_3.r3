# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:30:21 2022
# Execution time and date (local): Tue Jan  4 22:30:21 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 8692.736328            0 
            3           128           128   gramschmidt           MPI          1 8646.623047            2 
            3           128           128   gramschmidt           MPI          2 8574.451172            0 
            3           128           128   gramschmidt           MPI          3 8529.117188            0 
            3           128           128   gramschmidt           MPI          4 8639.332031            0 
            3           128           128   gramschmidt           MPI          5 8731.287109            0 
            3           128           128   gramschmidt           MPI          6 8646.216797            0 
            3           128           128   gramschmidt           MPI          7 8604.671875            0 
            3           128           128   gramschmidt           MPI          8 8921.955078            0 
            3           128           128   gramschmidt           MPI          9 8559.751953            0 
# Runtime: 19.116154 s (overhead: 0.000010 %) 10 records
