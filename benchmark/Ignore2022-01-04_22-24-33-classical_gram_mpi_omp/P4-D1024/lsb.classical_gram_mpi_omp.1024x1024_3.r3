# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:25:34 2022
# Execution time and date (local): Tue Jan  4 22:25:34 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 710234.351562            3 
            3          1024          1024   gramschmidt           MPI          1 716544.861328            5 
            3          1024          1024   gramschmidt           MPI          2 714390.843750            1 
            3          1024          1024   gramschmidt           MPI          3 715865.966797            0 
            3          1024          1024   gramschmidt           MPI          4 707539.669922            2 
            3          1024          1024   gramschmidt           MPI          5 715769.814453            0 
            3          1024          1024   gramschmidt           MPI          6 710387.171875            0 
            3          1024          1024   gramschmidt           MPI          7 1015345.666016            0 
            3          1024          1024   gramschmidt           MPI          8 710274.886719            6 
            3          1024          1024   gramschmidt           MPI          9 754073.392578            0 
# Runtime: 9.646156 s (overhead: 0.000176 %) 10 records
