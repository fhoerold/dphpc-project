# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:25:34 2022
# Execution time and date (local): Tue Jan  4 22:25:34 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 711146.392578            0 
            2          1024          1024   gramschmidt           MPI          1 717447.947266            3 
            2          1024          1024   gramschmidt           MPI          2 715380.546875            2 
            2          1024          1024   gramschmidt           MPI          3 716572.126953            0 
            2          1024          1024   gramschmidt           MPI          4 708442.808594            1 
            2          1024          1024   gramschmidt           MPI          5 716562.660156            0 
            2          1024          1024   gramschmidt           MPI          6 711286.382812            0 
            2          1024          1024   gramschmidt           MPI          7 1016220.998047            0 
            2          1024          1024   gramschmidt           MPI          8 711133.753906            4 
            2          1024          1024   gramschmidt           MPI          9 754902.994141            0 
# Runtime: 9.654338 s (overhead: 0.000104 %) 10 records
