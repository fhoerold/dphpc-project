# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:30:02 2022
# Execution time and date (local): Tue Jan  4 22:30:02 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 2022.457031            0 
            3            32            32   gramschmidt           MPI          1 2032.224609            0 
            3            32            32   gramschmidt           MPI          2 2082.919922            0 
            3            32            32   gramschmidt           MPI          3 2027.806641            0 
            3            32            32   gramschmidt           MPI          4 2011.615234            0 
            3            32            32   gramschmidt           MPI          5 2002.617188            0 
            3            32            32   gramschmidt           MPI          6 1997.267578            0 
            3            32            32   gramschmidt           MPI          7 1982.099609            0 
            3            32            32   gramschmidt           MPI          8 2009.650391            0 
            3            32            32   gramschmidt           MPI          9 2000.818359            0 
# Runtime: 0.027801 s (overhead: 0.000000 %) 10 records
