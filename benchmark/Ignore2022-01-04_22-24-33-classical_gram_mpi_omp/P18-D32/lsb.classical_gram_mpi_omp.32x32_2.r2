# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:30:02 2022
# Execution time and date (local): Tue Jan  4 22:30:02 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 2026.132812            0 
            2            32            32   gramschmidt           MPI          1 2038.332031            0 
            2            32            32   gramschmidt           MPI          2 2092.320312            0 
            2            32            32   gramschmidt           MPI          3 2030.683594            0 
            2            32            32   gramschmidt           MPI          4 2020.945312            0 
            2            32            32   gramschmidt           MPI          5 2012.285156            0 
            2            32            32   gramschmidt           MPI          6 2004.082031            0 
            2            32            32   gramschmidt           MPI          7 1987.916016            0 
            2            32            32   gramschmidt           MPI          8 2010.531250            0 
            2            32            32   gramschmidt           MPI          9 2007.128906            0 
# Runtime: 0.028092 s (overhead: 0.000000 %) 10 records
