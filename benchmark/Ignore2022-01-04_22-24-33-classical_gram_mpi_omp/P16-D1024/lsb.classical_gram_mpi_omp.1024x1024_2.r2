# Sysname : Linux
# Nodename: eu-a6-004-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:29:32 2022
# Execution time and date (local): Tue Jan  4 22:29:32 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 947122.650391            0 
            2          1024          1024   gramschmidt           MPI          1 987433.541016            5 
            2          1024          1024   gramschmidt           MPI          2 969664.982422            1 
            2          1024          1024   gramschmidt           MPI          3 959990.646484            0 
            2          1024          1024   gramschmidt           MPI          4 956149.265625            4 
            2          1024          1024   gramschmidt           MPI          5 948682.251953            0 
            2          1024          1024   gramschmidt           MPI          6 947320.673828            0 
            2          1024          1024   gramschmidt           MPI          7 947359.726562            0 
            2          1024          1024   gramschmidt           MPI          8 992715.187500            1 
            2          1024          1024   gramschmidt           MPI          9 955900.074219            0 
# Runtime: 13.587398 s (overhead: 0.000081 %) 10 records
