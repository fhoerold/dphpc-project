# Sysname : Linux
# Nodename: eu-a6-004-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:29:32 2022
# Execution time and date (local): Tue Jan  4 22:29:32 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 946495.333984            3 
            3          1024          1024   gramschmidt           MPI          1 986594.492188            4 
            3          1024          1024   gramschmidt           MPI          2 968768.837891            1 
            3          1024          1024   gramschmidt           MPI          3 959186.490234            0 
            3          1024          1024   gramschmidt           MPI          4 955442.257812            5 
            3          1024          1024   gramschmidt           MPI          5 947766.328125            0 
            3          1024          1024   gramschmidt           MPI          6 946260.787109            0 
            3          1024          1024   gramschmidt           MPI          7 946349.578125            0 
            3          1024          1024   gramschmidt           MPI          8 991843.937500            2 
            3          1024          1024   gramschmidt           MPI          9 955044.884766            0 
# Runtime: 15.585793 s (overhead: 0.000096 %) 10 records
