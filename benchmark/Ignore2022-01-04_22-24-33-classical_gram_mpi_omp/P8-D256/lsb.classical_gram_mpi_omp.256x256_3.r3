# Sysname : Linux
# Nodename: eu-a6-008-06
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:09 2022
# Execution time and date (local): Tue Jan  4 22:26:09 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 7285.302734            0 
            3           256           256   gramschmidt           MPI          1 7256.775391            1 
            3           256           256   gramschmidt           MPI          2 7269.132812            0 
            3           256           256   gramschmidt           MPI          3 7105.556641            0 
            3           256           256   gramschmidt           MPI          4 7221.736328            0 
            3           256           256   gramschmidt           MPI          5 7323.478516            0 
            3           256           256   gramschmidt           MPI          6 7070.089844            0 
            3           256           256   gramschmidt           MPI          7 7153.486328            0 
            3           256           256   gramschmidt           MPI          8 7255.207031            0 
            3           256           256   gramschmidt           MPI          9 7322.638672            0 
# Runtime: 0.101308 s (overhead: 0.000987 %) 10 records
