# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:03:24 2022
# Execution time and date (local): Tue Jan  4 23:03:24 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 50214.171875            0 
            2           256           256   gramschmidt           MPI          1 50374.033203            4 
            2           256           256   gramschmidt           MPI          2 50367.958984            1 
            2           256           256   gramschmidt           MPI          3 50524.234375            0 
            2           256           256   gramschmidt           MPI          4 50500.267578            1 
            2           256           256   gramschmidt           MPI          5 50926.214844            0 
            2           256           256   gramschmidt           MPI          6 50550.078125            0 
            2           256           256   gramschmidt           MPI          7 50617.554688            0 
            2           256           256   gramschmidt           MPI          8 50578.277344            1 
            2           256           256   gramschmidt           MPI          9 50643.613281            0 
# Runtime: 2.660854 s (overhead: 0.000263 %) 10 records
