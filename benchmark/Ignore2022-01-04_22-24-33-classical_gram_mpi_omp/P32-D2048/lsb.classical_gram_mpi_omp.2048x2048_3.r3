# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:00:30 2022
# Execution time and date (local): Tue Jan  4 23:00:30 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 8642929.957031            0 
            3          2048          2048   gramschmidt           MPI          1 8892084.593750            6 
            3          2048          2048   gramschmidt           MPI          2 8827347.808594            2 
            3          2048          2048   gramschmidt           MPI          3 8922704.851562            0 
            3          2048          2048   gramschmidt           MPI          4 9146927.953125            4 
            3          2048          2048   gramschmidt           MPI          5 8909163.947266            0 
            3          2048          2048   gramschmidt           MPI          6 8950724.531250            0 
            3          2048          2048   gramschmidt           MPI          7 9127053.771484            0 
            3          2048          2048   gramschmidt           MPI          8 8859838.820312            7 
            3          2048          2048   gramschmidt           MPI          9 8914321.333984            0 
# Runtime: 120.079202 s (overhead: 0.000016 %) 10 records
