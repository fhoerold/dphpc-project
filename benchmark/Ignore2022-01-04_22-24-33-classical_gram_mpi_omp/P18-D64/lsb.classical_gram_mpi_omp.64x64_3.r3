# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:30:12 2022
# Execution time and date (local): Tue Jan  4 22:30:12 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 4447.679688            0 
            3            64            64   gramschmidt           MPI          1 4399.259766            0 
            3            64            64   gramschmidt           MPI          2 4382.546875            0 
            3            64            64   gramschmidt           MPI          3 4375.513672            0 
            3            64            64   gramschmidt           MPI          4 4341.052734            0 
            3            64            64   gramschmidt           MPI          5 4356.089844            0 
            3            64            64   gramschmidt           MPI          6 4299.851562            0 
            3            64            64   gramschmidt           MPI          7 4311.511719            0 
            3            64            64   gramschmidt           MPI          8 4400.173828            0 
            3            64            64   gramschmidt           MPI          9 4675.126953            0 
# Runtime: 0.059588 s (overhead: 0.000000 %) 10 records
