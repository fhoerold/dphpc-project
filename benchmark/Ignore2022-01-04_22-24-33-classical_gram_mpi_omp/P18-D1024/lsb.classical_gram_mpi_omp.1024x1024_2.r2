# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:33:33 2022
# Execution time and date (local): Tue Jan  4 22:33:33 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 977649.234375            0 
            2          1024          1024   gramschmidt           MPI          1 968669.550781            4 
            2          1024          1024   gramschmidt           MPI          2 968734.945312            1 
            2          1024          1024   gramschmidt           MPI          3 969726.179688            0 
            2          1024          1024   gramschmidt           MPI          4 973194.615234            1 
            2          1024          1024   gramschmidt           MPI          5 971051.388672            0 
            2          1024          1024   gramschmidt           MPI          6 969663.132812            0 
            2          1024          1024   gramschmidt           MPI          7 975595.312500            0 
            2          1024          1024   gramschmidt           MPI          8 970392.554688            5 
            2          1024          1024   gramschmidt           MPI          9 971373.201172            0 
# Runtime: 15.636804 s (overhead: 0.000070 %) 10 records
