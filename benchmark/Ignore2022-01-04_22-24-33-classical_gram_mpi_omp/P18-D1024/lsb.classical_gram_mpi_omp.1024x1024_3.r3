# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:33:33 2022
# Execution time and date (local): Tue Jan  4 22:33:33 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 977085.757812            1 
            3          1024          1024   gramschmidt           MPI          1 967901.177734            4 
            3          1024          1024   gramschmidt           MPI          2 967961.626953            1 
            3          1024          1024   gramschmidt           MPI          3 968959.886719            0 
            3          1024          1024   gramschmidt           MPI          4 972601.892578            1 
            3          1024          1024   gramschmidt           MPI          5 970296.777344            0 
            3          1024          1024   gramschmidt           MPI          6 968849.267578            0 
            3          1024          1024   gramschmidt           MPI          7 974825.927734            0 
            3          1024          1024   gramschmidt           MPI          8 969613.044922            5 
            3          1024          1024   gramschmidt           MPI          9 970602.498047            0 
# Runtime: 15.627460 s (overhead: 0.000077 %) 10 records
