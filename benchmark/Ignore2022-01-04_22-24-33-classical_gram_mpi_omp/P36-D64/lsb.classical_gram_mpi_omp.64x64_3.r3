# Sysname : Linux
# Nodename: eu-a6-009-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:34:10 2022
# Execution time and date (local): Tue Jan  4 23:34:10 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 14372.373047            0 
            3            64            64   gramschmidt           MPI          1 14688.574219            0 
            3            64            64   gramschmidt           MPI          2 14711.578125            0 
            3            64            64   gramschmidt           MPI          3 14544.539062            0 
            3            64            64   gramschmidt           MPI          4 14395.101562            0 
            3            64            64   gramschmidt           MPI          5 14236.865234            0 
            3            64            64   gramschmidt           MPI          6 14343.408203            0 
            3            64            64   gramschmidt           MPI          7 14204.054688            0 
            3            64            64   gramschmidt           MPI          8 14841.951172            0 
            3            64            64   gramschmidt           MPI          9 14596.994141            0 
# Runtime: 0.189620 s (overhead: 0.000000 %) 10 records
