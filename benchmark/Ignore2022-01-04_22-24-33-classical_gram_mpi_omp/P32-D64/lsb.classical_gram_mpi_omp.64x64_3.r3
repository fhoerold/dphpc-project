# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:03:08 2022
# Execution time and date (local): Tue Jan  4 23:03:08 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 11259.031250            0 
            3            64            64   gramschmidt           MPI          1 11287.906250            4 
            3            64            64   gramschmidt           MPI          2 11522.474609            0 
            3            64            64   gramschmidt           MPI          3 11521.250000            0 
            3            64            64   gramschmidt           MPI          4 11567.544922            0 
            3            64            64   gramschmidt           MPI          5 11200.986328            0 
            3            64            64   gramschmidt           MPI          6 11270.396484            0 
            3            64            64   gramschmidt           MPI          7 11200.394531            0 
            3            64            64   gramschmidt           MPI          8 11214.783203            0 
            3            64            64   gramschmidt           MPI          9 11223.341797            0 
# Runtime: 2.148813 s (overhead: 0.000186 %) 10 records
