# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:03:08 2022
# Execution time and date (local): Tue Jan  4 23:03:08 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 11259.255859            0 
            2            64            64   gramschmidt           MPI          1 11283.718750            1 
            2            64            64   gramschmidt           MPI          2 11515.734375            0 
            2            64            64   gramschmidt           MPI          3 11509.363281            0 
            2            64            64   gramschmidt           MPI          4 11556.662109            1 
            2            64            64   gramschmidt           MPI          5 11185.005859            0 
            2            64            64   gramschmidt           MPI          6 11258.146484            0 
            2            64            64   gramschmidt           MPI          7 11190.289062            0 
            2            64            64   gramschmidt           MPI          8 11207.693359            0 
            2            64            64   gramschmidt           MPI          9 11213.027344            0 
# Runtime: 0.149570 s (overhead: 0.001337 %) 10 records
