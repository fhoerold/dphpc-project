# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:03:40 2022
# Execution time and date (local): Tue Jan  4 23:03:40 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 135978.310547            0 
            3           512           512   gramschmidt           MPI          1 136009.529297            2 
            3           512           512   gramschmidt           MPI          2 136298.880859            1 
            3           512           512   gramschmidt           MPI          3 136740.322266            0 
            3           512           512   gramschmidt           MPI          4 137107.736328            2 
            3           512           512   gramschmidt           MPI          5 151650.105469            0 
            3           512           512   gramschmidt           MPI          6 140656.816406            0 
            3           512           512   gramschmidt           MPI          7 137584.335938            0 
            3           512           512   gramschmidt           MPI          8 137716.845703            2 
            3           512           512   gramschmidt           MPI          9 199489.916016            0 
# Runtime: 1.863305 s (overhead: 0.000376 %) 10 records
