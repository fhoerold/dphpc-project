# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:03:40 2022
# Execution time and date (local): Tue Jan  4 23:03:40 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 136139.250000            0 
            2           512           512   gramschmidt           MPI          1 136168.484375            1 
            2           512           512   gramschmidt           MPI          2 136471.978516            1 
            2           512           512   gramschmidt           MPI          3 136915.113281            0 
            2           512           512   gramschmidt           MPI          4 137337.291016            1 
            2           512           512   gramschmidt           MPI          5 151887.058594            0 
            2           512           512   gramschmidt           MPI          6 140875.496094            0 
            2           512           512   gramschmidt           MPI          7 137791.048828            0 
            2           512           512   gramschmidt           MPI          8 137951.947266            1 
            2           512           512   gramschmidt           MPI          9 199744.123047            0 
# Runtime: 1.874462 s (overhead: 0.000213 %) 10 records
