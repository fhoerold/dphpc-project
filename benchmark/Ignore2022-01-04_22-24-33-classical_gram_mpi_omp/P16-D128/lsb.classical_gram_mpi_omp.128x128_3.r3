# Sysname : Linux
# Nodename: eu-a6-004-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:57 2022
# Execution time and date (local): Tue Jan  4 22:26:57 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 8934.359375            0 
            3           128           128   gramschmidt           MPI          1 8674.974609            0 
            3           128           128   gramschmidt           MPI          2 9103.986328            0 
            3           128           128   gramschmidt           MPI          3 8703.029297            0 
            3           128           128   gramschmidt           MPI          4 8924.435547            0 
            3           128           128   gramschmidt           MPI          5 9060.773438            0 
            3           128           128   gramschmidt           MPI          6 8691.753906            0 
            3           128           128   gramschmidt           MPI          7 8850.912109            0 
            3           128           128   gramschmidt           MPI          8 8927.175781            0 
            3           128           128   gramschmidt           MPI          9 8704.966797            0 
# Runtime: 0.118214 s (overhead: 0.000000 %) 10 records
