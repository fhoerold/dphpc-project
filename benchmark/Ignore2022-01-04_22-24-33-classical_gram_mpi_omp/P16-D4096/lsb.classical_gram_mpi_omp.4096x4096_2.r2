# Sysname : Linux
# Nodename: eu-a6-004-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:30:02 2022
# Execution time and date (local): Tue Jan  4 22:30:02 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 124328754.712891            0 
            2          4096          4096   gramschmidt           MPI          1 125608312.835938            8 
            2          4096          4096   gramschmidt           MPI          2 125628051.222656            7 
            2          4096          4096   gramschmidt           MPI          3 115750017.615234            2 
            2          4096          4096   gramschmidt           MPI          4 120438361.535156           10 
            2          4096          4096   gramschmidt           MPI          5 122208501.031250            0 
            2          4096          4096   gramschmidt           MPI          6 121572356.541016            0 
            2          4096          4096   gramschmidt           MPI          7 119823696.972656            0 
            2          4096          4096   gramschmidt           MPI          8 115434055.609375            7 
            2          4096          4096   gramschmidt           MPI          9 119468069.128906            0 
# Runtime: 1599.257441 s (overhead: 0.000002 %) 10 records
