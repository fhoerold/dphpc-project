# Sysname : Linux
# Nodename: eu-a6-006-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 23:05:38 2022
# Execution time and date (local): Wed Jan  5 00:05:38 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 163189.886719            0 
            3           512           512   gramschmidt           MPI          1 163915.763672            4 
            3           512           512   gramschmidt           MPI          2 161746.031250            0 
            3           512           512   gramschmidt           MPI          3 161114.445312            0 
            3           512           512   gramschmidt           MPI          4 160822.167969            0 
            3           512           512   gramschmidt           MPI          5 160216.552734            0 
            3           512           512   gramschmidt           MPI          6 160292.048828            0 
            3           512           512   gramschmidt           MPI          7 160630.535156            0 
            3           512           512   gramschmidt           MPI          8 161750.707031            0 
            3           512           512   gramschmidt           MPI          9 160813.185547            0 
# Runtime: 5.110273 s (overhead: 0.000078 %) 10 records
