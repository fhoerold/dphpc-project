# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:25:33 2022
# Execution time and date (local): Tue Jan  4 22:25:33 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 1704.414062            0 
            3           128           128   gramschmidt           MPI          1 1629.306641            2 
            3           128           128   gramschmidt           MPI          2 1642.945312            0 
            3           128           128   gramschmidt           MPI          3 1625.871094            0 
            3           128           128   gramschmidt           MPI          4 1617.619141            0 
            3           128           128   gramschmidt           MPI          5 1602.937500            0 
            3           128           128   gramschmidt           MPI          6 1593.726562            0 
            3           128           128   gramschmidt           MPI          7 1625.333984            0 
            3           128           128   gramschmidt           MPI          8 1594.142578            0 
            3           128           128   gramschmidt           MPI          9 1605.453125            0 
# Runtime: 9.005746 s (overhead: 0.000022 %) 10 records
