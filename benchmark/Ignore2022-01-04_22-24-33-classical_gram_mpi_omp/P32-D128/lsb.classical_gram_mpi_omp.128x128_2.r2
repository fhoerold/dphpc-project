# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:00:20 2022
# Execution time and date (local): Tue Jan  4 23:00:20 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 23197.878906            0 
            2           128           128   gramschmidt           MPI          1 23326.843750            4 
            2           128           128   gramschmidt           MPI          2 23265.031250            0 
            2           128           128   gramschmidt           MPI          3 23141.158203            0 
            2           128           128   gramschmidt           MPI          4 23251.935547            1 
            2           128           128   gramschmidt           MPI          5 23066.710938            0 
            2           128           128   gramschmidt           MPI          6 23379.896484            0 
            2           128           128   gramschmidt           MPI          7 23051.816406            0 
            2           128           128   gramschmidt           MPI          8 23267.990234            0 
            2           128           128   gramschmidt           MPI          9 23180.623047            0 
# Runtime: 1.305519 s (overhead: 0.000383 %) 10 records
