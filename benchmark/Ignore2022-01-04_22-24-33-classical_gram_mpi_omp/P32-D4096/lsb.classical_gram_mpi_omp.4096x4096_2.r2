# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:04:37 2022
# Execution time and date (local): Tue Jan  4 23:04:37 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 135081766.947266            0 
            2          4096          4096   gramschmidt           MPI          1 148241636.230469            5 
            2          4096          4096   gramschmidt           MPI          2 141648669.398438            7 
            2          4096          4096   gramschmidt           MPI          3 141766524.705078            0 
            2          4096          4096   gramschmidt           MPI          4 139892147.445312            7 
            2          4096          4096   gramschmidt           MPI          5 126434128.261719            0 
            2          4096          4096   gramschmidt           MPI          6 128555308.734375            0 
            2          4096          4096   gramschmidt           MPI          7 122796214.990234            2 
            2          4096          4096   gramschmidt           MPI          8 116577010.744141            7 
            2          4096          4096   gramschmidt           MPI          9 123744460.189453            3 
# Runtime: 1704.576917 s (overhead: 0.000002 %) 10 records
