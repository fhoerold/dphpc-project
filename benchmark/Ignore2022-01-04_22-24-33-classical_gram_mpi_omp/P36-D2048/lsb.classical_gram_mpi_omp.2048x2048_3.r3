# Sysname : Linux
# Nodename: eu-a6-006-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 23:05:50 2022
# Execution time and date (local): Wed Jan  5 00:05:50 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 8203673.751953            0 
            3          2048          2048   gramschmidt           MPI          1 8217741.132812            4 
            3          2048          2048   gramschmidt           MPI          2 8226606.003906            2 
            3          2048          2048   gramschmidt           MPI          3 8217419.798828            0 
            3          2048          2048   gramschmidt           MPI          4 8195573.269531            5 
            3          2048          2048   gramschmidt           MPI          5 8218928.980469            0 
            3          2048          2048   gramschmidt           MPI          6 8208458.074219            0 
            3          2048          2048   gramschmidt           MPI          7 8222567.246094            0 
            3          2048          2048   gramschmidt           MPI          8 8209811.539062            4 
            3          2048          2048   gramschmidt           MPI          9 8204473.085938            0 
# Runtime: 117.800139 s (overhead: 0.000013 %) 10 records
