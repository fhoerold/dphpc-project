# Sysname : Linux
# Nodename: eu-a6-006-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 23:05:50 2022
# Execution time and date (local): Wed Jan  5 00:05:50 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 8202881.175781            0 
            2          2048          2048   gramschmidt           MPI          1 8216925.470703           10 
            2          2048          2048   gramschmidt           MPI          2 8224911.828125            4 
            2          2048          2048   gramschmidt           MPI          3 8216525.281250            0 
            2          2048          2048   gramschmidt           MPI          4 8194771.837891            4 
            2          2048          2048   gramschmidt           MPI          5 8218139.421875            0 
            2          2048          2048   gramschmidt           MPI          6 8207627.585938            0 
            2          2048          2048   gramschmidt           MPI          7 8220871.953125            0 
            2          2048          2048   gramschmidt           MPI          8 8208963.771484            2 
            2          2048          2048   gramschmidt           MPI          9 8203689.751953            0 
# Runtime: 106.796306 s (overhead: 0.000019 %) 10 records
