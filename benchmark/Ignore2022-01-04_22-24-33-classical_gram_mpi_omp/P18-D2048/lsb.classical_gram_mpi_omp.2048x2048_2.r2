# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:31:25 2022
# Execution time and date (local): Tue Jan  4 22:31:25 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 7784812.742188            0 
            2          2048          2048   gramschmidt           MPI          1 7771886.548828            3 
            2          2048          2048   gramschmidt           MPI          2 7759365.675781            1 
            2          2048          2048   gramschmidt           MPI          3 7757328.876953            0 
            2          2048          2048   gramschmidt           MPI          4 7765162.757812            4 
            2          2048          2048   gramschmidt           MPI          5 7778189.388672            0 
            2          2048          2048   gramschmidt           MPI          6 7772768.513672            0 
            2          2048          2048   gramschmidt           MPI          7 7730403.558594            0 
            2          2048          2048   gramschmidt           MPI          8 7757274.818359            1 
            2          2048          2048   gramschmidt           MPI          9 7776275.017578            0 
# Runtime: 102.127652 s (overhead: 0.000009 %) 10 records
