# Sysname : Linux
# Nodename: eu-a6-008-06
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:32 2022
# Execution time and date (local): Tue Jan  4 22:26:32 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 136079174.587891            0 
            3          4096          4096   gramschmidt           MPI          1 135468827.498047            6 
            3          4096          4096   gramschmidt           MPI          2 136502541.523438            7 
            3          4096          4096   gramschmidt           MPI          3 136960301.056641            3 
            3          4096          4096   gramschmidt           MPI          4 136649609.693359            6 
            3          4096          4096   gramschmidt           MPI          5 135896092.310547            0 
            3          4096          4096   gramschmidt           MPI          6 136272218.392578            2 
            3          4096          4096   gramschmidt           MPI          7 136911295.855469            1 
            3          4096          4096   gramschmidt           MPI          8 137913868.494141            8 
            3          4096          4096   gramschmidt           MPI          9 136219191.048828            1 
# Runtime: 1781.654221 s (overhead: 0.000002 %) 10 records
