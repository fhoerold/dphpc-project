# Sysname : Linux
# Nodename: eu-a6-008-06
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:32 2022
# Execution time and date (local): Tue Jan  4 22:26:32 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 136072724.039062            0 
            2          4096          4096   gramschmidt           MPI          1 135462692.970703            5 
            2          4096          4096   gramschmidt           MPI          2 136496141.595703            6 
            2          4096          4096   gramschmidt           MPI          3 136954120.111328            0 
            2          4096          4096   gramschmidt           MPI          4 136643060.185547            7 
            2          4096          4096   gramschmidt           MPI          5 135888921.515625            2 
            2          4096          4096   gramschmidt           MPI          6 136265880.416016            2 
            2          4096          4096   gramschmidt           MPI          7 136904093.128906            0 
            2          4096          4096   gramschmidt           MPI          8 137907635.302734            7 
            2          4096          4096   gramschmidt           MPI          9 136212993.265625            0 
# Runtime: 1781.664825 s (overhead: 0.000002 %) 10 records
