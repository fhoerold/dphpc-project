# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:31:17 2022
# Execution time and date (local): Tue Jan  4 22:31:17 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 79446.253906            0 
            3           512           512   gramschmidt           MPI          1 80150.230469            1 
            3           512           512   gramschmidt           MPI          2 79466.658203            1 
            3           512           512   gramschmidt           MPI          3 78752.585938            0 
            3           512           512   gramschmidt           MPI          4 78943.027344            1 
            3           512           512   gramschmidt           MPI          5 79190.560547            0 
            3           512           512   gramschmidt           MPI          6 78937.216797            0 
            3           512           512   gramschmidt           MPI          7 78880.417969            0 
            3           512           512   gramschmidt           MPI          8 78714.603516            1 
            3           512           512   gramschmidt           MPI          9 78890.857422            0 
# Runtime: 1.045186 s (overhead: 0.000383 %) 10 records
