# Sysname : Linux
# Nodename: eu-a6-010-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:25:32 2022
# Execution time and date (local): Tue Jan  4 22:25:32 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 275.451172            0 
            3            32            32   gramschmidt           MPI          1 268.521484            2 
            3            32            32   gramschmidt           MPI          2 317.257812            0 
            3            32            32   gramschmidt           MPI          3 264.324219            0 
            3            32            32   gramschmidt           MPI          4 265.751953            0 
            3            32            32   gramschmidt           MPI          5 280.275391            0 
            3            32            32   gramschmidt           MPI          6 258.775391            0 
            3            32            32   gramschmidt           MPI          7 258.089844            0 
            3            32            32   gramschmidt           MPI          8 253.441406            0 
            3            32            32   gramschmidt           MPI          9 255.906250            0 
# Runtime: 11.977063 s (overhead: 0.000017 %) 10 records
