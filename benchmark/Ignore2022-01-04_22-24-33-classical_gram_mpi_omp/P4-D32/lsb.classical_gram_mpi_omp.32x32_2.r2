# Sysname : Linux
# Nodename: eu-a6-010-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:25:32 2022
# Execution time and date (local): Tue Jan  4 22:25:32 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 279.513672            0 
            2            32            32   gramschmidt           MPI          1 274.736328            0 
            2            32            32   gramschmidt           MPI          2 322.269531            0 
            2            32            32   gramschmidt           MPI          3 268.804688            0 
            2            32            32   gramschmidt           MPI          4 268.033203            0 
            2            32            32   gramschmidt           MPI          5 284.128906            0 
            2            32            32   gramschmidt           MPI          6 261.853516            0 
            2            32            32   gramschmidt           MPI          7 263.363281            0 
            2            32            32   gramschmidt           MPI          8 253.880859            0 
            2            32            32   gramschmidt           MPI          9 258.218750            0 
# Runtime: 0.004635 s (overhead: 0.000000 %) 10 records
