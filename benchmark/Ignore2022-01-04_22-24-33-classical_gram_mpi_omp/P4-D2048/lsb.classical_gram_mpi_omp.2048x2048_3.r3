# Sysname : Linux
# Nodename: eu-a6-011-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:26:05 2022
# Execution time and date (local): Tue Jan  4 22:26:05 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 13056912.572266            0 
            3          2048          2048   gramschmidt           MPI          1 13143699.236328            6 
            3          2048          2048   gramschmidt           MPI          2 13123051.164062            2 
            3          2048          2048   gramschmidt           MPI          3 12868233.271484            0 
            3          2048          2048   gramschmidt           MPI          4 13375590.392578            8 
            3          2048          2048   gramschmidt           MPI          5 13168040.595703            0 
            3          2048          2048   gramschmidt           MPI          6 13136660.064453            0 
            3          2048          2048   gramschmidt           MPI          7 13151336.865234            0 
            3          2048          2048   gramschmidt           MPI          8 13132817.462891            5 
            3          2048          2048   gramschmidt           MPI          9 12895853.927734            0 
# Runtime: 172.469812 s (overhead: 0.000012 %) 10 records
