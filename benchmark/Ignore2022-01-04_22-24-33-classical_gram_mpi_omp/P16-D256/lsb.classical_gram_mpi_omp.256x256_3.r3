# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:29:03 2022
# Execution time and date (local): Tue Jan  4 22:29:03 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 21333.681641            0 
            3           256           256   gramschmidt           MPI          1 21205.607422            1 
            3           256           256   gramschmidt           MPI          2 20986.660156            0 
            3           256           256   gramschmidt           MPI          3 21019.912109            0 
            3           256           256   gramschmidt           MPI          4 21068.115234            0 
            3           256           256   gramschmidt           MPI          5 21282.708984            0 
            3           256           256   gramschmidt           MPI          6 20935.541016            0 
            3           256           256   gramschmidt           MPI          7 20899.865234            0 
            3           256           256   gramschmidt           MPI          8 21058.562500            0 
            3           256           256   gramschmidt           MPI          9 21163.441406            0 
# Runtime: 0.282078 s (overhead: 0.000355 %) 10 records
