# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:29:23 2022
# Execution time and date (local): Tue Jan  4 22:29:23 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 79480.736328            0 
            3           512           512   gramschmidt           MPI          1 78793.345703            1 
            3           512           512   gramschmidt           MPI          2 79233.044922            1 
            3           512           512   gramschmidt           MPI          3 80737.335938            0 
            3           512           512   gramschmidt           MPI          4 79892.910156            1 
            3           512           512   gramschmidt           MPI          5 79539.412109            0 
            3           512           512   gramschmidt           MPI          6 79447.361328            0 
            3           512           512   gramschmidt           MPI          7 79850.720703            0 
            3           512           512   gramschmidt           MPI          8 79858.847656            1 
            3           512           512   gramschmidt           MPI          9 78907.384766            0 
# Runtime: 1.038814 s (overhead: 0.000385 %) 10 records
