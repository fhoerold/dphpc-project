# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 22:03:53 2022
# Execution time and date (local): Tue Jan  4 23:03:53 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 1097552.974609            0 
            3          1024          1024   gramschmidt           MPI          1 1054344.275391            6 
            3          1024          1024   gramschmidt           MPI          2 1053963.910156            5 
            3          1024          1024   gramschmidt           MPI          3 1058736.392578            0 
            3          1024          1024   gramschmidt           MPI          4 1058930.132812            1 
            3          1024          1024   gramschmidt           MPI          5 1082869.722656            0 
            3          1024          1024   gramschmidt           MPI          6 1050794.509766            0 
            3          1024          1024   gramschmidt           MPI          7 1079704.750000            0 
            3          1024          1024   gramschmidt           MPI          8 1058819.851562            7 
            3          1024          1024   gramschmidt           MPI          9 1059975.005859            0 
# Runtime: 13.886778 s (overhead: 0.000137 %) 10 records
