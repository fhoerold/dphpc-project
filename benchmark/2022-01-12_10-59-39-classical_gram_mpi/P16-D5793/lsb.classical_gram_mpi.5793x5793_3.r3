# Sysname : Linux
# Nodename: eu-a6-009-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:59:55 2022
# Execution time and date (local): Wed Jan 12 10:59:55 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          5793          5793   gramschmidt           MPI          0 86837931.919922            0 
            3          5793          5793   gramschmidt           MPI          1 87051429.351562            5 
            3          5793          5793   gramschmidt           MPI          2 86819141.460938            6 
            3          5793          5793   gramschmidt           MPI          3 86704850.048828            0 
            3          5793          5793   gramschmidt           MPI          4 87003870.695312            5 
            3          5793          5793   gramschmidt           MPI          5 86754165.023438            0 
            3          5793          5793   gramschmidt           MPI          6 87297822.878906            0 
            3          5793          5793   gramschmidt           MPI          7 87284748.345703            0 
            3          5793          5793   gramschmidt           MPI          8 87700463.500000            7 
            3          5793          5793   gramschmidt           MPI          9 86793989.714844            0 
# Runtime: 1146.763245 s (overhead: 0.000002 %) 10 records
