# Sysname : Linux
# Nodename: eu-a6-009-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 09:59:55 2022
# Execution time and date (local): Wed Jan 12 10:59:55 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          5793          5793   gramschmidt           MPI          0 86913747.884766            2 
            2          5793          5793   gramschmidt           MPI          1 87127361.433594            5 
            2          5793          5793   gramschmidt           MPI          2 86894939.808594            4 
            2          5793          5793   gramschmidt           MPI          3 86780365.246094            0 
            2          5793          5793   gramschmidt           MPI          4 87079851.861328           13 
            2          5793          5793   gramschmidt           MPI          5 86830003.929688            0 
            2          5793          5793   gramschmidt           MPI          6 87373941.222656            3 
            2          5793          5793   gramschmidt           MPI          7 87361023.386719            0 
            2          5793          5793   gramschmidt           MPI          8 87776513.380859            8 
            2          5793          5793   gramschmidt           MPI          9 86869870.638672            3 
# Runtime: 1147.802118 s (overhead: 0.000003 %) 10 records
