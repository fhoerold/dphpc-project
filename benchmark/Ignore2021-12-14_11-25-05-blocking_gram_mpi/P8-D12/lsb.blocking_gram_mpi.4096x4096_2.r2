# Sysname : Linux
# Nodename: eu-a6-009-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:27:11 2021
# Execution time and date (local): Tue Dec 14 11:27:11 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            2          4096          4096           MPI          0 74914596.037109            0 
            2          4096          4096           MPI          1 83093794.404297            5 
            2          4096          4096           MPI          2 73431119.516602            4 
            2          4096          4096           MPI          3 74756008.608398            0 
            2          4096          4096           MPI          4 73723560.583008            4 
            2          4096          4096           MPI          5 74591256.380859            0 
            2          4096          4096           MPI          6 72493612.499023            0 
            2          4096          4096           MPI          7 74508449.740234            0 
            2          4096          4096           MPI          8 72948514.350586            4 
            2          4096          4096           MPI          9 71961767.481445            0 
# Runtime: 11008.264698 s (overhead: 0.000000 %) 10 records
