# Sysname : Linux
# Nodename: eu-a6-009-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:27:11 2021
# Execution time and date (local): Tue Dec 14 11:27:11 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            3          4096          4096           MPI          0 104636551.582031            0 
            3          4096          4096           MPI          1 112310166.741211            4 
            3          4096          4096           MPI          2 102769187.786133            4 
            3          4096          4096           MPI          3 103758698.228516            0 
            3          4096          4096           MPI          4 102696211.644531            4 
            3          4096          4096           MPI          5 104009093.714844            0 
            3          4096          4096           MPI          6 101743246.684570            0 
            3          4096          4096           MPI          7 103084128.971680            0 
            3          4096          4096           MPI          8 101891277.563477            4 
            3          4096          4096           MPI          9 98094395.928711            0 
# Runtime: 11034.302607 s (overhead: 0.000000 %) 10 records
