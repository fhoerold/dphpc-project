# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:25:41 2021
# Execution time and date (local): Tue Dec 14 11:25:41 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            2            64            64           MPI          0 197.946289            0 
            2            64            64           MPI          1 331.254883            0 
            2            64            64           MPI          2 175.195312            0 
            2            64            64           MPI          3 178.773438            0 
            2            64            64           MPI          4 171.319336            0 
            2            64            64           MPI          5 173.172852            0 
            2            64            64           MPI          6 168.611328            0 
            2            64            64           MPI          7 157.760742            0 
            2            64            64           MPI          8 151.080078            0 
            2            64            64           MPI          9 152.515625            0 
# Runtime: 0.005132 s (overhead: 0.000000 %) 10 records
