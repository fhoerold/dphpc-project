# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:25:41 2021
# Execution time and date (local): Tue Dec 14 11:25:41 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            3            64            64           MPI          0 235.204102            1 
            3            64            64           MPI          1 184.062500            0 
            3            64            64           MPI          2 224.378906            0 
            3            64            64           MPI          3 226.836914            0 
            3            64            64           MPI          4 225.639648            0 
            3            64            64           MPI          5 218.566406            0 
            3            64            64           MPI          6 216.599609            0 
            3            64            64           MPI          7 206.771484            0 
            3            64            64           MPI          8 199.150391            0 
            3            64            64           MPI          9 200.758789            0 
# Runtime: 7.004344 s (overhead: 0.000014 %) 10 records
