# Sysname : Linux
# Nodename: eu-a6-006-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:26:33 2021
# Execution time and date (local): Tue Dec 14 11:26:33 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            3           512           512           MPI          0 30296.115234            0 
            3           512           512           MPI          1 30194.606445            0 
            3           512           512           MPI          2 30343.490234            0 
            3           512           512           MPI          3 29919.259766            0 
            3           512           512           MPI          4 29824.343750            0 
            3           512           512           MPI          5 30027.174805            0 
            3           512           512           MPI          6 29808.286133            0 
            3           512           512           MPI          7 29894.247070            0 
            3           512           512           MPI          8 29767.469727            2 
            3           512           512           MPI          9 29750.379883            0 
# Runtime: 13.247554 s (overhead: 0.000015 %) 10 records
