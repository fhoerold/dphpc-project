# Sysname : Linux
# Nodename: eu-a6-006-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:26:33 2021
# Execution time and date (local): Tue Dec 14 11:26:33 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            2           512           512           MPI          0 21995.373047            0 
            2           512           512           MPI          1 21951.203125            2 
            2           512           512           MPI          2 22057.303711            0 
            2           512           512           MPI          3 21597.615234            0 
            2           512           512           MPI          4 21548.043945            0 
            2           512           512           MPI          5 21799.101562            0 
            2           512           512           MPI          6 21534.731445            0 
            2           512           512           MPI          7 21656.134766            0 
            2           512           512           MPI          8 21515.023438            0 
            2           512           512           MPI          9 21510.912109            0 
# Runtime: 16.244462 s (overhead: 0.000012 %) 10 records
