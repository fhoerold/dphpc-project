# Sysname : Linux
# Nodename: eu-a6-007-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:25:41 2021
# Execution time and date (local): Tue Dec 14 11:25:41 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            3           512           512           MPI          0 130469.655273            0 
            3           512           512           MPI          1 130942.758789            6 
            3           512           512           MPI          2 131046.543945            1 
            3           512           512           MPI          3 130997.434570            0 
            3           512           512           MPI          4 131888.197266            1 
            3           512           512           MPI          5 130772.737305            0 
            3           512           512           MPI          6 130324.197266            0 
            3           512           512           MPI          7 129818.031250            0 
            3           512           512           MPI          8 129735.332031            1 
            3           512           512           MPI          9 128410.272461            0 
# Runtime: 13.457903 s (overhead: 0.000067 %) 10 records
