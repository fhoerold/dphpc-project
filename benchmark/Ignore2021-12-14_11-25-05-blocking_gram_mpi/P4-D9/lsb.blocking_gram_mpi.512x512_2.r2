# Sysname : Linux
# Nodename: eu-a6-007-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:25:41 2021
# Execution time and date (local): Tue Dec 14 11:25:41 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            2           512           512           MPI          0 93376.687500            0 
            2           512           512           MPI          1 93865.708984            1 
            2           512           512           MPI          2 93781.235352            1 
            2           512           512           MPI          3 93916.129883            0 
            2           512           512           MPI          4 94043.433594            4 
            2           512           512           MPI          5 93717.718750            0 
            2           512           512           MPI          6 93213.733398            0 
            2           512           512           MPI          7 92810.465820            0 
            2           512           512           MPI          8 92896.411133            1 
            2           512           512           MPI          9 91865.219727            0 
# Runtime: 11.409937 s (overhead: 0.000061 %) 10 records
