# Sysname : Linux
# Nodename: eu-a6-007-01
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:26:10 2021
# Execution time and date (local): Tue Dec 14 11:26:10 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            3            64            64           MPI          0 86.992188            0 
            3            64            64           MPI          1 125.051758            0 
            3            64            64           MPI          2 112.818359            0 
            3            64            64           MPI          3 112.889648            0 
            3            64            64           MPI          4 104.623047            0 
            3            64            64           MPI          5 125.361328            0 
            3            64            64           MPI          6 128.480469            0 
            3            64            64           MPI          7 101.647461            0 
            3            64            64           MPI          8 102.579102            0 
            3            64            64           MPI          9 111.207031            0 
# Runtime: 19.977962 s (overhead: 0.000000 %) 10 records
