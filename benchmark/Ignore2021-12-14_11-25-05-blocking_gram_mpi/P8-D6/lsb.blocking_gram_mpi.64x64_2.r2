# Sysname : Linux
# Nodename: eu-a6-007-01
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:26:10 2021
# Execution time and date (local): Tue Dec 14 11:26:10 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            2            64            64           MPI          0 303.765625            0 
            2            64            64           MPI          1 111.654297            0 
            2            64            64           MPI          2 96.325195            0 
            2            64            64           MPI          3 95.712891            0 
            2            64            64           MPI          4 89.258789            0 
            2            64            64           MPI          5 107.670898            0 
            2            64            64           MPI          6 96.208008            0 
            2            64            64           MPI          7 86.554688            0 
            2            64            64           MPI          8 87.509766            0 
            2            64            64           MPI          9 96.546875            0 
# Runtime: 0.005074 s (overhead: 0.000000 %) 10 records
