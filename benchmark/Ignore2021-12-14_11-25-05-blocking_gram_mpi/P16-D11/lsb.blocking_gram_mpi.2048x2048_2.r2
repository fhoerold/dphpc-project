# Sysname : Linux
# Nodename: eu-a6-006-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 12:49:33 2021
# Execution time and date (local): Tue Dec 14 13:49:33 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            2          2048          2048           MPI          0 1455647.359375            0 
            2          2048          2048           MPI          1 1463071.153320            5 
            2          2048          2048           MPI          2 1466844.972656            5 
            2          2048          2048           MPI          3 1455173.689453            0 
            2          2048          2048           MPI          4 1458093.056641            6 
            2          2048          2048           MPI          5 1449733.353516            0 
            2          2048          2048           MPI          6 1461044.890625            0 
            2          2048          2048           MPI          7 1467705.166992            0 
            2          2048          2048           MPI          8 1458161.199219            6 
            2          2048          2048           MPI          9 1446502.353516            0 
# Runtime: 798.145800 s (overhead: 0.000003 %) 10 records
