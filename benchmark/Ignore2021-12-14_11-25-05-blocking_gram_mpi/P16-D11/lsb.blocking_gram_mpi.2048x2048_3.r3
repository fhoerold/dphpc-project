# Sysname : Linux
# Nodename: eu-a6-006-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 12:49:33 2021
# Execution time and date (local): Tue Dec 14 13:49:33 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            3          2048          2048           MPI          0 2007680.918945            0 
            3          2048          2048           MPI          1 2014909.502930            6 
            3          2048          2048           MPI          2 2022415.255859            5 
            3          2048          2048           MPI          3 2007880.575195            0 
            3          2048          2048           MPI          4 2010514.389648            4 
            3          2048          2048           MPI          5 2009466.240234            0 
            3          2048          2048           MPI          6 2014442.355469            0 
            3          2048          2048           MPI          7 2012477.791992            0 
            3          2048          2048           MPI          8 2009864.949219            5 
            3          2048          2048           MPI          9 1983396.166992            0 
# Runtime: 801.471731 s (overhead: 0.000002 %) 10 records
