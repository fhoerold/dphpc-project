# Sysname : Linux
# Nodename: eu-a6-007-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:25:41 2021
# Execution time and date (local): Tue Dec 14 11:25:41 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            3           128           128           MPI          0 1844.257812            0 
            3           128           128           MPI          1 1804.489258            0 
            3           128           128           MPI          2 1860.333008            0 
            3           128           128           MPI          3 1822.723633            0 
            3           128           128           MPI          4 1859.906250            0 
            3           128           128           MPI          5 1805.754883            0 
            3           128           128           MPI          6 1786.517578            0 
            3           128           128           MPI          7 1848.028320            0 
            3           128           128           MPI          8 1837.228516            0 
            3           128           128           MPI          9 1850.362305            0 
# Runtime: 4.037200 s (overhead: 0.000000 %) 10 records
