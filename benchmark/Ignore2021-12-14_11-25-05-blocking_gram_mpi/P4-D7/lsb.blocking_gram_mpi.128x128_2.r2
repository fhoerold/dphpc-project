# Sysname : Linux
# Nodename: eu-a6-007-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:25:41 2021
# Execution time and date (local): Tue Dec 14 11:25:41 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            2           128           128           MPI          0 1379.852539            0 
            2           128           128           MPI          1 1328.962891            0 
            2           128           128           MPI          2 1358.592773            0 
            2           128           128           MPI          3 1331.917969            0 
            2           128           128           MPI          4 1342.903320            0 
            2           128           128           MPI          5 1316.453125            0 
            2           128           128           MPI          6 1295.163086            0 
            2           128           128           MPI          7 1361.232422            0 
            2           128           128           MPI          8 1365.029297            0 
            2           128           128           MPI          9 1351.779297            0 
# Runtime: 0.039308 s (overhead: 0.000000 %) 10 records
