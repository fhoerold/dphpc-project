# Sysname : Linux
# Nodename: eu-a6-007-01
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:26:38 2021
# Execution time and date (local): Tue Dec 14 11:26:38 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            2          1024          1024           MPI          0 286483.275391            0 
            2          1024          1024           MPI          1 258909.566406            3 
            2          1024          1024           MPI          2 257581.332031            4 
            2          1024          1024           MPI          3 278561.534180            0 
            2          1024          1024           MPI          4 320060.458984            1 
            2          1024          1024           MPI          5 314162.180664            0 
            2          1024          1024           MPI          6 284664.918945            0 
            2          1024          1024           MPI          7 283198.317383            0 
            2          1024          1024           MPI          8 282166.259766            5 
            2          1024          1024           MPI          9 276801.796875            0 
# Runtime: 86.205086 s (overhead: 0.000015 %) 10 records
