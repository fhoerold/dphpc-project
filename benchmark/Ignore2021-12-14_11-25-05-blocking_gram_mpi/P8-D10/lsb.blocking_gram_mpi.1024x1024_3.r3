# Sysname : Linux
# Nodename: eu-a6-007-01
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:26:38 2021
# Execution time and date (local): Tue Dec 14 11:26:38 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            3          1024          1024           MPI          0 413953.114258            0 
            3          1024          1024           MPI          1 370583.705078            5 
            3          1024          1024           MPI          2 368199.131836            5 
            3          1024          1024           MPI          3 391930.644531            0 
            3          1024          1024           MPI          4 418892.948242            2 
            3          1024          1024           MPI          5 413419.786133            0 
            3          1024          1024           MPI          6 391813.275391            0 
            3          1024          1024           MPI          7 376029.133789            0 
            3          1024          1024           MPI          8 380835.936523            5 
            3          1024          1024           MPI          9 394359.363281            0 
# Runtime: 86.322250 s (overhead: 0.000020 %) 10 records
