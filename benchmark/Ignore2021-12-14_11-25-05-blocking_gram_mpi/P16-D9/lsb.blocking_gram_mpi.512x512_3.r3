# Sysname : Linux
# Nodename: eu-a6-006-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 12:47:43 2021
# Execution time and date (local): Tue Dec 14 13:47:43 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            3           512           512           MPI          0 9659.056641            0 
            3           512           512           MPI          1 9563.152344            1 
            3           512           512           MPI          2 9491.226562            1 
            3           512           512           MPI          3 9223.768555            0 
            3           512           512           MPI          4 9115.874023            4 
            3           512           512           MPI          5 9210.901367            0 
            3           512           512           MPI          6 9177.027344            0 
            3           512           512           MPI          7 9665.383789            0 
            3           512           512           MPI          8 9430.287109            1 
            3           512           512           MPI          9 9131.292969            0 
# Runtime: 6.012637 s (overhead: 0.000116 %) 10 records
