# Sysname : Linux
# Nodename: eu-a6-006-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 12:47:43 2021
# Execution time and date (local): Tue Dec 14 13:47:43 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            2           512           512           MPI          0 7335.488281            0 
            2           512           512           MPI          1 7250.495117            4 
            2           512           512           MPI          2 7192.557617            1 
            2           512           512           MPI          3 6902.606445            0 
            2           512           512           MPI          4 6803.574219            1 
            2           512           512           MPI          5 6928.119141            0 
            2           512           512           MPI          6 6886.378906            0 
            2           512           512           MPI          7 7381.434570            0 
            2           512           512           MPI          8 7127.500000            1 
            2           512           512           MPI          9 6830.834961            0 
# Runtime: 13.017051 s (overhead: 0.000054 %) 10 records
