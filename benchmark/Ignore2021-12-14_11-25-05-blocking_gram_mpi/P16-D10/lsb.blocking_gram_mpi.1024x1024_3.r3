# Sysname : Linux
# Nodename: eu-a6-006-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 12:48:04 2021
# Execution time and date (local): Tue Dec 14 13:48:04 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            3          1024          1024           MPI          0 76394.541992            0 
            3          1024          1024           MPI          1 74472.669922            4 
            3          1024          1024           MPI          2 75597.085938            3 
            3          1024          1024           MPI          3 74433.323242            0 
            3          1024          1024           MPI          4 74007.169922            1 
            3          1024          1024           MPI          5 74259.202148            0 
            3          1024          1024           MPI          6 76302.586914            0 
            3          1024          1024           MPI          7 75162.642578            0 
            3          1024          1024           MPI          8 72900.734375            4 
            3          1024          1024           MPI          9 72578.413086            0 
# Runtime: 73.766981 s (overhead: 0.000016 %) 10 records
