# Sysname : Linux
# Nodename: eu-a6-006-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 12:48:04 2021
# Execution time and date (local): Tue Dec 14 13:48:04 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            2          1024          1024           MPI          0 55579.412109            0 
            2          1024          1024           MPI          1 55043.932617            4 
            2          1024          1024           MPI          2 55671.506836            3 
            2          1024          1024           MPI          3 54522.156250            0 
            2          1024          1024           MPI          4 54842.286133            1 
            2          1024          1024           MPI          5 54877.541016            0 
            2          1024          1024           MPI          6 56543.776367            0 
            2          1024          1024           MPI          7 55430.361328            0 
            2          1024          1024           MPI          8 53739.756836            5 
            2          1024          1024           MPI          9 54062.464844            0 
# Runtime: 76.762284 s (overhead: 0.000017 %) 10 records
