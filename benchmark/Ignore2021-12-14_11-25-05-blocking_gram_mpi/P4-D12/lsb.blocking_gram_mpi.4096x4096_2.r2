# Sysname : Linux
# Nodename: eu-a6-004-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:25:41 2021
# Execution time and date (local): Tue Dec 14 11:25:41 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            2          4096          4096           MPI          0 282891941.984375            0 
            2          4096          4096           MPI          1 285980882.544922            3 
            2          4096          4096           MPI          2 284587103.805664            4 
            2          4096          4096           MPI          3 282070028.763672            0 
            2          4096          4096           MPI          4 282166013.869141            4 
            2          4096          4096           MPI          5 287588306.551758            0 
            2          4096          4096           MPI          6 282090084.687500            1 
            2          4096          4096           MPI          7 285218831.736328            1 
            2          4096          4096           MPI          8 281333849.979492            5 
            2          4096          4096           MPI          9 288446419.886719            0 
# Runtime: 11965.089276 s (overhead: 0.000000 %) 10 records
