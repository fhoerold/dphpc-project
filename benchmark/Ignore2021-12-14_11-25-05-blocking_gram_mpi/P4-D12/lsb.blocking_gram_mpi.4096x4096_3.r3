# Sysname : Linux
# Nodename: eu-a6-004-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:25:41 2021
# Execution time and date (local): Tue Dec 14 11:25:41 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            3          4096          4096           MPI          0 397996096.776367            0 
            3          4096          4096           MPI          1 399148132.156250            5 
            3          4096          4096           MPI          2 398949639.723633            3 
            3          4096          4096           MPI          3 395507055.619141            0 
            3          4096          4096           MPI          4 396236231.001953            3 
            3          4096          4096           MPI          5 400767629.752930            0 
            3          4096          4096           MPI          6 397535113.479492            0 
            3          4096          4096           MPI          7 399683173.726562            0 
            3          4096          4096           MPI          8 394800230.574219            4 
            3          4096          4096           MPI          9 397010672.758789            0 
# Runtime: 12099.290687 s (overhead: 0.000000 %) 10 records
