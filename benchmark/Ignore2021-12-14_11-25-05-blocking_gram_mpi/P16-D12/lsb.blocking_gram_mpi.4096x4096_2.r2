# Sysname : Linux
# Nodename: eu-a6-004-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:46:50 2021
# Execution time and date (local): Tue Dec 14 11:46:50 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            2          4096          4096           MPI          0 12298069.029297            0 
            2          4096          4096           MPI          1 12375256.704102            6 
            2          4096          4096           MPI          2 12422382.953125            7 
            2          4096          4096           MPI          3 12323553.191406            0 
            2          4096          4096           MPI          4 12446666.582031            5 
            2          4096          4096           MPI          5 12457241.825195            0 
            2          4096          4096           MPI          6 12306318.267578            0 
            2          4096          4096           MPI          7 12521926.508789            0 
            2          4096          4096           MPI          8 12168887.325195            7 
            2          4096          4096           MPI          9 12160026.712891            0 
# Runtime: 6713.288669 s (overhead: 0.000000 %) 10 records
