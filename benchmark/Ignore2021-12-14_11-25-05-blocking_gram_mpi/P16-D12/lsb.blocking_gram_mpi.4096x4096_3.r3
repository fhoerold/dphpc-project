# Sysname : Linux
# Nodename: eu-a6-004-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:46:50 2021
# Execution time and date (local): Tue Dec 14 11:46:50 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            3          4096          4096           MPI          0 17305892.344727            0 
            3          4096          4096           MPI          1 17414747.771484            5 
            3          4096          4096           MPI          2 17452680.868164            6 
            3          4096          4096           MPI          3 17364379.506836            0 
            3          4096          4096           MPI          4 17651863.662109            5 
            3          4096          4096           MPI          5 17499555.970703            0 
            3          4096          4096           MPI          6 17342892.542969            0 
            3          4096          4096           MPI          7 17561247.714844            0 
            3          4096          4096           MPI          8 17167257.228516            7 
            3          4096          4096           MPI          9 17120245.488281            0 
# Runtime: 6716.612786 s (overhead: 0.000000 %) 10 records
