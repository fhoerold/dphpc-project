# Sysname : Linux
# Nodename: eu-a6-003-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:26:43 2021
# Execution time and date (local): Tue Dec 14 11:26:43 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            3          2048          2048           MPI          0 7370152.135742            0 
            3          2048          2048           MPI          1 7367570.545898            5 
            3          2048          2048           MPI          2 7348773.040039            5 
            3          2048          2048           MPI          3 7363080.513672            0 
            3          2048          2048           MPI          4 7344053.550781            6 
            3          2048          2048           MPI          5 7329597.333984            0 
            3          2048          2048           MPI          6 7342357.544922            0 
            3          2048          2048           MPI          7 7359881.744141            0 
            3          2048          2048           MPI          8 7389345.076172            6 
            3          2048          2048           MPI          9 7214088.390625            0 
# Runtime: 778.504672 s (overhead: 0.000003 %) 10 records
