# Sysname : Linux
# Nodename: eu-a6-003-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:26:43 2021
# Execution time and date (local): Tue Dec 14 11:26:43 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            2          2048          2048           MPI          0 5262099.340820            0 
            2          2048          2048           MPI          1 5245564.789062            6 
            2          2048          2048           MPI          2 5243119.669922            5 
            2          2048          2048           MPI          3 5256630.896484            0 
            2          2048          2048           MPI          4 5239687.757812            6 
            2          2048          2048           MPI          5 5233300.855469            0 
            2          2048          2048           MPI          6 5244022.074219            0 
            2          2048          2048           MPI          7 5258030.320312            0 
            2          2048          2048           MPI          8 5277690.769531            7 
            2          2048          2048           MPI          9 5189531.519531            0 
# Runtime: 776.478301 s (overhead: 0.000003 %) 10 records
