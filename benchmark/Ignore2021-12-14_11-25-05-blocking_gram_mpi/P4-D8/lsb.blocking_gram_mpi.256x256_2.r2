# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:25:41 2021
# Execution time and date (local): Tue Dec 14 11:25:41 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            2           256           256           MPI          0 11551.240234            2 
            2           256           256           MPI          1 11551.791016            1 
            2           256           256           MPI          2 11492.958008            1 
            2           256           256           MPI          3 11369.482422            0 
            2           256           256           MPI          4 11463.981445            1 
            2           256           256           MPI          5 11368.062500            0 
            2           256           256           MPI          6 11421.526367            0 
            2           256           256           MPI          7 11473.603516            0 
            2           256           256           MPI          8 11326.032227            1 
            2           256           256           MPI          9 11490.573242            0 
# Runtime: 1.381490 s (overhead: 0.000434 %) 10 records
