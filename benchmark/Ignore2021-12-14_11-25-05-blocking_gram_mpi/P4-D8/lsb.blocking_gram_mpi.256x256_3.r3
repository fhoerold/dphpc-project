# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:25:41 2021
# Execution time and date (local): Tue Dec 14 11:25:41 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            3           256           256           MPI          0 15968.232422            0 
            3           256           256           MPI          1 15954.474609            1 
            3           256           256           MPI          2 15929.581055            1 
            3           256           256           MPI          3 15769.825195            0 
            3           256           256           MPI          4 15876.356445            1 
            3           256           256           MPI          5 15723.542969            0 
            3           256           256           MPI          6 15823.983398            0 
            3           256           256           MPI          7 15867.000000            0 
            3           256           256           MPI          8 15733.441406            1 
            3           256           256           MPI          9 15884.276367            0 
# Runtime: 1.386573 s (overhead: 0.000288 %) 10 records
