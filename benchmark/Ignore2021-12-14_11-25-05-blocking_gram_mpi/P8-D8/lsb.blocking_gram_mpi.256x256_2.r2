# Sysname : Linux
# Nodename: eu-a6-006-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:26:11 2021
# Execution time and date (local): Tue Dec 14 11:26:11 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            2           256           256           MPI          0 3088.286133            0 
            2           256           256           MPI          1 2993.620117            0 
            2           256           256           MPI          2 2995.416992            0 
            2           256           256           MPI          3 2926.021484            0 
            2           256           256           MPI          4 2914.428711            0 
            2           256           256           MPI          5 2980.926758            0 
            2           256           256           MPI          6 2870.896484            0 
            2           256           256           MPI          7 2935.488281            0 
            2           256           256           MPI          8 2907.336914            2 
            2           256           256           MPI          9 2904.419922            0 
# Runtime: 14.220253 s (overhead: 0.000014 %) 10 records
