# Sysname : Linux
# Nodename: eu-a6-006-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:26:11 2021
# Execution time and date (local): Tue Dec 14 11:26:11 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            3           256           256           MPI          0 4122.014648            0 
            3           256           256           MPI          1 4033.683594            0 
            3           256           256           MPI          2 4065.488281            0 
            3           256           256           MPI          3 3997.936523            0 
            3           256           256           MPI          4 3970.894531            0 
            3           256           256           MPI          5 3707.274414            0 
            3           256           256           MPI          6 3949.586914            0 
            3           256           256           MPI          7 4001.348633            0 
            3           256           256           MPI          8 3961.749023            4 
            3           256           256           MPI          9 3972.679688            0 
# Runtime: 14.227444 s (overhead: 0.000028 %) 10 records
