# Sysname : Linux
# Nodename: eu-a6-006-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 12:47:23 2021
# Execution time and date (local): Tue Dec 14 13:47:23 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            3           128           128           MPI          0 391.931641            0 
            3           128           128           MPI          1 343.013672            0 
            3           128           128           MPI          2 369.367188            0 
            3           128           128           MPI          3 326.586914            0 
            3           128           128           MPI          4 327.221680            0 
            3           128           128           MPI          5 320.377930            0 
            3           128           128           MPI          6 324.600586            0 
            3           128           128           MPI          7 280.186523            0 
            3           128           128           MPI          8 291.484375            2 
            3           128           128           MPI          9 284.564453            0 
# Runtime: 10.020217 s (overhead: 0.000020 %) 10 records
