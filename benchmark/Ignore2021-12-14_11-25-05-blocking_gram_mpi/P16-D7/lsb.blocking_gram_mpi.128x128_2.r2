# Sysname : Linux
# Nodename: eu-a6-006-14
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 12:47:23 2021
# Execution time and date (local): Tue Dec 14 13:47:23 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            2           128           128           MPI          0 365.829102            0 
            2           128           128           MPI          1 303.174805            0 
            2           128           128           MPI          2 314.288086            0 
            2           128           128           MPI          3 283.095703            0 
            2           128           128           MPI          4 285.695312            0 
            2           128           128           MPI          5 280.920898            0 
            2           128           128           MPI          6 286.938477            0 
            2           128           128           MPI          7 242.019531            0 
            2           128           128           MPI          8 248.561523            2 
            2           128           128           MPI          9 232.690430            0 
# Runtime: 13.033177 s (overhead: 0.000015 %) 10 records
