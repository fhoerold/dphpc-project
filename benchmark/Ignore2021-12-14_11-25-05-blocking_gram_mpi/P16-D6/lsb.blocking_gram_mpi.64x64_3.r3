# Sysname : Linux
# Nodename: eu-a6-004-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:46:14 2021
# Execution time and date (local): Tue Dec 14 11:46:14 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            3            64            64           MPI          0 125.996094            0 
            3            64            64           MPI          1 231.752930            0 
            3            64            64           MPI          2 128.507812            0 
            3            64            64           MPI          3 102.425781            0 
            3            64            64           MPI          4 97.870117            0 
            3            64            64           MPI          5 87.717773            0 
            3            64            64           MPI          6 92.051758            0 
            3            64            64           MPI          7 101.826172            0 
            3            64            64           MPI          8 95.109375            0 
            3            64            64           MPI          9 85.713867            0 
# Runtime: 10.007833 s (overhead: 0.000000 %) 10 records
