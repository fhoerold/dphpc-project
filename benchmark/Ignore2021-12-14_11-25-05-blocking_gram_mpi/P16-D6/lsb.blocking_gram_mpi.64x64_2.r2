# Sysname : Linux
# Nodename: eu-a6-004-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:46:14 2021
# Execution time and date (local): Tue Dec 14 11:46:14 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            2            64            64           MPI          0 120.649414            0 
            2            64            64           MPI          1 235.455078            0 
            2            64            64           MPI          2 112.969727            0 
            2            64            64           MPI          3 92.430664            0 
            2            64            64           MPI          4 90.563477            0 
            2            64            64           MPI          5 82.412109            0 
            2            64            64           MPI          6 85.186523            0 
            2            64            64           MPI          7 95.684570            0 
            2            64            64           MPI          8 88.451172            0 
            2            64            64           MPI          9 78.402344            0 
# Runtime: 12.979234 s (overhead: 0.000000 %) 10 records
