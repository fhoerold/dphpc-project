# Sysname : Linux
# Nodename: eu-a6-004-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:25:41 2021
# Execution time and date (local): Tue Dec 14 11:25:41 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            3          2048          2048           MPI          0 45649628.124023            0 
            3          2048          2048           MPI          1 41952258.414062            5 
            3          2048          2048           MPI          2 41867710.869141            4 
            3          2048          2048           MPI          3 41871238.463867            0 
            3          2048          2048           MPI          4 41843568.518555            4 
            3          2048          2048           MPI          5 41826051.095703            0 
            3          2048          2048           MPI          6 41802154.850586            0 
            3          2048          2048           MPI          7 41896260.648438            0 
            3          2048          2048           MPI          8 41784145.686523            4 
            3          2048          2048           MPI          9 41285650.526367            0 
# Runtime: 1163.954237 s (overhead: 0.000001 %) 10 records
