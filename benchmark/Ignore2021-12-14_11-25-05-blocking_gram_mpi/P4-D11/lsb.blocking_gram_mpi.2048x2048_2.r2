# Sysname : Linux
# Nodename: eu-a6-004-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:25:41 2021
# Execution time and date (local): Tue Dec 14 11:25:41 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            2          2048          2048           MPI          0 33408928.935547            0 
            2          2048          2048           MPI          1 29724288.144531            5 
            2          2048          2048           MPI          2 29604944.551758            5 
            2          2048          2048           MPI          3 29671501.254883            0 
            2          2048          2048           MPI          4 29626705.140625            5 
            2          2048          2048           MPI          5 29611855.804688            0 
            2          2048          2048           MPI          6 29594069.000000            0 
            2          2048          2048           MPI          7 29642062.081055            0 
            2          2048          2048           MPI          8 29579811.526367            4 
            2          2048          2048           MPI          9 29520121.087891            0 
# Runtime: 1152.233076 s (overhead: 0.000002 %) 10 records
