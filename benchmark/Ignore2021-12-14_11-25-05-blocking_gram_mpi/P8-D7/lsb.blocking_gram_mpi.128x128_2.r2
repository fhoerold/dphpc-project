# Sysname : Linux
# Nodename: eu-a6-003-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:26:11 2021
# Execution time and date (local): Tue Dec 14 11:26:11 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            2           128           128           MPI          0 506.350586            0 
            2           128           128           MPI          1 484.634766            0 
            2           128           128           MPI          2 459.695312            0 
            2           128           128           MPI          3 438.970703            0 
            2           128           128           MPI          4 468.715820            0 
            2           128           128           MPI          5 463.456055            0 
            2           128           128           MPI          6 428.034180            0 
            2           128           128           MPI          7 436.840820            0 
            2           128           128           MPI          8 420.705078            0 
            2           128           128           MPI          9 437.191406            0 
# Runtime: 0.029052 s (overhead: 0.000000 %) 10 records
