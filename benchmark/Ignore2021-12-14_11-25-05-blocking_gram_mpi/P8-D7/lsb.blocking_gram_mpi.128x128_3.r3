# Sysname : Linux
# Nodename: eu-a6-003-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:26:11 2021
# Execution time and date (local): Tue Dec 14 11:26:11 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            3           128           128           MPI          0 632.087891            1 
            3           128           128           MPI          1 612.651367            0 
            3           128           128           MPI          2 572.300781            0 
            3           128           128           MPI          3 556.848633            0 
            3           128           128           MPI          4 590.929688            0 
            3           128           128           MPI          5 581.523438            0 
            3           128           128           MPI          6 547.596680            0 
            3           128           128           MPI          7 570.309570            0 
            3           128           128           MPI          8 536.965820            0 
            3           128           128           MPI          9 546.288086            0 
# Runtime: 25.029945 s (overhead: 0.000004 %) 10 records
