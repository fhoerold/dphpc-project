# Sysname : Linux
# Nodename: eu-a6-004-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:46:33 2021
# Execution time and date (local): Tue Dec 14 11:46:33 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            3           256           256           MPI          0 1453.768555            0 
            3           256           256           MPI          1 1379.338867            0 
            3           256           256           MPI          2 1354.099609            0 
            3           256           256           MPI          3 1311.162109            0 
            3           256           256           MPI          4 1322.691406            0 
            3           256           256           MPI          5 1331.126953            0 
            3           256           256           MPI          6 1269.092773            0 
            3           256           256           MPI          7 1219.211914            0 
            3           256           256           MPI          8 1253.111328            0 
            3           256           256           MPI          9 1247.455078            0 
# Runtime: 6.403855 s (overhead: 0.000000 %) 10 records
