# Sysname : Linux
# Nodename: eu-a6-004-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:46:33 2021
# Execution time and date (local): Tue Dec 14 11:46:33 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            2           256           256           MPI          0 1198.176758            1 
            2           256           256           MPI          1 1128.787109            0 
            2           256           256           MPI          2 1099.273438            0 
            2           256           256           MPI          3 1059.085938            0 
            2           256           256           MPI          4 1050.241211            0 
            2           256           256           MPI          5 1062.687500            0 
            2           256           256           MPI          6 1014.312500            0 
            2           256           256           MPI          7 963.503906            0 
            2           256           256           MPI          8 995.432617            0 
            2           256           256           MPI          9 988.505859            0 
# Runtime: 1.407747 s (overhead: 0.000071 %) 10 records
