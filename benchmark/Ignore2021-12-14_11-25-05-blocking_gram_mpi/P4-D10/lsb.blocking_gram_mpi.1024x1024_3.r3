# Sysname : Linux
# Nodename: eu-a6-009-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:25:41 2021
# Execution time and date (local): Tue Dec 14 11:25:41 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            3          1024          1024           MPI          0 3478330.050781            0 
            3          1024          1024           MPI          1 3479567.062500           10 
            3          1024          1024           MPI          2 3475117.909180            3 
            3          1024          1024           MPI          3 3478809.401367            0 
            3          1024          1024           MPI          4 3476824.611328            7 
            3          1024          1024           MPI          5 3468411.215820            0 
            3          1024          1024           MPI          6 3445487.583008            0 
            3          1024          1024           MPI          7 3460444.772461            0 
            3          1024          1024           MPI          8 3468905.019531            2 
            3          1024          1024           MPI          9 3470376.292969            0 
# Runtime: 117.275981 s (overhead: 0.000019 %) 10 records
