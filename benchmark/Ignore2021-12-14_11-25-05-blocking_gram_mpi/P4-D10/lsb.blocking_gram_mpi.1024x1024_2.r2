# Sysname : Linux
# Nodename: eu-a6-009-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 14 10:25:41 2021
# Execution time and date (local): Tue Dec 14 11:25:41 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size_m         size_n         type       id         time     overhead 
            2          1024          1024           MPI          0 2506946.467773            0 
            2          1024          1024           MPI          1 2510349.930664            7 
            2          1024          1024           MPI          2 2504618.296875            2 
            2          1024          1024           MPI          3 2505821.528320            0 
            2          1024          1024           MPI          4 2503956.240234            3 
            2          1024          1024           MPI          5 2496360.780273            0 
            2          1024          1024           MPI          6 2480410.807617            0 
            2          1024          1024           MPI          7 2495300.727539            0 
            2          1024          1024           MPI          8 2494446.563477            2 
            2          1024          1024           MPI          9 2501114.479492            0 
# Runtime: 107.354911 s (overhead: 0.000013 %) 10 records
