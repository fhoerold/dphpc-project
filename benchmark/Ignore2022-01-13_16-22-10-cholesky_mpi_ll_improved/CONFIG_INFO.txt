Invocation: ./cholesky/benchmark.sh -o ./benchmark -t 24:00 --mpi --linear-dim -d 8192 -p 32 ./cholesky/build/cholesky_mpi_ll_improved
Benchmark 'cholesky_mpi_ll_improved' for
> Dimensions:		8192, 
> Number of processors:	32, 
> CPU Model:		XeonGold_6150
> Use MPI:		true
> Use OpenMP:		false

Timestamp: 2022-01-13_16-22-10
