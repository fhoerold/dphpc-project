# Sysname : Linux
# Nodename: eu-a6-009-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:39:41 2022
# Execution time and date (local): Fri Jan  7 08:39:41 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 342969.691406            0 
            3          2048           MPI          1 339886.837891            1 
            3          2048           MPI          2 338560.619141            1 
            3          2048           MPI          3 345781.156250            0 
            3          2048           MPI          4 348051.570312            2 
# Runtime: 5.448842 s (overhead: 0.000073 %) 5 records
