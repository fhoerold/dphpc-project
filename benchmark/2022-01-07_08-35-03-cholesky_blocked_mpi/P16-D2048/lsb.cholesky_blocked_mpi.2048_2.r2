# Sysname : Linux
# Nodename: eu-a6-009-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:39:41 2022
# Execution time and date (local): Fri Jan  7 08:39:41 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 342975.107422            0 
            2          2048           MPI          1 339891.775391            1 
            2          2048           MPI          2 338566.205078            1 
            2          2048           MPI          3 345791.505859            0 
            2          2048           MPI          4 348056.789062            4 
# Runtime: 8.447615 s (overhead: 0.000071 %) 5 records
