# Sysname : Linux
# Nodename: eu-a6-008-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:35:13 2022
# Execution time and date (local): Fri Jan  7 10:35:13 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 21375181.595703            0 
            3          8192           MPI          1 21308996.246094            8 
            3          8192           MPI          2 21343536.361328            6 
            3          8192           MPI          3 20973415.488281            4 
            3          8192           MPI          4 20968115.888672            7 
# Runtime: 165.778909 s (overhead: 0.000015 %) 5 records
