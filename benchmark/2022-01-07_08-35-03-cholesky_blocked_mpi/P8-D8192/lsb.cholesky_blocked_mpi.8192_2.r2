# Sysname : Linux
# Nodename: eu-a6-008-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:35:13 2022
# Execution time and date (local): Fri Jan  7 10:35:13 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 21394669.587891            2 
            2          8192           MPI          1 21328423.353516            7 
            2          8192           MPI          2 21362996.222656            2 
            2          8192           MPI          3 20992523.515625            4 
            2          8192           MPI          4 20987226.244141            6 
# Runtime: 157.936661 s (overhead: 0.000013 %) 5 records
