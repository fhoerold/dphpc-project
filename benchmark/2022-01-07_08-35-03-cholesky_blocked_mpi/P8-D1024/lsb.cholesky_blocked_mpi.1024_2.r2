# Sysname : Linux
# Nodename: eu-a6-005-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:36:34 2022
# Execution time and date (local): Fri Jan  7 08:36:34 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 46328.277344            0 
            2          1024           MPI          1 46686.964844            3 
            2          1024           MPI          2 46583.498047            0 
            2          1024           MPI          3 49747.373047            0 
            2          1024           MPI          4 48125.269531            1 
# Runtime: 7.337307 s (overhead: 0.000055 %) 5 records
