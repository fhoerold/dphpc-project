# Sysname : Linux
# Nodename: eu-a6-005-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:36:34 2022
# Execution time and date (local): Fri Jan  7 08:36:34 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 46321.876953            0 
            3          1024           MPI          1 46784.564453            3 
            3          1024           MPI          2 46577.671875            0 
            3          1024           MPI          3 49862.455078            1 
            3          1024           MPI          4 48121.732422            6 
# Runtime: 10.326659 s (overhead: 0.000097 %) 5 records
