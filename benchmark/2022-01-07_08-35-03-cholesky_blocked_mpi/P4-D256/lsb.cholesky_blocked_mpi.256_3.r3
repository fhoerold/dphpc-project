# Sysname : Linux
# Nodename: eu-a6-009-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:35:30 2022
# Execution time and date (local): Fri Jan  7 08:35:30 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 1830.779297            0 
            3           256           MPI          1 1501.320312            3 
            3           256           MPI          2 1504.929688            0 
            3           256           MPI          3 1664.476562            0 
            3           256           MPI          4 1723.187500            0 
# Runtime: 10.014900 s (overhead: 0.000030 %) 5 records
