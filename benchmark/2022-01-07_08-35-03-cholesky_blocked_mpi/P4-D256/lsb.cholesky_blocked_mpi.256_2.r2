# Sysname : Linux
# Nodename: eu-a6-009-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:35:30 2022
# Execution time and date (local): Fri Jan  7 08:35:30 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 1466.203125            0 
            2           256           MPI          1 1452.359375            3 
            2           256           MPI          2 1460.171875            0 
            2           256           MPI          3 1612.882812            0 
            2           256           MPI          4 1676.693359            0 
# Runtime: 7.014433 s (overhead: 0.000043 %) 5 records
