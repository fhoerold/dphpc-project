# Sysname : Linux
# Nodename: eu-a6-011-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:17:37 2022
# Execution time and date (local): Fri Jan  7 10:17:37 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 36030990.052734            4 
            2          8192           MPI          1 35922023.376953            7 
            2          8192           MPI          2 36255131.472656            7 
            2          8192           MPI          3 35978699.082031            0 
            2          8192           MPI          4 36011778.134766            5 
# Runtime: 263.851845 s (overhead: 0.000009 %) 5 records
