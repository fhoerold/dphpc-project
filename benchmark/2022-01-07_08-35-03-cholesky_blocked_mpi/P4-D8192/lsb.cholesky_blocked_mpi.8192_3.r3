# Sysname : Linux
# Nodename: eu-a6-011-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:17:37 2022
# Execution time and date (local): Fri Jan  7 10:17:37 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 36033166.279297            0 
            3          8192           MPI          1 35924174.253906            6 
            3          8192           MPI          2 36257303.736328            7 
            3          8192           MPI          3 35980853.316406            0 
            3          8192           MPI          4 36013936.550781            7 
# Runtime: 262.867037 s (overhead: 0.000008 %) 5 records
