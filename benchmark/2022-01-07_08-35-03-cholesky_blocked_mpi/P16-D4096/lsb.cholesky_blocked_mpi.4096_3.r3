# Sysname : Linux
# Nodename: eu-a6-012-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:49:19 2022
# Execution time and date (local): Fri Jan  7 08:49:19 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 2419080.566406            0 
            3          4096           MPI          1 2294731.730469            1 
            3          4096           MPI          2 2345234.597656            1 
            3          4096           MPI          3 2358815.414062            0 
            3          4096           MPI          4 2367038.173828            1 
# Runtime: 17.053669 s (overhead: 0.000018 %) 5 records
