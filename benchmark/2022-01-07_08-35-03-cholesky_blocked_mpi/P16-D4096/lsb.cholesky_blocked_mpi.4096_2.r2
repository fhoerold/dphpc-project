# Sysname : Linux
# Nodename: eu-a6-012-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:49:19 2022
# Execution time and date (local): Fri Jan  7 08:49:19 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 2419079.066406            0 
            2          4096           MPI          1 2294732.601562            1 
            2          4096           MPI          2 2345233.572266            1 
            2          4096           MPI          3 2358814.289062            0 
            2          4096           MPI          4 2367036.943359            1 
# Runtime: 23.054314 s (overhead: 0.000013 %) 5 records
