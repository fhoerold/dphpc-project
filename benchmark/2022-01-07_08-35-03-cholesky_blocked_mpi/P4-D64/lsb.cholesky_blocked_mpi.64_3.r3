# Sysname : Linux
# Nodename: eu-a6-011-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:35:30 2022
# Execution time and date (local): Fri Jan  7 08:35:30 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            64           MPI          0 51.041016            0 
            3            64           MPI          1 51.599609            2 
            3            64           MPI          2 63.472656            0 
            3            64           MPI          3 78.119141            0 
            3            64           MPI          4 67.585938            0 
# Runtime: 1.995967 s (overhead: 0.000100 %) 5 records
