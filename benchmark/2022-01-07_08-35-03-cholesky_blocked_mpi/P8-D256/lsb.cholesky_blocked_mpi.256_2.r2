# Sysname : Linux
# Nodename: eu-a6-005-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:36:11 2022
# Execution time and date (local): Fri Jan  7 08:36:11 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 1564.210938            0 
            2           256           MPI          1 1524.892578            3 
            2           256           MPI          2 1425.599609            0 
            2           256           MPI          3 1431.080078            0 
            2           256           MPI          4 1476.246094            0 
# Runtime: 3.007030 s (overhead: 0.000100 %) 5 records
