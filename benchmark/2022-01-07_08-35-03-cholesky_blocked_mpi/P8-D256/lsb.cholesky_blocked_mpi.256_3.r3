# Sysname : Linux
# Nodename: eu-a6-005-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:36:11 2022
# Execution time and date (local): Fri Jan  7 08:36:11 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 1894.968750            0 
            3           256           MPI          1 1561.572266            2 
            3           256           MPI          2 1469.138672            0 
            3           256           MPI          3 1465.218750            0 
            3           256           MPI          4 1513.271484            0 
# Runtime: 4.026011 s (overhead: 0.000050 %) 5 records
