# Sysname : Linux
# Nodename: eu-a6-012-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:40:49 2022
# Execution time and date (local): Fri Jan  7 08:40:49 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 3185.429688            0 
            2           256           MPI          1 3004.808594            3 
            2           256           MPI          2 2817.626953            0 
            2           256           MPI          3 2510.142578            0 
            2           256           MPI          4 2520.583984            0 
# Runtime: 1.026685 s (overhead: 0.000292 %) 5 records
