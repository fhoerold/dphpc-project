# Sysname : Linux
# Nodename: eu-a6-012-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:40:49 2022
# Execution time and date (local): Fri Jan  7 08:40:49 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 3077.589844            0 
            3           256           MPI          1 2775.753906            4 
            3           256           MPI          2 3046.759766            1 
            3           256           MPI          3 2508.611328            0 
            3           256           MPI          4 2518.958984            0 
# Runtime: 7.027388 s (overhead: 0.000071 %) 5 records
