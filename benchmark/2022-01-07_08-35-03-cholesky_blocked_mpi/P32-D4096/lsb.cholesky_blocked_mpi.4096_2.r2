# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:29:17 2022
# Execution time and date (local): Fri Jan  7 12:29:17 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 2406497.087891            0 
            2          4096           MPI          1 2320142.630859            5 
            2          4096           MPI          2 2352855.542969            2 
            2          4096           MPI          3 2342820.953125            0 
            2          4096           MPI          4 2393572.910156            2 
# Runtime: 22.735086 s (overhead: 0.000040 %) 5 records
