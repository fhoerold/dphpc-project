# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:29:17 2022
# Execution time and date (local): Fri Jan  7 12:29:17 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 2405486.205078            0 
            3          4096           MPI          1 2319166.626953            4 
            3          4096           MPI          2 2351866.757812            1 
            3          4096           MPI          3 2341836.371094            0 
            3          4096           MPI          4 2392565.501953            1 
# Runtime: 25.727326 s (overhead: 0.000023 %) 5 records
