# Sysname : Linux
# Nodename: eu-a6-002-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:37:33 2022
# Execution time and date (local): Fri Jan  7 08:37:33 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 359333.128906            0 
            3          2048           MPI          1 356440.070312            1 
            3          2048           MPI          2 366427.111328            2 
            3          2048           MPI          3 353398.734375            0 
            3          2048           MPI          4 355592.441406            1 
# Runtime: 8.581640 s (overhead: 0.000047 %) 5 records
