# Sysname : Linux
# Nodename: eu-a6-002-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:37:33 2022
# Execution time and date (local): Fri Jan  7 08:37:33 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 359310.796875            0 
            2          2048           MPI          1 356410.558594            0 
            2          2048           MPI          2 366408.927734            1 
            2          2048           MPI          3 353381.886719            0 
            2          2048           MPI          4 355580.486328            1 
# Runtime: 8.582144 s (overhead: 0.000023 %) 5 records
