# Sysname : Linux
# Nodename: eu-a6-009-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:38:37 2022
# Execution time and date (local): Fri Jan  7 08:38:37 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 43497.757812            0 
            3          1024           MPI          1 43896.248047            3 
            3          1024           MPI          2 43815.652344            1 
            3          1024           MPI          3 45678.558594            0 
            3          1024           MPI          4 44533.589844            1 
# Runtime: 8.327138 s (overhead: 0.000060 %) 5 records
