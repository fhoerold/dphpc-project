# Sysname : Linux
# Nodename: eu-a6-009-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:38:37 2022
# Execution time and date (local): Fri Jan  7 08:38:37 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 43498.906250            0 
            2          1024           MPI          1 43896.675781            3 
            2          1024           MPI          2 43816.169922            0 
            2          1024           MPI          3 45678.855469            0 
            2          1024           MPI          4 44533.521484            1 
# Runtime: 1.323170 s (overhead: 0.000302 %) 5 records
