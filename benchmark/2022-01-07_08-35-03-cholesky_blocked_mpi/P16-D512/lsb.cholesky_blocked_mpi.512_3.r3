# Sysname : Linux
# Nodename: eu-a6-012-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:41:06 2022
# Execution time and date (local): Fri Jan  7 08:41:06 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 8636.443359            0 
            3           512           MPI          1 8681.080078            4 
            3           512           MPI          2 8799.949219            0 
            3           512           MPI          3 8350.414062            0 
            3           512           MPI          4 8504.251953            0 
# Runtime: 2.069690 s (overhead: 0.000193 %) 5 records
