# Sysname : Linux
# Nodename: eu-a6-012-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:41:06 2022
# Execution time and date (local): Fri Jan  7 08:41:06 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 8631.228516            0 
            2           512           MPI          1 9030.052734            4 
            2           512           MPI          2 8773.736328            0 
            2           512           MPI          3 8351.365234            0 
            2           512           MPI          4 8504.794922            1 
# Runtime: 5.070296 s (overhead: 0.000099 %) 5 records
