# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:14:00 2022
# Execution time and date (local): Fri Jan  7 12:14:00 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 1193.751953            0 
            3           128           MPI          1 865.218750            3 
            3           128           MPI          2 788.964844            0 
            3           128           MPI          3 710.615234            0 
            3           128           MPI          4 705.091797            0 
# Runtime: 9.986378 s (overhead: 0.000030 %) 5 records
