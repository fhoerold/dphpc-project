# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:14:00 2022
# Execution time and date (local): Fri Jan  7 12:14:00 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 1256.507812            0 
            2           128           MPI          1 865.482422            2 
            2           128           MPI          2 791.716797            0 
            2           128           MPI          3 713.033203            0 
            2           128           MPI          4 706.312500            0 
# Runtime: 8.981724 s (overhead: 0.000022 %) 5 records
