# Sysname : Linux
# Nodename: eu-a6-012-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:40:30 2022
# Execution time and date (local): Fri Jan  7 08:40:30 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            64           MPI          0 166.914062            1 
            2            64           MPI          1 89.962891            2 
            2            64           MPI          2 90.613281            0 
            2            64           MPI          3 96.486328            0 
            2            64           MPI          4 73.867188            0 
# Runtime: 9.007733 s (overhead: 0.000033 %) 5 records
