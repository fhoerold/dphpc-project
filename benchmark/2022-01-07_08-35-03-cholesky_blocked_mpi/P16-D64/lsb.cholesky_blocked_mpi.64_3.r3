# Sysname : Linux
# Nodename: eu-a6-012-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:40:30 2022
# Execution time and date (local): Fri Jan  7 08:40:30 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            64           MPI          0 115.769531            0 
            3            64           MPI          1 127.546875            2 
            3            64           MPI          2 80.003906            0 
            3            64           MPI          3 93.794922            0 
            3            64           MPI          4 73.101562            0 
# Runtime: 5.004283 s (overhead: 0.000040 %) 5 records
