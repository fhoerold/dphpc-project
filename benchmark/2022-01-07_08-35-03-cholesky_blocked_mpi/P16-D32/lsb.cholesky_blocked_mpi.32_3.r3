# Sysname : Linux
# Nodename: eu-a6-009-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:38:01 2022
# Execution time and date (local): Fri Jan  7 08:38:01 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 4.984375            1 
            3            32           MPI          1 0.115234            7 
            3            32           MPI          2 0.054688            0 
            3            32           MPI          3 0.035156            0 
            3            32           MPI          4 0.023438            2 
# Runtime: 3.999003 s (overhead: 0.000250 %) 5 records
