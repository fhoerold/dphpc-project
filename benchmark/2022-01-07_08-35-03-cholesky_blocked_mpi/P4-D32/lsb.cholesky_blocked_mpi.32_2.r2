# Sysname : Linux
# Nodename: eu-a6-002-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:35:30 2022
# Execution time and date (local): Fri Jan  7 08:35:30 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 6.802734            3 
            2            32           MPI          1 0.085938            3 
            2            32           MPI          2 0.052734            0 
            2            32           MPI          3 0.015625            0 
            2            32           MPI          4 0.015625            0 
# Runtime: 4.012322 s (overhead: 0.000150 %) 5 records
