# Sysname : Linux
# Nodename: eu-a6-002-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:36:32 2022
# Execution time and date (local): Fri Jan  7 08:36:32 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 547310.873047            0 
            2          2048           MPI          1 542199.130859            1 
            2          2048           MPI          2 560873.792969            2 
            2          2048           MPI          3 553111.423828            0 
            2          2048           MPI          4 540023.412109            1 
# Runtime: 3.956109 s (overhead: 0.000101 %) 5 records
