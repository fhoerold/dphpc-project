# Sysname : Linux
# Nodename: eu-a6-002-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:36:32 2022
# Execution time and date (local): Fri Jan  7 08:36:32 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 547325.445312            0 
            3          2048           MPI          1 542218.205078            1 
            3          2048           MPI          2 560895.937500            1 
            3          2048           MPI          3 553128.410156            0 
            3          2048           MPI          4 540040.423828            1 
# Runtime: 3.953338 s (overhead: 0.000076 %) 5 records
