# Sysname : Linux
# Nodename: eu-a6-004-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:46:27 2022
# Execution time and date (local): Fri Jan  7 08:46:27 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 2641881.626953            0 
            2          4096           MPI          1 2770818.136719            1 
            2          4096           MPI          2 2770726.480469            2 
            2          4096           MPI          3 2654796.492188            0 
            2          4096           MPI          4 2663387.373047            1 
# Runtime: 24.779984 s (overhead: 0.000016 %) 5 records
