# Sysname : Linux
# Nodename: eu-a6-004-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:46:27 2022
# Execution time and date (local): Fri Jan  7 08:46:27 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 2639244.070312            0 
            3          4096           MPI          1 2768039.525391            1 
            3          4096           MPI          2 2767950.738281            2 
            3          4096           MPI          3 2652143.628906            0 
            3          4096           MPI          4 2660686.505859            1 
# Runtime: 24.745768 s (overhead: 0.000016 %) 5 records
