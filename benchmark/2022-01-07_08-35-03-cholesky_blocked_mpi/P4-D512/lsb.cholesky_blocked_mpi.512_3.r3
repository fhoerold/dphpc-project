# Sysname : Linux
# Nodename: eu-a6-005-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:35:30 2022
# Execution time and date (local): Fri Jan  7 08:35:30 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 10916.583984            0 
            3           512           MPI          1 10559.722656            2 
            3           512           MPI          2 10518.693359            0 
            3           512           MPI          3 10347.195312            0 
            3           512           MPI          4 10702.134766            0 
# Runtime: 2.081406 s (overhead: 0.000096 %) 5 records
