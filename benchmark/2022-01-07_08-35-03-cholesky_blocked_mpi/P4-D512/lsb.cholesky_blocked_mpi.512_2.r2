# Sysname : Linux
# Nodename: eu-a6-005-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:35:30 2022
# Execution time and date (local): Fri Jan  7 08:35:30 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 10615.734375            0 
            2           512           MPI          1 10553.101562            0 
            2           512           MPI          2 10477.960938            0 
            2           512           MPI          3 10340.447266            0 
            2           512           MPI          4 10643.371094            0 
# Runtime: 0.079275 s (overhead: 0.000000 %) 5 records
