# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:19:37 2022
# Execution time and date (local): Fri Jan  7 12:19:37 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 50298.990234            0 
            3          1024           MPI          1 49852.525391            4 
            3          1024           MPI          2 50239.763672            1 
            3          1024           MPI          3 50042.105469            0 
            3          1024           MPI          4 50015.630859            1 
# Runtime: 10.362867 s (overhead: 0.000058 %) 5 records
