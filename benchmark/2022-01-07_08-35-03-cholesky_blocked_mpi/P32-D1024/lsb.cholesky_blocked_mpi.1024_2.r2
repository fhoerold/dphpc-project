# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:19:37 2022
# Execution time and date (local): Fri Jan  7 12:19:37 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 50332.025391            0 
            2          1024           MPI          1 49884.138672            1 
            2          1024           MPI          2 50271.742188            1 
            2          1024           MPI          3 50073.921875            0 
            2          1024           MPI          4 50047.417969            1 
# Runtime: 0.373632 s (overhead: 0.000803 %) 5 records
