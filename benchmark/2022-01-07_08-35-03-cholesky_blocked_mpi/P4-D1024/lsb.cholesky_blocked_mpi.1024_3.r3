# Sysname : Linux
# Nodename: eu-a6-005-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:35:46 2022
# Execution time and date (local): Fri Jan  7 08:35:46 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 77366.875000            0 
            3          1024           MPI          1 76659.716797            2 
            3          1024           MPI          2 76175.070312            0 
            3          1024           MPI          3 79466.957031            0 
            3          1024           MPI          4 77404.851562            0 
# Runtime: 3.555646 s (overhead: 0.000056 %) 5 records
