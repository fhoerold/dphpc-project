# Sysname : Linux
# Nodename: eu-a6-005-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:35:46 2022
# Execution time and date (local): Fri Jan  7 08:35:46 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 77356.037109            0 
            2          1024           MPI          1 76648.501953            3 
            2          1024           MPI          2 76162.972656            0 
            2          1024           MPI          3 79459.144531            0 
            2          1024           MPI          4 77398.792969            0 
# Runtime: 3.555944 s (overhead: 0.000084 %) 5 records
