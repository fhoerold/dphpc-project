# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:15:24 2022
# Execution time and date (local): Fri Jan  7 12:15:24 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 295807.025391            0 
            2          2048           MPI          1 298392.501953            4 
            2          2048           MPI          2 296716.886719            1 
            2          2048           MPI          3 302529.843750            0 
            2          2048           MPI          4 299189.693359            1 
# Runtime: 12.128936 s (overhead: 0.000049 %) 5 records
