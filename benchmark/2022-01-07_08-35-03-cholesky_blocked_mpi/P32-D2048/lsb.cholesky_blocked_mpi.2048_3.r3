# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:15:24 2022
# Execution time and date (local): Fri Jan  7 12:15:24 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 295804.611328            0 
            3          2048           MPI          1 298390.603516            5 
            3          2048           MPI          2 296714.109375            1 
            3          2048           MPI          3 302527.091797            0 
            3          2048           MPI          4 299186.521484            1 
# Runtime: 15.138677 s (overhead: 0.000046 %) 5 records
