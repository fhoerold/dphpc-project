# Sysname : Linux
# Nodename: eu-a6-009-06
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:47:30 2022
# Execution time and date (local): Fri Jan  7 08:47:30 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 4376976.250000            0 
            2          4096           MPI          1 4366752.568359            1 
            2          4096           MPI          2 4356143.802734            1 
            2          4096           MPI          3 4377168.904297            0 
            2          4096           MPI          4 4450360.636719            6 
# Runtime: 32.563667 s (overhead: 0.000025 %) 5 records
