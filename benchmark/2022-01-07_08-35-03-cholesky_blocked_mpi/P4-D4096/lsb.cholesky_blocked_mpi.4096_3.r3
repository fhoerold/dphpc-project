# Sysname : Linux
# Nodename: eu-a6-009-06
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:47:30 2022
# Execution time and date (local): Fri Jan  7 08:47:30 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 4373721.394531            0 
            3          4096           MPI          1 4363493.734375            1 
            3          4096           MPI          2 4352891.433594            2 
            3          4096           MPI          3 4373896.791016            0 
            3          4096           MPI          4 4447041.511719            7 
# Runtime: 32.541276 s (overhead: 0.000031 %) 5 records
