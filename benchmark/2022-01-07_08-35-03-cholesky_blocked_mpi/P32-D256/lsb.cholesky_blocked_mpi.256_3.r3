# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:14:18 2022
# Execution time and date (local): Fri Jan  7 12:14:18 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 3295.527344            0 
            3           256           MPI          1 3215.576172            3 
            3           256           MPI          2 2837.710938            0 
            3           256           MPI          3 2689.500000            0 
            3           256           MPI          4 2632.265625            0 
# Runtime: 4.009828 s (overhead: 0.000075 %) 5 records
