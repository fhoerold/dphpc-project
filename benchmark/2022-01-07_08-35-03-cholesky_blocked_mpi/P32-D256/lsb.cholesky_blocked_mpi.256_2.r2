# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:14:18 2022
# Execution time and date (local): Fri Jan  7 12:14:18 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 3275.861328            0 
            2           256           MPI          1 3233.632812            3 
            2           256           MPI          2 2839.554688            0 
            2           256           MPI          3 2695.675781            0 
            2           256           MPI          4 2633.376953            0 
# Runtime: 6.032091 s (overhead: 0.000050 %) 5 records
