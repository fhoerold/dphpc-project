# Sysname : Linux
# Nodename: eu-a6-009-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:38:19 2022
# Execution time and date (local): Fri Jan  7 08:38:19 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 731.601562            0 
            3           128           MPI          1 792.208984            2 
            3           128           MPI          2 902.105469            0 
            3           128           MPI          3 650.201172            0 
            3           128           MPI          4 623.746094            0 
# Runtime: 7.001771 s (overhead: 0.000029 %) 5 records
