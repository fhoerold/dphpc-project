# Sysname : Linux
# Nodename: eu-a6-009-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:38:19 2022
# Execution time and date (local): Fri Jan  7 08:38:19 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 1072.546875            0 
            2           128           MPI          1 729.777344            3 
            2           128           MPI          2 908.021484            0 
            2           128           MPI          3 653.859375            0 
            2           128           MPI          4 624.601562            0 
# Runtime: 7.004577 s (overhead: 0.000043 %) 5 records
