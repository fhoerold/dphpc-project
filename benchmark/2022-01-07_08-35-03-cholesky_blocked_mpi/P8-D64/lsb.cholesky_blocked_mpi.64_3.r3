# Sysname : Linux
# Nodename: eu-a6-005-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:36:00 2022
# Execution time and date (local): Fri Jan  7 08:36:00 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            64           MPI          0 72.650391            0 
            3            64           MPI          1 84.349609            2 
            3            64           MPI          2 324.974609            0 
            3            64           MPI          3 63.468750            0 
            3            64           MPI          4 66.234375            0 
# Runtime: 4.002220 s (overhead: 0.000050 %) 5 records
