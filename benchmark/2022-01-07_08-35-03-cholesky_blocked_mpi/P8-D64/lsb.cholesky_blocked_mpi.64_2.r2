# Sysname : Linux
# Nodename: eu-a6-005-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:36:00 2022
# Execution time and date (local): Fri Jan  7 08:36:00 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            64           MPI          0 70.203125            0 
            2            64           MPI          1 97.080078            2 
            2            64           MPI          2 49.982422            0 
            2            64           MPI          3 63.361328            0 
            2            64           MPI          4 70.500000            0 
# Runtime: 1.991485 s (overhead: 0.000100 %) 5 records
