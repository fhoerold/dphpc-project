# Sysname : Linux
# Nodename: eu-a6-009-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:41:15 2022
# Execution time and date (local): Fri Jan  7 10:41:15 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 18366795.972656            0 
            3          8192           MPI          1 18491337.164062            4 
            3          8192           MPI          2 20111999.085938            1 
            3          8192           MPI          3 19567897.455078            0 
            3          8192           MPI          4 18807681.677734            7 
# Runtime: 151.812247 s (overhead: 0.000008 %) 5 records
