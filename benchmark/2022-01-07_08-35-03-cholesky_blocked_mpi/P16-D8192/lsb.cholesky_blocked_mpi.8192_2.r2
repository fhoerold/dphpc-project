# Sysname : Linux
# Nodename: eu-a6-009-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 09:41:15 2022
# Execution time and date (local): Fri Jan  7 10:41:15 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 18370223.583984            2 
            2          8192           MPI          1 18494785.884766            5 
            2          8192           MPI          2 20115750.037109            6 
            2          8192           MPI          3 19571561.425781            1 
            2          8192           MPI          4 18811189.808594            8 
# Runtime: 145.841618 s (overhead: 0.000015 %) 5 records
