# Sysname : Linux
# Nodename: eu-a6-002-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:35:30 2022
# Execution time and date (local): Fri Jan  7 08:35:30 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 762.421875            0 
            3           128           MPI          1 424.687500            3 
            3           128           MPI          2 428.931641            0 
            3           128           MPI          3 377.523438            0 
            3           128           MPI          4 383.011719            0 
# Runtime: 5.006694 s (overhead: 0.000060 %) 5 records
