# Sysname : Linux
# Nodename: eu-a6-002-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:35:30 2022
# Execution time and date (local): Fri Jan  7 08:35:30 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 695.703125            0 
            2           128           MPI          1 388.949219            0 
            2           128           MPI          2 384.976562            0 
            2           128           MPI          3 344.751953            0 
            2           128           MPI          4 342.978516            0 
# Runtime: 0.004965 s (overhead: 0.000000 %) 5 records
