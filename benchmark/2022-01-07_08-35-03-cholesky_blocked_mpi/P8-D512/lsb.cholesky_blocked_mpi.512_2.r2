# Sysname : Linux
# Nodename: eu-a6-008-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:36:12 2022
# Execution time and date (local): Fri Jan  7 08:36:12 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 7118.335938            0 
            2           512           MPI          1 7045.658203            1 
            2           512           MPI          2 6838.261719            0 
            2           512           MPI          3 6943.296875            0 
            2           512           MPI          4 6864.867188            0 
# Runtime: 0.056309 s (overhead: 0.001776 %) 5 records
