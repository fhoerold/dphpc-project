# Sysname : Linux
# Nodename: eu-a6-008-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:36:12 2022
# Execution time and date (local): Fri Jan  7 08:36:12 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 7265.626953            0 
            3           512           MPI          1 7108.587891            3 
            3           512           MPI          2 6878.074219            1 
            3           512           MPI          3 6983.857422            0 
            3           512           MPI          4 6903.123047            1 
# Runtime: 9.040526 s (overhead: 0.000055 %) 5 records
