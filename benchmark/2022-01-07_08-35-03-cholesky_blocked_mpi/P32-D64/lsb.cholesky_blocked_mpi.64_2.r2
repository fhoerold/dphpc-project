# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:18:43 2022
# Execution time and date (local): Fri Jan  7 12:18:43 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            64           MPI          0 140.783203            0 
            2            64           MPI          1 185.580078            2 
            2            64           MPI          2 43.890625            0 
            2            64           MPI          3 44.916016            0 
            2            64           MPI          4 51.544922            0 
# Runtime: 17.010922 s (overhead: 0.000012 %) 5 records
