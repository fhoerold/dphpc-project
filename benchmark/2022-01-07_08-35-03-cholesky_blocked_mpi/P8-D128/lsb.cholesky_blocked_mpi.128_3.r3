# Sysname : Linux
# Nodename: eu-a6-002-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:36:30 2022
# Execution time and date (local): Fri Jan  7 08:36:30 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 923.015625            0 
            3           128           MPI          1 587.525391            3 
            3           128           MPI          2 509.226562            0 
            3           128           MPI          3 558.757812            0 
            3           128           MPI          4 498.369141            0 
# Runtime: 2.004576 s (overhead: 0.000150 %) 5 records
