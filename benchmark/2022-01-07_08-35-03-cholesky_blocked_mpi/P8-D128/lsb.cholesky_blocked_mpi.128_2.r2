# Sysname : Linux
# Nodename: eu-a6-002-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:36:30 2022
# Execution time and date (local): Fri Jan  7 08:36:30 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 588.167969            0 
            2           128           MPI          1 564.537109            3 
            2           128           MPI          2 473.509766            0 
            2           128           MPI          3 529.283203            0 
            2           128           MPI          4 457.363281            0 
# Runtime: 7.997860 s (overhead: 0.000038 %) 5 records
