# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:19:13 2022
# Execution time and date (local): Fri Jan  7 12:19:13 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 10382.378906            0 
            2           512           MPI          1 10111.693359            3 
            2           512           MPI          2 9873.652344            0 
            2           512           MPI          3 9772.564453            0 
            2           512           MPI          4 9973.982422            0 
# Runtime: 8.086497 s (overhead: 0.000037 %) 5 records
