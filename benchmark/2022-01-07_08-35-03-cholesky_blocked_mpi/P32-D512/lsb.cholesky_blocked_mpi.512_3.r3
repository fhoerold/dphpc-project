# Sysname : Linux
# Nodename: eu-a6-005-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:19:13 2022
# Execution time and date (local): Fri Jan  7 12:19:13 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 10361.294922            0 
            3           512           MPI          1 10091.380859            3 
            3           512           MPI          2 9853.871094            0 
            3           512           MPI          3 9752.720703            0 
            3           512           MPI          4 9953.857422            0 
# Runtime: 14.049420 s (overhead: 0.000021 %) 5 records
