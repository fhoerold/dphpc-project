# Sysname : Linux
# Nodename: eu-a6-004-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:19:44 2022
# Execution time and date (local): Mon Jan 10 17:19:44 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 553603.400391            0 
         1024          1024   gramschmidt          1 510580.929688            3 
         1024          1024   gramschmidt          2 505094.289062            1 
         1024          1024   gramschmidt          3 512802.505859            0 
         1024          1024   gramschmidt          4 510314.982422            1 
         1024          1024   gramschmidt          5 513251.683594            0 
         1024          1024   gramschmidt          6 517993.572266            0 
         1024          1024   gramschmidt          7 503158.480469            0 
         1024          1024   gramschmidt          8 503238.726562            3 
         1024          1024   gramschmidt          9 477668.306641            0 
# Runtime: 5.107749 s (overhead: 0.000157 %) 10 records
