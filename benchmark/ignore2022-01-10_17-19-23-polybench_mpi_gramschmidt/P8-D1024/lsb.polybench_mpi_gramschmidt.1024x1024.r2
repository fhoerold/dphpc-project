# Sysname : Linux
# Nodename: eu-a6-004-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Mon Jan 10 16:19:44 2022
# Execution time and date (local): Mon Jan 10 17:19:44 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         1024          1024   gramschmidt          0 5530228.298828            0 
         1024          1024   gramschmidt          1 509926.912109            4 
         1024          1024   gramschmidt          2 504465.447266            1 
         1024          1024   gramschmidt          3 512147.505859            0 
         1024          1024   gramschmidt          4 509661.033203            1 
         1024          1024   gramschmidt          5 512591.990234            0 
         1024          1024   gramschmidt          6 517239.734375            0 
         1024          1024   gramschmidt          7 502607.378906            0 
         1024          1024   gramschmidt          8 502585.195312            4 
         1024          1024   gramschmidt          9 477045.302734            0 
# Runtime: 10.078544 s (overhead: 0.000099 %) 10 records
