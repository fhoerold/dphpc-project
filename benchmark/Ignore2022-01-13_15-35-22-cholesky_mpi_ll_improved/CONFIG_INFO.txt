Invocation: ./cholesky/benchmark.sh -o ./benchmark -t 24:00 --mpi --linear-dim -d 2580 -p 1 ./cholesky/build/cholesky_mpi_ll_improved
Benchmark 'cholesky_mpi_ll_improved' for
> Dimensions:		2580, 
> Number of processors:	1, 
> CPU Model:		XeonGold_6150
> Use MPI:		true
> Use OpenMP:		false

Timestamp: 2022-01-13_15-35-22
