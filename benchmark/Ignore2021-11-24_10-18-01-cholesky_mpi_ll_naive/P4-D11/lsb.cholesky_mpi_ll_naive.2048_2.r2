# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:19:26 2021
# Execution time and date (local): Wed Nov 24 10:19:26 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 505067.880859            0 
            2          2048           MPI          1 505624.083984            1 
            2          2048           MPI          2 501763.726562            1 
            2          2048           MPI          3 503446.925781            0 
            2          2048           MPI          4 504338.633789            0 
            2          2048           MPI          5 504742.577148            0 
            2          2048           MPI          6 504936.041016            0 
            2          2048           MPI          7 503380.917969            0 
            2          2048           MPI          8 504001.964844            1 
            2          2048           MPI          9 505037.084961            0 
# Runtime: 9.040591 s (overhead: 0.000033 %) 10 records
