# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:19:26 2021
# Execution time and date (local): Wed Nov 24 10:19:26 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 505337.061523            0 
            3          2048           MPI          1 506269.795898            1 
            3          2048           MPI          2 502407.191406            0 
            3          2048           MPI          3 504103.669922            0 
            3          2048           MPI          4 504985.928711            1 
            3          2048           MPI          5 505389.936523            0 
            3          2048           MPI          6 505583.995117            0 
            3          2048           MPI          7 504026.969727            0 
            3          2048           MPI          8 504660.637695            1 
            3          2048           MPI          9 505685.117188            0 
# Runtime: 9.058910 s (overhead: 0.000033 %) 10 records
