# Sysname : Linux
# Nodename: eu-a6-002-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:21:54 2021
# Execution time and date (local): Wed Nov 24 10:21:54 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 1559.086914            0 
            3           128           MPI          1 1266.015625            0 
            3           128           MPI          2 1170.673828            0 
            3           128           MPI          3 1207.230469            0 
            3           128           MPI          4 1186.243164            0 
            3           128           MPI          5 1186.857422            0 
            3           128           MPI          6 1188.361328            0 
            3           128           MPI          7 1185.308594            0 
            3           128           MPI          8 1197.718750            0 
            3           128           MPI          9 1205.618164            0 
# Runtime: 0.017930 s (overhead: 0.000000 %) 10 records
