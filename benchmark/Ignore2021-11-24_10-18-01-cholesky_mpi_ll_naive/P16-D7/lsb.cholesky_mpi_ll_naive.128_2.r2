# Sysname : Linux
# Nodename: eu-a6-002-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:21:54 2021
# Execution time and date (local): Wed Nov 24 10:21:54 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 1220.802734            1 
            2           128           MPI          1 1598.404297            3 
            2           128           MPI          2 1170.132812            3 
            2           128           MPI          3 1206.585938            0 
            2           128           MPI          4 1185.628906            0 
            2           128           MPI          5 1186.323242            0 
            2           128           MPI          6 1187.730469            0 
            2           128           MPI          7 1187.074219            0 
            2           128           MPI          8 1199.603516            1 
            2           128           MPI          9 1204.985352            0 
# Runtime: 3.033697 s (overhead: 0.000264 %) 10 records
