# Sysname : Linux
# Nodename: eu-a6-002-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:21:39 2021
# Execution time and date (local): Wed Nov 24 10:21:39 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 123.716797            0 
            3            32           MPI          1 466.595703            0 
            3            32           MPI          2 415.183594            0 
            3            32           MPI          3 124.916992            0 
            3            32           MPI          4 121.995117            0 
            3            32           MPI          5 134.252930            0 
            3            32           MPI          6 124.725586            0 
            3            32           MPI          7 133.968750            0 
            3            32           MPI          8 125.858398            0 
            3            32           MPI          9 130.121094            0 
# Runtime: 0.003941 s (overhead: 0.000000 %) 10 records
