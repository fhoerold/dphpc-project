# Sysname : Linux
# Nodename: eu-a6-002-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:21:39 2021
# Execution time and date (local): Wed Nov 24 10:21:39 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 491.026367            1 
            2            32           MPI          1 157.744141            2 
            2            32           MPI          2 116.984375            0 
            2            32           MPI          3 123.071289            0 
            2            32           MPI          4 118.834961            0 
            2            32           MPI          5 132.301758            0 
            2            32           MPI          6 121.716797            0 
            2            32           MPI          7 130.064453            0 
            2            32           MPI          8 122.790039            0 
            2            32           MPI          9 132.017578            0 
# Runtime: 8.003462 s (overhead: 0.000037 %) 10 records
