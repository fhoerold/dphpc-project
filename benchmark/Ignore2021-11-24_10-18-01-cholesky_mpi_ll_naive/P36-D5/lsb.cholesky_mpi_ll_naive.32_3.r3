# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 10:09:32 2021
# Execution time and date (local): Wed Nov 24 11:09:32 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 17.246094            2 
            3            32           MPI          1 0.501953            5 
            3            32           MPI          2 0.263672            0 
            3            32           MPI          3 0.249023            0 
            3            32           MPI          4 0.256836            1 
            3            32           MPI          5 0.222656            0 
            3            32           MPI          6 0.225586            0 
            3            32           MPI          7 0.233398            0 
            3            32           MPI          8 0.213867            3 
            3            32           MPI          9 0.261719            0 
# Runtime: 12.003813 s (overhead: 0.000092 %) 10 records
