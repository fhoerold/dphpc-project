# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 10:09:32 2021
# Execution time and date (local): Wed Nov 24 11:09:32 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 18.377930            1 
            2            32           MPI          1 0.337891            4 
            2            32           MPI          2 0.291992            0 
            2            32           MPI          3 0.230469            0 
            2            32           MPI          4 0.248047            1 
            2            32           MPI          5 0.247070            0 
            2            32           MPI          6 0.244141            0 
            2            32           MPI          7 0.214844            0 
            2            32           MPI          8 0.213867            3 
            2            32           MPI          9 0.233398            0 
# Runtime: 6.008315 s (overhead: 0.000150 %) 10 records
