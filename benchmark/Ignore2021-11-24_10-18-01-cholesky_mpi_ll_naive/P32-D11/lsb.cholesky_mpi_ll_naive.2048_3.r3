# Sysname : Linux
# Nodename: eu-a6-010-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:27:29 2021
# Execution time and date (local): Wed Nov 24 10:27:29 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 470374.309570            2 
            3          2048           MPI          1 470457.921875            3 
            3          2048           MPI          2 468976.087891            5 
            3          2048           MPI          3 467819.735352            0 
            3          2048           MPI          4 468176.035156            3 
            3          2048           MPI          5 468927.422852            0 
            3          2048           MPI          6 467164.537109            0 
            3          2048           MPI          7 468525.512695            0 
            3          2048           MPI          8 466758.758789            3 
            3          2048           MPI          9 466341.156250            0 
# Runtime: 15.667988 s (overhead: 0.000102 %) 10 records
