# Sysname : Linux
# Nodename: eu-a6-010-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:27:29 2021
# Execution time and date (local): Wed Nov 24 10:27:29 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 470394.938477            2 
            2          2048           MPI          1 470474.020508            4 
            2          2048           MPI          2 468992.023438            0 
            2          2048           MPI          3 467835.400391            0 
            2          2048           MPI          4 468195.553711            0 
            2          2048           MPI          5 468938.402344            0 
            2          2048           MPI          6 467180.908203            0 
            2          2048           MPI          7 468546.793945            0 
            2          2048           MPI          8 466780.604492            5 
            2          2048           MPI          9 466360.604492            0 
# Runtime: 18.670117 s (overhead: 0.000059 %) 10 records
