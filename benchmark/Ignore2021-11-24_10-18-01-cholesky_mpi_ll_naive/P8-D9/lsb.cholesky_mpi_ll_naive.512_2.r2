# Sysname : Linux
# Nodename: eu-a6-004-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:52 2021
# Execution time and date (local): Wed Nov 24 10:18:52 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 8304.927734            0 
            2           512           MPI          1 8320.233398            0 
            2           512           MPI          2 8316.132812            0 
            2           512           MPI          3 8304.080078            0 
            2           512           MPI          4 8312.783203            0 
            2           512           MPI          5 8281.330078            0 
            2           512           MPI          6 8277.779297            0 
            2           512           MPI          7 8279.456055            0 
            2           512           MPI          8 8279.538086            0 
            2           512           MPI          9 8315.685547            0 
# Runtime: 0.107775 s (overhead: 0.000000 %) 10 records
