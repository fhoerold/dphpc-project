# Sysname : Linux
# Nodename: eu-a6-004-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:52 2021
# Execution time and date (local): Wed Nov 24 10:18:52 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 8304.038086            0 
            3           512           MPI          1 8330.119141            0 
            3           512           MPI          2 8295.772461            0 
            3           512           MPI          3 8304.680664            0 
            3           512           MPI          4 8313.400391            0 
            3           512           MPI          5 8281.738281            0 
            3           512           MPI          6 8278.153320            0 
            3           512           MPI          7 8280.068359            0 
            3           512           MPI          8 8279.780273            0 
            3           512           MPI          9 8316.322266            0 
# Runtime: 0.104698 s (overhead: 0.000000 %) 10 records
