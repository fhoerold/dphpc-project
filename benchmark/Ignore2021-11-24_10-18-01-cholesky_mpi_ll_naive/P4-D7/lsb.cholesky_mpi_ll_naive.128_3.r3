# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:22 2021
# Execution time and date (local): Wed Nov 24 10:18:22 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 804.106445            1 
            3           128           MPI          1 849.443359            2 
            3           128           MPI          2 475.616211            0 
            3           128           MPI          3 478.890625            0 
            3           128           MPI          4 442.700195            0 
            3           128           MPI          5 446.933594            0 
            3           128           MPI          6 462.198242            0 
            3           128           MPI          7 458.214844            0 
            3           128           MPI          8 460.605469            0 
            3           128           MPI          9 470.311523            0 
# Runtime: 6.011306 s (overhead: 0.000050 %) 10 records
