# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:22 2021
# Execution time and date (local): Wed Nov 24 10:18:22 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 832.073242            1 
            2           128           MPI          1 489.867188            2 
            2           128           MPI          2 474.603516            0 
            2           128           MPI          3 478.250000            0 
            2           128           MPI          4 442.056641            0 
            2           128           MPI          5 446.293945            0 
            2           128           MPI          6 461.534180            0 
            2           128           MPI          7 457.548828            0 
            2           128           MPI          8 459.916992            0 
            2           128           MPI          9 469.691406            0 
# Runtime: 20.002487 s (overhead: 0.000015 %) 10 records
