# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 10:11:03 2021
# Execution time and date (local): Wed Nov 24 11:11:03 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 485616.182617            0 
            3          2048           MPI          1 484910.659180            3 
            3          2048           MPI          2 483932.621094            3 
            3          2048           MPI          3 485724.810547            0 
            3          2048           MPI          4 485642.678711            3 
            3          2048           MPI          5 484707.959961            0 
            3          2048           MPI          6 487377.615234            0 
            3          2048           MPI          7 486227.482422            0 
            3          2048           MPI          8 488369.329102            1 
            3          2048           MPI          9 482800.540039            0 
# Runtime: 18.871982 s (overhead: 0.000053 %) 10 records
