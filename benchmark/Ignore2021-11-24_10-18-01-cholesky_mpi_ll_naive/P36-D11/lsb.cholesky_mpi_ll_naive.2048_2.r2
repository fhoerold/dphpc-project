# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 10:11:03 2021
# Execution time and date (local): Wed Nov 24 11:11:03 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 485636.090820            2 
            2          2048           MPI          1 485270.858398            3 
            2          2048           MPI          2 484288.641602            0 
            2          2048           MPI          3 486084.036133            0 
            2          2048           MPI          4 486002.385742            1 
            2          2048           MPI          5 485067.134766            0 
            2          2048           MPI          6 487737.689453            0 
            2          2048           MPI          7 486586.919922            0 
            2          2048           MPI          8 488731.181641            5 
            2          2048           MPI          9 483165.013672            0 
# Runtime: 15.903143 s (overhead: 0.000069 %) 10 records
