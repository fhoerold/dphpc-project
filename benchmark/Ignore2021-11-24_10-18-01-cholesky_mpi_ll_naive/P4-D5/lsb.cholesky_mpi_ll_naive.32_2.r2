# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:25 2021
# Execution time and date (local): Wed Nov 24 10:18:25 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 466.672852            0 
            2            32           MPI          1 64.272461            0 
            2            32           MPI          2 66.708008            0 
            2            32           MPI          3 78.476562            0 
            2            32           MPI          4 63.282227            0 
            2            32           MPI          5 80.041992            0 
            2            32           MPI          6 103.153320            0 
            2            32           MPI          7 65.167969            0 
            2            32           MPI          8 66.340820            0 
            2            32           MPI          9 66.608398            0 
# Runtime: 0.001957 s (overhead: 0.000000 %) 10 records
