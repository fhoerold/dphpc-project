# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:25 2021
# Execution time and date (local): Wed Nov 24 10:18:25 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 75.376953            0 
            3            32           MPI          1 64.897461            2 
            3            32           MPI          2 67.067383            0 
            3            32           MPI          3 78.727539            0 
            3            32           MPI          4 63.399414            0 
            3            32           MPI          5 80.285156            0 
            3            32           MPI          6 103.241211            0 
            3            32           MPI          7 65.236328            0 
            3            32           MPI          8 66.426758            0 
            3            32           MPI          9 66.863281            0 
# Runtime: 1.990868 s (overhead: 0.000100 %) 10 records
