# Sysname : Linux
# Nodename: eu-a6-008-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:23 2021
# Execution time and date (local): Wed Nov 24 10:18:23 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 161.150391            0 
            2            32           MPI          1 168.468750            0 
            2            32           MPI          2 427.312500            0 
            2            32           MPI          3 91.467773            0 
            2            32           MPI          4 89.480469            0 
            2            32           MPI          5 91.740234            0 
            2            32           MPI          6 91.092773            0 
            2            32           MPI          7 88.973633            0 
            2            32           MPI          8 98.433594            0 
            2            32           MPI          9 96.965820            0 
# Runtime: 0.003425 s (overhead: 0.000000 %) 10 records
