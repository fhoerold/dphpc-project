# Sysname : Linux
# Nodename: eu-a6-008-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:23 2021
# Execution time and date (local): Wed Nov 24 10:18:23 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 135.276367            0 
            3            32           MPI          1 169.875000            2 
            3            32           MPI          2 425.986328            0 
            3            32           MPI          3 91.799805            0 
            3            32           MPI          4 89.866211            0 
            3            32           MPI          5 92.004883            0 
            3            32           MPI          6 91.297852            0 
            3            32           MPI          7 89.196289            0 
            3            32           MPI          8 98.274414            0 
            3            32           MPI          9 97.290039            0 
# Runtime: 13.987386 s (overhead: 0.000014 %) 10 records
