# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:24:43 2021
# Execution time and date (local): Wed Nov 24 10:24:43 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 236377.411133            0 
            2          2048           MPI          1 233306.421875            1 
            2          2048           MPI          2 234166.557617            1 
            2          2048           MPI          3 231026.639648            0 
            2          2048           MPI          4 231591.582031            0 
            2          2048           MPI          5 232980.492188            0 
            2          2048           MPI          6 231877.183594            0 
            2          2048           MPI          7 231265.847656            0 
            2          2048           MPI          8 232683.111328            1 
            2          2048           MPI          9 232400.619141            0 
# Runtime: 2.805762 s (overhead: 0.000107 %) 10 records
