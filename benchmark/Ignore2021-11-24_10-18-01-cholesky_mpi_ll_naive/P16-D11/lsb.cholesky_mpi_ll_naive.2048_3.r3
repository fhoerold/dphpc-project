# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:24:43 2021
# Execution time and date (local): Wed Nov 24 10:24:43 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 236339.839844            0 
            3          2048           MPI          1 233331.343750            1 
            3          2048           MPI          2 234192.696289            0 
            3          2048           MPI          3 231052.951172            0 
            3          2048           MPI          4 231618.104492            0 
            3          2048           MPI          5 233006.937500            0 
            3          2048           MPI          6 231903.331055            0 
            3          2048           MPI          7 231291.907227            0 
            3          2048           MPI          8 232709.376953            0 
            3          2048           MPI          9 232426.368164            0 
# Runtime: 3.816051 s (overhead: 0.000026 %) 10 records
