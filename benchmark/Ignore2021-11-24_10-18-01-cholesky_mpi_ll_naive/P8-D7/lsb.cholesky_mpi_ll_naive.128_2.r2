# Sysname : Linux
# Nodename: eu-a6-004-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:21 2021
# Execution time and date (local): Wed Nov 24 10:18:21 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 681.309570            1 
            2           128           MPI          1 614.929688            2 
            2           128           MPI          2 602.988281            0 
            2           128           MPI          3 618.212891            0 
            2           128           MPI          4 624.613281            0 
            2           128           MPI          5 609.157227            0 
            2           128           MPI          6 605.600586            0 
            2           128           MPI          7 605.602539            0 
            2           128           MPI          8 620.934570            0 
            2           128           MPI          9 1089.740234            1 
# Runtime: 24.009739 s (overhead: 0.000017 %) 10 records
