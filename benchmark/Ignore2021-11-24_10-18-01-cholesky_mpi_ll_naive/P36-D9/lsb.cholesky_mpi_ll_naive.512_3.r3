# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 10:05:36 2021
# Execution time and date (local): Wed Nov 24 11:05:36 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 39552.447266            3 
            3           512           MPI          1 38924.204102            2 
            3           512           MPI          2 38867.745117            2 
            3           512           MPI          3 38838.362305            0 
            3           512           MPI          4 38865.830078            1 
            3           512           MPI          5 38803.459961            0 
            3           512           MPI          6 38854.779297            0 
            3           512           MPI          7 39521.177734            0 
            3           512           MPI          8 38814.858398            2 
            3           512           MPI          9 38828.391602            0 
# Runtime: 7.479403 s (overhead: 0.000134 %) 10 records
