# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 10:05:36 2021
# Execution time and date (local): Wed Nov 24 11:05:36 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 39111.200195            3 
            2           512           MPI          1 38921.032227            6 
            2           512           MPI          2 38862.828125            2 
            2           512           MPI          3 38835.018555            0 
            2           512           MPI          4 38857.507812            1 
            2           512           MPI          5 38798.067383            0 
            2           512           MPI          6 38848.194336            0 
            2           512           MPI          7 39511.560547            0 
            2           512           MPI          8 38808.995117            1 
            2           512           MPI          9 38822.019531            0 
# Runtime: 9.483130 s (overhead: 0.000137 %) 10 records
