# Sysname : Linux
# Nodename: eu-a6-010-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:26:24 2021
# Execution time and date (local): Wed Nov 24 10:26:24 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 18.732422            0 
            2            32           MPI          1 1.977539            2 
            2            32           MPI          2 2.101562            0 
            2            32           MPI          3 0.357422            0 
            2            32           MPI          4 0.384766            1 
            2            32           MPI          5 0.348633            0 
            2            32           MPI          6 0.239258            0 
            2            32           MPI          7 0.311523            0 
            2            32           MPI          8 0.289062            0 
            2            32           MPI          9 0.235352            0 
# Runtime: 8.033664 s (overhead: 0.000037 %) 10 records
