# Sysname : Linux
# Nodename: eu-a6-010-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:59:37 2021
# Execution time and date (local): Wed Nov 24 10:59:37 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 38598.474609            0 
            2           512           MPI          1 37973.818359            2 
            2           512           MPI          2 37627.608398            2 
            2           512           MPI          3 37928.637695            0 
            2           512           MPI          4 38444.767578            4 
            2           512           MPI          5 39262.100586            0 
            2           512           MPI          6 38358.716797            0 
            2           512           MPI          7 38628.993164            0 
            2           512           MPI          8 40806.782227            3 
            2           512           MPI          9 38504.221680            0 
# Runtime: 5.480897 s (overhead: 0.000201 %) 10 records
