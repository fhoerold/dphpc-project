# Sysname : Linux
# Nodename: eu-a6-010-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:59:37 2021
# Execution time and date (local): Wed Nov 24 10:59:37 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 38134.291992            0 
            3           512           MPI          1 38008.833984            1 
            3           512           MPI          2 37634.142578            0 
            3           512           MPI          3 37922.868164            0 
            3           512           MPI          4 38445.741211            1 
            3           512           MPI          5 39263.469727            0 
            3           512           MPI          6 38356.197266            0 
            3           512           MPI          7 38629.506836            0 
            3           512           MPI          8 40813.777344            7 
            3           512           MPI          9 38496.303711            0 
# Runtime: 0.476374 s (overhead: 0.001889 %) 10 records
