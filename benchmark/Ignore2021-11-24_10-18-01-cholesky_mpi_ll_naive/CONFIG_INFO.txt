Invocation: ../benchmark.sh -o /cluster/home/forstesa/repos/dphpc-project/cholesky/benchmark -d 5 7 9 11 -p 2 4 8 16 32 36 ../build/cholesky_mpi_ll_naive
Benchmark 'cholesky_mpi_ll_naive' for
> Dimensions:		32, 128, 512, 2048, 
> Number of processors:	2, 4, 8, 16, 32, 36, 
> Use Fullnode:		false
> CPU Model:		XeonGold_6150
> Use MPI:		true

Timestamp: 2021-11-24_10-18-01
