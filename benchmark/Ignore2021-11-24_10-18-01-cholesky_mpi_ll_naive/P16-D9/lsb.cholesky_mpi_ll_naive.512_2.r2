# Sysname : Linux
# Nodename: eu-a6-001-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:23:43 2021
# Execution time and date (local): Wed Nov 24 10:23:43 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 22910.691406            0 
            2           512           MPI          1 22503.805664            4 
            2           512           MPI          2 22253.019531            1 
            2           512           MPI          3 22273.678711            0 
            2           512           MPI          4 22061.980469            0 
            2           512           MPI          5 22293.741211            0 
            2           512           MPI          6 22179.341797            0 
            2           512           MPI          7 22098.519531            0 
            2           512           MPI          8 22304.588867            1 
            2           512           MPI          9 22464.679688            0 
# Runtime: 21.221019 s (overhead: 0.000028 %) 10 records
