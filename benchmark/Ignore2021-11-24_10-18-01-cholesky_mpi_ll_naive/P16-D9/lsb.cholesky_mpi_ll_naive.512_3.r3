# Sysname : Linux
# Nodename: eu-a6-001-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:23:43 2021
# Execution time and date (local): Wed Nov 24 10:23:43 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 22622.338867            3 
            3           512           MPI          1 22893.683594            4 
            3           512           MPI          2 22294.875977            0 
            3           512           MPI          3 22315.525391            0 
            3           512           MPI          4 22102.637695            0 
            3           512           MPI          5 22335.161133            0 
            3           512           MPI          6 22220.869141            0 
            3           512           MPI          7 22139.537109            0 
            3           512           MPI          8 22345.688477           10 
            3           512           MPI          9 22502.083984            0 
# Runtime: 14.263459 s (overhead: 0.000119 %) 10 records
