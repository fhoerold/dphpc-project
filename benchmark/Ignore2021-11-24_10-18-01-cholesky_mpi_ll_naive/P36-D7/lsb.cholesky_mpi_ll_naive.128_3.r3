# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 10:09:52 2021
# Execution time and date (local): Wed Nov 24 11:09:52 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 5004.315430            1 
            3           128           MPI          1 5040.219727            2 
            3           128           MPI          2 4844.166992            0 
            3           128           MPI          3 4839.520508            0 
            3           128           MPI          4 4770.710938            2 
            3           128           MPI          5 4785.861328            0 
            3           128           MPI          6 4801.922852            0 
            3           128           MPI          7 4770.070312            0 
            3           128           MPI          8 4819.367188            1 
            3           128           MPI          9 4809.902344            0 
# Runtime: 13.038511 s (overhead: 0.000046 %) 10 records
