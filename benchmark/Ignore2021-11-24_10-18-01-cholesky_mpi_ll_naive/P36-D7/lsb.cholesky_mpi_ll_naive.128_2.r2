# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 10:09:52 2021
# Execution time and date (local): Wed Nov 24 11:09:52 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 5277.899414            3 
            2           128           MPI          1 5031.231445            2 
            2           128           MPI          2 4848.542969            1 
            2           128           MPI          3 4844.479492            0 
            2           128           MPI          4 4773.432617            1 
            2           128           MPI          5 4788.027344            0 
            2           128           MPI          6 4806.468750            0 
            2           128           MPI          7 4774.679688            0 
            2           128           MPI          8 4823.616211            1 
            2           128           MPI          9 4814.160156            0 
# Runtime: 10.042409 s (overhead: 0.000080 %) 10 records
