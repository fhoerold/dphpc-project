# Sysname : Linux
# Nodename: eu-a6-008-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:19:31 2021
# Execution time and date (local): Wed Nov 24 10:19:31 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 284489.476562            0 
            2          2048           MPI          1 283998.796875            1 
            2          2048           MPI          2 284366.330078            0 
            2          2048           MPI          3 284137.841797            0 
            2          2048           MPI          4 284075.518555            0 
            2          2048           MPI          5 284063.207031            0 
            2          2048           MPI          6 284423.566406            0 
            2          2048           MPI          7 283997.095703            0 
            2          2048           MPI          8 284114.560547            0 
            2          2048           MPI          9 283969.181641            0 
# Runtime: 11.412990 s (overhead: 0.000009 %) 10 records
