# Sysname : Linux
# Nodename: eu-a6-008-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:19:31 2021
# Execution time and date (local): Wed Nov 24 10:19:31 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 284617.184570            0 
            3          2048           MPI          1 284243.109375            0 
            3          2048           MPI          2 284610.662109            0 
            3          2048           MPI          3 284382.512695            0 
            3          2048           MPI          4 284319.524414            0 
            3          2048           MPI          5 284307.316406            0 
            3          2048           MPI          6 284668.091797            0 
            3          2048           MPI          7 284241.541016            0 
            3          2048           MPI          8 284358.537109            0 
            3          2048           MPI          9 284213.340820            0 
# Runtime: 6.425467 s (overhead: 0.000000 %) 10 records
