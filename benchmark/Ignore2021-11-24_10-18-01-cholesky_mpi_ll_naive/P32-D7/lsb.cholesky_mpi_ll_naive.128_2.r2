# Sysname : Linux
# Nodename: eu-a6-010-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:59:22 2021
# Execution time and date (local): Wed Nov 24 10:59:22 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 5115.096680            0 
            2           128           MPI          1 4731.781250            4 
            2           128           MPI          2 4830.992188            0 
            2           128           MPI          3 4815.000977            0 
            2           128           MPI          4 4910.163086            3 
            2           128           MPI          5 4939.794922            0 
            2           128           MPI          6 4912.190430            0 
            2           128           MPI          7 4913.189453            0 
            2           128           MPI          8 4903.918945            0 
            2           128           MPI          9 4914.455078            0 
# Runtime: 3.104751 s (overhead: 0.000225 %) 10 records
