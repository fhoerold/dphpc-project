# Sysname : Linux
# Nodename: eu-a6-010-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:59:22 2021
# Execution time and date (local): Wed Nov 24 10:59:22 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 4771.052734            0 
            3           128           MPI          1 4976.363281            4 
            3           128           MPI          2 4831.129883            0 
            3           128           MPI          3 4814.894531            0 
            3           128           MPI          4 4905.398438            0 
            3           128           MPI          5 4942.842773            0 
            3           128           MPI          6 4912.362305            0 
            3           128           MPI          7 4913.250977            0 
            3           128           MPI          8 4901.389648            0 
            3           128           MPI          9 4916.817383            0 
# Runtime: 7.089140 s (overhead: 0.000056 %) 10 records
