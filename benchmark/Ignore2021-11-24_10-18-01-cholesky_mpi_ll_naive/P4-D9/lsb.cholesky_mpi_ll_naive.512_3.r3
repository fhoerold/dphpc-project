# Sysname : Linux
# Nodename: eu-a6-006-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:22 2021
# Execution time and date (local): Wed Nov 24 10:18:22 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 9394.655273            0 
            3           512           MPI          1 9312.741211            2 
            3           512           MPI          2 9368.387695            0 
            3           512           MPI          3 9346.371094            0 
            3           512           MPI          4 9293.407227            0 
            3           512           MPI          5 9304.650391            0 
            3           512           MPI          6 9285.179688            0 
            3           512           MPI          7 9277.087891            0 
            3           512           MPI          8 9290.751953            0 
            3           512           MPI          9 9322.576172            0 
# Runtime: 1.106239 s (overhead: 0.000181 %) 10 records
