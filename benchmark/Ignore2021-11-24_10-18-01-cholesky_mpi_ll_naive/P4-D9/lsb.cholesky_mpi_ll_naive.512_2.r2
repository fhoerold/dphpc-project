# Sysname : Linux
# Nodename: eu-a6-006-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:22 2021
# Execution time and date (local): Wed Nov 24 10:18:22 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 9713.815430            0 
            2           512           MPI          1 9311.613281            2 
            2           512           MPI          2 9367.513672            0 
            2           512           MPI          3 9345.071289            0 
            2           512           MPI          4 9292.524414            0 
            2           512           MPI          5 9303.833008            0 
            2           512           MPI          6 9284.324219            0 
            2           512           MPI          7 9276.246094            0 
            2           512           MPI          8 9289.847656            0 
            2           512           MPI          9 9321.858398            0 
# Runtime: 1.105112 s (overhead: 0.000181 %) 10 records
