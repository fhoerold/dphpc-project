# Sysname : Linux
# Nodename: eu-a6-002-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:21:18 2021
# Execution time and date (local): Wed Nov 24 10:21:18 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 979826.899414            0 
            2          2048           MPI          1 980691.590820            1 
            2          2048           MPI          2 978889.043945            0 
            2          2048           MPI          3 981216.958984            0 
            2          2048           MPI          4 980592.500977            0 
            2          2048           MPI          5 981878.809570            0 
            2          2048           MPI          6 980319.595703            0 
            2          2048           MPI          7 979756.900391            0 
            2          2048           MPI          8 979762.661133            1 
            2          2048           MPI          9 980684.496094            0 
# Runtime: 14.741180 s (overhead: 0.000014 %) 10 records
