# Sysname : Linux
# Nodename: eu-a6-002-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:21:18 2021
# Execution time and date (local): Wed Nov 24 10:21:18 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 980003.002930            0 
            3          2048           MPI          1 980547.566406            1 
            3          2048           MPI          2 978742.351562            1 
            3          2048           MPI          3 981072.725586            0 
            3          2048           MPI          4 980450.170898            1 
            3          2048           MPI          5 981733.348633            0 
            3          2048           MPI          6 980174.721680            0 
            3          2048           MPI          7 979612.827148            0 
            3          2048           MPI          8 979616.522461            1 
            3          2048           MPI          9 980535.992188            0 
# Runtime: 14.742715 s (overhead: 0.000027 %) 10 records
