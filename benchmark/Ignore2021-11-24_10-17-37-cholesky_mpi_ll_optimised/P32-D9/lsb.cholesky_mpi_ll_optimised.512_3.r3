# Sysname : Linux
# Nodename: eu-a6-010-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:59:07 2021
# Execution time and date (local): Wed Nov 24 10:59:07 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 62695.359375            0 
            3           512           MPI          1 62370.238281            3 
            3           512           MPI          2 62178.315430            0 
            3           512           MPI          3 62562.081055            0 
            3           512           MPI          4 62378.721680            0 
            3           512           MPI          5 63358.300781            0 
            3           512           MPI          6 62125.869141            0 
            3           512           MPI          7 62230.474609            0 
            3           512           MPI          8 63822.555664            7 
            3           512           MPI          9 63943.919922            0 
# Runtime: 7.775496 s (overhead: 0.000129 %) 10 records
