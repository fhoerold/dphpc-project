# Sysname : Linux
# Nodename: eu-a6-010-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:59:07 2021
# Execution time and date (local): Wed Nov 24 10:59:07 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 63076.123047            1 
            2           512           MPI          1 62366.564453            3 
            2           512           MPI          2 62174.869141            4 
            2           512           MPI          3 62559.335938            0 
            2           512           MPI          4 62377.879883            4 
            2           512           MPI          5 63354.771484            0 
            2           512           MPI          6 62118.827148            0 
            2           512           MPI          7 62222.826172            0 
            2           512           MPI          8 63816.480469            0 
            2           512           MPI          9 63947.211914            0 
# Runtime: 5.783192 s (overhead: 0.000207 %) 10 records
