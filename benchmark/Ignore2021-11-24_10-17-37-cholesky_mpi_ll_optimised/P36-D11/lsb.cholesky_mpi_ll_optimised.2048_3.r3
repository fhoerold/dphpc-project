# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 10:08:59 2021
# Execution time and date (local): Wed Nov 24 11:08:59 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 1596953.599609            2 
            3          2048           MPI          1 1604405.866211            6 
            3          2048           MPI          2 1597309.094727            1 
            3          2048           MPI          3 1596484.547852            0 
            3          2048           MPI          4 1601519.673828            1 
            3          2048           MPI          5 1598271.870117            0 
            3          2048           MPI          6 1601747.166992            0 
            3          2048           MPI          7 1612692.594727            0 
            3          2048           MPI          8 1600384.807617            1 
            3          2048           MPI          9 1600181.253906            0 
# Runtime: 24.355030 s (overhead: 0.000045 %) 10 records
