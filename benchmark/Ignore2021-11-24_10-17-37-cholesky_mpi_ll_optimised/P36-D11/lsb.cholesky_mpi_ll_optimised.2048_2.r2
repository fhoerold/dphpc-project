# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 10:08:59 2021
# Execution time and date (local): Wed Nov 24 11:08:59 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 1596284.802734            0 
            2          2048           MPI          1 1604118.100586            4 
            2          2048           MPI          2 1597021.495117            1 
            2          2048           MPI          3 1596198.181641            0 
            2          2048           MPI          4 1601238.113281            1 
            2          2048           MPI          5 1597986.017578            0 
            2          2048           MPI          6 1601455.634766            0 
            2          2048           MPI          7 1612405.311523            0 
            2          2048           MPI          8 1600097.308594            1 
            2          2048           MPI          9 1599894.799805            0 
# Runtime: 26.327966 s (overhead: 0.000027 %) 10 records
