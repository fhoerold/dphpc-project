# Sysname : Linux
# Nodename: eu-a6-004-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:12 2021
# Execution time and date (local): Wed Nov 24 10:18:12 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 17662.698242            0 
            3           512           MPI          1 17384.057617            3 
            3           512           MPI          2 17309.850586            0 
            3           512           MPI          3 17306.834961            0 
            3           512           MPI          4 17304.965820            0 
            3           512           MPI          5 17288.408203            0 
            3           512           MPI          6 17322.238281            0 
            3           512           MPI          7 17337.606445            0 
            3           512           MPI          8 17300.054688            0 
            3           512           MPI          9 17301.272461            0 
# Runtime: 1.220677 s (overhead: 0.000246 %) 10 records
