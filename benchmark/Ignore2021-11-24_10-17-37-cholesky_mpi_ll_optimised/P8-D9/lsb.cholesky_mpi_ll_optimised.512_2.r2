# Sysname : Linux
# Nodename: eu-a6-004-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:12 2021
# Execution time and date (local): Wed Nov 24 10:18:12 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 17670.470703            0 
            2           512           MPI          1 17390.691406            2 
            2           512           MPI          2 17321.238281            0 
            2           512           MPI          3 17317.441406            0 
            2           512           MPI          4 17316.401367            0 
            2           512           MPI          5 17299.952148            0 
            2           512           MPI          6 17333.693359            0 
            2           512           MPI          7 17349.044922            0 
            2           512           MPI          8 17311.706055            0 
            2           512           MPI          9 17312.849609            0 
# Runtime: 1.212299 s (overhead: 0.000165 %) 10 records
