# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 10:05:19 2021
# Execution time and date (local): Wed Nov 24 11:05:19 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 65508.732422            0 
            3           512           MPI          1 64674.772461            3 
            3           512           MPI          2 64815.499023            0 
            3           512           MPI          3 64645.107422            0 
            3           512           MPI          4 66933.814453            5 
            3           512           MPI          5 65366.842773            0 
            3           512           MPI          6 64666.049805            0 
            3           512           MPI          7 64838.977539            0 
            3           512           MPI          8 65796.118164            6 
            3           512           MPI          9 64672.822266            0 
# Runtime: 4.812725 s (overhead: 0.000291 %) 10 records
