# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 10:05:19 2021
# Execution time and date (local): Wed Nov 24 11:05:19 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 65538.236328            0 
            2           512           MPI          1 64666.779297            2 
            2           512           MPI          2 64811.464844            0 
            2           512           MPI          3 64637.436523            0 
            2           512           MPI          4 66920.462891            0 
            2           512           MPI          5 65369.417969            0 
            2           512           MPI          6 64658.642578            0 
            2           512           MPI          7 64833.400391            0 
            2           512           MPI          8 65790.454102            2 
            2           512           MPI          9 64671.183594            0 
# Runtime: 9.795313 s (overhead: 0.000041 %) 10 records
