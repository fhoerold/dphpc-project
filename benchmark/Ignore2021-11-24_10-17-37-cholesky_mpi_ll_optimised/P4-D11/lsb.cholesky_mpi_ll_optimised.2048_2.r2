# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:58 2021
# Execution time and date (local): Wed Nov 24 10:18:58 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 2519124.412109            0 
            2          2048           MPI          1 2493096.678711            3 
            2          2048           MPI          2 2193670.572266            1 
            2          2048           MPI          3 2214119.551758            0 
            2          2048           MPI          4 2419572.980469            1 
            2          2048           MPI          5 2436630.663086            0 
            2          2048           MPI          6 2405128.473633            0 
            2          2048           MPI          7 2296685.462891            0 
            2          2048           MPI          8 2182718.395508            1 
            2          2048           MPI          9 2184136.080078            0 
# Runtime: 42.410033 s (overhead: 0.000014 %) 10 records
