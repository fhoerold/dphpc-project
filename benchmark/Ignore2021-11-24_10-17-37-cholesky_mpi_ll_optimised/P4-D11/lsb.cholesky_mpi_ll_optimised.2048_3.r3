# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:58 2021
# Execution time and date (local): Wed Nov 24 10:18:58 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 2518148.745117            0 
            3          2048           MPI          1 2491563.280273            3 
            3          2048           MPI          2 2192506.820312            3 
            3          2048           MPI          3 2212947.968750            0 
            3          2048           MPI          4 2418289.101562            1 
            3          2048           MPI          5 2435344.440430            0 
            3          2048           MPI          6 2403854.316406            0 
            3          2048           MPI          7 2295467.072266            0 
            3          2048           MPI          8 2181562.450195            1 
            3          2048           MPI          9 2183027.679688            0 
# Runtime: 42.386915 s (overhead: 0.000019 %) 10 records
