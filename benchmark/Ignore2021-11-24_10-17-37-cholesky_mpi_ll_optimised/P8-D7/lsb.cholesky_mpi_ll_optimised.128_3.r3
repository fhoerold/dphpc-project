# Sysname : Linux
# Nodename: eu-a6-012-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:12 2021
# Execution time and date (local): Wed Nov 24 10:18:12 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 782.120117            0 
            3           128           MPI          1 1115.466797            2 
            3           128           MPI          2 738.183594            0 
            3           128           MPI          3 768.037109            0 
            3           128           MPI          4 788.958008            0 
            3           128           MPI          5 812.007812            0 
            3           128           MPI          6 817.146484            0 
            3           128           MPI          7 809.615234            0 
            3           128           MPI          8 813.244141            0 
            3           128           MPI          9 789.651367            0 
# Runtime: 4.015270 s (overhead: 0.000050 %) 10 records
