# Sysname : Linux
# Nodename: eu-a6-012-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:12 2021
# Execution time and date (local): Wed Nov 24 10:18:12 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 1156.063477            0 
            2           128           MPI          1 773.486328            6 
            2           128           MPI          2 738.010742            0 
            2           128           MPI          3 769.621094            0 
            2           128           MPI          4 790.496094            0 
            2           128           MPI          5 811.741211            0 
            2           128           MPI          6 816.731445            0 
            2           128           MPI          7 803.386719            0 
            2           128           MPI          8 806.438477            0 
            2           128           MPI          9 789.380859            0 
# Runtime: 1.007607 s (overhead: 0.000595 %) 10 records
