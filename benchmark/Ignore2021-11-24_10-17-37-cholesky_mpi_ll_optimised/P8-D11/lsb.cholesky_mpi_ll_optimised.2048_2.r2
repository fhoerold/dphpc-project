# Sysname : Linux
# Nodename: eu-a6-012-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:19:09 2021
# Execution time and date (local): Wed Nov 24 10:19:09 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 1311106.977539            0 
            2          2048           MPI          1 1306879.108398            2 
            2          2048           MPI          2 1323711.783203            1 
            2          2048           MPI          3 1309668.154297            0 
            2          2048           MPI          4 1325315.245117            2 
            2          2048           MPI          5 1303896.568359            0 
            2          2048           MPI          6 1301178.827148            0 
            2          2048           MPI          7 1307894.938477            0 
            2          2048           MPI          8 1295450.201172            1 
            2          2048           MPI          9 1322714.823242            0 
# Runtime: 18.751223 s (overhead: 0.000032 %) 10 records
