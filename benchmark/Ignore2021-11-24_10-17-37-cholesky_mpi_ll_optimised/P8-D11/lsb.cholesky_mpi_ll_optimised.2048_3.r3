# Sysname : Linux
# Nodename: eu-a6-012-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:19:09 2021
# Execution time and date (local): Wed Nov 24 10:19:09 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 1311663.321289            0 
            3          2048           MPI          1 1307437.481445            1 
            3          2048           MPI          2 1324278.604492            1 
            3          2048           MPI          3 1310228.559570            0 
            3          2048           MPI          4 1325880.695312            1 
            3          2048           MPI          5 1304454.146484            0 
            3          2048           MPI          6 1301734.508789            0 
            3          2048           MPI          7 1308452.636719            0 
            3          2048           MPI          8 1296002.597656            0 
            3          2048           MPI          9 1323281.411133            0 
# Runtime: 17.762790 s (overhead: 0.000017 %) 10 records
