# Sysname : Linux
# Nodename: eu-a6-008-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:11 2021
# Execution time and date (local): Wed Nov 24 10:18:11 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 443.423828            1 
            3            32           MPI          1 170.049805            1 
            3            32           MPI          2 128.859375            0 
            3            32           MPI          3 99.673828            0 
            3            32           MPI          4 99.562500            0 
            3            32           MPI          5 100.281250            0 
            3            32           MPI          6 97.179688            0 
            3            32           MPI          7 99.455078            0 
            3            32           MPI          8 101.546875            0 
            3            32           MPI          9 99.946289            0 
# Runtime: 4.998880 s (overhead: 0.000040 %) 10 records
