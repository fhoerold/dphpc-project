# Sysname : Linux
# Nodename: eu-a6-008-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:11 2021
# Execution time and date (local): Wed Nov 24 10:18:11 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 455.139648            1 
            2            32           MPI          1 165.237305            2 
            2            32           MPI          2 437.359375            0 
            2            32           MPI          3 99.208008            0 
            2            32           MPI          4 99.273438            0 
            2            32           MPI          5 99.802734            0 
            2            32           MPI          6 96.821289            0 
            2            32           MPI          7 99.200195            0 
            2            32           MPI          8 101.143555            0 
            2            32           MPI          9 99.386719            0 
# Runtime: 4.996394 s (overhead: 0.000060 %) 10 records
