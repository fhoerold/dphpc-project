# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 10:04:59 2021
# Execution time and date (local): Wed Nov 24 11:04:59 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 5186.041016            1 
            3           128           MPI          1 6058.116211            2 
            3           128           MPI          2 6758.366211            0 
            3           128           MPI          3 6501.145508            0 
            3           128           MPI          4 5665.655273            2 
            3           128           MPI          5 4997.417969            0 
            3           128           MPI          6 5013.587891            0 
            3           128           MPI          7 5007.353516            0 
            3           128           MPI          8 5158.622070            1 
            3           128           MPI          9 5360.972656            0 
# Runtime: 7.099392 s (overhead: 0.000085 %) 10 records
