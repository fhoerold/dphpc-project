# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 10:04:59 2021
# Execution time and date (local): Wed Nov 24 11:04:59 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 5639.253906            0 
            2           128           MPI          1 5540.824219            4 
            2           128           MPI          2 6753.899414            3 
            2           128           MPI          3 6506.437500            0 
            2           128           MPI          4 5657.865234            0 
            2           128           MPI          5 4992.955078            0 
            2           128           MPI          6 5010.811523            0 
            2           128           MPI          7 5004.337891            0 
            2           128           MPI          8 5155.684570            2 
            2           128           MPI          9 5357.773438            0 
# Runtime: 11.083384 s (overhead: 0.000081 %) 10 records
