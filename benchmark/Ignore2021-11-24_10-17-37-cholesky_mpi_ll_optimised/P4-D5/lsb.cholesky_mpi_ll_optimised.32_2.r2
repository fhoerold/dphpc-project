# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:12 2021
# Execution time and date (local): Wed Nov 24 10:18:12 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 82.301758            0 
            2            32           MPI          1 468.544922            2 
            2            32           MPI          2 68.026367            0 
            2            32           MPI          3 67.147461            0 
            2            32           MPI          4 66.084961            0 
            2            32           MPI          5 67.828125            0 
            2            32           MPI          6 68.646484            0 
            2            32           MPI          7 66.504883            0 
            2            32           MPI          8 65.784180            1 
            2            32           MPI          9 63.950195            0 
# Runtime: 1.995485 s (overhead: 0.000150 %) 10 records
