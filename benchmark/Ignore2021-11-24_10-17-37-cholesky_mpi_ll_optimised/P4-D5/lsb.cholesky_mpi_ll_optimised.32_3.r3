# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:12 2021
# Execution time and date (local): Wed Nov 24 10:18:12 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 416.441406            1 
            3            32           MPI          1 110.581055            2 
            3            32           MPI          2 68.577148            0 
            3            32           MPI          3 67.748047            0 
            3            32           MPI          4 66.600586            0 
            3            32           MPI          5 68.182617            0 
            3            32           MPI          6 67.283203            0 
            3            32           MPI          7 67.090820            0 
            3            32           MPI          8 66.228516            0 
            3            32           MPI          9 64.503906            0 
# Runtime: 1.003582 s (overhead: 0.000299 %) 10 records
