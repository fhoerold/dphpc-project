# Sysname : Linux
# Nodename: eu-a6-010-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:58:45 2021
# Execution time and date (local): Wed Nov 24 10:58:45 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 5533.416016            3 
            3           128           MPI          1 5069.455078            4 
            3           128           MPI          2 5423.346680            0 
            3           128           MPI          3 5209.678711            0 
            3           128           MPI          4 5232.502930            0 
            3           128           MPI          5 5251.406250            0 
            3           128           MPI          6 5240.805664            0 
            3           128           MPI          7 5266.879883            0 
            3           128           MPI          8 5231.797852            0 
            3           128           MPI          9 5255.418945            0 
# Runtime: 10.966045 s (overhead: 0.000064 %) 10 records
