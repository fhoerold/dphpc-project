# Sysname : Linux
# Nodename: eu-a6-010-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:58:45 2021
# Execution time and date (local): Wed Nov 24 10:58:45 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 5548.939453            1 
            2           128           MPI          1 5069.930664            3 
            2           128           MPI          2 5195.649414            0 
            2           128           MPI          3 5213.558594            0 
            2           128           MPI          4 5235.795898            0 
            2           128           MPI          5 5255.123047            0 
            2           128           MPI          6 5241.985352            0 
            2           128           MPI          7 5261.217773            0 
            2           128           MPI          8 5233.064453            1 
            2           128           MPI          9 5259.097656            0 
# Runtime: 5.979618 s (overhead: 0.000084 %) 10 records
