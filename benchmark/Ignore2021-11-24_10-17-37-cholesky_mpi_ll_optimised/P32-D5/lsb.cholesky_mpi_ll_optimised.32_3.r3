# Sysname : Linux
# Nodename: eu-a6-010-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:24:43 2021
# Execution time and date (local): Wed Nov 24 10:24:43 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 15.023438            1 
            3            32           MPI          1 0.665039            2 
            3            32           MPI          2 0.538086            3 
            3            32           MPI          3 0.259766            0 
            3            32           MPI          4 0.256836            0 
            3            32           MPI          5 0.320312            0 
            3            32           MPI          6 0.541992            0 
            3            32           MPI          7 0.462891            0 
            3            32           MPI          8 0.239258            1 
            3            32           MPI          9 0.254883            0 
# Runtime: 7.000891 s (overhead: 0.000100 %) 10 records
