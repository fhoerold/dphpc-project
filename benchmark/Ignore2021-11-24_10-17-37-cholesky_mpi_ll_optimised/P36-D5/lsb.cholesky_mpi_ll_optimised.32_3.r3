# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 10:04:24 2021
# Execution time and date (local): Wed Nov 24 11:04:24 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 12.818359            1 
            3            32           MPI          1 0.634766            4 
            3            32           MPI          2 0.269531            3 
            3            32           MPI          3 0.264648            0 
            3            32           MPI          4 0.248047            1 
            3            32           MPI          5 0.218750            0 
            3            32           MPI          6 0.237305            0 
            3            32           MPI          7 0.202148            0 
            3            32           MPI          8 0.211914            1 
            3            32           MPI          9 0.214844            0 
# Runtime: 8.980304 s (overhead: 0.000111 %) 10 records
