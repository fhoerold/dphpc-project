# Sysname : Linux
# Nodename: eu-a6-008-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 10:04:24 2021
# Execution time and date (local): Wed Nov 24 11:04:24 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 14.166992            2 
            2            32           MPI          1 0.479492            4 
            2            32           MPI          2 0.458008            0 
            2            32           MPI          3 0.256836            0 
            2            32           MPI          4 0.259766            0 
            2            32           MPI          5 0.221680            0 
            2            32           MPI          6 0.225586            0 
            2            32           MPI          7 0.216797            0 
            2            32           MPI          8 0.209961            3 
            2            32           MPI          9 0.242188            0 
# Runtime: 9.953053 s (overhead: 0.000090 %) 10 records
