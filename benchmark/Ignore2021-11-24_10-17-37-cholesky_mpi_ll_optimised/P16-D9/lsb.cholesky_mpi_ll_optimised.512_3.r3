# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:23:25 2021
# Execution time and date (local): Wed Nov 24 10:23:25 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 29036.208984            0 
            3           512           MPI          1 28602.053711            2 
            3           512           MPI          2 28534.873047            2 
            3           512           MPI          3 28467.008789            0 
            3           512           MPI          4 28555.053711            0 
            3           512           MPI          5 28483.679688            0 
            3           512           MPI          6 28492.350586            0 
            3           512           MPI          7 28481.164062            0 
            3           512           MPI          8 28509.619141            0 
            3           512           MPI          9 28476.443359            0 
# Runtime: 23.337777 s (overhead: 0.000017 %) 10 records
