# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:23:25 2021
# Execution time and date (local): Wed Nov 24 10:23:25 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 29058.796875            1 
            2           512           MPI          1 28603.724609            2 
            2           512           MPI          2 28536.422852            0 
            2           512           MPI          3 28468.583984            0 
            2           512           MPI          4 28556.665039            2 
            2           512           MPI          5 28485.295898            0 
            2           512           MPI          6 28494.022461            0 
            2           512           MPI          7 28480.059570            0 
            2           512           MPI          8 28511.258789            0 
            2           512           MPI          9 28478.160156            0 
# Runtime: 20.345164 s (overhead: 0.000025 %) 10 records
