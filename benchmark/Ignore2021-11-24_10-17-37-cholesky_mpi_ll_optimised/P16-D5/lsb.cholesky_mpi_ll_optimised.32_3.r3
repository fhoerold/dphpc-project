# Sysname : Linux
# Nodename: eu-a6-002-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:20:12 2021
# Execution time and date (local): Wed Nov 24 10:20:12 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 476.130859            0 
            3            32           MPI          1 157.860352            2 
            3            32           MPI          2 143.241211            0 
            3            32           MPI          3 145.649414            0 
            3            32           MPI          4 144.052734            0 
            3            32           MPI          5 146.510742            0 
            3            32           MPI          6 143.055664            0 
            3            32           MPI          7 151.991211            0 
            3            32           MPI          8 141.272461            0 
            3            32           MPI          9 148.195312            0 
# Runtime: 11.007529 s (overhead: 0.000018 %) 10 records
