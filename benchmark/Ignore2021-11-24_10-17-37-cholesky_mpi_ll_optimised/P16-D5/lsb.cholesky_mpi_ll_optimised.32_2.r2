# Sysname : Linux
# Nodename: eu-a6-002-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:20:12 2021
# Execution time and date (local): Wed Nov 24 10:20:12 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 492.910156            0 
            2            32           MPI          1 158.086914            4 
            2            32           MPI          2 141.518555            0 
            2            32           MPI          3 142.747070            0 
            2            32           MPI          4 142.716797            0 
            2            32           MPI          5 143.688477            0 
            2            32           MPI          6 141.502930            0 
            2            32           MPI          7 149.132812            0 
            2            32           MPI          8 141.703125            0 
            2            32           MPI          9 145.576172            0 
# Runtime: 4.025970 s (overhead: 0.000099 %) 10 records
