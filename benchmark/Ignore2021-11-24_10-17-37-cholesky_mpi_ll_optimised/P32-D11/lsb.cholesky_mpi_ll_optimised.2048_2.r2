# Sysname : Linux
# Nodename: eu-a6-010-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:25:51 2021
# Execution time and date (local): Wed Nov 24 10:25:51 2021
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 1509453.315430            0 
            2          2048           MPI          1 1510170.564453            4 
            2          2048           MPI          2 1508741.138672            1 
            2          2048           MPI          3 1510273.683594            0 
            2          2048           MPI          4 1508301.485352            0 
            2          2048           MPI          5 1512159.118164            0 
            2          2048           MPI          6 1520949.723633            0 
            2          2048           MPI          7 1515243.971680            0 
            2          2048           MPI          8 1509883.082031            1 
            2          2048           MPI          9 1509640.626953            0 
# Runtime: 26.259387 s (overhead: 0.000023 %) 10 records
