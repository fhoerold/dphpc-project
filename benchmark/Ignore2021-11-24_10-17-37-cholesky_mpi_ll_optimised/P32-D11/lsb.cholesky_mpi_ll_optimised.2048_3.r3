# Sysname : Linux
# Nodename: eu-a6-010-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:25:51 2021
# Execution time and date (local): Wed Nov 24 10:25:51 2021
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 1509449.648438            0 
            3          2048           MPI          1 1510171.616211            5 
            3          2048           MPI          2 1508746.806641            1 
            3          2048           MPI          3 1510272.580078            0 
            3          2048           MPI          4 1508307.058594            1 
            3          2048           MPI          5 1512155.646484            0 
            3          2048           MPI          6 1520952.358398            0 
            3          2048           MPI          7 1515245.273438            0 
            3          2048           MPI          8 1509885.537109            2 
            3          2048           MPI          9 1509638.247070            0 
# Runtime: 24.255276 s (overhead: 0.000037 %) 10 records
