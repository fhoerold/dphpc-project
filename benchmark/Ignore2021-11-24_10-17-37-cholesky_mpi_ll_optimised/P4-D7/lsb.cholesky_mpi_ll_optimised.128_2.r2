# Sysname : Linux
# Nodename: eu-a6-006-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:12 2021
# Execution time and date (local): Wed Nov 24 10:18:12 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 798.999023            0 
            2           128           MPI          1 461.825195            0 
            2           128           MPI          2 451.815430            0 
            2           128           MPI          3 459.405273            0 
            2           128           MPI          4 448.192383            0 
            2           128           MPI          5 469.570312            0 
            2           128           MPI          6 442.612305            0 
            2           128           MPI          7 477.840820            0 
            2           128           MPI          8 449.259766            0 
            2           128           MPI          9 442.655273            0 
# Runtime: 0.007887 s (overhead: 0.000000 %) 10 records
