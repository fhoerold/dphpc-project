# Sysname : Linux
# Nodename: eu-a6-006-01
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:12 2021
# Execution time and date (local): Wed Nov 24 10:18:12 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 787.753906            0 
            3           128           MPI          1 462.309570            0 
            3           128           MPI          2 452.137695            0 
            3           128           MPI          3 460.140625            0 
            3           128           MPI          4 448.697266            0 
            3           128           MPI          5 470.013672            0 
            3           128           MPI          6 443.175781            0 
            3           128           MPI          7 478.506836            0 
            3           128           MPI          8 449.680664            0 
            3           128           MPI          9 443.125977            0 
# Runtime: 0.006912 s (overhead: 0.000000 %) 10 records
