# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:12 2021
# Execution time and date (local): Wed Nov 24 10:18:12 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 25811.166016            0 
            2           512           MPI          1 25775.955078            3 
            2           512           MPI          2 25862.923828            0 
            2           512           MPI          3 25641.654297            0 
            2           512           MPI          4 25758.824219            0 
            2           512           MPI          5 25483.226562            0 
            2           512           MPI          6 25318.251953            0 
            2           512           MPI          7 25219.135742            0 
            2           512           MPI          8 25360.498047            0 
            2           512           MPI          9 25220.977539            0 
# Runtime: 4.323931 s (overhead: 0.000069 %) 10 records
