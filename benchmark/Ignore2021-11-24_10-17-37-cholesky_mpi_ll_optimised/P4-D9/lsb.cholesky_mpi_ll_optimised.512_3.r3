# Sysname : Linux
# Nodename: eu-a6-009-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:18:12 2021
# Execution time and date (local): Wed Nov 24 10:18:12 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 25434.187500            0 
            3           512           MPI          1 25427.246094            1 
            3           512           MPI          2 25857.908203            0 
            3           512           MPI          3 25636.781250            0 
            3           512           MPI          4 25754.131836            0 
            3           512           MPI          5 25477.967773            0 
            3           512           MPI          6 25324.715820            0 
            3           512           MPI          7 25213.850586            0 
            3           512           MPI          8 25355.313477            0 
            3           512           MPI          9 25216.011719            0 
# Runtime: 0.309289 s (overhead: 0.000323 %) 10 records
