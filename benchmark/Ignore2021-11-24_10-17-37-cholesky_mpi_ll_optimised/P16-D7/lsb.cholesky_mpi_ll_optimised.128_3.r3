# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:23:11 2021
# Execution time and date (local): Wed Nov 24 10:23:11 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 1624.142578            0 
            3           128           MPI          1 1394.961914            2 
            3           128           MPI          2 1589.670898            0 
            3           128           MPI          3 1334.838867            0 
            3           128           MPI          4 1304.947266            0 
            3           128           MPI          5 1325.148438            0 
            3           128           MPI          6 1315.375000            0 
            3           128           MPI          7 1336.873047            0 
            3           128           MPI          8 1311.591797            0 
            3           128           MPI          9 1314.079102            0 
# Runtime: 7.021925 s (overhead: 0.000028 %) 10 records
