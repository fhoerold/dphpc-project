# Sysname : Linux
# Nodename: eu-a6-011-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Nov 24 09:23:11 2021
# Execution time and date (local): Wed Nov 24 10:23:11 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 1628.434570            0 
            2           128           MPI          1 1684.276367            3 
            2           128           MPI          2 1332.067383            0 
            2           128           MPI          3 1333.507812            0 
            2           128           MPI          4 1303.498047            0 
            2           128           MPI          5 1323.854492            0 
            2           128           MPI          6 1314.018555            0 
            2           128           MPI          7 1337.298828            0 
            2           128           MPI          8 1310.374023            0 
            2           128           MPI          9 1308.640625            0 
# Runtime: 4.023324 s (overhead: 0.000075 %) 10 records
