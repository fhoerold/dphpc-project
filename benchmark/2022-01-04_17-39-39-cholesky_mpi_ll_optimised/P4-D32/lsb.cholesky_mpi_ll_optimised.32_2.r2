# Sysname : Linux
# Nodename: eu-a6-007-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:41:00 2022
# Execution time and date (local): Tue Jan  4 17:41:00 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 431.525391            0 
            2            32           MPI          1 67.021484            2 
            2            32           MPI          2 66.417969            0 
            2            32           MPI          3 67.601562            0 
            2            32           MPI          4 64.144531            0 
            2            32           MPI          5 66.283203            0 
            2            32           MPI          6 66.242188            0 
            2            32           MPI          7 65.994141            0 
            2            32           MPI          8 65.417969            0 
            2            32           MPI          9 67.181641            0 
# Runtime: 2.005268 s (overhead: 0.000100 %) 10 records
