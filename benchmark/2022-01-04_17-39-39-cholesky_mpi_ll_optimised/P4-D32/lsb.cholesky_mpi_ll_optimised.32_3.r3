# Sysname : Linux
# Nodename: eu-a6-007-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:41:00 2022
# Execution time and date (local): Tue Jan  4 17:41:00 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 402.177734            0 
            3            32           MPI          1 67.841797            0 
            3            32           MPI          2 66.492188            0 
            3            32           MPI          3 68.001953            0 
            3            32           MPI          4 64.546875            0 
            3            32           MPI          5 66.621094            0 
            3            32           MPI          6 66.554688            0 
            3            32           MPI          7 66.599609            0 
            3            32           MPI          8 65.687500            0 
            3            32           MPI          9 67.591797            0 
# Runtime: 0.002851 s (overhead: 0.000000 %) 10 records
