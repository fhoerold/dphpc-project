# Sysname : Linux
# Nodename: eu-a6-010-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:17:48 2022
# Execution time and date (local): Tue Jan  4 22:17:48 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            64           MPI          0 1419.201172            0 
            2            64           MPI          1 1556.333984            2 
            2            64           MPI          2 1360.060547            0 
            2            64           MPI          3 1361.189453            0 
            2            64           MPI          4 1336.373047            2 
            2            64           MPI          5 1314.500000            0 
            2            64           MPI          6 1319.320312            0 
            2            64           MPI          7 1363.177734            0 
            2            64           MPI          8 1344.720703            1 
            2            64           MPI          9 1337.105469            0 
# Runtime: 5.020746 s (overhead: 0.000100 %) 10 records
