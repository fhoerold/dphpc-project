# Sysname : Linux
# Nodename: eu-a6-010-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:17:48 2022
# Execution time and date (local): Tue Jan  4 22:17:48 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            64           MPI          0 1438.634766            1 
            3            64           MPI          1 1558.933594            2 
            3            64           MPI          2 1361.160156            1 
            3            64           MPI          3 1361.947266            0 
            3            64           MPI          4 1337.160156            0 
            3            64           MPI          5 1313.158203            0 
            3            64           MPI          6 1319.742188            0 
            3            64           MPI          7 1364.009766            0 
            3            64           MPI          8 1345.117188            2 
            3            64           MPI          9 1334.357422            0 
# Runtime: 5.021889 s (overhead: 0.000119 %) 10 records
