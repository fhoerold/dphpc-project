# Sysname : Linux
# Nodename: eu-a6-007-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:41:00 2022
# Execution time and date (local): Tue Jan  4 17:41:00 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 2342.322266            0 
            3           256           MPI          1 2075.097656            2 
            3           256           MPI          2 2056.978516            0 
            3           256           MPI          3 2069.052734            0 
            3           256           MPI          4 2071.886719            0 
            3           256           MPI          5 2054.843750            0 
            3           256           MPI          6 2069.835938            0 
            3           256           MPI          7 2048.638672            0 
            3           256           MPI          8 2044.185547            2 
            3           256           MPI          9 2058.343750            0 
# Runtime: 2.033022 s (overhead: 0.000197 %) 10 records
