# Sysname : Linux
# Nodename: eu-a6-007-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:41:00 2022
# Execution time and date (local): Tue Jan  4 17:41:00 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 2064.894531            0 
            2           256           MPI          1 2394.015625            2 
            2           256           MPI          2 2054.751953            0 
            2           256           MPI          3 2068.005859            0 
            2           256           MPI          4 2070.855469            0 
            2           256           MPI          5 2053.751953            0 
            2           256           MPI          6 2068.990234            0 
            2           256           MPI          7 2047.560547            0 
            2           256           MPI          8 2043.037109            0 
            2           256           MPI          9 2057.283203            0 
# Runtime: 2.032639 s (overhead: 0.000098 %) 10 records
