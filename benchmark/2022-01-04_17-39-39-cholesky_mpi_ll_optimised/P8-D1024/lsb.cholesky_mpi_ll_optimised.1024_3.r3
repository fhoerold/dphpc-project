# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:40:38 2022
# Execution time and date (local): Tue Jan  4 17:40:38 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 179408.150391            0 
            3          1024           MPI          1 179551.373047            4 
            3          1024           MPI          2 180049.226562            0 
            3          1024           MPI          3 183518.703125            0 
            3          1024           MPI          4 180941.580078            3 
            3          1024           MPI          5 180986.394531            0 
            3          1024           MPI          6 179739.503906            0 
            3          1024           MPI          7 178982.130859            0 
            3          1024           MPI          8 180897.263672            0 
            3          1024           MPI          9 181704.347656            0 
# Runtime: 9.164021 s (overhead: 0.000076 %) 10 records
