# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:40:38 2022
# Execution time and date (local): Tue Jan  4 17:40:38 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 179676.078125            0 
            2          1024           MPI          1 179638.693359            4 
            2          1024           MPI          2 180291.203125            0 
            2          1024           MPI          3 183752.046875            0 
            2          1024           MPI          4 181188.851562            1 
            2          1024           MPI          5 181229.542969            0 
            2          1024           MPI          6 179980.607422            0 
            2          1024           MPI          7 179222.943359            0 
            2          1024           MPI          8 181140.234375            0 
            2          1024           MPI          9 181949.310547            0 
# Runtime: 7.178558 s (overhead: 0.000070 %) 10 records
