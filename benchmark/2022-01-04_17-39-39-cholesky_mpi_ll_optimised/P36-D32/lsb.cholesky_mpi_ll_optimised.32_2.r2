# Sysname : Linux
# Nodename: eu-a6-010-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:17:33 2022
# Execution time and date (local): Tue Jan  4 22:17:33 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 8.730469            3 
            2            32           MPI          1 0.539062            2 
            2            32           MPI          2 0.333984            0 
            2            32           MPI          3 0.230469            0 
            2            32           MPI          4 0.232422            2 
            2            32           MPI          5 0.269531            0 
            2            32           MPI          6 0.248047            0 
            2            32           MPI          7 0.244141            0 
            2            32           MPI          8 0.222656            0 
            2            32           MPI          9 0.216797            0 
# Runtime: 5.982885 s (overhead: 0.000117 %) 10 records
