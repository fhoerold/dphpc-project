# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 19:19:18 2022
# Execution time and date (local): Tue Jan  4 20:19:18 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 5557.898438            1 
            2           128           MPI          1 4977.632812            3 
            2           128           MPI          2 5070.701172            0 
            2           128           MPI          3 5054.880859            0 
            2           128           MPI          4 5138.464844            0 
            2           128           MPI          5 5104.394531            0 
            2           128           MPI          6 5114.216797            0 
            2           128           MPI          7 5147.613281            0 
            2           128           MPI          8 5135.226562            2 
            2           128           MPI          9 5116.171875            0 
# Runtime: 15.045542 s (overhead: 0.000040 %) 10 records
