# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 19:19:18 2022
# Execution time and date (local): Tue Jan  4 20:19:18 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 5025.755859            0 
            3           128           MPI          1 5184.550781            5 
            3           128           MPI          2 5079.291016            0 
            3           128           MPI          3 5063.056641            0 
            3           128           MPI          4 5146.435547            2 
            3           128           MPI          5 5116.029297            0 
            3           128           MPI          6 5128.601562            0 
            3           128           MPI          7 5152.976562            0 
            3           128           MPI          8 5140.904297            2 
            3           128           MPI          9 5117.609375            0 
# Runtime: 1.056955 s (overhead: 0.000852 %) 10 records
