# Sysname : Linux
# Nodename: eu-a6-010-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:31:02 2022
# Execution time and date (local): Tue Jan  4 22:31:02 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 5325.671875            3 
            3           128           MPI          1 5380.021484            2 
            3           128           MPI          2 5236.349609            0 
            3           128           MPI          3 5218.662109            0 
            3           128           MPI          4 5222.923828            1 
            3           128           MPI          5 5203.457031            0 
            3           128           MPI          6 5167.117188            0 
            3           128           MPI          7 5197.677734            0 
            3           128           MPI          8 5199.087891            2 
            3           128           MPI          9 5179.345703            0 
# Runtime: 11.043907 s (overhead: 0.000072 %) 10 records
