# Sysname : Linux
# Nodename: eu-a6-010-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:31:02 2022
# Execution time and date (local): Tue Jan  4 22:31:02 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 5665.253906            0 
            2           128           MPI          1 5385.523438            0 
            2           128           MPI          2 5235.650391            0 
            2           128           MPI          3 5218.365234            0 
            2           128           MPI          4 5223.423828            0 
            2           128           MPI          5 5199.972656            0 
            2           128           MPI          6 5168.250000            0 
            2           128           MPI          7 5197.238281            0 
            2           128           MPI          8 5199.107422            0 
            2           128           MPI          9 5179.367188            0 
# Runtime: 0.071497 s (overhead: 0.000000 %) 10 records
