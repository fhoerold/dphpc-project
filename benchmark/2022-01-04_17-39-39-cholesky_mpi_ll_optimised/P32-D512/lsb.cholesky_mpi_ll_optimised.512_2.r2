# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 19:19:43 2022
# Execution time and date (local): Tue Jan  4 20:19:43 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 64285.259766            0 
            2           512           MPI          1 62883.826172            1 
            2           512           MPI          2 62745.240234            1 
            2           512           MPI          3 63817.130859            4 
            2           512           MPI          4 63134.566406            6 
            2           512           MPI          5 62868.908203            0 
            2           512           MPI          6 62902.986328            0 
            2           512           MPI          7 64611.300781            0 
            2           512           MPI          8 63176.613281            3 
            2           512           MPI          9 63111.177734            0 
# Runtime: 0.799533 s (overhead: 0.001876 %) 10 records
