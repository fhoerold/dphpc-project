# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 19:19:43 2022
# Execution time and date (local): Tue Jan  4 20:19:43 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 64255.931641            0 
            3           512           MPI          1 62887.978516            7 
            3           512           MPI          2 62751.822266            0 
            3           512           MPI          3 63815.250000            0 
            3           512           MPI          4 63143.925781            2 
            3           512           MPI          5 62877.544922            0 
            3           512           MPI          6 62903.480469            0 
            3           512           MPI          7 64625.441406            1 
            3           512           MPI          8 63173.445312            8 
            3           512           MPI          9 63115.083984            0 
# Runtime: 7.791938 s (overhead: 0.000231 %) 10 records
