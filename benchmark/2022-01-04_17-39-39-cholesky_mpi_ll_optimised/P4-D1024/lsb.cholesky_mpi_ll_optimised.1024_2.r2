# Sysname : Linux
# Nodename: eu-a6-007-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:41:35 2022
# Execution time and date (local): Tue Jan  4 17:41:35 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 269787.345703            3 
            2          1024           MPI          1 270134.449219            5 
            2          1024           MPI          2 287254.667969            1 
            2          1024           MPI          3 269430.371094            0 
            2          1024           MPI          4 270010.849609            1 
            2          1024           MPI          5 268423.292969            0 
            2          1024           MPI          6 270522.119141            0 
            2          1024           MPI          7 270766.919922            0 
            2          1024           MPI          8 270026.250000            7 
            2          1024           MPI          9 269871.566406            0 
# Runtime: 8.260511 s (overhead: 0.000206 %) 10 records
