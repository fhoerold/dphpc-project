# Sysname : Linux
# Nodename: eu-a6-007-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:41:35 2022
# Execution time and date (local): Tue Jan  4 17:41:35 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 269806.041016            0 
            3          1024           MPI          1 270547.148438            4 
            3          1024           MPI          2 287281.363281            1 
            3          1024           MPI          3 269455.599609            0 
            3          1024           MPI          4 270035.464844            1 
            3          1024           MPI          5 268448.367188            0 
            3          1024           MPI          6 270547.410156            0 
            3          1024           MPI          7 270791.089844            0 
            3          1024           MPI          8 270050.917969            1 
            3          1024           MPI          9 269896.589844            0 
# Runtime: 3.259622 s (overhead: 0.000215 %) 10 records
