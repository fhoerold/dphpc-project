# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:04:43 2022
# Execution time and date (local): Tue Jan  4 22:04:43 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 61600077.156250            2 
            3          8192           MPI          1 61668825.259766            4 
            3          8192           MPI          2 61105830.888672            4 
            3          8192           MPI          3 61243340.835938            0 
            3          8192           MPI          4 60886800.916016            8 
            3          8192           MPI          5 60441611.003906            3 
            3          8192           MPI          6 60453426.408203            2 
            3          8192           MPI          7 60526707.121094            2 
            3          8192           MPI          8 61702442.695312            6 
            3          8192           MPI          9 61738498.687500            0 
# Runtime: 741.884492 s (overhead: 0.000004 %) 10 records
