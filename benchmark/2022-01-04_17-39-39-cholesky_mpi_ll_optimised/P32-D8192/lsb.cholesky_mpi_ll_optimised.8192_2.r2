# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:04:43 2022
# Execution time and date (local): Tue Jan  4 22:04:43 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 61604678.001953            2 
            2          8192           MPI          1 61673443.208984            9 
            2          8192           MPI          2 61110387.710938            5 
            2          8192           MPI          3 61247916.074219            0 
            2          8192           MPI          4 60891346.044922            7 
            2          8192           MPI          5 60446111.183594            2 
            2          8192           MPI          6 60457938.363281            3 
            2          8192           MPI          7 60531234.523438            2 
            2          8192           MPI          8 61707047.863281            7 
            2          8192           MPI          9 61743117.076172            1 
# Runtime: 741.970400 s (overhead: 0.000005 %) 10 records
