# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:41:32 2022
# Execution time and date (local): Tue Jan  4 17:41:32 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 1366514.037109            0 
            2          2048           MPI          1 1363222.111328            1 
            2          2048           MPI          2 1379189.916016            1 
            2          2048           MPI          3 1364807.105469            0 
            2          2048           MPI          4 1393588.187500            1 
            2          2048           MPI          5 1362213.820312            0 
            2          2048           MPI          6 1365887.781250            0 
            2          2048           MPI          7 1365902.363281            0 
            2          2048           MPI          8 1367426.841797            1 
            2          2048           MPI          9 1386770.046875            0 
# Runtime: 16.516295 s (overhead: 0.000024 %) 10 records
