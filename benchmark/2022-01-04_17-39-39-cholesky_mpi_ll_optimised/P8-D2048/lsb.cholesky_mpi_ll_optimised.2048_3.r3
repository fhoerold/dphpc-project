# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:41:32 2022
# Execution time and date (local): Tue Jan  4 17:41:32 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 1366749.078125            0 
            3          2048           MPI          1 1363454.873047            1 
            3          2048           MPI          2 1379429.791016            0 
            3          2048           MPI          3 1365043.960938            0 
            3          2048           MPI          4 1393830.957031            1 
            3          2048           MPI          5 1362449.462891            0 
            3          2048           MPI          6 1366124.996094            0 
            3          2048           MPI          7 1366140.281250            0 
            3          2048           MPI          8 1367664.195312            1 
            3          2048           MPI          9 1387012.150391            0 
# Runtime: 16.520515 s (overhead: 0.000018 %) 10 records
