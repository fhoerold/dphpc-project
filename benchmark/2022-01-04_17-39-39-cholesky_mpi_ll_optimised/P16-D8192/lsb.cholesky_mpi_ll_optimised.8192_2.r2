# Sysname : Linux
# Nodename: eu-a6-008-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 18:44:11 2022
# Execution time and date (local): Tue Jan  4 19:44:11 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 117495268.199219            3 
            2          8192           MPI          1 115919273.558594            8 
            2          8192           MPI          2 117224351.351562            3 
            2          8192           MPI          3 116443397.919922            0 
            2          8192           MPI          4 116958744.781250            3 
            2          8192           MPI          5 103562998.960938            0 
            2          8192           MPI          6 81948744.072266            0 
            2          8192           MPI          7 81927260.695312            0 
            2          8192           MPI          8 81987699.025391            5 
            2          8192           MPI          9 81926216.970703            2 
# Runtime: 1252.753353 s (overhead: 0.000002 %) 10 records
