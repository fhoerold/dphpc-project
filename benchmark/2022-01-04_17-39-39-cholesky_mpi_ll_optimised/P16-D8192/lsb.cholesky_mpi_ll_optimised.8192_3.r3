# Sysname : Linux
# Nodename: eu-a6-008-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 18:44:11 2022
# Execution time and date (local): Tue Jan  4 19:44:11 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 117502110.414062            3 
            3          8192           MPI          1 115926023.917969            6 
            3          8192           MPI          2 117231167.705078            3 
            3          8192           MPI          3 116450208.060547            0 
            3          8192           MPI          4 116965561.746094            4 
            3          8192           MPI          5 103569035.527344            0 
            3          8192           MPI          6 81953538.257812            0 
            3          8192           MPI          7 81932055.974609            0 
            3          8192           MPI          8 81992499.082031            3 
            3          8192           MPI          9 81931011.923828            0 
# Runtime: 1249.811046 s (overhead: 0.000002 %) 10 records
