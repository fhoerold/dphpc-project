# Sysname : Linux
# Nodename: eu-a6-010-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:19:06 2022
# Execution time and date (local): Tue Jan  4 22:19:06 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 1757913.462891            3 
            3          2048           MPI          1 1753089.017578            5 
            3          2048           MPI          2 1749543.029297            0 
            3          2048           MPI          3 1756894.914062            0 
            3          2048           MPI          4 1762113.853516            1 
            3          2048           MPI          5 1772905.548828            0 
            3          2048           MPI          6 1768942.990234            0 
            3          2048           MPI          7 1777526.033203            0 
            3          2048           MPI          8 1768414.316406            1 
            3          2048           MPI          9 1767020.371094            0 
# Runtime: 25.227794 s (overhead: 0.000040 %) 10 records
