# Sysname : Linux
# Nodename: eu-a6-010-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:19:06 2022
# Execution time and date (local): Tue Jan  4 22:19:06 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 1758017.511719            0 
            2          2048           MPI          1 1753213.011719            4 
            2          2048           MPI          2 1749665.039062            1 
            2          2048           MPI          3 1757016.414062            0 
            2          2048           MPI          4 1762235.769531            1 
            2          2048           MPI          5 1773028.761719            0 
            2          2048           MPI          6 1769058.349609            0 
            2          2048           MPI          7 1777649.406250            0 
            2          2048           MPI          8 1768536.925781            1 
            2          2048           MPI          9 1767142.119141            0 
# Runtime: 23.241801 s (overhead: 0.000030 %) 10 records
