# Sysname : Linux
# Nodename: eu-a6-010-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:28:43 2022
# Execution time and date (local): Tue Jan  4 22:28:43 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 8713771.511719            3 
            3          4096           MPI          1 8671716.140625            5 
            3          4096           MPI          2 8684400.410156            5 
            3          4096           MPI          3 8706410.720703            0 
            3          4096           MPI          4 8721095.330078            5 
            3          4096           MPI          5 8769730.275391            0 
            3          4096           MPI          6 8701283.333984            2 
            3          4096           MPI          7 8703877.343750            0 
            3          4096           MPI          8 8686899.503906            9 
            3          4096           MPI          9 8683721.082031            0 
# Runtime: 111.725966 s (overhead: 0.000026 %) 10 records
