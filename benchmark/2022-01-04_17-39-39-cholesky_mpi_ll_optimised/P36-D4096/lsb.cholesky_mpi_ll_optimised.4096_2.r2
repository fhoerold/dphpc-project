# Sysname : Linux
# Nodename: eu-a6-010-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:28:43 2022
# Execution time and date (local): Tue Jan  4 22:28:43 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 8714022.021484            0 
            2          4096           MPI          1 8671549.302734            8 
            2          4096           MPI          2 8684241.628906            2 
            2          4096           MPI          3 8706250.779297            0 
            2          4096           MPI          4 8720934.472656            2 
            2          4096           MPI          5 8769568.289062            0 
            2          4096           MPI          6 8701118.240234            0 
            2          4096           MPI          7 8703723.060547            0 
            2          4096           MPI          8 8686742.791016            8 
            2          4096           MPI          9 8683559.839844            0 
# Runtime: 111.726995 s (overhead: 0.000018 %) 10 records
