# Sysname : Linux
# Nodename: eu-a6-012-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 18:14:21 2022
# Execution time and date (local): Tue Jan  4 19:14:21 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 111669629.728516            1 
            2          8192           MPI          1 111390787.494141            5 
            2          8192           MPI          2 111399521.750000            3 
            2          8192           MPI          3 111472287.876953            0 
            2          8192           MPI          4 111267743.662109            3 
            2          8192           MPI          5 111450141.630859            0 
            2          8192           MPI          6 111294231.302734            0 
            2          8192           MPI          7 111278875.060547            0 
            2          8192           MPI          8 111377362.429688            4 
            2          8192           MPI          9 111290299.175781            0 
# Runtime: 1341.627799 s (overhead: 0.000001 %) 10 records
