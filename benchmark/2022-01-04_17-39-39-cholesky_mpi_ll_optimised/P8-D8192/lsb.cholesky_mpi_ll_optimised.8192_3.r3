# Sysname : Linux
# Nodename: eu-a6-012-07
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 18:14:21 2022
# Execution time and date (local): Tue Jan  4 19:14:21 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 111656111.781250            0 
            3          8192           MPI          1 111377307.376953            4 
            3          8192           MPI          2 111386039.140625            4 
            3          8192           MPI          3 111458796.486328            1 
            3          8192           MPI          4 111254282.316406            3 
            3          8192           MPI          5 111436654.710938            0 
            3          8192           MPI          6 111280765.339844            0 
            3          8192           MPI          7 111265409.466797            0 
            3          8192           MPI          8 111363884.927734            4 
            3          8192           MPI          9 111276830.896484            0 
# Runtime: 1343.462634 s (overhead: 0.000001 %) 10 records
