# Sysname : Linux
# Nodename: eu-a6-007-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:41:00 2022
# Execution time and date (local): Tue Jan  4 17:41:00 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            64           MPI          0 447.457031            1 
            3            64           MPI          1 183.375000            2 
            3            64           MPI          2 156.078125            0 
            3            64           MPI          3 148.056641            0 
            3            64           MPI          4 158.708984            0 
            3            64           MPI          5 143.203125            0 
            3            64           MPI          6 152.943359            0 
            3            64           MPI          7 143.117188            0 
            3            64           MPI          8 169.853516            0 
            3            64           MPI          9 140.464844            0 
# Runtime: 0.990048 s (overhead: 0.000303 %) 10 records
