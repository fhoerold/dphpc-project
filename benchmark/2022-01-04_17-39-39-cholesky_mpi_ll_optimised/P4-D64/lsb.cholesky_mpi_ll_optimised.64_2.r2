# Sysname : Linux
# Nodename: eu-a6-007-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:41:00 2022
# Execution time and date (local): Tue Jan  4 17:41:00 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            64           MPI          0 181.599609            1 
            2            64           MPI          1 496.556641            2 
            2            64           MPI          2 155.892578            0 
            2            64           MPI          3 148.025391            0 
            2            64           MPI          4 158.587891            0 
            2            64           MPI          5 142.732422            0 
            2            64           MPI          6 152.791016            0 
            2            64           MPI          7 142.787109            0 
            2            64           MPI          8 169.625000            0 
            2            64           MPI          9 140.267578            0 
# Runtime: 0.996766 s (overhead: 0.000301 %) 10 records
