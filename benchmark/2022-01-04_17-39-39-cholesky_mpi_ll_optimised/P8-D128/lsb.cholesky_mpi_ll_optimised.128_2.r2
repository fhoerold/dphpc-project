# Sysname : Linux
# Nodename: eu-a6-001-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:40:28 2022
# Execution time and date (local): Tue Jan  4 17:40:28 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 748.677734            0 
            2           128           MPI          1 745.634766            4 
            2           128           MPI          2 734.220703            0 
            2           128           MPI          3 719.365234            0 
            2           128           MPI          4 731.810547            0 
            2           128           MPI          5 726.998047            0 
            2           128           MPI          6 727.625000            0 
            2           128           MPI          7 715.283203            0 
            2           128           MPI          8 770.578125            0 
            2           128           MPI          9 725.476562            0 
# Runtime: 23.989424 s (overhead: 0.000017 %) 10 records
