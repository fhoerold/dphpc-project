# Sysname : Linux
# Nodename: eu-a6-001-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:40:28 2022
# Execution time and date (local): Tue Jan  4 17:40:28 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 769.302734            0 
            3           128           MPI          1 742.412109            0 
            3           128           MPI          2 735.378906            0 
            3           128           MPI          3 720.470703            0 
            3           128           MPI          4 733.070312            0 
            3           128           MPI          5 719.794922            0 
            3           128           MPI          6 727.958984            0 
            3           128           MPI          7 720.046875            0 
            3           128           MPI          8 775.658203            0 
            3           128           MPI          9 726.728516            0 
# Runtime: 0.010878 s (overhead: 0.000000 %) 10 records
