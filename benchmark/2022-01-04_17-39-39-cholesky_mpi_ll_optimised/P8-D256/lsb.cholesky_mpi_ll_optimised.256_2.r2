# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:40:28 2022
# Execution time and date (local): Tue Jan  4 17:40:28 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 2574.820312            0 
            2           256           MPI          1 2617.750000            2 
            2           256           MPI          2 2575.869141            0 
            2           256           MPI          3 2559.837891            0 
            2           256           MPI          4 2573.373047            0 
            2           256           MPI          5 2564.341797            0 
            2           256           MPI          6 2559.455078            0 
            2           256           MPI          7 2604.156250            0 
            2           256           MPI          8 2537.466797            0 
            2           256           MPI          9 2569.585938            0 
# Runtime: 8.025976 s (overhead: 0.000025 %) 10 records
