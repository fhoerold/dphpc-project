# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:40:28 2022
# Execution time and date (local): Tue Jan  4 17:40:28 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 2586.117188            0 
            3           256           MPI          1 2615.083984            2 
            3           256           MPI          2 2582.003906            0 
            3           256           MPI          3 2566.007812            0 
            3           256           MPI          4 2579.517578            0 
            3           256           MPI          5 2570.531250            0 
            3           256           MPI          6 2565.621094            0 
            3           256           MPI          7 2610.347656            0 
            3           256           MPI          8 2551.542969            0 
            3           256           MPI          9 2579.667969            0 
# Runtime: 4.053413 s (overhead: 0.000049 %) 10 records
