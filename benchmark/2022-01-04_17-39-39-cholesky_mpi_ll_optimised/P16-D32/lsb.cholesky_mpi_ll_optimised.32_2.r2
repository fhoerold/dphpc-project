# Sysname : Linux
# Nodename: eu-a6-001-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:43:29 2022
# Execution time and date (local): Tue Jan  4 17:43:29 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 555.333984            0 
            2            32           MPI          1 141.037109            3 
            2            32           MPI          2 137.800781            0 
            2            32           MPI          3 153.740234            0 
            2            32           MPI          4 147.150391            0 
            2            32           MPI          5 160.750000            0 
            2            32           MPI          6 147.111328            0 
            2            32           MPI          7 152.515625            0 
            2            32           MPI          8 150.228516            1 
            2            32           MPI          9 150.066406            0 
# Runtime: 5.017035 s (overhead: 0.000080 %) 10 records
