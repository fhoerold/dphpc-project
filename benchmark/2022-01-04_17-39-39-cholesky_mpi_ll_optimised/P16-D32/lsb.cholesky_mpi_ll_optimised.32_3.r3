# Sysname : Linux
# Nodename: eu-a6-001-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:43:29 2022
# Execution time and date (local): Tue Jan  4 17:43:29 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 558.435547            0 
            3            32           MPI          1 476.529297            3 
            3            32           MPI          2 139.863281            0 
            3            32           MPI          3 157.421875            0 
            3            32           MPI          4 148.968750            0 
            3            32           MPI          5 164.203125            0 
            3            32           MPI          6 148.824219            0 
            3            32           MPI          7 156.423828            0 
            3            32           MPI          8 152.099609            0 
            3            32           MPI          9 151.189453            0 
# Runtime: 5.005472 s (overhead: 0.000060 %) 10 records
