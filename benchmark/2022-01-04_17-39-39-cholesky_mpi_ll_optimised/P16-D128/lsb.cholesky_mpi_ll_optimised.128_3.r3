# Sysname : Linux
# Nodename: eu-a6-001-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:43:58 2022
# Execution time and date (local): Tue Jan  4 17:43:58 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 1855.265625            0 
            3           128           MPI          1 1827.839844            2 
            3           128           MPI          2 1541.394531            0 
            3           128           MPI          3 1470.476562            0 
            3           128           MPI          4 1481.507812            0 
            3           128           MPI          5 1482.248047            0 
            3           128           MPI          6 1531.207031            0 
            3           128           MPI          7 1473.628906            0 
            3           128           MPI          8 1489.927734            2 
            3           128           MPI          9 1475.527344            0 
# Runtime: 1.028758 s (overhead: 0.000389 %) 10 records
