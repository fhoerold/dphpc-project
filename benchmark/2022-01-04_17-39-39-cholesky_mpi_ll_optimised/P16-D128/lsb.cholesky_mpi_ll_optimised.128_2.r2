# Sysname : Linux
# Nodename: eu-a6-001-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:43:58 2022
# Execution time and date (local): Tue Jan  4 17:43:58 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 2196.839844            3 
            2           128           MPI          1 1826.562500            4 
            2           128           MPI          2 1847.332031            1 
            2           128           MPI          3 1469.437500            0 
            2           128           MPI          4 1480.453125            0 
            2           128           MPI          5 1481.154297            0 
            2           128           MPI          6 1530.158203            0 
            2           128           MPI          7 1472.707031            0 
            2           128           MPI          8 1491.513672            0 
            2           128           MPI          9 1474.365234            0 
# Runtime: 3.017859 s (overhead: 0.000265 %) 10 records
