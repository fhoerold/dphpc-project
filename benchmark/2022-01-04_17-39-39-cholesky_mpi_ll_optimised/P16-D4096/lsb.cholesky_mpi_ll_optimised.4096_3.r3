# Sysname : Linux
# Nodename: eu-a6-008-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:55:18 2022
# Execution time and date (local): Tue Jan  4 17:55:18 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 8066335.197266            0 
            3          4096           MPI          1 8027354.742188            5 
            3          4096           MPI          2 8012792.623047            2 
            3          4096           MPI          3 8018484.240234            0 
            3          4096           MPI          4 8019694.326172            1 
            3          4096           MPI          5 8017195.613281            0 
            3          4096           MPI          6 8054598.429688            0 
            3          4096           MPI          7 8046389.267578            0 
            3          4096           MPI          8 8027899.894531            7 
            3          4096           MPI          9 8023404.509766            0 
# Runtime: 106.600220 s (overhead: 0.000014 %) 10 records
