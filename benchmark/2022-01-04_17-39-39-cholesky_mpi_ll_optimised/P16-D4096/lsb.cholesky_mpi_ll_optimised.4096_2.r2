# Sysname : Linux
# Nodename: eu-a6-008-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:55:18 2022
# Execution time and date (local): Tue Jan  4 17:55:18 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 8063197.722656            0 
            2          4096           MPI          1 8024230.726562            6 
            2          4096           MPI          2 8009676.603516            2 
            2          4096           MPI          3 8015363.076172            0 
            2          4096           MPI          4 8016573.466797            2 
            2          4096           MPI          5 8014075.203125            0 
            2          4096           MPI          6 8051466.640625            0 
            2          4096           MPI          7 8043257.234375            0 
            2          4096           MPI          8 8024782.261719            6 
            2          4096           MPI          9 8020289.144531            0 
# Runtime: 105.550417 s (overhead: 0.000015 %) 10 records
