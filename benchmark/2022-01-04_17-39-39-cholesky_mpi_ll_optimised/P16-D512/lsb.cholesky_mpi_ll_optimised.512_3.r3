# Sysname : Linux
# Nodename: eu-a6-008-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:45:01 2022
# Execution time and date (local): Tue Jan  4 17:45:01 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 27768.025391            0 
            3           512           MPI          1 27573.394531            6 
            3           512           MPI          2 26607.707031            0 
            3           512           MPI          3 26999.785156            0 
            3           512           MPI          4 27081.445312            0 
            3           512           MPI          5 27314.419922            0 
            3           512           MPI          6 26525.460938            0 
            3           512           MPI          7 27266.712891            0 
            3           512           MPI          8 27118.335938            0 
            3           512           MPI          9 26555.828125            0 
# Runtime: 9.322242 s (overhead: 0.000064 %) 10 records
