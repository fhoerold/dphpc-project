# Sysname : Linux
# Nodename: eu-a6-008-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:45:01 2022
# Execution time and date (local): Tue Jan  4 17:45:01 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 27749.632812            2 
            2           512           MPI          1 27560.144531            3 
            2           512           MPI          2 26604.615234            0 
            2           512           MPI          3 26996.341797            0 
            2           512           MPI          4 27078.320312            0 
            2           512           MPI          5 27311.632812            0 
            2           512           MPI          6 26522.525391            0 
            2           512           MPI          7 27263.734375            0 
            2           512           MPI          8 27111.535156            0 
            2           512           MPI          9 26553.242188            0 
# Runtime: 7.331251 s (overhead: 0.000068 %) 10 records
