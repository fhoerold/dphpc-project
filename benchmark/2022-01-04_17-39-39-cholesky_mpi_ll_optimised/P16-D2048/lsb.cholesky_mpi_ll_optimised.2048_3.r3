# Sysname : Linux
# Nodename: eu-a6-008-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:46:06 2022
# Execution time and date (local): Tue Jan  4 17:46:06 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 1006430.941406            0 
            3          2048           MPI          1 1008893.900391            1 
            3          2048           MPI          2 1006471.433594            1 
            3          2048           MPI          3 1004558.332031            0 
            3          2048           MPI          4 1009574.302734            1 
            3          2048           MPI          5 1009502.576172            0 
            3          2048           MPI          6 1016081.078125            0 
            3          2048           MPI          7 1011513.314453            0 
            3          2048           MPI          8 1013437.310547            2 
            3          2048           MPI          9 1011717.511719            0 
# Runtime: 18.164057 s (overhead: 0.000028 %) 10 records
