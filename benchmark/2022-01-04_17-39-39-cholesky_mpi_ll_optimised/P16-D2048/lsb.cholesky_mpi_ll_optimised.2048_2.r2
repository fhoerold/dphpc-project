# Sysname : Linux
# Nodename: eu-a6-008-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:46:07 2022
# Execution time and date (local): Tue Jan  4 17:46:07 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 1006438.632812            0 
            2          2048           MPI          1 1008898.152344            1 
            2          2048           MPI          2 1006477.855469            2 
            2          2048           MPI          3 1004561.935547            0 
            2          2048           MPI          4 1009579.046875            1 
            2          2048           MPI          5 1009506.140625            0 
            2          2048           MPI          6 1016085.111328            0 
            2          2048           MPI          7 1011515.828125            0 
            2          2048           MPI          8 1013453.177734            2 
            2          2048           MPI          9 1011722.871094            0 
# Runtime: 18.127655 s (overhead: 0.000033 %) 10 records
