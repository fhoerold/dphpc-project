# Sysname : Linux
# Nodename: eu-a6-007-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:41:00 2022
# Execution time and date (local): Tue Jan  4 17:41:00 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 21884.650391            0 
            3           512           MPI          1 21853.794922            0 
            3           512           MPI          2 21860.750000            0 
            3           512           MPI          3 21887.701172            0 
            3           512           MPI          4 21894.091797            0 
            3           512           MPI          5 21881.707031            0 
            3           512           MPI          6 21853.804688            0 
            3           512           MPI          7 21840.728516            0 
            3           512           MPI          8 21850.783203            0 
            3           512           MPI          9 21841.447266            0 
# Runtime: 0.264898 s (overhead: 0.000000 %) 10 records
