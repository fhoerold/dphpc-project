# Sysname : Linux
# Nodename: eu-a6-007-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:41:00 2022
# Execution time and date (local): Tue Jan  4 17:41:00 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 22184.402344            0 
            2           512           MPI          1 21849.867188            2 
            2           512           MPI          2 21856.583984            0 
            2           512           MPI          3 21883.552734            0 
            2           512           MPI          4 21889.779297            0 
            2           512           MPI          5 21875.787109            0 
            2           512           MPI          6 21849.695312            0 
            2           512           MPI          7 21836.650391            0 
            2           512           MPI          8 21846.812500            0 
            2           512           MPI          9 21837.285156            0 
# Runtime: 2.265027 s (overhead: 0.000088 %) 10 records
