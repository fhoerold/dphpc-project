# Sysname : Linux
# Nodename: eu-a6-001-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:51:29 2022
# Execution time and date (local): Tue Jan  4 17:51:29 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 17583642.492188            0 
            2          4096           MPI          1 17606307.794922            4 
            2          4096           MPI          2 17712703.923828            1 
            2          4096           MPI          3 17836611.810547            0 
            2          4096           MPI          4 17733525.435547            3 
            2          4096           MPI          5 18139964.472656            0 
            2          4096           MPI          6 17648769.078125            0 
            2          4096           MPI          7 17886783.955078            0 
            2          4096           MPI          8 17712442.814453            6 
            2          4096           MPI          9 17574606.892578            0 
# Runtime: 215.908154 s (overhead: 0.000006 %) 10 records
