# Sysname : Linux
# Nodename: eu-a6-001-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:51:29 2022
# Execution time and date (local): Tue Jan  4 17:51:29 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 17591162.306641            0 
            3          4096           MPI          1 17613841.263672            3 
            3          4096           MPI          2 17720280.511719            1 
            3          4096           MPI          3 17844238.648438            0 
            3          4096           MPI          4 17741115.656250            3 
            3          4096           MPI          5 18147727.503906            0 
            3          4096           MPI          6 17656315.582031            0 
            3          4096           MPI          7 17894436.000000            0 
            3          4096           MPI          8 17720022.099609            5 
            3          4096           MPI          9 17582121.191406            0 
# Runtime: 217.007929 s (overhead: 0.000006 %) 10 records
