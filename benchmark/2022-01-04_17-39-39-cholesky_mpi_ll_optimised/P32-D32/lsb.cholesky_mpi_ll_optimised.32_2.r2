# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 19:05:49 2022
# Execution time and date (local): Tue Jan  4 20:05:49 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 15.578125            2 
            2            32           MPI          1 0.642578            6 
            2            32           MPI          2 0.279297            2 
            2            32           MPI          3 0.238281            0 
            2            32           MPI          4 0.251953            0 
            2            32           MPI          5 0.275391            0 
            2            32           MPI          6 0.261719            0 
            2            32           MPI          7 0.242188            0 
            2            32           MPI          8 0.464844            4 
            2            32           MPI          9 0.513672            0 
# Runtime: 9.003016 s (overhead: 0.000156 %) 10 records
