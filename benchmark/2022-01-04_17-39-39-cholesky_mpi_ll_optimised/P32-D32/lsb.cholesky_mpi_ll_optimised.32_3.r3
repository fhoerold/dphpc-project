# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 19:05:49 2022
# Execution time and date (local): Tue Jan  4 20:05:49 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 4.236328            0 
            3            32           MPI          1 0.298828            0 
            3            32           MPI          2 0.234375            0 
            3            32           MPI          3 0.568359            0 
            3            32           MPI          4 0.285156            0 
            3            32           MPI          5 0.228516            0 
            3            32           MPI          6 0.824219            0 
            3            32           MPI          7 0.488281            0 
            3            32           MPI          8 0.468750            0 
            3            32           MPI          9 0.636719            0 
# Runtime: 0.001705 s (overhead: 0.000000 %) 10 records
