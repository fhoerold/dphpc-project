# Sysname : Linux
# Nodename: eu-a6-001-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:44:11 2022
# Execution time and date (local): Tue Jan  4 17:44:11 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 4924.931641            0 
            2           256           MPI          1 4511.046875            2 
            2           256           MPI          2 4478.410156            2 
            2           256           MPI          3 4448.521484            0 
            2           256           MPI          4 4466.457031            0 
            2           256           MPI          5 4457.142578            0 
            2           256           MPI          6 4505.552734            0 
            2           256           MPI          7 4443.763672            0 
            2           256           MPI          8 4480.689453            1 
            2           256           MPI          9 4494.369141            0 
# Runtime: 6.059975 s (overhead: 0.000083 %) 10 records
