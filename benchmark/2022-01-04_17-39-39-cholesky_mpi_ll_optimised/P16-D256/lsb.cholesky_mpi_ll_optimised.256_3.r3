# Sysname : Linux
# Nodename: eu-a6-001-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:44:11 2022
# Execution time and date (local): Tue Jan  4 17:44:11 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 4582.394531            0 
            3           256           MPI          1 4511.498047            4 
            3           256           MPI          2 4477.189453            0 
            3           256           MPI          3 4448.933594            0 
            3           256           MPI          4 4471.005859            0 
            3           256           MPI          5 4457.472656            0 
            3           256           MPI          6 4505.984375            0 
            3           256           MPI          7 4707.742188            0 
            3           256           MPI          8 4481.003906            1 
            3           256           MPI          9 4489.994141            0 
# Runtime: 1.052598 s (overhead: 0.000475 %) 10 records
