# Sysname : Linux
# Nodename: eu-a6-007-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:41:00 2022
# Execution time and date (local): Tue Jan  4 17:41:00 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 452.378906            0 
            3           128           MPI          1 455.142578            0 
            3           128           MPI          2 457.654297            0 
            3           128           MPI          3 441.966797            0 
            3           128           MPI          4 434.044922            0 
            3           128           MPI          5 433.988281            0 
            3           128           MPI          6 454.501953            0 
            3           128           MPI          7 441.890625            0 
            3           128           MPI          8 446.779297            0 
            3           128           MPI          9 431.101562            0 
# Runtime: 0.006798 s (overhead: 0.000000 %) 10 records
