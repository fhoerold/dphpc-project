# Sysname : Linux
# Nodename: eu-a6-007-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:41:00 2022
# Execution time and date (local): Tue Jan  4 17:41:00 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 463.634766            1 
            2           128           MPI          1 454.806641            2 
            2           128           MPI          2 782.826172            0 
            2           128           MPI          3 441.708984            0 
            2           128           MPI          4 433.634766            0 
            2           128           MPI          5 433.623047            0 
            2           128           MPI          6 454.169922            0 
            2           128           MPI          7 441.683594            0 
            2           128           MPI          8 446.414062            0 
            2           128           MPI          9 430.697266            0 
# Runtime: 6.025263 s (overhead: 0.000050 %) 10 records
