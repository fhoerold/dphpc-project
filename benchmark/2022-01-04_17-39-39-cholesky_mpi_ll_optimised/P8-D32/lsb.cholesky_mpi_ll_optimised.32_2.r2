# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:39:59 2022
# Execution time and date (local): Tue Jan  4 17:39:59 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 468.171875            2 
            2            32           MPI          1 475.255859            3 
            2            32           MPI          2 125.541016            0 
            2            32           MPI          3 99.417969            0 
            2            32           MPI          4 105.509766            0 
            2            32           MPI          5 97.902344            0 
            2            32           MPI          6 98.228516            0 
            2            32           MPI          7 97.273438            0 
            2            32           MPI          8 97.384766            0 
            2            32           MPI          9 98.541016            0 
# Runtime: 5.998278 s (overhead: 0.000083 %) 10 records
