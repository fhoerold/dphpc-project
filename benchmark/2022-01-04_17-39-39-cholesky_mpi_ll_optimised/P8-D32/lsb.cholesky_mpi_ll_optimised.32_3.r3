# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:39:59 2022
# Execution time and date (local): Tue Jan  4 17:39:59 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 465.837891            0 
            3            32           MPI          1 474.820312            3 
            3            32           MPI          2 134.681641            0 
            3            32           MPI          3 99.822266            0 
            3            32           MPI          4 105.839844            0 
            3            32           MPI          5 98.228516            0 
            3            32           MPI          6 98.621094            0 
            3            32           MPI          7 97.492188            0 
            3            32           MPI          8 97.757812            0 
            3            32           MPI          9 99.005859            0 
# Runtime: 7.002993 s (overhead: 0.000043 %) 10 records
