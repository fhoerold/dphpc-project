# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 19:06:10 2022
# Execution time and date (local): Tue Jan  4 20:06:10 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            64           MPI          0 1437.285156            0 
            2            64           MPI          1 1642.556641            3 
            2            64           MPI          2 1388.818359            2 
            2            64           MPI          3 1379.433594            0 
            2            64           MPI          4 1363.023438            0 
            2            64           MPI          5 1386.644531            0 
            2            64           MPI          6 1349.718750            0 
            2            64           MPI          7 1371.642578            0 
            2            64           MPI          8 1401.724609            0 
            2            64           MPI          9 1399.453125            0 
# Runtime: 4.013897 s (overhead: 0.000125 %) 10 records
