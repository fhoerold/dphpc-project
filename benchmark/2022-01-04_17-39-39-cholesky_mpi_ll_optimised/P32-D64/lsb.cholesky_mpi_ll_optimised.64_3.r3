# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 19:06:10 2022
# Execution time and date (local): Tue Jan  4 20:06:10 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            64           MPI          0 1883.410156            1 
            3            64           MPI          1 1643.183594            3 
            3            64           MPI          2 1389.705078            2 
            3            64           MPI          3 1376.816406            0 
            3            64           MPI          4 1367.552734            2 
            3            64           MPI          5 1387.007812            0 
            3            64           MPI          6 1352.240234            0 
            3            64           MPI          7 1372.033203            0 
            3            64           MPI          8 1397.083984            0 
            3            64           MPI          9 1403.689453            0 
# Runtime: 16.055911 s (overhead: 0.000050 %) 10 records
