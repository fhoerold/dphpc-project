# Sysname : Linux
# Nodename: eu-a6-001-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:43:41 2022
# Execution time and date (local): Tue Jan  4 17:43:41 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            64           MPI          0 532.021484            0 
            3            64           MPI          1 555.585938            2 
            3            64           MPI          2 843.361328            0 
            3            64           MPI          3 510.414062            0 
            3            64           MPI          4 508.691406            0 
            3            64           MPI          5 512.949219            0 
            3            64           MPI          6 508.111328            0 
            3            64           MPI          7 510.632812            0 
            3            64           MPI          8 508.539062            0 
            3            64           MPI          9 523.035156            0 
# Runtime: 11.021067 s (overhead: 0.000018 %) 10 records
