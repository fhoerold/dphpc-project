# Sysname : Linux
# Nodename: eu-a6-001-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:43:41 2022
# Execution time and date (local): Tue Jan  4 17:43:41 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            64           MPI          0 917.898438            0 
            2            64           MPI          1 566.162109            3 
            2            64           MPI          2 842.824219            0 
            2            64           MPI          3 510.027344            0 
            2            64           MPI          4 508.220703            0 
            2            64           MPI          5 512.500000            0 
            2            64           MPI          6 507.632812            0 
            2            64           MPI          7 510.091797            0 
            2            64           MPI          8 510.382812            2 
            2            64           MPI          9 524.687500            0 
# Runtime: 11.018027 s (overhead: 0.000045 %) 10 records
