# Sysname : Linux
# Nodename: eu-a6-007-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:48:39 2022
# Execution time and date (local): Tue Jan  4 17:48:39 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 23867351.216797            0 
            3          4096           MPI          1 23765962.583984            5 
            3          4096           MPI          2 23383239.464844            3 
            3          4096           MPI          3 22809107.634766            0 
            3          4096           MPI          4 22829186.896484            5 
            3          4096           MPI          5 23795797.716797            0 
            3          4096           MPI          6 23558678.164062            0 
            3          4096           MPI          7 23450242.500000            0 
            3          4096           MPI          8 23244461.150391            4 
            3          4096           MPI          9 23117278.054688            0 
# Runtime: 290.124289 s (overhead: 0.000006 %) 10 records
