# Sysname : Linux
# Nodename: eu-a6-007-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:48:39 2022
# Execution time and date (local): Tue Jan  4 17:48:39 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 23873522.609375            2 
            2          4096           MPI          1 23772128.410156            5 
            2          4096           MPI          2 23389278.880859            3 
            2          4096           MPI          3 22814998.578125            0 
            2          4096           MPI          4 22835079.085938            1 
            2          4096           MPI          5 23801950.515625            0 
            2          4096           MPI          6 23564763.238281            0 
            2          4096           MPI          7 23456271.294922            0 
            2          4096           MPI          8 23250466.042969            5 
            2          4096           MPI          9 23123245.394531            0 
# Runtime: 284.195319 s (overhead: 0.000006 %) 10 records
