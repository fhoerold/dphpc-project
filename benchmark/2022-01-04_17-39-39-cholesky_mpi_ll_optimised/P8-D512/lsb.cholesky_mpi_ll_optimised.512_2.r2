# Sysname : Linux
# Nodename: eu-a6-012-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:40:29 2022
# Execution time and date (local): Tue Jan  4 17:40:29 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 21406.808594            0 
            2           512           MPI          1 21422.189453            1 
            2           512           MPI          2 21997.974609            1 
            2           512           MPI          3 22161.451172            0 
            2           512           MPI          4 21674.945312            1 
            2           512           MPI          5 21129.189453            0 
            2           512           MPI          6 21613.078125            0 
            2           512           MPI          7 21397.765625            0 
            2           512           MPI          8 21088.642578            1 
            2           512           MPI          9 20294.109375            0 
# Runtime: 0.262098 s (overhead: 0.001526 %) 10 records
