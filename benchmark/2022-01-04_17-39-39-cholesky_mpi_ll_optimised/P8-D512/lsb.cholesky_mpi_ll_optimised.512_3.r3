# Sysname : Linux
# Nodename: eu-a6-012-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:40:29 2022
# Execution time and date (local): Tue Jan  4 17:40:29 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 21407.566406            0 
            3           512           MPI          1 21423.203125            1 
            3           512           MPI          2 22002.794922            0 
            3           512           MPI          3 22165.117188            0 
            3           512           MPI          4 21678.814453            0 
            3           512           MPI          5 21132.974609            0 
            3           512           MPI          6 21617.988281            0 
            3           512           MPI          7 21401.125000            0 
            3           512           MPI          8 21092.406250            1 
            3           512           MPI          9 20298.648438            0 
# Runtime: 0.261040 s (overhead: 0.000766 %) 10 records
