# Sysname : Linux
# Nodename: eu-a6-010-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:31:26 2022
# Execution time and date (local): Tue Jan  4 22:31:26 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 69452.546875            1 
            2           512           MPI          1 69044.660156            2 
            2           512           MPI          2 68822.640625            1 
            2           512           MPI          3 69664.496094            0 
            2           512           MPI          4 70607.828125            6 
            2           512           MPI          5 69115.625000            0 
            2           512           MPI          6 69499.826172            0 
            2           512           MPI          7 69852.515625            0 
            2           512           MPI          8 69113.416016            0 
            2           512           MPI          9 70922.531250            0 
# Runtime: 4.883798 s (overhead: 0.000205 %) 10 records
