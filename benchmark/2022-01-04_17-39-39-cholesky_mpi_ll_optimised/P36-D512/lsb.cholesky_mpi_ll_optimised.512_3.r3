# Sysname : Linux
# Nodename: eu-a6-010-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:31:26 2022
# Execution time and date (local): Tue Jan  4 22:31:26 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 69441.468750            3 
            3           512           MPI          1 69051.386719            2 
            3           512           MPI          2 68825.937500            0 
            3           512           MPI          3 70286.636719            0 
            3           512           MPI          4 70596.822266            2 
            3           512           MPI          5 69127.845703            0 
            3           512           MPI          6 69502.111328            0 
            3           512           MPI          7 69856.884766            0 
            3           512           MPI          8 69120.861328            5 
            3           512           MPI          9 70925.349609            0 
# Runtime: 4.880680 s (overhead: 0.000246 %) 10 records
