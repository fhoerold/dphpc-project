# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 19:08:07 2022
# Execution time and date (local): Tue Jan  4 20:08:07 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 1547299.783203            2 
            2          2048           MPI          1 1543317.201172            4 
            2          2048           MPI          2 1547329.453125            1 
            2          2048           MPI          3 1522204.587891            0 
            2          2048           MPI          4 1513225.263672            0 
            2          2048           MPI          5 1505572.945312            0 
            2          2048           MPI          6 1531176.937500            0 
            2          2048           MPI          7 1530836.591797            0 
            2          2048           MPI          8 1519415.292969            1 
            2          2048           MPI          9 1527708.406250            0 
# Runtime: 28.492782 s (overhead: 0.000028 %) 10 records
