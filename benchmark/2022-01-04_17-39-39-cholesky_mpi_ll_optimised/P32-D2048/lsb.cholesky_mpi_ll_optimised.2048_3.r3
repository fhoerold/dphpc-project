# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 19:08:07 2022
# Execution time and date (local): Tue Jan  4 20:08:07 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 1546948.035156            0 
            3          2048           MPI          1 1542964.416016            5 
            3          2048           MPI          2 1546977.554688            1 
            3          2048           MPI          3 1521857.082031            0 
            3          2048           MPI          4 1512883.556641            1 
            3          2048           MPI          5 1505236.386719            0 
            3          2048           MPI          6 1530830.451172            0 
            3          2048           MPI          7 1530495.896484            0 
            3          2048           MPI          8 1519063.363281            1 
            3          2048           MPI          9 1527364.048828            0 
# Runtime: 28.480032 s (overhead: 0.000028 %) 10 records
