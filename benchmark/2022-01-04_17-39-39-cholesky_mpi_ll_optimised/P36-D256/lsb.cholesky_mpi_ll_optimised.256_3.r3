# Sysname : Linux
# Nodename: eu-a6-010-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:18:03 2022
# Execution time and date (local): Tue Jan  4 22:18:03 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 16525.875000            0 
            3           256           MPI          1 15904.757812            5 
            3           256           MPI          2 15877.029297            0 
            3           256           MPI          3 15797.744141            0 
            3           256           MPI          4 15797.542969            2 
            3           256           MPI          5 15817.839844            0 
            3           256           MPI          6 15803.896484            0 
            3           256           MPI          7 15779.681641            0 
            3           256           MPI          8 15887.435547            2 
            3           256           MPI          9 15870.853516            0 
# Runtime: 2.226397 s (overhead: 0.000404 %) 10 records
