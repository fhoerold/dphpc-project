# Sysname : Linux
# Nodename: eu-a6-010-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:18:03 2022
# Execution time and date (local): Tue Jan  4 22:18:03 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 16277.017578            1 
            2           256           MPI          1 15900.701172            6 
            2           256           MPI          2 15874.630859            0 
            2           256           MPI          3 15796.025391            0 
            2           256           MPI          4 15795.167969            2 
            2           256           MPI          5 15815.943359            0 
            2           256           MPI          6 15802.042969            0 
            2           256           MPI          7 15777.345703            0 
            2           256           MPI          8 15885.177734            3 
            2           256           MPI          9 15868.771484            0 
# Runtime: 2.197356 s (overhead: 0.000546 %) 10 records
