# Sysname : Linux
# Nodename: eu-a6-007-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 18:09:27 2022
# Execution time and date (local): Tue Jan  4 19:09:27 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 265760697.041016            0 
            3          8192           MPI          1 265727303.097656            3 
            3          8192           MPI          2 262017616.496094            4 
            3          8192           MPI          3 251197168.029297            1 
            3          8192           MPI          4 237957464.462891            4 
            3          8192           MPI          5 250515703.722656            0 
            3          8192           MPI          6 256861375.462891            0 
            3          8192           MPI          7 260046239.027344            0 
            3          8192           MPI          8 259052104.949219            3 
            3          8192           MPI          9 259796525.412109            0 
# Runtime: 3106.039084 s (overhead: 0.000000 %) 10 records
