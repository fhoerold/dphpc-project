# Sysname : Linux
# Nodename: eu-a6-007-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 18:09:27 2022
# Execution time and date (local): Tue Jan  4 19:09:27 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 265762776.199219            0 
            2          8192           MPI          1 265729375.933594            4 
            2          8192           MPI          2 262019661.214844            5 
            2          8192           MPI          3 251199125.703125            1 
            2          8192           MPI          4 237959309.919922            5 
            2          8192           MPI          5 250517616.488281            1 
            2          8192           MPI          6 256863380.992188            1 
            2          8192           MPI          7 260048260.378906            2 
            2          8192           MPI          8 259054135.035156            5 
            2          8192           MPI          9 259798594.927734            0 
# Runtime: 3106.069073 s (overhead: 0.000001 %) 10 records
