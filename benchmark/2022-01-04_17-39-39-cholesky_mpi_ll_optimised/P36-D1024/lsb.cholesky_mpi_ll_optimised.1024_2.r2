# Sysname : Linux
# Nodename: eu-a6-010-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:31:45 2022
# Execution time and date (local): Tue Jan  4 22:31:45 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 306824.677734            3 
            2          1024           MPI          1 305725.056641            7 
            2          1024           MPI          2 309999.250000            1 
            2          1024           MPI          3 306533.964844            0 
            2          1024           MPI          4 305814.537109            2 
            2          1024           MPI          5 305822.498047            0 
            2          1024           MPI          6 305102.736328            0 
            2          1024           MPI          7 306016.480469            0 
            2          1024           MPI          8 305759.439453            2 
            2          1024           MPI          9 305275.822266            0 
# Runtime: 11.745852 s (overhead: 0.000128 %) 10 records
