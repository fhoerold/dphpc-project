# Sysname : Linux
# Nodename: eu-a6-010-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 21:31:45 2022
# Execution time and date (local): Tue Jan  4 22:31:45 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 306396.904297            4 
            3          1024           MPI          1 305741.890625            4 
            3          1024           MPI          2 310011.544922            3 
            3          1024           MPI          3 306545.341797            0 
            3          1024           MPI          4 305825.978516            3 
            3          1024           MPI          5 305836.183594            0 
            3          1024           MPI          6 305116.144531            0 
            3          1024           MPI          7 306029.689453            0 
            3          1024           MPI          8 305774.326172            0 
            3          1024           MPI          9 305290.964844            0 
# Runtime: 8.746647 s (overhead: 0.000160 %) 10 records
