# Sysname : Linux
# Nodename: eu-a6-007-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:42:17 2022
# Execution time and date (local): Tue Jan  4 17:42:17 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 2371688.882812            0 
            3          2048           MPI          1 2361232.640625            1 
            3          2048           MPI          2 2362806.783203            1 
            3          2048           MPI          3 2360476.287109            0 
            3          2048           MPI          4 2364879.164062            1 
            3          2048           MPI          5 2401101.437500            0 
            3          2048           MPI          6 2380724.775391            0 
            3          2048           MPI          7 2368416.988281            0 
            3          2048           MPI          8 2367002.191406            6 
            3          2048           MPI          9 2367875.517578            0 
# Runtime: 28.487040 s (overhead: 0.000032 %) 10 records
