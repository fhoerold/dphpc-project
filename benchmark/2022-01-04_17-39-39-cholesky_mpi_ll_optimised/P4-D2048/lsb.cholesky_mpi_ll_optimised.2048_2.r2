# Sysname : Linux
# Nodename: eu-a6-007-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:42:17 2022
# Execution time and date (local): Tue Jan  4 17:42:17 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 2371081.166016            0 
            2          2048           MPI          1 2361181.447266            2 
            2          2048           MPI          2 2362554.755859            1 
            2          2048           MPI          3 2360225.996094            0 
            2          2048           MPI          4 2364626.068359            1 
            2          2048           MPI          5 2400843.878906            0 
            2          2048           MPI          6 2380469.390625            0 
            2          2048           MPI          7 2368162.802734            0 
            2          2048           MPI          8 2366747.712891            5 
            2          2048           MPI          9 2367621.935547            0 
# Runtime: 28.483087 s (overhead: 0.000032 %) 10 records
