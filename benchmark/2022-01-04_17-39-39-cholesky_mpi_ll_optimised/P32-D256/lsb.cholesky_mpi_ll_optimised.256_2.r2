# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 19:06:35 2022
# Execution time and date (local): Tue Jan  4 20:06:35 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 15272.949219            3 
            2           256           MPI          1 15047.439453            6 
            2           256           MPI          2 14738.648438            0 
            2           256           MPI          3 14657.972656            0 
            2           256           MPI          4 14723.597656            3 
            2           256           MPI          5 14701.705078            0 
            2           256           MPI          6 14627.029297            0 
            2           256           MPI          7 14560.662109            0 
            2           256           MPI          8 14666.607422            3 
            2           256           MPI          9 15233.742188            0 
# Runtime: 6.152019 s (overhead: 0.000244 %) 10 records
