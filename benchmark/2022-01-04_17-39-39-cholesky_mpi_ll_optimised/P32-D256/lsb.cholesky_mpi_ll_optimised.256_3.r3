# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 19:06:35 2022
# Execution time and date (local): Tue Jan  4 20:06:35 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 15263.244141            3 
            3           256           MPI          1 15114.005859            8 
            3           256           MPI          2 14739.287109            0 
            3           256           MPI          3 14659.019531            0 
            3           256           MPI          4 14718.003906            0 
            3           256           MPI          5 14701.435547            0 
            3           256           MPI          6 14630.556641            0 
            3           256           MPI          7 14561.193359            0 
            3           256           MPI          8 14667.685547            4 
            3           256           MPI          9 15230.701172            0 
# Runtime: 1.123369 s (overhead: 0.001335 %) 10 records
