# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:40:13 2022
# Execution time and date (local): Tue Jan  4 17:40:13 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            64           MPI          0 273.480469            0 
            3            64           MPI          1 579.335938            3 
            3            64           MPI          2 581.652344            0 
            3            64           MPI          3 590.158203            0 
            3            64           MPI          4 267.519531            0 
            3            64           MPI          5 256.330078            0 
            3            64           MPI          6 270.826172            0 
            3            64           MPI          7 258.857422            0 
            3            64           MPI          8 260.080078            0 
            3            64           MPI          9 263.607422            0 
# Runtime: 13.987504 s (overhead: 0.000021 %) 10 records
