# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:40:13 2022
# Execution time and date (local): Tue Jan  4 17:40:13 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            64           MPI          0 646.658203            0 
            2            64           MPI          1 577.724609            3 
            2            64           MPI          2 290.636719            0 
            2            64           MPI          3 263.597656            0 
            2            64           MPI          4 268.912109            0 
            2            64           MPI          5 256.324219            0 
            2            64           MPI          6 270.986328            0 
            2            64           MPI          7 258.878906            0 
            2            64           MPI          8 260.281250            0 
            2            64           MPI          9 263.486328            0 
# Runtime: 14.020082 s (overhead: 0.000021 %) 10 records
