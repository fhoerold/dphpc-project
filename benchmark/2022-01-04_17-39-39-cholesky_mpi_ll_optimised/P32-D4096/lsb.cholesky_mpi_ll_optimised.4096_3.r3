# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 19:16:52 2022
# Execution time and date (local): Tue Jan  4 20:16:52 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 9170040.626953            0 
            3          4096           MPI          1 9078580.818359            6 
            3          4096           MPI          2 9004132.792969            1 
            3          4096           MPI          3 8979014.580078            0 
            3          4096           MPI          4 8478249.550781            8 
            3          4096           MPI          5 9073703.011719            0 
            3          4096           MPI          6 9038426.597656            0 
            3          4096           MPI          7 9048762.888672            0 
            3          4096           MPI          8 8981201.632812            9 
            3          4096           MPI          9 8849140.980469            0 
# Runtime: 112.274665 s (overhead: 0.000021 %) 10 records
