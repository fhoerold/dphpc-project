# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 19:16:52 2022
# Execution time and date (local): Tue Jan  4 20:16:52 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 9170703.150391           18 
            2          4096           MPI          1 9079243.357422           11 
            2          4096           MPI          2 9004789.794922            7 
            2          4096           MPI          3 8979667.646484            0 
            2          4096           MPI          4 8478874.250000            2 
            2          4096           MPI          5 9074369.181641            0 
            2          4096           MPI          6 9039087.609375            0 
            2          4096           MPI          7 9049426.140625            0 
            2          4096           MPI          8 8981860.197266           13 
            2          4096           MPI          9 8849780.359375            0 
# Runtime: 110.298498 s (overhead: 0.000046 %) 10 records
