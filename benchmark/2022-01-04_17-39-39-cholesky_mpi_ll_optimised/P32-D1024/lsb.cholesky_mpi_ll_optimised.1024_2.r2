# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 19:06:54 2022
# Execution time and date (local): Tue Jan  4 20:06:54 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 277944.083984            3 
            2          1024           MPI          1 277891.373047           11 
            2          1024           MPI          2 278586.898438            1 
            2          1024           MPI          3 278011.285156            0 
            2          1024           MPI          4 276283.255859            1 
            2          1024           MPI          5 277278.937500            0 
            2          1024           MPI          6 275413.630859            0 
            2          1024           MPI          7 276359.421875            0 
            2          1024           MPI          8 277771.320312            6 
            2          1024           MPI          9 276893.861328            0 
# Runtime: 3.381229 s (overhead: 0.000651 %) 10 records
