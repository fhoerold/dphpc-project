# Sysname : Linux
# Nodename: eu-a6-011-20
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 19:06:53 2022
# Execution time and date (local): Tue Jan  4 20:06:53 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 277860.427734            2 
            3          1024           MPI          1 277871.462891            7 
            3          1024           MPI          2 278554.496094            0 
            3          1024           MPI          3 277979.988281            0 
            3          1024           MPI          4 276252.158203            0 
            3          1024           MPI          5 277251.226562            0 
            3          1024           MPI          6 275391.214844            0 
            3          1024           MPI          7 276332.638672            0 
            3          1024           MPI          8 277739.343750            3 
            3          1024           MPI          9 276871.580078            0 
# Runtime: 13.452805 s (overhead: 0.000089 %) 10 records
