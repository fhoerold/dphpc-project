# Sysname : Linux
# Nodename: eu-a6-010-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 23:30:55 2022
# Execution time and date (local): Wed Jan  5 00:30:55 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 66087941.039062            2 
            3          8192           MPI          1 66029946.849609            6 
            3          8192           MPI          2 65854151.746094            5 
            3          8192           MPI          3 65891953.900391            0 
            3          8192           MPI          4 65847004.132812            6 
            3          8192           MPI          5 65817574.189453            2 
            3          8192           MPI          6 65839363.283203            2 
            3          8192           MPI          7 65698243.552734            2 
            3          8192           MPI          8 65845176.976562            5 
            3          8192           MPI          9 65865889.277344            2 
# Runtime: 796.975901 s (overhead: 0.000004 %) 10 records
