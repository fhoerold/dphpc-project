# Sysname : Linux
# Nodename: eu-a6-010-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 23:30:55 2022
# Execution time and date (local): Wed Jan  5 00:30:55 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 66072553.876953            0 
            2          8192           MPI          1 66014236.160156            6 
            2          8192           MPI          2 65838460.957031            6 
            2          8192           MPI          3 65876240.492188            0 
            2          8192           MPI          4 65831329.888672            4 
            2          8192           MPI          5 65801910.029297            0 
            2          8192           MPI          6 65823697.974609            0 
            2          8192           MPI          7 65682601.339844            0 
            2          8192           MPI          8 65829489.935547            7 
            2          8192           MPI          9 65850196.171875            2 
# Runtime: 799.810508 s (overhead: 0.000003 %) 10 records
