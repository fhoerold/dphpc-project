# Sysname : Linux
# Nodename: eu-a6-001-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:44:28 2022
# Execution time and date (local): Tue Jan  4 17:44:28 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 152010.402344            0 
            2          1024           MPI          1 151179.369141            4 
            2          1024           MPI          2 152354.093750            0 
            2          1024           MPI          3 153955.691406            0 
            2          1024           MPI          4 151320.568359            1 
            2          1024           MPI          5 157638.712891            0 
            2          1024           MPI          6 152225.650391            0 
            2          1024           MPI          7 151385.517578            0 
            2          1024           MPI          8 153264.865234            1 
            2          1024           MPI          9 151602.683594            0 
# Runtime: 2.846433 s (overhead: 0.000211 %) 10 records
