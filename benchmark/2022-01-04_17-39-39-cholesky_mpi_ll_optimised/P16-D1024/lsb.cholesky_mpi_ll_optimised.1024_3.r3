# Sysname : Linux
# Nodename: eu-a6-001-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Jan  4 16:44:28 2022
# Execution time and date (local): Tue Jan  4 17:44:28 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 151675.113281            0 
            3          1024           MPI          1 151182.279297            0 
            3          1024           MPI          2 152363.093750            7 
            3          1024           MPI          3 153951.771484            0 
            3          1024           MPI          4 151323.539062            3 
            3          1024           MPI          5 157642.128906            0 
            3          1024           MPI          6 152228.269531            0 
            3          1024           MPI          7 151394.160156            0 
            3          1024           MPI          8 153271.667969            4 
            3          1024           MPI          9 151603.685547            0 
# Runtime: 1.844806 s (overhead: 0.000759 %) 10 records
