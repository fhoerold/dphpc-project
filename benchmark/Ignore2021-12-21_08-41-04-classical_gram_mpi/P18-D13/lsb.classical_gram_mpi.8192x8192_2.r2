# Sysname : Linux
# Nodename: eu-a6-004-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 14:37:18 2021
# Execution time and date (local): Wed Dec 22 15:37:18 2021
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          8192          8192   gramschmidt           MPI          0 1343550633.136719            0 
            2          8192          8192   gramschmidt           MPI          1 1344379147.242188            4 
            2          8192          8192   gramschmidt           MPI          2 1329028886.150391            5 
            2          8192          8192   gramschmidt           MPI          3 1341372347.914062            1 
            2          8192          8192   gramschmidt           MPI          4 1350099351.085938            8 
            2          8192          8192   gramschmidt           MPI          5 1339512949.501953            2 
            2          8192          8192   gramschmidt           MPI          6 1324280934.710938            2 
            2          8192          8192   gramschmidt           MPI          7 1345559518.566406            2 
            2          8192          8192   gramschmidt           MPI          8 1334305350.650391            7 
            2          8192          8192   gramschmidt           MPI          9 1277301493.398438            2 
# Runtime: 17350.662840 s (overhead: 0.000000 %) 10 records
