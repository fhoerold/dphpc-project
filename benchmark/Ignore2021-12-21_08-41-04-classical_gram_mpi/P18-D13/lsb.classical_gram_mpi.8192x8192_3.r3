# Sysname : Linux
# Nodename: eu-a6-004-11
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 14:37:18 2021
# Execution time and date (local): Wed Dec 22 15:37:18 2021
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          8192          8192   gramschmidt           MPI          0 1343405204.912109            0 
            3          8192          8192   gramschmidt           MPI          1 1344233676.564453            5 
            3          8192          8192   gramschmidt           MPI          2 1328885104.966797            6 
            3          8192          8192   gramschmidt           MPI          3 1341227269.451172            1 
            3          8192          8192   gramschmidt           MPI          4 1349953151.548828            9 
            3          8192          8192   gramschmidt           MPI          5 1339368025.609375            1 
            3          8192          8192   gramschmidt           MPI          6 1324137616.339844            3 
            3          8192          8192   gramschmidt           MPI          7 1345413942.767578            3 
            3          8192          8192   gramschmidt           MPI          8 1334160081.251953            9 
            3          8192          8192   gramschmidt           MPI          9 1277162702.224609            3 
# Runtime: 17350.351764 s (overhead: 0.000000 %) 10 records
