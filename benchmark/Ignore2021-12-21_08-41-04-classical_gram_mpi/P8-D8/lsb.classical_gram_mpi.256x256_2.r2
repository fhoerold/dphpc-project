# Sysname : Linux
# Nodename: eu-a6-008-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 09:07:13 2021
# Execution time and date (local): Tue Dec 21 10:07:13 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 20107.402344            0 
            2           256           256   gramschmidt           MPI          1 20035.117188            0 
            2           256           256   gramschmidt           MPI          2 20071.369141            0 
            2           256           256   gramschmidt           MPI          3 20346.681641            0 
            2           256           256   gramschmidt           MPI          4 20066.880859            0 
            2           256           256   gramschmidt           MPI          5 20085.281250            0 
            2           256           256   gramschmidt           MPI          6 20014.652344            0 
            2           256           256   gramschmidt           MPI          7 20105.675781            0 
            2           256           256   gramschmidt           MPI          8 20276.398438            0 
            2           256           256   gramschmidt           MPI          9 20016.847656            0 
# Runtime: 0.266308 s (overhead: 0.000000 %) 10 records
