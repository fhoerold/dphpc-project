# Sysname : Linux
# Nodename: eu-a6-008-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 09:07:13 2021
# Execution time and date (local): Tue Dec 21 10:07:13 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 20105.900391            0 
            3           256           256   gramschmidt           MPI          1 20022.763672            1 
            3           256           256   gramschmidt           MPI          2 20086.941406            0 
            3           256           256   gramschmidt           MPI          3 20351.558594            0 
            3           256           256   gramschmidt           MPI          4 20067.808594            0 
            3           256           256   gramschmidt           MPI          5 20089.837891            0 
            3           256           256   gramschmidt           MPI          6 20009.550781            0 
            3           256           256   gramschmidt           MPI          7 20103.611328            0 
            3           256           256   gramschmidt           MPI          8 20275.208984            0 
            3           256           256   gramschmidt           MPI          9 20014.474609            0 
# Runtime: 0.266683 s (overhead: 0.000375 %) 10 records
