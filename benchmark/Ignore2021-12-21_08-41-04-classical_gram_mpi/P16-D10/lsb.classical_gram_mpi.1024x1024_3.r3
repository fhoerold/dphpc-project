# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:46:01 2021
# Execution time and date (local): Tue Dec 21 16:46:01 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 646584.632812            0 
            3          1024          1024   gramschmidt           MPI          1 640119.587891            4 
            3          1024          1024   gramschmidt           MPI          2 636845.304688            2 
            3          1024          1024   gramschmidt           MPI          3 642436.201172            0 
            3          1024          1024   gramschmidt           MPI          4 636221.166016            4 
            3          1024          1024   gramschmidt           MPI          5 631072.775391            0 
            3          1024          1024   gramschmidt           MPI          6 632237.388672            0 
            3          1024          1024   gramschmidt           MPI          7 627505.623047            0 
            3          1024          1024   gramschmidt           MPI          8 635862.429688            2 
            3          1024          1024   gramschmidt           MPI          9 631085.957031            0 
# Runtime: 18.334725 s (overhead: 0.000065 %) 10 records
