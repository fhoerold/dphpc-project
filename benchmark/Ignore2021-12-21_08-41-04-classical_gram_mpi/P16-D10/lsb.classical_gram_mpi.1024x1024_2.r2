# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:46:01 2021
# Execution time and date (local): Tue Dec 21 16:46:01 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 646940.859375            0 
            2          1024          1024   gramschmidt           MPI          1 640515.511719            5 
            2          1024          1024   gramschmidt           MPI          2 637236.013672            1 
            2          1024          1024   gramschmidt           MPI          3 642823.814453            0 
            2          1024          1024   gramschmidt           MPI          4 636612.330078            1 
            2          1024          1024   gramschmidt           MPI          5 631459.632812            0 
            2          1024          1024   gramschmidt           MPI          6 632623.195312            0 
            2          1024          1024   gramschmidt           MPI          7 627879.380859            0 
            2          1024          1024   gramschmidt           MPI          8 636251.466797            2 
            2          1024          1024   gramschmidt           MPI          9 631469.558594            0 
# Runtime: 22.367420 s (overhead: 0.000040 %) 10 records
