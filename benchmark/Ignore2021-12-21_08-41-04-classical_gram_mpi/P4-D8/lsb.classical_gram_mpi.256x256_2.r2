# Sysname : Linux
# Nodename: eu-a6-006-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:42:02 2021
# Execution time and date (local): Tue Dec 21 08:42:02 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 65619.865234            0 
            2           256           256   gramschmidt           MPI          1 62658.812500            5 
            2           256           256   gramschmidt           MPI          2 64058.783203            1 
            2           256           256   gramschmidt           MPI          3 65713.605469            0 
            2           256           256   gramschmidt           MPI          4 62668.105469            1 
            2           256           256   gramschmidt           MPI          5 60173.789062            0 
            2           256           256   gramschmidt           MPI          6 67983.234375            0 
            2           256           256   gramschmidt           MPI          7 64607.820312            0 
            2           256           256   gramschmidt           MPI          8 62895.003906            1 
            2           256           256   gramschmidt           MPI          9 62012.246094            0 
# Runtime: 11.837053 s (overhead: 0.000068 %) 10 records
