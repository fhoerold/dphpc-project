# Sysname : Linux
# Nodename: eu-a6-006-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:42:02 2021
# Execution time and date (local): Tue Dec 21 08:42:02 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 65633.156250            0 
            3           256           256   gramschmidt           MPI          1 62081.837891            7 
            3           256           256   gramschmidt           MPI          2 63948.384766            2 
            3           256           256   gramschmidt           MPI          3 65602.837891            0 
            3           256           256   gramschmidt           MPI          4 62514.023438            2 
            3           256           256   gramschmidt           MPI          5 59994.341797            0 
            3           256           256   gramschmidt           MPI          6 67823.767578            0 
            3           256           256   gramschmidt           MPI          7 64470.882812            0 
            3           256           256   gramschmidt           MPI          8 55716.847656            1 
            3           256           256   gramschmidt           MPI          9 56459.759766            0 
# Runtime: 6.830465 s (overhead: 0.000176 %) 10 records
