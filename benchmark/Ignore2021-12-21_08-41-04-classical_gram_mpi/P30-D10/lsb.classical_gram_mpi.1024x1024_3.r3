# Sysname : Linux
# Nodename: eu-a6-007-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 05:36:25 2021
# Execution time and date (local): Wed Dec 22 06:36:25 2021
# MPI execution on rank 3 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 420346.695312            0 
            3          1024          1024   gramschmidt           MPI          1 414035.439453            4 
            3          1024          1024   gramschmidt           MPI          2 409435.380859            2 
            3          1024          1024   gramschmidt           MPI          3 408793.605469            0 
            3          1024          1024   gramschmidt           MPI          4 419637.458984            1 
            3          1024          1024   gramschmidt           MPI          5 409163.121094            0 
            3          1024          1024   gramschmidt           MPI          6 406846.236328            0 
            3          1024          1024   gramschmidt           MPI          7 407232.068359            0 
            3          1024          1024   gramschmidt           MPI          8 407100.808594            3 
            3          1024          1024   gramschmidt           MPI          9 412071.695312            0 
# Runtime: 5.397676 s (overhead: 0.000185 %) 10 records
