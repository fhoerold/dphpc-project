# Sysname : Linux
# Nodename: eu-a6-007-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 05:36:25 2021
# Execution time and date (local): Wed Dec 22 06:36:25 2021
# MPI execution on rank 2 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 420349.021484            0 
            2          1024          1024   gramschmidt           MPI          1 413987.478516            4 
            2          1024          1024   gramschmidt           MPI          2 409380.058594            2 
            2          1024          1024   gramschmidt           MPI          3 408730.072266            0 
            2          1024          1024   gramschmidt           MPI          4 419560.390625            2 
            2          1024          1024   gramschmidt           MPI          5 409111.939453            0 
            2          1024          1024   gramschmidt           MPI          6 406794.861328            0 
            2          1024          1024   gramschmidt           MPI          7 407181.019531            0 
            2          1024          1024   gramschmidt           MPI          8 407045.882812            1 
            2          1024          1024   gramschmidt           MPI          9 412021.019531            0 
# Runtime: 11.408862 s (overhead: 0.000079 %) 10 records
