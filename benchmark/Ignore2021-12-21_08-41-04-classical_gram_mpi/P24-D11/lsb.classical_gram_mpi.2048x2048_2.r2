# Sysname : Linux
# Nodename: eu-a6-009-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 19:51:26 2021
# Execution time and date (local): Tue Dec 21 20:51:26 2021
# MPI execution on rank 2 with 24 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 6654214.357422            0 
            2          2048          2048   gramschmidt           MPI          1 6657389.876953            4 
            2          2048          2048   gramschmidt           MPI          2 6664483.894531            2 
            2          2048          2048   gramschmidt           MPI          3 6648224.519531            0 
            2          2048          2048   gramschmidt           MPI          4 6652415.277344            4 
            2          2048          2048   gramschmidt           MPI          5 6644732.367188            0 
            2          2048          2048   gramschmidt           MPI          6 6673578.275391            0 
            2          2048          2048   gramschmidt           MPI          7 6657696.455078            0 
            2          2048          2048   gramschmidt           MPI          8 6652642.814453            2 
            2          2048          2048   gramschmidt           MPI          9 6679044.203125            0 
# Runtime: 93.015484 s (overhead: 0.000013 %) 10 records
