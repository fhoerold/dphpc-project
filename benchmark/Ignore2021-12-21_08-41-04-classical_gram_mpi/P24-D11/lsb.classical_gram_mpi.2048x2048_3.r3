# Sysname : Linux
# Nodename: eu-a6-009-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 19:51:26 2021
# Execution time and date (local): Tue Dec 21 20:51:26 2021
# MPI execution on rank 3 with 24 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 6641718.732422            0 
            3          2048          2048   gramschmidt           MPI          1 6644990.697266            4 
            3          2048          2048   gramschmidt           MPI          2 6651994.826172            1 
            3          2048          2048   gramschmidt           MPI          3 6635761.894531            0 
            3          2048          2048   gramschmidt           MPI          4 6640032.654297            4 
            3          2048          2048   gramschmidt           MPI          5 6632279.515625            0 
            3          2048          2048   gramschmidt           MPI          6 6661067.197266            0 
            3          2048          2048   gramschmidt           MPI          7 6645211.984375            0 
            3          2048          2048   gramschmidt           MPI          8 6640180.136719            2 
            3          2048          2048   gramschmidt           MPI          9 6666616.654297            0 
# Runtime: 92.810162 s (overhead: 0.000012 %) 10 records
