# Sysname : Linux
# Nodename: eu-a6-009-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 19:50:26 2021
# Execution time and date (local): Tue Dec 21 20:50:26 2021
# MPI execution on rank 2 with 24 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 21271.746094            0 
            2           256           256   gramschmidt           MPI          1 18880.267578            4 
            2           256           256   gramschmidt           MPI          2 21089.273438            0 
            2           256           256   gramschmidt           MPI          3 18703.064453            0 
            2           256           256   gramschmidt           MPI          4 18665.275391            0 
            2           256           256   gramschmidt           MPI          5 18508.255859            0 
            2           256           256   gramschmidt           MPI          6 18601.806641            0 
            2           256           256   gramschmidt           MPI          7 18560.042969            0 
            2           256           256   gramschmidt           MPI          8 18427.037109            0 
            2           256           256   gramschmidt           MPI          9 18494.634766            0 
# Runtime: 7.293070 s (overhead: 0.000055 %) 10 records
