# Sysname : Linux
# Nodename: eu-a6-009-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 19:50:26 2021
# Execution time and date (local): Tue Dec 21 20:50:26 2021
# MPI execution on rank 3 with 24 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 20537.214844            0 
            3           256           256   gramschmidt           MPI          1 20628.404297            4 
            3           256           256   gramschmidt           MPI          2 22801.726562            3 
            3           256           256   gramschmidt           MPI          3 20479.390625            0 
            3           256           256   gramschmidt           MPI          4 20480.060547            1 
            3           256           256   gramschmidt           MPI          5 20335.962891            0 
            3           256           256   gramschmidt           MPI          6 20351.767578            0 
            3           256           256   gramschmidt           MPI          7 20378.521484            0 
            3           256           256   gramschmidt           MPI          8 20245.923828            0 
            3           256           256   gramschmidt           MPI          9 20266.982422            0 
# Runtime: 8.285760 s (overhead: 0.000097 %) 10 records
