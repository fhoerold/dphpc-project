# Sysname : Linux
# Nodename: eu-a6-008-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 09:07:22 2021
# Execution time and date (local): Tue Dec 21 10:07:22 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 146089.000000            0 
            3           512           512   gramschmidt           MPI          1 146052.535156            5 
            3           512           512   gramschmidt           MPI          2 146231.607422            1 
            3           512           512   gramschmidt           MPI          3 151533.154297            0 
            3           512           512   gramschmidt           MPI          4 147453.166016            1 
            3           512           512   gramschmidt           MPI          5 146706.646484            0 
            3           512           512   gramschmidt           MPI          6 146490.517578            0 
            3           512           512   gramschmidt           MPI          7 146810.119141            0 
            3           512           512   gramschmidt           MPI          8 147255.328125            1 
            3           512           512   gramschmidt           MPI          9 146522.060547            0 
# Runtime: 3.919145 s (overhead: 0.000204 %) 10 records
