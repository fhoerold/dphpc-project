# Sysname : Linux
# Nodename: eu-a6-008-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 09:07:22 2021
# Execution time and date (local): Tue Dec 21 10:07:22 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 146079.757812            0 
            2           512           512   gramschmidt           MPI          1 146041.199219            1 
            2           512           512   gramschmidt           MPI          2 146222.230469            1 
            2           512           512   gramschmidt           MPI          3 151526.138672            0 
            2           512           512   gramschmidt           MPI          4 147442.482422            6 
            2           512           512   gramschmidt           MPI          5 146688.173828            0 
            2           512           512   gramschmidt           MPI          6 146481.343750            0 
            2           512           512   gramschmidt           MPI          7 146800.763672            0 
            2           512           512   gramschmidt           MPI          8 147240.642578            1 
            2           512           512   gramschmidt           MPI          9 146509.462891            0 
# Runtime: 1.926162 s (overhead: 0.000467 %) 10 records
