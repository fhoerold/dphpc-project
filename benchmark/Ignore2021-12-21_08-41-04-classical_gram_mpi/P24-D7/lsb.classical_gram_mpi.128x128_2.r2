# Sysname : Linux
# Nodename: eu-a6-009-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 19:35:20 2021
# Execution time and date (local): Tue Dec 21 20:35:20 2021
# MPI execution on rank 2 with 24 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 5269.421875            0 
            2           128           128   gramschmidt           MPI          1 4849.925781            3 
            2           128           128   gramschmidt           MPI          2 4734.691406            0 
            2           128           128   gramschmidt           MPI          3 4724.705078            0 
            2           128           128   gramschmidt           MPI          4 4747.255859            0 
            2           128           128   gramschmidt           MPI          5 4733.136719            0 
            2           128           128   gramschmidt           MPI          6 4776.097656            0 
            2           128           128   gramschmidt           MPI          7 4742.298828            0 
            2           128           128   gramschmidt           MPI          8 4753.738281            0 
            2           128           128   gramschmidt           MPI          9 4744.382812            0 
# Runtime: 6.095805 s (overhead: 0.000049 %) 10 records
