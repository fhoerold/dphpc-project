# Sysname : Linux
# Nodename: eu-a6-009-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 19:35:20 2021
# Execution time and date (local): Tue Dec 21 20:35:20 2021
# MPI execution on rank 3 with 24 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 5290.697266            0 
            3           128           128   gramschmidt           MPI          1 4850.335938            2 
            3           128           128   gramschmidt           MPI          2 4735.916016            0 
            3           128           128   gramschmidt           MPI          3 4725.554688            0 
            3           128           128   gramschmidt           MPI          4 4750.894531            0 
            3           128           128   gramschmidt           MPI          5 4729.173828            0 
            3           128           128   gramschmidt           MPI          6 4775.539062            0 
            3           128           128   gramschmidt           MPI          7 4738.394531            0 
            3           128           128   gramschmidt           MPI          8 4755.855469            0 
            3           128           128   gramschmidt           MPI          9 4744.117188            0 
# Runtime: 6.095392 s (overhead: 0.000033 %) 10 records
