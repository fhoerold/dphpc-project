# Sysname : Linux
# Nodename: eu-a6-008-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 13:50:13 2021
# Execution time and date (local): Tue Dec 21 14:50:13 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 113724.398438            0 
            3           512           512   gramschmidt           MPI          1 114324.255859            3 
            3           512           512   gramschmidt           MPI          2 112608.496094            1 
            3           512           512   gramschmidt           MPI          3 113802.796875            0 
            3           512           512   gramschmidt           MPI          4 114005.826172            1 
            3           512           512   gramschmidt           MPI          5 113162.210938            0 
            3           512           512   gramschmidt           MPI          6 113184.326172            0 
            3           512           512   gramschmidt           MPI          7 113272.546875            0 
            3           512           512   gramschmidt           MPI          8 113179.376953            1 
            3           512           512   gramschmidt           MPI          9 113482.451172            0 
# Runtime: 2.493773 s (overhead: 0.000241 %) 10 records
