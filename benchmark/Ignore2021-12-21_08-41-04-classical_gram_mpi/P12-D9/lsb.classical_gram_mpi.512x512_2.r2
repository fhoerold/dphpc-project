# Sysname : Linux
# Nodename: eu-a6-008-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 13:50:13 2021
# Execution time and date (local): Tue Dec 21 14:50:13 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 113987.515625            0 
            2           512           512   gramschmidt           MPI          1 114334.017578            4 
            2           512           512   gramschmidt           MPI          2 112622.371094            2 
            2           512           512   gramschmidt           MPI          3 113802.609375            0 
            2           512           512   gramschmidt           MPI          4 114011.783203            1 
            2           512           512   gramschmidt           MPI          5 113489.160156            0 
            2           512           512   gramschmidt           MPI          6 113187.914062            0 
            2           512           512   gramschmidt           MPI          7 113281.431641            0 
            2           512           512   gramschmidt           MPI          8 113178.707031            1 
            2           512           512   gramschmidt           MPI          9 113482.593750            0 
# Runtime: 2.494906 s (overhead: 0.000321 %) 10 records
