# Sysname : Linux
# Nodename: eu-a6-001-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 14:35:44 2021
# Execution time and date (local): Wed Dec 22 15:35:44 2021
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 1492.291016            0 
            2            64            64   gramschmidt           MPI          1 1424.451172            2 
            2            64            64   gramschmidt           MPI          2 1328.791016            0 
            2            64            64   gramschmidt           MPI          3 1270.734375            0 
            2            64            64   gramschmidt           MPI          4 1281.791016            0 
            2            64            64   gramschmidt           MPI          5 1251.419922            0 
            2            64            64   gramschmidt           MPI          6 1254.183594            0 
            2            64            64   gramschmidt           MPI          7 1245.052734            0 
            2            64            64   gramschmidt           MPI          8 1226.294922            0 
            2            64            64   gramschmidt           MPI          9 1225.634766            0 
# Runtime: 7.005032 s (overhead: 0.000029 %) 10 records
