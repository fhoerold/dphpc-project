# Sysname : Linux
# Nodename: eu-a6-001-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 14:35:44 2021
# Execution time and date (local): Wed Dec 22 15:35:44 2021
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 1500.199219            0 
            3            64            64   gramschmidt           MPI          1 1430.246094            2 
            3            64            64   gramschmidt           MPI          2 1329.730469            0 
            3            64            64   gramschmidt           MPI          3 1271.623047            0 
            3            64            64   gramschmidt           MPI          4 1279.837891            0 
            3            64            64   gramschmidt           MPI          5 1254.335938            0 
            3            64            64   gramschmidt           MPI          6 1256.447266            0 
            3            64            64   gramschmidt           MPI          7 1242.718750            0 
            3            64            64   gramschmidt           MPI          8 1228.779297            0 
            3            64            64   gramschmidt           MPI          9 1226.367188            0 
# Runtime: 5.025825 s (overhead: 0.000040 %) 10 records
