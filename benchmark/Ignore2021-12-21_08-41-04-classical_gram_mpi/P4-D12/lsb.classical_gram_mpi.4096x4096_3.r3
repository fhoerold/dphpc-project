# Sysname : Linux
# Nodename: eu-a6-007-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 08:21:32 2021
# Execution time and date (local): Tue Dec 21 09:21:32 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 655910451.011719            0 
            3          4096          4096   gramschmidt           MPI          1 646566759.636719            5 
            3          4096          4096   gramschmidt           MPI          2 658282213.226562            6 
            3          4096          4096   gramschmidt           MPI          3 656560678.125000            1 
            3          4096          4096   gramschmidt           MPI          4 648639990.722656            7 
            3          4096          4096   gramschmidt           MPI          5 657971320.316406            1 
            3          4096          4096   gramschmidt           MPI          6 656900738.121094            1 
            3          4096          4096   gramschmidt           MPI          7 648885953.488281            1 
            3          4096          4096   gramschmidt           MPI          8 658042958.423828            7 
            3          4096          4096   gramschmidt           MPI          9 657299823.128906            1 
# Runtime: 8507.671122 s (overhead: 0.000000 %) 10 records
