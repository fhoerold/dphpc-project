# Sysname : Linux
# Nodename: eu-a6-007-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 08:21:32 2021
# Execution time and date (local): Tue Dec 21 09:21:32 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 655854887.222656            0 
            2          4096          4096   gramschmidt           MPI          1 646512015.398438            4 
            2          4096          4096   gramschmidt           MPI          2 658226646.056641            5 
            2          4096          4096   gramschmidt           MPI          3 656505182.599609            0 
            2          4096          4096   gramschmidt           MPI          4 648584706.945312            4 
            2          4096          4096   gramschmidt           MPI          5 657915759.023438            0 
            2          4096          4096   gramschmidt           MPI          6 656845147.167969            0 
            2          4096          4096   gramschmidt           MPI          7 648830885.892578            0 
            2          4096          4096   gramschmidt           MPI          8 657987155.406250            4 
            2          4096          4096   gramschmidt           MPI          9 657244384.824219            0 
# Runtime: 8510.156928 s (overhead: 0.000000 %) 10 records
