# Sysname : Linux
# Nodename: eu-a6-008-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 13:49:57 2021
# Execution time and date (local): Tue Dec 21 14:49:57 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 1075.080078            0 
            3            64            64   gramschmidt           MPI          1 1011.498047            2 
            3            64            64   gramschmidt           MPI          2 1005.554688            0 
            3            64            64   gramschmidt           MPI          3 992.597656            0 
            3            64            64   gramschmidt           MPI          4 990.917969            0 
            3            64            64   gramschmidt           MPI          5 1004.494141            0 
            3            64            64   gramschmidt           MPI          6 992.257812            0 
            3            64            64   gramschmidt           MPI          7 992.931641            0 
            3            64            64   gramschmidt           MPI          8 996.636719            0 
            3            64            64   gramschmidt           MPI          9 989.617188            0 
# Runtime: 8.011525 s (overhead: 0.000025 %) 10 records
