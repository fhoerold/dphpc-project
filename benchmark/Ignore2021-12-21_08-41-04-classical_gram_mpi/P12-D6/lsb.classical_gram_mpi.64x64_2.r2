# Sysname : Linux
# Nodename: eu-a6-008-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 13:49:57 2021
# Execution time and date (local): Tue Dec 21 14:49:57 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 1021.710938            0 
            2            64            64   gramschmidt           MPI          1 1013.343750            2 
            2            64            64   gramschmidt           MPI          2 1001.847656            0 
            2            64            64   gramschmidt           MPI          3 993.777344            0 
            2            64            64   gramschmidt           MPI          4 992.037109            0 
            2            64            64   gramschmidt           MPI          5 1006.041016            0 
            2            64            64   gramschmidt           MPI          6 990.492188            0 
            2            64            64   gramschmidt           MPI          7 995.232422            0 
            2            64            64   gramschmidt           MPI          8 997.203125            0 
            2            64            64   gramschmidt           MPI          9 993.470703            0 
# Runtime: 8.017319 s (overhead: 0.000025 %) 10 records
