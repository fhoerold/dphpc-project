# Sysname : Linux
# Nodename: eu-a6-008-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 09:07:33 2021
# Execution time and date (local): Tue Dec 21 10:07:33 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 1794246.441406            0 
            2          1024          1024   gramschmidt           MPI          1 1802544.361328            6 
            2          1024          1024   gramschmidt           MPI          2 1809880.068359            1 
            2          1024          1024   gramschmidt           MPI          3 1797197.640625            0 
            2          1024          1024   gramschmidt           MPI          4 1784636.451172            6 
            2          1024          1024   gramschmidt           MPI          5 1791611.000000            0 
            2          1024          1024   gramschmidt           MPI          6 1799358.910156            0 
            2          1024          1024   gramschmidt           MPI          7 1788338.421875            0 
            2          1024          1024   gramschmidt           MPI          8 1799816.294922            2 
            2          1024          1024   gramschmidt           MPI          9 1795775.398438            0 
# Runtime: 23.396364 s (overhead: 0.000064 %) 10 records
