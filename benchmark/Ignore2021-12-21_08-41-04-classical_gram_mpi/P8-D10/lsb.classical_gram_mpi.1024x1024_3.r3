# Sysname : Linux
# Nodename: eu-a6-008-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 09:07:33 2021
# Execution time and date (local): Tue Dec 21 10:07:33 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 1792866.169922            0 
            3          1024          1024   gramschmidt           MPI          1 1801182.748047            3 
            3          1024          1024   gramschmidt           MPI          2 1808451.199219            1 
            3          1024          1024   gramschmidt           MPI          3 1795780.031250            0 
            3          1024          1024   gramschmidt           MPI          4 1783233.968750            1 
            3          1024          1024   gramschmidt           MPI          5 1790228.570312            0 
            3          1024          1024   gramschmidt           MPI          6 1798006.121094            0 
            3          1024          1024   gramschmidt           MPI          7 1786923.857422            0 
            3          1024          1024   gramschmidt           MPI          8 1798402.615234            4 
            3          1024          1024   gramschmidt           MPI          9 1794377.042969            0 
# Runtime: 29.358745 s (overhead: 0.000031 %) 10 records
