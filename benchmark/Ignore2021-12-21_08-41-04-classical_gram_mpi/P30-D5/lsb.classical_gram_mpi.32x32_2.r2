# Sysname : Linux
# Nodename: eu-a6-007-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 05:35:29 2021
# Execution time and date (local): Wed Dec 22 06:35:29 2021
# MPI execution on rank 2 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 1358.357422            0 
            2            32            32   gramschmidt           MPI          1 1269.511719            3 
            2            32            32   gramschmidt           MPI          2 960.298828            0 
            2            32            32   gramschmidt           MPI          3 947.863281            0 
            2            32            32   gramschmidt           MPI          4 937.023438            0 
            2            32            32   gramschmidt           MPI          5 930.148438            0 
            2            32            32   gramschmidt           MPI          6 933.882812            0 
            2            32            32   gramschmidt           MPI          7 915.074219            0 
            2            32            32   gramschmidt           MPI          8 887.326172            0 
            2            32            32   gramschmidt           MPI          9 905.693359            0 
# Runtime: 16.025471 s (overhead: 0.000019 %) 10 records
