# Sysname : Linux
# Nodename: eu-a6-007-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 05:35:29 2021
# Execution time and date (local): Wed Dec 22 06:35:29 2021
# MPI execution on rank 3 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 1637.505859            0 
            3            32            32   gramschmidt           MPI          1 1260.070312            0 
            3            32            32   gramschmidt           MPI          2 936.097656            0 
            3            32            32   gramschmidt           MPI          3 944.814453            0 
            3            32            32   gramschmidt           MPI          4 923.994141            0 
            3            32            32   gramschmidt           MPI          5 913.466797            0 
            3            32            32   gramschmidt           MPI          6 928.359375            0 
            3            32            32   gramschmidt           MPI          7 899.423828            0 
            3            32            32   gramschmidt           MPI          8 875.152344            0 
            3            32            32   gramschmidt           MPI          9 895.060547            0 
# Runtime: 0.017059 s (overhead: 0.000000 %) 10 records
