# Sysname : Linux
# Nodename: eu-a6-002-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 11:12:57 2021
# Execution time and date (local): Thu Dec 23 12:12:57 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 455259.650391            0 
            2          1024          1024   gramschmidt           MPI          1 461442.595703            5 
            2          1024          1024   gramschmidt           MPI          2 449393.638672            2 
            2          1024          1024   gramschmidt           MPI          3 449073.322266            1 
            2          1024          1024   gramschmidt           MPI          4 449360.472656            1 
            2          1024          1024   gramschmidt           MPI          5 458363.656250            0 
            2          1024          1024   gramschmidt           MPI          6 449802.771484            0 
            2          1024          1024   gramschmidt           MPI          7 451427.166016            0 
            2          1024          1024   gramschmidt           MPI          8 452724.234375            9 
            2          1024          1024   gramschmidt           MPI          9 448087.619141            0 
# Runtime: 14.094259 s (overhead: 0.000128 %) 10 records
