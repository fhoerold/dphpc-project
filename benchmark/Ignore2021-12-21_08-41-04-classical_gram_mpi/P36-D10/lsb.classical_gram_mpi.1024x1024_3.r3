# Sysname : Linux
# Nodename: eu-a6-002-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 11:12:57 2021
# Execution time and date (local): Thu Dec 23 12:12:57 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 455126.679688            0 
            3          1024          1024   gramschmidt           MPI          1 461289.537109            5 
            3          1024          1024   gramschmidt           MPI          2 449225.955078            1 
            3          1024          1024   gramschmidt           MPI          3 448928.160156            0 
            3          1024          1024   gramschmidt           MPI          4 449213.896484            1 
            3          1024          1024   gramschmidt           MPI          5 458207.906250            0 
            3          1024          1024   gramschmidt           MPI          6 449654.453125            0 
            3          1024          1024   gramschmidt           MPI          7 451246.746094            0 
            3          1024          1024   gramschmidt           MPI          8 452251.863281            1 
            3          1024          1024   gramschmidt           MPI          9 447948.933594            0 
# Runtime: 18.077949 s (overhead: 0.000044 %) 10 records
