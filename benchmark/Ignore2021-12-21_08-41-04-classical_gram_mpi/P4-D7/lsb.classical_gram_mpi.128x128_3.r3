# Sysname : Linux
# Nodename: eu-a6-006-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:41:46 2021
# Execution time and date (local): Tue Dec 21 08:41:46 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 8259.849609            0 
            3           128           128   gramschmidt           MPI          1 8177.757812            5 
            3           128           128   gramschmidt           MPI          2 8016.652344            0 
            3           128           128   gramschmidt           MPI          3 7974.906250            0 
            3           128           128   gramschmidt           MPI          4 8299.349609            1 
            3           128           128   gramschmidt           MPI          5 8269.173828            0 
            3           128           128   gramschmidt           MPI          6 7405.761719            0 
            3           128           128   gramschmidt           MPI          7 7752.007812            0 
            3           128           128   gramschmidt           MPI          8 8005.501953            0 
            3           128           128   gramschmidt           MPI          9 8179.808594            0 
# Runtime: 7.111239 s (overhead: 0.000084 %) 10 records
