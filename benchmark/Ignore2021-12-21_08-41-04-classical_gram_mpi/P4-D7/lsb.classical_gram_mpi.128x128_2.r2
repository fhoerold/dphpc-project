# Sysname : Linux
# Nodename: eu-a6-006-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:41:46 2021
# Execution time and date (local): Tue Dec 21 08:41:46 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 8276.750000            0 
            2           128           128   gramschmidt           MPI          1 8206.818359            3 
            2           128           128   gramschmidt           MPI          2 8044.316406            0 
            2           128           128   gramschmidt           MPI          3 7989.958984            0 
            2           128           128   gramschmidt           MPI          4 8324.923828            0 
            2           128           128   gramschmidt           MPI          5 8295.960938            0 
            2           128           128   gramschmidt           MPI          6 7445.306641            0 
            2           128           128   gramschmidt           MPI          7 7789.039062            0 
            2           128           128   gramschmidt           MPI          8 8037.437500            0 
            2           128           128   gramschmidt           MPI          9 8228.759766            0 
# Runtime: 9.102924 s (overhead: 0.000033 %) 10 records
