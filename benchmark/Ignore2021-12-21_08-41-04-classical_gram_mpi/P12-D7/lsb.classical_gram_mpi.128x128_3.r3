# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:47:07 2021
# Execution time and date (local): Tue Dec 21 08:47:07 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 3699.121094            1 
            3           128           128   gramschmidt           MPI          1 3544.691406            2 
            3           128           128   gramschmidt           MPI          2 3543.916016            0 
            3           128           128   gramschmidt           MPI          3 3534.394531            0 
            3           128           128   gramschmidt           MPI          4 3583.708984            0 
            3           128           128   gramschmidt           MPI          5 3601.287109            0 
            3           128           128   gramschmidt           MPI          6 3530.347656            0 
            3           128           128   gramschmidt           MPI          7 3590.238281            0 
            3           128           128   gramschmidt           MPI          8 3534.412109            0 
            3           128           128   gramschmidt           MPI          9 3510.654297            0 
# Runtime: 6.049495 s (overhead: 0.000050 %) 10 records
