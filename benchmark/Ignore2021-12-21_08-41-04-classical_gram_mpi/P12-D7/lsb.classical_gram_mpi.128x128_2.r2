# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:47:07 2021
# Execution time and date (local): Tue Dec 21 08:47:07 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 3720.234375            0 
            2           128           128   gramschmidt           MPI          1 3375.634766            2 
            2           128           128   gramschmidt           MPI          2 3371.246094            0 
            2           128           128   gramschmidt           MPI          3 3367.726562            0 
            2           128           128   gramschmidt           MPI          4 3413.671875            0 
            2           128           128   gramschmidt           MPI          5 3434.060547            0 
            2           128           128   gramschmidt           MPI          6 3368.353516            0 
            2           128           128   gramschmidt           MPI          7 3430.554688            0 
            2           128           128   gramschmidt           MPI          8 3373.222656            0 
            2           128           128   gramschmidt           MPI          9 3345.427734            0 
# Runtime: 6.046337 s (overhead: 0.000033 %) 10 records
