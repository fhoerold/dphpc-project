# Sysname : Linux
# Nodename: eu-a6-005-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:45:05 2021
# Execution time and date (local): Tue Dec 21 08:45:05 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          8192          8192   gramschmidt           MPI          0 1643404094.601562            0 
            3          8192          8192   gramschmidt           MPI          1 1643016957.503906            4 
            3          8192          8192   gramschmidt           MPI          2 1643770929.099609            4 
            3          8192          8192   gramschmidt           MPI          3 1643585251.845703            2 
            3          8192          8192   gramschmidt           MPI          4 1691090038.498047            7 
            3          8192          8192   gramschmidt           MPI          5 1739675138.048828            2 
            3          8192          8192   gramschmidt           MPI          6 1721375189.332031            2 
            3          8192          8192   gramschmidt           MPI          7 1775072336.054688            1 
            3          8192          8192   gramschmidt           MPI          8 1741420718.962891            4 
            3          8192          8192   gramschmidt           MPI          9 1648317052.898438            0 
# Runtime: 21849.373527 s (overhead: 0.000000 %) 10 records
