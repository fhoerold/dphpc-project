# Sysname : Linux
# Nodename: eu-a6-005-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:45:05 2021
# Execution time and date (local): Tue Dec 21 08:45:05 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          8192          8192   gramschmidt           MPI          0 1643476650.503906            1 
            2          8192          8192   gramschmidt           MPI          1 1643089612.529297            5 
            2          8192          8192   gramschmidt           MPI          2 1643843579.875000            3 
            2          8192          8192   gramschmidt           MPI          3 1643657876.791016            0 
            2          8192          8192   gramschmidt           MPI          4 1691164413.568359            6 
            2          8192          8192   gramschmidt           MPI          5 1739752728.808594            1 
            2          8192          8192   gramschmidt           MPI          6 1721451823.974609            1 
            2          8192          8192   gramschmidt           MPI          7 1775151166.814453            1 
            2          8192          8192   gramschmidt           MPI          8 1741498238.957031            5 
            2          8192          8192   gramschmidt           MPI          9 1648389919.287109            1 
# Runtime: 21849.801783 s (overhead: 0.000000 %) 10 records
