# Sysname : Linux
# Nodename: eu-a6-006-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:41:35 2021
# Execution time and date (local): Tue Dec 21 08:41:35 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 356.214844            0 
            3            32            32   gramschmidt           MPI          1 333.505859            0 
            3            32            32   gramschmidt           MPI          2 341.611328            0 
            3            32            32   gramschmidt           MPI          3 333.195312            0 
            3            32            32   gramschmidt           MPI          4 334.705078            0 
            3            32            32   gramschmidt           MPI          5 341.839844            0 
            3            32            32   gramschmidt           MPI          6 337.453125            0 
            3            32            32   gramschmidt           MPI          7 335.117188            0 
            3            32            32   gramschmidt           MPI          8 442.681641            0 
            3            32            32   gramschmidt           MPI          9 378.978516            0 
# Runtime: 0.006138 s (overhead: 0.000000 %) 10 records
