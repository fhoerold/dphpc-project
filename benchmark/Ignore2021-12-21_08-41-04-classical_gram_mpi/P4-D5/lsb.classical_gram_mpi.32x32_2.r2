# Sysname : Linux
# Nodename: eu-a6-006-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:41:35 2021
# Execution time and date (local): Tue Dec 21 08:41:35 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 358.519531            0 
            2            32            32   gramschmidt           MPI          1 335.097656            2 
            2            32            32   gramschmidt           MPI          2 343.441406            0 
            2            32            32   gramschmidt           MPI          3 337.189453            0 
            2            32            32   gramschmidt           MPI          4 339.429688            0 
            2            32            32   gramschmidt           MPI          5 345.947266            0 
            2            32            32   gramschmidt           MPI          6 339.097656            0 
            2            32            32   gramschmidt           MPI          7 337.931641            0 
            2            32            32   gramschmidt           MPI          8 444.677734            0 
            2            32            32   gramschmidt           MPI          9 379.843750            0 
# Runtime: 3.005791 s (overhead: 0.000067 %) 10 records
