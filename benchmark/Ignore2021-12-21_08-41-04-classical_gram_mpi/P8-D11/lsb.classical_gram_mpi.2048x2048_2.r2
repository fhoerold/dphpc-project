# Sysname : Linux
# Nodename: eu-a6-005-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 08:28:29 2021
# Execution time and date (local): Tue Dec 21 09:28:29 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 20335994.201172            0 
            2          2048          2048   gramschmidt           MPI          1 20295186.695312            4 
            2          2048          2048   gramschmidt           MPI          2 20280807.253906            2 
            2          2048          2048   gramschmidt           MPI          3 20366307.314453            0 
            2          2048          2048   gramschmidt           MPI          4 20255977.199219            7 
            2          2048          2048   gramschmidt           MPI          5 20251582.123047            0 
            2          2048          2048   gramschmidt           MPI          6 20285548.728516            0 
            2          2048          2048   gramschmidt           MPI          7 20435708.177734            0 
            2          2048          2048   gramschmidt           MPI          8 20300550.884766            7 
            2          2048          2048   gramschmidt           MPI          9 21750264.109375            0 
# Runtime: 270.654841 s (overhead: 0.000007 %) 10 records
