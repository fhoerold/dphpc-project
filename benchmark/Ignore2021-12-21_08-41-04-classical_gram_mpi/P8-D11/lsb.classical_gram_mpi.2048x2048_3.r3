# Sysname : Linux
# Nodename: eu-a6-005-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 08:28:29 2021
# Execution time and date (local): Tue Dec 21 09:28:29 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 20336934.148438            0 
            3          2048          2048   gramschmidt           MPI          1 20295835.695312            4 
            3          2048          2048   gramschmidt           MPI          2 20281445.283203            2 
            3          2048          2048   gramschmidt           MPI          3 20367179.691406            0 
            3          2048          2048   gramschmidt           MPI          4 20256602.986328            5 
            3          2048          2048   gramschmidt           MPI          5 20252209.507812            0 
            3          2048          2048   gramschmidt           MPI          6 20286409.187500            0 
            3          2048          2048   gramschmidt           MPI          7 20436341.707031            0 
            3          2048          2048   gramschmidt           MPI          8 20301185.542969            7 
            3          2048          2048   gramschmidt           MPI          9 21751145.740234            0 
# Runtime: 265.632004 s (overhead: 0.000007 %) 10 records
