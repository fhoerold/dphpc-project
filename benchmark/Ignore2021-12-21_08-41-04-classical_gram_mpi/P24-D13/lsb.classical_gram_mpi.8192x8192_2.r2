# Sysname : Linux
# Nodename: eu-a6-004-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 17:15:08 2021
# Execution time and date (local): Wed Dec 22 18:15:08 2021
# MPI execution on rank 2 with 24 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          8192          8192   gramschmidt           MPI          0 1543936708.208984            0 
            2          8192          8192   gramschmidt           MPI          1 1547030801.958984            7 
            2          8192          8192   gramschmidt           MPI          2 1544434749.550781            7 
            2          8192          8192   gramschmidt           MPI          3 1537638904.781250            2 
            2          8192          8192   gramschmidt           MPI          4 1562520162.937500            7 
            2          8192          8192   gramschmidt           MPI          5 1549374247.792969            2 
            2          8192          8192   gramschmidt           MPI          6 1567469111.306641            2 
            2          8192          8192   gramschmidt           MPI          7 1561353267.644531            2 
            2          8192          8192   gramschmidt           MPI          8 1550087627.316406            8 
            2          8192          8192   gramschmidt           MPI          9 1576157060.142578            4 
# Runtime: 20106.110017 s (overhead: 0.000000 %) 10 records
