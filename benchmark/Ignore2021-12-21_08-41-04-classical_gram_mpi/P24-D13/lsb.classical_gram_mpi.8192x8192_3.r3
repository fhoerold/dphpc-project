# Sysname : Linux
# Nodename: eu-a6-004-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 17:15:09 2021
# Execution time and date (local): Wed Dec 22 18:15:09 2021
# MPI execution on rank 3 with 24 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          8192          8192   gramschmidt           MPI          0 1543257070.642578            2 
            3          8192          8192   gramschmidt           MPI          1 1546349483.550781           17 
            3          8192          8192   gramschmidt           MPI          2 1543755246.548828            7 
            3          8192          8192   gramschmidt           MPI          3 1536965468.642578            2 
            3          8192          8192   gramschmidt           MPI          4 1561831726.744141           10 
            3          8192          8192   gramschmidt           MPI          5 1548691734.087891            4 
            3          8192          8192   gramschmidt           MPI          6 1566778770.708984            3 
            3          8192          8192   gramschmidt           MPI          7 1560664866.244141            4 
            3          8192          8192   gramschmidt           MPI          8 1549404940.523438            8 
            3          8192          8192   gramschmidt           MPI          9 1575461713.695312            2 
# Runtime: 20096.820011 s (overhead: 0.000000 %) 10 records
