# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:45:15 2021
# Execution time and date (local): Tue Dec 21 16:45:15 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 2878.285156            0 
            3           128           128   gramschmidt           MPI          1 2816.861328            2 
            3           128           128   gramschmidt           MPI          2 2833.914062            0 
            3           128           128   gramschmidt           MPI          3 2852.730469            0 
            3           128           128   gramschmidt           MPI          4 2780.724609            0 
            3           128           128   gramschmidt           MPI          5 2766.207031            0 
            3           128           128   gramschmidt           MPI          6 2807.097656            0 
            3           128           128   gramschmidt           MPI          7 2754.931641            0 
            3           128           128   gramschmidt           MPI          8 2760.037109            0 
            3           128           128   gramschmidt           MPI          9 2771.601562            0 
# Runtime: 4.037320 s (overhead: 0.000050 %) 10 records
