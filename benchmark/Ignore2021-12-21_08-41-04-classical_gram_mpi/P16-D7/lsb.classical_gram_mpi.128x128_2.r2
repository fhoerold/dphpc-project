# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:45:15 2021
# Execution time and date (local): Tue Dec 21 16:45:15 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 2878.748047            0 
            2           128           128   gramschmidt           MPI          1 2818.361328            2 
            2           128           128   gramschmidt           MPI          2 2832.541016            0 
            2           128           128   gramschmidt           MPI          3 2848.416016            0 
            2           128           128   gramschmidt           MPI          4 2777.291016            0 
            2           128           128   gramschmidt           MPI          5 2762.595703            0 
            2           128           128   gramschmidt           MPI          6 2804.552734            0 
            2           128           128   gramschmidt           MPI          7 2754.605469            0 
            2           128           128   gramschmidt           MPI          8 2757.490234            0 
            2           128           128   gramschmidt           MPI          9 2768.839844            0 
# Runtime: 4.032400 s (overhead: 0.000050 %) 10 records
