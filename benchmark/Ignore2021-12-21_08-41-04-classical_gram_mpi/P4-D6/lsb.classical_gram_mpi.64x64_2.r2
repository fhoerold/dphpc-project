# Sysname : Linux
# Nodename: eu-a6-006-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:43:06 2021
# Execution time and date (local): Tue Dec 21 08:43:06 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 1313.916016            0 
            2            64            64   gramschmidt           MPI          1 1328.884766            0 
            2            64            64   gramschmidt           MPI          2 1430.523438            0 
            2            64            64   gramschmidt           MPI          3 1394.132812            0 
            2            64            64   gramschmidt           MPI          4 1374.357422            0 
            2            64            64   gramschmidt           MPI          5 1389.033203            0 
            2            64            64   gramschmidt           MPI          6 1404.388672            0 
            2            64            64   gramschmidt           MPI          7 1383.246094            0 
            2            64            64   gramschmidt           MPI          8 1372.240234            0 
            2            64            64   gramschmidt           MPI          9 1491.761719            0 
# Runtime: 0.021068 s (overhead: 0.000000 %) 10 records
