# Sysname : Linux
# Nodename: eu-a6-006-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:43:06 2021
# Execution time and date (local): Tue Dec 21 08:43:06 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 1398.976562            0 
            3            64            64   gramschmidt           MPI          1 1402.591797            6 
            3            64            64   gramschmidt           MPI          2 1510.910156            0 
            3            64            64   gramschmidt           MPI          3 1476.187500            0 
            3            64            64   gramschmidt           MPI          4 1459.060547            0 
            3            64            64   gramschmidt           MPI          5 1470.121094            0 
            3            64            64   gramschmidt           MPI          6 1485.755859            0 
            3            64            64   gramschmidt           MPI          7 1467.396484            0 
            3            64            64   gramschmidt           MPI          8 1456.830078            0 
            3            64            64   gramschmidt           MPI          9 1552.054688            0 
# Runtime: 1.020785 s (overhead: 0.000588 %) 10 records
