# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:46:54 2021
# Execution time and date (local): Tue Dec 21 08:46:54 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 436.798828            0 
            2            32            32   gramschmidt           MPI          1 405.494141            2 
            2            32            32   gramschmidt           MPI          2 395.900391            0 
            2            32            32   gramschmidt           MPI          3 399.960938            0 
            2            32            32   gramschmidt           MPI          4 390.062500            0 
            2            32            32   gramschmidt           MPI          5 415.841797            0 
            2            32            32   gramschmidt           MPI          6 386.900391            0 
            2            32            32   gramschmidt           MPI          7 379.550781            0 
            2            32            32   gramschmidt           MPI          8 395.029297            0 
            2            32            32   gramschmidt           MPI          9 376.335938            0 
# Runtime: 6.009233 s (overhead: 0.000033 %) 10 records
