# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:46:54 2021
# Execution time and date (local): Tue Dec 21 08:46:54 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 443.738281            0 
            3            32            32   gramschmidt           MPI          1 388.250000            1 
            3            32            32   gramschmidt           MPI          2 383.201172            0 
            3            32            32   gramschmidt           MPI          3 389.570312            0 
            3            32            32   gramschmidt           MPI          4 386.072266            0 
            3            32            32   gramschmidt           MPI          5 402.761719            0 
            3            32            32   gramschmidt           MPI          6 379.714844            0 
            3            32            32   gramschmidt           MPI          7 372.015625            0 
            3            32            32   gramschmidt           MPI          8 386.914062            0 
            3            32            32   gramschmidt           MPI          9 372.814453            0 
# Runtime: 5.001076 s (overhead: 0.000020 %) 10 records
