# Sysname : Linux
# Nodename: eu-a6-006-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:43:16 2021
# Execution time and date (local): Tue Dec 21 08:43:16 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 634898.212891            0 
            3           512           512   gramschmidt           MPI          1 609554.335938            7 
            3           512           512   gramschmidt           MPI          2 621394.626953            2 
            3           512           512   gramschmidt           MPI          3 639365.089844            0 
            3           512           512   gramschmidt           MPI          4 593343.054688            2 
            3           512           512   gramschmidt           MPI          5 609673.503906            0 
            3           512           512   gramschmidt           MPI          6 606259.507812            0 
            3           512           512   gramschmidt           MPI          7 611971.490234            0 
            3           512           512   gramschmidt           MPI          8 590611.261719            6 
            3           512           512   gramschmidt           MPI          9 583910.505859            0 
# Runtime: 10.803096 s (overhead: 0.000157 %) 10 records
