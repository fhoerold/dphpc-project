# Sysname : Linux
# Nodename: eu-a6-006-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:43:16 2021
# Execution time and date (local): Tue Dec 21 08:43:16 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 634772.599609            4 
            2           512           512   gramschmidt           MPI          1 609442.128906            3 
            2           512           512   gramschmidt           MPI          2 621252.886719            2 
            2           512           512   gramschmidt           MPI          3 639191.742188            0 
            2           512           512   gramschmidt           MPI          4 593227.066406            2 
            2           512           512   gramschmidt           MPI          5 609556.175781            0 
            2           512           512   gramschmidt           MPI          6 606143.294922            0 
            2           512           512   gramschmidt           MPI          7 611862.527344            0 
            2           512           512   gramschmidt           MPI          8 590496.062500            7 
            2           512           512   gramschmidt           MPI          9 583804.234375            0 
# Runtime: 7.805770 s (overhead: 0.000231 %) 10 records
