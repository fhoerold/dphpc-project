# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:44:56 2021
# Execution time and date (local): Tue Dec 21 16:44:56 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 1088.154297            0 
            3            64            64   gramschmidt           MPI          1 1169.066406            2 
            3            64            64   gramschmidt           MPI          2 1040.000000            0 
            3            64            64   gramschmidt           MPI          3 1069.501953            0 
            3            64            64   gramschmidt           MPI          4 1010.810547            0 
            3            64            64   gramschmidt           MPI          5 991.712891            0 
            3            64            64   gramschmidt           MPI          6 998.150391            0 
            3            64            64   gramschmidt           MPI          7 1029.669922            0 
            3            64            64   gramschmidt           MPI          8 1018.091797            0 
            3            64            64   gramschmidt           MPI          9 1021.226562            0 
# Runtime: 10.026386 s (overhead: 0.000020 %) 10 records
