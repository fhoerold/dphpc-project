# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:44:56 2021
# Execution time and date (local): Tue Dec 21 16:44:56 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 1094.488281            0 
            2            64            64   gramschmidt           MPI          1 1169.884766            3 
            2            64            64   gramschmidt           MPI          2 1041.398438            0 
            2            64            64   gramschmidt           MPI          3 1071.068359            0 
            2            64            64   gramschmidt           MPI          4 1010.427734            0 
            2            64            64   gramschmidt           MPI          5 991.314453            0 
            2            64            64   gramschmidt           MPI          6 998.457031            0 
            2            64            64   gramschmidt           MPI          7 1030.369141            0 
            2            64            64   gramschmidt           MPI          8 1041.230469            0 
            2            64            64   gramschmidt           MPI          9 1020.251953            0 
# Runtime: 9.027568 s (overhead: 0.000033 %) 10 records
