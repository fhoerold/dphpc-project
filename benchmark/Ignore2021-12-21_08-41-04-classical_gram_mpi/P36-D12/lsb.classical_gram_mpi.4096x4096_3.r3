# Sysname : Linux
# Nodename: eu-a6-002-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 11:14:54 2021
# Execution time and date (local): Thu Dec 23 12:14:54 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 42229575.970703            0 
            3          4096          4096   gramschmidt           MPI          1 42244756.275391            6 
            3          4096          4096   gramschmidt           MPI          2 42120858.960938            2 
            3          4096          4096   gramschmidt           MPI          3 42319423.589844            0 
            3          4096          4096   gramschmidt           MPI          4 42262786.046875            4 
            3          4096          4096   gramschmidt           MPI          5 42104715.134766            0 
            3          4096          4096   gramschmidt           MPI          6 42390080.433594            0 
            3          4096          4096   gramschmidt           MPI          7 42194128.955078            0 
            3          4096          4096   gramschmidt           MPI          8 42273761.589844            6 
            3          4096          4096   gramschmidt           MPI          9 42198300.982422            0 
# Runtime: 553.334127 s (overhead: 0.000003 %) 10 records
