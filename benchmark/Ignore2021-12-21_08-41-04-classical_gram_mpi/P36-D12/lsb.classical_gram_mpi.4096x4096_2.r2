# Sysname : Linux
# Nodename: eu-a6-002-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 11:14:54 2021
# Execution time and date (local): Thu Dec 23 12:14:54 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 42225075.375000            0 
            2          4096          4096   gramschmidt           MPI          1 42240524.562500            7 
            2          4096          4096   gramschmidt           MPI          2 42116293.492188            2 
            2          4096          4096   gramschmidt           MPI          3 42314966.210938            0 
            2          4096          4096   gramschmidt           MPI          4 42258305.271484            4 
            2          4096          4096   gramschmidt           MPI          5 42100391.521484            0 
            2          4096          4096   gramschmidt           MPI          6 42385615.726562            0 
            2          4096          4096   gramschmidt           MPI          7 42189639.664062            0 
            2          4096          4096   gramschmidt           MPI          8 42269507.775391            7 
            2          4096          4096   gramschmidt           MPI          9 42193760.597656            0 
# Runtime: 555.361901 s (overhead: 0.000004 %) 10 records
