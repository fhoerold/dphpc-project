# Sysname : Linux
# Nodename: eu-a6-012-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 07:06:19 2021
# Execution time and date (local): Wed Dec 22 08:06:19 2021
# MPI execution on rank 2 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          8192          8192   gramschmidt           MPI          0 491086066.515625            2 
            2          8192          8192   gramschmidt           MPI          1 489411310.964844            5 
            2          8192          8192   gramschmidt           MPI          2 489431610.121094            4 
            2          8192          8192   gramschmidt           MPI          3 489610837.875000            0 
            2          8192          8192   gramschmidt           MPI          4 489559177.509766            4 
            2          8192          8192   gramschmidt           MPI          5 494892707.257812            0 
            2          8192          8192   gramschmidt           MPI          6 496316466.117188            0 
            2          8192          8192   gramschmidt           MPI          7 490099915.169922            0 
            2          8192          8192   gramschmidt           MPI          8 489316191.322266            7 
            2          8192          8192   gramschmidt           MPI          9 492007731.632812            2 
# Runtime: 6409.794372 s (overhead: 0.000000 %) 10 records
