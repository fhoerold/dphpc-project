# Sysname : Linux
# Nodename: eu-a6-012-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 07:06:19 2021
# Execution time and date (local): Wed Dec 22 08:06:19 2021
# MPI execution on rank 3 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          8192          8192   gramschmidt           MPI          0 491066417.783203            3 
            3          8192          8192   gramschmidt           MPI          1 489392025.072266            7 
            3          8192          8192   gramschmidt           MPI          2 489412168.662109            7 
            3          8192          8192   gramschmidt           MPI          3 489591268.599609            0 
            3          8192          8192   gramschmidt           MPI          4 489539931.603516            4 
            3          8192          8192   gramschmidt           MPI          5 494872993.509766            0 
            3          8192          8192   gramschmidt           MPI          6 496296955.646484            0 
            3          8192          8192   gramschmidt           MPI          7 490080507.855469            0 
            3          8192          8192   gramschmidt           MPI          8 489296876.957031            8 
            3          8192          8192   gramschmidt           MPI          9 491988367.050781            1 
# Runtime: 6409.155916 s (overhead: 0.000000 %) 10 records
