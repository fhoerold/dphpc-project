# Sysname : Linux
# Nodename: eu-a6-003-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 16:00:46 2021
# Execution time and date (local): Tue Dec 21 17:00:46 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 9787835.738281            0 
            2          2048          2048   gramschmidt           MPI          1 9786123.542969            6 
            2          2048          2048   gramschmidt           MPI          2 9780621.412109            2 
            2          2048          2048   gramschmidt           MPI          3 9771892.236328            0 
            2          2048          2048   gramschmidt           MPI          4 9792468.494141            2 
            2          2048          2048   gramschmidt           MPI          5 9800225.595703            0 
            2          2048          2048   gramschmidt           MPI          6 9769633.800781            0 
            2          2048          2048   gramschmidt           MPI          7 9819359.902344            0 
            2          2048          2048   gramschmidt           MPI          8 9789623.048828            8 
            2          2048          2048   gramschmidt           MPI          9 9837929.828125            0 
# Runtime: 137.564428 s (overhead: 0.000013 %) 10 records
