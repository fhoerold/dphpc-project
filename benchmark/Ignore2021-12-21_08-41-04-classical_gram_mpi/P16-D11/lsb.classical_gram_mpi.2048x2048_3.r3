# Sysname : Linux
# Nodename: eu-a6-003-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 16:00:46 2021
# Execution time and date (local): Tue Dec 21 17:00:46 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 9786905.558594            0 
            3          2048          2048   gramschmidt           MPI          1 9785088.300781            5 
            3          2048          2048   gramschmidt           MPI          2 9779689.416016            3 
            3          2048          2048   gramschmidt           MPI          3 9770846.296875            0 
            3          2048          2048   gramschmidt           MPI          4 9791384.576172            2 
            3          2048          2048   gramschmidt           MPI          5 9799285.972656            0 
            3          2048          2048   gramschmidt           MPI          6 9768547.078125            0 
            3          2048          2048   gramschmidt           MPI          7 9818267.001953            0 
            3          2048          2048   gramschmidt           MPI          8 9788535.761719            6 
            3          2048          2048   gramschmidt           MPI          9 9836887.105469            0 
# Runtime: 128.510022 s (overhead: 0.000012 %) 10 records
