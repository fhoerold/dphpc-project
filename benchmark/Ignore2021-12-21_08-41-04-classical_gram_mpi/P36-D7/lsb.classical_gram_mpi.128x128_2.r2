# Sysname : Linux
# Nodename: eu-a6-004-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 22:54:05 2021
# Execution time and date (local): Wed Dec 22 23:54:05 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 6412.220703            0 
            2           128           128   gramschmidt           MPI          1 5951.451172            0 
            2           128           128   gramschmidt           MPI          2 5997.394531            0 
            2           128           128   gramschmidt           MPI          3 5875.775391            0 
            2           128           128   gramschmidt           MPI          4 5894.296875            0 
            2           128           128   gramschmidt           MPI          5 5946.763672            0 
            2           128           128   gramschmidt           MPI          6 5897.855469            0 
            2           128           128   gramschmidt           MPI          7 5915.941406            0 
            2           128           128   gramschmidt           MPI          8 5917.371094            0 
            2           128           128   gramschmidt           MPI          9 5866.392578            0 
# Runtime: 0.081695 s (overhead: 0.000000 %) 10 records
