# Sysname : Linux
# Nodename: eu-a6-004-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 22:54:05 2021
# Execution time and date (local): Wed Dec 22 23:54:05 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 6400.984375            0 
            3           128           128   gramschmidt           MPI          1 5954.884766            3 
            3           128           128   gramschmidt           MPI          2 5997.714844            0 
            3           128           128   gramschmidt           MPI          3 5876.984375            0 
            3           128           128   gramschmidt           MPI          4 5890.250000            0 
            3           128           128   gramschmidt           MPI          5 5944.164062            0 
            3           128           128   gramschmidt           MPI          6 5895.789062            0 
            3           128           128   gramschmidt           MPI          7 5911.617188            0 
            3           128           128   gramschmidt           MPI          8 5916.757812            0 
            3           128           128   gramschmidt           MPI          9 5868.339844            0 
# Runtime: 7.040244 s (overhead: 0.000043 %) 10 records
