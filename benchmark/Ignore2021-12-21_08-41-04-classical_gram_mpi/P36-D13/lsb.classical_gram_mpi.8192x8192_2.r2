# Sysname : Linux
# Nodename: eu-a6-002-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 01:11:11 2021
# Execution time and date (local): Thu Dec 23 02:11:11 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          8192          8192   gramschmidt           MPI          0 519034173.382812            0 
            2          8192          8192   gramschmidt           MPI          1 519640171.708984            6 
            2          8192          8192   gramschmidt           MPI          2 519258359.513672            4 
            2          8192          8192   gramschmidt           MPI          3 521369292.326172            0 
            2          8192          8192   gramschmidt           MPI          4 520709273.052734            6 
            2          8192          8192   gramschmidt           MPI          5 520548545.037109            2 
            2          8192          8192   gramschmidt           MPI          6 519804013.500000            2 
            2          8192          8192   gramschmidt           MPI          7 520793633.951172            2 
            2          8192          8192   gramschmidt           MPI          8 519525238.203125            9 
            2          8192          8192   gramschmidt           MPI          9 521955687.623047            2 
# Runtime: 6778.251786 s (overhead: 0.000000 %) 10 records
