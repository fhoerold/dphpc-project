# Sysname : Linux
# Nodename: eu-a6-002-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 01:11:11 2021
# Execution time and date (local): Thu Dec 23 02:11:11 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          8192          8192   gramschmidt           MPI          0 519545173.179688            2 
            3          8192          8192   gramschmidt           MPI          1 520151481.861328            4 
            3          8192          8192   gramschmidt           MPI          2 519769241.015625           11 
            3          8192          8192   gramschmidt           MPI          3 521882259.146484            0 
            3          8192          8192   gramschmidt           MPI          4 521221882.533203            6 
            3          8192          8192   gramschmidt           MPI          5 521060937.283203            1 
            3          8192          8192   gramschmidt           MPI          6 520315459.826172            2 
            3          8192          8192   gramschmidt           MPI          7 521306315.000000            2 
            3          8192          8192   gramschmidt           MPI          8 520036414.080078            7 
            3          8192          8192   gramschmidt           MPI          9 522469289.677734            2 
# Runtime: 6786.569006 s (overhead: 0.000001 %) 10 records
