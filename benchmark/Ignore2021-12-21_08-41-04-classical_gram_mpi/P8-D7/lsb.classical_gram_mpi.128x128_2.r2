# Sysname : Linux
# Nodename: eu-a6-012-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 09:06:38 2021
# Execution time and date (local): Tue Dec 21 10:06:38 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 3300.615234            0 
            2           128           128   gramschmidt           MPI          1 3238.759766            3 
            2           128           128   gramschmidt           MPI          2 3229.005859            0 
            2           128           128   gramschmidt           MPI          3 3238.460938            0 
            2           128           128   gramschmidt           MPI          4 3217.222656            0 
            2           128           128   gramschmidt           MPI          5 3199.208984            0 
            2           128           128   gramschmidt           MPI          6 3208.927734            0 
            2           128           128   gramschmidt           MPI          7 3217.250000            0 
            2           128           128   gramschmidt           MPI          8 3227.519531            0 
            2           128           128   gramschmidt           MPI          9 3190.128906            0 
# Runtime: 9.015799 s (overhead: 0.000033 %) 10 records
