# Sysname : Linux
# Nodename: eu-a6-012-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 09:06:38 2021
# Execution time and date (local): Tue Dec 21 10:06:38 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 3293.056641            0 
            3           128           128   gramschmidt           MPI          1 3231.375000            3 
            3           128           128   gramschmidt           MPI          2 3236.548828            0 
            3           128           128   gramschmidt           MPI          3 3228.626953            0 
            3           128           128   gramschmidt           MPI          4 3215.056641            0 
            3           128           128   gramschmidt           MPI          5 3190.988281            0 
            3           128           128   gramschmidt           MPI          6 3204.871094            0 
            3           128           128   gramschmidt           MPI          7 3215.386719            0 
            3           128           128   gramschmidt           MPI          8 3218.482422            0 
            3           128           128   gramschmidt           MPI          9 3181.156250            0 
# Runtime: 6.043158 s (overhead: 0.000050 %) 10 records
