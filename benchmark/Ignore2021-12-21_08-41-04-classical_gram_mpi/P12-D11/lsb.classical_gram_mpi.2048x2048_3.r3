# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:48:20 2021
# Execution time and date (local): Tue Dec 21 08:48:20 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 10820232.062500            0 
            3          2048          2048   gramschmidt           MPI          1 10806230.728516            5 
            3          2048          2048   gramschmidt           MPI          2 10817733.398438            4 
            3          2048          2048   gramschmidt           MPI          3 10842835.957031            0 
            3          2048          2048   gramschmidt           MPI          4 10827348.734375            2 
            3          2048          2048   gramschmidt           MPI          5 10806144.169922            0 
            3          2048          2048   gramschmidt           MPI          6 10821173.609375            0 
            3          2048          2048   gramschmidt           MPI          7 10803475.511719            0 
            3          2048          2048   gramschmidt           MPI          8 10826055.628906            5 
            3          2048          2048   gramschmidt           MPI          9 10808010.894531            0 
# Runtime: 140.553325 s (overhead: 0.000011 %) 10 records
