# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:48:20 2021
# Execution time and date (local): Tue Dec 21 08:48:20 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 10817915.453125            0 
            2          2048          2048   gramschmidt           MPI          1 10804115.416016            4 
            2          2048          2048   gramschmidt           MPI          2 10815592.902344            2 
            2          2048          2048   gramschmidt           MPI          3 10840492.492188            0 
            2          2048          2048   gramschmidt           MPI          4 10825222.908203            5 
            2          2048          2048   gramschmidt           MPI          5 10803976.146484            0 
            2          2048          2048   gramschmidt           MPI          6 10819054.837891            0 
            2          2048          2048   gramschmidt           MPI          7 10801364.054688            0 
            2          2048          2048   gramschmidt           MPI          8 10823741.779297            3 
            2          2048          2048   gramschmidt           MPI          9 10805834.273438            0 
# Runtime: 155.550964 s (overhead: 0.000009 %) 10 records
