# Sysname : Linux
# Nodename: eu-a6-009-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 19:51:01 2021
# Execution time and date (local): Tue Dec 21 20:51:01 2021
# MPI execution on rank 3 with 24 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 606754.589844            0 
            3          1024          1024   gramschmidt           MPI          1 606795.769531            4 
            3          1024          1024   gramschmidt           MPI          2 603742.390625            1 
            3          1024          1024   gramschmidt           MPI          3 608847.359375            0 
            3          1024          1024   gramschmidt           MPI          4 610147.925781            1 
            3          1024          1024   gramschmidt           MPI          5 607345.060547            0 
            3          1024          1024   gramschmidt           MPI          6 611299.621094            0 
            3          1024          1024   gramschmidt           MPI          7 609251.406250            0 
            3          1024          1024   gramschmidt           MPI          8 612395.677734            5 
            3          1024          1024   gramschmidt           MPI          9 604688.289062            0 
# Runtime: 14.971154 s (overhead: 0.000073 %) 10 records
