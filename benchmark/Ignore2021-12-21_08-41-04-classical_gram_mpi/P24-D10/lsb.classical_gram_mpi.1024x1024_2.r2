# Sysname : Linux
# Nodename: eu-a6-009-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 19:51:01 2021
# Execution time and date (local): Tue Dec 21 20:51:01 2021
# MPI execution on rank 2 with 24 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 607193.869141            0 
            2          1024          1024   gramschmidt           MPI          1 607172.363281            4 
            2          1024          1024   gramschmidt           MPI          2 604125.478516            1 
            2          1024          1024   gramschmidt           MPI          3 609227.615234            0 
            2          1024          1024   gramschmidt           MPI          4 610535.474609            2 
            2          1024          1024   gramschmidt           MPI          5 607700.386719            0 
            2          1024          1024   gramschmidt           MPI          6 611688.658203            0 
            2          1024          1024   gramschmidt           MPI          7 609639.904297            0 
            2          1024          1024   gramschmidt           MPI          8 612789.734375            4 
            2          1024          1024   gramschmidt           MPI          9 605071.414062            0 
# Runtime: 14.000256 s (overhead: 0.000079 %) 10 records
