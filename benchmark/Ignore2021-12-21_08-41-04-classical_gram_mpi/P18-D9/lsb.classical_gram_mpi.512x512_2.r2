# Sysname : Linux
# Nodename: eu-a6-001-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 14:36:18 2021
# Execution time and date (local): Wed Dec 22 15:36:18 2021
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 88788.587891            0 
            2           512           512   gramschmidt           MPI          1 88014.074219            3 
            2           512           512   gramschmidt           MPI          2 87755.482422            1 
            2           512           512   gramschmidt           MPI          3 87804.886719            0 
            2           512           512   gramschmidt           MPI          4 87653.000000            1 
            2           512           512   gramschmidt           MPI          5 87856.662109            0 
            2           512           512   gramschmidt           MPI          6 90712.181641            0 
            2           512           512   gramschmidt           MPI          7 87992.345703            0 
            2           512           512   gramschmidt           MPI          8 87948.466797            2 
            2           512           512   gramschmidt           MPI          9 91858.273438            0 
# Runtime: 16.179976 s (overhead: 0.000043 %) 10 records
