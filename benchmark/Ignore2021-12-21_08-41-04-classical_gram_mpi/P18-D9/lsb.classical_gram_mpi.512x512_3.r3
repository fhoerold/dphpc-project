# Sysname : Linux
# Nodename: eu-a6-001-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 14:36:18 2021
# Execution time and date (local): Wed Dec 22 15:36:18 2021
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 88525.660156            0 
            3           512           512   gramschmidt           MPI          1 88000.564453            4 
            3           512           512   gramschmidt           MPI          2 87721.142578            1 
            3           512           512   gramschmidt           MPI          3 87803.623047            0 
            3           512           512   gramschmidt           MPI          4 87661.699219            1 
            3           512           512   gramschmidt           MPI          5 87852.652344            0 
            3           512           512   gramschmidt           MPI          6 90334.732422            0 
            3           512           512   gramschmidt           MPI          7 87992.583984            0 
            3           512           512   gramschmidt           MPI          8 87944.052734            1 
            3           512           512   gramschmidt           MPI          9 91845.593750            0 
# Runtime: 16.171625 s (overhead: 0.000043 %) 10 records
