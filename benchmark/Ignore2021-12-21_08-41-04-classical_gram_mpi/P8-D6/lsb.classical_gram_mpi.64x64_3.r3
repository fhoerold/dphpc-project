# Sysname : Linux
# Nodename: eu-a6-012-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 09:06:29 2021
# Execution time and date (local): Tue Dec 21 10:06:29 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 846.187500            0 
            3            64            64   gramschmidt           MPI          1 821.367188            2 
            3            64            64   gramschmidt           MPI          2 848.238281            0 
            3            64            64   gramschmidt           MPI          3 826.498047            0 
            3            64            64   gramschmidt           MPI          4 864.416016            0 
            3            64            64   gramschmidt           MPI          5 815.097656            0 
            3            64            64   gramschmidt           MPI          6 828.054688            0 
            3            64            64   gramschmidt           MPI          7 871.630859            0 
            3            64            64   gramschmidt           MPI          8 810.160156            0 
            3            64            64   gramschmidt           MPI          9 828.425781            0 
# Runtime: 1.011974 s (overhead: 0.000198 %) 10 records
