# Sysname : Linux
# Nodename: eu-a6-012-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 09:06:29 2021
# Execution time and date (local): Tue Dec 21 10:06:29 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 856.468750            0 
            2            64            64   gramschmidt           MPI          1 827.625000            2 
            2            64            64   gramschmidt           MPI          2 853.259766            0 
            2            64            64   gramschmidt           MPI          3 836.253906            0 
            2            64            64   gramschmidt           MPI          4 873.462891            0 
            2            64            64   gramschmidt           MPI          5 819.388672            0 
            2            64            64   gramschmidt           MPI          6 837.720703            0 
            2            64            64   gramschmidt           MPI          7 873.154297            0 
            2            64            64   gramschmidt           MPI          8 811.908203            0 
            2            64            64   gramschmidt           MPI          9 829.451172            0 
# Runtime: 1.030216 s (overhead: 0.000194 %) 10 records
