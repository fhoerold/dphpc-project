# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:47:21 2021
# Execution time and date (local): Tue Dec 21 08:47:21 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 16194.593750            0 
            3           256           256   gramschmidt           MPI          1 15968.140625            2 
            3           256           256   gramschmidt           MPI          2 15862.117188            0 
            3           256           256   gramschmidt           MPI          3 15825.923828            0 
            3           256           256   gramschmidt           MPI          4 16273.808594            0 
            3           256           256   gramschmidt           MPI          5 18202.875000            0 
            3           256           256   gramschmidt           MPI          6 15856.134766            0 
            3           256           256   gramschmidt           MPI          7 15799.566406            0 
            3           256           256   gramschmidt           MPI          8 15824.753906            0 
            3           256           256   gramschmidt           MPI          9 15763.025391            0 
# Runtime: 6.224260 s (overhead: 0.000032 %) 10 records
