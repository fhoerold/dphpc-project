# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:47:21 2021
# Execution time and date (local): Tue Dec 21 08:47:21 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 16552.636719            0 
            2           256           256   gramschmidt           MPI          1 15971.855469            3 
            2           256           256   gramschmidt           MPI          2 15872.050781            0 
            2           256           256   gramschmidt           MPI          3 15833.500000            0 
            2           256           256   gramschmidt           MPI          4 16252.970703            0 
            2           256           256   gramschmidt           MPI          5 18211.201172            0 
            2           256           256   gramschmidt           MPI          6 15854.013672            0 
            2           256           256   gramschmidt           MPI          7 15804.623047            0 
            2           256           256   gramschmidt           MPI          8 15840.394531            0 
            2           256           256   gramschmidt           MPI          9 15772.517578            0 
# Runtime: 9.226513 s (overhead: 0.000033 %) 10 records
