# Sysname : Linux
# Nodename: eu-a6-007-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 05:36:08 2021
# Execution time and date (local): Wed Dec 22 06:36:08 2021
# MPI execution on rank 3 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 64025.322266            0 
            3           512           512   gramschmidt           MPI          1 68028.273438            4 
            3           512           512   gramschmidt           MPI          2 63106.289062            1 
            3           512           512   gramschmidt           MPI          3 63051.291016            0 
            3           512           512   gramschmidt           MPI          4 63094.396484            1 
            3           512           512   gramschmidt           MPI          5 63075.511719            0 
            3           512           512   gramschmidt           MPI          6 62797.150391            0 
            3           512           512   gramschmidt           MPI          7 63127.986328            0 
            3           512           512   gramschmidt           MPI          8 63094.367188            1 
            3           512           512   gramschmidt           MPI          9 63024.671875            0 
# Runtime: 4.884591 s (overhead: 0.000143 %) 10 records
