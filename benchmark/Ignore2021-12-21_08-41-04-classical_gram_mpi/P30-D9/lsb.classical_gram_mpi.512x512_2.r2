# Sysname : Linux
# Nodename: eu-a6-007-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 05:36:08 2021
# Execution time and date (local): Wed Dec 22 06:36:08 2021
# MPI execution on rank 2 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 64230.667969            0 
            2           512           512   gramschmidt           MPI          1 68032.951172            4 
            2           512           512   gramschmidt           MPI          2 63118.818359            1 
            2           512           512   gramschmidt           MPI          3 63073.785156            0 
            2           512           512   gramschmidt           MPI          4 63104.552734            1 
            2           512           512   gramschmidt           MPI          5 63084.380859            0 
            2           512           512   gramschmidt           MPI          6 62806.408203            0 
            2           512           512   gramschmidt           MPI          7 63132.554688            0 
            2           512           512   gramschmidt           MPI          8 63114.576172            1 
            2           512           512   gramschmidt           MPI          9 63037.945312            0 
# Runtime: 9.886608 s (overhead: 0.000071 %) 10 records
