# Sysname : Linux
# Nodename: eu-a6-009-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 19:35:34 2021
# Execution time and date (local): Tue Dec 21 20:35:34 2021
# MPI execution on rank 2 with 24 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 80714.802734            0 
            2           512           512   gramschmidt           MPI          1 80838.433594            4 
            2           512           512   gramschmidt           MPI          2 80475.244141            1 
            2           512           512   gramschmidt           MPI          3 80371.562500            0 
            2           512           512   gramschmidt           MPI          4 80167.511719            1 
            2           512           512   gramschmidt           MPI          5 80312.740234            0 
            2           512           512   gramschmidt           MPI          6 80305.318359            0 
            2           512           512   gramschmidt           MPI          7 80479.691406            0 
            2           512           512   gramschmidt           MPI          8 80226.169922            1 
            2           512           512   gramschmidt           MPI          9 79831.767578            0 
# Runtime: 9.067419 s (overhead: 0.000077 %) 10 records
