# Sysname : Linux
# Nodename: eu-a6-009-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 19:35:34 2021
# Execution time and date (local): Tue Dec 21 20:35:34 2021
# MPI execution on rank 3 with 24 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 80653.279297            0 
            3           512           512   gramschmidt           MPI          1 80752.216797            5 
            3           512           512   gramschmidt           MPI          2 80405.445312            1 
            3           512           512   gramschmidt           MPI          3 80307.667969            0 
            3           512           512   gramschmidt           MPI          4 80108.138672            1 
            3           512           512   gramschmidt           MPI          5 80222.474609            0 
            3           512           512   gramschmidt           MPI          6 80219.365234            0 
            3           512           512   gramschmidt           MPI          7 80408.052734            0 
            3           512           512   gramschmidt           MPI          8 80137.855469            1 
            3           512           512   gramschmidt           MPI          9 79732.693359            4 
# Runtime: 12.057289 s (overhead: 0.000100 %) 10 records
