# Sysname : Linux
# Nodename: eu-a6-002-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 11:12:38 2021
# Execution time and date (local): Thu Dec 23 12:12:38 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 2226.328125            0 
            3            64            64   gramschmidt           MPI          1 2155.720703            2 
            3            64            64   gramschmidt           MPI          2 2143.236328            0 
            3            64            64   gramschmidt           MPI          3 2153.341797            0 
            3            64            64   gramschmidt           MPI          4 2137.455078            0 
            3            64            64   gramschmidt           MPI          5 2134.236328            0 
            3            64            64   gramschmidt           MPI          6 2145.480469            0 
            3            64            64   gramschmidt           MPI          7 2118.621094            0 
            3            64            64   gramschmidt           MPI          8 2091.533203            0 
            3            64            64   gramschmidt           MPI          9 2109.759766            0 
# Runtime: 10.047182 s (overhead: 0.000020 %) 10 records
