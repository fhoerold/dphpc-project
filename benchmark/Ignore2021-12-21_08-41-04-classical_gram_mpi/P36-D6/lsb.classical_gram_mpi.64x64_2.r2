# Sysname : Linux
# Nodename: eu-a6-002-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 11:12:38 2021
# Execution time and date (local): Thu Dec 23 12:12:38 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 2263.644531            0 
            2            64            64   gramschmidt           MPI          1 2205.052734            2 
            2            64            64   gramschmidt           MPI          2 2192.646484            0 
            2            64            64   gramschmidt           MPI          3 2195.791016            0 
            2            64            64   gramschmidt           MPI          4 2174.294922            0 
            2            64            64   gramschmidt           MPI          5 2177.542969            0 
            2            64            64   gramschmidt           MPI          6 2183.832031            0 
            2            64            64   gramschmidt           MPI          7 2157.095703            0 
            2            64            64   gramschmidt           MPI          8 2117.580078            0 
            2            64            64   gramschmidt           MPI          9 2145.976562            0 
# Runtime: 12.039934 s (overhead: 0.000017 %) 10 records
