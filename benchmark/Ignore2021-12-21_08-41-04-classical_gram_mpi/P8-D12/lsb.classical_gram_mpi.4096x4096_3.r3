# Sysname : Linux
# Nodename: eu-a6-007-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 09:08:43 2021
# Execution time and date (local): Tue Dec 21 10:08:43 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 181969111.062500            0 
            3          4096          4096   gramschmidt           MPI          1 181725779.896484            4 
            3          4096          4096   gramschmidt           MPI          2 182368337.921875            4 
            3          4096          4096   gramschmidt           MPI          3 183543031.509766            0 
            3          4096          4096   gramschmidt           MPI          4 182706576.492188            4 
            3          4096          4096   gramschmidt           MPI          5 182274117.710938            0 
            3          4096          4096   gramschmidt           MPI          6 182370252.062500            0 
            3          4096          4096   gramschmidt           MPI          7 182037432.699219            0 
            3          4096          4096   gramschmidt           MPI          8 182204839.041016            5 
            3          4096          4096   gramschmidt           MPI          9 182191488.277344            0 
# Runtime: 2372.463793 s (overhead: 0.000001 %) 10 records
