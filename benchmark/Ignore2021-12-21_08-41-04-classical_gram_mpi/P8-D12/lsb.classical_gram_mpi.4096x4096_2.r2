# Sysname : Linux
# Nodename: eu-a6-007-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 09:08:43 2021
# Execution time and date (local): Tue Dec 21 10:08:43 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 182169046.140625            1 
            2          4096          4096   gramschmidt           MPI          1 181925218.277344            4 
            2          4096          4096   gramschmidt           MPI          2 182568836.982422            6 
            2          4096          4096   gramschmidt           MPI          3 183744661.789062            0 
            2          4096          4096   gramschmidt           MPI          4 182907350.216797            4 
            2          4096          4096   gramschmidt           MPI          5 182474494.986328            0 
            2          4096          4096   gramschmidt           MPI          6 182570651.363281            0 
            2          4096          4096   gramschmidt           MPI          7 182237542.283203            0 
            2          4096          4096   gramschmidt           MPI          8 182405153.462891            4 
            2          4096          4096   gramschmidt           MPI          9 182391733.599609            0 
# Runtime: 2375.211004 s (overhead: 0.000001 %) 10 records
