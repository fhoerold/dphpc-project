# Sysname : Linux
# Nodename: eu-a6-004-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 22:54:59 2021
# Execution time and date (local): Wed Dec 22 23:54:59 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 65148.984375            0 
            3           512           512   gramschmidt           MPI          1 68048.638672            5 
            3           512           512   gramschmidt           MPI          2 64858.445312            1 
            3           512           512   gramschmidt           MPI          3 64823.642578            0 
            3           512           512   gramschmidt           MPI          4 64894.699219            0 
            3           512           512   gramschmidt           MPI          5 64756.609375            0 
            3           512           512   gramschmidt           MPI          6 65027.845703            0 
            3           512           512   gramschmidt           MPI          7 65318.839844            0 
            3           512           512   gramschmidt           MPI          8 65116.525391            1 
            3           512           512   gramschmidt           MPI          9 66724.140625            0 
# Runtime: 10.873224 s (overhead: 0.000064 %) 10 records
