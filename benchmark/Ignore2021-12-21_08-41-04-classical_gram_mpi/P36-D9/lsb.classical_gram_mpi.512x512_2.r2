# Sysname : Linux
# Nodename: eu-a6-004-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 22:54:59 2021
# Execution time and date (local): Wed Dec 22 23:54:59 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 65244.617188            4 
            2           512           512   gramschmidt           MPI          1 68150.306641            4 
            2           512           512   gramschmidt           MPI          2 64958.468750            1 
            2           512           512   gramschmidt           MPI          3 64926.445312            0 
            2           512           512   gramschmidt           MPI          4 65003.513672            1 
            2           512           512   gramschmidt           MPI          5 64866.421875            0 
            2           512           512   gramschmidt           MPI          6 65142.125000            0 
            2           512           512   gramschmidt           MPI          7 65412.425781            0 
            2           512           512   gramschmidt           MPI          8 65214.900391            1 
            2           512           512   gramschmidt           MPI          9 66830.763672            0 
# Runtime: 10.896502 s (overhead: 0.000101 %) 10 records
