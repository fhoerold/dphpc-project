# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:45:26 2021
# Execution time and date (local): Tue Dec 21 16:45:26 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 12675.701172            0 
            3           256           256   gramschmidt           MPI          1 12673.951172            3 
            3           256           256   gramschmidt           MPI          2 12604.910156            0 
            3           256           256   gramschmidt           MPI          3 12486.257812            0 
            3           256           256   gramschmidt           MPI          4 12544.156250            0 
            3           256           256   gramschmidt           MPI          5 12961.427734            0 
            3           256           256   gramschmidt           MPI          6 12533.652344            0 
            3           256           256   gramschmidt           MPI          7 12426.240234            0 
            3           256           256   gramschmidt           MPI          8 12500.365234            0 
            3           256           256   gramschmidt           MPI          9 12506.007812            0 
# Runtime: 1.168870 s (overhead: 0.000257 %) 10 records
