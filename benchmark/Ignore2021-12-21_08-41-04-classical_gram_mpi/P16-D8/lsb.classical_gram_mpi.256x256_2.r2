# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:45:26 2021
# Execution time and date (local): Tue Dec 21 16:45:26 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 12675.980469            0 
            2           256           256   gramschmidt           MPI          1 12680.734375            3 
            2           256           256   gramschmidt           MPI          2 12606.283203            0 
            2           256           256   gramschmidt           MPI          3 12491.019531            0 
            2           256           256   gramschmidt           MPI          4 12541.593750            0 
            2           256           256   gramschmidt           MPI          5 12968.423828            0 
            2           256           256   gramschmidt           MPI          6 12539.082031            0 
            2           256           256   gramschmidt           MPI          7 12423.167969            0 
            2           256           256   gramschmidt           MPI          8 12502.337891            0 
            2           256           256   gramschmidt           MPI          9 12506.716797            0 
# Runtime: 7.166982 s (overhead: 0.000042 %) 10 records
