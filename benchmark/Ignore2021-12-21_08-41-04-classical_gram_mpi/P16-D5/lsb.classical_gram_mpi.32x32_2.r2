# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:44:39 2021
# Execution time and date (local): Tue Dec 21 16:44:39 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 499.906250            0 
            2            32            32   gramschmidt           MPI          1 465.423828            2 
            2            32            32   gramschmidt           MPI          2 450.611328            0 
            2            32            32   gramschmidt           MPI          3 444.669922            0 
            2            32            32   gramschmidt           MPI          4 486.742188            0 
            2            32            32   gramschmidt           MPI          5 423.447266            0 
            2            32            32   gramschmidt           MPI          6 427.992188            0 
            2            32            32   gramschmidt           MPI          7 422.453125            0 
            2            32            32   gramschmidt           MPI          8 428.492188            0 
            2            32            32   gramschmidt           MPI          9 439.302734            0 
# Runtime: 11.010803 s (overhead: 0.000018 %) 10 records
