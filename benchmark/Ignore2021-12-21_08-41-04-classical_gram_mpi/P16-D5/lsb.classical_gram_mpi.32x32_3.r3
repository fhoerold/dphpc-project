# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:44:39 2021
# Execution time and date (local): Tue Dec 21 16:44:39 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 497.175781            0 
            3            32            32   gramschmidt           MPI          1 463.394531            2 
            3            32            32   gramschmidt           MPI          2 443.033203            0 
            3            32            32   gramschmidt           MPI          3 442.328125            0 
            3            32            32   gramschmidt           MPI          4 481.767578            0 
            3            32            32   gramschmidt           MPI          5 419.343750            0 
            3            32            32   gramschmidt           MPI          6 425.277344            0 
            3            32            32   gramschmidt           MPI          7 417.986328            0 
            3            32            32   gramschmidt           MPI          8 423.679688            0 
            3            32            32   gramschmidt           MPI          9 430.615234            0 
# Runtime: 11.010856 s (overhead: 0.000018 %) 10 records
