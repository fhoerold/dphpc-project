# Sysname : Linux
# Nodename: eu-a6-007-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 05:36:45 2021
# Execution time and date (local): Wed Dec 22 06:36:45 2021
# MPI execution on rank 3 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 4994892.802734            0 
            3          2048          2048   gramschmidt           MPI          1 4945700.552734            5 
            3          2048          2048   gramschmidt           MPI          2 5015111.367188            2 
            3          2048          2048   gramschmidt           MPI          3 4948762.529297            0 
            3          2048          2048   gramschmidt           MPI          4 4953392.724609            2 
            3          2048          2048   gramschmidt           MPI          5 4958575.269531            0 
            3          2048          2048   gramschmidt           MPI          6 4964721.292969            0 
            3          2048          2048   gramschmidt           MPI          7 5031732.193359            0 
            3          2048          2048   gramschmidt           MPI          8 4970966.431641            7 
            3          2048          2048   gramschmidt           MPI          9 4979406.177734            0 
# Runtime: 76.463680 s (overhead: 0.000021 %) 10 records
