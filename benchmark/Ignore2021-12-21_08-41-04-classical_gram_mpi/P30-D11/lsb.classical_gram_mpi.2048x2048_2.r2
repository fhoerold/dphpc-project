# Sysname : Linux
# Nodename: eu-a6-007-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 05:36:45 2021
# Execution time and date (local): Wed Dec 22 06:36:45 2021
# MPI execution on rank 2 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 4994796.125000            0 
            2          2048          2048   gramschmidt           MPI          1 4945696.810547            4 
            2          2048          2048   gramschmidt           MPI          2 5015109.009766            2 
            2          2048          2048   gramschmidt           MPI          3 4948692.166016            0 
            2          2048          2048   gramschmidt           MPI          4 4953390.902344            2 
            2          2048          2048   gramschmidt           MPI          5 4958563.650391            0 
            2          2048          2048   gramschmidt           MPI          6 4964723.720703            0 
            2          2048          2048   gramschmidt           MPI          7 5031648.066406            0 
            2          2048          2048   gramschmidt           MPI          8 4970964.466797            6 
            2          2048          2048   gramschmidt           MPI          9 4979402.437500            0 
# Runtime: 76.481473 s (overhead: 0.000018 %) 10 records
