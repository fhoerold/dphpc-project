# Sysname : Linux
# Nodename: eu-a6-009-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 08:17:31 2021
# Execution time and date (local): Tue Dec 21 09:17:31 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 42939415.601562            0 
            2          2048          2048   gramschmidt           MPI          1 48426717.542969            6 
            2          2048          2048   gramschmidt           MPI          2 48091061.351562            5 
            2          2048          2048   gramschmidt           MPI          3 41449388.537109            0 
            2          2048          2048   gramschmidt           MPI          4 56926427.685547            7 
            2          2048          2048   gramschmidt           MPI          5 47319045.119141            0 
            2          2048          2048   gramschmidt           MPI          6 60669253.140625            0 
            2          2048          2048   gramschmidt           MPI          7 41169112.251953            0 
            2          2048          2048   gramschmidt           MPI          8 48728519.027344            6 
            2          2048          2048   gramschmidt           MPI          9 54517237.792969            0 
# Runtime: 653.973816 s (overhead: 0.000004 %) 10 records
