# Sysname : Linux
# Nodename: eu-a6-009-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 08:17:31 2021
# Execution time and date (local): Tue Dec 21 09:17:31 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 42943459.328125            0 
            3          2048          2048   gramschmidt           MPI          1 48431063.244141            5 
            3          2048          2048   gramschmidt           MPI          2 48094891.626953            3 
            3          2048          2048   gramschmidt           MPI          3 41453292.880859            0 
            3          2048          2048   gramschmidt           MPI          4 56931358.095703            4 
            3          2048          2048   gramschmidt           MPI          5 47323823.419922            1 
            3          2048          2048   gramschmidt           MPI          6 60674205.093750            0 
            3          2048          2048   gramschmidt           MPI          7 41172609.494141            0 
            3          2048          2048   gramschmidt           MPI          8 48732858.197266            9 
            3          2048          2048   gramschmidt           MPI          9 54521962.083984            0 
# Runtime: 663.980010 s (overhead: 0.000003 %) 10 records
