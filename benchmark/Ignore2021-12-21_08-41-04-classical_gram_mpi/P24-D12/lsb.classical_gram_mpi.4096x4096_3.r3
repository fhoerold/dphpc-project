# Sysname : Linux
# Nodename: eu-a6-004-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 16:55:36 2021
# Execution time and date (local): Wed Dec 22 17:55:36 2021
# MPI execution on rank 3 with 24 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 89814268.968750            2 
            3          4096          4096   gramschmidt           MPI          1 87927105.271484           14 
            3          4096          4096   gramschmidt           MPI          2 86555201.732422           12 
            3          4096          4096   gramschmidt           MPI          3 84782174.427734            3 
            3          4096          4096   gramschmidt           MPI          4 85417829.205078           16 
            3          4096          4096   gramschmidt           MPI          5 84579090.035156            4 
            3          4096          4096   gramschmidt           MPI          6 85140092.662109            6 
            3          4096          4096   gramschmidt           MPI          7 85558849.781250            5 
            3          4096          4096   gramschmidt           MPI          8 85452444.564453           17 
            3          4096          4096   gramschmidt           MPI          9 85246363.498047            2 
# Runtime: 1127.960350 s (overhead: 0.000007 %) 10 records
