# Sysname : Linux
# Nodename: eu-a6-004-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 16:55:36 2021
# Execution time and date (local): Wed Dec 22 17:55:36 2021
# MPI execution on rank 2 with 24 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 89785700.224609            3 
            2          4096          4096   gramschmidt           MPI          1 87898697.750000           17 
            2          4096          4096   gramschmidt           MPI          2 86527072.386719            5 
            2          4096          4096   gramschmidt           MPI          3 84753765.445312            0 
            2          4096          4096   gramschmidt           MPI          4 85389764.564453            5 
            2          4096          4096   gramschmidt           MPI          5 84551163.259766            0 
            2          4096          4096   gramschmidt           MPI          6 85111291.812500            0 
            2          4096          4096   gramschmidt           MPI          7 85530769.068359            0 
            2          4096          4096   gramschmidt           MPI          8 85424298.957031            6 
            2          4096          4096   gramschmidt           MPI          9 85217967.957031            0 
# Runtime: 1128.966332 s (overhead: 0.000003 %) 10 records
