# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:45:42 2021
# Execution time and date (local): Tue Dec 21 16:45:42 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 83166.843750            0 
            2           512           512   gramschmidt           MPI          1 79964.710938            3 
            2           512           512   gramschmidt           MPI          2 79794.248047            0 
            2           512           512   gramschmidt           MPI          3 79991.333984            0 
            2           512           512   gramschmidt           MPI          4 80334.511719            1 
            2           512           512   gramschmidt           MPI          5 79937.714844            0 
            2           512           512   gramschmidt           MPI          6 80431.378906            0 
            2           512           512   gramschmidt           MPI          7 83318.759766            0 
            2           512           512   gramschmidt           MPI          8 81017.183594            1 
            2           512           512   gramschmidt           MPI          9 80691.695312            0 
# Runtime: 4.076179 s (overhead: 0.000123 %) 10 records
