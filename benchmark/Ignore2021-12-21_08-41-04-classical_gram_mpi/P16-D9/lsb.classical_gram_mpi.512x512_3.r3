# Sysname : Linux
# Nodename: eu-a6-003-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 15:45:42 2021
# Execution time and date (local): Tue Dec 21 16:45:42 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 83183.791016            0 
            3           512           512   gramschmidt           MPI          1 79958.039062            4 
            3           512           512   gramschmidt           MPI          2 79787.025391            1 
            3           512           512   gramschmidt           MPI          3 79983.625000            0 
            3           512           512   gramschmidt           MPI          4 80336.748047            1 
            3           512           512   gramschmidt           MPI          5 80317.835938            0 
            3           512           512   gramschmidt           MPI          6 80433.171875            0 
            3           512           512   gramschmidt           MPI          7 83320.007812            0 
            3           512           512   gramschmidt           MPI          8 81019.253906            1 
            3           512           512   gramschmidt           MPI          9 80688.033203            0 
# Runtime: 11.073847 s (overhead: 0.000063 %) 10 records
