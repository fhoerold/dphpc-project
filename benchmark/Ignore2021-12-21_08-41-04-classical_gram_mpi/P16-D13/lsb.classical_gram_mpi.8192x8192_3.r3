# Sysname : Linux
# Nodename: eu-a6-001-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 10:09:35 2021
# Execution time and date (local): Wed Dec 22 11:09:35 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          8192          8192   gramschmidt           MPI          0 1221706567.515625            1 
            3          8192          8192   gramschmidt           MPI          1 1221754764.199219           11 
            3          8192          8192   gramschmidt           MPI          2 1221772538.589844           10 
            3          8192          8192   gramschmidt           MPI          3 1222254874.958984            2 
            3          8192          8192   gramschmidt           MPI          4 1225490691.707031           10 
            3          8192          8192   gramschmidt           MPI          5 1214790886.359375            3 
            3          8192          8192   gramschmidt           MPI          6 1214913218.318359            3 
            3          8192          8192   gramschmidt           MPI          7 1216134023.193359            3 
            3          8192          8192   gramschmidt           MPI          8 1227529296.214844           11 
            3          8192          8192   gramschmidt           MPI          9 1270049888.115234            4 
# Runtime: 15945.352641 s (overhead: 0.000000 %) 10 records
