# Sysname : Linux
# Nodename: eu-a6-001-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 10:09:35 2021
# Execution time and date (local): Wed Dec 22 11:09:35 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          8192          8192   gramschmidt           MPI          0 1221857898.541016            3 
            2          8192          8192   gramschmidt           MPI          1 1221906385.781250            9 
            2          8192          8192   gramschmidt           MPI          2 1221924228.859375            6 
            2          8192          8192   gramschmidt           MPI          3 1222406480.035156            2 
            2          8192          8192   gramschmidt           MPI          4 1225642638.248047            8 
            2          8192          8192   gramschmidt           MPI          5 1214941501.876953            1 
            2          8192          8192   gramschmidt           MPI          6 1215064001.464844            1 
            2          8192          8192   gramschmidt           MPI          7 1216284780.144531            2 
            2          8192          8192   gramschmidt           MPI          8 1227681352.261719            7 
            2          8192          8192   gramschmidt           MPI          9 1270207942.505859            1 
# Runtime: 15950.772292 s (overhead: 0.000000 %) 10 records
