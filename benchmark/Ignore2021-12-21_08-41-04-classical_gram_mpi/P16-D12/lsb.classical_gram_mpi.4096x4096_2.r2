# Sysname : Linux
# Nodename: eu-a6-001-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 09:51:23 2021
# Execution time and date (local): Wed Dec 22 10:51:23 2021
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 80650362.062500            1 
            2          4096          4096   gramschmidt           MPI          1 80438437.533203            4 
            2          4096          4096   gramschmidt           MPI          2 80396504.373047            6 
            2          4096          4096   gramschmidt           MPI          3 80558257.289062            2 
            2          4096          4096   gramschmidt           MPI          4 80127516.429688            5 
            2          4096          4096   gramschmidt           MPI          5 80141759.191406            1 
            2          4096          4096   gramschmidt           MPI          6 80072880.832031            1 
            2          4096          4096   gramschmidt           MPI          7 80049856.443359            1 
            2          4096          4096   gramschmidt           MPI          8 80244208.193359            6 
            2          4096          4096   gramschmidt           MPI          9 79853465.636719            0 
# Runtime: 1053.980194 s (overhead: 0.000003 %) 10 records
