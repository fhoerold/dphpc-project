# Sysname : Linux
# Nodename: eu-a6-001-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 09:51:23 2021
# Execution time and date (local): Wed Dec 22 10:51:23 2021
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 80650352.830078            0 
            3          4096          4096   gramschmidt           MPI          1 80438399.712891            4 
            3          4096          4096   gramschmidt           MPI          2 80396591.937500            4 
            3          4096          4096   gramschmidt           MPI          3 80558247.810547            0 
            3          4096          4096   gramschmidt           MPI          4 80127470.009766            4 
            3          4096          4096   gramschmidt           MPI          5 80141724.607422            0 
            3          4096          4096   gramschmidt           MPI          6 80072869.777344            0 
            3          4096          4096   gramschmidt           MPI          7 80049840.414062            0 
            3          4096          4096   gramschmidt           MPI          8 80244169.914062            5 
            3          4096          4096   gramschmidt           MPI          9 79853692.339844            1 
# Runtime: 1053.874040 s (overhead: 0.000002 %) 10 records
