# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 17:32:13 2021
# Execution time and date (local): Tue Dec 21 18:32:13 2021
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 749617.796875            0 
            2          1024          1024   gramschmidt           MPI          1 753192.445312            3 
            2          1024          1024   gramschmidt           MPI          2 750851.767578            1 
            2          1024          1024   gramschmidt           MPI          3 754325.041016            0 
            2          1024          1024   gramschmidt           MPI          4 746573.347656            1 
            2          1024          1024   gramschmidt           MPI          5 749768.626953            0 
            2          1024          1024   gramschmidt           MPI          6 747681.630859            0 
            2          1024          1024   gramschmidt           MPI          7 757623.798828            0 
            2          1024          1024   gramschmidt           MPI          8 745992.271484            5 
            2          1024          1024   gramschmidt           MPI          9 749174.164062            0 
# Runtime: 14.848713 s (overhead: 0.000067 %) 10 records
