# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 17:32:13 2021
# Execution time and date (local): Tue Dec 21 18:32:13 2021
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 748336.824219            0 
            3          1024          1024   gramschmidt           MPI          1 751950.019531            4 
            3          1024          1024   gramschmidt           MPI          2 749619.830078            1 
            3          1024          1024   gramschmidt           MPI          3 753136.970703            0 
            3          1024          1024   gramschmidt           MPI          4 745346.275391            1 
            3          1024          1024   gramschmidt           MPI          5 748532.515625            0 
            3          1024          1024   gramschmidt           MPI          6 746446.285156            0 
            3          1024          1024   gramschmidt           MPI          7 756373.812500            0 
            3          1024          1024   gramschmidt           MPI          8 744756.996094            6 
            3          1024          1024   gramschmidt           MPI          9 747934.371094            0 
# Runtime: 16.803655 s (overhead: 0.000071 %) 10 records
