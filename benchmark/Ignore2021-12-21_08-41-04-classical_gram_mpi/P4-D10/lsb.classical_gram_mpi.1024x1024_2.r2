# Sysname : Linux
# Nodename: eu-a6-006-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:42:20 2021
# Execution time and date (local): Tue Dec 21 08:42:20 2021
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 5037322.406250            0 
            2          1024          1024   gramschmidt           MPI          1 5544691.955078            4 
            2          1024          1024   gramschmidt           MPI          2 5590847.128906            5 
            2          1024          1024   gramschmidt           MPI          3 5936043.753906            0 
            2          1024          1024   gramschmidt           MPI          4 4482701.212891            1 
            2          1024          1024   gramschmidt           MPI          5 5302679.046875            0 
            2          1024          1024   gramschmidt           MPI          6 5599031.107422            0 
            2          1024          1024   gramschmidt           MPI          7 5662711.677734            0 
            2          1024          1024   gramschmidt           MPI          8 4778945.367188            6 
            2          1024          1024   gramschmidt           MPI          9 6013809.837891            0 
# Runtime: 73.877274 s (overhead: 0.000022 %) 10 records
