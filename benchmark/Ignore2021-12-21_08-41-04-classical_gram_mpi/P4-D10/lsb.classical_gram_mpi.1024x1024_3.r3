# Sysname : Linux
# Nodename: eu-a6-006-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:42:20 2021
# Execution time and date (local): Tue Dec 21 08:42:20 2021
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 5037658.908203            4 
            3          1024          1024   gramschmidt           MPI          1 5545190.031250            8 
            3          1024          1024   gramschmidt           MPI          2 5591267.005859            2 
            3          1024          1024   gramschmidt           MPI          3 5936545.294922            0 
            3          1024          1024   gramschmidt           MPI          4 4483059.687500            5 
            3          1024          1024   gramschmidt           MPI          5 5303039.812500            0 
            3          1024          1024   gramschmidt           MPI          6 5599442.968750            0 
            3          1024          1024   gramschmidt           MPI          7 5663197.925781            0 
            3          1024          1024   gramschmidt           MPI          8 4779311.611328            6 
            3          1024          1024   gramschmidt           MPI          9 6014209.818359            0 
# Runtime: 69.870251 s (overhead: 0.000036 %) 10 records
