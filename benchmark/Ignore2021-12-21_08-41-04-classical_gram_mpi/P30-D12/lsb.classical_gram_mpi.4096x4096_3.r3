# Sysname : Linux
# Nodename: eu-a6-005-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 22:55:25 2021
# Execution time and date (local): Wed Dec 22 23:55:25 2021
# MPI execution on rank 3 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 52566081.164062            0 
            3          4096          4096   gramschmidt           MPI          1 52732467.554688            5 
            3          4096          4096   gramschmidt           MPI          2 52675974.531250            5 
            3          4096          4096   gramschmidt           MPI          3 52758532.164062            0 
            3          4096          4096   gramschmidt           MPI          4 52754044.490234            1 
            3          4096          4096   gramschmidt           MPI          5 52621115.748047            0 
            3          4096          4096   gramschmidt           MPI          6 52745465.330078            0 
            3          4096          4096   gramschmidt           MPI          7 52730779.671875            0 
            3          4096          4096   gramschmidt           MPI          8 52683213.648438            5 
            3          4096          4096   gramschmidt           MPI          9 52748201.312500            0 
# Runtime: 694.665673 s (overhead: 0.000002 %) 10 records
