# Sysname : Linux
# Nodename: eu-a6-005-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 22:55:25 2021
# Execution time and date (local): Wed Dec 22 23:55:25 2021
# MPI execution on rank 2 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 52575418.375000            0 
            2          4096          4096   gramschmidt           MPI          1 52742073.222656            4 
            2          4096          4096   gramschmidt           MPI          2 52685244.917969            7 
            2          4096          4096   gramschmidt           MPI          3 52767914.199219            0 
            2          4096          4096   gramschmidt           MPI          4 52763422.269531            2 
            2          4096          4096   gramschmidt           MPI          5 52630485.474609            0 
            2          4096          4096   gramschmidt           MPI          6 52754847.572266            0 
            2          4096          4096   gramschmidt           MPI          7 52740179.908203            0 
            2          4096          4096   gramschmidt           MPI          8 52692579.455078            6 
            2          4096          4096   gramschmidt           MPI          9 52757826.408203            0 
# Runtime: 690.871158 s (overhead: 0.000003 %) 10 records
