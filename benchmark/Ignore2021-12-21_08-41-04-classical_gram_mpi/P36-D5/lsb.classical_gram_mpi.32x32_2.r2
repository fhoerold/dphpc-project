# Sysname : Linux
# Nodename: eu-a6-002-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 11:12:21 2021
# Execution time and date (local): Thu Dec 23 12:12:21 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 1046.914062            0 
            2            32            32   gramschmidt           MPI          1 939.167969            0 
            2            32            32   gramschmidt           MPI          2 925.619141            0 
            2            32            32   gramschmidt           MPI          3 889.914062            0 
            2            32            32   gramschmidt           MPI          4 886.085938            0 
            2            32            32   gramschmidt           MPI          5 865.005859            0 
            2            32            32   gramschmidt           MPI          6 861.238281            0 
            2            32            32   gramschmidt           MPI          7 886.777344            0 
            2            32            32   gramschmidt           MPI          8 869.189453            0 
            2            32            32   gramschmidt           MPI          9 874.320312            0 
# Runtime: 0.015238 s (overhead: 0.000000 %) 10 records
