# Sysname : Linux
# Nodename: eu-a6-002-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 11:12:21 2021
# Execution time and date (local): Thu Dec 23 12:12:21 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 1022.847656            0 
            3            32            32   gramschmidt           MPI          1 923.839844            2 
            3            32            32   gramschmidt           MPI          2 915.062500            0 
            3            32            32   gramschmidt           MPI          3 878.160156            0 
            3            32            32   gramschmidt           MPI          4 871.753906            0 
            3            32            32   gramschmidt           MPI          5 856.716797            0 
            3            32            32   gramschmidt           MPI          6 850.412109            0 
            3            32            32   gramschmidt           MPI          7 867.789062            0 
            3            32            32   gramschmidt           MPI          8 858.220703            0 
            3            32            32   gramschmidt           MPI          9 862.132812            0 
# Runtime: 10.012300 s (overhead: 0.000020 %) 10 records
