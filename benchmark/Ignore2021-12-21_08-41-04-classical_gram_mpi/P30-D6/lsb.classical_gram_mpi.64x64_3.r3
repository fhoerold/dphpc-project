# Sysname : Linux
# Nodename: eu-a6-007-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 05:35:53 2021
# Execution time and date (local): Wed Dec 22 06:35:53 2021
# MPI execution on rank 3 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 2370.789062            0 
            3            64            64   gramschmidt           MPI          1 2027.701172            3 
            3            64            64   gramschmidt           MPI          2 1908.373047            0 
            3            64            64   gramschmidt           MPI          3 1856.080078            0 
            3            64            64   gramschmidt           MPI          4 1863.501953            0 
            3            64            64   gramschmidt           MPI          5 1871.751953            0 
            3            64            64   gramschmidt           MPI          6 1852.167969            0 
            3            64            64   gramschmidt           MPI          7 1858.667969            0 
            3            64            64   gramschmidt           MPI          8 1846.181641            0 
            3            64            64   gramschmidt           MPI          9 1855.283203            0 
# Runtime: 7.036786 s (overhead: 0.000043 %) 10 records
