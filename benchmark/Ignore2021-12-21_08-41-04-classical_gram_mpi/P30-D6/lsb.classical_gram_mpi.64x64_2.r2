# Sysname : Linux
# Nodename: eu-a6-007-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 05:35:53 2021
# Execution time and date (local): Wed Dec 22 06:35:53 2021
# MPI execution on rank 2 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 2417.287109            0 
            2            64            64   gramschmidt           MPI          1 2074.716797            3 
            2            64            64   gramschmidt           MPI          2 1950.687500            0 
            2            64            64   gramschmidt           MPI          3 1891.037109            0 
            2            64            64   gramschmidt           MPI          4 1898.328125            0 
            2            64            64   gramschmidt           MPI          5 1907.460938            0 
            2            64            64   gramschmidt           MPI          6 1881.228516            0 
            2            64            64   gramschmidt           MPI          7 1895.593750            0 
            2            64            64   gramschmidt           MPI          8 1865.144531            0 
            2            64            64   gramschmidt           MPI          9 1869.628906            0 
# Runtime: 4.039526 s (overhead: 0.000074 %) 10 records
