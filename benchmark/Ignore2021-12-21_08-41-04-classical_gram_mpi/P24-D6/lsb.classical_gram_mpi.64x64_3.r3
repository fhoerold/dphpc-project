# Sysname : Linux
# Nodename: eu-a6-009-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 19:50:09 2021
# Execution time and date (local): Tue Dec 21 20:50:09 2021
# MPI execution on rank 3 with 24 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 2051.992188            0 
            3            64            64   gramschmidt           MPI          1 1935.988281            3 
            3            64            64   gramschmidt           MPI          2 1916.119141            0 
            3            64            64   gramschmidt           MPI          3 1885.140625            0 
            3            64            64   gramschmidt           MPI          4 1855.998047            0 
            3            64            64   gramschmidt           MPI          5 1823.839844            0 
            3            64            64   gramschmidt           MPI          6 1844.798828            0 
            3            64            64   gramschmidt           MPI          7 1834.648438            0 
            3            64            64   gramschmidt           MPI          8 1820.177734            0 
            3            64            64   gramschmidt           MPI          9 1854.703125            0 
# Runtime: 1.063224 s (overhead: 0.000282 %) 10 records
