# Sysname : Linux
# Nodename: eu-a6-009-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 19:50:09 2021
# Execution time and date (local): Tue Dec 21 20:50:09 2021
# MPI execution on rank 2 with 24 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 2094.175781            0 
            2            64            64   gramschmidt           MPI          1 1958.154297            3 
            2            64            64   gramschmidt           MPI          2 1960.513672            0 
            2            64            64   gramschmidt           MPI          3 1928.074219            0 
            2            64            64   gramschmidt           MPI          4 1895.841797            0 
            2            64            64   gramschmidt           MPI          5 1856.013672            0 
            2            64            64   gramschmidt           MPI          6 1870.271484            0 
            2            64            64   gramschmidt           MPI          7 1860.539062            0 
            2            64            64   gramschmidt           MPI          8 1852.404297            0 
            2            64            64   gramschmidt           MPI          9 1871.490234            0 
# Runtime: 10.058367 s (overhead: 0.000030 %) 10 records
