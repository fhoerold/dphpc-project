# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 17:31:29 2021
# Execution time and date (local): Tue Dec 21 18:31:29 2021
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 1082.199219            0 
            3            32            32   gramschmidt           MPI          1 1270.062500            3 
            3            32            32   gramschmidt           MPI          2 922.458984            0 
            3            32            32   gramschmidt           MPI          3 947.150391            0 
            3            32            32   gramschmidt           MPI          4 863.123047            0 
            3            32            32   gramschmidt           MPI          5 795.310547            0 
            3            32            32   gramschmidt           MPI          6 799.123047            0 
            3            32            32   gramschmidt           MPI          7 798.101562            0 
            3            32            32   gramschmidt           MPI          8 760.318359            0 
            3            32            32   gramschmidt           MPI          9 756.816406            0 
# Runtime: 11.985856 s (overhead: 0.000025 %) 10 records
