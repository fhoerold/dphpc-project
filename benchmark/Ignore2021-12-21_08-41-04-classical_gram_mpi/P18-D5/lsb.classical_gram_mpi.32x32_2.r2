# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 17:31:29 2021
# Execution time and date (local): Tue Dec 21 18:31:29 2021
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 1082.171875            0 
            2            32            32   gramschmidt           MPI          1 951.027344            3 
            2            32            32   gramschmidt           MPI          2 933.351562            0 
            2            32            32   gramschmidt           MPI          3 955.175781            0 
            2            32            32   gramschmidt           MPI          4 876.847656            0 
            2            32            32   gramschmidt           MPI          5 803.630859            0 
            2            32            32   gramschmidt           MPI          6 809.789062            0 
            2            32            32   gramschmidt           MPI          7 808.267578            0 
            2            32            32   gramschmidt           MPI          8 772.195312            0 
            2            32            32   gramschmidt           MPI          9 766.607422            0 
# Runtime: 11.985465 s (overhead: 0.000025 %) 10 records
