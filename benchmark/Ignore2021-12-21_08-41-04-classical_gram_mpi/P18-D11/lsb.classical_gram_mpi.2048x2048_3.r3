# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 17:32:37 2021
# Execution time and date (local): Tue Dec 21 18:32:37 2021
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 9139724.009766            0 
            3          2048          2048   gramschmidt           MPI          1 9154494.548828            5 
            3          2048          2048   gramschmidt           MPI          2 9145267.773438            2 
            3          2048          2048   gramschmidt           MPI          3 9181613.199219            0 
            3          2048          2048   gramschmidt           MPI          4 9185986.267578            2 
            3          2048          2048   gramschmidt           MPI          5 9177076.771484            0 
            3          2048          2048   gramschmidt           MPI          6 9143647.376953            0 
            3          2048          2048   gramschmidt           MPI          7 9138004.818359            0 
            3          2048          2048   gramschmidt           MPI          8 9170328.800781            6 
            3          2048          2048   gramschmidt           MPI          9 9230279.566406            0 
# Runtime: 130.481949 s (overhead: 0.000011 %) 10 records
