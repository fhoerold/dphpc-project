# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 17:32:37 2021
# Execution time and date (local): Tue Dec 21 18:32:37 2021
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 9141554.976562            0 
            2          2048          2048   gramschmidt           MPI          1 9156164.632812            5 
            2          2048          2048   gramschmidt           MPI          2 9147089.917969            4 
            2          2048          2048   gramschmidt           MPI          3 9183443.314453            0 
            2          2048          2048   gramschmidt           MPI          4 9187826.144531            2 
            2          2048          2048   gramschmidt           MPI          5 9178785.029297            0 
            2          2048          2048   gramschmidt           MPI          6 9145470.533203            0 
            2          2048          2048   gramschmidt           MPI          7 9139843.945312            0 
            2          2048          2048   gramschmidt           MPI          8 9172164.048828            6 
            2          2048          2048   gramschmidt           MPI          9 9232127.521484            0 
# Runtime: 127.537702 s (overhead: 0.000013 %) 10 records
