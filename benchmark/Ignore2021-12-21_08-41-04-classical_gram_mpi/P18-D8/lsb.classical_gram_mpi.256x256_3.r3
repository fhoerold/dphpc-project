# Sysname : Linux
# Nodename: eu-a6-001-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 14:35:59 2021
# Execution time and date (local): Wed Dec 22 15:35:59 2021
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 14164.501953            0 
            3           256           256   gramschmidt           MPI          1 13664.835938            1 
            3           256           256   gramschmidt           MPI          2 13614.255859            0 
            3           256           256   gramschmidt           MPI          3 13662.939453            0 
            3           256           256   gramschmidt           MPI          4 13554.716797            0 
            3           256           256   gramschmidt           MPI          5 13475.595703            0 
            3           256           256   gramschmidt           MPI          6 13460.843750            0 
            3           256           256   gramschmidt           MPI          7 13537.037109            0 
            3           256           256   gramschmidt           MPI          8 13421.085938            0 
            3           256           256   gramschmidt           MPI          9 13457.470703            0 
# Runtime: 0.186791 s (overhead: 0.000535 %) 10 records
