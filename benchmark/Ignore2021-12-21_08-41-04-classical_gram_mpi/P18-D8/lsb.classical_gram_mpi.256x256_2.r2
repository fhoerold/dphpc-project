# Sysname : Linux
# Nodename: eu-a6-001-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 14:35:59 2021
# Execution time and date (local): Wed Dec 22 15:35:59 2021
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 14064.240234            0 
            2           256           256   gramschmidt           MPI          1 13657.541016            2 
            2           256           256   gramschmidt           MPI          2 13612.074219            0 
            2           256           256   gramschmidt           MPI          3 13696.585938            0 
            2           256           256   gramschmidt           MPI          4 13552.941406            0 
            2           256           256   gramschmidt           MPI          5 13475.773438            0 
            2           256           256   gramschmidt           MPI          6 13465.300781            0 
            2           256           256   gramschmidt           MPI          7 13531.070312            0 
            2           256           256   gramschmidt           MPI          8 13423.503906            0 
            2           256           256   gramschmidt           MPI          9 13457.683594            0 
# Runtime: 12.179465 s (overhead: 0.000016 %) 10 records
