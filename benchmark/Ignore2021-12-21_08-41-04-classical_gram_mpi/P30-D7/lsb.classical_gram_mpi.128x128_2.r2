# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 05:30:42 2021
# Execution time and date (local): Wed Dec 22 06:30:42 2021
# MPI execution on rank 2 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 5049.697266            0 
            2           128           128   gramschmidt           MPI          1 4704.687500            3 
            2           128           128   gramschmidt           MPI          2 4697.927734            0 
            2           128           128   gramschmidt           MPI          3 4645.083984            0 
            2           128           128   gramschmidt           MPI          4 4696.044922            0 
            2           128           128   gramschmidt           MPI          5 4625.689453            0 
            2           128           128   gramschmidt           MPI          6 4640.089844            0 
            2           128           128   gramschmidt           MPI          7 4642.082031            0 
            2           128           128   gramschmidt           MPI          8 4642.146484            0 
            2           128           128   gramschmidt           MPI          9 4613.074219            0 
# Runtime: 6.051521 s (overhead: 0.000050 %) 10 records
