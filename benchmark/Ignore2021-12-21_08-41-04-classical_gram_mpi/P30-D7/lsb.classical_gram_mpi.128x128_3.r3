# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 05:30:42 2021
# Execution time and date (local): Wed Dec 22 06:30:42 2021
# MPI execution on rank 3 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 5390.380859            0 
            3           128           128   gramschmidt           MPI          1 4944.091797            3 
            3           128           128   gramschmidt           MPI          2 4703.968750            0 
            3           128           128   gramschmidt           MPI          3 4651.156250            0 
            3           128           128   gramschmidt           MPI          4 4702.695312            0 
            3           128           128   gramschmidt           MPI          5 4634.181641            0 
            3           128           128   gramschmidt           MPI          6 4647.644531            0 
            3           128           128   gramschmidt           MPI          7 4648.437500            0 
            3           128           128   gramschmidt           MPI          8 4648.830078            0 
            3           128           128   gramschmidt           MPI          9 4622.312500            0 
# Runtime: 6.068770 s (overhead: 0.000049 %) 10 records
