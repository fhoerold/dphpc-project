# Sysname : Linux
# Nodename: eu-a6-009-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 19:35:05 2021
# Execution time and date (local): Tue Dec 21 20:35:05 2021
# MPI execution on rank 2 with 24 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 1049.140625            0 
            2            32            32   gramschmidt           MPI          1 978.632812            2 
            2            32            32   gramschmidt           MPI          2 948.839844            0 
            2            32            32   gramschmidt           MPI          3 921.656250            0 
            2            32            32   gramschmidt           MPI          4 893.753906            0 
            2            32            32   gramschmidt           MPI          5 902.296875            0 
            2            32            32   gramschmidt           MPI          6 870.001953            0 
            2            32            32   gramschmidt           MPI          7 886.146484            0 
            2            32            32   gramschmidt           MPI          8 878.923828            0 
            2            32            32   gramschmidt           MPI          9 870.162109            0 
# Runtime: 5.032490 s (overhead: 0.000040 %) 10 records
