# Sysname : Linux
# Nodename: eu-a6-009-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 19:35:05 2021
# Execution time and date (local): Tue Dec 21 20:35:05 2021
# MPI execution on rank 3 with 24 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 1045.783203            0 
            3            32            32   gramschmidt           MPI          1 969.935547            2 
            3            32            32   gramschmidt           MPI          2 936.902344            0 
            3            32            32   gramschmidt           MPI          3 909.576172            0 
            3            32            32   gramschmidt           MPI          4 880.488281            0 
            3            32            32   gramschmidt           MPI          5 885.406250            0 
            3            32            32   gramschmidt           MPI          6 861.466797            0 
            3            32            32   gramschmidt           MPI          7 879.511719            0 
            3            32            32   gramschmidt           MPI          8 860.388672            0 
            3            32            32   gramschmidt           MPI          9 861.970703            0 
# Runtime: 6.010881 s (overhead: 0.000033 %) 10 records
