# Sysname : Linux
# Nodename: eu-a6-002-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 11:13:23 2021
# Execution time and date (local): Thu Dec 23 12:13:23 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 4111276.408203            0 
            2          2048          2048   gramschmidt           MPI          1 4105165.634766            5 
            2          2048          2048   gramschmidt           MPI          2 4109373.156250            2 
            2          2048          2048   gramschmidt           MPI          3 4149339.230469            0 
            2          2048          2048   gramschmidt           MPI          4 4145372.798828            5 
            2          2048          2048   gramschmidt           MPI          5 4116407.621094            0 
            2          2048          2048   gramschmidt           MPI          6 4131464.494141            0 
            2          2048          2048   gramschmidt           MPI          7 4113716.109375            0 
            2          2048          2048   gramschmidt           MPI          8 4114021.357422            2 
            2          2048          2048   gramschmidt           MPI          9 4121598.507812            1 
# Runtime: 82.149174 s (overhead: 0.000018 %) 10 records
