# Sysname : Linux
# Nodename: eu-a6-002-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Dec 23 11:13:23 2021
# Execution time and date (local): Thu Dec 23 12:13:23 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 4111646.738281            0 
            3          2048          2048   gramschmidt           MPI          1 4105563.636719            6 
            3          2048          2048   gramschmidt           MPI          2 4109782.298828            2 
            3          2048          2048   gramschmidt           MPI          3 4149736.837891            0 
            3          2048          2048   gramschmidt           MPI          4 4145846.947266            2 
            3          2048          2048   gramschmidt           MPI          5 4116823.988281            0 
            3          2048          2048   gramschmidt           MPI          6 4131863.492188            0 
            3          2048          2048   gramschmidt           MPI          7 4114118.236328            0 
            3          2048          2048   gramschmidt           MPI          8 4114426.867188            7 
            3          2048          2048   gramschmidt           MPI          9 4122021.283203            0 
# Runtime: 73.125371 s (overhead: 0.000023 %) 10 records
