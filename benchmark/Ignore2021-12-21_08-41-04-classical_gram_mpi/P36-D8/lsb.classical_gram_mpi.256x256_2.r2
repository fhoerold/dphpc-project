# Sysname : Linux
# Nodename: eu-a6-004-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 22:54:31 2021
# Execution time and date (local): Wed Dec 22 23:54:31 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 14725.115234            0 
            2           256           256   gramschmidt           MPI          1 14124.484375            3 
            2           256           256   gramschmidt           MPI          2 13994.253906            0 
            2           256           256   gramschmidt           MPI          3 14124.191406            0 
            2           256           256   gramschmidt           MPI          4 13918.927734            0 
            2           256           256   gramschmidt           MPI          5 13932.361328            0 
            2           256           256   gramschmidt           MPI          6 13918.935547            0 
            2           256           256   gramschmidt           MPI          7 14075.656250            0 
            2           256           256   gramschmidt           MPI          8 14082.958984            0 
            2           256           256   gramschmidt           MPI          9 14032.740234            0 
# Runtime: 14.216938 s (overhead: 0.000021 %) 10 records
