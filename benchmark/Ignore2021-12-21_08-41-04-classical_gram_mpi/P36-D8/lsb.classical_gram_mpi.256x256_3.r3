# Sysname : Linux
# Nodename: eu-a6-004-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 22:54:31 2021
# Execution time and date (local): Wed Dec 22 23:54:31 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 14725.697266            0 
            3           256           256   gramschmidt           MPI          1 14121.548828            3 
            3           256           256   gramschmidt           MPI          2 13994.710938            0 
            3           256           256   gramschmidt           MPI          3 14123.089844            0 
            3           256           256   gramschmidt           MPI          4 13941.632812            0 
            3           256           256   gramschmidt           MPI          5 13934.511719            0 
            3           256           256   gramschmidt           MPI          6 13919.312500            0 
            3           256           256   gramschmidt           MPI          7 14073.810547            0 
            3           256           256   gramschmidt           MPI          8 14078.720703            0 
            3           256           256   gramschmidt           MPI          9 14031.533203            0 
# Runtime: 20.215760 s (overhead: 0.000015 %) 10 records
