# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:47:37 2021
# Execution time and date (local): Tue Dec 21 08:47:37 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 1039282.257812            4 
            2          1024          1024   gramschmidt           MPI          1 1048821.185547            4 
            2          1024          1024   gramschmidt           MPI          2 1043568.466797            1 
            2          1024          1024   gramschmidt           MPI          3 1040195.519531            0 
            2          1024          1024   gramschmidt           MPI          4 1040934.097656            1 
            2          1024          1024   gramschmidt           MPI          5 1046709.830078            0 
            2          1024          1024   gramschmidt           MPI          6 1039963.800781            0 
            2          1024          1024   gramschmidt           MPI          7 1038877.025391            0 
            2          1024          1024   gramschmidt           MPI          8 1039579.662109            1 
            2          1024          1024   gramschmidt           MPI          9 1049143.136719            0 
# Runtime: 35.601816 s (overhead: 0.000031 %) 10 records
