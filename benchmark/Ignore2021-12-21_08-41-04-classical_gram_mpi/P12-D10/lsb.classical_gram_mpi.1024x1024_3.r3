# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:47:37 2021
# Execution time and date (local): Tue Dec 21 08:47:37 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 1039935.117188            0 
            3          1024          1024   gramschmidt           MPI          1 1049615.351562            4 
            3          1024          1024   gramschmidt           MPI          2 1044275.353516            2 
            3          1024          1024   gramschmidt           MPI          3 1040902.994141            0 
            3          1024          1024   gramschmidt           MPI          4 1041700.488281            2 
            3          1024          1024   gramschmidt           MPI          5 1047416.521484            0 
            3          1024          1024   gramschmidt           MPI          6 1040667.253906            0 
            3          1024          1024   gramschmidt           MPI          7 1039578.457031            0 
            3          1024          1024   gramschmidt           MPI          8 1040283.714844            2 
            3          1024          1024   gramschmidt           MPI          9 1049917.583984            0 
# Runtime: 27.624256 s (overhead: 0.000036 %) 10 records
