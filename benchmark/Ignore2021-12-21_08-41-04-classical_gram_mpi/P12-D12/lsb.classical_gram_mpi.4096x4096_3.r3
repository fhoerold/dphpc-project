# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:51:47 2021
# Execution time and date (local): Tue Dec 21 08:51:47 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 116408691.019531            0 
            3          4096          4096   gramschmidt           MPI          1 116367497.849609            4 
            3          4096          4096   gramschmidt           MPI          2 116153567.421875            4 
            3          4096          4096   gramschmidt           MPI          3 116558406.611328            0 
            3          4096          4096   gramschmidt           MPI          4 116394132.035156            4 
            3          4096          4096   gramschmidt           MPI          5 116476710.779297            0 
            3          4096          4096   gramschmidt           MPI          6 116154256.228516            0 
            3          4096          4096   gramschmidt           MPI          7 116339596.363281            0 
            3          4096          4096   gramschmidt           MPI          8 116380446.978516            6 
            3          4096          4096   gramschmidt           MPI          9 116377592.525391            0 
# Runtime: 1521.821939 s (overhead: 0.000001 %) 10 records
