# Sysname : Linux
# Nodename: eu-a6-005-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 07:51:47 2021
# Execution time and date (local): Tue Dec 21 08:51:47 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 116176879.357422            2 
            2          4096          4096   gramschmidt           MPI          1 116135698.343750            5 
            2          4096          4096   gramschmidt           MPI          2 115922256.673828            4 
            2          4096          4096   gramschmidt           MPI          3 116326268.404297            0 
            2          4096          4096   gramschmidt           MPI          4 116162129.900391            4 
            2          4096          4096   gramschmidt           MPI          5 116244717.683594            0 
            2          4096          4096   gramschmidt           MPI          6 115922694.687500            0 
            2          4096          4096   gramschmidt           MPI          7 116107655.478516            0 
            2          4096          4096   gramschmidt           MPI          8 116148586.275391            4 
            2          4096          4096   gramschmidt           MPI          9 116145809.843750            0 
# Runtime: 1522.884596 s (overhead: 0.000001 %) 10 records
