# Sysname : Linux
# Nodename: eu-a6-012-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 09:06:14 2021
# Execution time and date (local): Tue Dec 21 10:06:14 2021
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 341.390625            0 
            3            32            32   gramschmidt           MPI          1 300.222656            2 
            3            32            32   gramschmidt           MPI          2 300.687500            0 
            3            32            32   gramschmidt           MPI          3 310.201172            0 
            3            32            32   gramschmidt           MPI          4 300.029297            0 
            3            32            32   gramschmidt           MPI          5 306.759766            0 
            3            32            32   gramschmidt           MPI          6 324.066406            0 
            3            32            32   gramschmidt           MPI          7 292.808594            0 
            3            32            32   gramschmidt           MPI          8 296.207031            0 
            3            32            32   gramschmidt           MPI          9 299.203125            0 
# Runtime: 4.998040 s (overhead: 0.000040 %) 10 records
