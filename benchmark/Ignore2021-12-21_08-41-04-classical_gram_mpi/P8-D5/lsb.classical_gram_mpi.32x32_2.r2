# Sysname : Linux
# Nodename: eu-a6-012-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 09:06:14 2021
# Execution time and date (local): Tue Dec 21 10:06:14 2021
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 346.792969            1 
            2            32            32   gramschmidt           MPI          1 304.623047            2 
            2            32            32   gramschmidt           MPI          2 304.857422            0 
            2            32            32   gramschmidt           MPI          3 310.958984            0 
            2            32            32   gramschmidt           MPI          4 303.058594            0 
            2            32            32   gramschmidt           MPI          5 307.781250            0 
            2            32            32   gramschmidt           MPI          6 327.792969            0 
            2            32            32   gramschmidt           MPI          7 298.966797            0 
            2            32            32   gramschmidt           MPI          8 297.998047            0 
            2            32            32   gramschmidt           MPI          9 304.425781            0 
# Runtime: 8.977566 s (overhead: 0.000033 %) 10 records
