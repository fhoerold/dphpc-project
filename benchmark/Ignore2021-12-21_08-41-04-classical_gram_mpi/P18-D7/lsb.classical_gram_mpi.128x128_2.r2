# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 17:31:50 2021
# Execution time and date (local): Tue Dec 21 18:31:50 2021
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 4776.111328            0 
            2           128           128   gramschmidt           MPI          1 4769.812500            2 
            2           128           128   gramschmidt           MPI          2 4602.259766            0 
            2           128           128   gramschmidt           MPI          3 4422.529297            0 
            2           128           128   gramschmidt           MPI          4 4473.837891            0 
            2           128           128   gramschmidt           MPI          5 4583.824219            0 
            2           128           128   gramschmidt           MPI          6 4528.525391            0 
            2           128           128   gramschmidt           MPI          7 4425.904297            0 
            2           128           128   gramschmidt           MPI          8 4370.056641            0 
            2           128           128   gramschmidt           MPI          9 4424.474609            0 
# Runtime: 16.066394 s (overhead: 0.000012 %) 10 records
