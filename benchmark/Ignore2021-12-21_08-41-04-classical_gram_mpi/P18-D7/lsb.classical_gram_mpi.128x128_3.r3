# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 17:31:50 2021
# Execution time and date (local): Tue Dec 21 18:31:50 2021
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 4794.250000            0 
            3           128           128   gramschmidt           MPI          1 5078.759766            3 
            3           128           128   gramschmidt           MPI          2 4605.705078            0 
            3           128           128   gramschmidt           MPI          3 4423.947266            0 
            3           128           128   gramschmidt           MPI          4 4474.062500            0 
            3           128           128   gramschmidt           MPI          5 4583.537109            0 
            3           128           128   gramschmidt           MPI          6 4526.162109            0 
            3           128           128   gramschmidt           MPI          7 4424.580078            0 
            3           128           128   gramschmidt           MPI          8 4370.068359            0 
            3           128           128   gramschmidt           MPI          9 4424.656250            0 
# Runtime: 10.066773 s (overhead: 0.000030 %) 10 records
