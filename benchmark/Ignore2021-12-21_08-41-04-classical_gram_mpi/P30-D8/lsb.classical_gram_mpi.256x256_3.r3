# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 05:30:56 2021
# Execution time and date (local): Wed Dec 22 06:30:56 2021
# MPI execution on rank 3 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 17551.521484            0 
            3           256           256   gramschmidt           MPI          1 17408.552734            4 
            3           256           256   gramschmidt           MPI          2 17226.972656            0 
            3           256           256   gramschmidt           MPI          3 17203.599609            0 
            3           256           256   gramschmidt           MPI          4 17208.841797            0 
            3           256           256   gramschmidt           MPI          5 17176.150391            0 
            3           256           256   gramschmidt           MPI          6 17143.681641            0 
            3           256           256   gramschmidt           MPI          7 17194.189453            0 
            3           256           256   gramschmidt           MPI          8 17144.693359            0 
            3           256           256   gramschmidt           MPI          9 17216.287109            0 
# Runtime: 1.282210 s (overhead: 0.000312 %) 10 records
