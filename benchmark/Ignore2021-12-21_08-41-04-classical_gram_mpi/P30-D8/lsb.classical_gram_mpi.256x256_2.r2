# Sysname : Linux
# Nodename: eu-a6-003-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Dec 22 05:30:56 2021
# Execution time and date (local): Wed Dec 22 06:30:56 2021
# MPI execution on rank 2 with 30 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 17893.369141            0 
            2           256           256   gramschmidt           MPI          1 17314.794922            4 
            2           256           256   gramschmidt           MPI          2 17253.740234            0 
            2           256           256   gramschmidt           MPI          3 17228.490234            0 
            2           256           256   gramschmidt           MPI          4 17207.679688            0 
            2           256           256   gramschmidt           MPI          5 17183.359375            0 
            2           256           256   gramschmidt           MPI          6 17159.613281            0 
            2           256           256   gramschmidt           MPI          7 17190.515625            0 
            2           256           256   gramschmidt           MPI          8 17139.820312            0 
            2           256           256   gramschmidt           MPI          9 17230.605469            0 
# Runtime: 4.275739 s (overhead: 0.000094 %) 10 records
