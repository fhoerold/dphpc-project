# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 17:48:30 2021
# Execution time and date (local): Tue Dec 21 18:48:30 2021
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 82720551.933594            0 
            2          4096          4096   gramschmidt           MPI          1 82861076.931641            4 
            2          4096          4096   gramschmidt           MPI          2 82469823.242188            3 
            2          4096          4096   gramschmidt           MPI          3 82120489.580078            1 
            2          4096          4096   gramschmidt           MPI          4 82252964.482422            6 
            2          4096          4096   gramschmidt           MPI          5 82426675.355469            1 
            2          4096          4096   gramschmidt           MPI          6 82257574.783203            2 
            2          4096          4096   gramschmidt           MPI          7 82056222.058594            2 
            2          4096          4096   gramschmidt           MPI          8 82097380.990234            3 
            2          4096          4096   gramschmidt           MPI          9 82192551.923828            0 
# Runtime: 1080.314764 s (overhead: 0.000002 %) 10 records
