# Sysname : Linux
# Nodename: eu-a6-012-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Dec 21 17:48:30 2021
# Execution time and date (local): Tue Dec 21 18:48:30 2021
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 82799650.410156            1 
            3          4096          4096   gramschmidt           MPI          1 82940340.048828            5 
            3          4096          4096   gramschmidt           MPI          2 82548856.171875            4 
            3          4096          4096   gramschmidt           MPI          3 82199024.189453            0 
            3          4096          4096   gramschmidt           MPI          4 82331684.894531            5 
            3          4096          4096   gramschmidt           MPI          5 82505563.960938            2 
            3          4096          4096   gramschmidt           MPI          6 82336265.498047            1 
            3          4096          4096   gramschmidt           MPI          7 82134816.808594            1 
            3          4096          4096   gramschmidt           MPI          8 82175936.324219            7 
            3          4096          4096   gramschmidt           MPI          9 82271207.578125            4 
# Runtime: 1081.252370 s (overhead: 0.000003 %) 10 records
