Invocation: ./cholesky/benchmark.sh -o ./benchmark --openmp --linear-dim -d 6502 -p 16 ./cholesky/build/cholesky_openmp_simd_ll_naive
Benchmark 'cholesky_openmp_simd_ll_naive' for
> Dimensions:		6502, 
> Number of processors:	16, 
> CPU Model:		XeonGold_6150
> Use MPI:		false
> Use OpenMP:		true

Timestamp: 2022-01-14_23-36-34
