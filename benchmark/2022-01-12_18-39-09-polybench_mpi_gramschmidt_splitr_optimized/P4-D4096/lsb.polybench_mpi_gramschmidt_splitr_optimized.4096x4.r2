# Sysname : Linux
# Nodename: eu-a6-008-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 17:39:33 2022
# Execution time and date (local): Wed Jan 12 18:39:33 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 204773699.066406            2 
         4096          4096   gramschmidt          1 199958227.501953            7 
         4096          4096   gramschmidt          2 200252428.218750            5 
         4096          4096   gramschmidt          3 200790429.476562            0 
         4096          4096   gramschmidt          4 200980591.318359            4 
         4096          4096   gramschmidt          5 199373891.203125            0 
         4096          4096   gramschmidt          6 199467573.761719            0 
         4096          4096   gramschmidt          7 199438916.871094            1 
         4096          4096   gramschmidt          8 198504434.035156            4 
         4096          4096   gramschmidt          9 198546829.228516            0 
# Runtime: 2002.087084 s (overhead: 0.000001 %) 10 records
