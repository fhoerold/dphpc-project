# Sysname : Linux
# Nodename: eu-a6-008-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan 12 17:39:33 2022
# Execution time and date (local): Wed Jan 12 18:39:33 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         4096          4096   gramschmidt          0 199814984.652344            0 
         4096          4096   gramschmidt          1 199988551.166016            5 
         4096          4096   gramschmidt          2 200283565.636719            4 
         4096          4096   gramschmidt          3 200820645.267578            0 
         4096          4096   gramschmidt          4 201011836.482422            4 
         4096          4096   gramschmidt          5 199404375.611328            0 
         4096          4096   gramschmidt          6 199498565.720703            0 
         4096          4096   gramschmidt          7 199469924.914062            0 
         4096          4096   gramschmidt          8 198534633.095703            3 
         4096          4096   gramschmidt          9 198577445.556641            0 
# Runtime: 1997.404575 s (overhead: 0.000001 %) 10 records
