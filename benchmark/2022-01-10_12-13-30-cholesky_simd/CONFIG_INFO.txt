Invocation: ../cholesky/benchmark.sh -o /cluster/home/forstesa/repos/dphpc-project/benchmark --cachegrind -d 5 7 9 11 -p 1 ../cholesky/build/cholesky_simd
Benchmark 'cholesky_simd' for
> Dimensions:		32, 128, 512, 2048, 
> Number of processors:	1, 
> CPU Model:		XeonGold_6150
> Use MPI:		false
> Use OpenMP:		false

Timestamp: 2022-01-10_12-13-30
