# Sysname : Linux
# Nodename: eu-a6-001-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:12:16 2022
# Execution time and date (local): Fri Jan  7 12:12:16 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 2136224.589844            2 
            2          1024          1024   gramschmidt           MPI          1 2149303.957031            5 
            2          1024          1024   gramschmidt           MPI          2 2139109.039062            1 
            2          1024          1024   gramschmidt           MPI          3 2138341.363281            0 
            2          1024          1024   gramschmidt           MPI          4 2116048.216797            1 
            2          1024          1024   gramschmidt           MPI          5 2142339.994141            0 
            2          1024          1024   gramschmidt           MPI          6 2115845.876953            0 
            2          1024          1024   gramschmidt           MPI          7 2021955.683594            0 
            2          1024          1024   gramschmidt           MPI          8 2042432.091797            7 
            2          1024          1024   gramschmidt           MPI          9 2035943.363281            0 
# Runtime: 34.329398 s (overhead: 0.000047 %) 10 records
