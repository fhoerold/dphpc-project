# Sysname : Linux
# Nodename: eu-a6-001-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:12:16 2022
# Execution time and date (local): Fri Jan  7 12:12:16 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 2136541.273438            0 
            3          1024          1024   gramschmidt           MPI          1 2149436.281250            5 
            3          1024          1024   gramschmidt           MPI          2 2139252.146484            4 
            3          1024          1024   gramschmidt           MPI          3 2138662.685547            0 
            3          1024          1024   gramschmidt           MPI          4 2116181.906250            1 
            3          1024          1024   gramschmidt           MPI          5 2142458.718750            0 
            3          1024          1024   gramschmidt           MPI          6 2115978.601562            0 
            3          1024          1024   gramschmidt           MPI          7 2022088.994141            0 
            3          1024          1024   gramschmidt           MPI          8 2042760.234375            2 
            3          1024          1024   gramschmidt           MPI          9 2036076.050781            0 
# Runtime: 40.324685 s (overhead: 0.000030 %) 10 records
