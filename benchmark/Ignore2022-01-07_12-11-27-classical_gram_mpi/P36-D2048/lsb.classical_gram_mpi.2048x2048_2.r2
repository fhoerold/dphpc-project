# Sysname : Linux
# Nodename: eu-a6-005-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:51:15 2022
# Execution time and date (local): Fri Jan  7 18:51:15 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 3111655.738281            2 
            2          2048          2048   gramschmidt           MPI          1 3100865.048828            5 
            2          2048          2048   gramschmidt           MPI          2 3122527.878906           12 
            2          2048          2048   gramschmidt           MPI          3 3195958.554688            0 
            2          2048          2048   gramschmidt           MPI          4 3100096.412109            1 
            2          2048          2048   gramschmidt           MPI          5 3124449.822266            0 
            2          2048          2048   gramschmidt           MPI          6 3111450.962891            0 
            2          2048          2048   gramschmidt           MPI          7 3106871.638672            0 
            2          2048          2048   gramschmidt           MPI          8 3108285.982422            5 
            2          2048          2048   gramschmidt           MPI          9 3211273.845703            0 
# Runtime: 40.593582 s (overhead: 0.000062 %) 10 records
