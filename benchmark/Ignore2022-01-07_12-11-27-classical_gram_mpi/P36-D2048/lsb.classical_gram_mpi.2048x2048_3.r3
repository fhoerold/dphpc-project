# Sysname : Linux
# Nodename: eu-a6-005-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:51:15 2022
# Execution time and date (local): Fri Jan  7 18:51:15 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 3111358.412109            0 
            3          2048          2048   gramschmidt           MPI          1 3100603.779297           11 
            3          2048          2048   gramschmidt           MPI          2 3122215.867188            2 
            3          2048          2048   gramschmidt           MPI          3 3195575.796875            0 
            3          2048          2048   gramschmidt           MPI          4 3099775.619141            1 
            3          2048          2048   gramschmidt           MPI          5 3124091.359375            3 
            3          2048          2048   gramschmidt           MPI          6 3111068.470703            0 
            3          2048          2048   gramschmidt           MPI          7 3106529.445312            0 
            3          2048          2048   gramschmidt           MPI          8 3108028.076172            5 
            3          2048          2048   gramschmidt           MPI          9 3210884.730469            0 
# Runtime: 47.558855 s (overhead: 0.000046 %) 10 records
