# Sysname : Linux
# Nodename: eu-a6-008-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:13:15 2022
# Execution time and date (local): Fri Jan  7 12:13:15 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 854650.755859            0 
            2          1024          1024   gramschmidt           MPI          1 843568.279297            4 
            2          1024          1024   gramschmidt           MPI          2 842938.130859            1 
            2          1024          1024   gramschmidt           MPI          3 849902.630859            0 
            2          1024          1024   gramschmidt           MPI          4 858013.248047            1 
            2          1024          1024   gramschmidt           MPI          5 875989.552734            0 
            2          1024          1024   gramschmidt           MPI          6 969713.066406            0 
            2          1024          1024   gramschmidt           MPI          7 876038.427734            0 
            2          1024          1024   gramschmidt           MPI          8 857078.376953            4 
            2          1024          1024   gramschmidt           MPI          9 857185.189453            0 
# Runtime: 26.314291 s (overhead: 0.000038 %) 10 records
