# Sysname : Linux
# Nodename: eu-a6-008-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:13:15 2022
# Execution time and date (local): Fri Jan  7 12:13:15 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 854977.712891            0 
            3          1024          1024   gramschmidt           MPI          1 843870.861328            5 
            3          1024          1024   gramschmidt           MPI          2 843240.833984            2 
            3          1024          1024   gramschmidt           MPI          3 850188.976562            0 
            3          1024          1024   gramschmidt           MPI          4 858291.025391            2 
            3          1024          1024   gramschmidt           MPI          5 876225.687500            0 
            3          1024          1024   gramschmidt           MPI          6 969956.970703            0 
            3          1024          1024   gramschmidt           MPI          7 876371.132812            0 
            3          1024          1024   gramschmidt           MPI          8 857308.619141            2 
            3          1024          1024   gramschmidt           MPI          9 857467.533203            0 
# Runtime: 22.318755 s (overhead: 0.000049 %) 10 records
