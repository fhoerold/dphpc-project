# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:28:40 2022
# Execution time and date (local): Fri Jan  7 12:28:40 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 433.853516            0 
            2            32            32   gramschmidt           MPI          1 412.273438            2 
            2            32            32   gramschmidt           MPI          2 475.896484            0 
            2            32            32   gramschmidt           MPI          3 398.185547            0 
            2            32            32   gramschmidt           MPI          4 393.654297            0 
            2            32            32   gramschmidt           MPI          5 380.384766            0 
            2            32            32   gramschmidt           MPI          6 397.167969            0 
            2            32            32   gramschmidt           MPI          7 328.443359            0 
            2            32            32   gramschmidt           MPI          8 312.902344            0 
            2            32            32   gramschmidt           MPI          9 337.917969            0 
# Runtime: 7.014102 s (overhead: 0.000029 %) 10 records
