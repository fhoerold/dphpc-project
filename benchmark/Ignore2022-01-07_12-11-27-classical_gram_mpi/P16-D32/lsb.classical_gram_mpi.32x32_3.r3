# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:28:40 2022
# Execution time and date (local): Fri Jan  7 12:28:40 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 430.501953            0 
            3            32            32   gramschmidt           MPI          1 410.806641            2 
            3            32            32   gramschmidt           MPI          2 467.972656            0 
            3            32            32   gramschmidt           MPI          3 385.673828            0 
            3            32            32   gramschmidt           MPI          4 393.443359            0 
            3            32            32   gramschmidt           MPI          5 375.705078            0 
            3            32            32   gramschmidt           MPI          6 392.029297            0 
            3            32            32   gramschmidt           MPI          7 325.208984            0 
            3            32            32   gramschmidt           MPI          8 309.148438            0 
            3            32            32   gramschmidt           MPI          9 335.996094            0 
# Runtime: 8.007362 s (overhead: 0.000025 %) 10 records
