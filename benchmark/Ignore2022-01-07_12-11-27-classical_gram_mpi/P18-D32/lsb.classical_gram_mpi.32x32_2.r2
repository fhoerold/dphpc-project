# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 13:47:49 2022
# Execution time and date (local): Fri Jan  7 14:47:49 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 585.722656            0 
            2            32            32   gramschmidt           MPI          1 583.343750            2 
            2            32            32   gramschmidt           MPI          2 494.019531            0 
            2            32            32   gramschmidt           MPI          3 481.609375            0 
            2            32            32   gramschmidt           MPI          4 453.167969            0 
            2            32            32   gramschmidt           MPI          5 446.613281            0 
            2            32            32   gramschmidt           MPI          6 457.771484            0 
            2            32            32   gramschmidt           MPI          7 425.869141            0 
            2            32            32   gramschmidt           MPI          8 445.919922            0 
            2            32            32   gramschmidt           MPI          9 411.951172            0 
# Runtime: 6.016332 s (overhead: 0.000033 %) 10 records
