# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 13:47:49 2022
# Execution time and date (local): Fri Jan  7 14:47:49 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 882.119141            0 
            3            32            32   gramschmidt           MPI          1 569.136719            3 
            3            32            32   gramschmidt           MPI          2 481.580078            0 
            3            32            32   gramschmidt           MPI          3 470.275391            0 
            3            32            32   gramschmidt           MPI          4 437.388672            0 
            3            32            32   gramschmidt           MPI          5 437.933594            0 
            3            32            32   gramschmidt           MPI          6 441.798828            0 
            3            32            32   gramschmidt           MPI          7 417.041016            0 
            3            32            32   gramschmidt           MPI          8 438.773438            0 
            3            32            32   gramschmidt           MPI          9 403.462891            0 
# Runtime: 6.984625 s (overhead: 0.000043 %) 10 records
