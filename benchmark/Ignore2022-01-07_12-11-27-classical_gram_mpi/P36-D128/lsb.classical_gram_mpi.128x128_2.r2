# Sysname : Linux
# Nodename: eu-a6-005-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:50:08 2022
# Execution time and date (local): Fri Jan  7 18:50:08 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 4222.859375            0 
            2           128           128   gramschmidt           MPI          1 3834.376953            2 
            2           128           128   gramschmidt           MPI          2 3758.919922            0 
            2           128           128   gramschmidt           MPI          3 3751.931641            0 
            2           128           128   gramschmidt           MPI          4 3732.414062            0 
            2           128           128   gramschmidt           MPI          5 3743.839844            0 
            2           128           128   gramschmidt           MPI          6 3723.150391            0 
            2           128           128   gramschmidt           MPI          7 3765.505859            0 
            2           128           128   gramschmidt           MPI          8 3715.839844            0 
            2           128           128   gramschmidt           MPI          9 3735.542969            0 
# Runtime: 8.029689 s (overhead: 0.000025 %) 10 records
