# Sysname : Linux
# Nodename: eu-a6-005-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:50:08 2022
# Execution time and date (local): Fri Jan  7 18:50:08 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 4225.490234            0 
            3           128           128   gramschmidt           MPI          1 3835.923828            2 
            3           128           128   gramschmidt           MPI          2 3765.625000            0 
            3           128           128   gramschmidt           MPI          3 3757.386719            0 
            3           128           128   gramschmidt           MPI          4 3739.234375            0 
            3           128           128   gramschmidt           MPI          5 3747.115234            0 
            3           128           128   gramschmidt           MPI          6 3726.935547            0 
            3           128           128   gramschmidt           MPI          7 3772.138672            0 
            3           128           128   gramschmidt           MPI          8 3718.908203            0 
            3           128           128   gramschmidt           MPI          9 3741.705078            0 
# Runtime: 5.060010 s (overhead: 0.000040 %) 10 records
