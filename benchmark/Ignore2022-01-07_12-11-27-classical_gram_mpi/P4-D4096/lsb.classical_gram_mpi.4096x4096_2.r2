# Sysname : Linux
# Nodename: eu-a6-005-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:12:14 2022
# Execution time and date (local): Fri Jan  7 12:12:14 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 264392604.740234            0 
            2          4096          4096   gramschmidt           MPI          1 250689056.373047            4 
            2          4096          4096   gramschmidt           MPI          2 259101041.406250            6 
            2          4096          4096   gramschmidt           MPI          3 257883809.761719            2 
            2          4096          4096   gramschmidt           MPI          4 254120577.732422            6 
            2          4096          4096   gramschmidt           MPI          5 254838601.144531            2 
            2          4096          4096   gramschmidt           MPI          6 262892393.267578            0 
            2          4096          4096   gramschmidt           MPI          7 241455254.804688            2 
            2          4096          4096   gramschmidt           MPI          8 249366402.146484            4 
            2          4096          4096   gramschmidt           MPI          9 246648513.302734            0 
# Runtime: 3325.557195 s (overhead: 0.000001 %) 10 records
