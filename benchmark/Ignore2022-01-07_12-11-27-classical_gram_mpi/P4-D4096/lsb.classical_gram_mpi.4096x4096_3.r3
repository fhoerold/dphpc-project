# Sysname : Linux
# Nodename: eu-a6-005-12
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:12:14 2022
# Execution time and date (local): Fri Jan  7 12:12:14 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 264405324.416016            0 
            3          4096          4096   gramschmidt           MPI          1 250701051.695312            6 
            3          4096          4096   gramschmidt           MPI          2 259113327.199219            7 
            3          4096          4096   gramschmidt           MPI          3 257895803.804688            1 
            3          4096          4096   gramschmidt           MPI          4 254132739.574219            7 
            3          4096          4096   gramschmidt           MPI          5 254850602.597656            0 
            3          4096          4096   gramschmidt           MPI          6 262904634.525391            1 
            3          4096          4096   gramschmidt           MPI          7 241467232.082031            1 
            3          4096          4096   gramschmidt           MPI          8 249378470.671875            4 
            3          4096          4096   gramschmidt           MPI          9 246660419.636719            0 
# Runtime: 3325.611313 s (overhead: 0.000001 %) 10 records
