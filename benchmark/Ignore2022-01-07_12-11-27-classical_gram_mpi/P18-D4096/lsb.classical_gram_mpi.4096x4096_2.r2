# Sysname : Linux
# Nodename: eu-a6-003-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 13:56:19 2022
# Execution time and date (local): Fri Jan  7 14:56:19 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 43629015.048828            0 
            2          4096          4096   gramschmidt           MPI          1 44540974.226562            4 
            2          4096          4096   gramschmidt           MPI          2 43105123.441406            8 
            2          4096          4096   gramschmidt           MPI          3 43189933.267578            0 
            2          4096          4096   gramschmidt           MPI          4 44153957.908203            7 
            2          4096          4096   gramschmidt           MPI          5 44031626.548828            0 
            2          4096          4096   gramschmidt           MPI          6 44263885.369141            2 
            2          4096          4096   gramschmidt           MPI          7 42826978.541016            2 
            2          4096          4096   gramschmidt           MPI          8 44427064.189453            8 
            2          4096          4096   gramschmidt           MPI          9 43274934.369141            0 
# Runtime: 574.153415 s (overhead: 0.000005 %) 10 records
