# Sysname : Linux
# Nodename: eu-a6-003-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 13:56:19 2022
# Execution time and date (local): Fri Jan  7 14:56:19 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 43630214.720703            0 
            3          4096          4096   gramschmidt           MPI          1 44542852.263672            4 
            3          4096          4096   gramschmidt           MPI          2 43107131.861328            8 
            3          4096          4096   gramschmidt           MPI          3 43191900.042969            0 
            3          4096          4096   gramschmidt           MPI          4 44155169.796875           12 
            3          4096          4096   gramschmidt           MPI          5 44033513.388672            2 
            3          4096          4096   gramschmidt           MPI          6 44265767.533203            2 
            3          4096          4096   gramschmidt           MPI          7 42828999.664062            2 
            3          4096          4096   gramschmidt           MPI          8 44428303.861328            3 
            3          4096          4096   gramschmidt           MPI          9 43276865.085938            1 
# Runtime: 574.151299 s (overhead: 0.000006 %) 10 records
