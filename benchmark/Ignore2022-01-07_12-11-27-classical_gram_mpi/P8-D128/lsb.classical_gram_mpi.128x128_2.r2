# Sysname : Linux
# Nodename: eu-a6-008-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:13:06 2022
# Execution time and date (local): Fri Jan  7 12:13:06 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 1924.658203            0 
            2           128           128   gramschmidt           MPI          1 1729.242188            0 
            2           128           128   gramschmidt           MPI          2 1650.814453            0 
            2           128           128   gramschmidt           MPI          3 1632.251953            0 
            2           128           128   gramschmidt           MPI          4 1661.542969            0 
            2           128           128   gramschmidt           MPI          5 1657.001953            0 
            2           128           128   gramschmidt           MPI          6 1676.697266            0 
            2           128           128   gramschmidt           MPI          7 1734.712891            0 
            2           128           128   gramschmidt           MPI          8 1646.007812            0 
            2           128           128   gramschmidt           MPI          9 1740.125000            0 
# Runtime: 0.026534 s (overhead: 0.000000 %) 10 records
