# Sysname : Linux
# Nodename: eu-a6-008-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:13:06 2022
# Execution time and date (local): Fri Jan  7 12:13:06 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 1912.916016            0 
            3           128           128   gramschmidt           MPI          1 1712.302734            0 
            3           128           128   gramschmidt           MPI          2 1622.496094            0 
            3           128           128   gramschmidt           MPI          3 1609.091797            0 
            3           128           128   gramschmidt           MPI          4 1633.386719            0 
            3           128           128   gramschmidt           MPI          5 1636.150391            0 
            3           128           128   gramschmidt           MPI          6 1657.728516            0 
            3           128           128   gramschmidt           MPI          7 1702.882812            0 
            3           128           128   gramschmidt           MPI          8 1646.605469            0 
            3           128           128   gramschmidt           MPI          9 1731.201172            0 
# Runtime: 0.032626 s (overhead: 0.000000 %) 10 records
