# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:12:14 2022
# Execution time and date (local): Fri Jan  7 12:12:14 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 15957405.253906            0 
            2          2048          2048   gramschmidt           MPI          1 16259190.375000            3 
            2          2048          2048   gramschmidt           MPI          2 15968061.113281            2 
            2          2048          2048   gramschmidt           MPI          3 16130116.783203            0 
            2          2048          2048   gramschmidt           MPI          4 16063342.044922            5 
            2          2048          2048   gramschmidt           MPI          5 16049484.156250            0 
            2          2048          2048   gramschmidt           MPI          6 16131271.853516            0 
            2          2048          2048   gramschmidt           MPI          7 16219567.392578            0 
            2          2048          2048   gramschmidt           MPI          8 19232823.447266            9 
            2          2048          2048   gramschmidt           MPI          9 18920791.324219            0 
# Runtime: 215.786656 s (overhead: 0.000009 %) 10 records
