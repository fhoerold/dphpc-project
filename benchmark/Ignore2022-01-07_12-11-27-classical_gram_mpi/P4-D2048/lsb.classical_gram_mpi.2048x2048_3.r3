# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:12:14 2022
# Execution time and date (local): Fri Jan  7 12:12:14 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 15968464.384766            0 
            3          2048          2048   gramschmidt           MPI          1 16261271.265625            5 
            3          2048          2048   gramschmidt           MPI          2 15970964.730469            2 
            3          2048          2048   gramschmidt           MPI          3 16132279.517578            0 
            3          2048          2048   gramschmidt           MPI          4 16065450.750000            5 
            3          2048          2048   gramschmidt           MPI          5 16052429.806641            0 
            3          2048          2048   gramschmidt           MPI          6 16133384.322266            0 
            3          2048          2048   gramschmidt           MPI          7 16221669.507812            0 
            3          2048          2048   gramschmidt           MPI          8 19234997.953125            7 
            3          2048          2048   gramschmidt           MPI          9 18923688.287109            0 
# Runtime: 214.788622 s (overhead: 0.000009 %) 10 records
