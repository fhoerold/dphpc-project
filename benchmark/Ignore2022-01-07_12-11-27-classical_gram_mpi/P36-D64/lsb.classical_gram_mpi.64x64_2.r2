# Sysname : Linux
# Nodename: eu-a6-005-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:49:54 2022
# Execution time and date (local): Fri Jan  7 18:49:54 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 1809.781250            0 
            2            64            64   gramschmidt           MPI          1 1681.964844            2 
            2            64            64   gramschmidt           MPI          2 1670.396484            0 
            2            64            64   gramschmidt           MPI          3 1646.292969            0 
            2            64            64   gramschmidt           MPI          4 1619.791016            0 
            2            64            64   gramschmidt           MPI          5 1602.271484            0 
            2            64            64   gramschmidt           MPI          6 1579.386719            0 
            2            64            64   gramschmidt           MPI          7 1640.775391            0 
            2            64            64   gramschmidt           MPI          8 1515.457031            0 
            2            64            64   gramschmidt           MPI          9 1538.796875            0 
# Runtime: 7.046091 s (overhead: 0.000028 %) 10 records
