# Sysname : Linux
# Nodename: eu-a6-005-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:49:54 2022
# Execution time and date (local): Fri Jan  7 18:49:54 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 1736.052734            0 
            3            64            64   gramschmidt           MPI          1 1652.015625            2 
            3            64            64   gramschmidt           MPI          2 1633.277344            0 
            3            64            64   gramschmidt           MPI          3 1607.089844            0 
            3            64            64   gramschmidt           MPI          4 1577.330078            0 
            3            64            64   gramschmidt           MPI          5 1576.898438            0 
            3            64            64   gramschmidt           MPI          6 1553.021484            0 
            3            64            64   gramschmidt           MPI          7 1611.910156            0 
            3            64            64   gramschmidt           MPI          8 1479.175781            0 
            3            64            64   gramschmidt           MPI          9 1509.351562            0 
# Runtime: 4.036666 s (overhead: 0.000050 %) 10 records
