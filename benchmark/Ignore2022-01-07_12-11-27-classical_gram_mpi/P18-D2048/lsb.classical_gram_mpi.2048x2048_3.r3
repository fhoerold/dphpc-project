# Sysname : Linux
# Nodename: eu-a6-002-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 13:54:20 2022
# Execution time and date (local): Fri Jan  7 14:54:20 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 2760291.537109            0 
            3          2048          2048   gramschmidt           MPI          1 2755505.423828            4 
            3          2048          2048   gramschmidt           MPI          2 2726146.927734            2 
            3          2048          2048   gramschmidt           MPI          3 2732838.511719            0 
            3          2048          2048   gramschmidt           MPI          4 2748392.408203            5 
            3          2048          2048   gramschmidt           MPI          5 2732557.826172            0 
            3          2048          2048   gramschmidt           MPI          6 2764632.001953            0 
            3          2048          2048   gramschmidt           MPI          7 2714679.224609            0 
            3          2048          2048   gramschmidt           MPI          8 2680132.017578            2 
            3          2048          2048   gramschmidt           MPI          9 2685598.337891            0 
# Runtime: 50.461501 s (overhead: 0.000026 %) 10 records
