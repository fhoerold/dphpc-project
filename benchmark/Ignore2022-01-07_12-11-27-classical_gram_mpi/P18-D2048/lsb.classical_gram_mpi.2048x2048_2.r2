# Sysname : Linux
# Nodename: eu-a6-002-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 13:54:20 2022
# Execution time and date (local): Fri Jan  7 14:54:20 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 2759842.589844            0 
            2          2048          2048   gramschmidt           MPI          1 2755145.328125            5 
            2          2048          2048   gramschmidt           MPI          2 2725771.861328            2 
            2          2048          2048   gramschmidt           MPI          3 2732503.455078            0 
            2          2048          2048   gramschmidt           MPI          4 2748020.708984            6 
            2          2048          2048   gramschmidt           MPI          5 2732050.054688            1 
            2          2048          2048   gramschmidt           MPI          6 2764310.525391            0 
            2          2048          2048   gramschmidt           MPI          7 2714315.501953            0 
            2          2048          2048   gramschmidt           MPI          8 2679757.681641            2 
            2          2048          2048   gramschmidt           MPI          9 2685240.603516            0 
# Runtime: 44.461929 s (overhead: 0.000036 %) 10 records
