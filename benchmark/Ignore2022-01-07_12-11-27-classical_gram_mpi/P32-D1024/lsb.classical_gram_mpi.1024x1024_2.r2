# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:06:59 2022
# Execution time and date (local): Fri Jan  7 17:06:59 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 348383.197266            0 
            2          1024          1024   gramschmidt           MPI          1 351423.431641            4 
            2          1024          1024   gramschmidt           MPI          2 352767.822266            2 
            2          1024          1024   gramschmidt           MPI          3 338453.257812            0 
            2          1024          1024   gramschmidt           MPI          4 335481.414062            2 
            2          1024          1024   gramschmidt           MPI          5 341261.634766            0 
            2          1024          1024   gramschmidt           MPI          6 326996.230469            0 
            2          1024          1024   gramschmidt           MPI          7 402756.912109            0 
            2          1024          1024   gramschmidt           MPI          8 320803.572266            2 
            2          1024          1024   gramschmidt           MPI          9 318130.601562            0 
# Runtime: 22.583822 s (overhead: 0.000044 %) 10 records
