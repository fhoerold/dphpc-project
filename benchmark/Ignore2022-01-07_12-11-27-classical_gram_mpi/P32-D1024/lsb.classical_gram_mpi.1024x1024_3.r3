# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:06:59 2022
# Execution time and date (local): Fri Jan  7 17:06:59 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 348604.187500            0 
            3          1024          1024   gramschmidt           MPI          1 351556.419922            5 
            3          1024          1024   gramschmidt           MPI          2 352908.322266            2 
            3          1024          1024   gramschmidt           MPI          3 338664.599609            0 
            3          1024          1024   gramschmidt           MPI          4 335789.318359            3 
            3          1024          1024   gramschmidt           MPI          5 341453.851562            0 
            3          1024          1024   gramschmidt           MPI          6 327164.892578            0 
            3          1024          1024   gramschmidt           MPI          7 402915.273438            0 
            3          1024          1024   gramschmidt           MPI          8 320913.933594            2 
            3          1024          1024   gramschmidt           MPI          9 318266.455078            0 
# Runtime: 17.559976 s (overhead: 0.000068 %) 10 records
