# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:35:16 2022
# Execution time and date (local): Fri Jan  7 12:35:16 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 75185.859375            0 
            2           512           512   gramschmidt           MPI          1 86593.230469            4 
            2           512           512   gramschmidt           MPI          2 89048.445312            1 
            2           512           512   gramschmidt           MPI          3 70731.671875            0 
            2           512           512   gramschmidt           MPI          4 50625.832031            1 
            2           512           512   gramschmidt           MPI          5 51118.410156            0 
            2           512           512   gramschmidt           MPI          6 50159.416016            0 
            2           512           512   gramschmidt           MPI          7 50941.664062            0 
            2           512           512   gramschmidt           MPI          8 49835.929688            2 
            2           512           512   gramschmidt           MPI          9 50937.468750            0 
# Runtime: 6.876141 s (overhead: 0.000116 %) 10 records
