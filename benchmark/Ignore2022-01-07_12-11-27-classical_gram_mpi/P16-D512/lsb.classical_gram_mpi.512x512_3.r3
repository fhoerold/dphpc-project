# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:35:16 2022
# Execution time and date (local): Fri Jan  7 12:35:16 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 75175.597656            0 
            3           512           512   gramschmidt           MPI          1 86568.345703            4 
            3           512           512   gramschmidt           MPI          2 89033.625000            1 
            3           512           512   gramschmidt           MPI          3 70705.783203            0 
            3           512           512   gramschmidt           MPI          4 50625.818359            1 
            3           512           512   gramschmidt           MPI          5 51111.658203            0 
            3           512           512   gramschmidt           MPI          6 50165.550781            0 
            3           512           512   gramschmidt           MPI          7 50920.835938            0 
            3           512           512   gramschmidt           MPI          8 49834.353516            1 
            3           512           512   gramschmidt           MPI          9 50928.238281            0 
# Runtime: 11.857423 s (overhead: 0.000059 %) 10 records
