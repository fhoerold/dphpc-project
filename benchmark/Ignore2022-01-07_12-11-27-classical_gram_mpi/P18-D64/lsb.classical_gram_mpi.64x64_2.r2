# Sysname : Linux
# Nodename: eu-a6-002-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 13:52:50 2022
# Execution time and date (local): Fri Jan  7 14:52:50 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 1192.218750            0 
            2            64            64   gramschmidt           MPI          1 1190.843750            2 
            2            64            64   gramschmidt           MPI          2 1146.722656            0 
            2            64            64   gramschmidt           MPI          3 1129.240234            0 
            2            64            64   gramschmidt           MPI          4 1099.931641            0 
            2            64            64   gramschmidt           MPI          5 1107.675781            0 
            2            64            64   gramschmidt           MPI          6 1113.277344            0 
            2            64            64   gramschmidt           MPI          7 1125.177734            0 
            2            64            64   gramschmidt           MPI          8 1129.974609            0 
            2            64            64   gramschmidt           MPI          9 1125.152344            0 
# Runtime: 9.028728 s (overhead: 0.000022 %) 10 records
