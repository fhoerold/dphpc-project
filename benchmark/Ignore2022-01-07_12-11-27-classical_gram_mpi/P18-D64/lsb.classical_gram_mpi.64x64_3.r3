# Sysname : Linux
# Nodename: eu-a6-002-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 13:52:50 2022
# Execution time and date (local): Fri Jan  7 14:52:50 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 1204.052734            0 
            3            64            64   gramschmidt           MPI          1 1190.898438            3 
            3            64            64   gramschmidt           MPI          2 1146.173828            0 
            3            64            64   gramschmidt           MPI          3 1129.523438            0 
            3            64            64   gramschmidt           MPI          4 1097.523438            0 
            3            64            64   gramschmidt           MPI          5 1108.085938            0 
            3            64            64   gramschmidt           MPI          6 1112.261719            0 
            3            64            64   gramschmidt           MPI          7 1123.033203            0 
            3            64            64   gramschmidt           MPI          8 1130.312500            0 
            3            64            64   gramschmidt           MPI          9 1124.046875            0 
# Runtime: 13.035848 s (overhead: 0.000023 %) 10 records
