# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:06:20 2022
# Execution time and date (local): Fri Jan  7 17:06:20 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 3761.964844            0 
            2           128           128   gramschmidt           MPI          1 3692.185547            3 
            2           128           128   gramschmidt           MPI          2 3670.205078            0 
            2           128           128   gramschmidt           MPI          3 3690.296875            0 
            2           128           128   gramschmidt           MPI          4 3701.453125            0 
            2           128           128   gramschmidt           MPI          5 3632.468750            0 
            2           128           128   gramschmidt           MPI          6 3713.759766            0 
            2           128           128   gramschmidt           MPI          7 3658.736328            0 
            2           128           128   gramschmidt           MPI          8 3670.595703            0 
            2           128           128   gramschmidt           MPI          9 3742.539062            0 
# Runtime: 3.068404 s (overhead: 0.000098 %) 10 records
