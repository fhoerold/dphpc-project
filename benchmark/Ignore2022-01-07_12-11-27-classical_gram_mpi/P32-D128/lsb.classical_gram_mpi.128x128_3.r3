# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:06:20 2022
# Execution time and date (local): Fri Jan  7 17:06:20 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 3764.982422            0 
            3           128           128   gramschmidt           MPI          1 3692.699219            2 
            3           128           128   gramschmidt           MPI          2 3671.029297            0 
            3           128           128   gramschmidt           MPI          3 3692.261719            0 
            3           128           128   gramschmidt           MPI          4 3701.710938            0 
            3           128           128   gramschmidt           MPI          5 3635.953125            0 
            3           128           128   gramschmidt           MPI          6 3714.197266            0 
            3           128           128   gramschmidt           MPI          7 3659.734375            0 
            3           128           128   gramschmidt           MPI          8 3671.349609            0 
            3           128           128   gramschmidt           MPI          9 3744.130859            0 
# Runtime: 3.064703 s (overhead: 0.000065 %) 10 records
