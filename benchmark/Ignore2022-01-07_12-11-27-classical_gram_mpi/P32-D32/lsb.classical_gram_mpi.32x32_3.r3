# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 15:59:51 2022
# Execution time and date (local): Fri Jan  7 16:59:51 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 891.207031            0 
            3            32            32   gramschmidt           MPI          1 804.207031            3 
            3            32            32   gramschmidt           MPI          2 790.246094            0 
            3            32            32   gramschmidt           MPI          3 753.396484            0 
            3            32            32   gramschmidt           MPI          4 735.355469            0 
            3            32            32   gramschmidt           MPI          5 742.390625            0 
            3            32            32   gramschmidt           MPI          6 763.621094            0 
            3            32            32   gramschmidt           MPI          7 760.919922            0 
            3            32            32   gramschmidt           MPI          8 719.306641            0 
            3            32            32   gramschmidt           MPI          9 731.433594            0 
# Runtime: 8.026760 s (overhead: 0.000037 %) 10 records
