# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 15:59:51 2022
# Execution time and date (local): Fri Jan  7 16:59:51 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 896.671875            0 
            2            32            32   gramschmidt           MPI          1 810.945312            2 
            2            32            32   gramschmidt           MPI          2 795.914062            0 
            2            32            32   gramschmidt           MPI          3 762.261719            0 
            2            32            32   gramschmidt           MPI          4 742.248047            0 
            2            32            32   gramschmidt           MPI          5 748.769531            0 
            2            32            32   gramschmidt           MPI          6 772.525391            0 
            2            32            32   gramschmidt           MPI          7 766.982422            0 
            2            32            32   gramschmidt           MPI          8 729.343750            0 
            2            32            32   gramschmidt           MPI          9 740.283203            0 
# Runtime: 8.029315 s (overhead: 0.000025 %) 10 records
