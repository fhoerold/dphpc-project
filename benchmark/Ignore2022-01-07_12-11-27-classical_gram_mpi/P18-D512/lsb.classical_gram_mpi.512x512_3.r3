# Sysname : Linux
# Nodename: eu-a6-002-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 13:53:29 2022
# Execution time and date (local): Fri Jan  7 14:53:29 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 85112.162109            0 
            3           512           512   gramschmidt           MPI          1 68839.416016            4 
            3           512           512   gramschmidt           MPI          2 64333.431641            1 
            3           512           512   gramschmidt           MPI          3 100397.882812            0 
            3           512           512   gramschmidt           MPI          4 83607.480469            1 
            3           512           512   gramschmidt           MPI          5 99509.224609            0 
            3           512           512   gramschmidt           MPI          6 65369.412109            0 
            3           512           512   gramschmidt           MPI          7 64775.759766            0 
            3           512           512   gramschmidt           MPI          8 66665.187500            2 
            3           512           512   gramschmidt           MPI          9 65012.166016            0 
# Runtime: 9.008060 s (overhead: 0.000089 %) 10 records
