# Sysname : Linux
# Nodename: eu-a6-002-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 13:53:29 2022
# Execution time and date (local): Fri Jan  7 14:53:29 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 85266.275391            0 
            2           512           512   gramschmidt           MPI          1 68443.916016            4 
            2           512           512   gramschmidt           MPI          2 64160.103516            2 
            2           512           512   gramschmidt           MPI          3 100301.988281            0 
            2           512           512   gramschmidt           MPI          4 83458.552734            1 
            2           512           512   gramschmidt           MPI          5 99413.287109            0 
            2           512           512   gramschmidt           MPI          6 65183.117188            0 
            2           512           512   gramschmidt           MPI          7 64607.761719            0 
            2           512           512   gramschmidt           MPI          8 66497.236328            1 
            2           512           512   gramschmidt           MPI          9 64850.882812            0 
# Runtime: 6.037716 s (overhead: 0.000133 %) 10 records
