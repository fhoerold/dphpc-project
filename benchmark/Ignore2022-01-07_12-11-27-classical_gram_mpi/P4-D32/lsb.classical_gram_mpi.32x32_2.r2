# Sysname : Linux
# Nodename: eu-a6-002-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:12:15 2022
# Execution time and date (local): Fri Jan  7 12:12:15 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 303.736328            0 
            2            32            32   gramschmidt           MPI          1 310.810547            0 
            2            32            32   gramschmidt           MPI          2 332.386719            0 
            2            32            32   gramschmidt           MPI          3 294.671875            0 
            2            32            32   gramschmidt           MPI          4 292.486328            0 
            2            32            32   gramschmidt           MPI          5 275.757812            0 
            2            32            32   gramschmidt           MPI          6 283.482422            0 
            2            32            32   gramschmidt           MPI          7 278.052734            0 
            2            32            32   gramschmidt           MPI          8 280.005859            0 
            2            32            32   gramschmidt           MPI          9 287.460938            0 
# Runtime: 0.007282 s (overhead: 0.000000 %) 10 records
