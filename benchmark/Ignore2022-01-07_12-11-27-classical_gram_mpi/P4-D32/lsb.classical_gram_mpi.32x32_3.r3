# Sysname : Linux
# Nodename: eu-a6-002-19
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:12:15 2022
# Execution time and date (local): Fri Jan  7 12:12:15 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 295.998047            0 
            3            32            32   gramschmidt           MPI          1 306.355469            0 
            3            32            32   gramschmidt           MPI          2 328.962891            0 
            3            32            32   gramschmidt           MPI          3 289.640625            0 
            3            32            32   gramschmidt           MPI          4 289.968750            0 
            3            32            32   gramschmidt           MPI          5 270.876953            0 
            3            32            32   gramschmidt           MPI          6 279.427734            0 
            3            32            32   gramschmidt           MPI          7 274.619141            0 
            3            32            32   gramschmidt           MPI          8 275.750000            0 
            3            32            32   gramschmidt           MPI          9 284.515625            0 
# Runtime: 0.005139 s (overhead: 0.000000 %) 10 records
