# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:29:11 2022
# Execution time and date (local): Fri Jan  7 12:29:11 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 3184023.822266            0 
            2          2048          2048   gramschmidt           MPI          1 3258065.320312            6 
            2          2048          2048   gramschmidt           MPI          2 3309847.800781            2 
            2          2048          2048   gramschmidt           MPI          3 3219688.425781            0 
            2          2048          2048   gramschmidt           MPI          4 3207819.341797            2 
            2          2048          2048   gramschmidt           MPI          5 3163011.611328            0 
            2          2048          2048   gramschmidt           MPI          6 3215070.013672            0 
            2          2048          2048   gramschmidt           MPI          7 3209459.785156            0 
            2          2048          2048   gramschmidt           MPI          8 3232907.855469            6 
            2          2048          2048   gramschmidt           MPI          9 3230990.027344            0 
# Runtime: 46.069047 s (overhead: 0.000035 %) 10 records
