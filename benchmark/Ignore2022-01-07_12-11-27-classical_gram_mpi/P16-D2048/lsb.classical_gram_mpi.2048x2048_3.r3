# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:29:11 2022
# Execution time and date (local): Fri Jan  7 12:29:11 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 3184436.406250            0 
            3          2048          2048   gramschmidt           MPI          1 3258253.554688            4 
            3          2048          2048   gramschmidt           MPI          2 3310050.435547            2 
            3          2048          2048   gramschmidt           MPI          3 3219904.060547            0 
            3          2048          2048   gramschmidt           MPI          4 3208087.451172            2 
            3          2048          2048   gramschmidt           MPI          5 3163234.957031            2 
            3          2048          2048   gramschmidt           MPI          6 3215279.884766            0 
            3          2048          2048   gramschmidt           MPI          7 3209869.396484            0 
            3          2048          2048   gramschmidt           MPI          8 3233126.357422            5 
            3          2048          2048   gramschmidt           MPI          9 3231252.228516            0 
# Runtime: 47.069706 s (overhead: 0.000032 %) 10 records
