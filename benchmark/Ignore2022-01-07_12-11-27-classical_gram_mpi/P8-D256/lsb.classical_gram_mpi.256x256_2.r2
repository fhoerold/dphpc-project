# Sysname : Linux
# Nodename: eu-a6-006-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:13:13 2022
# Execution time and date (local): Fri Jan  7 12:13:13 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 8541.750000            0 
            2           256           256   gramschmidt           MPI          1 8527.542969            4 
            2           256           256   gramschmidt           MPI          2 8641.523438            1 
            2           256           256   gramschmidt           MPI          3 8505.498047            0 
            2           256           256   gramschmidt           MPI          4 8390.787109            1 
            2           256           256   gramschmidt           MPI          5 8401.062500            0 
            2           256           256   gramschmidt           MPI          6 8479.101562            0 
            2           256           256   gramschmidt           MPI          7 8421.562500            0 
            2           256           256   gramschmidt           MPI          8 8377.976562            1 
            2           256           256   gramschmidt           MPI          9 8448.361328            0 
# Runtime: 6.122733 s (overhead: 0.000114 %) 10 records
