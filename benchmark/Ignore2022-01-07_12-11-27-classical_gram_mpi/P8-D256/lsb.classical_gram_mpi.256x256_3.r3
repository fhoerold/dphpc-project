# Sysname : Linux
# Nodename: eu-a6-006-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:13:13 2022
# Execution time and date (local): Fri Jan  7 12:13:13 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 8525.605469            0 
            3           256           256   gramschmidt           MPI          1 8524.130859            4 
            3           256           256   gramschmidt           MPI          2 8636.416016            1 
            3           256           256   gramschmidt           MPI          3 8499.390625            0 
            3           256           256   gramschmidt           MPI          4 8382.671875            1 
            3           256           256   gramschmidt           MPI          5 8396.449219            0 
            3           256           256   gramschmidt           MPI          6 8461.128906            0 
            3           256           256   gramschmidt           MPI          7 8407.791016            0 
            3           256           256   gramschmidt           MPI          8 8367.013672            1 
            3           256           256   gramschmidt           MPI          9 8439.078125            0 
# Runtime: 7.113133 s (overhead: 0.000098 %) 10 records
