# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:36:50 2022
# Execution time and date (local): Fri Jan  7 12:36:50 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 50564601.857422            0 
            2          4096          4096   gramschmidt           MPI          1 50711319.843750            6 
            2          4096          4096   gramschmidt           MPI          2 50767623.863281            5 
            2          4096          4096   gramschmidt           MPI          3 50644844.785156            0 
            2          4096          4096   gramschmidt           MPI          4 50551446.714844            7 
            2          4096          4096   gramschmidt           MPI          5 50585356.445312            2 
            2          4096          4096   gramschmidt           MPI          6 50657878.310547            1 
            2          4096          4096   gramschmidt           MPI          7 50546621.376953            0 
            2          4096          4096   gramschmidt           MPI          8 50662631.289062            8 
            2          4096          4096   gramschmidt           MPI          9 50767653.953125            2 
# Runtime: 660.512535 s (overhead: 0.000005 %) 10 records
