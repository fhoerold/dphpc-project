# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:36:50 2022
# Execution time and date (local): Fri Jan  7 12:36:50 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 50562184.662109            0 
            3          4096          4096   gramschmidt           MPI          1 50708889.359375            4 
            3          4096          4096   gramschmidt           MPI          2 50765250.197266            3 
            3          4096          4096   gramschmidt           MPI          3 50641643.455078            0 
            3          4096          4096   gramschmidt           MPI          4 50549039.416016            4 
            3          4096          4096   gramschmidt           MPI          5 50582332.759766            2 
            3          4096          4096   gramschmidt           MPI          6 50654830.763672            2 
            3          4096          4096   gramschmidt           MPI          7 50543802.951172            2 
            3          4096          4096   gramschmidt           MPI          8 50659600.279297            6 
            3          4096          4096   gramschmidt           MPI          9 50763939.732422            0 
# Runtime: 667.448928 s (overhead: 0.000003 %) 10 records
