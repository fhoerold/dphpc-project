# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:06:39 2022
# Execution time and date (local): Fri Jan  7 17:06:39 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 52176.091797            0 
            3           512           512   gramschmidt           MPI          1 51212.900391            5 
            3           512           512   gramschmidt           MPI          2 50924.048828            2 
            3           512           512   gramschmidt           MPI          3 50725.292969            0 
            3           512           512   gramschmidt           MPI          4 50957.560547            2 
            3           512           512   gramschmidt           MPI          5 50812.804688            0 
            3           512           512   gramschmidt           MPI          6 50827.060547            0 
            3           512           512   gramschmidt           MPI          7 50685.662109            0 
            3           512           512   gramschmidt           MPI          8 54631.292969            2 
            3           512           512   gramschmidt           MPI          9 51276.718750            0 
# Runtime: 12.035092 s (overhead: 0.000091 %) 10 records
