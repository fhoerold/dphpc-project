# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:07:31 2022
# Execution time and date (local): Fri Jan  7 17:07:31 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 2144296.712891            0 
            3          2048          2048   gramschmidt           MPI          1 2161502.062500            5 
            3          2048          2048   gramschmidt           MPI          2 2052436.443359           16 
            3          2048          2048   gramschmidt           MPI          3 2113285.380859            0 
            3          2048          2048   gramschmidt           MPI          4 2031775.210938            1 
            3          2048          2048   gramschmidt           MPI          5 2052936.302734            0 
            3          2048          2048   gramschmidt           MPI          6 2013131.550781            0 
            3          2048          2048   gramschmidt           MPI          7 1995695.484375            0 
            3          2048          2048   gramschmidt           MPI          8 2014100.109375            2 
            3          2048          2048   gramschmidt           MPI          9 2084377.841797            0 
# Runtime: 33.885303 s (overhead: 0.000071 %) 10 records
