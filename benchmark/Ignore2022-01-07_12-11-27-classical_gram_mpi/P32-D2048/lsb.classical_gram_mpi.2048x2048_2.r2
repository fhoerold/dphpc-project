# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:07:31 2022
# Execution time and date (local): Fri Jan  7 17:07:31 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 2144181.892578            0 
            2          2048          2048   gramschmidt           MPI          1 2161447.699219            5 
            2          2048          2048   gramschmidt           MPI          2 2052001.164062            2 
            2          2048          2048   gramschmidt           MPI          3 2113176.185547            3 
            2          2048          2048   gramschmidt           MPI          4 2031719.371094            5 
            2          2048          2048   gramschmidt           MPI          5 2052866.281250            1 
            2          2048          2048   gramschmidt           MPI          6 2013118.462891            0 
            2          2048          2048   gramschmidt           MPI          7 1995677.101562            0 
            2          2048          2048   gramschmidt           MPI          8 2014027.330078            2 
            2          2048          2048   gramschmidt           MPI          9 2084208.441406            0 
# Runtime: 33.908608 s (overhead: 0.000053 %) 10 records
