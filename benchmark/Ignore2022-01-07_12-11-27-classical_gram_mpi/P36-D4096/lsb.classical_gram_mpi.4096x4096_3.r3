# Sysname : Linux
# Nodename: eu-a6-005-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:52:21 2022
# Execution time and date (local): Fri Jan  7 18:52:21 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 31031086.712891            0 
            3          4096          4096   gramschmidt           MPI          1 31046467.060547            6 
            3          4096          4096   gramschmidt           MPI          2 31168944.162109            2 
            3          4096          4096   gramschmidt           MPI          3 30982830.011719            2 
            3          4096          4096   gramschmidt           MPI          4 31378184.888672            6 
            3          4096          4096   gramschmidt           MPI          5 31304243.167969            0 
            3          4096          4096   gramschmidt           MPI          6 31233349.730469            0 
            3          4096          4096   gramschmidt           MPI          7 30923970.560547            2 
            3          4096          4096   gramschmidt           MPI          8 31317989.308594            6 
            3          4096          4096   gramschmidt           MPI          9 31029539.333984            2 
# Runtime: 410.818413 s (overhead: 0.000006 %) 10 records
