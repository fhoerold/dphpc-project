# Sysname : Linux
# Nodename: eu-a6-005-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:52:21 2022
# Execution time and date (local): Fri Jan  7 18:52:21 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 31021179.210938            4 
            2          4096          4096   gramschmidt           MPI          1 31036506.535156            8 
            2          4096          4096   gramschmidt           MPI          2 31158595.185547            2 
            2          4096          4096   gramschmidt           MPI          3 30972916.705078            2 
            2          4096          4096   gramschmidt           MPI          4 31367684.023438            6 
            2          4096          4096   gramschmidt           MPI          5 31294281.185547            2 
            2          4096          4096   gramschmidt           MPI          6 31222914.406250            0 
            2          4096          4096   gramschmidt           MPI          7 30914047.388672            3 
            2          4096          4096   gramschmidt           MPI          8 31307453.265625            5 
            2          4096          4096   gramschmidt           MPI          9 31019605.914062            3 
# Runtime: 409.692542 s (overhead: 0.000009 %) 10 records
