# Sysname : Linux
# Nodename: eu-a6-008-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:12:51 2022
# Execution time and date (local): Fri Jan  7 12:12:51 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 706.337891            0 
            3            64            64   gramschmidt           MPI          1 602.746094            0 
            3            64            64   gramschmidt           MPI          2 569.974609            0 
            3            64            64   gramschmidt           MPI          3 558.109375            0 
            3            64            64   gramschmidt           MPI          4 549.021484            0 
            3            64            64   gramschmidt           MPI          5 526.679688            0 
            3            64            64   gramschmidt           MPI          6 531.619141            0 
            3            64            64   gramschmidt           MPI          7 540.968750            0 
            3            64            64   gramschmidt           MPI          8 535.070312            0 
            3            64            64   gramschmidt           MPI          9 538.140625            0 
# Runtime: 0.009557 s (overhead: 0.000000 %) 10 records
