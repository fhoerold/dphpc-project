# Sysname : Linux
# Nodename: eu-a6-008-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:12:51 2022
# Execution time and date (local): Fri Jan  7 12:12:51 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 722.300781            0 
            2            64            64   gramschmidt           MPI          1 618.224609            2 
            2            64            64   gramschmidt           MPI          2 577.841797            0 
            2            64            64   gramschmidt           MPI          3 557.326172            0 
            2            64            64   gramschmidt           MPI          4 552.048828            0 
            2            64            64   gramschmidt           MPI          5 529.728516            0 
            2            64            64   gramschmidt           MPI          6 534.033203            0 
            2            64            64   gramschmidt           MPI          7 551.431641            0 
            2            64            64   gramschmidt           MPI          8 542.218750            0 
            2            64            64   gramschmidt           MPI          9 540.673828            0 
# Runtime: 9.010623 s (overhead: 0.000022 %) 10 records
