# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:14:14 2022
# Execution time and date (local): Fri Jan  7 12:14:14 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          2048          2048   gramschmidt           MPI          0 11718368.027344            0 
            2          2048          2048   gramschmidt           MPI          1 11729517.138672            4 
            2          2048          2048   gramschmidt           MPI          2 11659038.390625            2 
            2          2048          2048   gramschmidt           MPI          3 11608623.783203            0 
            2          2048          2048   gramschmidt           MPI          4 10132714.908203            6 
            2          2048          2048   gramschmidt           MPI          5 8932780.449219            0 
            2          2048          2048   gramschmidt           MPI          6 9245619.457031            0 
            2          2048          2048   gramschmidt           MPI          7 8665157.169922            0 
            2          2048          2048   gramschmidt           MPI          8 8567151.908203            2 
            2          2048          2048   gramschmidt           MPI          9 8717305.599609            1 
# Runtime: 144.812313 s (overhead: 0.000010 %) 10 records
