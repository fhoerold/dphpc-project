# Sysname : Linux
# Nodename: eu-a6-008-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:14:14 2022
# Execution time and date (local): Fri Jan  7 12:14:14 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          2048          2048   gramschmidt           MPI          0 11719949.240234            0 
            3          2048          2048   gramschmidt           MPI          1 11731439.384766            4 
            3          2048          2048   gramschmidt           MPI          2 11660572.748047            2 
            3          2048          2048   gramschmidt           MPI          3 11610564.041016            0 
            3          2048          2048   gramschmidt           MPI          4 10134127.138672            4 
            3          2048          2048   gramschmidt           MPI          5 8934141.119141            0 
            3          2048          2048   gramschmidt           MPI          6 9247029.783203            0 
            3          2048          2048   gramschmidt           MPI          7 8666553.294922            0 
            3          2048          2048   gramschmidt           MPI          8 8568494.996094            2 
            3          2048          2048   gramschmidt           MPI          9 8719147.992188            0 
# Runtime: 146.820047 s (overhead: 0.000008 %) 10 records
