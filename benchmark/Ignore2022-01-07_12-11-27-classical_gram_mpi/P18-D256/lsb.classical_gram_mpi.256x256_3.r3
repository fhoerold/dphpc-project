# Sysname : Linux
# Nodename: eu-a6-002-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 13:53:10 2022
# Execution time and date (local): Fri Jan  7 14:53:10 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 12650.957031            0 
            3           256           256   gramschmidt           MPI          1 10478.716797            3 
            3           256           256   gramschmidt           MPI          2 13779.205078            0 
            3           256           256   gramschmidt           MPI          3 12546.947266            0 
            3           256           256   gramschmidt           MPI          4 17247.140625            1 
            3           256           256   gramschmidt           MPI          5 13282.904297            0 
            3           256           256   gramschmidt           MPI          6 12087.685547            0 
            3           256           256   gramschmidt           MPI          7 10318.398438            0 
            3           256           256   gramschmidt           MPI          8 10202.048828            1 
            3           256           256   gramschmidt           MPI          9 9945.882812            0 
# Runtime: 10.161685 s (overhead: 0.000049 %) 10 records
