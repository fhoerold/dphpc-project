# Sysname : Linux
# Nodename: eu-a6-002-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 13:53:10 2022
# Execution time and date (local): Fri Jan  7 14:53:10 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 12733.931641            0 
            2           256           256   gramschmidt           MPI          1 10480.591797            4 
            2           256           256   gramschmidt           MPI          2 13801.853516            1 
            2           256           256   gramschmidt           MPI          3 12543.908203            0 
            2           256           256   gramschmidt           MPI          4 17245.017578            0 
            2           256           256   gramschmidt           MPI          5 13284.925781            0 
            2           256           256   gramschmidt           MPI          6 12086.521484            0 
            2           256           256   gramschmidt           MPI          7 10321.234375            0 
            2           256           256   gramschmidt           MPI          8 10203.138672            1 
            2           256           256   gramschmidt           MPI          9 9945.947266            0 
# Runtime: 10.156041 s (overhead: 0.000059 %) 10 records
