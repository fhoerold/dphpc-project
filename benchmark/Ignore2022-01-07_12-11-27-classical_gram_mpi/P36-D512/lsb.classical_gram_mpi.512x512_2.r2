# Sysname : Linux
# Nodename: eu-a6-005-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:48:08 2022
# Execution time and date (local): Fri Jan  7 18:48:08 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 80084.638672            0 
            2           512           512   gramschmidt           MPI          1 102431.914062            5 
            2           512           512   gramschmidt           MPI          2 107590.646484            2 
            2           512           512   gramschmidt           MPI          3 110789.574219            0 
            2           512           512   gramschmidt           MPI          4 81896.587891            2 
            2           512           512   gramschmidt           MPI          5 63737.853516            0 
            2           512           512   gramschmidt           MPI          6 61933.041016            0 
            2           512           512   gramschmidt           MPI          7 62692.455078            0 
            2           512           512   gramschmidt           MPI          8 62168.378906            1 
            2           512           512   gramschmidt           MPI          9 61835.972656            0 
# Runtime: 17.378596 s (overhead: 0.000058 %) 10 records
