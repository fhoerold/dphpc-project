# Sysname : Linux
# Nodename: eu-a6-005-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:48:08 2022
# Execution time and date (local): Fri Jan  7 18:48:08 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 80060.648438            0 
            3           512           512   gramschmidt           MPI          1 102462.251953            4 
            3           512           512   gramschmidt           MPI          2 107614.777344            2 
            3           512           512   gramschmidt           MPI          3 110816.212891            0 
            3           512           512   gramschmidt           MPI          4 81918.580078            2 
            3           512           512   gramschmidt           MPI          5 63746.509766            1 
            3           512           512   gramschmidt           MPI          6 61939.781250            0 
            3           512           512   gramschmidt           MPI          7 62702.074219            0 
            3           512           512   gramschmidt           MPI          8 62186.689453            1 
            3           512           512   gramschmidt           MPI          9 61855.365234            0 
# Runtime: 16.390371 s (overhead: 0.000061 %) 10 records
