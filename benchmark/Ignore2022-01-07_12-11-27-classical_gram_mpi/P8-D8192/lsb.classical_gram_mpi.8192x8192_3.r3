# Sysname : Linux
# Nodename: eu-a6-008-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:15:44 2022
# Execution time and date (local): Fri Jan  7 12:15:44 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          8192          8192   gramschmidt           MPI          0 1498669599.132812            3 
            3          8192          8192   gramschmidt           MPI          1 1503271333.320312           10 
            3          8192          8192   gramschmidt           MPI          2 1515431387.773438            8 
            3          8192          8192   gramschmidt           MPI          3 1507599226.751953            2 
            3          8192          8192   gramschmidt           MPI          4 1525476344.558594           19 
            3          8192          8192   gramschmidt           MPI          5 1498762616.542969            1 
            3          8192          8192   gramschmidt           MPI          6 1499346178.916016            3 
            3          8192          8192   gramschmidt           MPI          7 1505902704.810547            3 
            3          8192          8192   gramschmidt           MPI          8 1498954566.609375            7 
            3          8192          8192   gramschmidt           MPI          9 1502154892.761719            0 
# Runtime: 19593.805303 s (overhead: 0.000000 %) 10 records
