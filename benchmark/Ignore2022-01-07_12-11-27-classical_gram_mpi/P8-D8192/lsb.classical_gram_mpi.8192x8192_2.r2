# Sysname : Linux
# Nodename: eu-a6-008-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:15:44 2022
# Execution time and date (local): Fri Jan  7 12:15:44 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          8192          8192   gramschmidt           MPI          0 1498152079.578125            2 
            2          8192          8192   gramschmidt           MPI          1 1502752520.259766           10 
            2          8192          8192   gramschmidt           MPI          2 1514906924.093750            8 
            2          8192          8192   gramschmidt           MPI          3 1507078233.580078            2 
            2          8192          8192   gramschmidt           MPI          4 1524949726.648438            6 
            2          8192          8192   gramschmidt           MPI          5 1498244952.796875            3 
            2          8192          8192   gramschmidt           MPI          6 1498828815.775391            2 
            2          8192          8192   gramschmidt           MPI          7 1505382343.255859            3 
            2          8192          8192   gramschmidt           MPI          8 1498436547.093750           11 
            2          8192          8192   gramschmidt           MPI          9 1501635848.134766            3 
# Runtime: 19589.229697 s (overhead: 0.000000 %) 10 records
