# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:35:02 2022
# Execution time and date (local): Fri Jan  7 12:35:02 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 14517.835938            0 
            2           256           256   gramschmidt           MPI          1 11843.537109            3 
            2           256           256   gramschmidt           MPI          2 10153.306641            0 
            2           256           256   gramschmidt           MPI          3 13702.707031            0 
            2           256           256   gramschmidt           MPI          4 10023.207031            0 
            2           256           256   gramschmidt           MPI          5 6665.134766            0 
            2           256           256   gramschmidt           MPI          6 6500.949219            0 
            2           256           256   gramschmidt           MPI          7 9130.482422            0 
            2           256           256   gramschmidt           MPI          8 9525.892578            3 
            2           256           256   gramschmidt           MPI          9 6587.935547            0 
# Runtime: 8.159407 s (overhead: 0.000074 %) 10 records
