# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:35:02 2022
# Execution time and date (local): Fri Jan  7 12:35:02 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 14512.003906            0 
            3           256           256   gramschmidt           MPI          1 11855.669922            3 
            3           256           256   gramschmidt           MPI          2 10146.099609            0 
            3           256           256   gramschmidt           MPI          3 13697.074219            0 
            3           256           256   gramschmidt           MPI          4 10019.349609            0 
            3           256           256   gramschmidt           MPI          5 6657.529297            0 
            3           256           256   gramschmidt           MPI          6 6495.212891            0 
            3           256           256   gramschmidt           MPI          7 9118.054688            0 
            3           256           256   gramschmidt           MPI          8 9519.029297            0 
            3           256           256   gramschmidt           MPI          9 6582.794922            0 
# Runtime: 3.152887 s (overhead: 0.000095 %) 10 records
