# Sysname : Linux
# Nodename: eu-a6-005-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:06:25 2022
# Execution time and date (local): Fri Jan  7 19:06:25 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          8192          8192   gramschmidt           MPI          0 269241344.990234            3 
            2          8192          8192   gramschmidt           MPI          1 271836718.414062            6 
            2          8192          8192   gramschmidt           MPI          2 270974665.035156           11 
            2          8192          8192   gramschmidt           MPI          3 270557809.453125            3 
            2          8192          8192   gramschmidt           MPI          4 270735199.849609           20 
            2          8192          8192   gramschmidt           MPI          5 268265370.251953            4 
            2          8192          8192   gramschmidt           MPI          6 271085599.326172            3 
            2          8192          8192   gramschmidt           MPI          7 270479932.914062            4 
            2          8192          8192   gramschmidt           MPI          8 271907659.771484           12 
            2          8192          8192   gramschmidt           MPI          9 270928952.419922            2 
# Runtime: 3538.121339 s (overhead: 0.000002 %) 10 records
