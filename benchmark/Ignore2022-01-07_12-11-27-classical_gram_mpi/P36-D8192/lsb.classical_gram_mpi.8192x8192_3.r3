# Sysname : Linux
# Nodename: eu-a6-005-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 18:06:25 2022
# Execution time and date (local): Fri Jan  7 19:06:25 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          8192          8192   gramschmidt           MPI          0 269249232.732422            3 
            3          8192          8192   gramschmidt           MPI          1 271844592.691406            9 
            3          8192          8192   gramschmidt           MPI          2 270982788.099609            8 
            3          8192          8192   gramschmidt           MPI          3 270565918.419922            0 
            3          8192          8192   gramschmidt           MPI          4 270743233.818359            6 
            3          8192          8192   gramschmidt           MPI          5 268273472.636719            2 
            3          8192          8192   gramschmidt           MPI          6 271093487.851562            4 
            3          8192          8192   gramschmidt           MPI          7 270488056.701172            4 
            3          8192          8192   gramschmidt           MPI          8 271915689.468750            8 
            3          8192          8192   gramschmidt           MPI          9 270937070.931641            2 
# Runtime: 3538.196780 s (overhead: 0.000001 %) 10 records
