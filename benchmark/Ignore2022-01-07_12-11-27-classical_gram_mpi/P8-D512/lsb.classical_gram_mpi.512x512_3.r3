# Sysname : Linux
# Nodename: eu-a6-004-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:13:43 2022
# Execution time and date (local): Fri Jan  7 12:13:43 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 67913.539062            0 
            3           512           512   gramschmidt           MPI          1 68707.902344            4 
            3           512           512   gramschmidt           MPI          2 67964.179688            2 
            3           512           512   gramschmidt           MPI          3 68832.136719            0 
            3           512           512   gramschmidt           MPI          4 67658.792969            1 
            3           512           512   gramschmidt           MPI          5 66776.275391            0 
            3           512           512   gramschmidt           MPI          6 67906.933594            0 
            3           512           512   gramschmidt           MPI          7 67925.556641            0 
            3           512           512   gramschmidt           MPI          8 68051.742188            1 
            3           512           512   gramschmidt           MPI          9 67510.037109            0 
# Runtime: 6.880267 s (overhead: 0.000116 %) 10 records
