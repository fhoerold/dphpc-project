# Sysname : Linux
# Nodename: eu-a6-004-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:13:43 2022
# Execution time and date (local): Fri Jan  7 12:13:43 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 67677.833984            0 
            2           512           512   gramschmidt           MPI          1 68469.287109            5 
            2           512           512   gramschmidt           MPI          2 67721.912109            1 
            2           512           512   gramschmidt           MPI          3 68591.578125            0 
            2           512           512   gramschmidt           MPI          4 67424.677734            1 
            2           512           512   gramschmidt           MPI          5 66532.017578            0 
            2           512           512   gramschmidt           MPI          6 67659.259766            0 
            2           512           512   gramschmidt           MPI          7 67642.246094            0 
            2           512           512   gramschmidt           MPI          8 67803.578125            1 
            2           512           512   gramschmidt           MPI          9 67250.783203            0 
# Runtime: 3.891570 s (overhead: 0.000206 %) 10 records
