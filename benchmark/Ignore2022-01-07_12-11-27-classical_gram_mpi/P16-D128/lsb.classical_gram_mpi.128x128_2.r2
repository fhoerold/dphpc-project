# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:34:45 2022
# Execution time and date (local): Fri Jan  7 12:34:45 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 2044.525391            0 
            2           128           128   gramschmidt           MPI          1 1910.523438            2 
            2           128           128   gramschmidt           MPI          2 1871.880859            0 
            2           128           128   gramschmidt           MPI          3 1852.927734            0 
            2           128           128   gramschmidt           MPI          4 1873.894531            0 
            2           128           128   gramschmidt           MPI          5 1833.447266            0 
            2           128           128   gramschmidt           MPI          6 1853.417969            0 
            2           128           128   gramschmidt           MPI          7 1786.089844            0 
            2           128           128   gramschmidt           MPI          8 1794.382812            0 
            2           128           128   gramschmidt           MPI          9 1714.351562            0 
# Runtime: 9.030014 s (overhead: 0.000022 %) 10 records
