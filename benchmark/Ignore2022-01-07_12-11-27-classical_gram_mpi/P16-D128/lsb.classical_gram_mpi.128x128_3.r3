# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:34:45 2022
# Execution time and date (local): Fri Jan  7 12:34:45 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 2047.251953            0 
            3           128           128   gramschmidt           MPI          1 1911.392578            0 
            3           128           128   gramschmidt           MPI          2 1870.662109            0 
            3           128           128   gramschmidt           MPI          3 1848.998047            0 
            3           128           128   gramschmidt           MPI          4 1874.380859            0 
            3           128           128   gramschmidt           MPI          5 1835.714844            0 
            3           128           128   gramschmidt           MPI          6 1854.082031            0 
            3           128           128   gramschmidt           MPI          7 1787.652344            0 
            3           128           128   gramschmidt           MPI          8 1795.187500            0 
            3           128           128   gramschmidt           MPI          9 1717.748047            0 
# Runtime: 0.028122 s (overhead: 0.000000 %) 10 records
