# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:00:25 2022
# Execution time and date (local): Fri Jan  7 17:00:25 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 12102.902344            0 
            3           256           256   gramschmidt           MPI          1 11973.429688            3 
            3           256           256   gramschmidt           MPI          2 11851.414062            0 
            3           256           256   gramschmidt           MPI          3 15735.400391            0 
            3           256           256   gramschmidt           MPI          4 12262.179688            0 
            3           256           256   gramschmidt           MPI          5 11952.525391            0 
            3           256           256   gramschmidt           MPI          6 14102.505859            0 
            3           256           256   gramschmidt           MPI          7 11768.949219            0 
            3           256           256   gramschmidt           MPI          8 11625.072266            0 
            3           256           256   gramschmidt           MPI          9 11710.076172            0 
# Runtime: 10.210566 s (overhead: 0.000029 %) 10 records
