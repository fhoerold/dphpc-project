# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:00:25 2022
# Execution time and date (local): Fri Jan  7 17:00:25 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 12077.195312            0 
            2           256           256   gramschmidt           MPI          1 11967.343750            3 
            2           256           256   gramschmidt           MPI          2 11845.224609            0 
            2           256           256   gramschmidt           MPI          3 15725.888672            0 
            2           256           256   gramschmidt           MPI          4 12257.412109            0 
            2           256           256   gramschmidt           MPI          5 11946.160156            0 
            2           256           256   gramschmidt           MPI          6 14096.222656            0 
            2           256           256   gramschmidt           MPI          7 11764.058594            0 
            2           256           256   gramschmidt           MPI          8 11616.580078            0 
            2           256           256   gramschmidt           MPI          9 11709.990234            0 
# Runtime: 10.211185 s (overhead: 0.000029 %) 10 records
