# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 13:48:03 2022
# Execution time and date (local): Fri Jan  7 14:48:03 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 2728.066406            0 
            3           128           128   gramschmidt           MPI          1 2422.892578            3 
            3           128           128   gramschmidt           MPI          2 2325.406250            0 
            3           128           128   gramschmidt           MPI          3 2279.253906            0 
            3           128           128   gramschmidt           MPI          4 2181.585938            0 
            3           128           128   gramschmidt           MPI          5 2240.082031            0 
            3           128           128   gramschmidt           MPI          6 2224.416016            0 
            3           128           128   gramschmidt           MPI          7 2204.242188            0 
            3           128           128   gramschmidt           MPI          8 2184.853516            0 
            3           128           128   gramschmidt           MPI          9 2236.542969            0 
# Runtime: 3.039458 s (overhead: 0.000099 %) 10 records
