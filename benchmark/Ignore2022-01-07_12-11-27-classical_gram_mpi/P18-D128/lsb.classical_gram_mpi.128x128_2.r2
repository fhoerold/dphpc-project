# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 13:48:03 2022
# Execution time and date (local): Fri Jan  7 14:48:03 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 2466.986328            0 
            2           128           128   gramschmidt           MPI          1 2424.847656            0 
            2           128           128   gramschmidt           MPI          2 2325.619141            0 
            2           128           128   gramschmidt           MPI          3 2280.226562            0 
            2           128           128   gramschmidt           MPI          4 2181.425781            0 
            2           128           128   gramschmidt           MPI          5 2242.783203            0 
            2           128           128   gramschmidt           MPI          6 2225.617188            0 
            2           128           128   gramschmidt           MPI          7 2209.070312            0 
            2           128           128   gramschmidt           MPI          8 2184.833984            0 
            2           128           128   gramschmidt           MPI          9 2237.656250            0 
# Runtime: 0.043538 s (overhead: 0.000000 %) 10 records
