# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:28:55 2022
# Execution time and date (local): Fri Jan  7 12:28:55 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 835.666016            0 
            2            64            64   gramschmidt           MPI          1 773.767578            2 
            2            64            64   gramschmidt           MPI          2 722.251953            0 
            2            64            64   gramschmidt           MPI          3 829.685547            0 
            2            64            64   gramschmidt           MPI          4 763.035156            0 
            2            64            64   gramschmidt           MPI          5 718.371094            0 
            2            64            64   gramschmidt           MPI          6 744.541016            0 
            2            64            64   gramschmidt           MPI          7 668.865234            0 
            2            64            64   gramschmidt           MPI          8 706.460938            0 
            2            64            64   gramschmidt           MPI          9 665.751953            0 
# Runtime: 8.023100 s (overhead: 0.000025 %) 10 records
