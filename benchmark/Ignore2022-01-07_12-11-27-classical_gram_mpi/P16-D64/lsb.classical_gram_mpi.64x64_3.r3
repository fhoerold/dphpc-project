# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:28:55 2022
# Execution time and date (local): Fri Jan  7 12:28:55 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 833.009766            0 
            3            64            64   gramschmidt           MPI          1 774.341797            2 
            3            64            64   gramschmidt           MPI          2 721.488281            0 
            3            64            64   gramschmidt           MPI          3 830.458984            0 
            3            64            64   gramschmidt           MPI          4 763.464844            0 
            3            64            64   gramschmidt           MPI          5 717.468750            0 
            3            64            64   gramschmidt           MPI          6 744.906250            0 
            3            64            64   gramschmidt           MPI          7 669.847656            0 
            3            64            64   gramschmidt           MPI          8 708.376953            0 
            3            64            64   gramschmidt           MPI          9 665.134766            0 
# Runtime: 9.015670 s (overhead: 0.000022 %) 10 records
