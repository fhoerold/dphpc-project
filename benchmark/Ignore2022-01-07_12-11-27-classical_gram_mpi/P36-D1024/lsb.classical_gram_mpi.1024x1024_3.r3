# Sysname : Linux
# Nodename: eu-a6-005-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:50:47 2022
# Execution time and date (local): Fri Jan  7 18:50:47 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 753119.511719            0 
            3          1024          1024   gramschmidt           MPI          1 691173.027344            5 
            3          1024          1024   gramschmidt           MPI          2 295893.003906            1 
            3          1024          1024   gramschmidt           MPI          3 276072.361328            0 
            3          1024          1024   gramschmidt           MPI          4 274241.753906            2 
            3          1024          1024   gramschmidt           MPI          5 457211.890625            0 
            3          1024          1024   gramschmidt           MPI          6 275366.451172            0 
            3          1024          1024   gramschmidt           MPI          7 274587.767578            0 
            3          1024          1024   gramschmidt           MPI          8 279400.566406            2 
            3          1024          1024   gramschmidt           MPI          9 275506.097656            0 
# Runtime: 20.984234 s (overhead: 0.000048 %) 10 records
