# Sysname : Linux
# Nodename: eu-a6-005-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:50:47 2022
# Execution time and date (local): Fri Jan  7 18:50:47 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 753130.394531            0 
            2          1024          1024   gramschmidt           MPI          1 691142.755859            4 
            2          1024          1024   gramschmidt           MPI          2 295876.132812            2 
            2          1024          1024   gramschmidt           MPI          3 276034.873047            0 
            2          1024          1024   gramschmidt           MPI          4 274233.423828            1 
            2          1024          1024   gramschmidt           MPI          5 457178.294922            0 
            2          1024          1024   gramschmidt           MPI          6 275343.402344            0 
            2          1024          1024   gramschmidt           MPI          7 274569.917969            0 
            2          1024          1024   gramschmidt           MPI          8 279385.646484           11 
            2          1024          1024   gramschmidt           MPI          9 275494.556641            0 
# Runtime: 13.979317 s (overhead: 0.000129 %) 10 records
