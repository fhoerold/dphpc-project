# Sysname : Linux
# Nodename: eu-a6-004-22
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:12:14 2022
# Execution time and date (local): Fri Jan  7 12:12:14 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 14623.373047            0 
            2           256           256   gramschmidt           MPI          1 14648.529297            1 
            2           256           256   gramschmidt           MPI          2 14492.427734            1 
            2           256           256   gramschmidt           MPI          3 14305.628906            0 
            2           256           256   gramschmidt           MPI          4 14491.765625            1 
            2           256           256   gramschmidt           MPI          5 14338.777344            0 
            2           256           256   gramschmidt           MPI          6 14424.876953            0 
            2           256           256   gramschmidt           MPI          7 14356.935547            0 
            2           256           256   gramschmidt           MPI          8 14694.593750            1 
            2           256           256   gramschmidt           MPI          9 14487.353516            0 
# Runtime: 0.194181 s (overhead: 0.002060 %) 10 records
