# Sysname : Linux
# Nodename: eu-a6-005-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:47:53 2022
# Execution time and date (local): Fri Jan  7 18:47:53 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 1096.562500            2 
            3            32            32   gramschmidt           MPI          1 787.267578            3 
            3            32            32   gramschmidt           MPI          2 778.546875            0 
            3            32            32   gramschmidt           MPI          3 708.263672            0 
            3            32            32   gramschmidt           MPI          4 709.689453            0 
            3            32            32   gramschmidt           MPI          5 668.980469            0 
            3            32            32   gramschmidt           MPI          6 660.275391            0 
            3            32            32   gramschmidt           MPI          7 647.078125            0 
            3            32            32   gramschmidt           MPI          8 667.337891            0 
            3            32            32   gramschmidt           MPI          9 643.949219            0 
# Runtime: 3.008560 s (overhead: 0.000166 %) 10 records
