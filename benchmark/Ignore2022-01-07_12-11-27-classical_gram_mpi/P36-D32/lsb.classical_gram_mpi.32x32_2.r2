# Sysname : Linux
# Nodename: eu-a6-005-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:47:53 2022
# Execution time and date (local): Fri Jan  7 18:47:53 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 1456.708984            0 
            2            32            32   gramschmidt           MPI          1 791.335938            3 
            2            32            32   gramschmidt           MPI          2 793.828125            0 
            2            32            32   gramschmidt           MPI          3 719.890625            0 
            2            32            32   gramschmidt           MPI          4 723.130859            0 
            2            32            32   gramschmidt           MPI          5 679.472656            0 
            2            32            32   gramschmidt           MPI          6 670.892578            0 
            2            32            32   gramschmidt           MPI          7 652.761719            0 
            2            32            32   gramschmidt           MPI          8 673.015625            0 
            2            32            32   gramschmidt           MPI          9 652.644531            0 
# Runtime: 6.025792 s (overhead: 0.000050 %) 10 records
