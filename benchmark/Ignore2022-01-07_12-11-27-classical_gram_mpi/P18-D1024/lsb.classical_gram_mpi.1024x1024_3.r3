# Sysname : Linux
# Nodename: eu-a6-002-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 13:53:44 2022
# Execution time and date (local): Fri Jan  7 14:53:44 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 323244.951172            0 
            3          1024          1024   gramschmidt           MPI          1 321919.107422            6 
            3          1024          1024   gramschmidt           MPI          2 322385.296875            2 
            3          1024          1024   gramschmidt           MPI          3 322266.357422            0 
            3          1024          1024   gramschmidt           MPI          4 321879.521484            1 
            3          1024          1024   gramschmidt           MPI          5 322841.347656            0 
            3          1024          1024   gramschmidt           MPI          6 322502.781250            0 
            3          1024          1024   gramschmidt           MPI          7 322063.730469            0 
            3          1024          1024   gramschmidt           MPI          8 323565.324219            1 
            3          1024          1024   gramschmidt           MPI          9 365917.587891            0 
# Runtime: 4.307658 s (overhead: 0.000232 %) 10 records
