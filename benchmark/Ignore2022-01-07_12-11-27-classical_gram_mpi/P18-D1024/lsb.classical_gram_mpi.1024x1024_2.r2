# Sysname : Linux
# Nodename: eu-a6-002-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 13:53:44 2022
# Execution time and date (local): Fri Jan  7 14:53:44 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 324478.121094            0 
            2          1024          1024   gramschmidt           MPI          1 323108.712891            4 
            2          1024          1024   gramschmidt           MPI          2 323583.585938            2 
            2          1024          1024   gramschmidt           MPI          3 323461.699219            0 
            2          1024          1024   gramschmidt           MPI          4 323074.615234            1 
            2          1024          1024   gramschmidt           MPI          5 324090.019531            0 
            2          1024          1024   gramschmidt           MPI          6 323729.798828            0 
            2          1024          1024   gramschmidt           MPI          7 323299.953125            0 
            2          1024          1024   gramschmidt           MPI          8 324816.835938            3 
            2          1024          1024   gramschmidt           MPI          9 367122.658203            0 
# Runtime: 20.308861 s (overhead: 0.000049 %) 10 records
