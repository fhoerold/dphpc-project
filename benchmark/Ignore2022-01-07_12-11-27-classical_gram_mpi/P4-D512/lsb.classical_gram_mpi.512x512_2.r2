# Sysname : Linux
# Nodename: eu-a6-001-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:12:14 2022
# Execution time and date (local): Fri Jan  7 12:12:14 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           512           512   gramschmidt           MPI          0 144546.046875            0 
            2           512           512   gramschmidt           MPI          1 145173.931641            2 
            2           512           512   gramschmidt           MPI          2 147174.126953            1 
            2           512           512   gramschmidt           MPI          3 149833.400391            0 
            2           512           512   gramschmidt           MPI          4 145686.417969            5 
            2           512           512   gramschmidt           MPI          5 143818.511719            0 
            2           512           512   gramschmidt           MPI          6 143633.869141            0 
            2           512           512   gramschmidt           MPI          7 143425.984375            0 
            2           512           512   gramschmidt           MPI          8 143884.330078            2 
            2           512           512   gramschmidt           MPI          9 146454.978516            0 
# Runtime: 1.895624 s (overhead: 0.000528 %) 10 records
