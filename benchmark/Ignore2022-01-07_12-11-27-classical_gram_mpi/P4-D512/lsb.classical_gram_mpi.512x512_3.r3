# Sysname : Linux
# Nodename: eu-a6-001-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:12:14 2022
# Execution time and date (local): Fri Jan  7 12:12:14 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           512           512   gramschmidt           MPI          0 144299.185547            0 
            3           512           512   gramschmidt           MPI          1 144920.435547            7 
            3           512           512   gramschmidt           MPI          2 146919.408203            2 
            3           512           512   gramschmidt           MPI          3 149625.146484            0 
            3           512           512   gramschmidt           MPI          4 145441.679688            6 
            3           512           512   gramschmidt           MPI          5 143556.916016            0 
            3           512           512   gramschmidt           MPI          6 143350.312500            0 
            3           512           512   gramschmidt           MPI          7 143176.275391            0 
            3           512           512   gramschmidt           MPI          8 143620.285156            2 
            3           512           512   gramschmidt           MPI          9 146193.298828            0 
# Runtime: 5.901995 s (overhead: 0.000288 %) 10 records
