# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:09:50 2022
# Execution time and date (local): Fri Jan  7 17:09:50 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 29853554.228516            0 
            3          4096          4096   gramschmidt           MPI          1 30363907.597656            5 
            3          4096          4096   gramschmidt           MPI          2 29761740.529297            3 
            3          4096          4096   gramschmidt           MPI          3 30060420.380859            0 
            3          4096          4096   gramschmidt           MPI          4 29898406.638672            7 
            3          4096          4096   gramschmidt           MPI          5 30137510.433594            3 
            3          4096          4096   gramschmidt           MPI          6 29948661.761719            0 
            3          4096          4096   gramschmidt           MPI          7 30167231.921875            5 
            3          4096          4096   gramschmidt           MPI          8 29853367.605469            8 
            3          4096          4096   gramschmidt           MPI          9 30035913.964844            6 
# Runtime: 397.052731 s (overhead: 0.000009 %) 10 records
