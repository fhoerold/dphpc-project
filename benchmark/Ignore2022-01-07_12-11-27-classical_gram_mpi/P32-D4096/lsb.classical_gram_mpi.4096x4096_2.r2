# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:09:50 2022
# Execution time and date (local): Fri Jan  7 17:09:50 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 29850809.464844            1 
            2          4096          4096   gramschmidt           MPI          1 30360848.210938            8 
            2          4096          4096   gramschmidt           MPI          2 29759054.230469            3 
            2          4096          4096   gramschmidt           MPI          3 30057405.902344            0 
            2          4096          4096   gramschmidt           MPI          4 29895730.701172            3 
            2          4096          4096   gramschmidt           MPI          5 30134387.511719            0 
            2          4096          4096   gramschmidt           MPI          6 29945964.310547            0 
            2          4096          4096   gramschmidt           MPI          7 30164119.646484            0 
            2          4096          4096   gramschmidt           MPI          8 29850664.269531            6 
            2          4096          4096   gramschmidt           MPI          9 30032888.441406            2 
# Runtime: 398.045526 s (overhead: 0.000006 %) 10 records
