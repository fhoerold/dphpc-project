# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:35:37 2022
# Execution time and date (local): Fri Jan  7 12:35:37 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          1024          1024   gramschmidt           MPI          0 324434.531250            0 
            2          1024          1024   gramschmidt           MPI          1 304334.757812            4 
            2          1024          1024   gramschmidt           MPI          2 304031.429688            2 
            2          1024          1024   gramschmidt           MPI          3 303142.906250            0 
            2          1024          1024   gramschmidt           MPI          4 309001.138672            4 
            2          1024          1024   gramschmidt           MPI          5 347779.742188            0 
            2          1024          1024   gramschmidt           MPI          6 308445.437500            0 
            2          1024          1024   gramschmidt           MPI          7 345296.857422            0 
            2          1024          1024   gramschmidt           MPI          8 298249.218750            2 
            2          1024          1024   gramschmidt           MPI          9 314286.517578            0 
# Runtime: 6.172304 s (overhead: 0.000194 %) 10 records
