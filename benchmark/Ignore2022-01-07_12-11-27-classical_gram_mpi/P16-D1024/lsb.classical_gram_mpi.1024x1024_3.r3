# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:35:37 2022
# Execution time and date (local): Fri Jan  7 12:35:37 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          1024          1024   gramschmidt           MPI          0 323177.339844            0 
            3          1024          1024   gramschmidt           MPI          1 303099.589844            5 
            3          1024          1024   gramschmidt           MPI          2 302768.347656            2 
            3          1024          1024   gramschmidt           MPI          3 301857.613281            0 
            3          1024          1024   gramschmidt           MPI          4 307745.601562            2 
            3          1024          1024   gramschmidt           MPI          5 346638.142578            0 
            3          1024          1024   gramschmidt           MPI          6 307292.642578            0 
            3          1024          1024   gramschmidt           MPI          7 344134.421875            0 
            3          1024          1024   gramschmidt           MPI          8 297098.521484            2 
            3          1024          1024   gramschmidt           MPI          9 313102.775391            0 
# Runtime: 12.177048 s (overhead: 0.000090 %) 10 records
