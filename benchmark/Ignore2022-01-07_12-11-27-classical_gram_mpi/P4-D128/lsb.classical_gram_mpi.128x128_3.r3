# Sysname : Linux
# Nodename: eu-a6-001-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:12:14 2022
# Execution time and date (local): Fri Jan  7 12:12:14 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           128           128   gramschmidt           MPI          0 2021.232422            0 
            3           128           128   gramschmidt           MPI          1 1991.707031            0 
            3           128           128   gramschmidt           MPI          2 2016.167969            0 
            3           128           128   gramschmidt           MPI          3 1949.292969            0 
            3           128           128   gramschmidt           MPI          4 1948.232422            0 
            3           128           128   gramschmidt           MPI          5 1967.650391            0 
            3           128           128   gramschmidt           MPI          6 1950.210938            0 
            3           128           128   gramschmidt           MPI          7 2066.082031            0 
            3           128           128   gramschmidt           MPI          8 2047.871094            0 
            3           128           128   gramschmidt           MPI          9 2092.320312            0 
# Runtime: 0.034104 s (overhead: 0.000000 %) 10 records
