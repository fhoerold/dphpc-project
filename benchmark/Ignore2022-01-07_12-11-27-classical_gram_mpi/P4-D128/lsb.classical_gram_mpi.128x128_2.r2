# Sysname : Linux
# Nodename: eu-a6-001-18
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:12:14 2022
# Execution time and date (local): Fri Jan  7 12:12:14 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           128           128   gramschmidt           MPI          0 2053.494141            0 
            2           128           128   gramschmidt           MPI          1 2017.783203            0 
            2           128           128   gramschmidt           MPI          2 2054.882812            0 
            2           128           128   gramschmidt           MPI          3 1985.427734            0 
            2           128           128   gramschmidt           MPI          4 1981.505859            0 
            2           128           128   gramschmidt           MPI          5 1992.152344            0 
            2           128           128   gramschmidt           MPI          6 1974.787109            0 
            2           128           128   gramschmidt           MPI          7 2115.878906            0 
            2           128           128   gramschmidt           MPI          8 2084.560547            0 
            2           128           128   gramschmidt           MPI          9 2111.751953            0 
# Runtime: 0.031566 s (overhead: 0.000000 %) 10 records
