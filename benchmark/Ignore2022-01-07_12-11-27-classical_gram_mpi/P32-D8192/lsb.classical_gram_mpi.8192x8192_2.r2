# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:16:52 2022
# Execution time and date (local): Fri Jan  7 17:16:52 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          8192          8192   gramschmidt           MPI          0 293900446.253906            0 
            2          8192          8192   gramschmidt           MPI          1 296273900.904297            9 
            2          8192          8192   gramschmidt           MPI          2 304346852.277344            8 
            2          8192          8192   gramschmidt           MPI          3 299263520.119141            2 
            2          8192          8192   gramschmidt           MPI          4 296406021.029297            9 
            2          8192          8192   gramschmidt           MPI          5 300321635.341797            2 
            2          8192          8192   gramschmidt           MPI          6 297257690.046875            3 
            2          8192          8192   gramschmidt           MPI          7 299552060.488281            2 
            2          8192          8192   gramschmidt           MPI          8 298279756.099609           12 
            2          8192          8192   gramschmidt           MPI          9 305260153.701172            5 
# Runtime: 3880.587137 s (overhead: 0.000001 %) 10 records
