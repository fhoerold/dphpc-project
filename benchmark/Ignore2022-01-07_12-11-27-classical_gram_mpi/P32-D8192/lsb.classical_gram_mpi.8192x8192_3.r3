# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:16:52 2022
# Execution time and date (local): Fri Jan  7 17:16:52 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          8192          8192   gramschmidt           MPI          0 293863411.767578            2 
            3          8192          8192   gramschmidt           MPI          1 296236658.654297           20 
            3          8192          8192   gramschmidt           MPI          2 304308627.156250           16 
            3          8192          8192   gramschmidt           MPI          3 299225667.966797            5 
            3          8192          8192   gramschmidt           MPI          4 296368607.851562           15 
            3          8192          8192   gramschmidt           MPI          5 300283636.257812            3 
            3          8192          8192   gramschmidt           MPI          6 297220236.835938            3 
            3          8192          8192   gramschmidt           MPI          7 299514101.876953            2 
            3          8192          8192   gramschmidt           MPI          8 298242331.380859           14 
            3          8192          8192   gramschmidt           MPI          9 305221497.085938            4 
# Runtime: 3882.037841 s (overhead: 0.000002 %) 10 records
