# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:00:07 2022
# Execution time and date (local): Fri Jan  7 17:00:07 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 1843.636719            0 
            2            64            64   gramschmidt           MPI          1 1775.384766            2 
            2            64            64   gramschmidt           MPI          2 1612.310547            0 
            2            64            64   gramschmidt           MPI          3 1642.384766            0 
            2            64            64   gramschmidt           MPI          4 1558.609375            0 
            2            64            64   gramschmidt           MPI          5 1563.751953            0 
            2            64            64   gramschmidt           MPI          6 1569.890625            0 
            2            64            64   gramschmidt           MPI          7 1591.027344            0 
            2            64            64   gramschmidt           MPI          8 1561.566406            0 
            2            64            64   gramschmidt           MPI          9 1587.138672            0 
# Runtime: 10.057612 s (overhead: 0.000020 %) 10 records
