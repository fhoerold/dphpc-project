# Sysname : Linux
# Nodename: eu-a6-006-07
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:00:07 2022
# Execution time and date (local): Fri Jan  7 17:00:07 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 1783.773438            0 
            3            64            64   gramschmidt           MPI          1 1733.406250            3 
            3            64            64   gramschmidt           MPI          2 1586.919922            0 
            3            64            64   gramschmidt           MPI          3 1619.080078            0 
            3            64            64   gramschmidt           MPI          4 1531.449219            0 
            3            64            64   gramschmidt           MPI          5 1541.408203            0 
            3            64            64   gramschmidt           MPI          6 1541.611328            0 
            3            64            64   gramschmidt           MPI          7 1539.404297            0 
            3            64            64   gramschmidt           MPI          8 1541.011719            0 
            3            64            64   gramschmidt           MPI          9 1561.880859            0 
# Runtime: 5.043915 s (overhead: 0.000059 %) 10 records
