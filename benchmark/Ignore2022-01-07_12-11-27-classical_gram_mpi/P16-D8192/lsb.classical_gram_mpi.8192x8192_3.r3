# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:48:15 2022
# Execution time and date (local): Fri Jan  7 12:48:15 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          8192          8192   gramschmidt           MPI          0 547196384.619141            0 
            3          8192          8192   gramschmidt           MPI          1 576051275.591797            8 
            3          8192          8192   gramschmidt           MPI          2 551056989.179688            9 
            3          8192          8192   gramschmidt           MPI          3 555165715.560547            0 
            3          8192          8192   gramschmidt           MPI          4 554995689.457031            7 
            3          8192          8192   gramschmidt           MPI          5 542065805.544922            2 
            3          8192          8192   gramschmidt           MPI          6 545080254.503906            2 
            3          8192          8192   gramschmidt           MPI          7 538570849.962891            2 
            3          8192          8192   gramschmidt           MPI          8 533280201.910156            8 
            3          8192          8192   gramschmidt           MPI          9 546082057.923828            0 
# Runtime: 7153.769081 s (overhead: 0.000001 %) 10 records
