# Sysname : Linux
# Nodename: eu-a6-006-15
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:48:15 2022
# Execution time and date (local): Fri Jan  7 12:48:15 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          8192          8192   gramschmidt           MPI          0 547410872.130859            0 
            2          8192          8192   gramschmidt           MPI          1 576277912.802734            6 
            2          8192          8192   gramschmidt           MPI          2 551273150.904297            6 
            2          8192          8192   gramschmidt           MPI          3 555383596.304688            4 
            2          8192          8192   gramschmidt           MPI          4 555213477.261719            8 
            2          8192          8192   gramschmidt           MPI          5 542278555.714844            3 
            2          8192          8192   gramschmidt           MPI          6 545294356.542969            3 
            2          8192          8192   gramschmidt           MPI          7 538782031.470703            3 
            2          8192          8192   gramschmidt           MPI          8 533489286.330078            9 
            2          8192          8192   gramschmidt           MPI          9 546296442.035156            3 
# Runtime: 7158.701138 s (overhead: 0.000001 %) 10 records
