# Sysname : Linux
# Nodename: eu-a6-003-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 14:06:20 2022
# Execution time and date (local): Fri Jan  7 15:06:20 2022
# MPI execution on rank 2 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          8192          8192   gramschmidt           MPI          0 536762394.187500            4 
            2          8192          8192   gramschmidt           MPI          1 512670422.123047            8 
            2          8192          8192   gramschmidt           MPI          2 510343422.195312            9 
            2          8192          8192   gramschmidt           MPI          3 525755500.685547            2 
            2          8192          8192   gramschmidt           MPI          4 515325411.707031            6 
            2          8192          8192   gramschmidt           MPI          5 515082472.410156            2 
            2          8192          8192   gramschmidt           MPI          6 517533833.472656            2 
            2          8192          8192   gramschmidt           MPI          7 514095508.437500            2 
            2          8192          8192   gramschmidt           MPI          8 516054956.644531            6 
            2          8192          8192   gramschmidt           MPI          9 519957470.695312            2 
# Runtime: 6775.495220 s (overhead: 0.000001 %) 10 records
