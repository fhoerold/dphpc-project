# Sysname : Linux
# Nodename: eu-a6-003-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 14:06:20 2022
# Execution time and date (local): Fri Jan  7 15:06:20 2022
# MPI execution on rank 3 with 18 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          8192          8192   gramschmidt           MPI          0 536777412.371094            3 
            3          8192          8192   gramschmidt           MPI          1 512685145.306641           13 
            3          8192          8192   gramschmidt           MPI          2 510358065.449219           10 
            3          8192          8192   gramschmidt           MPI          3 525770682.085938            4 
            3          8192          8192   gramschmidt           MPI          4 515340307.986328            7 
            3          8192          8192   gramschmidt           MPI          5 515097201.943359            3 
            3          8192          8192   gramschmidt           MPI          6 517548479.878906            2 
            3          8192          8192   gramschmidt           MPI          7 514110147.382812            3 
            3          8192          8192   gramschmidt           MPI          8 516069850.884766           13 
            3          8192          8192   gramschmidt           MPI          9 519972995.576172            5 
# Runtime: 6775.606777 s (overhead: 0.000001 %) 10 records
