# Sysname : Linux
# Nodename: eu-a6-002-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:14:14 2022
# Execution time and date (local): Fri Jan  7 12:14:14 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2          4096          4096   gramschmidt           MPI          0 110100380.802734            2 
            2          4096          4096   gramschmidt           MPI          1 115314833.763672            8 
            2          4096          4096   gramschmidt           MPI          2 118746870.390625            9 
            2          4096          4096   gramschmidt           MPI          3 113637721.728516            2 
            2          4096          4096   gramschmidt           MPI          4 118079142.957031            6 
            2          4096          4096   gramschmidt           MPI          5 115053531.492188            0 
            2          4096          4096   gramschmidt           MPI          6 113541047.531250            0 
            2          4096          4096   gramschmidt           MPI          7 119835154.535156            0 
            2          4096          4096   gramschmidt           MPI          8 113731306.742188            3 
            2          4096          4096   gramschmidt           MPI          9 116415407.152344            1 
# Runtime: 1483.544622 s (overhead: 0.000002 %) 10 records
