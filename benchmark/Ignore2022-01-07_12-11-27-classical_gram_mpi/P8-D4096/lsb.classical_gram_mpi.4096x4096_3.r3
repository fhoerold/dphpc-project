# Sysname : Linux
# Nodename: eu-a6-002-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:14:14 2022
# Execution time and date (local): Fri Jan  7 12:14:14 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3          4096          4096   gramschmidt           MPI          0 110103666.578125            0 
            3          4096          4096   gramschmidt           MPI          1 115319316.248047            5 
            3          4096          4096   gramschmidt           MPI          2 118750192.199219            6 
            3          4096          4096   gramschmidt           MPI          3 113641329.783203            0 
            3          4096          4096   gramschmidt           MPI          4 118082947.128906            4 
            3          4096          4096   gramschmidt           MPI          5 115057225.041016            0 
            3          4096          4096   gramschmidt           MPI          6 113544769.900391            0 
            3          4096          4096   gramschmidt           MPI          7 119838744.796875            0 
            3          4096          4096   gramschmidt           MPI          8 113734935.052734            8 
            3          4096          4096   gramschmidt           MPI          9 116419493.197266            3 
# Runtime: 1483.544942 s (overhead: 0.000002 %) 10 records
