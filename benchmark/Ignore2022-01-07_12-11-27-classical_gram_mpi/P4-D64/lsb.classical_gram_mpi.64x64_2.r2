# Sysname : Linux
# Nodename: eu-a6-001-24
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:12:14 2022
# Execution time and date (local): Fri Jan  7 12:12:14 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            64            64   gramschmidt           MPI          0 496.351562            0 
            2            64            64   gramschmidt           MPI          1 471.466797            0 
            2            64            64   gramschmidt           MPI          2 480.431641            0 
            2            64            64   gramschmidt           MPI          3 452.386719            0 
            2            64            64   gramschmidt           MPI          4 459.525391            0 
            2            64            64   gramschmidt           MPI          5 474.644531            0 
            2            64            64   gramschmidt           MPI          6 451.378906            0 
            2            64            64   gramschmidt           MPI          7 484.921875            0 
            2            64            64   gramschmidt           MPI          8 462.275391            0 
            2            64            64   gramschmidt           MPI          9 458.191406            0 
# Runtime: 0.008153 s (overhead: 0.000000 %) 10 records
