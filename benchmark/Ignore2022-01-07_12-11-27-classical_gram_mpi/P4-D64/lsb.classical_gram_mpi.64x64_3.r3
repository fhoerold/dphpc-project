# Sysname : Linux
# Nodename: eu-a6-001-24
# Release : 3.10.0-1160.36.2.el7.x86_64
# Version : #1 SMP Wed Jul 21 11:57:15 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:12:14 2022
# Execution time and date (local): Fri Jan  7 12:12:14 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            64            64   gramschmidt           MPI          0 491.173828            0 
            3            64            64   gramschmidt           MPI          1 465.718750            0 
            3            64            64   gramschmidt           MPI          2 482.179688            0 
            3            64            64   gramschmidt           MPI          3 454.533203            0 
            3            64            64   gramschmidt           MPI          4 459.580078            0 
            3            64            64   gramschmidt           MPI          5 474.173828            0 
            3            64            64   gramschmidt           MPI          6 450.148438            0 
            3            64            64   gramschmidt           MPI          7 485.080078            0 
            3            64            64   gramschmidt           MPI          8 462.412109            0 
            3            64            64   gramschmidt           MPI          9 462.742188            0 
# Runtime: 0.010060 s (overhead: 0.000000 %) 10 records
