# Sysname : Linux
# Nodename: eu-a6-005-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:50:24 2022
# Execution time and date (local): Fri Jan  7 18:50:24 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3           256           256   gramschmidt           MPI          0 22129.603516            0 
            3           256           256   gramschmidt           MPI          1 20197.994141            4 
            3           256           256   gramschmidt           MPI          2 21858.248047            1 
            3           256           256   gramschmidt           MPI          3 18278.285156            0 
            3           256           256   gramschmidt           MPI          4 18069.457031            1 
            3           256           256   gramschmidt           MPI          5 18051.953125            0 
            3           256           256   gramschmidt           MPI          6 18122.958984            0 
            3           256           256   gramschmidt           MPI          7 18824.953125            0 
            3           256           256   gramschmidt           MPI          8 18102.156250            1 
            3           256           256   gramschmidt           MPI          9 18164.701172            0 
# Runtime: 5.278651 s (overhead: 0.000133 %) 10 records
