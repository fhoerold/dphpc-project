# Sysname : Linux
# Nodename: eu-a6-005-02
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:50:24 2022
# Execution time and date (local): Fri Jan  7 18:50:24 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2           256           256   gramschmidt           MPI          0 22209.712891            0 
            2           256           256   gramschmidt           MPI          1 20268.111328            4 
            2           256           256   gramschmidt           MPI          2 21931.679688            0 
            2           256           256   gramschmidt           MPI          3 18348.880859            0 
            2           256           256   gramschmidt           MPI          4 18133.382812            0 
            2           256           256   gramschmidt           MPI          5 18127.654297            0 
            2           256           256   gramschmidt           MPI          6 18193.712891            0 
            2           256           256   gramschmidt           MPI          7 18895.525391            0 
            2           256           256   gramschmidt           MPI          8 18172.511719            1 
            2           256           256   gramschmidt           MPI          9 18231.869141            0 
# Runtime: 15.333348 s (overhead: 0.000033 %) 10 records
