# Sysname : Linux
# Nodename: eu-a6-008-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:12:43 2022
# Execution time and date (local): Fri Jan  7 12:12:43 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            2            32            32   gramschmidt           MPI          0 377.828125            0 
            2            32            32   gramschmidt           MPI          1 258.523438            2 
            2            32            32   gramschmidt           MPI          2 243.875000            0 
            2            32            32   gramschmidt           MPI          3 217.550781            0 
            2            32            32   gramschmidt           MPI          4 216.564453            0 
            2            32            32   gramschmidt           MPI          5 234.687500            0 
            2            32            32   gramschmidt           MPI          6 213.197266            0 
            2            32            32   gramschmidt           MPI          7 217.337891            0 
            2            32            32   gramschmidt           MPI          8 213.404297            0 
            2            32            32   gramschmidt           MPI          9 210.945312            0 
# Runtime: 1.019151 s (overhead: 0.000196 %) 10 records
