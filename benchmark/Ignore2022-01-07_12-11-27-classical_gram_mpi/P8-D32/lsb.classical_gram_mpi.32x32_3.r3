# Sysname : Linux
# Nodename: eu-a6-008-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 11:12:43 2022
# Execution time and date (local): Fri Jan  7 12:12:43 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank       size_m       size_n       region         type       id         time     overhead 
            3            32            32   gramschmidt           MPI          0 372.095703            0 
            3            32            32   gramschmidt           MPI          1 252.109375            0 
            3            32            32   gramschmidt           MPI          2 238.250000            0 
            3            32            32   gramschmidt           MPI          3 214.851562            0 
            3            32            32   gramschmidt           MPI          4 215.443359            0 
            3            32            32   gramschmidt           MPI          5 227.951172            0 
            3            32            32   gramschmidt           MPI          6 209.984375            0 
            3            32            32   gramschmidt           MPI          7 215.455078            0 
            3            32            32   gramschmidt           MPI          8 211.623047            0 
            3            32            32   gramschmidt           MPI          9 205.437500            0 
# Runtime: 0.014584 s (overhead: 0.000000 %) 10 records
