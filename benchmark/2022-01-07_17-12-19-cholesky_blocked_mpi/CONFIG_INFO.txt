Invocation: ./cholesky/benchmark.sh -o ./benchmark --mpi -d 5 -p 1 --blocksize 4 ./cholesky/build/cholesky_blocked_mpi
Benchmark 'cholesky_blocked_mpi' for
> Dimensions:		32, 
> Number of processors:	1, 
> CPU Model:		XeonGold_6150
> Use MPI:		true
> Use OpenMP:		false

Timestamp: 2022-01-07_17-12-19
