# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 17 18:12:54 2021
# Execution time and date (local): Fri Dec 17 19:12:54 2021
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 10238398.101562            0 
            2          8192           MPI          1 10284731.150391           13 
            2          8192           MPI          2 10265988.125000            1 
            2          8192           MPI          3 10256076.549805            0 
            2          8192           MPI          4 10263118.349609            1 
            2          8192           MPI          5 10264439.375000            0 
            2          8192           MPI          6 10292931.475586            0 
            2          8192           MPI          7 10332977.272461            3 
            2          8192           MPI          8 10253840.619141           11 
            2          8192           MPI          9 10247257.906250            0 
# Runtime: 124.421247 s (overhead: 0.000023 %) 10 records
