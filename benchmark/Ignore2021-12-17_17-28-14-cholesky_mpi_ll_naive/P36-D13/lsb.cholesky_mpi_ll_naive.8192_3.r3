# Sysname : Linux
# Nodename: eu-a6-004-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Dec 17 18:12:54 2021
# Execution time and date (local): Fri Dec 17 19:12:54 2021
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 10238124.979492            0 
            3          8192           MPI          1 10284453.255859            9 
            3          8192           MPI          2 10265699.237305            1 
            3          8192           MPI          3 10255798.161133            0 
            3          8192           MPI          4 10262842.381836            6 
            3          8192           MPI          5 10264163.654297            0 
            3          8192           MPI          6 10292653.631836            0 
            3          8192           MPI          7 10332709.643555            2 
            3          8192           MPI          8 10253563.684570           12 
            3          8192           MPI          9 10246980.460938            0 
# Runtime: 125.415921 s (overhead: 0.000024 %) 10 records
