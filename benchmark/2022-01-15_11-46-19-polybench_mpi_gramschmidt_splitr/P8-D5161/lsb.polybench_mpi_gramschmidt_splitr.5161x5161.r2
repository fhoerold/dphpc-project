# Sysname : Linux
# Nodename: eu-a6-012-21
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan 15 10:46:28 2022
# Execution time and date (local): Sat Jan 15 11:46:28 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         5161          5161   gramschmidt          0 125058539.894531            0 
         5161          5161   gramschmidt          1 123678651.578125            3 
         5161          5161   gramschmidt          2 123865577.029297            5 
         5161          5161   gramschmidt          3 123223428.904297            0 
         5161          5161   gramschmidt          4 123976459.642578            4 
         5161          5161   gramschmidt          5 123826708.462891            0 
         5161          5161   gramschmidt          6 122992399.031250            0 
         5161          5161   gramschmidt          7 124179582.648438            0 
         5161          5161   gramschmidt          8 123979261.675781            5 
         5161          5161   gramschmidt          9 123141163.451172            0 
# Runtime: 1237.921827 s (overhead: 0.000001 %) 10 records
