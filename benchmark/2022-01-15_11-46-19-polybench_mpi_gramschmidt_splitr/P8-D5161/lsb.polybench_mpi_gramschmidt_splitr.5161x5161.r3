# Sysname : Linux
# Nodename: eu-a6-012-21
# Release : 3.10.0-1160.45.1.el7.x86_64
# Version : #1 SMP Wed Oct 13 17:20:51 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan 15 10:46:28 2022
# Execution time and date (local): Sat Jan 15 11:46:28 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
       size_m       size_n       region       id         time     overhead 
         5161          5161   gramschmidt          0 124103379.164062            0 
         5161          5161   gramschmidt          1 123712064.214844            5 
         5161          5161   gramschmidt          2 123899057.992188            5 
         5161          5161   gramschmidt          3 123256717.982422            0 
         5161          5161   gramschmidt          4 124010184.408203            6 
         5161          5161   gramschmidt          5 123860314.050781            0 
         5161          5161   gramschmidt          6 123025605.097656            0 
         5161          5161   gramschmidt          7 124213428.658203            0 
         5161          5161   gramschmidt          8 124012654.478516            5 
         5161          5161   gramschmidt          9 123174735.400391            0 
# Runtime: 1237.268199 s (overhead: 0.000002 %) 10 records
