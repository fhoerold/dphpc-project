# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 23:14:25 2022
# Execution time and date (local): Sat Jan  8 00:14:25 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 4504011.156250            4 
            3          2048           MPI          1 4447789.658203            6 
            3          2048           MPI          2 4531202.994141            1 
            3          2048           MPI          3 4522429.578125            0 
            3          2048           MPI          4 4509543.996094           10 
            3          2048           MPI          5 4400152.697266            0 
            3          2048           MPI          6 4532380.744141            0 
            3          2048           MPI          7 4905444.101562            0 
            3          2048           MPI          8 4386414.695312            2 
            3          2048           MPI          9 4351483.025391            0 
# Runtime: 62.221606 s (overhead: 0.000037 %) 10 records
