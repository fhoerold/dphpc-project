# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 23:14:25 2022
# Execution time and date (local): Sat Jan  8 00:14:25 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 4504002.677734            6 
            2          2048           MPI          1 4447785.115234            6 
            2          2048           MPI          2 4531206.814453            4 
            2          2048           MPI          3 4522411.515625            0 
            2          2048           MPI          4 4509520.390625            9 
            2          2048           MPI          5 4400148.525391            0 
            2          2048           MPI          6 4532371.539062            0 
            2          2048           MPI          7 4905437.898438            0 
            2          2048           MPI          8 4386401.960938            2 
            2          2048           MPI          9 4351473.191406            0 
# Runtime: 67.220045 s (overhead: 0.000040 %) 10 records
