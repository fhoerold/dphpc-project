# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:19:46 2022
# Execution time and date (local): Fri Jan  7 18:19:46 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 74390.382812            0 
            3           512           MPI          1 73552.585938            3 
            3           512           MPI          2 71474.871094            1 
            3           512           MPI          3 73184.947266            0 
            3           512           MPI          4 72067.140625            6 
            3           512           MPI          5 71827.587891            0 
            3           512           MPI          6 75435.882812            0 
            3           512           MPI          7 71671.585938            0 
            3           512           MPI          8 72504.066406            1 
            3           512           MPI          9 72160.103516            0 
# Runtime: 10.909672 s (overhead: 0.000101 %) 10 records
