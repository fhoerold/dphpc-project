# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:19:46 2022
# Execution time and date (local): Fri Jan  7 18:19:46 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 74430.208984            4 
            2           512           MPI          1 73549.685547            4 
            2           512           MPI          2 71467.171875            1 
            2           512           MPI          3 73182.402344            0 
            2           512           MPI          4 72063.572266            1 
            2           512           MPI          5 71824.736328            0 
            2           512           MPI          6 75432.789062            0 
            2           512           MPI          7 71667.404297            0 
            2           512           MPI          8 72501.632812            1 
            2           512           MPI          9 72157.513672            0 
# Runtime: 13.912756 s (overhead: 0.000079 %) 10 records
