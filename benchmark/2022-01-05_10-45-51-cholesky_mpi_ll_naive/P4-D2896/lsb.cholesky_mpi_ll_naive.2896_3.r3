# Sysname : Linux
# Nodename: eu-a6-003-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 11:16:39 2022
# Execution time and date (local): Wed Jan  5 12:16:39 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2896           MPI          0 13592788.250000            0 
            3          2896           MPI          1 13218620.765625            6 
            3          2896           MPI          2 13159524.570312            1 
            3          2896           MPI          3 13172121.794922            0 
            3          2896           MPI          4 13225584.025391            1 
            3          2896           MPI          5 13182852.150391            0 
            3          2896           MPI          6 13213597.369141            0 
            3          2896           MPI          7 12941472.525391            0 
            3          2896           MPI          8 13228720.806641            5 
            3          2896           MPI          9 13438472.763672            0 
# Runtime: 162.711190 s (overhead: 0.000008 %) 10 records
