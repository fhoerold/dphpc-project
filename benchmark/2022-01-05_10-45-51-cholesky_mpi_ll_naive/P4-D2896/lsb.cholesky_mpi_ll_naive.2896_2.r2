# Sysname : Linux
# Nodename: eu-a6-003-23
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 11:16:39 2022
# Execution time and date (local): Wed Jan  5 12:16:39 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2896           MPI          0 13593339.576172            0 
            2          2896           MPI          1 13219160.408203            3 
            2          2896           MPI          2 13160050.857422            1 
            2          2896           MPI          3 13172648.988281            0 
            2          2896           MPI          4 13226109.218750            1 
            2          2896           MPI          5 13183382.869141            0 
            2          2896           MPI          6 13214123.041016            0 
            2          2896           MPI          7 12941990.357422            0 
            2          2896           MPI          8 13229250.158203            6 
            2          2896           MPI          9 13439011.503906            0 
# Runtime: 160.719504 s (overhead: 0.000007 %) 10 records
