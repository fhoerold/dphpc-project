# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:13:08 2022
# Execution time and date (local): Fri Jan  7 08:13:08 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 79604.654297            0 
            2           512           MPI          1 78067.333984            5 
            2           512           MPI          2 77879.025391            5 
            2           512           MPI          3 82745.550781            0 
            2           512           MPI          4 79582.066406            1 
            2           512           MPI          5 78796.701172            0 
            2           512           MPI          6 79354.283203            0 
            2           512           MPI          7 85051.916016            0 
            2           512           MPI          8 79241.167969            6 
            2           512           MPI          9 78642.642578            0 
# Runtime: 12.985012 s (overhead: 0.000131 %) 10 records
