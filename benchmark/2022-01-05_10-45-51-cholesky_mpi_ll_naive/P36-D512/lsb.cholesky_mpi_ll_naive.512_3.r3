# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:13:08 2022
# Execution time and date (local): Fri Jan  7 08:13:08 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 80090.175781            0 
            3           512           MPI          1 78060.964844            4 
            3           512           MPI          2 77876.769531            1 
            3           512           MPI          3 82743.429688            0 
            3           512           MPI          4 79577.906250            0 
            3           512           MPI          5 78793.195312            0 
            3           512           MPI          6 79348.111328            0 
            3           512           MPI          7 85050.671875            3 
            3           512           MPI          8 79230.134766            6 
            3           512           MPI          9 78636.656250            0 
# Runtime: 12.975740 s (overhead: 0.000108 %) 10 records
