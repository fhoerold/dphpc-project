# Sysname : Linux
# Nodename: eu-a6-007-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:32:05 2022
# Execution time and date (local): Wed Jan  5 18:32:05 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2896           MPI          0 12319392.541016            0 
            2          2896           MPI          1 12630837.292969            3 
            2          2896           MPI          2 12771621.775391            4 
            2          2896           MPI          3 12762716.974609            0 
            2          2896           MPI          4 12751719.791016            1 
            2          2896           MPI          5 12771831.427734            0 
            2          2896           MPI          6 12762172.037109            1 
            2          2896           MPI          7 12685405.244141            0 
            2          2896           MPI          8 12730880.277344            5 
            2          2896           MPI          9 12709464.037109            0 
# Runtime: 152.412283 s (overhead: 0.000009 %) 10 records
