# Sysname : Linux
# Nodename: eu-a6-007-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:32:05 2022
# Execution time and date (local): Wed Jan  5 18:32:05 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2896           MPI          0 12320167.861328            0 
            3          2896           MPI          1 12631629.988281            6 
            3          2896           MPI          2 12772423.621094            1 
            3          2896           MPI          3 12763518.925781            0 
            3          2896           MPI          4 12752520.865234            1 
            3          2896           MPI          5 12772633.650391            0 
            3          2896           MPI          6 12762972.318359            1 
            3          2896           MPI          7 12686201.078125            0 
            3          2896           MPI          8 12731679.931641            5 
            3          2896           MPI          9 12710262.226562            0 
# Runtime: 152.443209 s (overhead: 0.000009 %) 10 records
