# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 11:08:38 2022
# Execution time and date (local): Wed Jan  5 12:08:38 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            64           MPI          0 300.029297            0 
            2            64           MPI          1 696.472656            0 
            2            64           MPI          2 244.070312            0 
            2            64           MPI          3 244.734375            0 
            2            64           MPI          4 222.347656            0 
            2            64           MPI          5 256.259766            0 
            2            64           MPI          6 230.964844            0 
            2            64           MPI          7 234.898438            0 
            2            64           MPI          8 230.359375            0 
            2            64           MPI          9 296.119141            0 
# Runtime: 0.005173 s (overhead: 0.000000 %) 10 records
