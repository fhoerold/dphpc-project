# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 11:08:38 2022
# Execution time and date (local): Wed Jan  5 12:08:38 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            64           MPI          0 306.080078            0 
            3            64           MPI          1 290.662109            3 
            3            64           MPI          2 624.941406            0 
            3            64           MPI          3 244.931641            0 
            3            64           MPI          4 222.669922            0 
            3            64           MPI          5 256.667969            0 
            3            64           MPI          6 231.224609            0 
            3            64           MPI          7 235.656250            0 
            3            64           MPI          8 230.656250            0 
            3            64           MPI          9 296.566406            0 
# Runtime: 7.011205 s (overhead: 0.000043 %) 10 records
