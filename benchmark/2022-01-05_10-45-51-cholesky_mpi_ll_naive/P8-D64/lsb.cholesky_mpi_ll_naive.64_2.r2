# Sysname : Linux
# Nodename: eu-a6-007-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:28:36 2022
# Execution time and date (local): Wed Jan  5 18:28:36 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            64           MPI          0 656.009766            0 
            2            64           MPI          1 273.271484            2 
            2            64           MPI          2 302.177734            0 
            2            64           MPI          3 269.955078            0 
            2            64           MPI          4 270.603516            0 
            2            64           MPI          5 275.806641            0 
            2            64           MPI          6 272.574219            0 
            2            64           MPI          7 271.220703            0 
            2            64           MPI          8 273.640625            0 
            2            64           MPI          9 264.314453            0 
# Runtime: 17.975138 s (overhead: 0.000011 %) 10 records
