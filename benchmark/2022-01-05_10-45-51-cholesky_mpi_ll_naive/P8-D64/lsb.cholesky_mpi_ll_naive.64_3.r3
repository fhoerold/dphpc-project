# Sysname : Linux
# Nodename: eu-a6-007-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:28:36 2022
# Execution time and date (local): Wed Jan  5 18:28:36 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            64           MPI          0 306.251953            1 
            3            64           MPI          1 271.882812            2 
            3            64           MPI          2 613.625000            0 
            3            64           MPI          3 571.425781            0 
            3            64           MPI          4 270.923828            0 
            3            64           MPI          5 276.117188            0 
            3            64           MPI          6 272.830078            0 
            3            64           MPI          7 271.535156            0 
            3            64           MPI          8 273.871094            0 
            3            64           MPI          9 264.769531            0 
# Runtime: 20.963510 s (overhead: 0.000014 %) 10 records
