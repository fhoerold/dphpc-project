# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 20:20:22 2022
# Execution time and date (local): Fri Jan  7 21:20:22 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 789540083.412109            2 
            2          8192           MPI          1 790024329.470703            6 
            2          8192           MPI          2 786986662.140625            6 
            2          8192           MPI          3 784763155.689453            0 
            2          8192           MPI          4 783616069.425781            5 
            2          8192           MPI          5 782361737.261719            1 
            2          8192           MPI          6 781878420.767578            1 
            2          8192           MPI          7 782706796.287109            2 
            2          8192           MPI          8 781168975.291016            6 
            2          8192           MPI          9 782856005.701172            1 
# Runtime: 9439.544355 s (overhead: 0.000000 %) 10 records
