# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 20:20:22 2022
# Execution time and date (local): Fri Jan  7 21:20:22 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 789926299.109375            1 
            3          8192           MPI          1 790410796.875000            8 
            3          8192           MPI          2 787371644.099609            6 
            3          8192           MPI          3 785147068.439453            0 
            3          8192           MPI          4 783999402.437500            8 
            3          8192           MPI          5 782744460.099609            4 
            3          8192           MPI          6 782260900.433594            0 
            3          8192           MPI          7 783089685.287109            5 
            3          8192           MPI          8 781551126.792969            7 
            3          8192           MPI          9 783238989.443359            2 
# Runtime: 9443.179663 s (overhead: 0.000000 %) 10 records
