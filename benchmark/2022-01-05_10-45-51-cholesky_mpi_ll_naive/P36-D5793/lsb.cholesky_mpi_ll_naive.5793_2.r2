# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 08:05:09 2022
# Execution time and date (local): Fri Jan  7 09:05:09 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          5793           MPI          0 222399390.072266            3 
            2          5793           MPI          1 216157732.078125            8 
            2          5793           MPI          2 216633059.324219            4 
            2          5793           MPI          3 216592431.384766            0 
            2          5793           MPI          4 215434803.132812            6 
            2          5793           MPI          5 216149243.722656            2 
            2          5793           MPI          6 216665869.267578            2 
            2          5793           MPI          7 214752239.531250            2 
            2          5793           MPI          8 216597493.091797            9 
            2          5793           MPI          9 216146260.949219            0 
# Runtime: 2622.981204 s (overhead: 0.000001 %) 10 records
