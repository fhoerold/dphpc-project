# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 08:05:09 2022
# Execution time and date (local): Fri Jan  7 09:05:09 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          5793           MPI          0 222384758.679688            2 
            3          5793           MPI          1 216143556.187500            5 
            3          5793           MPI          2 216618852.896484            6 
            3          5793           MPI          3 216578225.681641            0 
            3          5793           MPI          4 215420676.228516            4 
            3          5793           MPI          5 216135065.960938            0 
            3          5793           MPI          6 216651659.220703            0 
            3          5793           MPI          7 214738169.792969            0 
            3          5793           MPI          8 216583280.548828            6 
            3          5793           MPI          9 216132085.271484            0 
# Runtime: 2625.817538 s (overhead: 0.000001 %) 10 records
