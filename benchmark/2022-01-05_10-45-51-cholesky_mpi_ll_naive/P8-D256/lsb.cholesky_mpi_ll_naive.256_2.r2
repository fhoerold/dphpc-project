# Sysname : Linux
# Nodename: eu-a6-007-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:29:07 2022
# Execution time and date (local): Wed Jan  5 18:29:07 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 5395.769531            0 
            2           256           MPI          1 4673.072266            2 
            2           256           MPI          2 4620.201172            0 
            2           256           MPI          3 4662.585938            0 
            2           256           MPI          4 4669.117188            0 
            2           256           MPI          5 4629.742188            0 
            2           256           MPI          6 4650.703125            0 
            2           256           MPI          7 4661.271484            0 
            2           256           MPI          8 4629.718750            0 
            2           256           MPI          9 4667.705078            0 
# Runtime: 2.056396 s (overhead: 0.000097 %) 10 records
