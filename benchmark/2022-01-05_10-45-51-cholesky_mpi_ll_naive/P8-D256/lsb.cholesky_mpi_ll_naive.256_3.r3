# Sysname : Linux
# Nodename: eu-a6-007-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:29:07 2022
# Execution time and date (local): Wed Jan  5 18:29:07 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 5037.783203            1 
            3           256           MPI          1 4980.046875            2 
            3           256           MPI          2 4619.103516            0 
            3           256           MPI          3 4661.347656            0 
            3           256           MPI          4 4668.052734            0 
            3           256           MPI          5 4628.609375            0 
            3           256           MPI          6 4649.646484            0 
            3           256           MPI          7 4660.085938            0 
            3           256           MPI          8 4631.513672            0 
            3           256           MPI          9 4666.589844            0 
# Runtime: 9.079479 s (overhead: 0.000033 %) 10 records
