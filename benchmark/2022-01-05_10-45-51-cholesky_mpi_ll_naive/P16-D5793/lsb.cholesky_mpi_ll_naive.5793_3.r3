# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 02:14:45 2022
# Execution time and date (local): Thu Jan  6 03:14:45 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          5793           MPI          0 176362357.058594            1 
            3          5793           MPI          1 173804249.351562            5 
            3          5793           MPI          2 176167793.964844            7 
            3          5793           MPI          3 175679641.425781            2 
            3          5793           MPI          4 175182816.154297            6 
            3          5793           MPI          5 172795955.457031            2 
            3          5793           MPI          6 172341537.324219            2 
            3          5793           MPI          7 175374715.751953            2 
            3          5793           MPI          8 175244104.958984            7 
            3          5793           MPI          9 175364400.478516            2 
# Runtime: 2120.443175 s (overhead: 0.000002 %) 10 records
