# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 02:14:45 2022
# Execution time and date (local): Thu Jan  6 03:14:45 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          5793           MPI          0 176335375.474609            4 
            2          5793           MPI          1 173777660.736328            8 
            2          5793           MPI          2 176140846.468750            8 
            2          5793           MPI          3 175652769.378906            2 
            2          5793           MPI          4 175156019.248047            6 
            2          5793           MPI          5 172769521.382812            2 
            2          5793           MPI          6 172315178.431641            2 
            2          5793           MPI          7 175347883.039062            2 
            2          5793           MPI          8 175217300.220703            7 
            2          5793           MPI          9 175337575.958984            2 
# Runtime: 2125.122561 s (overhead: 0.000002 %) 10 records
