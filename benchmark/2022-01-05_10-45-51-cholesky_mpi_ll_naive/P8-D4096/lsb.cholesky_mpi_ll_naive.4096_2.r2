# Sysname : Linux
# Nodename: eu-a6-007-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:40:46 2022
# Execution time and date (local): Wed Jan  5 18:40:46 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 62164340.845703            0 
            2          4096           MPI          1 62343893.388672            3 
            2          4096           MPI          2 61914395.757812            5 
            2          4096           MPI          3 61834658.310547            0 
            2          4096           MPI          4 61868103.871094            4 
            2          4096           MPI          5 61872464.884766            2 
            2          4096           MPI          6 61874098.615234            1 
            2          4096           MPI          7 61890962.162109            1 
            2          4096           MPI          8 61848314.494141            6 
            2          4096           MPI          9 61866167.636719            0 
# Runtime: 755.986184 s (overhead: 0.000003 %) 10 records
