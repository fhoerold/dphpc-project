# Sysname : Linux
# Nodename: eu-a6-007-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:40:46 2022
# Execution time and date (local): Wed Jan  5 18:40:46 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 62161462.435547            0 
            3          4096           MPI          1 62340992.281250            5 
            3          4096           MPI          2 61911507.455078            3 
            3          4096           MPI          3 61831773.896484            0 
            3          4096           MPI          4 61865219.919922            5 
            3          4096           MPI          5 61869579.945312            0 
            3          4096           MPI          6 61871214.890625            0 
            3          4096           MPI          7 61888076.941406            0 
            3          4096           MPI          8 61845430.619141            3 
            3          4096           MPI          9 61863282.910156            0 
# Runtime: 756.957801 s (overhead: 0.000002 %) 10 records
