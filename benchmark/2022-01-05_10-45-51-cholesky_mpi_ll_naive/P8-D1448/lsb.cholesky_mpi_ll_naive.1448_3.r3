# Sysname : Linux
# Nodename: eu-a6-007-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:29:31 2022
# Execution time and date (local): Wed Jan  5 18:29:31 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1448           MPI          0 1371722.308594            0 
            3          1448           MPI          1 1344174.490234            3 
            3          1448           MPI          2 1331699.292969            1 
            3          1448           MPI          3 1322912.208984            0 
            3          1448           MPI          4 1334157.076172            2 
            3          1448           MPI          5 1267939.837891            0 
            3          1448           MPI          6 1271417.998047            0 
            3          1448           MPI          7 1265733.474609            0 
            3          1448           MPI          8 1267035.392578            6 
            3          1448           MPI          9 1265653.259766            0 
# Runtime: 18.896172 s (overhead: 0.000064 %) 10 records
