# Sysname : Linux
# Nodename: eu-a6-007-08
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:29:31 2022
# Execution time and date (local): Wed Jan  5 18:29:31 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1448           MPI          0 1370824.050781            0 
            2          1448           MPI          1 1342988.408203            4 
            2          1448           MPI          2 1330520.269531            1 
            2          1448           MPI          3 1321874.152344            0 
            2          1448           MPI          4 1333101.640625            7 
            2          1448           MPI          5 1266941.945312            0 
            2          1448           MPI          6 1270416.716797            0 
            2          1448           MPI          7 1264736.560547            0 
            2          1448           MPI          8 1266031.968750            2 
            2          1448           MPI          9 1264656.908203            0 
# Runtime: 15.946439 s (overhead: 0.000088 %) 10 records
