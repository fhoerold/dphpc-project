# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:51:13 2022
# Execution time and date (local): Fri Jan  7 17:51:13 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 4363821.939453            0 
            3          2048           MPI          1 4265407.568359            8 
            3          2048           MPI          2 4311972.441406            1 
            3          2048           MPI          3 4257686.876953            0 
            3          2048           MPI          4 4431331.597656            6 
            3          2048           MPI          5 4396041.166016            0 
            3          2048           MPI          6 4316629.656250            2 
            3          2048           MPI          7 4257034.578125            0 
            3          2048           MPI          8 4298202.441406            9 
            3          2048           MPI          9 4403512.152344            0 
# Runtime: 53.868424 s (overhead: 0.000048 %) 10 records
