# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:51:13 2022
# Execution time and date (local): Fri Jan  7 17:51:13 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 4363849.162109            3 
            2          2048           MPI          1 4265404.175781            6 
            2          2048           MPI          2 4311968.021484            6 
            2          2048           MPI          3 4257684.857422            0 
            2          2048           MPI          4 4431340.007812            7 
            2          2048           MPI          5 4396032.716797            0 
            2          2048           MPI          6 4316617.349609            0 
            2          2048           MPI          7 4257028.169922            0 
            2          2048           MPI          8 4298203.017578            1 
            2          2048           MPI          9 4403509.205078            0 
# Runtime: 61.876626 s (overhead: 0.000037 %) 10 records
