# Sysname : Linux
# Nodename: eu-a6-007-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:32:07 2022
# Execution time and date (local): Wed Jan  5 18:32:07 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 1305.037109            1 
            3           128           MPI          1 1105.572266            3 
            3           128           MPI          2 1298.779297            0 
            3           128           MPI          3 1099.064453            0 
            3           128           MPI          4 1104.007812            0 
            3           128           MPI          5 1097.839844            0 
            3           128           MPI          6 1105.761719            0 
            3           128           MPI          7 1093.542969            0 
            3           128           MPI          8 1096.353516            0 
            3           128           MPI          9 1086.515625            0 
# Runtime: 3.047950 s (overhead: 0.000131 %) 10 records
