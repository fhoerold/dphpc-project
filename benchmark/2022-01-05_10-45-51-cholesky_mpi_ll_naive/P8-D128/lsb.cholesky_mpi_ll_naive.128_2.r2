# Sysname : Linux
# Nodename: eu-a6-007-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:32:07 2022
# Execution time and date (local): Wed Jan  5 18:32:07 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 1295.105469            0 
            2           128           MPI          1 1100.839844            2 
            2           128           MPI          2 1282.156250            0 
            2           128           MPI          3 1098.371094            0 
            2           128           MPI          4 1103.419922            0 
            2           128           MPI          5 1097.439453            0 
            2           128           MPI          6 1105.173828            0 
            2           128           MPI          7 1092.906250            0 
            2           128           MPI          8 1095.849609            2 
            2           128           MPI          9 1085.707031            0 
# Runtime: 2.024210 s (overhead: 0.000198 %) 10 records
