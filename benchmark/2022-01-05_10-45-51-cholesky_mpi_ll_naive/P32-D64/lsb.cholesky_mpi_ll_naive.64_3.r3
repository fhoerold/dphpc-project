# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:49:44 2022
# Execution time and date (local): Fri Jan  7 17:49:44 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            64           MPI          0 1718.166016            3 
            3            64           MPI          1 1637.974609            3 
            3            64           MPI          2 1279.250000            0 
            3            64           MPI          3 1259.660156            0 
            3            64           MPI          4 1250.933594            0 
            3            64           MPI          5 1259.689453            0 
            3            64           MPI          6 1260.351562            0 
            3            64           MPI          7 1274.751953            0 
            3            64           MPI          8 1250.103516            0 
            3            64           MPI          9 1244.302734            0 
# Runtime: 5.998120 s (overhead: 0.000100 %) 10 records
