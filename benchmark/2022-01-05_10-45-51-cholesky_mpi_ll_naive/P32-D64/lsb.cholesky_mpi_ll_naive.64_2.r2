# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:49:44 2022
# Execution time and date (local): Fri Jan  7 17:49:44 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            64           MPI          0 1647.125000            3 
            2            64           MPI          1 1638.857422            3 
            2            64           MPI          2 1279.158203            2 
            2            64           MPI          3 1258.884766            0 
            2            64           MPI          4 1253.312500            0 
            2            64           MPI          5 1259.431641            0 
            2            64           MPI          6 1261.443359            0 
            2            64           MPI          7 1274.541016            0 
            2            64           MPI          8 1247.511719            2 
            2            64           MPI          9 1244.046875            0 
# Runtime: 10.000254 s (overhead: 0.000100 %) 10 records
