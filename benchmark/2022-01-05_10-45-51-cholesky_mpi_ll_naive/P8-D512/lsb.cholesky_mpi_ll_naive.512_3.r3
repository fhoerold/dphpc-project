# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:29:08 2022
# Execution time and date (local): Wed Jan  5 18:29:08 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 44965.333984            0 
            3           512           MPI          1 44265.949219            3 
            3           512           MPI          2 44073.039062            0 
            3           512           MPI          3 43839.927734            0 
            3           512           MPI          4 43721.738281            0 
            3           512           MPI          5 43811.742188            0 
            3           512           MPI          6 43815.277344            0 
            3           512           MPI          7 43828.599609            0 
            3           512           MPI          8 43791.322266            0 
            3           512           MPI          9 45727.767578            0 
# Runtime: 6.535524 s (overhead: 0.000046 %) 10 records
