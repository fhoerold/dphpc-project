# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:29:08 2022
# Execution time and date (local): Wed Jan  5 18:29:08 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 45353.138672            0 
            2           512           MPI          1 44043.201172            3 
            2           512           MPI          2 43848.654297            0 
            2           512           MPI          3 43907.458984            0 
            2           512           MPI          4 43789.130859            2 
            2           512           MPI          5 43879.140625            0 
            2           512           MPI          6 43882.656250            0 
            2           512           MPI          7 43895.953125            0 
            2           512           MPI          8 43858.425781            0 
            2           512           MPI          9 45805.117188            0 
# Runtime: 4.534024 s (overhead: 0.000110 %) 10 records
