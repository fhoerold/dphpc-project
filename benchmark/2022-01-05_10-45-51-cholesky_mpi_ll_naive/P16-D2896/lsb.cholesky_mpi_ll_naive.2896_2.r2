# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 19:01:41 2022
# Execution time and date (local): Wed Jan  5 20:01:41 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2896           MPI          0 27982383.632812            0 
            2          2896           MPI          1 28208019.193359            9 
            2          2896           MPI          2 28098672.251953            2 
            2          2896           MPI          3 28121512.341797            0 
            2          2896           MPI          4 28021759.896484            9 
            2          2896           MPI          5 27509712.054688            0 
            2          2896           MPI          6 27884288.171875            2 
            2          2896           MPI          7 27968870.882812            0 
            2          2896           MPI          8 27992547.060547           11 
            2          2896           MPI          9 28053676.267578            0 
# Runtime: 345.697044 s (overhead: 0.000010 %) 10 records
