# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 19:01:41 2022
# Execution time and date (local): Wed Jan  5 20:01:41 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2896           MPI          0 27985750.185547            2 
            3          2896           MPI          1 28211403.558594            5 
            3          2896           MPI          2 28102062.353516            9 
            3          2896           MPI          3 28124872.267578            0 
            3          2896           MPI          4 28025130.365234            9 
            3          2896           MPI          5 27513007.708984            0 
            3          2896           MPI          6 27887639.912109            4 
            3          2896           MPI          7 27972227.931641            0 
            3          2896           MPI          8 27995910.197266            9 
            3          2896           MPI          9 28057043.464844            0 
# Runtime: 338.731182 s (overhead: 0.000011 %) 10 records
