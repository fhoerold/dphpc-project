# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:21:36 2022
# Execution time and date (local): Fri Jan  7 08:21:36 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 92849602.410156            1 
            2          4096           MPI          1 91869695.339844           10 
            2          4096           MPI          2 91081469.574219            7 
            2          4096           MPI          3 89931649.787109            0 
            2          4096           MPI          4 91260082.517578           17 
            2          4096           MPI          5 90101440.953125            4 
            2          4096           MPI          6 90607033.312500            4 
            2          4096           MPI          7 90268397.617188            5 
            2          4096           MPI          8 89808432.103516            9 
            2          4096           MPI          9 89996674.820312            1 
# Runtime: 1100.764806 s (overhead: 0.000005 %) 10 records
