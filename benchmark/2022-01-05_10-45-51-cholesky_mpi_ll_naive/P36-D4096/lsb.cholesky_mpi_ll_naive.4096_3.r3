# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 07:21:36 2022
# Execution time and date (local): Fri Jan  7 08:21:36 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 92849308.037109            2 
            3          4096           MPI          1 91869446.447266            6 
            3          4096           MPI          2 91081225.031250            9 
            3          4096           MPI          3 89931426.359375            2 
            3          4096           MPI          4 91259838.830078           14 
            3          4096           MPI          5 90101219.625000            0 
            3          4096           MPI          6 90606797.289062            0 
            3          4096           MPI          7 90268156.261719            0 
            3          4096           MPI          8 89808202.582031            9 
            3          4096           MPI          9 89996449.085938            2 
# Runtime: 1105.761728 s (overhead: 0.000004 %) 10 records
