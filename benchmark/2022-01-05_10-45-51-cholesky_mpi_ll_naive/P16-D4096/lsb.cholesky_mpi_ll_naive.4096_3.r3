# Sysname : Linux
# Nodename: eu-a6-003-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 01:40:36 2022
# Execution time and date (local): Thu Jan  6 02:40:36 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 54853952.472656            0 
            3          4096           MPI          1 55346841.669922            6 
            3          4096           MPI          2 53474325.564453            6 
            3          4096           MPI          3 51652255.148438            0 
            3          4096           MPI          4 51603075.242188            8 
            3          4096           MPI          5 51611371.126953            2 
            3          4096           MPI          6 51580868.925781            2 
            3          4096           MPI          7 51676764.847656            2 
            3          4096           MPI          8 51616121.785156            8 
            3          4096           MPI          9 51607964.314453            4 
# Runtime: 646.814801 s (overhead: 0.000006 %) 10 records
