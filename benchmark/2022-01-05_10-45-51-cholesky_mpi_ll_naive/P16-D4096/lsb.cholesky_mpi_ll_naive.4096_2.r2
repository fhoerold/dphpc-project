# Sysname : Linux
# Nodename: eu-a6-003-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 01:40:36 2022
# Execution time and date (local): Thu Jan  6 02:40:36 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 54852032.630859            2 
            2          4096           MPI          1 55344863.937500            6 
            2          4096           MPI          2 53472416.109375            6 
            2          4096           MPI          3 51650410.863281            0 
            2          4096           MPI          4 51601232.863281            7 
            2          4096           MPI          5 51609525.326172            2 
            2          4096           MPI          6 51579026.128906            1 
            2          4096           MPI          7 51674916.710938            1 
            2          4096           MPI          8 51614277.072266            7 
            2          4096           MPI          9 51606121.757812            3 
# Runtime: 647.789700 s (overhead: 0.000005 %) 10 records
