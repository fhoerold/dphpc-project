# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 01:08:38 2022
# Execution time and date (local): Sat Jan  8 02:08:38 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 824080619.646484            4 
            3          8192           MPI          1 820823460.429688           10 
            3          8192           MPI          2 820295933.316406            6 
            3          8192           MPI          3 820108617.777344            0 
            3          8192           MPI          4 819949905.779297            8 
            3          8192           MPI          5 819928267.453125            0 
            3          8192           MPI          6 819822670.509766            0 
            3          8192           MPI          7 820487897.273438            0 
            3          8192           MPI          8 820237232.726562            8 
            3          8192           MPI          9 820162738.498047            2 
# Runtime: 9879.203642 s (overhead: 0.000000 %) 10 records
