# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Sat Jan  8 01:08:38 2022
# Execution time and date (local): Sat Jan  8 02:08:38 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 824151567.441406            2 
            2          8192           MPI          1 820894146.878906            6 
            2          8192           MPI          2 820366573.255859            6 
            2          8192           MPI          3 820179244.035156            0 
            2          8192           MPI          4 820020526.271484            8 
            2          8192           MPI          5 819998866.488281            2 
            2          8192           MPI          6 819893272.984375            2 
            2          8192           MPI          7 820558549.566406            3 
            2          8192           MPI          8 820307874.197266           11 
            2          8192           MPI          9 820233371.621094            2 
# Runtime: 9880.044224 s (overhead: 0.000000 %) 10 records
