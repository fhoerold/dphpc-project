# Sysname : Linux
# Nodename: eu-a6-007-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:57:31 2022
# Execution time and date (local): Wed Jan  5 18:57:31 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          5793           MPI          0 270882119.164062            2 
            2          5793           MPI          1 275743729.822266            5 
            2          5793           MPI          2 284354169.458984            7 
            2          5793           MPI          3 274097128.675781            3 
            2          5793           MPI          4 271670853.501953            7 
            2          5793           MPI          5 276867534.814453            0 
            2          5793           MPI          6 276829830.023438            0 
            2          5793           MPI          7 274541210.083984            0 
            2          5793           MPI          8 281687028.802734            5 
            2          5793           MPI          9 279534442.158203            0 
# Runtime: 3319.838123 s (overhead: 0.000001 %) 10 records
