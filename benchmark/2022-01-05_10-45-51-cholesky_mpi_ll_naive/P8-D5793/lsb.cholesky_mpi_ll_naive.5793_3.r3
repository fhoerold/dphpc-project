# Sysname : Linux
# Nodename: eu-a6-007-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:57:31 2022
# Execution time and date (local): Wed Jan  5 18:57:31 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          5793           MPI          0 270870554.371094            1 
            3          5793           MPI          1 275731780.466797            4 
            3          5793           MPI          2 284341625.330078            4 
            3          5793           MPI          3 274085293.488281            0 
            3          5793           MPI          4 271659085.564453            4 
            3          5793           MPI          5 276855537.777344            0 
            3          5793           MPI          6 276817827.972656            0 
            3          5793           MPI          7 274529335.673828            0 
            3          5793           MPI          8 281674837.603516            6 
            3          5793           MPI          9 279522323.515625            1 
# Runtime: 3317.698699 s (overhead: 0.000001 %) 10 records
