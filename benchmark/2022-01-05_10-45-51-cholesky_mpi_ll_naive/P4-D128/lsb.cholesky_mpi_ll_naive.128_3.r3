# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 11:12:24 2022
# Execution time and date (local): Wed Jan  5 12:12:24 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 940.398438            0 
            3           128           MPI          1 941.603516            3 
            3           128           MPI          2 919.541016            0 
            3           128           MPI          3 948.968750            0 
            3           128           MPI          4 939.707031            0 
            3           128           MPI          5 918.208984            0 
            3           128           MPI          6 928.851562            0 
            3           128           MPI          7 934.845703            0 
            3           128           MPI          8 944.179688            0 
            3           128           MPI          9 897.746094            0 
# Runtime: 5.003202 s (overhead: 0.000060 %) 10 records
