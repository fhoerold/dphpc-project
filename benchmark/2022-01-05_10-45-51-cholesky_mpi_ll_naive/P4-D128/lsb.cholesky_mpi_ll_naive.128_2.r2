# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 11:12:24 2022
# Execution time and date (local): Wed Jan  5 12:12:24 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 995.248047            0 
            2           128           MPI          1 1354.273438            0 
            2           128           MPI          2 920.197266            0 
            2           128           MPI          3 949.527344            0 
            2           128           MPI          4 940.601562            0 
            2           128           MPI          5 918.705078            0 
            2           128           MPI          6 929.597656            0 
            2           128           MPI          7 935.652344            0 
            2           128           MPI          8 945.296875            0 
            2           128           MPI          9 898.496094            0 
# Runtime: 0.013379 s (overhead: 0.000000 %) 10 records
