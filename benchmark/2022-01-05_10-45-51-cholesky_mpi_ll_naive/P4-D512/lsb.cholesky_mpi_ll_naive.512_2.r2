# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 11:12:37 2022
# Execution time and date (local): Wed Jan  5 12:12:37 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 57893.357422            0 
            2           512           MPI          1 57950.517578            1 
            2           512           MPI          2 55924.882812            0 
            2           512           MPI          3 60693.207031            0 
            2           512           MPI          4 58292.906250            6 
            2           512           MPI          5 57304.044922            0 
            2           512           MPI          6 57714.519531            0 
            2           512           MPI          7 58127.166016            0 
            2           512           MPI          8 56850.240234            1 
            2           512           MPI          9 58779.751953            0 
# Runtime: 0.698343 s (overhead: 0.001146 %) 10 records
