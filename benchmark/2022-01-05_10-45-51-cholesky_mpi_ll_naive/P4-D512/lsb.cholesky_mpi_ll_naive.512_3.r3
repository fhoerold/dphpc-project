# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 11:12:37 2022
# Execution time and date (local): Wed Jan  5 12:12:37 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 57911.710938            0 
            3           512           MPI          1 57556.632812            4 
            3           512           MPI          2 55926.037109            0 
            3           512           MPI          3 60686.320312            0 
            3           512           MPI          4 58293.197266            1 
            3           512           MPI          5 57307.035156            0 
            3           512           MPI          6 57718.080078            0 
            3           512           MPI          7 58135.947266            0 
            3           512           MPI          8 57266.796875            6 
            3           512           MPI          9 58784.585938            0 
# Runtime: 5.692991 s (overhead: 0.000193 %) 10 records
