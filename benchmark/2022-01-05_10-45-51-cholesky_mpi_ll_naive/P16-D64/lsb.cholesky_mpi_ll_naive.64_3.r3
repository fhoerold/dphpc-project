# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 18:54:02 2022
# Execution time and date (local): Wed Jan  5 19:54:02 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            64           MPI          0 1224.457031            1 
            3            64           MPI          1 1998.134766            3 
            3            64           MPI          2 1239.888672            0 
            3            64           MPI          3 1230.177734            0 
            3            64           MPI          4 1226.574219            0 
            3            64           MPI          5 1206.894531            0 
            3            64           MPI          6 1238.435547            0 
            3            64           MPI          7 1189.683594            0 
            3            64           MPI          8 1192.367188            0 
            3            64           MPI          9 1295.892578            0 
# Runtime: 13.010721 s (overhead: 0.000031 %) 10 records
