# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 18:54:02 2022
# Execution time and date (local): Wed Jan  5 19:54:02 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            64           MPI          0 1666.906250            0 
            2            64           MPI          1 1672.929688            4 
            2            64           MPI          2 1239.208984            0 
            2            64           MPI          3 1229.625000            0 
            2            64           MPI          4 1226.060547            0 
            2            64           MPI          5 1206.466797            0 
            2            64           MPI          6 1237.949219            0 
            2            64           MPI          7 1189.244141            0 
            2            64           MPI          8 1191.880859            0 
            2            64           MPI          9 1295.232422            0 
# Runtime: 11.986860 s (overhead: 0.000033 %) 10 records
