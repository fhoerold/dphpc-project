# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:50:03 2022
# Execution time and date (local): Fri Jan  7 17:50:03 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 16027.900391            4 
            2           256           MPI          1 15792.011719            2 
            2           256           MPI          2 15936.419922            2 
            2           256           MPI          3 15082.259766            0 
            2           256           MPI          4 15885.255859            2 
            2           256           MPI          5 15230.880859            0 
            2           256           MPI          6 15130.341797            0 
            2           256           MPI          7 15073.628906            0 
            2           256           MPI          8 15157.029297            4 
            2           256           MPI          9 15153.287109            0 
# Runtime: 7.192825 s (overhead: 0.000195 %) 10 records
