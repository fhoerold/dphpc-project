# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:50:03 2022
# Execution time and date (local): Fri Jan  7 17:50:03 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 15771.791016            0 
            3           256           MPI          1 15650.248047            9 
            3           256           MPI          2 15941.884766            0 
            3           256           MPI          3 15082.460938            0 
            3           256           MPI          4 15889.503906            0 
            3           256           MPI          5 15230.398438            0 
            3           256           MPI          6 15127.738281            0 
            3           256           MPI          7 15071.390625            0 
            3           256           MPI          8 15159.224609            5 
            3           256           MPI          9 15145.720703            0 
# Runtime: 9.193220 s (overhead: 0.000152 %) 10 records
