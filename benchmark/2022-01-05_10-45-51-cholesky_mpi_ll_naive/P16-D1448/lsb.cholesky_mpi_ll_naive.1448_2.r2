# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 18:54:53 2022
# Execution time and date (local): Wed Jan  5 19:54:53 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1448           MPI          0 1479459.240234            0 
            2          1448           MPI          1 1518063.882812           13 
            2          1448           MPI          2 1537070.681641            2 
            2          1448           MPI          3 1540553.257812            0 
            2          1448           MPI          4 1535847.306641            1 
            2          1448           MPI          5 1534481.937500            0 
            2          1448           MPI          6 1539296.349609            1 
            2          1448           MPI          7 1536995.904297            0 
            2          1448           MPI          8 1546367.812500           10 
            2          1448           MPI          9 1508187.896484            0 
# Runtime: 22.408402 s (overhead: 0.000120 %) 10 records
