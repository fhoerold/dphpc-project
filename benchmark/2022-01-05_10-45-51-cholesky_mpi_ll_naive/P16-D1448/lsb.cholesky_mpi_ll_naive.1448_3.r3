# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 18:54:53 2022
# Execution time and date (local): Wed Jan  5 19:54:53 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1448           MPI          0 1479163.792969            0 
            3          1448           MPI          1 1517795.019531            3 
            3          1448           MPI          2 1536779.960938            1 
            3          1448           MPI          3 1540266.853516            0 
            3          1448           MPI          4 1535563.830078            1 
            3          1448           MPI          5 1534198.140625            0 
            3          1448           MPI          6 1539007.976562            0 
            3          1448           MPI          7 1536713.173828            0 
            3          1448           MPI          8 1546081.472656           11 
            3          1448           MPI          9 1507907.390625            0 
# Runtime: 22.329338 s (overhead: 0.000072 %) 10 records
