# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:30:29 2022
# Execution time and date (local): Wed Jan  5 18:30:29 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 5775662.119141            0 
            2          2048           MPI          1 5679057.550781            3 
            2          2048           MPI          2 5610117.628906            1 
            2          2048           MPI          3 5601869.660156            0 
            2          2048           MPI          4 5596618.755859            1 
            2          2048           MPI          5 5627231.351562            0 
            2          2048           MPI          6 5593230.207031            0 
            2          2048           MPI          7 5596589.488281            0 
            2          2048           MPI          8 5601353.894531            1 
            2          2048           MPI          9 5605764.728516            0 
# Runtime: 69.201497 s (overhead: 0.000009 %) 10 records
