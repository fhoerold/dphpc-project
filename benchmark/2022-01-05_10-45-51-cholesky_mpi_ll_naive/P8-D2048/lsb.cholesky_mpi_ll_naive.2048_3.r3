# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:30:29 2022
# Execution time and date (local): Wed Jan  5 18:30:29 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 5775778.617188            0 
            3          2048           MPI          1 5679184.208984            3 
            3          2048           MPI          2 5610237.082031            2 
            3          2048           MPI          3 5601990.984375            0 
            3          2048           MPI          4 5596739.955078            1 
            3          2048           MPI          5 5627352.927734            0 
            3          2048           MPI          6 5593351.642578            0 
            3          2048           MPI          7 5596710.636719            0 
            3          2048           MPI          8 5601474.982422            1 
            3          2048           MPI          9 5605885.373047            0 
# Runtime: 69.212225 s (overhead: 0.000010 %) 10 records
