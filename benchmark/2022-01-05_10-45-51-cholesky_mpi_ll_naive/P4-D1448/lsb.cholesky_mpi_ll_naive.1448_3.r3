# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 11:13:24 2022
# Execution time and date (local): Wed Jan  5 12:13:24 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1448           MPI          0 2081679.394531            0 
            3          1448           MPI          1 2093659.455078            4 
            3          1448           MPI          2 1898413.542969            1 
            3          1448           MPI          3 1802466.115234            0 
            3          1448           MPI          4 1760847.507812            3 
            3          1448           MPI          5 1937535.039062            0 
            3          1448           MPI          6 1919729.822266            0 
            3          1448           MPI          7 1942756.914062            0 
            3          1448           MPI          8 1919683.406250            1 
            3          1448           MPI          9 2001721.923828            0 
# Runtime: 27.676123 s (overhead: 0.000033 %) 10 records
