# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 11:13:24 2022
# Execution time and date (local): Wed Jan  5 12:13:24 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1448           MPI          0 2081821.398438            0 
            2          1448           MPI          1 2093872.601562            5 
            2          1448           MPI          2 1898539.113281            4 
            2          1448           MPI          3 1802580.050781            0 
            2          1448           MPI          4 1760947.048828            1 
            2          1448           MPI          5 1937657.701172            0 
            2          1448           MPI          6 1919850.005859            0 
            2          1448           MPI          7 1942879.550781            0 
            2          1448           MPI          8 1919803.294922            2 
            2          1448           MPI          9 2001853.384766            0 
# Runtime: 30.686655 s (overhead: 0.000039 %) 10 records
