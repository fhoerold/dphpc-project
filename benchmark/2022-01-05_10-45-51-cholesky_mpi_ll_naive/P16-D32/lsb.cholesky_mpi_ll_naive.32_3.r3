# Sysname : Linux
# Nodename: eu-a6-003-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 01:31:47 2022
# Execution time and date (local): Thu Jan  6 02:31:47 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 513.324219            0 
            3            32           MPI          1 812.667969            0 
            3            32           MPI          2 139.357422            0 
            3            32           MPI          3 136.332031            0 
            3            32           MPI          4 130.787109            0 
            3            32           MPI          5 145.060547            0 
            3            32           MPI          6 138.755859            0 
            3            32           MPI          7 141.000000            0 
            3            32           MPI          8 141.658203            0 
            3            32           MPI          9 140.433594            0 
# Runtime: 0.004686 s (overhead: 0.000000 %) 10 records
