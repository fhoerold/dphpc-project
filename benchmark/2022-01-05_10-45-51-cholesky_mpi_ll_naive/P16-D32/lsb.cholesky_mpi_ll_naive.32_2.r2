# Sysname : Linux
# Nodename: eu-a6-003-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 01:31:47 2022
# Execution time and date (local): Thu Jan  6 02:31:47 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 561.050781            0 
            2            32           MPI          1 127.300781            2 
            2            32           MPI          2 137.105469            0 
            2            32           MPI          3 133.117188            0 
            2            32           MPI          4 128.689453            0 
            2            32           MPI          5 141.925781            0 
            2            32           MPI          6 136.666016            0 
            2            32           MPI          7 137.718750            0 
            2            32           MPI          8 141.203125            0 
            2            32           MPI          9 134.515625            0 
# Runtime: 9.009758 s (overhead: 0.000022 %) 10 records
