# Sysname : Linux
# Nodename: eu-a6-003-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 01:32:02 2022
# Execution time and date (local): Thu Jan  6 02:32:02 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 50384.716797            0 
            3           512           MPI          1 49728.699219            3 
            3           512           MPI          2 50717.775391            0 
            3           512           MPI          3 50831.257812            0 
            3           512           MPI          4 51282.626953            0 
            3           512           MPI          5 52396.339844            0 
            3           512           MPI          6 49558.511719            0 
            3           512           MPI          7 52637.958984            0 
            3           512           MPI          8 51883.642578            3 
            3           512           MPI          9 51213.931641            0 
# Runtime: 7.600462 s (overhead: 0.000079 %) 10 records
