# Sysname : Linux
# Nodename: eu-a6-003-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 01:32:02 2022
# Execution time and date (local): Thu Jan  6 02:32:02 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 50386.125000            0 
            2           512           MPI          1 50137.578125            4 
            2           512           MPI          2 50432.164062            0 
            2           512           MPI          3 50827.048828            0 
            2           512           MPI          4 51277.880859            0 
            2           512           MPI          5 52392.632812            0 
            2           512           MPI          6 49554.548828            0 
            2           512           MPI          7 52634.171875            0 
            2           512           MPI          8 51879.380859            0 
            2           512           MPI          9 51205.238281            0 
# Runtime: 7.608270 s (overhead: 0.000053 %) 10 records
