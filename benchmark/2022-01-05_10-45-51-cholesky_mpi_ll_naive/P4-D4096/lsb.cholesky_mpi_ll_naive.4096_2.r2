# Sysname : Linux
# Nodename: eu-a6-009-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 11:27:00 2022
# Execution time and date (local): Wed Jan  5 12:27:00 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 82709519.859375            2 
            2          4096           MPI          1 81864368.109375            5 
            2          4096           MPI          2 79619207.652344            6 
            2          4096           MPI          3 80686389.230469            0 
            2          4096           MPI          4 79824049.197266            5 
            2          4096           MPI          5 81518233.796875            0 
            2          4096           MPI          6 81133284.654297            0 
            2          4096           MPI          7 81722886.757812            0 
            2          4096           MPI          8 87761265.162109            5 
            2          4096           MPI          9 89829879.224609            0 
# Runtime: 999.272531 s (overhead: 0.000002 %) 10 records
