# Sysname : Linux
# Nodename: eu-a6-009-05
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 11:27:00 2022
# Execution time and date (local): Wed Jan  5 12:27:00 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 82705372.587891            0 
            3          4096           MPI          1 81860262.220703            3 
            3          4096           MPI          2 79615227.251953            3 
            3          4096           MPI          3 80682363.224609            0 
            3          4096           MPI          4 79820061.285156            4 
            3          4096           MPI          5 81514158.363281            0 
            3          4096           MPI          6 81129229.689453            0 
            3          4096           MPI          7 81718801.871094            0 
            3          4096           MPI          8 87756878.363281            6 
            3          4096           MPI          9 89825392.550781            2 
# Runtime: 1002.227012 s (overhead: 0.000002 %) 10 records
