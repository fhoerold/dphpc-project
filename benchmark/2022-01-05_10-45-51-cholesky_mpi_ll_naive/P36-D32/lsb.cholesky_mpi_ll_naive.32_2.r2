# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 23:11:13 2022
# Execution time and date (local): Sat Jan  8 00:11:13 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 9.699219            0 
            2            32           MPI          1 0.890625            2 
            2            32           MPI          2 0.421875            0 
            2            32           MPI          3 0.330078            0 
            2            32           MPI          4 0.246094            0 
            2            32           MPI          5 0.234375            0 
            2            32           MPI          6 0.197266            0 
            2            32           MPI          7 0.212891            0 
            2            32           MPI          8 0.189453            0 
            2            32           MPI          9 0.199219            0 
# Runtime: 3.031547 s (overhead: 0.000066 %) 10 records
