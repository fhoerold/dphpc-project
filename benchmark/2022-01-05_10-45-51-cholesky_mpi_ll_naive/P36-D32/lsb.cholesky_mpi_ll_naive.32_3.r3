# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 23:11:12 2022
# Execution time and date (local): Sat Jan  8 00:11:12 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 17.960938            3 
            3            32           MPI          1 0.466797            4 
            3            32           MPI          2 0.347656            0 
            3            32           MPI          3 0.277344            0 
            3            32           MPI          4 0.468750            1 
            3            32           MPI          5 0.226562            0 
            3            32           MPI          6 0.220703            0 
            3            32           MPI          7 0.486328            0 
            3            32           MPI          8 0.207031            3 
            3            32           MPI          9 0.197266            0 
# Runtime: 5.033237 s (overhead: 0.000219 %) 10 records
