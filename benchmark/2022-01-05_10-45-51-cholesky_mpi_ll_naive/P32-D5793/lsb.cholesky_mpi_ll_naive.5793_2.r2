# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:53:08 2022
# Execution time and date (local): Fri Jan  7 18:53:08 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          5793           MPI          0 217269613.277344            3 
            2          5793           MPI          1 215054714.039062            7 
            2          5793           MPI          2 213431287.722656            9 
            2          5793           MPI          3 211686994.830078            2 
            2          5793           MPI          4 212890653.082031            5 
            2          5793           MPI          5 211217439.855469            0 
            2          5793           MPI          6 211722970.650391            0 
            2          5793           MPI          7 211700775.568359            0 
            2          5793           MPI          8 211280236.117188            8 
            2          5793           MPI          9 212093929.673828            1 
# Runtime: 2576.180713 s (overhead: 0.000001 %) 10 records
