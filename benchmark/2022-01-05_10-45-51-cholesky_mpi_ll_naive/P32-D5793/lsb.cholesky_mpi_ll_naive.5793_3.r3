# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:53:08 2022
# Execution time and date (local): Fri Jan  7 18:53:08 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          5793           MPI          0 217271417.519531            0 
            3          5793           MPI          1 215056532.935547            6 
            3          5793           MPI          2 213433091.382812            3 
            3          5793           MPI          3 211688782.626953            0 
            3          5793           MPI          4 212892459.882812            9 
            3          5793           MPI          5 211219218.912109            2 
            3          5793           MPI          6 211724762.515625            2 
            3          5793           MPI          7 211702562.816406            2 
            3          5793           MPI          8 211282018.201172            9 
            3          5793           MPI          9 212095727.781250            2 
# Runtime: 2568.198616 s (overhead: 0.000001 %) 10 records
