# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 23:12:08 2022
# Execution time and date (local): Sat Jan  8 00:12:08 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 17984.941406            0 
            2           256           MPI          1 17754.136719            5 
            2           256           MPI          2 18449.759766            0 
            2           256           MPI          3 19228.808594            0 
            2           256           MPI          4 18632.113281           11 
            2           256           MPI          5 18103.226562            0 
            2           256           MPI          6 17957.669922            0 
            2           256           MPI          7 17684.216797            0 
            2           256           MPI          8 17346.724609            2 
            2           256           MPI          9 17864.447266            0 
# Runtime: 5.236283 s (overhead: 0.000344 %) 10 records
