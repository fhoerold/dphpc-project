# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 23:12:08 2022
# Execution time and date (local): Sat Jan  8 00:12:08 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 17969.955078            0 
            3           256           MPI          1 17753.072266            6 
            3           256           MPI          2 18452.328125            0 
            3           256           MPI          3 19239.703125            1 
            3           256           MPI          4 18625.941406            4 
            3           256           MPI          5 18104.562500            0 
            3           256           MPI          6 17958.781250            0 
            3           256           MPI          7 17685.769531            0 
            3           256           MPI          8 17348.105469            5 
            3           256           MPI          9 17865.589844            0 
# Runtime: 8.233113 s (overhead: 0.000194 %) 10 records
