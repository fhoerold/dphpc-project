# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 23:11:46 2022
# Execution time and date (local): Sat Jan  8 00:11:46 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 9026.679688            0 
            3           128           MPI          1 8290.589844            3 
            3           128           MPI          2 6525.318359            0 
            3           128           MPI          3 5911.015625            0 
            3           128           MPI          4 8331.322266            2 
            3           128           MPI          5 8349.837891            0 
            3           128           MPI          6 6546.587891            0 
            3           128           MPI          7 7266.382812            0 
            3           128           MPI          8 8486.335938            0 
            3           128           MPI          9 7015.312500            0 
# Runtime: 13.094202 s (overhead: 0.000038 %) 10 records
