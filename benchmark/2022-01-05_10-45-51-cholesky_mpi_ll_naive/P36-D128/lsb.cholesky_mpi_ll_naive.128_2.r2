# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 23:11:46 2022
# Execution time and date (local): Sat Jan  8 00:11:46 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 8664.445312            0 
            2           128           MPI          1 8657.113281            6 
            2           128           MPI          2 6522.919922            0 
            2           128           MPI          3 5909.511719            0 
            2           128           MPI          4 8331.501953            0 
            2           128           MPI          5 8345.044922            0 
            2           128           MPI          6 6538.816406            0 
            2           128           MPI          7 7264.068359            0 
            2           128           MPI          8 8484.726562            2 
            2           128           MPI          9 7015.261719            0 
# Runtime: 15.099591 s (overhead: 0.000053 %) 10 records
