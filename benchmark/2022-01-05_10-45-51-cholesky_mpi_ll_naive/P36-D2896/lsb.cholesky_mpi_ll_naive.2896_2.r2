# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 23:18:06 2022
# Execution time and date (local): Sat Jan  8 00:18:06 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2896           MPI          0 22170879.277344            3 
            2          2896           MPI          1 21374378.634766            7 
            2          2896           MPI          2 21124451.968750            7 
            2          2896           MPI          3 21863469.265625            0 
            2          2896           MPI          4 21677016.708984            7 
            2          2896           MPI          5 21255412.306641            0 
            2          2896           MPI          6 21636444.908203            0 
            2          2896           MPI          7 21961067.347656            0 
            2          2896           MPI          8 21606392.916016           10 
            2          2896           MPI          9 21644394.228516            0 
# Runtime: 275.685248 s (overhead: 0.000012 %) 10 records
