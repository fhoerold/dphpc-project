# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 23:18:06 2022
# Execution time and date (local): Sat Jan  8 00:18:06 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2896           MPI          0 22170804.927734            0 
            3          2896           MPI          1 21374327.712891            5 
            3          2896           MPI          2 21124404.259766           11 
            3          2896           MPI          3 21863415.628906            1 
            3          2896           MPI          4 21676959.208984            6 
            3          2896           MPI          5 21255348.617188            0 
            3          2896           MPI          6 21636393.996094            0 
            3          2896           MPI          7 21961007.876953            0 
            3          2896           MPI          8 21606347.884766           12 
            3          2896           MPI          9 21644336.740234            0 
# Runtime: 276.687943 s (overhead: 0.000013 %) 10 records
