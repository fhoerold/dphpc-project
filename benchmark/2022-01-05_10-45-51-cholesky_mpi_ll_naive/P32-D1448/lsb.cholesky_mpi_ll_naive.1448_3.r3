# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:20:45 2022
# Execution time and date (local): Fri Jan  7 18:20:45 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1448           MPI          0 1394577.070312            4 
            3          1448           MPI          1 1304035.560547            6 
            3          1448           MPI          2 1304475.150391            8 
            3          1448           MPI          3 1346890.773438            0 
            3          1448           MPI          4 1303635.429688            1 
            3          1448           MPI          5 1384394.580078            0 
            3          1448           MPI          6 1316921.921875            0 
            3          1448           MPI          7 1309173.419922            0 
            3          1448           MPI          8 1285418.208984            2 
            3          1448           MPI          9 1300993.640625            0 
# Runtime: 28.923221 s (overhead: 0.000073 %) 10 records
