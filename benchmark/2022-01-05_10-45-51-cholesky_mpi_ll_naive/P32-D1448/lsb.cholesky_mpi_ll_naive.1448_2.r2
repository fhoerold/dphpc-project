# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:20:45 2022
# Execution time and date (local): Fri Jan  7 18:20:45 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1448           MPI          0 1394568.779297            3 
            2          1448           MPI          1 1304031.296875            6 
            2          1448           MPI          2 1304469.304688            1 
            2          1448           MPI          3 1346886.167969            0 
            2          1448           MPI          4 1303626.173828            2 
            2          1448           MPI          5 1384385.138672            0 
            2          1448           MPI          6 1316913.048828            0 
            2          1448           MPI          7 1309164.783203            0 
            2          1448           MPI          8 1285409.341797            6 
            2          1448           MPI          9 1300984.171875            0 
# Runtime: 29.925044 s (overhead: 0.000060 %) 10 records
