# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 23:13:00 2022
# Execution time and date (local): Sat Jan  8 00:13:00 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1448           MPI          0 1392725.912109            0 
            3          1448           MPI          1 1345936.466797           10 
            3          1448           MPI          2 1381153.591797            1 
            3          1448           MPI          3 1526618.759766            0 
            3          1448           MPI          4 1595042.550781            7 
            3          1448           MPI          5 1462922.464844            0 
            3          1448           MPI          6 1434933.259766            1 
            3          1448           MPI          7 1357672.845703            0 
            3          1448           MPI          8 1341116.160156            6 
            3          1448           MPI          9 1354142.652344            0 
# Runtime: 26.010929 s (overhead: 0.000096 %) 10 records
