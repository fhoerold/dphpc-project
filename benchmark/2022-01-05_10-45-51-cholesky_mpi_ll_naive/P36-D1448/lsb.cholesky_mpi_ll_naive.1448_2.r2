# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 23:13:00 2022
# Execution time and date (local): Sat Jan  8 00:13:00 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1448           MPI          0 1392733.791016            4 
            2          1448           MPI          1 1345946.119141            6 
            2          1448           MPI          2 1381121.945312            1 
            2          1448           MPI          3 1526599.173828            0 
            2          1448           MPI          4 1595039.632812            2 
            2          1448           MPI          5 1462921.253906            0 
            2          1448           MPI          6 1434948.361328            2 
            2          1448           MPI          7 1357657.917969            0 
            2          1448           MPI          8 1341115.800781            9 
            2          1448           MPI          9 1354133.855469            0 
# Runtime: 21.014276 s (overhead: 0.000114 %) 10 records
