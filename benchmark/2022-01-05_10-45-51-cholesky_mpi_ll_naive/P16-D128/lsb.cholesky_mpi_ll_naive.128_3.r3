# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 18:54:22 2022
# Execution time and date (local): Wed Jan  5 19:54:22 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 4151.197266            0 
            3           128           MPI          1 5478.937500            6 
            3           128           MPI          2 4025.945312            2 
            3           128           MPI          3 3797.308594            0 
            3           128           MPI          4 4238.931641            0 
            3           128           MPI          5 3711.410156            0 
            3           128           MPI          6 3831.455078            0 
            3           128           MPI          7 3766.019531            0 
            3           128           MPI          8 3726.085938            0 
            3           128           MPI          9 3829.623047            0 
# Runtime: 3.864512 s (overhead: 0.000207 %) 10 records
