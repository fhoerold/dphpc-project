# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 18:54:22 2022
# Execution time and date (local): Wed Jan  5 19:54:22 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 4161.771484            0 
            2           128           MPI          1 5513.787109            5 
            2           128           MPI          2 3699.482422            4 
            2           128           MPI          3 3796.361328            0 
            2           128           MPI          4 4238.277344            0 
            2           128           MPI          5 3711.730469            0 
            2           128           MPI          6 3831.453125            0 
            2           128           MPI          7 3765.199219            0 
            2           128           MPI          8 3729.773438            0 
            2           128           MPI          9 3829.105469            0 
# Runtime: 3.978680 s (overhead: 0.000226 %) 10 records
