# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:23:51 2022
# Execution time and date (local): Fri Jan  7 18:23:51 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2896           MPI          0 21018407.660156            0 
            3          2896           MPI          1 21476821.751953            5 
            3          2896           MPI          2 20823909.259766            3 
            3          2896           MPI          3 20886078.388672            2 
            3          2896           MPI          4 20576010.332031           10 
            3          2896           MPI          5 20515519.630859            0 
            3          2896           MPI          6 21017738.644531            0 
            3          2896           MPI          7 20502332.603516            0 
            3          2896           MPI          8 20938956.707031            6 
            3          2896           MPI          9 20846099.851562            0 
# Runtime: 263.998757 s (overhead: 0.000010 %) 10 records
