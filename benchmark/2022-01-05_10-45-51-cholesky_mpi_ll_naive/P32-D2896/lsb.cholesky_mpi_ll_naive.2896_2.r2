# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:23:51 2022
# Execution time and date (local): Fri Jan  7 18:23:51 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2896           MPI          0 21018425.316406            0 
            2          2896           MPI          1 21476849.992188            6 
            2          2896           MPI          2 20823938.990234            2 
            2          2896           MPI          3 20886105.982422            0 
            2          2896           MPI          4 20576042.691406            6 
            2          2896           MPI          5 20515537.001953            0 
            2          2896           MPI          6 21017781.177734            0 
            2          2896           MPI          7 20502353.640625            0 
            2          2896           MPI          8 20938978.429688           12 
            2          2896           MPI          9 20846133.167969            4 
# Runtime: 260.004990 s (overhead: 0.000012 %) 10 records
