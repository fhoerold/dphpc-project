# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 23:11:28 2022
# Execution time and date (local): Sat Jan  8 00:11:28 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            64           MPI          0 1286.363281            0 
            3            64           MPI          1 1734.642578            3 
            3            64           MPI          2 1276.257812            0 
            3            64           MPI          3 1245.263672            0 
            3            64           MPI          4 1212.716797            0 
            3            64           MPI          5 1215.062500            0 
            3            64           MPI          6 1211.765625            0 
            3            64           MPI          7 1242.966797            0 
            3            64           MPI          8 1271.902344            0 
            3            64           MPI          9 1269.470703            0 
# Runtime: 7.994286 s (overhead: 0.000038 %) 10 records
