# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 23:11:28 2022
# Execution time and date (local): Sat Jan  8 00:11:28 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            64           MPI          0 1724.087891            0 
            2            64           MPI          1 1684.156250            3 
            2            64           MPI          2 1274.300781            0 
            2            64           MPI          3 1243.994141            0 
            2            64           MPI          4 1212.048828            0 
            2            64           MPI          5 1214.816406            0 
            2            64           MPI          6 1211.035156            0 
            2            64           MPI          7 1242.166016            0 
            2            64           MPI          8 1271.156250            0 
            2            64           MPI          9 1270.199219            0 
# Runtime: 6.019937 s (overhead: 0.000050 %) 10 records
