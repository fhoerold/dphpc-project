# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 11:08:23 2022
# Execution time and date (local): Wed Jan  5 12:08:23 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 463.734375            0 
            3            32           MPI          1 76.466797            2 
            3            32           MPI          2 459.019531            0 
            3            32           MPI          3 75.476562            0 
            3            32           MPI          4 74.728516            0 
            3            32           MPI          5 74.158203            0 
            3            32           MPI          6 89.351562            0 
            3            32           MPI          7 76.568359            0 
            3            32           MPI          8 73.464844            0 
            3            32           MPI          9 83.607422            0 
# Runtime: 8.999642 s (overhead: 0.000022 %) 10 records
