# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 11:08:23 2022
# Execution time and date (local): Wed Jan  5 12:08:23 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 503.347656            1 
            2            32           MPI          1 434.441406            3 
            2            32           MPI          2 76.896484            0 
            2            32           MPI          3 75.207031            0 
            2            32           MPI          4 74.371094            0 
            2            32           MPI          5 73.611328            0 
            2            32           MPI          6 88.970703            0 
            2            32           MPI          7 75.943359            0 
            2            32           MPI          8 73.130859            0 
            2            32           MPI          9 83.136719            0 
# Runtime: 8.997203 s (overhead: 0.000044 %) 10 records
