# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:28:36 2022
# Execution time and date (local): Wed Jan  5 18:28:36 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 457.941406            1 
            2            32           MPI          1 101.968750            2 
            2            32           MPI          2 140.896484            0 
            2            32           MPI          3 89.462891            0 
            2            32           MPI          4 89.406250            0 
            2            32           MPI          5 96.882812            0 
            2            32           MPI          6 93.601562            0 
            2            32           MPI          7 100.638672            0 
            2            32           MPI          8 87.419922            0 
            2            32           MPI          9 89.675781            0 
# Runtime: 24.971269 s (overhead: 0.000012 %) 10 records
