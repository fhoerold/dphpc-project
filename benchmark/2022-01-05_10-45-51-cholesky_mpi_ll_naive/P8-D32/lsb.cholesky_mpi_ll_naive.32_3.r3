# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:28:36 2022
# Execution time and date (local): Wed Jan  5 18:28:36 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 138.261719            1 
            3            32           MPI          1 101.986328            2 
            3            32           MPI          2 455.601562            0 
            3            32           MPI          3 89.919922            0 
            3            32           MPI          4 89.759766            0 
            3            32           MPI          5 97.210938            0 
            3            32           MPI          6 93.953125            0 
            3            32           MPI          7 101.101562            0 
            3            32           MPI          8 87.736328            1 
            3            32           MPI          9 90.068359            0 
# Runtime: 24.007642 s (overhead: 0.000017 %) 10 records
