# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:20:10 2022
# Execution time and date (local): Fri Jan  7 18:20:10 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 511605.640625            0 
            3          1024           MPI          1 501800.468750            4 
            3          1024           MPI          2 518273.087891            2 
            3          1024           MPI          3 503090.498047            0 
            3          1024           MPI          4 501483.847656            2 
            3          1024           MPI          5 501122.193359            0 
            3          1024           MPI          6 504536.761719            0 
            3          1024           MPI          7 515112.244141            0 
            3          1024           MPI          8 507380.082031            6 
            3          1024           MPI          9 509153.804688            0 
# Runtime: 22.218276 s (overhead: 0.000063 %) 10 records
