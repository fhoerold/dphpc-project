# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:20:10 2022
# Execution time and date (local): Fri Jan  7 18:20:10 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 511633.925781            0 
            2          1024           MPI          1 501840.376953            6 
            2          1024           MPI          2 518322.609375            1 
            2          1024           MPI          3 503139.742188            0 
            2          1024           MPI          4 501534.554688            2 
            2          1024           MPI          5 501171.398438            0 
            2          1024           MPI          6 504588.667969            0 
            2          1024           MPI          7 515161.404297            0 
            2          1024           MPI          8 507424.328125            1 
            2          1024           MPI          9 509202.257812            0 
# Runtime: 14.236415 s (overhead: 0.000070 %) 10 records
