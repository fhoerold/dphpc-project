# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:29:26 2022
# Execution time and date (local): Wed Jan  5 18:29:26 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 396388.458984            0 
            2          1024           MPI          1 396109.837891            4 
            2          1024           MPI          2 396134.050781            1 
            2          1024           MPI          3 396223.171875            0 
            2          1024           MPI          4 396680.910156            1 
            2          1024           MPI          5 397649.693359            0 
            2          1024           MPI          6 397191.492188            0 
            2          1024           MPI          7 399607.023438            1 
            2          1024           MPI          8 397870.556641            5 
            2          1024           MPI          9 397704.033203            0 
# Runtime: 9.775024 s (overhead: 0.000123 %) 10 records
