# Sysname : Linux
# Nodename: eu-a6-003-10
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 17:29:27 2022
# Execution time and date (local): Wed Jan  5 18:29:27 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 395742.386719            3 
            3          1024           MPI          1 395471.480469            3 
            3          1024           MPI          2 395497.880859            0 
            3          1024           MPI          3 395586.488281            0 
            3          1024           MPI          4 396049.205078            2 
            3          1024           MPI          5 397011.195312            0 
            3          1024           MPI          6 396341.763672            0 
            3          1024           MPI          7 398958.437500            0 
            3          1024           MPI          8 397229.916016            1 
            3          1024           MPI          9 397065.666016            0 
# Runtime: 10.735471 s (overhead: 0.000084 %) 10 records
