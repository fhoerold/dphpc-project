# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 18:54:34 2022
# Execution time and date (local): Wed Jan  5 19:54:34 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 12653.927734            0 
            2           256           MPI          1 13716.060547            9 
            2           256           MPI          2 12631.199219            0 
            2           256           MPI          3 12775.648438            0 
            2           256           MPI          4 12829.361328            4 
            2           256           MPI          5 12882.449219            0 
            2           256           MPI          6 12470.734375            0 
            2           256           MPI          7 12632.320312            0 
            2           256           MPI          8 12644.669922            4 
            2           256           MPI          9 12776.765625            0 
# Runtime: 5.168116 s (overhead: 0.000329 %) 10 records
