# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 18:54:34 2022
# Execution time and date (local): Wed Jan  5 19:54:34 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 12663.623047            0 
            3           256           MPI          1 13387.328125            4 
            3           256           MPI          2 12622.982422            0 
            3           256           MPI          3 12767.119141            0 
            3           256           MPI          4 12820.775391            0 
            3           256           MPI          5 12870.941406            0 
            3           256           MPI          6 12463.236328            0 
            3           256           MPI          7 12628.212891            0 
            3           256           MPI          8 12631.091797            4 
            3           256           MPI          9 12765.441406            0 
# Runtime: 7.149112 s (overhead: 0.000112 %) 10 records
