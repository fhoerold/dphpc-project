# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 23:12:28 2022
# Execution time and date (local): Sat Jan  8 00:12:28 2022
# MPI execution on rank 3 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 614933.287109            3 
            3          1024           MPI          1 619382.082031            5 
            3          1024           MPI          2 535117.884766            2 
            3          1024           MPI          3 521407.498047            0 
            3          1024           MPI          4 516991.871094            6 
            3          1024           MPI          5 591502.865234            0 
            3          1024           MPI          6 639629.724609            0 
            3          1024           MPI          7 569957.820312            0 
            3          1024           MPI          8 525812.744141            2 
            3          1024           MPI          9 527754.029297            0 
# Runtime: 20.932893 s (overhead: 0.000086 %) 10 records
