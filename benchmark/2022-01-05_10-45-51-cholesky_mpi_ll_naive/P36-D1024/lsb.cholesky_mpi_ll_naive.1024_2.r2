# Sysname : Linux
# Nodename: eu-a6-006-21
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 23:12:28 2022
# Execution time and date (local): Sat Jan  8 00:12:28 2022
# MPI execution on rank 2 with 36 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 614921.287109            0 
            2          1024           MPI          1 619378.984375            6 
            2          1024           MPI          2 535124.333984            3 
            2          1024           MPI          3 521395.255859            0 
            2          1024           MPI          4 516983.837891            2 
            2          1024           MPI          5 591481.253906            0 
            2          1024           MPI          6 639622.416016            0 
            2          1024           MPI          7 569952.455078            0 
            2          1024           MPI          8 525812.353516            2 
            2          1024           MPI          9 527744.044922            0 
# Runtime: 20.933183 s (overhead: 0.000062 %) 10 records
