# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 11:09:56 2022
# Execution time and date (local): Wed Jan  5 12:09:56 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 8704007.125000            0 
            2          2048           MPI          1 7822244.597656            6 
            2          2048           MPI          2 7671496.771484            1 
            2          2048           MPI          3 7677860.919922            0 
            2          2048           MPI          4 7743613.816406            2 
            2          2048           MPI          5 8359769.472656            0 
            2          2048           MPI          6 9933609.638672            0 
            2          2048           MPI          7 9986313.937500            0 
            2          2048           MPI          8 9860544.916016            5 
            2          2048           MPI          9 8745507.107422            0 
# Runtime: 117.963216 s (overhead: 0.000012 %) 10 records
