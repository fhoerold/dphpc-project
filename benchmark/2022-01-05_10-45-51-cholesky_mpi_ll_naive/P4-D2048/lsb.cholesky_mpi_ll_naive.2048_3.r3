# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 11:09:56 2022
# Execution time and date (local): Wed Jan  5 12:09:56 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 8713115.904297            0 
            3          2048           MPI          1 7830423.531250            6 
            3          2048           MPI          2 7679526.371094            1 
            3          2048           MPI          3 7685896.550781            0 
            3          2048           MPI          4 7751719.998047            1 
            3          2048           MPI          5 8368519.283203            0 
            3          2048           MPI          6 9944006.728516            0 
            3          2048           MPI          7 9996762.730469            0 
            3          2048           MPI          8 9870862.300781           10 
            3          2048           MPI          9 8754659.705078            0 
# Runtime: 116.094508 s (overhead: 0.000016 %) 10 records
