# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:49:28 2022
# Execution time and date (local): Fri Jan  7 17:49:28 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 10.365234            1 
            3            32           MPI          1 0.673828            3 
            3            32           MPI          2 0.417969            0 
            3            32           MPI          3 0.208984            0 
            3            32           MPI          4 0.250000            0 
            3            32           MPI          5 0.195312            0 
            3            32           MPI          6 0.216797            0 
            3            32           MPI          7 0.183594            0 
            3            32           MPI          8 0.205078            0 
            3            32           MPI          9 0.375000            0 
# Runtime: 3.012840 s (overhead: 0.000133 %) 10 records
