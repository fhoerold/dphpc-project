# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 16:49:28 2022
# Execution time and date (local): Fri Jan  7 17:49:28 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 8.908203            0 
            2            32           MPI          1 0.402344            3 
            2            32           MPI          2 0.255859            0 
            2            32           MPI          3 0.183594            0 
            2            32           MPI          4 0.242188            0 
            2            32           MPI          5 0.169922            0 
            2            32           MPI          6 0.185547            0 
            2            32           MPI          7 0.404297            0 
            2            32           MPI          8 0.398438            2 
            2            32           MPI          9 0.394531            0 
# Runtime: 6.008037 s (overhead: 0.000083 %) 10 records
