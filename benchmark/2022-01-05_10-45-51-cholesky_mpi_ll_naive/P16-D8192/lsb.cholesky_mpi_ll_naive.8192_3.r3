# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 04:40:26 2022
# Execution time and date (local): Thu Jan  6 05:40:26 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 753214961.769531            0 
            3          8192           MPI          1 750514340.390625            3 
            3          8192           MPI          2 756438985.816406            6 
            3          8192           MPI          3 747929935.980469            1 
            3          8192           MPI          4 739849943.958984            7 
            3          8192           MPI          5 747913851.945312            1 
            3          8192           MPI          6 677755386.519531            1 
            3          8192           MPI          7 611582578.763672            1 
            3          8192           MPI          8 612934823.132812            6 
            3          8192           MPI          9 612347487.902344            3 
# Runtime: 8560.738796 s (overhead: 0.000000 %) 10 records
