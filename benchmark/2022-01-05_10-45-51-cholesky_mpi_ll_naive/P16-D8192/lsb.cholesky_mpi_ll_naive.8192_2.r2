# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 04:40:26 2022
# Execution time and date (local): Thu Jan  6 05:40:26 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 753246748.550781            0 
            2          8192           MPI          1 750546007.968750            5 
            2          8192           MPI          2 756470907.445312            3 
            2          8192           MPI          3 747961497.595703            0 
            2          8192           MPI          4 739881162.443359            4 
            2          8192           MPI          5 747945413.484375            0 
            2          8192           MPI          6 677783986.699219            0 
            2          8192           MPI          7 611608384.066406            0 
            2          8192           MPI          8 612960687.207031            8 
            2          8192           MPI          9 612373326.357422            1 
# Runtime: 8556.101062 s (overhead: 0.000000 %) 10 records
