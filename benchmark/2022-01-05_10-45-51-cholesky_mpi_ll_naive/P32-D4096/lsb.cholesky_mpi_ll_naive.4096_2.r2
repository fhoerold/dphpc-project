# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:00:32 2022
# Execution time and date (local): Fri Jan  7 18:00:32 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 91959899.408203            2 
            2          4096           MPI          1 90654108.916016            9 
            2          4096           MPI          2 90502574.359375            6 
            2          4096           MPI          3 90057711.908203            2 
            2          4096           MPI          4 89702765.935547            6 
            2          4096           MPI          5 89462864.988281            0 
            2          4096           MPI          6 90098033.628906            0 
            2          4096           MPI          7 90087149.603516            0 
            2          4096           MPI          8 89619053.310547            8 
            2          4096           MPI          9 90051225.500000            2 
# Runtime: 1096.670413 s (overhead: 0.000003 %) 10 records
