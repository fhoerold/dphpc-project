# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:00:32 2022
# Execution time and date (local): Fri Jan  7 18:00:32 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 91963911.832031            1 
            3          4096           MPI          1 90658069.263672            6 
            3          4096           MPI          2 90506532.250000            6 
            3          4096           MPI          3 90061649.833984            0 
            3          4096           MPI          4 89706689.396484            8 
            3          4096           MPI          5 89466771.769531            2 
            3          4096           MPI          6 90101982.462891            2 
            3          4096           MPI          7 90091082.904297            2 
            3          4096           MPI          8 89622970.000000            9 
            3          4096           MPI          9 90055163.279297            4 
# Runtime: 1090.716414 s (overhead: 0.000004 %) 10 records
