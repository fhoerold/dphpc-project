# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 11:12:55 2022
# Execution time and date (local): Wed Jan  5 12:12:55 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 894799.074219            0 
            3          1024           MPI          1 847195.142578            4 
            3          1024           MPI          2 822814.828125            1 
            3          1024           MPI          3 853298.761719            0 
            3          1024           MPI          4 826001.080078            1 
            3          1024           MPI          5 842520.830078            0 
            3          1024           MPI          6 822827.253906            0 
            3          1024           MPI          7 835137.578125            0 
            3          1024           MPI          8 839204.189453            3 
            3          1024           MPI          9 835474.455078            0 
# Runtime: 17.207203 s (overhead: 0.000052 %) 10 records
