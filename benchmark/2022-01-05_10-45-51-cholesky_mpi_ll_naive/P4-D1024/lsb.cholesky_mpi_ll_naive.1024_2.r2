# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 11:12:55 2022
# Execution time and date (local): Wed Jan  5 12:12:55 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 896140.730469            0 
            2          1024           MPI          1 848222.179688            5 
            2          1024           MPI          2 823538.500000            2 
            2          1024           MPI          3 854336.697266            0 
            2          1024           MPI          4 826999.171875            6 
            2          1024           MPI          5 843538.355469            0 
            2          1024           MPI          6 823822.080078            0 
            2          1024           MPI          7 836147.992188            0 
            2          1024           MPI          8 840207.863281            2 
            2          1024           MPI          9 836483.746094            0 
# Runtime: 13.240183 s (overhead: 0.000113 %) 10 records
