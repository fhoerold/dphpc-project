# Sysname : Linux
# Nodename: eu-a6-003-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 01:32:23 2022
# Execution time and date (local): Thu Jan  6 02:32:23 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 440677.984375            4 
            2          1024           MPI          1 436291.148438            5 
            2          1024           MPI          2 432009.031250            4 
            2          1024           MPI          3 430467.406250            0 
            2          1024           MPI          4 438119.867188            1 
            2          1024           MPI          5 437910.027344            0 
            2          1024           MPI          6 439786.943359            0 
            2          1024           MPI          7 451211.869141            0 
            2          1024           MPI          8 438796.583984            0 
            2          1024           MPI          9 426859.876953            0 
# Runtime: 15.255884 s (overhead: 0.000092 %) 10 records
