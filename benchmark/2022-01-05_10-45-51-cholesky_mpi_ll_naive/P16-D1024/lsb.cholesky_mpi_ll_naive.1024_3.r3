# Sysname : Linux
# Nodename: eu-a6-003-16
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 01:32:23 2022
# Execution time and date (local): Thu Jan  6 02:32:23 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 440666.730469            0 
            3          1024           MPI          1 435938.103516            5 
            3          1024           MPI          2 432179.597656            1 
            3          1024           MPI          3 430462.628906            0 
            3          1024           MPI          4 438115.949219            1 
            3          1024           MPI          5 437905.029297            0 
            3          1024           MPI          6 439792.121094            0 
            3          1024           MPI          7 451201.324219            0 
            3          1024           MPI          8 438790.765625           10 
            3          1024           MPI          9 426855.603516            0 
# Runtime: 5.236538 s (overhead: 0.000325 %) 10 records
