# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 11:08:51 2022
# Execution time and date (local): Wed Jan  5 12:08:51 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 6263.931641            0 
            3           256           MPI          1 6153.119141            3 
            3           256           MPI          2 6159.759766            0 
            3           256           MPI          3 5788.892578            0 
            3           256           MPI          4 5464.927734            0 
            3           256           MPI          5 5719.255859            0 
            3           256           MPI          6 5928.986328            0 
            3           256           MPI          7 5727.171875            0 
            3           256           MPI          8 6155.251953            0 
            3           256           MPI          9 5988.878906            0 
# Runtime: 6.071233 s (overhead: 0.000049 %) 10 records
