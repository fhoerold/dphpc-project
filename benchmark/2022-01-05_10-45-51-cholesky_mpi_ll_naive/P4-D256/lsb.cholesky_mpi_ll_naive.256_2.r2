# Sysname : Linux
# Nodename: eu-a6-005-13
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 11:08:51 2022
# Execution time and date (local): Wed Jan  5 12:08:51 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 6289.615234            0 
            2           256           MPI          1 6559.941406            4 
            2           256           MPI          2 6157.714844            1 
            2           256           MPI          3 5786.960938            0 
            2           256           MPI          4 5462.814453            0 
            2           256           MPI          5 5717.828125            0 
            2           256           MPI          6 5927.242188            0 
            2           256           MPI          7 5725.564453            0 
            2           256           MPI          8 6153.126953            1 
            2           256           MPI          9 5986.531250            0 
# Runtime: 6.064385 s (overhead: 0.000099 %) 10 records
