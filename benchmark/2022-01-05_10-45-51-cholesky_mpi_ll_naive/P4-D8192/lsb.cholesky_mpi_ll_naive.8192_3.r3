# Sysname : Linux
# Nodename: eu-a6-011-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 14:46:53 2022
# Execution time and date (local): Wed Jan  5 15:46:53 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 1014136296.988281            2 
            3          8192           MPI          1 1009564336.109375            7 
            3          8192           MPI          2 1014707534.263672            4 
            3          8192           MPI          3 1045823948.412109            0 
            3          8192           MPI          4 1074416922.783203            6 
            3          8192           MPI          5 1076434132.582031            0 
            3          8192           MPI          6 1013866572.003906            0 
            3          8192           MPI          7 1007145764.187500            0 
            3          8192           MPI          8 1002268138.285156            7 
            3          8192           MPI          9 1001448624.289062            0 
# Runtime: 12312.560398 s (overhead: 0.000000 %) 10 records
