# Sysname : Linux
# Nodename: eu-a6-011-24
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 14:46:53 2022
# Execution time and date (local): Wed Jan  5 15:46:53 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 1014135867.103516            0 
            2          8192           MPI          1 1009563601.464844            5 
            2          8192           MPI          2 1014706790.867188            5 
            2          8192           MPI          3 1045823171.867188            0 
            2          8192           MPI          4 1074416155.189453            9 
            2          8192           MPI          5 1076433309.443359            4 
            2          8192           MPI          6 1013865841.734375            3 
            2          8192           MPI          7 1007145033.126953            3 
            2          8192           MPI          8 1002267405.199219            7 
            2          8192           MPI          9 1001447860.720703            2 
# Runtime: 12309.561592 s (overhead: 0.000000 %) 10 records
