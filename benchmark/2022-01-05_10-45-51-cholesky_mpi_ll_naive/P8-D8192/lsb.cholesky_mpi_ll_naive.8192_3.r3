# Sysname : Linux
# Nodename: eu-a6-007-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 19:45:52 2022
# Execution time and date (local): Wed Jan  5 20:45:52 2022
# MPI execution on rank 3 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          8192           MPI          0 868116843.914062            2 
            3          8192           MPI          1 869345643.513672            5 
            3          8192           MPI          2 927703703.935547            3 
            3          8192           MPI          3 903119947.949219            0 
            3          8192           MPI          4 901790661.234375            5 
            3          8192           MPI          5 886160907.490234            0 
            3          8192           MPI          6 852542305.326172            0 
            3          8192           MPI          7 879222132.705078            0 
            3          8192           MPI          8 972513459.673828            4 
            3          8192           MPI          9 959901985.664062            0 
# Runtime: 10765.797232 s (overhead: 0.000000 %) 10 records
