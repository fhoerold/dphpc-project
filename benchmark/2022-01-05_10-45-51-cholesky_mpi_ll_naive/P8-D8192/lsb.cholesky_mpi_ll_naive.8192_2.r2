# Sysname : Linux
# Nodename: eu-a6-007-04
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 19:45:51 2022
# Execution time and date (local): Wed Jan  5 20:45:51 2022
# MPI execution on rank 2 with 8 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          8192           MPI          0 868132798.886719            2 
            2          8192           MPI          1 869361318.697266            6 
            2          8192           MPI          2 927720425.603516            5 
            2          8192           MPI          3 903136185.669922            0 
            2          8192           MPI          4 901806913.486328           10 
            2          8192           MPI          5 886176844.896484            0 
            2          8192           MPI          6 852557642.037109            3 
            2          8192           MPI          7 879237923.675781            0 
            2          8192           MPI          8 972531002.005859            6 
            2          8192           MPI          9 959919262.167969            0 
# Runtime: 10766.057096 s (overhead: 0.000000 %) 10 records
