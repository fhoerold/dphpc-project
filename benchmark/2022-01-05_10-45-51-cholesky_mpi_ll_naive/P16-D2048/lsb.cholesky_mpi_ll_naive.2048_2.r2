# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 18:56:17 2022
# Execution time and date (local): Wed Jan  5 19:56:17 2022
# MPI execution on rank 2 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 13966262.761719            0 
            2          2048           MPI          1 13909504.800781            7 
            2          2048           MPI          2 13757866.109375            2 
            2          2048           MPI          3 13717143.263672            0 
            2          2048           MPI          4 13422524.388672            7 
            2          2048           MPI          5 13381530.416016            0 
            2          2048           MPI          6 13844034.111328            0 
            2          2048           MPI          7 13809833.261719            0 
            2          2048           MPI          8 13778028.226562            7 
            2          2048           MPI          9 13766384.652344            0 
# Runtime: 172.421547 s (overhead: 0.000013 %) 10 records
