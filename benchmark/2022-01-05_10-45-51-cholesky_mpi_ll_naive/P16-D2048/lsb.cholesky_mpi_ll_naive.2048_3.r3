# Sysname : Linux
# Nodename: eu-a6-007-03
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 18:56:17 2022
# Execution time and date (local): Wed Jan  5 19:56:17 2022
# MPI execution on rank 3 with 16 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 13967463.560547            0 
            3          2048           MPI          1 13910691.607422            9 
            3          2048           MPI          2 13759040.292969            1 
            3          2048           MPI          3 13718303.597656            0 
            3          2048           MPI          4 13423675.757812            9 
            3          2048           MPI          5 13382668.271484            0 
            3          2048           MPI          6 13845213.839844            0 
            3          2048           MPI          7 13811004.302734            0 
            3          2048           MPI          8 13779198.878906           10 
            3          2048           MPI          9 13767556.492188            0 
# Runtime: 166.559309 s (overhead: 0.000017 %) 10 records
