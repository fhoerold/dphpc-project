# Sysname : Linux
# Nodename: eu-a6-005-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 12:04:59 2022
# Execution time and date (local): Wed Jan  5 13:04:59 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          5793           MPI          0 220506289.277344            0 
            3          5793           MPI          1 219958648.714844            3 
            3          5793           MPI          2 219927241.609375            6 
            3          5793           MPI          3 218942591.986328            2 
            3          5793           MPI          4 218953286.003906            6 
            3          5793           MPI          5 218757676.644531            0 
            3          5793           MPI          6 219238410.652344            0 
            3          5793           MPI          7 220740575.798828            0 
            3          5793           MPI          8 220443202.222656            6 
            3          5793           MPI          9 218801494.369141            0 
# Runtime: 2637.109767 s (overhead: 0.000001 %) 10 records
