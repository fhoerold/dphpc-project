# Sysname : Linux
# Nodename: eu-a6-005-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Wed Jan  5 12:04:59 2022
# Execution time and date (local): Wed Jan  5 13:04:59 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          5793           MPI          0 220506314.710938            0 
            2          5793           MPI          1 219958428.787109            3 
            2          5793           MPI          2 219927022.457031            4 
            2          5793           MPI          3 218942379.779297            0 
            2          5793           MPI          4 218953081.207031            5 
            2          5793           MPI          5 218757458.523438            0 
            2          5793           MPI          6 219238178.779297            0 
            2          5793           MPI          7 220740357.683594            0 
            2          5793           MPI          8 220443008.162109            4 
            2          5793           MPI          9 218801284.898438            0 
# Runtime: 2638.141616 s (overhead: 0.000001 %) 10 records
