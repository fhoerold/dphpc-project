# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:19:28 2022
# Execution time and date (local): Fri Jan  7 18:19:28 2022
# MPI execution on rank 2 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 5220.699219            0 
            2           128           MPI          1 5074.511719            2 
            2           128           MPI          2 5040.132812            2 
            2           128           MPI          3 4749.687500            0 
            2           128           MPI          4 4723.636719            1 
            2           128           MPI          5 5435.271484            0 
            2           128           MPI          6 6232.521484            0 
            2           128           MPI          7 6185.675781            0 
            2           128           MPI          8 4971.181641            2 
            2           128           MPI          9 4680.347656            0 
# Runtime: 6.088966 s (overhead: 0.000115 %) 10 records
