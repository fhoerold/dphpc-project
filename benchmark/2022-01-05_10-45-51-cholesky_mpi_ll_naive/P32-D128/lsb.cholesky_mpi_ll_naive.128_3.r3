# Sysname : Linux
# Nodename: eu-a6-003-17
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Fri Jan  7 17:19:28 2022
# Execution time and date (local): Fri Jan  7 18:19:28 2022
# MPI execution on rank 3 with 32 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 5132.882812            0 
            3           128           MPI          1 5070.892578            1 
            3           128           MPI          2 5040.759766            0 
            3           128           MPI          3 4750.410156            0 
            3           128           MPI          4 4726.835938            0 
            3           128           MPI          5 5436.265625            0 
            3           128           MPI          6 6234.093750            0 
            3           128           MPI          7 6187.378906            0 
            3           128           MPI          8 4967.771484            0 
            3           128           MPI          9 4678.343750            0 
# Runtime: 0.068445 s (overhead: 0.001461 %) 10 records
