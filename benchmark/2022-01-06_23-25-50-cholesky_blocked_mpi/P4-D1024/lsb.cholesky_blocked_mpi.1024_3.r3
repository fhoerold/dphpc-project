# Sysname : Linux
# Nodename: eu-a6-009-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 22:28:21 2022
# Execution time and date (local): Thu Jan  6 23:28:21 2022
# MPI execution on rank 3 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 68155.175781            0 
            3          1024           MPI          1 66679.751953            1 
            3          1024           MPI          2 66995.601562            1 
            3          1024           MPI          3 70361.880859            0 
            3          1024           MPI          4 80740.906250            2 
# Runtime: 0.509352 s (overhead: 0.000785 %) 5 records
