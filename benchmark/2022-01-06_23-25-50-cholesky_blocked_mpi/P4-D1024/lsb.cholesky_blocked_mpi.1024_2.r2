# Sysname : Linux
# Nodename: eu-a6-009-09
# Release : 3.10.0-1160.31.1.el7.x86_64
# Version : #1 SMP Thu Jun 10 13:32:12 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Thu Jan  6 22:28:21 2022
# Execution time and date (local): Thu Jan  6 23:28:21 2022
# MPI execution on rank 2 with 4 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 68154.746094            0 
            2          1024           MPI          1 66678.304688            1 
            2          1024           MPI          2 66657.453125            1 
            2          1024           MPI          3 70020.939453            0 
            2          1024           MPI          4 80713.207031            1 
# Runtime: 2.509975 s (overhead: 0.000120 %) 5 records
