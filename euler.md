# Euler Guides

## Euler Setup

Do this the first time you run benchmarks:

### Load new software stack

* Always load modules first 

```
env2lmod
module load gcc/8.2.0 openmpi/4.0.2 cmake/3.20.3
```

* (optional) to permanentely set the default software stack to NEW, use

```
set_software_stack.sh new
```

### Installing libscibench (locally) in a cluster environment
* Install the library locally in your home folder:
```
git clone https://github.com/spcl/liblsb.git
cd liblsb
./configure --prefix=$HOME/.local
make
make install
```


### Setup Script for Path variables

Do this every time you run benchmarks

Small script to run on login 
* loads modules (see compilation)
* sets search directories

source with path to installation of libscibench as first parameter
```
source configure_euler.sh $HOME/.local
```

This script basically just sets the ENV variables as follows:

* Make sure prerequisites libscibench (see below) and MPI are installed.
* Add local paths to environment variables to load libscibench:
```
export LD_LIBRARY_PATH=$HOME/.local/lib:$LD_LIBRARY_PATH
export LIBRARY_PATH=$HOME/.local/lib:$LIBRARY_PATH
export CPATH=$HOME/.local/include:$CPATH
```

### Build sources

* Don't forget to specify mpicc as the compiler when running CMake:
```
mkdir build && cd build
cmake -DCMAKE_C_COMPILER=mpicc -DCMAKE_BUILD_TYPE=RELEASE ..
make
```

## Run benchmark script

Benchmark scripts are located in the cholesky / gram-schmidt subfolders.

* Make sure to point the script to the dphpc-project/benchmark folder. This will make things easier when running the pipeline


Example 1: benchmarking binary cholesky_openmp_simd_ll_naive with openmp

* run this from the dphpc project root folder 

```
./cholesky/benchmark.sh -o ./benchmark -d 10 11 12 13 -p 2 4 8 16 18 ./gramschmidt/build/parallel_gramschmidt
```


Example 2: benchmarking binary cholesky_openmp_simd_ll_naive with openmp

* run this from the dphpc project root folder 

```
./cholesky/benchmark.sh -o ./benchmark --openmp -d 10 11 12 13 -p 2 4 8 16 18 ./cholesky/build/cholesky_openmp_simd_ll_naive
```


For help, run 
```
./cholesky/benchmark.sh -h
```


## Cholesky (deprecated)

See the [benchmark script for euler](cholesky/run_benchmarks_euler.sh)

```
cd $HOME/projects/dphpc-project/cholesky/benchmarks/euler
$HOME/projects/dphpc-project/cholesky/run_mpi_benchmarks_euler.sh $HOME/projects/dphpc-project/cholesky/build/cholesky_mpi
```

## Varia
### Specify CPU when running job on euler
```
bsub -n 36 -R "select[model == XeonGold_6150]" "mpirun $CH_BUILD/cholesky_mpi_ll_optimised -d $((2**14)) -b"
```
Note this will also put you in a high priority queue (hpc.4h)

### Running valgrind
First load valgrind (different gcc version required)
```
module load gcc/6.3.0 valgrind/3.13.0
```
Then can use valgrind in jobs.

### Using Gitlab pubkey

Create new ssh key pair

```
ssh-keygen -t ed25519 -f $HOME/.ssh/id_ed25519_gitlab
```

Copy public key to your gitlab account (Profile->Preferences->SSH Keys). Public key is stored in 

```
cat $HOME/.ssh/id_ed25519_gitlab.pub
```

### ssh config for gitlab

Open ssh config
```
vi $HOME/.ssh/config
```
Add config for gitlab
```
Host gitlab.ethz.ch
    PreferredAuthentications publickey
    IdentityFile ~/.ssh/id_ed25519_gitlab
```

Tesing config should yield your username

```
ssh -T git@gitlab.ethz.ch
```

```
Welcome to GitLab, @sihaefliger!
```

### Working with gitlab repos

Login nodes do not seem to require proxy module

```
mkdir $HOME/projects && cd $HOME/projects
git clone git@gitlab.ethz.ch:fhoerold/dphpc-project.git
chmod -x $HOME/projects/dphpc-project/cholesky/run_benchmarks.sh
````

We must not run anything on the login node, disable default run scripts! 

### Benchmark Sizes for Weak Scaling

|N        |P  |
|---------|---|
|1448     |1  |
|2048     |2  |
|2896     |4  |
|4096     |8  |
|5793     |16 |
|8192     |32 |

### Sizes for cachegrind measurements

2^5, 2^7, 2^9, 2^11

### Bjobs reference

https://scicomp.ethz.ch/wiki/LSF_mini_reference