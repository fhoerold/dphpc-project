#!/bin/bash

## Default variables
CPU_INFO_FILE="CPU_INFO.txt"
CONFIG_INFO_FILE="CONFIG_INFO.txt"

DIM=(5)
PROC=(4)
USE_MPI=false
USE_OPENMP=false
SEQUENTIAL=false
DRYRUN=false
CACHEGRIND=false
LINEAR_DIMS=false
time_requirement='8:00'
mpi_processes_arg=-1
mpi_processes=1

benchmark_flag=-b

CPU="XeonGold_6150"
OUTPUT="./benchmark"

usage="\n\n$(basename "$0") [-o|--output=<path>] [-d|--dim=<list of numbers>] [-p|--processes=<list of numbers>]
[--cpu=<model>] [--fullnode] [--cachegrind] [--sequential] [--no-commit] [-h|--help] sourcefile

Creates a benchmark folder structure and submits the specified <sourcefile> to the euler batch system. A total of processes X dim jobs are submitted.

Arguments:
    -o|--output : specifies the destination folder where all benchmarks are stored (default is $OUTPUT).
    -d|--dim : list of space separated numbers as log2(required dimension), i.e. '-d 1 2 3'. Specifies the dimensionality of the problem set (default is $DIM)
    --linear-dim : interpret input dims linearly without exponentiation
    -p|--processes : list of space separated numbers, i.e. '-p 1 2 3'. Specifies the number of processes to use in MPI (default is $PROC)
    --cpu : specifies the specific cpu model to use for the benchmark (default is $CPU)
    --cachegrind : set flag to run cachegrind on the algorithm
    --sequential : submits the binary as a sequential task to the batch system. Identical with -p 1 (by default, flag is not set)
    --openmp : number of threads to use for openmp
    --mpi : use mpi, must NOT be the last argument, if using together with openmp, can provide number of mpi processes to use
    --no-commit : dry run, does not submit anything to the batch system but still creates the folder structure at -o <path>
    -h|--help : prints this help text
"

INVOCATION="$0 $@" # TODO: Not sure if this is safe
ROOT=$(pwd)

## Functions


get_abs_filename() {
    ###
  # $1 : relative filename
  echo "$(cd "$(dirname "$1")" && pwd)/$(basename "$1")"
}

# Define a timestamp function
timestamp() {
    date +"%Y-%m-%d_%H-%M-%S" # current time
}

exists(){
  hash "$1" >/dev/null 2>&1
}

cpu_info() { # param: output file
    if exists vmic; then # Windows
        wmic cpu list /format:list >> "$1"
    elif exists lscpu; then # Linux
        lscpu >> "$1"
    elif exists sysctl; then # OSX
        sysctl -a | grep machdep.cpu >> "$1"
    else
        echo "CPU information not available (lscpu/wmic/sysctl not installed)" | tee -a "$1"
    fi
}

approve_action() {
    while true ; do
        read -p "$1 [Y/n] " yn
        case $yn in
            [Yy]* ) break;;
            [Nn]* ) echo "Byebye..."; exit;;
            * ) echo "$yn is unknown";
        esac
    done
}

config_info(){

    HUMAN_READABLE=${1:-false }

    $HUMAN_READABLE || echo "Invocation: $INVOCATION"

    printf "Benchmark '%s' for\n" "$(basename "$EXEC")"

    printf "> Dimensions:\t\t"
    for i in "${DIM[@]}"; do
        if $LINEAR_DIMS; then
            printf "%s, " "$i"
        else
            printf "%s, " "$((2**i))"
        fi
    done

    printf "\n> Number of processors:\t"
    printf "%s, " "${PROC[@]}"
    printf "\n"

    printf "> CPU Model:\t\t%s\n" "$CPU"
    printf "> Use MPI:\t\t%s\n" "$USE_MPI"
    printf "> Use OpenMP:\t\t%s\n\n" "$USE_OPENMP"

    $HUMAN_READABLE || echo "Timestamp: $(timestamp)"
}

## Argument parsing
while [[ $# -gt 0 ]]; do
    key="$1"
    case $key in
        -o|--output)
            if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
                OUTPUT=$2
                shift 2
            else
                echo "Error: Argument for $1 is missing" >&2
                echo "$usage"
                exit 1
            fi
            ;;
        -d|--dim)
            if [ -n "$2" ] && [[ "$2" =~ ^[0-9]+$ ]]; then
                DIM=()
                while [ -n "$2" ] && [[ "$2" =~ ^[0-9]+$ ]]; do
                    DIM+=( "$2" )
                    shift # past value
                done
                shift # past argument
            else
                echo "Error: $1 expects one or more space separated values" >&2
                echo "$usage"
                exit 1
            fi
            ;;
        --linear-dim)
            LINEAR_DIMS=true
            shift # past argument
            ;;
        -p|--processes)
            if [ -n "$2" ] && [[ "$2" =~ ^[0-9]+$ ]]; then
                PROC=()
                while [ -n "$2" ] && [[ "$2" =~ ^[0-9]+$ ]]; do
                    PROC+=( "$2" )
                    shift # past value
                done
                shift # past argument
            else
                echo "Error: $1 expects one or more space separated values" >&2
                echo "$usage"
                exit 1
            fi
            ;;
        --cpu)
            if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
                CPU=$2
                shift 2
            else
                echo "Error: Argument for $1 is missing" >&2
                echo "$usage"
                exit 1
            fi
            ;;
        -t|--time)
            if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
                time_requirement=$2
                shift 2
            else
                echo "Error: Argument for $1 is missing" >&2
                echo "$usage"
                exit 1
            fi
            ;;
        --sequential)
            SEQUENTIAL=true
            PROC=(1)
            shift # past argument
            ;;
        --no-commit)
            DRYRUN=true
            shift # past argument
            ;;
        --cachegrind)
            CACHEGRIND=true
            shift # past argument
            ;; 
        --mpi)
            USE_MPI=true
            if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
                mpi_processes_arg=$2
                shift 2
            else
                mpi_processes_arg=-1
                shift
            fi
            ;; 
        --openmp)
            USE_OPENMP=true
            shift # past argument
            ;; 
        -*|--*=) # unsupported flags
            echo "Error: Unsupported flag $1" >&2
            echo "$usage"
            exit 1
            ;;
        *) # sourcefile
            if [ -z "$EXEC" ] && [ -f "$1" ];
                then EXEC="$1";
                shift
            else
                echo "Error: Unexpected positional argument $1" >&2
                echo "$usage"
                exit 1
            fi
            ;;
    esac
done


dryrun=''
if "$DRYRUN"; then
    dryrun=echo
fi

cachegrind=''
if "$CACHEGRIND"; then
    cachegrind="valgrind --tool=cachegrind"
    benchmark_flag=''
fi

mpi=''
if "$USE_MPI"; then
    mpi=mpirun
fi

# ------------------------------------------------------------------------------------

## The binary file that needs to be run

## Make sure we have executable
if [ ! -f "$EXEC" ]; then
    echo "Error: Executable <sourcefile> required" ## TODO: print help here
    echo "$usage"
    exit 1
fi

BINARY_NAME="$(basename "$EXEC")"
BINARY_PATH_REL="$EXEC" # Only use this for stdout
BINARY_PATH_ABS="$(get_abs_filename "$BINARY_PATH_REL")"


# ------------------------------------------------------------------------------------


## Verify Parameters
config_info true
approve_action "Submit $((${#PROC[@]} * ${#DIM[@]})) jobs to batch system?"


# ------------------------------------------------------------------------------------

##Root folder where benchmarks are stored
BENCHMARK_ROOT_REL="$OUTPUT"

## Create folder structure for benchmark results
printf "\nCreate benchmark folder structure in %s ..." "$BENCHMARK_ROOT_REL"
mkdir -p "$BENCHMARK_ROOT_REL"

BENCHMARK_ROOT_ABS="$(get_abs_filename "$BENCHMARK_ROOT_REL")"

# ------------------------------------------------------------------------------------

## Run = one execution of this script
RUN_NAME="$(timestamp)-$BINARY_NAME"
RUN_PATH_REL="$BENCHMARK_ROOT_REL/$RUN_NAME" # Only use this for stdout
mkdir -p "$RUN_PATH_REL"
RUN_PATH_ABS="$(get_abs_filename "$RUN_PATH_REL")"

## Create symlink to benchmark root folder
ln -sfn "$RUN_PATH_ABS" "$BENCHMARK_ROOT_ABS/last"

printf "Done\n"
printf "> Benchmarks will be written to %s\n" "$RUN_PATH_REL"
printf "> This benchmark will also be available at %s/last\n\n" "$BENCHMARK_ROOT_REL"

# ------------------------------------------------------------------------------------

## Snapshot current binary
BINARY_SNAPSHOT_PATH_REL="$RUN_PATH_REL/snapshot"
printf "Snapshotting binaries to %s ... " "$BINARY_SNAPSHOT_PATH_REL"
mkdir "$BINARY_SNAPSHOT_PATH_REL"
BINARY_SNAPSHOT_PATH_ABS="$(get_abs_filename "$BINARY_SNAPSHOT_PATH_REL")"
cp "$BINARY_PATH_ABS" "$BINARY_SNAPSHOT_PATH_ABS"
#BINARY_SNAPSHOT_ABS="$BINARY_SNAPSHOT_PATH_ABS/$BINARY_NAME"
printf "Done\n\n"

# ------------------------------------------------------------------------------------


## Write CPU info about login node (only applies to euler)
cpu_info "$RUN_PATH_ABS/$CPU_INFO_FILE"
config_info >> "$RUN_PATH_ABS/$CONFIG_INFO_FILE"

## Create all jobs + sub folders
for number_of_cores in "${PROC[@]}"; do
    for number_of_dimensions in "${DIM[@]}"; do
        # use linear or non linear dims
        current_dim=$number_of_dimensions
        if $LINEAR_DIMS; then
            current_dim=$number_of_dimensions
        else
            current_dim=$((2**number_of_dimensions))
        fi
# ------------------------------------------------------------------------------------

        EXPERIMENT_NAME="P${number_of_cores}-D${current_dim}"
        EXPERIMENT_PATH_REL="$RUN_PATH_REL/$EXPERIMENT_NAME"
        mkdir "$EXPERIMENT_PATH_REL"
        EXPERIMENT_PATH_ABS="$(get_abs_filename "$EXPERIMENT_PATH_REL")"

# ------------------------------------------------------------------------------------
# Submit to batch system

        cd "$EXPERIMENT_PATH_ABS"

        if ((mpi_processes_arg == -1)); then
            mpi_processes=$number_of_cores
        else
            mpi_processes=$mpi_processes_arg
        fi

        if $SEQUENTIAL; then
            number_of_cores=1
            mpi_processes=1
        fi

        if $USE_OPENMP; then
            # never use more threads than we are requesting cores
            if $USE_MPI; then
                export OMP_NUM_THREADS=$((number_of_cores / mpi_processes))
            else
                export OMP_NUM_THREADS=$number_of_cores
            fi
        fi

        mpirun=''
        if $USE_MPI; then
	    if $USE_OPENMP; then
		mpirun="${mpi} -np ${mpi_processes} --bind-to none"
	    else
		mpirun="${mpi} -np ${mpi_processes}"
	    fi
        fi

        # calculate needed memory, +1000 to be safe
        needed_memory=$((current_dim**2 * 16 * 3 / 1000000 / number_of_cores + 1000))

        # calculate default memory
        default_memory=1024

        memory_arg=''
        if ((needed_memory > default_memory)); then
            memory_arg="-R rusage[mem=${needed_memory}]"
        fi

        echo "> Submit $BINARY_NAME-P${number_of_cores}-D${current_dim} with OMP_NUM_THREADS=${OMP_NUM_THREADS}, MPI_Processes=${mpi_processes}:"

        $dryrun bsub -n $number_of_cores -W $time_requirement -R "select[model == $CPU]" $memory_arg -o "$EXPERIMENT_PATH_ABS/job.log" \
                -J "$BINARY_NAME-P${number_of_cores}-D${current_dim}" \
                "lscpu >> $EXPERIMENT_PATH_ABS/$CPU_INFO_FILE; $mpirun $cachegrind $BINARY_SNAPSHOT_PATH_ABS/$BINARY_NAME -d $current_dim $benchmark_flag"

        cd "$ROOT"
    done
done
