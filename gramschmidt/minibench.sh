min_size=256
max_size=512
increment=256

dir=$(dirname "$0")
bindir=$(dirname "$1")

hasgp=0
if command -v parallel &>/dev/null
then
    echo "Using GNU parallel."
    hasgp=1
else
    echo -e "\n$(tput bold)Consider installing GNU parallel to speed up verification.$(tput sgr0)\n"
fi

for prog in "$@"
do
    progname=$(echo $prog | sed -n 's/^\(.*\/\)*\(.*\)/\2/p')

    iseq=""
    jseq=""
    lsb=""
    for i in $(seq $min_size $increment $max_size)
    do
        if [ $hasgp -eq 1 ]
        then
            for j in $(seq $min_size $increment $i)
            do
                iseq="$iseq $i"
                jseq="$jseq $j"
                lsb="$lsb $bindir/lsb.$progname.${i}x${j}.r0"
            done
        else
            for j in $(seq $min_size $increment $i)
            do
                lsb="$bindir/lsb.$progname.${i}x${j}.r0"
                $prog -m $i -n $j -b &>/dev/null
                printf "$prog %10s: " "(${i}x${j})"
                cat "$lsb" | grep "Runtime"
                rm "$lsb"                
            done
        fi            
    done

    if [ $hasgp -eq 1 ]
    then
        parallel --keep-order --link --use-cores-instead-of-threads \
                 $prog -m {1} -n {2} -b '&>/dev/null' ';' \
                 echo -n "$prog '('{1}x{2}')': " ';' \
                 cat {3} '|' grep "Runtime" ';' \
                 rm {3} \
                 ::: $iseq ::: $jseq ::: $lsb
    fi
done
