#ifndef IO_UTIL_H
#define IO_UTIL_H

void print_array(int m, int n, double *R, double *Q);
void save_array(int m, int n, double *R, double *Q, char *path);
void init_array(int m, int n, double *A, double *R, double *Q);
void handle_args(int argc, char** argv, int *m, int *n, int *dump_array_term, int *dump_array_file, char **data_file, int *do_benchmark, char** exe_name, int *num_runs);

#endif
