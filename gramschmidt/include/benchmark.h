#ifndef BENCHMARK_H
#define BENCHMARK_H

void benchmark_gram(void (*gram)(int, int, double *, double *, double *), int m,
                    int n, double *A, double *R, double *Q, int num_runs,
                    char *exe_name);

void benchmark_gram_mpi(void (*gram)(int, int, double *, double *, double *),
                        int rank, int m, int n, double *A, double *R, double *Q,
                        int num_runs, char *exe_name);

void benchmark_blocking_gram_mpi(void (*fun_main)(int, int, const double *, double *,
                                                  double *, int, int),
                                 int m, int n, double *A, double *Q, double *R,
                                 int rank, int numproc, int num_runs,
                                 char *exe_name);

#endif
