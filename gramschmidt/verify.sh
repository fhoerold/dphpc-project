#!/bin/bash

# min and max matrix test sizes
min_size=128
max_size=256
increment=128  # Use for nonstandard increments

dir=$(dirname "$0")
bindir=$(dirname "$1")

custom_diff () {
    # tolerance for floating-point comparison
    tol="10^(-6)"
    
    plot=""
    show=0

    # read line-by-line from the supplied files
    while read -a r && read -a d <&3
    do
        len=${#r[@]}
        export BC_LINE_LENGTH=$(($len + 3))
        calstr="1"
        for k in ${!r[@]}      
        do
	    a=${r[k]}
	    b=${d[k]}
            calstr="($calstr) * 2 + ((($a - $b) < $tol) && (($b - $a) < $tol))"
        done
        calstr="obase=2;$calstr"
        
        ans=$(echo "$calstr" | bc -l | sed 's/1//1')

        if echo $ans | grep 0 &>/dev/null
        then
            if [ $show -eq 0 ]
            then
                echo " x"
            fi
            for (( c=0; c < ${#ans}; c++ ))
            do
                if [ ${ans:$c:1} == 0 ]
                then
                    echo -e "${d[c]} != ${r[c]} (ref)"
                fi
            done
            show=1
        fi
        
        plot="$plot$ans\n"        
    done < $1 3< $2

    # show the spy-plot in case there where any errors
    if [ $show -eq 1 ]
    then
	# show succesful compares as o
	# show failed compares as x
	echo -e "$plot" | sed 's/1/o/g' | sed 's/0/x/g'
    else
	echo " ✓"
    fi
}


hasgp=0
if command -v parallel &>/dev/null
then
    echo "Using GNU parallel."
    hasgp=1
else
    echo -e "\n$(tput bold)Consider installing GNU parallel to speed up verification.$(tput sgr0)\n"
fi

# test the supplied programs
for prog in "$@"
do
    # strip path from prog
    progname=$(echo $prog | sed -n 's/^\(.*\/\)*\(.*\)/\2/p')

    iseq=""
    jseq=""
    dat=""
    ref=""    
    # for all valid combinations of i,j
    for i in $(seq $min_size $increment $max_size)
    do
        if [ $hasgp -eq 1 ]
        then
            # precalculate dimensions and filenames
            for j in $(seq $min_size $increment $i)
            do
                iseq="$iseq $i"
                jseq="$jseq $j"
                dat="$dat /tmp/${progname}_${i}_${j}.dat"
	        ref="$ref /tmp/gramschmidt_ref_${i}_${j}.dat"
            done
        else
	    for j in $(seq $min_size $increment $i)
	    do
	        # generate datafile name
	        dat="/tmp/${progname}_${j}_${i}.dat"
	        # generate reference name
	        ref="/tmp/gramschmidt_ref_${j}_${i}.dat"
	        # run reference and program
	        echo -n "Running $progname ${j}x${i}"
	        $bindir/gramschmidt -m $i -n $j -s $ref &>/dev/null
	        $prog -m $i -n $j -s $dat &>/dev/null
	        # compare program with reference implementation
                echo -ne "\rVerifying $progname ${i}x${j}"
	        custom_diff $dat $ref
	        # remove temporary data files
	        rm $ref $dat
	    done
        fi
    done

    if [ $hasgp -eq 1 ]
    then
        export -f custom_diff
        parallel --keep-order --link \
                 echo -n "Verified $progname {1}x{2}" ';' \
                 $bindir/gramschmidt -m {1} -n {2} -s {4} '&>/dev/null' ';' \
                 $prog -m {1} -n {2} -s {3} '&>/dev/null' ';' \
                 custom_diff {4} {3} ';' \
                 rm {4} {3} \
                 ::: $iseq ::: $jseq ::: $dat ::: $ref
    fi
done
