#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <liblsb.h>
#include <mpi.h>

#include "io_util.h"
#include "benchmark.h"

#define NUM_RUNS 10
#define SIGMA 0.5  // Should only cause a single recompute.

/**
 * Algo 2.3 from Iterated Classical Gram-Schmidt (ICGS)
 * https://onlinelibrary.wiley.com/doi/epdf/10.1002/1099-1506(200005)7%3A4%3C219%3A%3AAID-NLA196%3E3.0.CO%3B2-L
 */
static void icgs(int m, int n, double *A, double *R, double *Q) {
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int width, idx;
    if( rank == size-1 ) {
	width = m / size + m % size;
    } else {
	width = m / size;
    }
    
    double *d = (double *) malloc (n * sizeof(double));
    double *e = (double *) malloc (n * sizeof(double));
    double *w = (double *) malloc (width * sizeof(double));
    double norm[4];

    for (int i = 0; i < n; ++i) {
	// w_i = a_i
	for (int k = 0; k < width; ++k) {
	    w[k] = A[k * n + i];
	}

        do {
	    // ||A_1||(1)
	    norm[0] = 0.0;
	    for (int k = 0; k < width; ++k) {
		norm[0] += w[k] * w[k];
	    }
	
	    // d = Q_i-1^T * w_i
	    for (int j = 0; j < i; ++j) {
		d[j] = 0.0;
		for (int k = 0; k < width; ++k) {
		    d[j] += Q[k * n + j] * w[k];
		}
	    }

	    MPI_Allreduce(d, e, i, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

	    // e = Q_i-1 * d
	    for (int j = 0; j < width; ++j) {
		d[j] = 0.0;
		for (int k = 0; k < i; ++k) {
		    d[j] += Q[j * n + k] * e[k];
		}
	    }
	
	    // w_i = w_i - Q_i-1 * d
	    for (int k = 0; k < width; ++k) {
		w[k] = w[k] - d[k];
	    }

	    // ||A_1||(i)
	    norm[1] = 0.0;
	    for (int k = 0; k < width; ++k) {
		norm[1] += w[k] * w[k];
	    }

            MPI_Allreduce(norm, norm+2, 2, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

	    // ||A_1||(i) <= SIGMA * ||A_1||(1)
        } while (norm[3] <= SIGMA * SIGMA * norm[2]);

	// Q_i = A_i / norm2
	norm[3] = sqrt(norm[3]);
	for (int k = 0; k < width; ++k) {
	    Q[k * n + i] = w[k] / norm[3];
	}
    }
    
    free(d);
    free(e);
    free(w);   

    /*
    // fill in R
    for (int i = 0; i < n; i++) {	
	for (int j = i; j < n; j++) {
	    // sprod = qi*aj
	    double sprod = 0.0;
	    for (int k = 0; k < width; k++) {
		sprod += Q[k * n + i] * A[k * n + j];
	    }
	    R[i * n + j] = sprod;
	}
    }
    */

    for (int i = 0; i < n; ++i) {
        for (int k = 0; k < width; ++k) {
            const double Qki = Q[k * n + i];
            for (int j = i; j < n; ++j) {
                R[i * n + j] += Qki * A[k * n + j];
            }
        }
    }

    if (rank == 0) {
        MPI_Reduce(MPI_IN_PLACE, R, n * n, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    } else {
        MPI_Reduce(R, R, n * n, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    }

    int counts[size];
    int displacements[size];
    if (rank == 0) {
        for (unsigned int i = 0; i < size-1; ++i) {
            counts[i] = width * n;
            displacements[i] = i * width * n;
        }
        counts[size-1] = (width + m % size) * n;
        displacements[size-1] = (size-1) * width * n;     
    }    

    if (rank == 0) {
        MPI_Gatherv(MPI_IN_PLACE, width * n, MPI_DOUBLE,
                    Q, counts, displacements, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    } else {
        MPI_Gatherv(Q, width * n, MPI_DOUBLE, Q, counts, displacements, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    }
}

// Wrapper function to support timing non-root array setup and scatter
static void wrapper_icgs(int m, int n, double *A, double *R, double *Q) {
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // Not necessary, but should be measured.
    MPI_Bcast(&m, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);        
    
    int width, idx;
    if( rank == size-1 ) {
	width = m / size + m % size;
	idx = (size-1) * (m / size);
    } else {
	width = m / size;
	idx = rank * width;
    }

    if (rank != 0) {
        A = (double *) malloc(width * n * sizeof(double));
        R = (double *) malloc(n * n * sizeof(double));
        Q = (double *) malloc(width * n * sizeof(double));
    }    

    MPI_Bcast(R, n * n, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    int counts[size];
    int displacements[size];
    if (rank == 0) {
        for (unsigned int i = 0; i < size-1; ++i) {
            counts[i] = width * n;
            displacements[i] = i * width * n;
        }
        counts[size-1] = (width + m % size) * n;
        displacements[size-1] = (size-1) * width * n;     
    }

    if (rank == 0) {
        MPI_Scatterv (A, counts, displacements, MPI_DOUBLE,
                      MPI_IN_PLACE, width * n, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        MPI_Scatterv (Q, counts, displacements, MPI_DOUBLE,
                      MPI_IN_PLACE, width * n, MPI_DOUBLE, 0, MPI_COMM_WORLD);                
    } else {
        MPI_Scatterv (A, counts, displacements, MPI_DOUBLE, A, width * n, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        MPI_Scatterv (Q, counts, displacements, MPI_DOUBLE, Q, width * n, MPI_DOUBLE, 0, MPI_COMM_WORLD);        
    }

    icgs(m, n, A, R, Q);

    if (rank != 0) {
        free(A);
        free(R);
        free(Q);
    }
}

/**
 * use param -print as first param to enable printing of results to stdout
 *
 * Matrices have dimensions A: mxn, Q: mxn, R: nxn
 */
int main(int argc, char **argv) {
    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);    
    
    int m = 1200;
    int n = 1000;
    int num_runs = NUM_RUNS;
    int dump_array_term = 0;
    int dump_array_file = 0;
    int do_benchmark = 0;

    char *data_file;
    char *exe_name = (char *) malloc(sizeof(char)*50);
    
    if (rank == 0) {
        handle_args(argc, argv, &m, &n,
                    &dump_array_term, &dump_array_file, &data_file,
                    &do_benchmark, &exe_name, &num_runs);
    }

    if (n > m) {
        if (rank == 0) {
            printf("n larger than m, terminating\n");
        }
        return 0;
    }

    MPI_Bcast(&m, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);    
    MPI_Bcast(&num_runs, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&do_benchmark, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dump_array_file, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dump_array_term, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(exe_name, 50, MPI_CHAR, 0, MPI_COMM_WORLD);

    double *A, *R, *Q;
    
    if (rank == 0) {
        A = (double *) malloc(m * n * sizeof(double));
        R = (double *) malloc(n * n * sizeof(double));
        Q = (double *) malloc(m * n * sizeof(double));
    }

    if (rank == 0) {
        init_array(m, n, A, R, Q);
    }

    if(do_benchmark){
        benchmark_gram_mpi(wrapper_icgs, rank, m, n, A, R, Q, num_runs, exe_name);
    }
    else{
        wrapper_icgs(m, n, A, R, Q);
    }

    if (rank == 0 && dump_array_file) {
        printf("dumping to file.\n");
        save_array(m, n, R, Q, data_file);
    }

    if (rank == 0 && dump_array_term) {
        print_array(m, n, R, Q);
    }

    if (rank == 0) {
        free(A);
        free(R);
        free(Q);
    }
    MPI_Finalize();

    return 0;
}
