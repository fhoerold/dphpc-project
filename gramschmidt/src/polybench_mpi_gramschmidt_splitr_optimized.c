/**
 * This version is stamped on May 10, 2016
 *
 * Contact:
 *   Louis-Noel Pouchet <pouchet.ohio-state.edu>
 *   Tomofumi Yuki <tomofumi.yuki.fr>
 *
 * Web address: http://polybench.sourceforge.net
 */
/* gramschmidt.c: this file is part of PolyBench/C */

#include <stdio.h>
#include <string.h>
#include <math.h>

#include <stdlib.h>

#include <liblsb.h>
#include <mpi.h>

#include "io_util.h"
#include "benchmark.h"

#define NUM_RUNS 10

static void partial_gramschmidt(int m, int n, double *A, double *R, double *Q, int start_r_index, int stop_r_index) {
    double* collectorArr = (double *) malloc(n * sizeof(double));
    
    for (int k = 0; k < n; k++) {
        
        collectorArr[k] = 0.0;
        for (int i = 0; i < m; i++) {
            collectorArr[k] = collectorArr[k] + A[i * n + k] * A[i * n + k];
        }

        for (int j = k+1; j < n; j++) {   
            collectorArr[j] = 0.0;
            for (int i = 0; i < m; i++) {
                collectorArr[j] = collectorArr[j] + A[i * n + k] * A[i * n + j];
            }
        }

        MPI_Allreduce(MPI_IN_PLACE, &collectorArr[k], n-k, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        collectorArr[k] = sqrt(collectorArr[k]);

        for(int j = k+1; j < n; j++) {
            collectorArr[j] = collectorArr[j] / collectorArr[k];
        }

        for (int i = 0; i < m; i++) {
            Q[i * n + k] = A[i * n + k] / collectorArr[k];
        }

        if (start_r_index <= k && k < stop_r_index) {
            R[(k - start_r_index) * n + k] = collectorArr[k];
            for (int j = k+1; j < n; j++) {
                R[(k - start_r_index) * n + j] = collectorArr[j];
            }
        }

        for (int j = k+1; j < n; j++) {           
            for (int i = 0; i < m; i++) {
                A[i * n + j] = A[i * n + j] - Q[i * n + k] * collectorArr[j];
            }
        }
    }

    free(collectorArr);
}

static void kernel_gramschmidt(int m, int n, double *A, double *R, double *Q) {  
    int my_rank;
    int world_size;

    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // rank i process contains rows of A and Q from start_index to start_index+height-1
    // rank i process additionally contains rows R from start_index_r to start_index_r+height_r-1
    int height = m / world_size;
    int height_r = n / world_size;
    int start_index = my_rank * height;
    int start_index_r = my_rank * height_r;
    if(my_rank == world_size - 1) height = m / world_size + m % world_size;
    if(my_rank == world_size - 1) height_r = n / world_size + n % world_size;

    double* partial_A = (double *) malloc(height * n * sizeof(double));
    double* partial_Q = (double *) malloc(height * n * sizeof(double));
    double* partial_R = (double *) malloc(height_r * n * sizeof(double));

    // scatter data to processors
    MPI_Scatter(A, (m / world_size)*n, MPI_DOUBLE, partial_A, (m / world_size)*n, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Scatter(Q, (m / world_size)*n, MPI_DOUBLE, partial_Q, (m / world_size)*n, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Scatter(R, (n / world_size)*n, MPI_DOUBLE, partial_R, (n / world_size)*n, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    // send to last process
    if(m % world_size != 0) {
        if(my_rank == 0) MPI_Send(&A[world_size*(m / world_size)*n], (m % world_size)*n, MPI_DOUBLE, world_size - 1, 0, MPI_COMM_WORLD);
        if(my_rank == 0) MPI_Send(&Q[world_size*(m / world_size)*n], (m % world_size)*n, MPI_DOUBLE, world_size - 1, 1, MPI_COMM_WORLD);
        if(my_rank == world_size - 1) MPI_Recv(&partial_A[(m / world_size)*n], (m % world_size)*n, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        if(my_rank == world_size - 1) MPI_Recv(&partial_Q[(m / world_size)*n], (m % world_size)*n, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);      
    }
    if(n % world_size != 0) {
        if(my_rank == 0) MPI_Send(&R[world_size*(n / world_size)*n], (n % world_size)*n, MPI_DOUBLE, world_size - 1, 2, MPI_COMM_WORLD);
        if(my_rank == world_size - 1) MPI_Recv(&partial_R[(n / world_size)*n], (n % world_size)*n, MPI_DOUBLE, 0, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
    
#pragma scop
    partial_gramschmidt(height, n, partial_A, partial_R, partial_Q, start_index_r, start_index_r+height_r);
#pragma endscop

    // collect data to rank 0
    MPI_Gather(partial_A, (m / world_size)*n, MPI_DOUBLE, A, (m / world_size)*n, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Gather(partial_Q, (m / world_size)*n, MPI_DOUBLE, Q, (m / world_size)*n, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Gather(partial_R, (n / world_size)*n, MPI_DOUBLE, R, (n / world_size)*n, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    // receive from last process
    if(m % world_size != 0) {
        if(my_rank == world_size - 1) MPI_Send(&partial_A[(m / world_size)*n], (m % world_size)*n, MPI_DOUBLE, 0, 3, MPI_COMM_WORLD);
        if(my_rank == world_size - 1) MPI_Send(&partial_Q[(m / world_size)*n], (m % world_size)*n, MPI_DOUBLE, 0, 4, MPI_COMM_WORLD);
        if(my_rank == 0) MPI_Recv(&A[world_size*(m / world_size)*n], (m % world_size)*n, MPI_DOUBLE, world_size - 1, 3, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        if(my_rank == 0) MPI_Recv(&Q[world_size*(m / world_size)*n], (m % world_size)*n, MPI_DOUBLE, world_size - 1, 4, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
    if(n % world_size != 0) {
        if(my_rank == world_size - 1) MPI_Send(&partial_R[(n / world_size)*n], (n % world_size)*n, MPI_DOUBLE, 0, 5, MPI_COMM_WORLD);
        if(my_rank == 0) MPI_Recv(&R[world_size*(n / world_size)*n], (n % world_size)*n, MPI_DOUBLE, world_size - 1, 5, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }

    free(partial_A);
    free(partial_Q);
    free(partial_R);
}


int main(int argc, char** argv) {
    MPI_Init(NULL, NULL);
    int m = 1200;
    int n = 1000;
    int num_runs = NUM_RUNS;
    int dump_array_term = 0;
    int dump_array_file = 0;
    int do_benchmark = 0;

    char *data_file;
    char *exe_name = (char *) malloc(sizeof(char)*50);

    handle_args(argc, argv, &m, &n, &dump_array_term, &dump_array_file, &data_file, &do_benchmark, &exe_name, &num_runs);

    int my_rank;

    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    
    double *A, *R, *Q;

    if (my_rank == 0) {
        A = (double *) malloc(m * n * sizeof(double));
        R = (double *) malloc(n * n * sizeof(double));
        Q = (double *) malloc(m * n * sizeof(double));
    
        if (n > m) {
            printf("n larger than m, terminating\n");
            return 0;
        }

        init_array(m, n, A, R, Q);
    }

    if (do_benchmark) {
        benchmark_gram(kernel_gramschmidt, m, n, A, R, Q, num_runs, exe_name);
    } else {
        kernel_gramschmidt (m, n, A, R, Q);
    }

    if (my_rank == 0 && dump_array_file) {
        printf("dumping to file.\n");
        save_array(m, n, R, Q, data_file);
    }

    if (my_rank == 0 && dump_array_term) {
        print_array(m, n, R, Q);
    }

    if (my_rank == 0) {
        free(A);
        free(R);
        free(Q);
    }

    MPI_Finalize();

    return 0;
}
