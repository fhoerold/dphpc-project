/**
 * This version is stamped on May 10, 2016
 *
 * Contact:
 *   Louis-Noel Pouchet <pouchet.ohio-state.edu>
 *   Tomofumi Yuki <tomofumi.yuki.fr>
 *
 * Web address: http://polybench.sourceforge.net
 */
/* gramschmidt.c: this file is part of PolyBench/C */

/**
 * Adapted by Jonas Passweg
 * 
 * Uses openmp to parallelize gramschmidt polybench
 * algorithm. Computes the same computations in the
 * same order for dependent computations up to my
 * knowledge of the polybench algorithm.
 *
 */

#include <stdio.h>
#include <string.h>
#include <math.h>

#include <stdlib.h>

#include <omp.h>
#include <liblsb.h>

#include "io_util.h"
#include "benchmark.h"

#define NUM_RUNS 10

static void kernel_gramschmidt(int m, int n, double *A, double *R, double *Q) {  
    double nrm;

#pragma scop
    
    for (int k = 0; k < n; k++) {
        nrm = 0.0;
		
        int i;
        #pragma omp parallel for reduction (+:nrm)
        for (i = 0; i < m; i++) {
            nrm = nrm + A[i * n + k] * A[i * n + k];
        }
		
        R[k * n + k] = sqrt(nrm);
		
        #pragma omp parallel for private(i) shared(A,Q,R,n,m,k) schedule(static)
        for (i = 0; i < m; i++) {
            Q[i * n + k] = A[i * n + k] / R[k * n + k];
        }
		
        int j;
        double r;
        #pragma omp parallel for private(i,r) shared(A,Q,R,n,m,k) schedule(static)
        for (j = k+1; j < n; j++) {
            r = 0.0;
            for (i = 0; i < m; i++) {
                r = r + Q[i * n + k] * A[i * n + j];
            }
            R[k * n + j] = r;
        }

        #pragma omp parallel for private(i) shared(A,Q,R,n,m,k) schedule(static)
        for (j = k+1; j < n; j++) {
            for (i = 0; i < m; i++) {
                A[i * n + j] = A[i * n + j] - Q[i * n + k] * R[k * n + j];
            }
        }
    }

#pragma endscop

}


int main(int argc, char** argv) {
    MPI_Init(NULL, NULL);
    int m = 1200;
    int n = 1000;
    int num_runs = NUM_RUNS;
    int dump_array_term = 0;
    int dump_array_file = 0;
    int do_benchmark = 0;

    char *data_file;
    char *exe_name = (char *) malloc(sizeof(char)*50);

    handle_args(argc, argv, &m, &n, &dump_array_term, &dump_array_file, &data_file, &do_benchmark, &exe_name, &num_runs);
    
    double *A = (double *) malloc (m * n * sizeof(double));
    double *R = (double *) malloc (n * n * sizeof(double));
    double *Q = (double *) malloc (m * n * sizeof(double));

    if (n > m) {
        printf("n larger than m, terminating\n");
        return 0;
    }
    
    init_array (m, n, A, R, Q);

    if (do_benchmark) {
        benchmark_gram(kernel_gramschmidt, m, n, A, R, Q, num_runs, exe_name);
    } else {
        kernel_gramschmidt (m, n, A, R, Q);
    }

    if (dump_array_file) {
        printf("dumping to file.\n");
        save_array(m, n, R, Q, data_file);
    }
  
    if (dump_array_term) {
        print_array(m, n, R, Q);
    }

    free((void*)A);
    free((void*)R);
    free((void*)Q);
    MPI_Finalize();
    
    return 0;
}
