#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <liblsb.h>
#include <mpi.h>

#include "io_util.h"
#include "benchmark.h"

#define NUM_RUNS 10
#define SIGMA 0.5  // Should only cause a single recompute.

// #define DEBUG

/**
 * Algo 2.3 from Iterated Classical Gram-Schmidt (ICGS)
 * https://onlinelibrary.wiley.com/doi/epdf/10.1002/1099-1506(200005)7%3A4%3C219%3A%3AAID-NLA196%3E3.0.CO%3B2-L
 */
static void icgs(int m, int n, double *A, double *R, double *Q) {
    double *d = (double *) malloc (m * sizeof(double));
    double *e = (double *) malloc (m * sizeof(double));
    double *w = (double *) malloc (m * sizeof(double));
    double norm1, norm2;

#ifdef DEBUG
    int max_reortho = 0;
    int reortho;
#endif
    
    for (int i = 0; i < n; ++i) {
	// w_i = a_i
	for (int k = 0; k < m; ++k) {
	    w[k] = A[k * n + i];
	}

#ifdef DEBUG
        reortho = 0;
#endif        

	do {
	    // ||A_1||(1)
	    norm1 = 0.0;
	    for (int k = 0; k < m; ++k) {
		norm1 += w[k] * w[k];
	    }
	
	    // d = Q_i-1^T * w_i	
	    for (int j = 0; j < i; ++j) {
		d[j] = 0.0;
		for (int k = 0; k < m; ++k) {
		    d[j] += Q[k * n + j] * w[k];
		}
	    }

	    // e = Q_i-1 * d
	    for (int j = 0; j < m; ++j) {
		e[j] = 0.0;
		for (int k = 0; k < i; ++k) {
		    e[j] += Q[j * n + k] * d[k];
		}
	    }
	
	    // w_i = w_i - Q_i-1 * d
	    for (int k = 0; k < m; ++k) {
		w[k] = w[k] - e[k];
	    }

	    // ||A_1||(i)
	    norm2 = 0.0;
	    for (int k = 0; k < m; ++k) {
		norm2 += w[k] * w[k];
	    }

#ifdef DEBUG
            ++reortho;
#endif
            
	    // ||A_1||(i) <= SIGMA * ||A_1||(1)
        } while (norm2 <= SIGMA * SIGMA * norm1);

#ifdef DEBUG
        max_reortho = (reortho > max_reortho) ? reortho : max_reortho;
#endif
        
	// Q_i = A_i / norm2
	norm2 = sqrt(norm2);
	for (int k = 0; k < m; ++k) {
	    Q[k * n + i] = w[k] / norm2;
	}
    }

#ifdef DEBUG
    printf("Reorthogonalized %dx.\n", max_reortho);
#endif    
    
    // fill in R
    for (int i = 0; i < n; i++) {
        for (int j = i; j < n; j++) {
            // sprod = qi*aj
            double sprod = 0.0;
            for (int k = 0; k < m; k++) {
                sprod += Q[k * n + i] * A[k * n + j];
            }
            R[i * n + j] = sprod;
        }
    }

    free(d);
    free(e);
    free(w);
}

/**
 * use param -print as first param to enable printing of results to stdout
 *
 * Matrices have dimensions A: mxn, Q: mxn, R: nxn
 */
int main(int argc, char **argv) {
    MPI_Init(NULL, NULL);
    int m = 1200;
    int n = 1000;
    int num_runs = NUM_RUNS;
    int dump_array_term = 0;
    int dump_array_file = 0;
    int do_benchmark = 0;

    char *data_file;
    char *exe_name = (char *) malloc(sizeof(char)*50);

    handle_args(argc, argv, &m, &n, &dump_array_term, &dump_array_file, &data_file, &do_benchmark, &exe_name, &num_runs);   

    double *A = (double *)malloc(m * n * sizeof(double));
    double *R = (double *)malloc(n * n * sizeof(double));
    double *Q = (double *)malloc(m * n * sizeof(double));

    if (n > m) {
        printf("n larger than m, terminating\n");
        return 0;
    }

    init_array(m, n, A, R, Q);

    if(do_benchmark){
        benchmark_gram(icgs, m, n, A, R, Q, num_runs, exe_name);
    }
    else{
        icgs(m, n, A, R, Q);
    }

    if (dump_array_file) {
        printf("dumping to file.\n");
        save_array(m, n, R, Q, data_file);
    }

    if (dump_array_term) {
        print_array(m, n, R, Q);
    }

    free((void *)A);
    free((void *)R);
    free((void *)Q);
    MPI_Finalize();

    return 0;
}
