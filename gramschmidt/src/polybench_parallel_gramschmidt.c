/**
 * This version is stamped on May 10, 2016
 *
 * Contact:
 *   Louis-Noel Pouchet <pouchet.ohio-state.edu>
 *   Tomofumi Yuki <tomofumi.yuki.fr>
 *
 * Web address: http://polybench.sourceforge.net
 */
/* gramschmidt.c: this file is part of PolyBench/C */

/*
 * Implementation of parallel gram schmidt q-r factorization trying to
 * stay as close as possible to the polybench code.
 * 
 * However, it uses at base gramschmidt.c instead of the polybench code
 * since we want 1d arrays and better print functions.
 * 
 * Additionally, init_array is called by every thread and the A, Q and R
 * is computed by all threads.
 * 
 * Since every iteration of the outer loop is largely dependent on the
 * previous iteration (see depende on A), we try a more fine-grained
 * approach of parallelism using mpi and focusing on trying to minimize
 * the data being sent around as to make sure the algorithm is not too
 * memory-heavy.
 */

#include <stdio.h>
#include <string.h>
#include <math.h>

#include <stdlib.h>

#include <liblsb.h>
#include <mpi.h>

#include "io_util.h"
#include "benchmark.h"

#define NUM_RUNS 10

static void kernel_gramschmidt(int m, int n, double *A, double *R, double *Q) {  
    int my_rank;
    int world_size;

    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    
    double nrm;

    double *temp_R = (double *) malloc (n * sizeof(double));

#pragma scop
    
    for (int k = 0; k < n; k++) {
        nrm = 0.0;
		
        for (int i = 0; i < m; i++) {
            nrm = nrm + A[i * n + k] * A[i * n + k];
        }
		
        R[k * n + k] = sqrt(nrm);
		
        for (int i = 0; i < m; i++) {
            Q[i * n + k] = A[i * n + k] / R[k * n + k];
        }

        
        int compute_range = (n - k - 1) / world_size;
        
        if(compute_range > 2) { // cutoff value (makes also sure, that no 2 nodes work on the same row)
            int start_index = k + 1 + (my_rank * compute_range);
            int stop_index = k + 1 + ((my_rank + 1) * compute_range);
            if(world_size - 1 == my_rank) stop_index = n;

            // normal R computation parralellised
            for (int j = start_index; j < stop_index; j++) {
                R[k * n + j] = 0.0;
                for (int i = 0; i < m; i++) {
                    R[k * n + j] = R[k * n + j] + (Q[i * n + k] * A[i * n + j]);
                }
                temp_R[j] = R[k * n + j];
            }

            // synchronization
            MPI_Allgather(&temp_R[start_index], compute_range, MPI_DOUBLE, &R[k*n + k + 1], compute_range, MPI_DOUBLE, MPI_COMM_WORLD);
            MPI_Bcast(&R[k*n + k + 1 + ((world_size - 1) * compute_range)], n - k + 1 + (world_size * compute_range), MPI_DOUBLE, (world_size - 1), MPI_COMM_WORLD);
            
        } else {

            // normal R computation sequential
            for (int j = k + 1; j < n; j++) {
                R[k * n + j] = 0.0;
                for (int i = 0; i < m; i++) {
                    R[k * n + j] = R[k * n + j] + (Q[i * n + k] * A[i * n + j]);
                }
            }
        }

        // A computation moved outside to put afterwards
        for (int j = k+1; j < n; j++) {
            for (int i = 0; i < m; i++) {
                A[i * n + j] = A[i * n + j] - Q[i * n + k] * R[k * n + j];
            }
        }
    }

#pragma endscop

}


int main(int argc, char** argv) {
    MPI_Init(NULL, NULL);
    int m = 1200;
    int n = 1000;
    int num_runs = NUM_RUNS;
    int dump_array_term = 0;
    int dump_array_file = 0;
    int do_benchmark = 0;

    char *data_file;
    char *exe_name = (char *) malloc(sizeof(char)*50);

    handle_args(argc, argv, &m, &n, &dump_array_term, &dump_array_file, &data_file, &do_benchmark, &exe_name, &num_runs);   
    
    double *A = (double *) malloc (m * n * sizeof(double));
    double *R = (double *) malloc (n * n * sizeof(double));
    double *Q = (double *) malloc (m * n * sizeof(double));

    if (n > m) {
        printf("n larger than m, terminating\n");
        return 0;
    }
    
    init_array (m, n, A, R, Q);

    if (do_benchmark) {
        int my_rank;
        MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);        
        benchmark_gram_mpi(kernel_gramschmidt, my_rank, m, n, A, R, Q, num_runs, exe_name);
    } else {
        kernel_gramschmidt(m, n, A, R, Q);
    }

    if (dump_array_file) {
        printf("dumping to file.\n");
        save_array(m, n, R, Q, data_file);
    }
  
    if (dump_array_term) {
        print_array(m, n, R, Q);
    }

    free((void*)A);
    free((void*)R);
    free((void*)Q);
    MPI_Finalize();
    
    return 0;
}
