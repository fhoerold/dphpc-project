#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <liblsb.h>
#include <mpi.h>

#include "io_util.h"
#include "benchmark.h"

#include <immintrin.h>

#define NUM_RUNS 10
#define SIGMA 0.5  // Should only cause a single recompute.

// #define DEBUG

// #pragma omp parallel for private(i,k) shared (A,j) schedule(static)

/**
 * Algo 2.3 from Iterated Classical Gram-Schmidt (ICGS)
 * https://onlinelibrary.wiley.com/doi/epdf/10.1002/1099-1506(200005)7%3A4%3C219%3A%3AAID-NLA196%3E3.0.CO%3B2-L
 */
static void icgs(int m, int n, double * A, double *R, double *Q) {
    double *d = (double *) aligned_alloc (64, m * sizeof(double));
    double *w = (double *) aligned_alloc (64, m * sizeof(double));        
    double norm1, norm2;

#ifdef DEBUG
    int max_reortho = 0;
    int reortho;
#endif

    double w_k, sum;    
    for (int i = 0; i < n; ++i) {
        // w_i = a_i
        for (int k = 0; k < m; ++k) {
            w[k] = A[k * n + i];
        }

#ifdef DEBUG
        reortho = 0;
#endif        
        
        do {
            // d = Q_i-1^T * w_i
            for (int j = 0; j < i; ++j) {
                d[j] = 0.0;
            }
            
            norm1 = 0.0;
            for (int k = 0; k < m; ++k) {
                w_k = w[k];
                // ||A_1||(1)
                norm1 += w_k * w_k;
                for (int j = 0; j < i-3; j+=4) {
                    d[j] += Q[k * n + j] * w_k;
                    d[j+1] += Q[k * n + (j+1)] * w_k;
                    d[j+2] += Q[k * n + (j+2)] * w_k;
                    d[j+3] += Q[k * n + (j+3)] * w_k;
                }
                for (int j = i - (i % 4); j < i; ++j) {
                    d[j] += Q[k * n + j] * w_k;
                }
            }

            
            norm2 = 0.0;                    
            for (int j = 0; j < m; ++j) {                    
                sum = 0.0;
                for (int k = 0; k < i-3; k+=4) {
                    sum += Q[j * n + k] * d[k]
                        + Q[j * n + (k+1)] * d[k+1]
                        + Q[j * n + (k+2)] * d[k+2]
                        + Q[j * n + (k+3)] * d[k+3];
                }
                for (int k = i - (i % 4); k < i; ++k) {
                    sum += Q[j * n + k] * d[k];
                }
                // w_i = w_i - Q_i-1 * d                    
                w[j] -= sum;
                // ||A_1||(i)                    
                norm2 += w[j] * w[j];
            }
#ifdef DEBUG
            ++reortho;
#endif
            
            // ||A_1||(i) <= SIGMA * ||A_1||(1)
        } while (norm2 <= SIGMA * SIGMA * norm1);

#ifdef DEBUG
        max_reortho = (reortho > max_reortho) ? reortho : max_reortho;
#endif
        
        // Q_i = A_i / norm2
        norm2 = sqrt(norm2);
        for (int k = 0; k < m; ++k) {
            Q[k * n + i] = w[k] / norm2;
        }
    }
    free(w);
    free(d);     

#ifdef DEBUG
    printf("Reorthogonalized %dx.\n", max_reortho);
#endif    

    // fill in R
    for (int k = 0; k < m; ++k) {
        for (int i = 0; i < n; ++i) {
            const double Qki = Q[k * n + i];
            for (int j = i; j < n-3; j+=4) {
                R[i * n + j] += Qki * A[k * n + j];
                R[i * n + (j+1)] += Qki * A[k * n + (j+1)];
                R[i * n + (j+2)] += Qki * A[k * n + (j+2)];
                R[i * n + (j+3)] += Qki * A[k * n + (j+3)];
            }
            for (int j = n - ((n - i) % 4); j < n; ++j) {
                R[i * n + j] += Qki * A[k * n + j];
            }
        }
    }  
}

/**
 * use param -print as first param to enable printing of results to stdout
 *
 * Matrices have dimensions A: mxn, Q: mxn, R: nxn
 */
int main(int argc, char **argv) {
    MPI_Init(NULL, NULL);
    int m = 1200;
    int n = 1000;
    int num_runs = NUM_RUNS;
    int dump_array_term = 0;
    int dump_array_file = 0;
    int do_benchmark = 0;

    char *data_file;
    char *exe_name = (char *) malloc(sizeof(char)*50);

    handle_args(argc, argv, &m, &n, &dump_array_term, &dump_array_file, &data_file, &do_benchmark, &exe_name, &num_runs);   

    double *A = (double *) aligned_alloc(64, m * n * sizeof(double));
    double *R = (double *) aligned_alloc(64, n * n * sizeof(double));
    double *Q = (double *) aligned_alloc(64, m * n * sizeof(double));

    if (n > m) {
        printf("n larger than m, terminating\n");
        return 0;
    }

    init_array(m, n, A, R, Q);
    
    if(do_benchmark){
        benchmark_gram(icgs, m, n, A, R, Q, num_runs, exe_name);
    }
    else{
        icgs(m, n, A, R, Q);
    }

    if (dump_array_file) {
        printf("dumping to file.\n");
        save_array(m, n, R, Q, data_file);
    }

    if (dump_array_term) {
        print_array(m, n, R, Q);
    }

    free((void *)A);
    free((void *)R);
    free((void *)Q);
    MPI_Finalize();

    return 0;
}
