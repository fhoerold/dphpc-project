#include <liblsb.h>
#include <math.h>
#include <mpi.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "benchmark.h"
#include "io_util.h"

#define NUM_RUNS 10
/***************VERIFICATION***************\
 use ./verify.sh 'mpirun -n 2 ../build/blocking_gram_mpi' to test correctness
 for input sizes given in verify script
*/

/**
 * @brief BLocking Modified gramschmidt Algo 2.1 from
 * https://onlinelibrary.wiley.com/doi/epdf/10.1002/1099-1506(200005)7%3A4%3C219%3A%3AAID-NLA196%3E3.0.CO%3B2-L
 *
 * @param A Starting matrix of dimensions mxn
 * @param Q Resulting mxn matrix
 * @param m Number of rows in A
 * @param n Number of columns in A
 */
static void mgs2b_mpi(const double *A, double *Q, int m, int n) {
    double *wi = (double *)malloc(m * sizeof(double));
    for (int i = 0; i < n; i++) {
        // wi = ai
        for (int j = 0; j < m; j++) {
            wi[j] = A[j * n + i];
        }
        for (int j = 0; j < i; j++) {
            double delta = 0;
            // delta = qjT*wi
            for (int k = 0; k < m; k++) {
                delta += Q[k * n + j] * wi[k];
            }
            // wi -= qj*delta
            for (int k = 0; k < m; k++) {
                wi[k] -= delta * Q[k * n + j];
            }
        }
        //||wi||
        double norm = 0;
        for (int j = 0; j < m; j++) {
            norm += wi[j] * wi[j];
        }
        norm = sqrt(norm);
        // qi = wi/||wi||
        for (int j = 0; j < m; j++) {
            Q[j * n + i] = wi[j] / norm;
        }
    }
    free(wi);
}

/**
 * @brief Creates the col rows of the final R matrix that invovle columns from
 * Qk. E.g. process with rank r creates rows [r*col, (r+1)*col-1] of R
 *
 * @param Rk cols x n matrix to hold the partial R computation
 * @param A The original complete input matrix used to compute
 * @param Qk m x cols block of the resulting Q matrix of gramschmidt
 * @param m Number of rows in A
 * @param n Number of columns in A
 * @param cols Number of columns in Q
 * @param offset Number of rows we are offset into the final complete R-matrix.
 * Needed to see how many leading 0s each row in Rk will have (computed via
 * rank*cols typically)
 */
static void create_partial_r(double *Rk, const double *A, const double *Qk,
                             int m, int n, int cols, int offset) {
    for (int i = offset; i < offset + cols; i++) {
        for (int j = i; j < n; j++) {
            // set to scalarproduct(qi, aj)
            double sprod = 0;
            for (int k = 0; k < m; k++) {
                // as Q is mxcols matrix
                sprod += Qk[k * cols + i - offset] * A[k * n + j];
            }
            Rk[(i - offset) * n + j] = sprod;
        }
    }
}

/**
 * @brief MPI Blocking modified gramschmidt Algo 2.5 from
 * https://onlinelibrary.wiley.com/doi/epdf/10.1002/1099-1506(200005)7%3A4%3C219%3A%3AAID-NLA196%3E3.0.CO%3B2-L
 * Computes Qk for each mpi process, distributes it to the processors with
 * higher rank and merges them into Q on process 0. Each process also calls MGS
 * twice for its own block.
 *
 * Each process gets its own copy of the whole A matrix
 *
 * @param m Number of columns in A, Q
 * @param n Number of rows in A, Q
 * @param A Relevant m x n block of the total A matrix
 * @param Q Relevant m x n block of the total Q matrix
 * @param rank Our current MPI rank
 * @param numproc Total number of MPI processes
 */
static void b2gs_mpi(int m, int n, double *A, double *Q, int rank,
                     int numproc) {
    double *delta = (double *)calloc(n * n, sizeof(double));
    // assume Wk=Ak, so we just use Ak as we don't need old values of Ak
    if (rank == 0) {
        // Q0 is just A0
        mgs2b_mpi(A, Q, m, n);
        memcpy(A, Q, m * n * sizeof(double));
        mgs2b_mpi(A, Q, m, n);
        // send Qo to all other processes
        for (int proc = 1; proc < numproc; proc++) {
            MPI_Send(Q, m * n, MPI_DOUBLE, proc, numproc, MPI_COMM_WORLD);
        }
    } else {
        for (int j = 0; j < rank; j++) {
            // alloc and receive previously computed Qs
            double *Qj = malloc(m * n * sizeof(double));
            MPI_Recv(Qj, m * n, MPI_DOUBLE, j, numproc + j, MPI_COMM_WORLD,
                     MPI_STATUS_IGNORE);
            // calculate delta = QjTWk (n x m * m x n)
            for (int i = 0; i < n; i++) {
                for (int k = 0; k < n; k++) {
                    double dval = 0;
                    for (int l = 0; l < m; l++) {
                        dval += Qj[l * n + i] * A[l * n + k];
                    }
                    delta[i * n + k] = dval;
                }
            }
            // calculate Wknew = Wk-Qj*delta (m x n - m x n * n x n)
            for (int i = 0; i < m; i++) {
                for (int k = 0; k < n; k++) {
                    double diff = 0;
                    for (int l = 0; l < n; l++) {
                        diff += Qj[i * n + l] * delta[l * n + k];
                    }
                    A[i * n + k] -= diff;
                }
            }
            free(Qj);  // won't need old Q blocks anymore
        }
        // sets current Qk (Q) MGS(MGS(A))
        mgs2b_mpi(A, Q, m, n);
        memcpy(A, Q, m * n * sizeof(double));
        mgs2b_mpi(A, Q, m, n);
        // send Qk to all other processors still needing it and to proc0
        for (int proc = rank + 1; proc < numproc; proc++) {
            MPI_Send(Q, m * n, MPI_DOUBLE, proc, numproc + rank,
                     MPI_COMM_WORLD);
        }
        MPI_Send(Q, m * n, MPI_DOUBLE, 0, numproc + rank, MPI_COMM_WORLD);
    }
    free(delta);
}

/**
 * @brief Wrapper function for MPI blocking modified gramschmidt. Handles
 * initial data splitting and sending to the respective processes -> benchmark
 * measures MPI data movement too. On rank 0 process, takes original initialized
 * A, Q and R matrices, other processes take initialized A matrix
 *
 * @param m Number of rows in A, Q
 * @param n Number of columns in A, Q, R, Number of rows in R
 * @param A Original m x n input matrix
 * @param Q Resulting m x n Q matrix, 0-initialized
 * @param R Resulting n x n R matrix, 0-initialized
 * @param rank Current MPI process rank
 * @param numproc Total number of MPI processes
 */
void b2gs_wrapper(int m, int n, const double *A, double *Q, double *R, int rank,
                  int numproc) {
    if (rank == 0) {
        // define block-width
        int cols = n / numproc;
        // create splitted A matrices and send to processes, where each block Ai
        // is defined such that A=(A0|A1|...|Acols-1)
        double *A0 = malloc(m * cols * sizeof(double));
        double *R0 = calloc(n * cols, sizeof(double));
        double *Q0 = calloc(m * cols, sizeof(double));
        for (int j = 0; j < m; j++) {
            memcpy(A0 + j * cols, A + j * n, cols * sizeof(double));
        }
        // call blocking mgs
        b2gs_mpi(m, cols, A0, Q0, rank, numproc);
        free(A0);
        // create R-block
        create_partial_r(R0, A, Q0, m, n, cols, rank * cols);
        // collect all blocks of Q and put them together
        for (int j = 0; j < m; j++) {
            memcpy(Q + j * n, Q0 + j * cols, cols * sizeof(double));
        }
        free(Q0);
        for (int i = 1; i < numproc; i++) {
            double *Qj = malloc(m * cols * sizeof(double));
            MPI_Recv(Qj, m * cols, MPI_DOUBLE, i, numproc + i, MPI_COMM_WORLD,
                     MPI_STATUS_IGNORE);
            for (int j = 0; j < m; j++) {
                memcpy(Q + j * n + i * cols, Qj + j * cols,
                       cols * sizeof(double));
            }
            free(Qj);
        }
        // collect all blocks of R and put them together
        // here each block of R resembles col rows, NOT columns, e.g. a block
        // here is cols x n, NOT m x cols
        for (int j = 0; j < cols; j++) {
            // copy jth row of R0 to R
            memcpy(R + j * n, R0 + j * n, n * sizeof(double));
        }
        free(R0);
        for (int i = 1; i < numproc; i++) {
            double *Rj = malloc(n * cols * sizeof(double));
            MPI_Recv(Rj, n * cols, MPI_DOUBLE, i, 2 * numproc + i,
                     MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            for (int j = 0; j < cols; j++) {
                memcpy(R + i * n * cols + j * n, Rj + j * n,
                       n * sizeof(double));
            }
            free(Rj);
        }
    } else {
        int cols = n / numproc;
        double *Anew = malloc(m * cols * sizeof(double));
        double *Qnew = calloc(m * cols, sizeof(double));
        double *Rnew = calloc(m * cols, sizeof(double));
        // define and fill our block of A
        for (int j = 0; j < m; j++) {
            memcpy(Anew + j * cols, A + j * n + rank * cols,
                   cols * sizeof(double));
        }
        b2gs_mpi(m, cols, Anew, Qnew, rank, numproc);
        create_partial_r(Rnew, A, Qnew, m, n, cols, rank * cols);
        // send Rnew to main thread
        MPI_Send(Rnew, n * cols, MPI_DOUBLE, 0, 2 * numproc + rank,
                 MPI_COMM_WORLD);

        free(Anew);
        free(Qnew);
        free(Rnew);
    }
}

/**
 *
 * Matrices have dimensions A: m x n, Q: m x n, R: n x n
 * BLocking gs Requires m>=n && and n%numprocessors==0
 */
int main(int argc, char **argv) {
    MPI_Init(NULL, NULL);
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int m = 1200;
    int n = 1000;
    int num_runs = NUM_RUNS;
    int dump_array_term = 0;
    int dump_array_file = 0;
    int do_benchmark = 0;
    char *data_file;
    char *exe_name = (char *)malloc(sizeof(char) * 50);

    if (rank == 0) {
        handle_args(argc, argv, &m, &n, &dump_array_term, &dump_array_file,
                    &data_file, &do_benchmark, &exe_name, &num_runs);
        if (n % size) {
            printf(
                "Columns not evenly divisible by number of processors. "
                "Exiting\n");
            return 0;
        }
        if (n > m) {
            printf("n larger than m, terminating\n");
            return 0;
        }
    }
    // send global n and m to all
    MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&m, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&num_runs, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(exe_name, 50, MPI_CHAR, 0, MPI_COMM_WORLD);
    MPI_Bcast(&do_benchmark, 1, MPI_INT, 0, MPI_COMM_WORLD);
    double *A = (double *)malloc(m * n * sizeof(double));
    double *R;
    double *Q;
    if (rank == 0) {
        R = (double *)malloc(n * n * sizeof(double));
        Q = (double *)malloc(m * n * sizeof(double));
        init_array(m, n, A, R, Q);
    }
    // share original A with all for later R computation
    MPI_Bcast(A, m * n, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    if (rank == 0) {
        if (do_benchmark) {
            benchmark_blocking_gram_mpi(b2gs_wrapper, m, n, A, Q, R, rank, size,
                                        num_runs, exe_name);
        } else {
            b2gs_wrapper(m, n, A, Q, R, rank, size);
        }

        if (dump_array_file) {
            printf("dumping to file.\n");
            save_array(m, n, R, Q, data_file);
        }

        if (dump_array_term) {
            print_array(m, n, R, Q);
        }
    } else {
        if (do_benchmark) {
            benchmark_blocking_gram_mpi(b2gs_wrapper, m, n, A, NULL, NULL, rank,
                                        size, num_runs, exe_name);
        } else {
            b2gs_wrapper(m, n, A, NULL, NULL, rank, size);
        }
    }
    MPI_Finalize();

    return 0;
}
