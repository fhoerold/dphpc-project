#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "io_util.h"

//#define DEBUG

// 64-bit KISS RNG functions
// KISS returns a random number between 0 and 1
// (local variables to be found in buddhaBrot function)
#define MWC (kiss_t=(kiss_x<<58)+kiss_c, kiss_c=(kiss_x>>6), kiss_x+=kiss_t, kiss_c+=(kiss_x<kiss_t), kiss_x)
#define XSH (kiss_y^=(kiss_y<<13), kiss_y^=(kiss_y>>17), kiss_y^=(kiss_y<<43))
#define CNG (kiss_z=6906969069LL*kiss_z+1234567)
#define KISS (MWC+XSH+CNG)

unsigned long long kiss_x=1234567890987654321ULL,
    kiss_c=123456123456123456ULL,
    kiss_y=362436362436362436ULL,
    kiss_z=1066149217761810ULL,
    kiss_t;

// 2^64
#define MAX64 18446744073709551616.0

void print_array(int m, int n, double *R, double *Q) {
    int i, j;

    fprintf(stderr, "==BEGIN DUMP_ARRAYS==\n");
    fprintf(stderr, "begin dump: %s", "R");
    for (i = 0; i < n; i++) {
	for (j = 0; j < n; j++) {
	    if ((i*n+j) % 20 == 0) fprintf (stderr, "\n");
	    fprintf (stderr, "%0.2lf ", R[i * n + j]);
	}
    }
    fprintf(stderr, "\nend   dump: %s\n", "R");

    fprintf(stderr, "begin dump: %s", "Q");
    for (i = 0; i < m; i++) {
	for (j = 0; j < n; j++) {
	    if ((i*n+j) % 20 == 0) fprintf (stderr, "\n");
	    fprintf (stderr, "%0.2lf ", Q[i * n + j]);
	}
    }
    fprintf(stderr, "\nend   dump: %s\n", "Q");
    fprintf(stderr, "==END   DUMP_ARRAYS==\n");
}

void save_array(int m, int n, double *R, double *Q, char *path) {
    int i, j;

    FILE *file = fopen(path, "w");
    if (file) {
	for (i = 0; i < n; i++) {
	    for (j = 0; j < n; j++) {
		fprintf(file, "%0.10lf ", R[i*n + j]);
	    }
	    fprintf(file, "\n");
	}
	fprintf(file, "\n");

	for (i = 0; i < m; i++) {
	    for (j = 0; j < n; j++) {
		fprintf(file, "%0.10lf ", Q[i*n + j]);
	    }
	    fprintf(file, "\n");
	}
	fclose(file);
    } else {
        printf("Couldn't open file");
    }
}

void init_array(int m, int n, double *A, double *R, double *Q) {    
    for (int i = 0; i < m; i++) {
	for (int j = 0; j < n; j++) {
	    A[i * n + j] = KISS / MAX64 * 100.0; // Random doubles in range 0..100
	    Q[i * n + j] = 0.0;
#ifdef DEBUG
	    printf("%f ", A[i * n + j]);
#endif
	}
#ifdef DEBUG
	printf("\n");
#endif
    }
    for (int i = 0; i < n; i++) {
	for (int j = 0; j < n; j++) {
	    R[i * n + j] = 0.0;
	}
    }
}

void handle_args(int argc, char** argv, int *m, int *n, int *dump_array_term, int *dump_array_file, char **data_file, int *do_benchmark, char**exe_name, int *num_runs) {
    char *pexe = argv[0] + strlen(argv[0]);
    for (; pexe > argv[0]; pexe--) {
        if ((*pexe == '\\') || (*pexe == '/')) {
            pexe++;
            break;
        }
    }
    strncpy(*exe_name, pexe, 49);

    int opt;
    while ((opt = getopt(argc, argv, "m:n:d:ps:br:")) != -1) {
        switch (opt) {
            case 'm': {
                *m = atoi(optarg);
                break;
            }
            case 'n': {
                *n = atoi(optarg);
                break;
            }
            case 'd': {
                *m = atoi(optarg);
                *n = *m;
                break;
            }
            case 'p': {
                *dump_array_term = 1;
                break;
            }
            case 's': {
                *data_file = optarg;
                *dump_array_file = 1;
                break;
            }
            case 'b': {
                *do_benchmark = 1;
                break;
            }
            case 'r': {
                *num_runs = atoi(optarg);
                break;
            }
        }
    }
}
