#include "benchmark.h"

#include <liblsb.h>
#include <mpi.h>
#include <stdio.h>
#include <string.h>

#define WARMUP_RUNS 3

void benchmark_gram(void (*gram)(int, int, double *, double *, double *), int m,
                    int n, double *A, double *R, double *Q, int num_runs,
                    char *exe_name) {
    char bench_name[50];
    char size_str[14];
    snprintf(size_str, 14, "%dx%d", m, n);

    strncpy(bench_name, exe_name, 50);
    strncat(bench_name, ".", sizeof(bench_name) - strlen(bench_name) - 1);
    strncat(bench_name, size_str, sizeof(bench_name) - strlen(bench_name) - 1);

    LSB_Init(bench_name, 0);
    // save information about benchmark
    LSB_Set_Rparam_int("size_m", m);
    LSB_Set_Rparam_int("size_n", n);
    LSB_Set_Rparam_string("region", "gramschmidt");

    for (int k = 0; k < num_runs; k++) {
        // reset measurement
        LSB_Res();

        gram(m, n, A, R, Q);

        LSB_Rec(k);
    }

    LSB_Finalize();
}

void benchmark_gram_mpi(void (*gram)(int, int, double *, double *, double *),
                        int rank, int m, int n, double *A, double *R, double *Q,
                        int num_runs, char *exe_name) {
    double win;
    double err;

    char bench_name[50];
    char size_str[14];
    snprintf(size_str, 14, "%dx%d", m, n);
    char rank_str[4];
    snprintf(rank_str, 4, "%d", rank);

    strncpy(bench_name, exe_name, 50);
    strncat(bench_name, ".", sizeof(bench_name) - strlen(bench_name) - 1);
    strncat(bench_name, size_str, sizeof(bench_name) - strlen(bench_name) - 1);
    strncat(bench_name, "_", sizeof(bench_name) - strlen(bench_name) - 1);
    strncat(bench_name, rank_str, sizeof(bench_name) - strlen(bench_name) - 1);

    LSB_Init(bench_name, 0);
    // save information about benchmark
    LSB_Set_Rparam_int("rank", rank);
    LSB_Set_Rparam_int("size_m", m);
    LSB_Set_Rparam_int("size_n", n);
    LSB_Set_Rparam_string("region", "gramschmidt");
    LSB_Set_Rparam_string("type", "WARMUP");

    // warmup
    for (int k = 0; k < WARMUP_RUNS; k++) {
        gram(m, n, A, R, Q);
    }

    LSB_Set_Rparam_string("type", "MPI");
    for (int k = 0; k < num_runs; k++) {
        MPI_Barrier(MPI_COMM_WORLD);

        LSB_Res();

        gram(m, n, A, R, Q);

        LSB_Rec(k);
    }

    LSB_Finalize();
}

void benchmark_blocking_gram_mpi(void (*b2gs)(int, int, const double *, double *,
                                              double *, int, int),
                                 int m, int n, double *A, double *Q, double *R,
                                 int rank, int numproc, int num_runs,
                                 char *exe_name) {
    double win;
    double err;
    char bench_name[50];
    char size_str[14];
    snprintf(size_str, 14, "%dx%d", m, n);
    char rank_str[4];
    snprintf(rank_str, 4, "%d", rank);

    strncpy(bench_name, exe_name, 50);
    strncat(bench_name, ".", sizeof(bench_name) - strlen(bench_name) - 1);
    strncat(bench_name, size_str, sizeof(bench_name) - strlen(bench_name) - 1);
    strncat(bench_name, "_", sizeof(bench_name) - strlen(bench_name) - 1);
    strncat(bench_name, rank_str, sizeof(bench_name) - strlen(bench_name) - 1);

    LSB_Init(bench_name, 0);
    // save information about benchmark
    LSB_Set_Rparam_int("rank", rank);
    LSB_Set_Rparam_int("size_m", m);
    LSB_Set_Rparam_int("size_n", n);
    LSB_Set_Rparam_string("region", "gramschmidt");
    LSB_Set_Rparam_string("type", "WARMUP");

    // warmup
    for (int k = 0; k < WARMUP_RUNS; k++) {
        if (rank == 0) {
            b2gs(m, n, A, Q, R, rank, numproc);
        } else {
            b2gs(m, n, A, NULL, NULL, rank, numproc);
        }
    }
    LSB_Set_Rparam_string("type", "MPI");
    for (int k = 0; k < num_runs; k++) {
        // Sync
        MPI_Barrier(MPI_COMM_WORLD);

        // measure time
        LSB_Res();

        if (rank == 0) {
            b2gs(m, n, A, Q, R, rank, numproc);
        } else {
            b2gs(m, n, A, NULL, NULL, rank, numproc);
        }

        LSB_Rec(k);
    }

    LSB_Finalize();
}