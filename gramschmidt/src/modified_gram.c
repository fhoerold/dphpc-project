#include <liblsb.h>
#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "benchmark.h"
#include "io_util.h"

#define NUM_RUNS 10
#define BLOCK_SIZE 4  // p in paper

/**
 * Takes full A, Q (both mxn), have to index with offset to get correct block
 * (mxblock_size) Algo 2.1 from
 * https://onlinelibrary.wiley.com/doi/epdf/10.1002/1099-1506(200005)7%3A4%3C219%3A%3AAID-NLA196%3E3.0.CO%3B2-L
 */
static void mgs2b(double *A, double *Q, int m, int n, int offset) {
    // can probably skip extra malloc for wi as it wont be touched before
    double *wi = (double *)malloc(m * sizeof(double));
    // as we only calculate for the current block, we have BLOCK_SIZE column
    // iters
    for (int i = 0; i < BLOCK_SIZE; i++) {
        // wi = ai (with offset)
        for (int j = 0; j < m; j++) {
            wi[j] = A[j * n + i + offset];
        }
        for (int j = 0; j < i; j++) {
            double delta = 0;
            // delta = qjT*wi (with offset)
            for (int k = 0; k < m; k++) {
                delta += Q[k * n + j + offset] * wi[k];
            }
            // wi -= qj*delta (with offset)
            for (int k = 0; k < m; k++) {
                wi[k] -= delta * Q[k * n + j + offset];
            }
        }
        //||wi||
        double norm = 0;
        for (int j = 0; j < m; j++) {
            norm += wi[j] *
                    wi[j];  // A[j * n + i + offset] * A[j * n + i + offset];
        }
        norm = sqrt(norm);
        // qi = wi/||wi||
        for (int j = 0; j < m; j++) {
            Q[j * n + i + offset] =
                wi[j] / norm;  // A[j * n + i + offset] / norm;
        }
    }
    free(wi);
}

static void create_r(double *R, double *A, double *Q, int m, int n) {
    for (int i = 0; i < n; i++) {
        for (int j = i; j < n; j++) {
            // set to scalarproduct(qi, aj)
            double sprod = 0;
            for (int k = 0; k < m; k++) {
                sprod += Q[k * n + i] * A[k * n + j];
            }
            R[i * n + j] = sprod;
        }
    }
}

/**
 * Algo 2.5 from Block 2 Gram-schmidt
 * https://onlinelibrary.wiley.com/doi/epdf/10.1002/1099-1506(200005)7%3A4%3C219%3A%3AAID-NLA196%3E3.0.CO%3B2-L
 *
 * ensure n%BLOCK_SIZE==0!
 */
static void b2gs(int m, int n, double *A, double *R, double *Q) {
    double *delta = (double *)calloc(BLOCK_SIZE * BLOCK_SIZE, sizeof(double));
    double *A_orig = (double *)malloc(m * n * sizeof(double));
    // currently need original A to fill in R
    memcpy(A_orig, A, m * n * sizeof(double));
    int numblocks = n / BLOCK_SIZE;
    int boffset = 0;
    for (int block = 0; block < numblocks; block++) {
        // assume Wk=Ak, so we just use Ak as we don't need old values of Ak
        // use j to calc offset for Qj
        for (int j = 0; j < block; j++) {
            // calculate delta = QjTWk (block_size x m * m x block_size)
            for (int i = 0; i < BLOCK_SIZE; i++) {
                for (int k = 0; k < BLOCK_SIZE; k++) {
                    double dval = 0;
                    for (int l = 0; l < m; l++) {
                        dval += Q[l * n + i + j * BLOCK_SIZE] *
                                A[l * n + k + boffset];
                    }
                    delta[i * BLOCK_SIZE + k] = dval;
                }
            }
            // calculate Wknew = Wk-Qj*delta (m x block_size - m x block_size *
            // block_size x block_size)
            for (int i = 0; i < m; i++) {
                for (int k = 0; k < BLOCK_SIZE; k++) {
                    double diff = 0;
                    for (int l = 0; l < BLOCK_SIZE; l++) {
                        diff += Q[i * n + l + j * BLOCK_SIZE] *
                                delta[l * BLOCK_SIZE + k];
                    }
                    A[i * n + k + boffset] -= diff;
                }
            }
        }
        // sets Qk
        mgs2b(A, Q, m, n, boffset);
        mgs2b(A, Q, m, n, boffset);
        boffset += BLOCK_SIZE;
    }
    free(delta);
    create_r(R, A_orig, Q, m, n);
    free(A_orig);

}

/**
 * Alg 1. from
 * https://link.springer.com/content/pdf/10.1007/3-540-48311-X_158.pdf
 */
static void paper_mgs(int m, int n, double *A, double *R, double *Q) {
    for (int i = 0; i < n; i++) {
        // qi=ai
        for (int j = 0; j < m; j++) {
            Q[j * n + i] = A[j * n + i];
        }
        for (int j = 0; j < i; j++) {
            // delta = qjT*qi
            double delta = 0;
            for (int k = 0; k < m; k++) {
                delta += Q[k * n + j] * Q[k * n + i];
            }
            // qi -= delta*qj
            for (int k = 0; k < m; k++) {
                Q[k * n + i] -= delta * Q[k * n + j];
            }
        }
        double norm = 0;
        //||qi||
        for (int k = 0; k < m; k++) {
            norm += Q[k * n + i] * Q[k * n + i];
        }
        norm = sqrt(norm);
        // qi/norm
        for (int k = 0; k < m; k++) {
            Q[k * n + i] /= norm;
        }
    }
    // fill in R matrix
    for (int i = 0; i < n; i++) {
        for (int j = i; j < n; j++) {
            // set to scalarproduct(qi, aj)
            double sprod = 0;
            for (int k = 0; k < m; k++) {
                sprod += Q[k * n + i] * A[k * n + j];
            }
            R[i * n + j] = sprod;
        }
    }
}

/**
 * use param -print as first param to enable printing of results to stdout
 *
 * Matrices have dimensions A: mxn, Q: mxn, R: nxn
 */
int main(int argc, char **argv) {
    MPI_Init(NULL, NULL);
    int m = 1200;
    int n = 1000;
    int num_runs = NUM_RUNS;
    int dump_array_term = 0;
    int dump_array_file = 0;
    int do_benchmark = 0;

    char *data_file;
    char *exe_name = (char *) malloc(sizeof(char)*50);

    handle_args(argc, argv, &m, &n, &dump_array_term, &dump_array_file, &data_file, &do_benchmark, &exe_name, &num_runs);   

    double *A = (double *)malloc(m * n * sizeof(double));
    double *R = (double *)malloc(n * n * sizeof(double));
    double *Q = (double *)malloc(m * n * sizeof(double));

    if (n > m) {
        printf("n larger than m, terminating\n");
        return 0;
    }

    init_array(m, n, A, R, Q);

    if (do_benchmark) {
        // benchmark_gram(paper_mgs, m, n, A, R, Q, 5);
        benchmark_gram(b2gs, m, n, A, R, Q, num_runs, exe_name);
    } else {
        // paper_mgs(m, n, A, R, Q);
        // mgs2b(A, Q, m, n, 0);
        b2gs(m, n, A, R, Q);
    }

    if (dump_array_file) {
        printf("dumping to file.\n");
        save_array(m, n, R, Q, data_file);
    }

    if (dump_array_term) {
        print_array(m, n, R, Q);
    }

    free((void *)A);
    free((void *)R);
    free((void *)Q);
    MPI_Finalize();

    return 0;
}
