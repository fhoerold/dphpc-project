#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <liblsb.h>
#include <mpi.h>

#include "io_util.h"
#include "benchmark.h"

#include <immintrin.h>

#define NUM_RUNS 10
#define SIGMA 0.5  // Should only cause a single recompute.

// #define DEBUG

// #pragma omp parallel for private(i,k) shared (A,j) schedule(static)

/**
 * Algo 2.3 from Iterated Classical Gram-Schmidt (ICGS)
 * https://onlinelibrary.wiley.com/doi/epdf/10.1002/1099-1506(200005)7%3A4%3C219%3A%3AAID-NLA196%3E3.0.CO%3B2-L
 */
static void icgs(int m, int n, double *A, double *R, double *Q) {
    const int npad = (n / 4 + 1) * 4;
    const int mpad = (m / 4 + 1) * 4;

    double *Apad = (double *) aligned_alloc (64, m * npad * sizeof(double));
    double *Rpad = (double *) aligned_alloc (64, n * npad * sizeof(double));
    double *Qpad = (double *) aligned_alloc (64, m * npad * sizeof(double));

    for (int i = 0; i < m; ++i) {
        memcpy(Apad + i * npad, A + i * n, n * sizeof(double));
    }
    
    double *d = (double *) aligned_alloc (64, mpad * sizeof(double));
    double *w = (double *) aligned_alloc (64, mpad * sizeof(double));        
    double norm1, norm2;

#ifdef DEBUG
    int max_reortho = 0;
    int reortho;
#endif

    double w_k, sum;    
    for (int i = 0; i < n; ++i) {
        // w_i = a_i
        for (int k = 0; k < m; ++k) {
            w[k] = Apad[k * npad + i];
        }

#ifdef DEBUG
        reortho = 0;
#endif        
        
        do {
            // d = Q_i-1^T * w_i
            for (int j = 0; j < i; ++j) {
                d[j] = 0.0;
            }
            
            norm1 = 0.0;
            for (int k = 0; k < m; ++k) {
                w_k = w[k];
                // ||A_1||(1)
                norm1 += w_k * w_k;                

                __m256d w_k4 = _mm256_set1_pd(w_k);
                __m256d mul, add;
                for (int j = 0; j < i-3; j+=4) {
                    mul = _mm256_mul_pd(w_k4, _mm256_load_pd(Qpad + k * npad + j));
                    add = _mm256_add_pd(mul, _mm256_load_pd(d + j));
                    _mm256_store_pd(d + j, add);
                }
                for (int j = i - (i % 4); j < i; ++j) {
                    d[j] += Qpad[k * npad + j] * w_k;
                }
            }

            
            norm2 = 0.0;                    
            for (int j = 0; j < m; ++j) {
                __m256d sum4 = _mm256_setzero_pd();
                __m256d mul, sub;
                for (int k = 0; k < i-3; k+=4) {
                    mul = _mm256_mul_pd(_mm256_load_pd(Qpad + j * npad + k), _mm256_load_pd(d + k));
                    sum4 = _mm256_add_pd(sum4, mul);
                }
                sum = sum4[0] + sum4[1] + sum4[2] + sum4[3];
                for (int k = i - (i % 4); k < i; ++k) {
                    sum += Qpad[j * npad + k] * d[k];
                }

                // w_i = w_i - Q_i-1 * d
                w[j] -= sum;
                // ||A_1||(i)
                norm2 += w[j] * w[j];
            }
#ifdef DEBUG
            ++reortho;
#endif
            
            // ||A_1||(i) <= SIGMA * ||A_1||(1)
        } while (norm2 <= SIGMA * SIGMA * norm1);

#ifdef DEBUG
        max_reortho = (reortho > max_reortho) ? reortho : max_reortho;
#endif
        
        // Q_i = A_i / norm2
        norm2 = sqrt(norm2);
        for (int k = 0; k < m; ++k) {
            Qpad[k * npad + i] = w[k] / norm2;
        }
    }
    free(w);
    free(d);     

#ifdef DEBUG
    printf("Reorthogonalized %dx.\n", max_reortho);
#endif    

    // fill in R
    double Qki;
    __m256d Qki4, mul, add;
    int start, end;
    for (int k = 0; k < m; ++k) {
        for (int i = 0; i < n; ++i) {
            Qki = Qpad[k * npad + i];            
            Qki4 = _mm256_set1_pd(Qki);
            start = (i / 4 + 1) * 4;
            end = i > n-4 ? n : (n - n % 4);
            
            for (int j = i; j < start; ++j) {
                Rpad[i * npad + j] += Qki * Apad[k * npad + j];
            }
            for (int j = start; j < end; j+=4) {
                mul = _mm256_mul_pd(Qki4, _mm256_load_pd(Apad + k * npad + j));
                add = _mm256_add_pd(mul, _mm256_load_pd(Rpad + i * npad + j));
                _mm256_store_pd(Rpad + i * npad + j, add);
            }
            for (int j = end; j < n; ++j) {
                Rpad[i * npad + j] += Qki * Apad[k * npad + j];
            }
        }
    }

    for (int i = 0; i < n; ++i) {
        memcpy(R + i * n, Rpad + i * npad, n * sizeof(double));
    }
    for (int i = 0; i < m; ++i) {
        memcpy(Q + i * n, Qpad + i * npad, n * sizeof(double));
    }

    free(Apad);
    free(Rpad);
    free(Qpad);
}

/**
 * use param -print as first param to enable printing of results to stdout
 *
 * Matrices have dimensions A: mxn, Q: mxn, R: nxn
 */
int main(int argc, char **argv) {
    MPI_Init(NULL, NULL);
    int m = 1200;
    int n = 1000;
    int num_runs = NUM_RUNS;
    int dump_array_term = 0;
    int dump_array_file = 0;
    int do_benchmark = 0;

    char *data_file;
    char *exe_name = (char *) malloc(sizeof(char)*50);

    handle_args(argc, argv, &m, &n, &dump_array_term, &dump_array_file, &data_file, &do_benchmark, &exe_name, &num_runs);   

    double *A = (double *) aligned_alloc(64, m * n * sizeof(double));
    double *R = (double *) aligned_alloc(64, n * n * sizeof(double));
    double *Q = (double *) aligned_alloc(64, m * n * sizeof(double));

    if (n > m) {
        printf("n larger than m, terminating\n");
        return 0;
    }

    init_array(m, n, A, R, Q);
    
    if(do_benchmark){
        benchmark_gram(icgs, m, n, A, R, Q, num_runs, exe_name);
    }
    else{
        icgs(m, n, A, R, Q);
    }

    if (dump_array_file) {
        printf("dumping to file.\n");
        save_array(m, n, R, Q, data_file);
    }

    if (dump_array_term) {
        print_array(m, n, R, Q);
    }

    free((void *)A);
    free((void *)R);
    free((void *)Q);
    MPI_Finalize();

    return 0;
}
