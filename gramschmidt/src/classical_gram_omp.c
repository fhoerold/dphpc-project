#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <liblsb.h>
#include <omp.h>
#include <mpi.h>

#include "io_util.h"
#include "benchmark.h"

#define NUM_RUNS 10
#define SIGMA 0.5  // Should only cause a single recompute.

/**
 * Algo 2.3 from Iterated Classical Gram-Schmidt (ICGS)
 * https://onlinelibrary.wiley.com/doi/epdf/10.1002/1099-1506(200005)7%3A4%3C219%3A%3AAID-NLA196%3E3.0.CO%3B2-L
 */
static void icgs(int m, int n, double * A, double *R, double *Q) {
    double *d = (double *) malloc (omp_get_max_threads() * n * sizeof(double));
    double *w = (double *) malloc (m * sizeof(double));    
    double norm1, norm2;
    
#pragma omp parallel
    {
        const int tn = omp_get_thread_num();
        const int tc = omp_get_num_threads();

        int width, idx;
        if (tn == tc-1) {
            width = m / tc + m % tc;
            idx = (tc - 1) * (m / tc);
        } else {
            width = m / tc;
            idx = tn * width;
        }

        double sum, lnorm1, lnorm2;
        
        for (int i = 0; i < n; ++i) {
            
            // w_i = a_i
            for (int k = idx; k < idx+width; ++k) {
                w[k] = A[k * n + i];
            }
        
            do {                
                for (int j = 0; j < i; ++j) {
                    d[tn * n + j] = 0.0;
                }

                lnorm1 = 0.0;
                for (int k = idx; k < idx+width; ++k) {
                    lnorm1 += w[k] * w[k];
                    for (int j = 0; j < i; ++j) {                        
                        d[tn * n + j] += Q[k * n + j] * w[k];
                    }
                }

#pragma omp barrier
                
                norm1 = 0.0;
                norm2 = 0.0;                

#pragma omp for
                for (int j = 0; j < i; ++j) {
                    sum = 0.0;
                    for (int t = 0; t < tc; ++t) {
                        sum += d[t * n + j];
                    }
                    d[j] = sum;
                }
                
#pragma omp barrier

                lnorm2 = 0.0;
                for (int j = idx; j < idx+width; ++j) {
                    for (int k = 0; k < i; ++k) {
                        w[j] -= Q[j * n + k] * d[k];
                    }
                    lnorm2 += w[j] * w[j];
                }

#pragma omp atomic
                norm1 += lnorm1;
#pragma omp atomic
                norm2 += lnorm2;
                
#pragma omp barrier
                
                // ||A_1||(i) <= SIGMA * ||A_1||(1)
            } while (norm2 <= SIGMA * SIGMA * norm1);
            
            // Q_i = A_i / norm2
            lnorm2 = sqrt(norm2);
            
#pragma omp for
            for (int k = 0; k < m; ++k) {
                Q[k * n + i] = w[k] / lnorm2;
            }
        }
        
        // fill in R
#pragma omp for  // loop order carefully chosen to allow parallelization and avoid race conditions
        for (int i = 0; i < n; ++i) {        
            for (int k = 0; k < m; ++k) {
                const double Qki = Q[k * n + i];
                for (int j = i; j < n; ++j) {
                    R[i * n + j] += Qki * A[k * n + j];
                }
            }
        }
        
    }  // pragma omp parallel
    
    free(d);
    free(w);    
}

/**
 * use param -print as first param to enable printing of results to stdout
 *
 * Matrices have dimensions A: mxn, Q: mxn, R: nxn
 */
int main(int argc, char **argv) {
    MPI_Init(NULL, NULL);
    int m = 1200;
    int n = 1000;
    int num_runs = NUM_RUNS;
    int dump_array_term = 0;
    int dump_array_file = 0;
    int do_benchmark = 0;

    char *data_file;
    char *exe_name = (char *) malloc(sizeof(char)*50);

    handle_args(argc, argv, &m, &n, &dump_array_term, &dump_array_file, &data_file, &do_benchmark, &exe_name, &num_runs);   

    double *A = (double *)malloc(m * n * sizeof(double));
    double *R = (double *)malloc(n * n * sizeof(double));
    double *Q = (double *)malloc(m * n * sizeof(double));

    if (n > m) {
        printf("n larger than m, terminating\n");
        return 0;
    }

    init_array(m, n, A, R, Q);
    
    if(do_benchmark){
        benchmark_gram(icgs, m, n, A, R, Q, num_runs, exe_name);
    }
    else{
        icgs(m, n, A, R, Q);
    }

    if (dump_array_file) {
        printf("dumping to file.\n");
        save_array(m, n, R, Q, data_file);
    }

    if (dump_array_term) {
        print_array(m, n, R, Q);
    }

    free(A);
    free(R);
    free(Q); 
    free(exe_name);
    MPI_Finalize();

    return 0;
}
