#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * NOTE: EVERYTHING IN (NOW) ROW MAJOR ORDER
 * i.e. A = [a1 a2 a3]
 * 
 * Implements gram schmidt q-r factorization by first computing q and then r
 * Uses the least possible library calls
 * 
 * Not finished:
 *  - linear dependence creates wrong results (try 3 1 2 3 4 5 6 7 8 9)
 *  - free memory
 */ 

#include <stdio.h>
#include <string.h>
#include <math.h>

#include <stdlib.h>

#include <liblsb.h>
#include <mpi.h>

#include "io_util.h"
#include "benchmark.h"

#define NUM_RUNS 10

double inner_product(double* vector_a, double* vector_b, int m, int n) {
    double sum = 0.0;
    for(int i = 0; i < m; i++) {
        sum = sum + vector_a[i*n]*vector_b[i*n];
    }
    return sum;
}

// returns projection vector #vector onto the line spanned by vector #onto
double* projection(double* vector, double* onto, int m, int n) {
    double *res = (double*) calloc(N, sizeof(double));
    double scale = inner_product(onto, vector, m, n) / inner_product(onto, onto, m, n);
    for(int i = 0; i < m; i++) {
        res[i] = onto[i*n] * scale;
    }
    return res;
}

void normalize(double* vector, int m, int n) {
    double sum = inner_product(vector, vector, m, n);
    double norm = sqrt(sum);
    for(int i = 0; i < m; i++) {
        vector[i*n] = vector[i*n] / norm;
    }
}

void compute_q_grams(double* A, double* Q, int m, int n) {
    for(int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            Q[j*n + i] = A[j*n + i];
        }

        for(int j = 0; j < i; j++) {
            double* proj = projection(&A[i], &Q[j], m, n);

            for(int k = 0; k < m; k++) {
                Q[k*n + i] = Q[k*n + i] - proj[k];
            }
        }
    }

    for(int i = 0; i < N; i++) {
        normalize(&Q[i], m, n);
    }
}

void compute_r_grams(double* A, double* Q, double* R, int m, int n) {
    for(int i = 0; i < n; i++) {
        for(int j = i; j < n; j++) {
            R[i*n + j] = inner_product(&Q[i], &A[j], m, n);
        }
    }
}

static void kernel_gramschmidt(int m, int n, double *A, double *R, double *Q) { 
    compute_q_grams(A, Q, m, n);
    compute_r_grams(A, Q, R, m, n);
}

int main(int argc, char *argv[]) {
    MPI_Init(NULL, NULL);
    int m = 1200;
    int n = 1000;
    int num_runs = NUM_RUNS;
    int dump_array_term = 0;
    int dump_array_file = 0;
    int do_benchmark = 0;

    char *data_file;
    char *exe_name = (char *) malloc(sizeof(char)*50);

    handle_args(argc, argv, &m, &n, &dump_array_term, &dump_array_file, &data_file, &do_benchmark, &exe_name, &num_runs);   
    
    double *A = (double *) malloc (m * n * sizeof(double));
    double *R = (double *) malloc (n * n * sizeof(double));
    double *Q = (double *) malloc (m * n * sizeof(double));

    if (n > m) {
        printf("n larger than m, terminating\n");
        return 0;
    }
    
    init_array (m, n, A, R, Q);

    if (do_benchmark) {
        int my_rank;
        MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);        
        benchmark_gram_mpi(kernel_gramschmidt, my_rank, m, n, A, R, Q, num_runs, exe_name);
    } else {
        kernel_gramschmidt(m, n, A, R, Q);
    }

    if (dump_array_file) {
        printf("dumping to file.\n");
        save_array(m, n, R, Q, data_file);
    }
  
    if (dump_array_term) {
        print_array(m, n, R, Q);
    }

    free((void*)A);
    free((void*)R);
    free((void*)Q);
    MPI_Finalize();
    
    return 0;
}