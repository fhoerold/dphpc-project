#ifndef IO_UTIL_H
#define IO_UTIL_H

void print_array(int n, double *A);
void save_array(int n, double *A, char *path);
void save_array_c_major(int n, double *A, char *path);
void init_array(int n, double *A);

void handle_args(int argc, char** argv, int *n, int *dump_array_term, int *dump_array_file, char **data_file, int *do_benchmark, char**exe_name, int *num_runs, int *block_size);

#endif