#ifndef BENCHMARK_H
#define BENCHMARK_H

void benchmark_fun(void (*fun)(int, double *), int n, double *A, int num_runs, char *exe_name);
void benchmark_fun_mpi(void (*fun_main)(int, double *), void (*fun_other)(int), int myrank, int n, double *A, int num_runs, char *exe_name, int blocksize);

#endif