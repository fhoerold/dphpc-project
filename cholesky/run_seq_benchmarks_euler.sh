#!/bin/bash

for i in {5..13};
do 
	bsub -n 1 -o "$(pwd)/logs.txt" "$1 -d $((2**i)) -b"
done
