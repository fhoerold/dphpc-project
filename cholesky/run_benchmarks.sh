#!/bin/bash

for i in {5..11};
do 
    mpiexec -np 12 "$1" -d $((2**i)) -b;
done