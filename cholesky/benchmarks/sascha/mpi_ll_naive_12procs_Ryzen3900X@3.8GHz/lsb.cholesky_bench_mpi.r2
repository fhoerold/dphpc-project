# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov  9 13:38:39 2021
# Execution time and date (local): Tue Nov  9 14:38:39 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 217.954115            0 
            2            32           MPI          1 12279.255028            1 
            2            32           MPI          2 30468.144331            1 
            2            32           MPI          3 39973.491533            0 
            2            32           MPI          4 53986.805172            2 
            2            32           MPI          5 49601.856083            0 
            2            32           MPI          6 60594.163593            0 
            2            32           MPI          7 75736.009981            0 
            2            32           MPI          8 55157.609837            3 
            2            32           MPI          9 59786.433783            0 
# Runtime: 0.516137 s (overhead: 0.001356 %) 10 records
