# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov  9 13:38:39 2021
# Execution time and date (local): Tue Nov  9 14:38:39 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 220.284599            0 
            3            32           MPI          1 12280.771330            1 
            3            32           MPI          2 30468.449646            1 
            3            32           MPI          3 39970.765245            0 
            3            32           MPI          4 56280.034899            1 
            3            32           MPI          5 49437.863304            0 
            3            32           MPI          6 60594.043266            0 
            3            32           MPI          7 73224.350155            0 
            3            32           MPI          8 60774.870621            2 
            3            32           MPI          9 59587.703629            0 
# Runtime: 6.503127 s (overhead: 0.000077 %) 10 records
