# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 11:36:36 2021
# Execution time and date (local): Tue Nov 23 12:36:36 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 377988.512873            0 
            2           512           MPI          1 271106.145933            2 
            2           512           MPI          2 428275.941028            2 
            2           512           MPI          3 292749.596031            0 
            2           512           MPI          4 270445.006180            1 
            2           512           MPI          5 416229.056592            0 
            2           512           MPI          6 394658.450958            0 
            2           512           MPI          7 322057.993593            0 
            2           512           MPI          8 408275.191085            2 
            2           512           MPI          9 522878.101264            0 
# Runtime: 9.568513 s (overhead: 0.000073 %) 10 records
