# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 11:35:35 2021
# Execution time and date (local): Tue Nov 23 12:35:35 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 83666.860584            0 
            3            32           MPI          1 62156.532135            2 
            3            32           MPI          2 54499.662228            1 
            3            32           MPI          3 39118.822408            0 
            3            32           MPI          4 47304.827919            3 
            3            32           MPI          5 52938.348475            0 
            3            32           MPI          6 52102.150734            0 
            3            32           MPI          7 59686.421048            0 
            3            32           MPI          8 63388.780388            2 
            3            32           MPI          9 40413.895983            0 
# Runtime: 12.908279 s (overhead: 0.000062 %) 10 records
