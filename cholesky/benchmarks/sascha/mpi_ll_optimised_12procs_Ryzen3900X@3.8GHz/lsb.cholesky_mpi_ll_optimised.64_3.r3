# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 11:35:52 2021
# Execution time and date (local): Tue Nov 23 12:35:52 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            64           MPI          0 165334.169886            0 
            3            64           MPI          1 251494.619797            3 
            3            64           MPI          2 155826.804793            1 
            3            64           MPI          3 150127.265360            0 
            3            64           MPI          4 131749.165963            2 
            3            64           MPI          5 164750.714987            0 
            3            64           MPI          6 190502.524405            0 
            3            64           MPI          7 144359.556196            0 
            3            64           MPI          8 95811.318911            1 
            3            64           MPI          9 2106.387179            0 
# Runtime: 1.716694 s (overhead: 0.000408 %) 10 records
