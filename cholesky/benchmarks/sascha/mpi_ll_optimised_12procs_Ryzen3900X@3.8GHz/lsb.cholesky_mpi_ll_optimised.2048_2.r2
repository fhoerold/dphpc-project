# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 11:48:45 2021
# Execution time and date (local): Tue Nov 23 12:48:45 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 5638682.373885            0 
            2          2048           MPI          1 5645106.901881            2 
            2          2048           MPI          2 5710816.256742            2 
            2          2048           MPI          3 5651194.927480            0 
            2          2048           MPI          4 5525026.137106            2 
            2          2048           MPI          5 5741813.134069            0 
            2          2048           MPI          6 5452351.038644            0 
            2          2048           MPI          7 5550424.833651            0 
            2          2048           MPI          8 5568290.808105            1 
            2          2048           MPI          9 5666042.149562            0 
# Runtime: 76.406980 s (overhead: 0.000009 %) 10 records
