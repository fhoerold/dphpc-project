# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 11:36:36 2021
# Execution time and date (local): Tue Nov 23 12:36:36 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 377993.441208            0 
            3           512           MPI          1 271108.877149            1 
            3           512           MPI          2 428277.879212            1 
            3           512           MPI          3 292772.766834            0 
            3           512           MPI          4 273728.685158            2 
            3           512           MPI          5 416344.379301            0 
            3           512           MPI          6 394660.001195            0 
            3           512           MPI          7 322055.569092            0 
            3           512           MPI          8 408277.906236            2 
            3           512           MPI          9 524203.848194            0 
# Runtime: 7.568495 s (overhead: 0.000079 %) 10 records
