# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 11:36:15 2021
# Execution time and date (local): Tue Nov 23 12:36:15 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 911068.832851            0 
            2           256           MPI          1 756310.253493            2 
            2           256           MPI          2 819001.631892            2 
            2           256           MPI          3 637476.098488            0 
            2           256           MPI          4 704058.673821            2 
            2           256           MPI          5 585579.027116            0 
            2           256           MPI          6 402908.084978            0 
            2           256           MPI          7 390172.449162            0 
            2           256           MPI          8 573304.326585            2 
            2           256           MPI          9 707069.467063            0 
# Runtime: 13.613724 s (overhead: 0.000059 %) 10 records
