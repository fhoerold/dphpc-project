# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 11:36:01 2021
# Execution time and date (local): Tue Nov 23 12:36:01 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 76249.276840            0 
            2           128           MPI          1 21182.501743            2 
            2           128           MPI          2 82345.121287            1 
            2           128           MPI          3 359766.035543            0 
            2           128           MPI          4 416967.853271            2 
            2           128           MPI          5 438894.709292            0 
            2           128           MPI          6 386835.467003            0 
            2           128           MPI          7 243972.547725            0 
            2           128           MPI          8 343841.888144            2 
            2           128           MPI          9 409581.223169            0 
# Runtime: 7.698184 s (overhead: 0.000091 %) 10 records
