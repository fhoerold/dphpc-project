# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 14:36:29 2021
# Execution time and date (local): Tue Nov 23 15:36:29 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          4096           MPI          0 116668137.207998            0 
            2          4096           MPI          1 118619268.247679            2 
            2          4096           MPI          2 119342958.495516            2 
            2          4096           MPI          3 124681445.938881            0 
            2          4096           MPI          4 123818101.881470            2 
            2          4096           MPI          5 123481414.213951            0 
            2          4096           MPI          6 122770809.786148            0 
            2          4096           MPI          7 122556293.922756            0 
            2          4096           MPI          8 121893882.342476            2 
            2          4096           MPI          9 122259569.842178            0 
# Runtime: 1457.942432 s (overhead: 0.000001 %) 10 records
