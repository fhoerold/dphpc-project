# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 11:35:52 2021
# Execution time and date (local): Tue Nov 23 12:35:52 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            64           MPI          0 165990.937000            0 
            2            64           MPI          1 251495.136256            3 
            2            64           MPI          2 160522.116334            2 
            2            64           MPI          3 150125.951997            0 
            2            64           MPI          4 135733.850018            2 
            2            64           MPI          5 166072.870493            0 
            2            64           MPI          6 193088.754891            0 
            2            64           MPI          7 147885.189958            0 
            2            64           MPI          8 100593.320878            2 
            2            64           MPI          9 2105.322795            0 
# Runtime: 2.716728 s (overhead: 0.000331 %) 10 records
