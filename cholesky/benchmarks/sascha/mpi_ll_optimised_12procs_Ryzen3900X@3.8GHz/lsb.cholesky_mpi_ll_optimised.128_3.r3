# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 11:36:01 2021
# Execution time and date (local): Tue Nov 23 12:36:01 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 76399.643166            0 
            3           128           MPI          1 21181.669970            1 
            3           128           MPI          2 83367.589977            1 
            3           128           MPI          3 357930.068450            0 
            3           128           MPI          4 418112.526012            1 
            3           128           MPI          5 441094.503390            1 
            3           128           MPI          6 387815.606065            0 
            3           128           MPI          7 242429.455071            0 
            3           128           MPI          8 343842.545996            2 
            3           128           MPI          9 407687.773795            0 
# Runtime: 4.698226 s (overhead: 0.000128 %) 10 records
