# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 11:37:00 2021
# Execution time and date (local): Tue Nov 23 12:37:00 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 1171531.621150            0 
            3          1024           MPI          1 1446582.495095            2 
            3          1024           MPI          2 1305200.544051            2 
            3          1024           MPI          3 1413396.457079            0 
            3          1024           MPI          4 1006571.713502            1 
            3          1024           MPI          5 1107490.660773            0 
            3          1024           MPI          6 1161475.898218            1 
            3          1024           MPI          7 1181575.744950            0 
            3          1024           MPI          8 1297766.270061            2 
            3          1024           MPI          9 1523878.364137            0 
# Runtime: 15.691045 s (overhead: 0.000051 %) 10 records
