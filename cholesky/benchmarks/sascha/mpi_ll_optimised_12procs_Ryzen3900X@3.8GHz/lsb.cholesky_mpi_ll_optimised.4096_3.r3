# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 14:36:29 2021
# Execution time and date (local): Tue Nov 23 15:36:29 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          4096           MPI          0 116669244.343666            0 
            3          4096           MPI          1 118614037.364202            2 
            3          4096           MPI          2 119342697.145014            2 
            3          4096           MPI          3 124679544.625841            0 
            3          4096           MPI          4 123817598.591530            2 
            3          4096           MPI          5 123480903.263329            0 
            3          4096           MPI          6 122770322.052006            0 
            3          4096           MPI          7 122555811.855263            0 
            3          4096           MPI          8 121893408.545149            2 
            3          4096           MPI          9 122259069.967249            0 
# Runtime: 1454.933933 s (overhead: 0.000001 %) 10 records
