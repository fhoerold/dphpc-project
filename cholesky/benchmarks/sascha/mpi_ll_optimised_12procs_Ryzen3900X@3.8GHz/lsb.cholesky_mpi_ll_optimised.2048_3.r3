# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 11:48:45 2021
# Execution time and date (local): Tue Nov 23 12:48:45 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 5640103.935759            0 
            3          2048           MPI          1 5644792.109898            2 
            3          2048           MPI          2 5710914.834435            1 
            3          2048           MPI          3 5649910.069059            0 
            3          2048           MPI          4 5524831.039929            2 
            3          2048           MPI          5 5741887.288183            0 
            3          2048           MPI          6 5447003.299195            0 
            3          2048           MPI          7 5547401.009032            0 
            3          2048           MPI          8 5562646.079634            2 
            3          2048           MPI          9 5666044.644121            0 
# Runtime: 77.408020 s (overhead: 0.000009 %) 10 records
