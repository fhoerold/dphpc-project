# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 11:36:15 2021
# Execution time and date (local): Tue Nov 23 12:36:15 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 911069.061996            0 
            3           256           MPI          1 756764.940071            2 
            3           256           MPI          2 819003.790620            1 
            3           256           MPI          3 641027.093098            0 
            3           256           MPI          4 696671.805954            3 
            3           256           MPI          5 586189.995559            0 
            3           256           MPI          6 402893.991134            0 
            3           256           MPI          7 390162.516990            0 
            3           256           MPI          8 573304.072829            1 
            3           256           MPI          9 709741.915073            0 
# Runtime: 12.613688 s (overhead: 0.000055 %) 10 records
