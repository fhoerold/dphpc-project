# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 11:37:00 2021
# Execution time and date (local): Tue Nov 23 12:37:00 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 1172556.731046            0 
            2          1024           MPI          1 1446578.090972            2 
            2          1024           MPI          2 1307282.146431            1 
            2          1024           MPI          3 1416186.113614            0 
            2          1024           MPI          4 1003990.860087            2 
            2          1024           MPI          5 1106081.237995            0 
            2          1024           MPI          6 1157232.785272            0 
            2          1024           MPI          7 1180112.875833            0 
            2          1024           MPI          8 1298838.220992            1 
            2          1024           MPI          9 1528318.986828            0 
# Runtime: 19.691432 s (overhead: 0.000030 %) 10 records
