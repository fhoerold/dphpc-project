# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 11:35:35 2021
# Execution time and date (local): Tue Nov 23 12:35:35 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 83667.430538            0 
            2            32           MPI          1 55997.722578            1 
            2            32           MPI          2 51897.993458            1 
            2            32           MPI          3 36887.021093            0 
            2            32           MPI          4 43354.367053            1 
            2            32           MPI          5 54106.665808            0 
            2            32           MPI          6 51006.457014            0 
            2            32           MPI          7 59685.523761            0 
            2            32           MPI          8 63330.128443            2 
            2            32           MPI          9 45263.232380            0 
# Runtime: 11.908666 s (overhead: 0.000042 %) 10 records
