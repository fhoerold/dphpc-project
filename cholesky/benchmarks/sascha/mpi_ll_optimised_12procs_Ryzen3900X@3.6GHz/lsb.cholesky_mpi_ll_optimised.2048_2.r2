# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 10:58:46 2021
# Execution time and date (local): Tue Nov 23 11:58:46 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          2048           MPI          0 3943165.646302            0 
            2          2048           MPI          1 4179879.709141            1 
            2          2048           MPI          2 3982526.723615            1 
            2          2048           MPI          3 3936612.833935            0 
            2          2048           MPI          4 4110430.558131            1 
            2          2048           MPI          5 4250786.726653            0 
            2          2048           MPI          6 4398200.303856            0 
            2          2048           MPI          7 3925868.392133            0 
            2          2048           MPI          8 3978769.629469            2 
            2          2048           MPI          9 4044493.827635            0 
# Runtime: 55.736239 s (overhead: 0.000009 %) 10 records
