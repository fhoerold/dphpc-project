# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 10:44:10 2021
# Execution time and date (local): Tue Nov 23 11:44:10 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            64           MPI          0 1202.951643            0 
            3            64           MPI          1 548.499713            1 
            3            64           MPI          2 552.789398            0 
            3            64           MPI          3 1158.824884            0 
            3            64           MPI          4 738.495758            0 
            3            64           MPI          5 679.770071            0 
            3            64           MPI          6 1197.902014            0 
            3            64           MPI          7 1358.630209            0 
            3            64           MPI          8 567.478319            0 
            3            64           MPI          9 568.438248            0 
# Runtime: 11.064645 s (overhead: 0.000009 %) 10 records
