# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 10:44:00 2021
# Execution time and date (local): Tue Nov 23 11:44:00 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            32           MPI          0 487.391332            0 
            2            32           MPI          1 156.447588            0 
            2            32           MPI          2 156.887553            0 
            2            32           MPI          3 169.016591            0 
            2            32           MPI          4 163.167055            0 
            2            32           MPI          5 263.979057            0 
            2            32           MPI          6 154.827716            0 
            2            32           MPI          7 202.513933            0 
            2            32           MPI          8 179.945724            0 
            2            32           MPI          9 150.748040            0 
# Runtime: 5.024549 s (overhead: 0.000000 %) 10 records
