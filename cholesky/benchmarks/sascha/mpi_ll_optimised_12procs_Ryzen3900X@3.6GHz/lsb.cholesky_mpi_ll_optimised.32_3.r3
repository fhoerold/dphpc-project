# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 10:44:00 2021
# Execution time and date (local): Tue Nov 23 11:44:00 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3            32           MPI          0 494.371695            0 
            3            32           MPI          1 156.837848            0 
            3            32           MPI          2 156.937840            0 
            3            32           MPI          3 170.366799            0 
            3            32           MPI          4 163.777310            0 
            3            32           MPI          5 263.729565            0 
            3            32           MPI          6 155.587945            0 
            3            32           MPI          7 165.577171            0 
            3            32           MPI          8 180.196038            0 
            3            32           MPI          9 151.708245            0 
# Runtime: 4.024513 s (overhead: 0.000000 %) 10 records
