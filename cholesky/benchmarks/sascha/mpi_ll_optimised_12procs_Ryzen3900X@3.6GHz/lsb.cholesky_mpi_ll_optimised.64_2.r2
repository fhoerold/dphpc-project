# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 10:44:10 2021
# Execution time and date (local): Tue Nov 23 11:44:10 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2            64           MPI          0 1201.112787            0 
            2            64           MPI          1 541.850656            0 
            2            64           MPI          2 552.319896            0 
            2            64           MPI          3 1157.915924            0 
            2            64           MPI          4 737.896144            0 
            2            64           MPI          5 678.610726            0 
            2            64           MPI          6 1198.053009            0 
            2            64           MPI          7 1357.301446            0 
            2            64           MPI          8 568.068753            0 
            2            64           MPI          9 568.238740            0 
# Runtime: 14.064709 s (overhead: 0.000000 %) 10 records
