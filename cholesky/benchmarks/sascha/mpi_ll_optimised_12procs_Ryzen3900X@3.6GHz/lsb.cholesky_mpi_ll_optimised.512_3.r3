# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 10:44:58 2021
# Execution time and date (local): Tue Nov 23 11:44:58 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           512           MPI          0 44166.178460            0 
            3           512           MPI          1 61889.667788            1 
            3           512           MPI          2 44342.115945            1 
            3           512           MPI          3 108922.902314            0 
            3           512           MPI          4 51029.400001            1 
            3           512           MPI          5 48185.792822            0 
            3           512           MPI          6 53225.954038            0 
            3           512           MPI          7 50753.739609            0 
            3           512           MPI          8 45828.500497            1 
            3           512           MPI          9 141299.859341            0 
# Runtime: 4.887753 s (overhead: 0.000082 %) 10 records
