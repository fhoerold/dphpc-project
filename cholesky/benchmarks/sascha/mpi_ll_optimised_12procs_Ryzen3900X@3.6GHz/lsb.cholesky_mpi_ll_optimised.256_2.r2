# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 10:44:45 2021
# Execution time and date (local): Tue Nov 23 11:44:45 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           256           MPI          0 5259.421690            0 
            2           256           MPI          1 6561.178054            0 
            2           256           MPI          2 5304.148473            0 
            2           256           MPI          3 6010.437669            0 
            2           256           MPI          4 17943.396276            1 
            2           256           MPI          5 6596.198869            0 
            2           256           MPI          6 5607.116680            0 
            2           256           MPI          7 16092.802167            0 
            2           256           MPI          8 7170.284241            0 
            2           256           MPI          9 5232.113654            0 
# Runtime: 0.093115 s (overhead: 0.001074 %) 10 records
