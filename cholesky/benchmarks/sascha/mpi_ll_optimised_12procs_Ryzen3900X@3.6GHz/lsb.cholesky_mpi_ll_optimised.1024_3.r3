# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 10:45:21 2021
# Execution time and date (local): Tue Nov 23 11:45:21 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          1024           MPI          0 522412.018737            0 
            3          1024           MPI          1 502047.220840            2 
            3          1024           MPI          2 965783.797378            1 
            3          1024           MPI          3 1042161.364238            0 
            3          1024           MPI          4 534847.403634            1 
            3          1024           MPI          5 535445.801537            0 
            3          1024           MPI          6 497718.525640            0 
            3          1024           MPI          7 533203.909531            0 
            3          1024           MPI          8 491618.314788            2 
            3          1024           MPI          9 648157.358977            0 
# Runtime: 6.966455 s (overhead: 0.000086 %) 10 records
