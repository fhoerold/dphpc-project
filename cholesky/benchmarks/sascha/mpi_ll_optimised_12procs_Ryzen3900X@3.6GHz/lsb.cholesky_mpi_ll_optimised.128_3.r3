# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 10:44:30 2021
# Execution time and date (local): Tue Nov 23 11:44:30 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           128           MPI          0 5992.151635            0 
            3           128           MPI          1 1629.252653            1 
            3           128           MPI          2 1657.660432            0 
            3           128           MPI          3 1989.794472            0 
            3           128           MPI          4 1624.952989            0 
            3           128           MPI          5 1871.483719            0 
            3           128           MPI          6 2707.478375            0 
            3           128           MPI          7 1587.775895            0 
            3           128           MPI          8 1565.057670            1 
            3           128           MPI          9 2501.704459            0 
# Runtime: 6.061076 s (overhead: 0.000033 %) 10 records
