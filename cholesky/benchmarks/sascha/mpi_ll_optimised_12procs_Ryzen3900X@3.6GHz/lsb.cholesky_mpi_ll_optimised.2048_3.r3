# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 10:58:46 2021
# Execution time and date (local): Tue Nov 23 11:58:46 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3          2048           MPI          0 3943210.264268            0 
            3          2048           MPI          1 4179936.112307            2 
            3          2048           MPI          2 3982575.433842            2 
            3          2048           MPI          3 3936660.036889            0 
            3          2048           MPI          4 4110477.779997            1 
            3          2048           MPI          5 4250835.988405            0 
            3          2048           MPI          6 4398247.617115            0 
            3          2048           MPI          7 3925914.608612            0 
            3          2048           MPI          8 3978817.080757            1 
            3          2048           MPI          9 4044543.328048            0 
# Runtime: 55.736610 s (overhead: 0.000011 %) 10 records
