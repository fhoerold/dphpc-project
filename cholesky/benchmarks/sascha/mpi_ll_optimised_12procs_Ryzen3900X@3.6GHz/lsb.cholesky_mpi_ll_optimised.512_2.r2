# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 10:44:58 2021
# Execution time and date (local): Tue Nov 23 11:44:58 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           512           MPI          0 44161.021071            0 
            2           512           MPI          1 61185.639070            1 
            2           512           MPI          2 44338.748798            0 
            2           512           MPI          3 109106.555173            0 
            2           512           MPI          4 51027.016472            2 
            2           512           MPI          5 48182.037385            0 
            2           512           MPI          6 53223.131382            0 
            2           512           MPI          7 50616.095491            0 
            2           512           MPI          8 45825.523524            0 
            2           512           MPI          9 141296.291934            0 
# Runtime: 7.887789 s (overhead: 0.000038 %) 10 records
