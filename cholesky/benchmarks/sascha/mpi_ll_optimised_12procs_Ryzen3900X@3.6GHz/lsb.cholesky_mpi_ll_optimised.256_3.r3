# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 10:44:45 2021
# Execution time and date (local): Tue Nov 23 11:44:45 2021
# MPI execution on rank 3 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            3           256           MPI          0 5262.506837            0 
            3           256           MPI          1 6562.552181            1 
            3           256           MPI          2 5305.883956            1 
            3           256           MPI          3 6010.442380            0 
            3           256           MPI          4 17943.283550            0 
            3           256           MPI          5 6598.159310            0 
            3           256           MPI          6 5608.041678            0 
            3           256           MPI          7 16094.998123            0 
            3           256           MPI          8 7171.497843            1 
            3           256           MPI          9 5233.828925            0 
# Runtime: 6.093320 s (overhead: 0.000049 %) 10 records
