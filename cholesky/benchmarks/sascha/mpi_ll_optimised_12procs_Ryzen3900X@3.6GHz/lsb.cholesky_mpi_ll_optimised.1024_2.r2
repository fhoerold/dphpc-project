# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 10:45:21 2021
# Execution time and date (local): Tue Nov 23 11:45:21 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2          1024           MPI          0 522396.971480            0 
            2          1024           MPI          1 502036.017792            2 
            2          1024           MPI          2 965768.104370            1 
            2          1024           MPI          3 1042138.513405            0 
            2          1024           MPI          4 534833.195004            2 
            2          1024           MPI          5 535523.974747            0 
            2          1024           MPI          6 497706.743232            0 
            2          1024           MPI          7 533189.011155            0 
            2          1024           MPI          8 491607.578286            2 
            2          1024           MPI          9 648142.952138            0 
# Runtime: 14.965970 s (overhead: 0.000047 %) 10 records
