# Sysname : Linux
# Nodename: SASCHA-PC
# Release : 5.10.60.1-microsoft-standard-WSL2
# Version : #1 SMP Wed Aug 25 23:20:18 UTC 2021
# Machine : x86_64
# Execution time and date (UTC): Tue Nov 23 10:44:30 2021
# Execution time and date (local): Tue Nov 23 11:44:30 2021
# MPI execution on rank 2 with 12 processes in world
# Reported time measurements are in microseconds
# pretty output format
         rank         size         type       id         time     overhead 
            2           128           MPI          0 5990.732133            0 
            2           128           MPI          1 1628.478255            0 
            2           128           MPI          2 1656.196183            0 
            2           128           MPI          3 1990.571185            0 
            2           128           MPI          4 1622.938669            1 
            2           128           MPI          5 1871.170111            0 
            2           128           MPI          6 2706.607654            0 
            2           128           MPI          7 1583.001655            0 
            2           128           MPI          8 1564.193061            0 
            2           128           MPI          9 2500.663050            0 
# Runtime: 9.059758 s (overhead: 0.000011 %) 10 records
