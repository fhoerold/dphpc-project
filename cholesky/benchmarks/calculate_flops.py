import numpy as np
import pandas as pd
# import matplotlib.pyplot as plt
import sys
import os

args = sys.argv

# Ryzen clock speed in GHz
CLOCK_SPEED = 3.8
# CLOCK_SPEED = 3.6

dir = args[1]
output_file_name = args[2]
if len(args) >= 4:
    for_roofline = args[3]
else:
    for_roofline = False

times_data = {}
flops_per_c_data = {}

roof_ns = [32, 128, 512, 2048]

dir_list = os.listdir(dir)
dir_list = sorted(dir_list)
k = 0
for filename in dir_list:
    filepath = os.path.join(dir, filename)
    df = pd.read_csv(filepath, comment='#', sep='\s+')
    n = df["size"][0]

    if not for_roofline or n in roof_ns:
        # discard first two runs
        times = df["time"][2:]
        # print(times)

        mean_time = np.mean(times)
        # print("Mean Time = " + str(mean_time))

        if n in times_data:
            times_data[n] += mean_time
        else:
            times_data[n] = mean_time
    k += 1

for n in times_data:
    if not for_roofline or n in roof_ns:
        # for now use (n^3)/3, but for more accurate results should count flops
        flops = (n**3) / 3

        mean_time = times_data[n]

        # convert to cycles, clock speed = [GHz], mean_time = [microseconds] ==> factor of 1000
        cycles = mean_time * CLOCK_SPEED * 1000
        # print("Cycles = " + str(cycles))

        # calculate flops/cycle
        flops_per_c = flops / cycles
        # print("Flops / Cycle = " + str(flops_per_c))

        flops_per_c_data[n] = flops_per_c

output_path = os.path.join(os.getcwd(), output_file_name)
if os.path.exists(output_path):
    output_df = pd.read_csv(output_path, index_col=0)

    index = sorted(flops_per_c_data)
    flops_per_c_list = []
    for idx in index:
        flops_per_c_list.append(flops_per_c_data[idx])

    output_df['perf'] = flops_per_c_list
else:
    output_df = pd.DataFrame.from_dict(flops_per_c_data, orient='index', columns=["perf"])
    output_df.index.name="n"
    output_df = output_df.sort_index()

output_df.to_csv(output_path)

# print(flops_list)

# plt.plot(n_list, flops_list)
# plt.xlabel('n')
# plt.xscale('log', base=2)
# plt.ylabel('flops / cycle')
# plt.grid(True)
# plt.show()