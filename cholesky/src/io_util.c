#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "io_util.h"

void print_array(int n, double *A) {
    int i, j;

    fprintf(stderr, "==BEGIN DUMP_ARRAYS==\n");
    fprintf(stderr, "begin dump: %s", "A");
    for (i = 0; i < n; i++) {
        for (j = 0; j <= i; j++) {
            if ((i*n + j) % 20 == 0) {
                fprintf (stderr, "\n");
            }
            fprintf(stderr, "%0.2lf ", A[i*n + j]);
        }
    }
    fprintf(stderr, "\nend   dump: %s\n", "A");
    fprintf(stderr, "==END   DUMP_ARRAYS==\n");
}

void save_array(int n, double *A, char *path) {
    int i, j;

    FILE *file = fopen(path, "w");
    if (file) {
        for (i = 0; i < n; i++) {
            for (j = 0; j <= i; j++) {
                fprintf(file, "%0.10lf ", A[i*n + j]);
            }
            fprintf(file, "\n");
        }
        fclose(file);
    } else {
        printf("Couldn't open file\n");
    }
}

void save_array_c_major(int n, double *A, char *path) {
    int i, j;

    FILE *file = fopen(path, "w");
    if (file) {
        for (i = 0; i < n; i++) {
            for (j = 0; j <= i; j++) {
                fprintf(file, "%0.10lf ", A[i + j*n]);
            }
            fprintf(file, "\n");
        }
        fclose(file);
    } else {
        printf("Couldn't open file");
    }
}

void init_array(int n, double *A) {
    for (int i = 0; i < n; i++){
        for (int j = 0; j <= i; j++) {
            A[i*n + j] = (double)(-j % n) / n + 1;
        }
        for (int j = i+1; j < n; j++) {
            A[i*n + j] = 0;
        }
        A[i*n + i] = 1;
    }

    double *B = (double *) malloc(n*n*sizeof(double));
    for (int r = 0; r < n; ++r) {
        for (int s = 0; s < n; ++s) {
            B[r*n + s] = 0;
        }
    }
    for (int t = 0; t < n; ++t) {
        for (int r = 0; r < n; ++r) {
            for (int s = 0; s < n; ++s) {
                B[r*n + s] = B[r*n + s] + (A[r*n + t] * A[s*n + t]);
            }
        }
    }
    for (int r = 0; r < n; ++r) {
        for (int s = 0; s < n; ++s) {
            A[r*n + s] = B[r*n + s];
        }
    }
    free(B);
}

void handle_args(int argc, char** argv, int *n, int *dump_array_term, int *dump_array_file, char **data_file, int *do_benchmark, char**exe_name, int *num_runs, int *block_size) {
    char *pexe = argv[0] + strlen(argv[0]);
    for (; pexe > argv[0]; pexe--) {
        if ((*pexe == '\\') || (*pexe == '/')) {
            pexe++;
            break;
        }
    }
    strncpy(*exe_name, pexe, 49);

    int opt;
    while ((opt = getopt(argc, argv, "d:ps:br:t:")) != -1) {
        switch (opt) {
            case 'd': {
                *n = atoi(optarg);
                break;
            }
            case 'p': {
                *dump_array_term = 1;
                break;
            }
            case 's': {
                *data_file = optarg;
                *dump_array_file = 1;
                break;
            }
            case 'b': {
                *do_benchmark = 1;
                break;
            }
            case 'r': {
                *num_runs = atoi(optarg);
                break;
            }
            case 't': {
                *block_size = atoi(optarg);
                break;
            }
        }
    }
}