/**
 * This version is stamped on May 10, 2016
 *
 * Contact:
 *   Louis-Noel Pouchet <pouchet.ohio-state.edu>
 *   Tomofumi Yuki <tomofumi.yuki.fr>
 *
 * Web address: http://polybench.sourceforge.net
 */
/* cholesky.c: this file is part of PolyBench/C */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#include <stdlib.h>
#include <liblsb.h>
#include <mpi.h>

#include "io_util.h"
#include "benchmark.h"

#define NUM_RUNS 10
#define N 32

int myrank, mpi_size, sbuf=23, rbuf=32;

static void kernel_cholesky_main(int n, double *A) {
    double *col = (double *) malloc(n*sizeof(double));
    double *top_row = (double *) malloc(n*sizeof(double));
    double *curr_rows;

    int max_col_len = n - 1;
    int max_chunk_size = (int) floor(max_col_len / mpi_size);

    if (myrank != 0) {
        curr_rows = (double *) malloc(max_chunk_size*n*sizeof(double));
    } else {
        curr_rows = NULL;
    }

    int *sendcounts = (int *) malloc(mpi_size*sizeof(int));
    int *displs = (int *) malloc(mpi_size*sizeof(int));
    int *rowscounts = (int *) malloc(mpi_size*sizeof(int));
    int *rowdispls = (int *) malloc(mpi_size*sizeof(int));

    for (int j = 0; j < n; j++) {
    // for (int j = 0; j < 5; j++) {
        double diag;
        int col_len = n - (j+1);
        int other_chunk_size = (int) floor(col_len / mpi_size);

        if (myrank == 0) {
            for (int k = 0; k < j; k++) {
                A[j*n + j] = A[j*n + j] - (A[j*n + k] * A[j*n + k]);
            }
            A[j*n + j] = sqrt(A[j*n + j]);
        }

        int my_chunk_size = other_chunk_size + (col_len - mpi_size*other_chunk_size);

        if (mpi_size > 1) {
            if (myrank == 0) {
                diag = A[j*n + j];

                int idx = 0;
                for (int k = j + 1; k < n; k++) {
                    col[idx++] = A[k*n + j];
                }
            }

            MPI_Bcast(&diag, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

            sendcounts[0] = my_chunk_size;
            displs[0] = 0;
            for (int p = 1; p < mpi_size; p++) {
                sendcounts[p] = other_chunk_size;
                displs[p] = my_chunk_size + (p-1)*other_chunk_size;
            }

            // printf("PRE_SCATTER, rank=%d, jsend=%d\n", myrank, j);

            MPI_Scatterv(col, sendcounts, displs, MPI_DOUBLE, col, other_chunk_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);

            // nothing to send if j == 0
            if (j > 0) {
                if (myrank == 0) {
                    for (int k = 0; k < j; k++) {
                        top_row[k] = A[j*n + k];
                    }
                }

                rowscounts[0] = 0;
                rowdispls[0] = 0;
                for (int p = 1; p < mpi_size; p++) {
                    rowscounts[p] = 1;
                    rowdispls[p] = 0;
                }

                MPI_Datatype vec_stride;
                MPI_Datatype vec_stride_res;
                MPI_Type_vector(other_chunk_size, j, n, MPI_DOUBLE, &vec_stride);
                MPI_Type_create_resized(vec_stride, 0, other_chunk_size*n*sizeof(double), &vec_stride_res);
                MPI_Type_commit(&vec_stride_res);

                MPI_Bcast(top_row, j, MPI_DOUBLE, 0, MPI_COMM_WORLD);

                MPI_Scatterv(&A[(j + 1 + my_chunk_size)*n], rowscounts, rowdispls, vec_stride_res, curr_rows, other_chunk_size*j, MPI_DOUBLE, 0, MPI_COMM_WORLD);
                // MPI_Scatterv(&A[-1], rowscounts, rowscounts, vec_stride, curr_rows, other_chunk_size*j, MPI_DOUBLE, 0, MPI_COMM_WORLD);
            }

            // printf("rank=%d, jsend=%d\n", myrank, j);
        }

        if (myrank == 0) {
            for (int i = j + 1; i < j + 1 + my_chunk_size; i++) {
                for (int k = 0; k < j; k++) {
                    A[i*n + j] = A[i*n + j] - (A[i*n + k] * A[j*n + k]);
                }
                A[i*n + j] = A[i*n + j] / A[j*n + j];
            }
        } else {
            // if (myrank == 1) {
            //     printf("pre %d : =======================\n", j);
            //     printf("col: ");
            //     for (int w = 0; w < other_chunk_size; w++) {
            //         printf("%f ", col[w]);
            //     }
            //     printf("\n");
            // }

            for (int i = 0; i < other_chunk_size; i++) {
                for (int k = 0; k < j; k++) {
                    col[i] -= curr_rows[i*j + k] * top_row[k];
                }
                col[i] /= diag;
            }

            // if (myrank == 1) {
            //     printf("post %d : =======================\n", j);

            //     printf("col: ");
            //     for (int w = 0; w < other_chunk_size; w++) {
            //         printf("%f ", col[w]);
            //     }
            //     printf("\n");

            //     printf("top_row: ");
            //     for (int w = 0; w < j; w++) {
            //         printf("%f ", top_row[w]);
            //     }
            //     printf("\n");

            //     printf("curr_rows: ");
            //     for (int w = 0; w < other_chunk_size; w++) {
            //         for (int v = 0; v < j; v++) {
            //             printf("%f ", curr_rows[w*j + v]);
            //         }
            //         printf("\n");
            //     }
            //     printf("\n");
            // }
        }

        if (mpi_size > 1) {
            // if (myrank == 0) {
            //     for (int w = 0; w < n; w++) {
            //         col[w] = 999999;
            //     }
            // }

            MPI_Gatherv(col, other_chunk_size, MPI_DOUBLE, col, sendcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);

            if (myrank == 0) {
                // printf("%d : =======================\n", j);
                // for (int w = 0; w < n; w++) {
                //     printf("%f ", col[w]);
                // }
                // printf("\n");

                int idx = my_chunk_size;
                for (int k = j + 1 + my_chunk_size; k < n; k++) {
                    A[k*n + j] = col[idx++];
                }

                // // printf("%d   %d\n", n-1, idx - 1);
                // // printf("%f\n", col[idx-1]);
                // printf("mychunk %d, otherchunk %d\n", my_chunk_size, other_chunk_size);
            }

            // printf("rank=%d, recv=%d\n", myrank, j);
        }
    }

    free(col);
    free(sendcounts);
    free(displs);
    free(rowscounts);
    free(top_row);

    if (myrank != 0) {
        free(curr_rows);
    }
}

static void kernel_cholesky_other(int n) {
    kernel_cholesky_main(n, NULL);
}

int main(int argc, char** argv) {
    MPI_Init(NULL, NULL);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);

    int n = N;
    int num_runs = NUM_RUNS;
    int do_benchmark = 0;
    int dump_array_term = 0;
    int dump_array_file = 0;
    double *A = NULL;
    int blocksize = 1;

    char *data_file;
    char *exe_name = (char *) malloc(sizeof(char)*50);

    if (myrank == 0) {
        handle_args(argc, argv, &n, &dump_array_term, &dump_array_file, &data_file, &do_benchmark, &exe_name, &num_runs, &blocksize);

        A = (double *) malloc(n*n*sizeof(double));

        init_array(n, A);
    }

    MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&num_runs, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&do_benchmark, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(exe_name, 50, MPI_CHAR, 0, MPI_COMM_WORLD);

    if (myrank == 0) {
        if (do_benchmark) {
            benchmark_fun_mpi(&kernel_cholesky_main, NULL, myrank, n, A, num_runs, exe_name, blocksize);
        } else {
            kernel_cholesky_main(n, A);

            if (dump_array_file) {
                save_array(n, A, data_file);
            }

            if (dump_array_term) {
                print_array(n, A);
            }
        }

        free(A);
    } else {
        if (do_benchmark) {
            benchmark_fun_mpi(NULL, &kernel_cholesky_other, myrank, n, NULL, num_runs, exe_name, blocksize);
        } else {
            kernel_cholesky_other(n);
        }
    }

    MPI_Finalize();

    return 0;
}