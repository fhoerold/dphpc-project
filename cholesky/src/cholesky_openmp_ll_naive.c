/**
 * This version is stamped on May 10, 2016
 *
 * Contact:
 *   Louis-Noel Pouchet <pouchet.ohio-state.edu>
 *   Tomofumi Yuki <tomofumi.yuki.fr>
 *
 * Web address: http://polybench.sourceforge.net
 */
/* cholesky.c: this file is part of PolyBench/C */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#include <stdlib.h>
#include <liblsb.h>
#include <mpi.h>
#include <omp.h>

#include "io_util.h"
#include "benchmark.h"

#define NUM_RUNS 10
#define N 32

static void kernel_cholesky(int n, double *A) {
    for (int j = 0; j < n; j++) {
        for (int k = 0; k < j; k++) {
            A[j*n + j] = A[j*n + j] - (A[j*n + k] * A[j*n + k]);
        }
        A[j*n + j] = sqrt(A[j*n + j]);

        int i, k;
        #pragma omp parallel for private(k) shared (A,j) schedule(static)
        for (i = j + 1; i < n; i++) {
            for (k = 0; k < j; k++) {
                A[i*n + j] = A[i*n + j] - (A[i*n + k] * A[j*n + k]);
            }
            A[i*n + j] = A[i*n + j] / A[j*n + j];
        }
    }
}


int main(int argc, char** argv) {
    MPI_Init(NULL, NULL);

    int n = N;
    int num_runs = NUM_RUNS;
    int dump_array_term = 0;
    int dump_array_file = 0;
    int do_benchmark = 0;
    int blocksize = 1;

    char *data_file;
    char *exe_name = (char *) malloc(sizeof(char)*50);

    handle_args(argc, argv, &n, &dump_array_term, &dump_array_file, &data_file, &do_benchmark, &exe_name, &num_runs, &blocksize);

    double *A = (double *) malloc(n*n*sizeof(double));

    init_array(n, A);

    if (do_benchmark) {
        benchmark_fun(&kernel_cholesky, n, A, num_runs, exe_name);
    } else {
        kernel_cholesky(n, A);

        if (dump_array_file) {
            save_array(n, A, data_file);
        }

        if (dump_array_term) {
            print_array(n, A);
        }
    }

    free(A);

    MPI_Finalize();

    return 0;
}