/**
 * This version is stamped on May 10, 2016
 *
 * Contact:
 *   Louis-Noel Pouchet <pouchet.ohio-state.edu>
 *   Tomofumi Yuki <tomofumi.yuki.fr>
 *
 * Web address: http://polybench.sourceforge.net
 */
/* cholesky.c: this file is part of PolyBench/C */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#include <stdlib.h>
#include <liblsb.h>
#include <mpi.h>

#include "io_util.h"
#include "benchmark.h"

#define NUM_RUNS 10
#define N 32

int myrank, mpi_size, sbuf=23, rbuf=32;

static void kernel_cholesky_main(int n, double *A) {
    double *col = (double *) malloc(n*sizeof(double));
    double *top_row = (double *) malloc(n*sizeof(double));

    int max_col_len = n - 1;
    int max_chunk_size = (int) floor(max_col_len / mpi_size);

    double *curr_rows = (double *) malloc(max_chunk_size*n*sizeof(double));

    for (int j = 0; j < n; j++) {
        int col_len = n - (j+1);
        int other_chunk_size = (int) floor(col_len / mpi_size);

        for (int k = 0; k < j; k++) {
            A[j*n + j] = A[j*n + j] - (A[j*n + k] * A[j*n + k]);
        }
        A[j*n + j] = sqrt(A[j*n + j]);

        int my_chunk_size = other_chunk_size + (col_len - mpi_size*other_chunk_size);

        if (mpi_size > 1) {
            double diag = A[j*n + j];
            int p = 1;
            for (int i = j + 1 + my_chunk_size; i < n; i+=other_chunk_size) {
                MPI_Send(&diag, 1, MPI_DOUBLE, p, 0, MPI_COMM_WORLD);

                int idx = 0;
                for (int k = i; k < i + other_chunk_size; k++) {
                    col[idx++] = A[k*n + j];
                }
                MPI_Send(col, other_chunk_size, MPI_DOUBLE, p, 0, MPI_COMM_WORLD);

                // nothing to send if j == 0
                if (j > 0) {
                    for (int k = 0; k < j; k++) {
                        top_row[k] = A[j*n + k];

                        int idx = 0;
                        for (int u = i; u < i+other_chunk_size; u++) {
                            curr_rows[idx*j + k] = A[u*n + k];
                            idx++;
                        }
                    }

                    MPI_Send(top_row, j, MPI_DOUBLE, p, 0, MPI_COMM_WORLD);
                    MPI_Send(curr_rows, other_chunk_size*j, MPI_DOUBLE, p, 0, MPI_COMM_WORLD);
                }

                p++;
            }
        }

        for (int i = j + 1; i < j + 1 + my_chunk_size; i++) {
            for (int k = 0; k < j; k++) {
                A[i*n + j] = A[i*n + j] - (A[i*n + k] * A[j*n + k]);
            }
            A[i*n + j] = A[i*n + j] / A[j*n + j];
        }

        if (mpi_size > 1) {
            int p = 1;
            for (int i = j + 1 + my_chunk_size; i < n; i+=other_chunk_size) {
                MPI_Recv(col, other_chunk_size, MPI_DOUBLE, p, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

                int idx = 0;
                for (int k = i; k < i + other_chunk_size; k++) {
                    A[k*n + j] = col[idx++];
                }
                p++;
            }
        }
    }

    free(col);
    free(top_row);
    free(curr_rows);
}

static void kernel_cholesky_other(int n) {
    double *col = (double *) malloc(n*sizeof(double));
    double *top_row = (double *) malloc(n*sizeof(double));

    int max_col_len = n - 1;
    int max_chunk_size = (int) floor(max_col_len / mpi_size);

    double *curr_rows = (double *) malloc(max_chunk_size*n*sizeof(double));

    for (int j = 0; j < n; j++) {
        int col_len = n - (j+1);
        int other_chunk_size = (int) floor(col_len / mpi_size);
        if (other_chunk_size == 0) {
            break;
        }
        double diag;
        MPI_Recv(&diag, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Recv(col, other_chunk_size, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        // nothing to receive
        if (j > 0) {
            MPI_Recv(top_row, j, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            MPI_Recv(curr_rows, other_chunk_size*j, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }

        for (int i = 0; i < other_chunk_size; i++) {
            for (int k = 0; k < j; k++) {
                col[i] -= curr_rows[i*j + k] * top_row[k];
            }
            col[i] /= diag;
        }

        MPI_Send(col, other_chunk_size, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
    }

    free(col);
    free(top_row);
    free(curr_rows);
}

int main(int argc, char** argv) {
    MPI_Init(NULL, NULL);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);

    int n = N;
    int num_runs = NUM_RUNS;
    int do_benchmark = 0;
    int dump_array_term = 0;
    int dump_array_file = 0;
    double *A = NULL;
    int blocksize = 1;
    
    char *data_file;
    char *exe_name = (char *) malloc(sizeof(char)*50);

    if (myrank == 0) {
        handle_args(argc, argv, &n, &dump_array_term, &dump_array_file, &data_file, &do_benchmark, &exe_name, &num_runs, &blocksize);

        A = (double *) malloc(n*n*sizeof(double));

        init_array(n, A);
    }

    MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&num_runs, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&do_benchmark, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(exe_name, 50, MPI_CHAR, 0, MPI_COMM_WORLD);

    if (myrank == 0) {
        if (do_benchmark) {
            benchmark_fun_mpi(&kernel_cholesky_main, NULL, myrank, n, A, num_runs, exe_name, blocksize);
        } else {
            kernel_cholesky_main(n, A);

            if (dump_array_file) {
                save_array(n, A, data_file);
            }

            if (dump_array_term) {
                print_array(n, A);
            }
        }

        free(A);
    } else {
        if (do_benchmark) {
            benchmark_fun_mpi(NULL, &kernel_cholesky_other, myrank, n, NULL, num_runs, exe_name, blocksize);
        } else {
            kernel_cholesky_other(n);
        }
    }

    MPI_Finalize();

    return 0;
}