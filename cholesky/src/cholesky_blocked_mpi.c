//
// Created by Simeon Haefliger on 16.12.21.
//

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#include <stdlib.h>
#include <liblsb.h>
#include <mpi.h>

#include "io_util.h"
#include "benchmark.h"

#define NUM_RUNS 5
#define N 32

#define DEFAULT_BLOCK_SIZE 16

#define MAX_BLOCK_SIZE 64

int myrank, mpi_size, blocksize;

/**
 * Print contiguous 2D array to stdout
 * @param A contiguous 2D array
 * @param height
 * @param width
 */
static void print_full_array(double *A, int height, int width){
    printf("[%i] Double array of size %i x %i:", myrank, height, width);
    for (int i = 0; i < height; i++){
        printf("\n[%i] ", myrank);
        for (int j = 0; j < width; j++){
            printf("%10.7f ", A[i * width + j]);
        }
    }
    printf("\n");
}

/**
 * Left Looking Cholesky-Banachiewicz
 * Computes the decomposition in place, starting from the upper left-hand corner of the matrix A in a row-wise manner
 * @param A contiguous 2D array
 * @param n total number of rows in the array
 * @param row_start start decomposition from this row
 * @param row_end end decomposition at this row
 * @param col_start start decomposition from this column
 * @param col_end (not used, inferred from row end)
 */
static void llcb(double *A, int n, int row_start, int row_end, int col_start, int col_end){
    for (int row = row_start; row < row_end; row++) {
        for (int col = col_start; col < row; col++) {
            for (int k = 0; k < col; k++) {
                A[row * n + col] = A[row * n + col] - (A[row * n + k] * A[col * n + k]);
            }
            A[row * n + col] = A[row * n + col] / A[col * n + col];
        }
        for (int k = 0; k < row; k++) {
            A[row * n + row] = A[row * n + row] - (A[row * n + k] * A[row * n + k]);
        }
        A[row * n + row] = sqrt(A[row * n + row]);
    }
}

/**
 * Full Block Left Looking Cholesky-Banachiewicz
 * Computes the decomposition in place, starting from the upper left-hand corner of the matrix L in a row-wise manner
 * @param R precomputed values
 * @param L block that needs to be calculated including all leading rows
 * @param width width of the L and R array
 * @param n_rows number of rows to decompose
 * @param col_offset column where decomposition starts
 * @param n_cols number of columns to decompose
 */
static void fbllcb(double *R, double *L, int width, int n_rows, int col_offset, int n_cols){
    for (int row = 0; row < n_rows; row++) {
        for (int col = 0; col < n_cols; col++) {
            for (int k = 0; k < col_offset + col; k++) {
                L[row * width + col_offset + col] = L[row * width + col_offset + col] - (L[row * width + k] * R[col * width + k]);
            }
            L[row * width + col_offset + col] = L[row * width + col_offset + col] / R[col * width + col_offset + col];
        }
    }
}


static void kernel_cholesky(int n, double *A) {

    int number_of_workers = mpi_size - 1;

    // If main thread is only thread, do whole calculation
    if(number_of_workers < 1){
        llcb(A, n, 0,n,0,n);
        return;
    }

//    int step = 0;
//    int max_block_size =   pow(2, floor(log2(n)) - 2);
//    max_block_size = (max_block_size > MAX_BLOCK_SIZE) ? MAX_BLOCK_SIZE : max_block_size;
//
//    int max_ramp_steps = log2(n) - log2(DEFAULT_BLOCK_SIZE) - 1;
//
//
//
//    int ramp_up =
//    int max_block_size = pow(2, floor(log2(n)) - 2);
//    max_block_size = (max_block_size > MAX_BLOCK_SIZE) ? MAX_BLOCK_SIZE : max_block_size;

    // Otherwise, perform calculation if upper left block
    if(myrank == 0){
        llcb(A, n, 0,blocksize,0,blocksize);
    }

    // TODO: Add calculation here to ramp up block size as we go
    int next_blocksize;

    for(int column = 0; column < n; column += blocksize) {

        /* -------------------------------------------------------------------------------------------------------------
        // Work partitioning
        // -------------------------------------------------------------------------------------------------------------

        -
        - | a
        - | b | c
        - | d | e | f
        - | g | h | i | j
        - | - | - | - | - | -
            ^
            current column

        - block |a| is already done
        - |b| and |c| must be calculated by rank 0 because |c| depends on |b|
        - blocks |d|, |g| etc. can be distributed to all ranks
        - if there are many blocks below |g|, we give rank 0 a share of that work on top of |b| and |c|
        */

        next_blocksize = blocksize;
//        next_blocksize = 2 * blocksize;
//        next_blocksize = (next_blocksize > max_block_size) ? max_block_size : next_blocksize;

        // Number of rows to be calculated in this iteration
        int remaining_work = n - column;

        // Number of rows to be calculated by each rank != 0
        int worker_workload = 0;

        // Case 1, there is enough work to be done, such that rank 0 can help
        if (remaining_work > mpi_size * next_blocksize * 2) {
            worker_workload = remaining_work / mpi_size; // This might not divide without rest

            // Case 2, to the end, rank 0 will need to do the majority in order to be able to process the next diagonal
        } else {
            worker_workload = (remaining_work - 2 * next_blocksize) / number_of_workers;
        }
        // Case 3, workers cannot help anymore, everything needs to be done by rank 0
        if (worker_workload < 0) {
            worker_workload = 0;
        }
        // Number of rows to be calculated by rank 0
        int producer_workload = n - column - blocksize - worker_workload * number_of_workers;

        // Case 3
        if (worker_workload <= 0) {

            // Terminate workers
            if (myrank != 0) {
                return;
            }

            // Calculate remaining work on rank 0
            fbllcb(&A[column * n], &A[(column + blocksize) * n], n, producer_workload, column, blocksize);

            llcb(A, n, column + blocksize, column + blocksize + producer_workload,
                 column + blocksize, column + blocksize + producer_workload);

            return;
        }

        // -------------------------------------------------------------------------------------------------------------
        // Distribute R
        // -------------------------------------------------------------------------------------------------------------

        double *R;
        int R_length = column + blocksize;
        int R_stride = (myrank == 0) ? n : R_length;
        R = (myrank == 0) ? &A[column * n] : (double *) malloc(R_length * blocksize * sizeof(double));

        MPI_Datatype Upper_Type;
        MPI_Type_vector(blocksize, R_length, R_stride, MPI_DOUBLE, &Upper_Type);
        MPI_Type_commit(&Upper_Type);

        MPI_Bcast(R, 1, Upper_Type, 0, MPI_COMM_WORLD);

        // -------------------------------------------------------------------------------------------------------------
        // Distribute L
        // -------------------------------------------------------------------------------------------------------------

        double *L;
        int L_length = column + blocksize;
        int L_stride = (myrank == 0) ? n : L_length;
        L = (myrank == 0) ? MPI_IN_PLACE : (double *) malloc(L_length * worker_workload * sizeof(double));

        MPI_Datatype Lower_Type;
        MPI_Datatype Lower_Type_Stride;
        MPI_Type_vector(worker_workload, L_length, L_stride, MPI_DOUBLE, &Lower_Type);
        MPI_Type_create_resized(Lower_Type, 0, L_stride * worker_workload * sizeof(double), &Lower_Type_Stride);
        MPI_Type_commit(&Lower_Type_Stride);

        MPI_Scatter(&A[(n - worker_workload * mpi_size) * n], 1, Lower_Type_Stride, L, 1,
                    Lower_Type_Stride, 0, MPI_COMM_WORLD);

        // -------------------------------------------------------------------------------------------------------------
        // Do cholesky
        // -------------------------------------------------------------------------------------------------------------

        if (myrank == 0) {
            fbllcb(&A[column * n], &A[(column + blocksize) * n], n, producer_workload, column, blocksize);

            llcb(A, n, column + blocksize, column + blocksize + next_blocksize,
                 column + blocksize, column + blocksize + next_blocksize);
        } else {
            fbllcb(R, L, R_length, worker_workload, column, blocksize);
        }

        // -------------------------------------------------------------------------------------------------------------
        // Collect result
        // -------------------------------------------------------------------------------------------------------------


        int result_stride = (myrank == 0) ? n : L_length;
        double *result = (myrank == 0) ? MPI_IN_PLACE : &L[column];

        MPI_Datatype Result_Type;
        MPI_Datatype Result_Type_Stride;
        MPI_Type_vector(worker_workload, blocksize, result_stride, MPI_DOUBLE, &Result_Type);
        MPI_Type_create_resized(Result_Type, 0, result_stride * worker_workload * sizeof(double), &Result_Type_Stride);
        MPI_Type_commit(&Result_Type_Stride);

        MPI_Gather(result, 1, Result_Type_Stride, &A[(n - worker_workload * mpi_size) * n + column], 1, Result_Type_Stride, 0, MPI_COMM_WORLD);

        if (myrank != 0){
            free(R);
            free(L);
        }

        blocksize = next_blocksize;
    }
}



static void kernel_cholesky_main(int n, double *A){
    kernel_cholesky(n, A);
};

static void kernel_cholesky_main_old(int n, double *A) {

    double handle = 0;

    int number_of_workers = mpi_size - 1;
    int blocksize = DEFAULT_BLOCK_SIZE;

    // If main thread is only thread, do whole calculation
    if(number_of_workers < 1){
        llcb(A, n, 0,n,0,n);
        return;
    }

    // Otherwise, perform calculation if upper left block
    llcb(A, n, 0,blocksize,0,blocksize);

    for(int column = 0; column < n; column += blocksize){

        /* Split work equally

            a
            b c
            d e f
            g h i j

        - block a is already done
        - blocks b + c must be calculated by rank 0
        - blocks d, g ff. can be distributed to all ranks
        */


        // TODO: Add calculation here to ramp up block size as we go
        int next_blocksize = blocksize;

        // Number of rows to be calculated in this iteration
        int remaining_work = n - column;

        // Number of rows to be calculated by each rank != 0
        int worker_workload = 0;

        // Case 1, there is enough work to be done, such that rank 0 can help
        if(remaining_work > mpi_size * next_blocksize * 2){
            worker_workload = remaining_work / mpi_size; // This might not divide without rest

        // Case 2, to the end, rank 0 will need to do the majority in order to be able to process the next diagonal
        } else {
            worker_workload = (remaining_work - 2 * next_blocksize) / number_of_workers;
        }
        // Case 3, workers cannot help anymore, everything needs to be done by rank 0
        if (worker_workload < 0){
            worker_workload = 0;
        }
        // Number of rows to be calculated by rank 0
        int producer_workload = n - column - blocksize - worker_workload * number_of_workers;

        printf("[%i] Work distribution - Remaining: %i, producer does %i, %i workers do %i each\n", myrank,
               remaining_work, producer_workload, number_of_workers, worker_workload);

        // TODO: Maybe we can calculate this on worker
        // Broadcast dimensionality of workload to workers
        // Workers will shutdown if worker_workload = 0
        MPI_Bcast(&worker_workload, 1, MPI_INT, 0, MPI_COMM_WORLD);

        // If there is work for the workers, distribute it
        if(worker_workload > 0){

            // Inform workers about desired block size
            MPI_Bcast(&blocksize, 1, MPI_INT, 0, MPI_COMM_WORLD);

            // Create new data type for broadcasting with stride
            MPI_Datatype Upper_Type;
            MPI_Type_vector(blocksize, column + blocksize, n, MPI_DOUBLE, &Upper_Type);
            MPI_Type_commit(&Upper_Type);

            int size;
            MPI_Type_size(Upper_Type, &size);

            //printf("\n[%i] Size of Upper Type is %i\n", myrank, size);

            // Broadcast already calculated part of matrix that is needed for block computation
            MPI_Bcast(&A[column*n], 1, Upper_Type, 0, MPI_COMM_WORLD);

            // Create new data type for scattering with stride
            // TODO: Merge datatypes
            MPI_Datatype Lower_Type;
            MPI_Datatype Lower_Type_Stride;
            MPI_Type_vector(worker_workload, column + blocksize, n, MPI_DOUBLE, &Lower_Type);
            MPI_Type_create_resized(Lower_Type, 0, n * worker_workload * sizeof(double), &Lower_Type_Stride);
            MPI_Type_commit(&Lower_Type_Stride);

            MPI_Scatter(&A[(n - worker_workload * mpi_size) * n], 1, Lower_Type_Stride, MPI_IN_PLACE, 1, Lower_Type_Stride, 0, MPI_COMM_WORLD);

        } else {
            next_blocksize = producer_workload;
        }

        fbllcb(&A[column*n], &A[(column + blocksize) * n], n, producer_workload, column, blocksize);

        llcb(A, n,column + blocksize, column + blocksize + next_blocksize,
                       column + blocksize, column + blocksize + next_blocksize);

        // If we have distributed work to workers, collect it
        if(worker_workload > 0){
            // Create new data type for scattering with stride
            MPI_Datatype Result_Type;
            MPI_Datatype Result_Type_Stride;
            MPI_Type_vector(worker_workload, blocksize, n, MPI_DOUBLE, &Result_Type);
            MPI_Type_create_resized(Result_Type, 0, n * worker_workload * sizeof(double), &Result_Type_Stride);
            MPI_Type_commit(&Result_Type_Stride);

            MPI_Gather(MPI_IN_PLACE, 1, Result_Type_Stride, &A[(n - worker_workload * mpi_size) * n + column], 1, Result_Type_Stride, 0, MPI_COMM_WORLD);
        } else {
            blocksize += remaining_work;
        }
    }


    return;

}

static void kernel_cholesky_other(int n){
    kernel_cholesky(n, NULL);
}

static void kernel_cholesky_other_old(int n) {

    double handle = 0;
    int column = 0;
    int blocksize, unit_workload;


    MPI_Bcast(&unit_workload, 1, MPI_INT, 0, MPI_COMM_WORLD);

    while(unit_workload > 0){

        MPI_Bcast(&blocksize, 1, MPI_INT, 0, MPI_COMM_WORLD);

        //sleep(myrank);

        //printf("[%i] Unit workload is %i\n", myrank, unit_workload);
        //printf("[%i] Block size is %i\n", myrank, blocksize);

        int R_length = column + blocksize;
        int R_size = R_length * blocksize;

        double *R = (double *) malloc(R_size * sizeof(double)); // somehow this does not work (segfault)

        //printf("[%i] Receiving array of size %i\n", myrank, R_size*sizeof(double));

        MPI_Bcast(R, R_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);

        int L_size = (column + blocksize) * unit_workload;
        double *L = (double *) malloc(L_size * sizeof(double)); // somehow this does not work (segfault)

        MPI_Scatter(&handle, 0, MPI_DOUBLE, L, L_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);

        fbllcb(R, L, R_length, unit_workload, column, blocksize);

        MPI_Datatype Result_Type;
        MPI_Datatype Result_Type_Stride;
        MPI_Type_vector(unit_workload, blocksize, R_length, MPI_DOUBLE, &Result_Type);
        MPI_Type_create_resized(Result_Type, 0, R_length*unit_workload*sizeof(double), &Result_Type_Stride);
        MPI_Type_commit(&Result_Type_Stride);

        MPI_Gather(&L[column], 1, Result_Type_Stride, &handle, 0, MPI_DOUBLE, 0, MPI_COMM_WORLD);

        free(R);
        free(L);

        column += blocksize;

        //printf("\n[%i] Get next workload size\n", myrank);

        // Get next workload (terminate if 0)
        MPI_Bcast(&unit_workload, 1, MPI_INT, 0, MPI_COMM_WORLD);
    }

    //printf("\n[%i] Terminating...\n", myrank);

    return;

}

int main(int argc, char** argv) {
    MPI_Init(NULL, NULL);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);

    int n = N;
    int num_runs = NUM_RUNS;
    int do_benchmark = 0;
    int dump_array_term = 0;
    int dump_array_file = 0;
    double *A = NULL;

    char *data_file;
    char *exe_name = (char *) malloc(sizeof(char)*50);

    blocksize = DEFAULT_BLOCK_SIZE;

    if (myrank == 0) {
        handle_args(argc, argv, &n, &dump_array_term, &dump_array_file, &data_file, &do_benchmark, &exe_name, &num_runs, &blocksize);

        A = (double *) malloc(n*n*sizeof(double));

        init_array(n, A);
    }

    MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&num_runs, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&do_benchmark, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&blocksize, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(exe_name, 50, MPI_CHAR, 0, MPI_COMM_WORLD);

    if (myrank == 0) {
        if (do_benchmark) {
            benchmark_fun_mpi(&kernel_cholesky_main, NULL, myrank, n, A, num_runs, exe_name, blocksize);
        } else {
            kernel_cholesky_main(n, A);

            if (dump_array_file) {
                save_array(n, A, data_file);
            }

            if (dump_array_term) {
                //print_array(n, A);
                print_full_array(A, n, n);
            }
        }

        free(A);
    } else {
        if (do_benchmark) {
            benchmark_fun_mpi(NULL, &kernel_cholesky_other, myrank, n, NULL, num_runs, exe_name, blocksize);
        } else {
            kernel_cholesky_other(n);
        }
    }

    MPI_Finalize();

    return 0;
}