#include <stdio.h>
#include <string.h>
#include <liblsb.h>
#include <mpi.h>

#include "benchmark.h"

#define WARMUP_RUNS 2

void benchmark_fun(void (*fun)(int, double *), int n, double *A, int num_runs, char *exe_name) {
    char bench_name [50];
    char size_str[6];
    snprintf(size_str, 6, "%d", n);

    strncpy(bench_name, exe_name, 50);
    strncat(bench_name, ".", sizeof(bench_name) - strlen(bench_name) - 1);
    strncat(bench_name, size_str, sizeof(bench_name) - strlen(bench_name) - 1);

    LSB_Init(bench_name, 0);
    // save information about benchmark
    LSB_Set_Rparam_int("size", n);
    LSB_Set_Rparam_string("region", "cholesky");

    for (int k = 0; k < num_runs; k++) {
        // reset measurement
        LSB_Res();

        fun(n, A);

        LSB_Rec(k);
    }

    LSB_Finalize();
}

void benchmark_fun_mpi(void (*fun_main)(int, double *), void (*fun_other)(int), int myrank, int n, double *A, int num_runs, char *exe_name, int blocksize) {
    double win;
    double err;

    char bench_name [50];
    char size_str[6];
    snprintf(size_str, 6, "%d", n);
    char rank_str[4];
    snprintf(rank_str, 4, "%d", myrank);

    strncpy(bench_name, exe_name, 49);
    strncat(bench_name, ".", sizeof(bench_name) - strlen(bench_name) - 1);
    strncat(bench_name, size_str, sizeof(bench_name) - strlen(bench_name) - 1);
    strncat(bench_name, "_", sizeof(bench_name) - strlen(bench_name) - 1);
    strncat(bench_name, rank_str, sizeof(bench_name) - strlen(bench_name) - 1);

    LSB_Init(bench_name, 0);
    // save information about benchmark
    LSB_Set_Rparam_int("rank", myrank);
    LSB_Set_Rparam_int("size", n);
    LSB_Set_Rparam_int("blocksize", blocksize);
    LSB_Set_Rparam_string("type", "WARMUP");

    // warmup
    for (int k = 0; k < WARMUP_RUNS; k++) {
        if (myrank == 0) {
            fun_main(n, A);
        } else {
            fun_other(n);
        }
    }

    LSB_Set_Rparam_string("type", "MPI");
    for (int k = 0; k < num_runs; k++) {
        //Sync
        MPI_Barrier(MPI_COMM_WORLD);

        // measure time
        LSB_Res();

        if (myrank == 0) {
            fun_main(n, A);
        } else {
            fun_other(n);
        }

        LSB_Rec(k);
    }

    LSB_Finalize();
}