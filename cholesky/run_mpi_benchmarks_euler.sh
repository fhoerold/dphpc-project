#!/bin/bash

for i in {5..13};
do 
	bsub -n 12 -o "$(pwd)/logs.txt" "mpirun $1 -d $((2**i)) -b"
done