# DPHC Project Polybench parallelization

### TODOs
* Read the benchmarking paper.
* Read some papers to get ideas for the parallel implementation of each algorithm.
* Implement the basic algorithm to make sure you understand it.

### Cholesky decomposition
* Sascha, Simeon
* ~~Consider solving using Gram-Schmidt~~. Could be a cool idea to combine both projects.
    * Gram-Schmidt can be implemented with Cholesky

### Gram-Schmidt orthogonalization
* Jonas, Marc, Faveo
* Numerical stability
* Sparseness
* Consider solving using Cholesky. Could be a cool idea to combine both projects.

### Benchmarking
* Write some kind of benchmarking library using Thorsten's paper. (See below)
** Use [LibSciBench](https://github.com/spcl/liblsb)
* seaborn vs matplotlib for graphs

## Cheatsheet

### Deadlines
| Date | Todo |
|-|-|
| Before October 18 | Find team (let TA know names). Find project. If you have a suggestion send email with topic and rough plan and references to TAs and lecturer for approval. Note that this may take more than one iteration. |
| October 18 | You have a team and an approved project. |
| Before November 24 | Mandatory progress presentation. A short presentation on the status of the project It should include a brief summary what was done, how the work was divided, and initial results. (It should include half the mpi implementation and preliminary test results for that implementation - from first meeting) |
| Last week of semester (December 20-24) | Project presentations with project supervisor. |
| January 15 | Project reports due.|

Info from [Course Webpage](http://spcl.inf.ethz.ch/Teaching/2021-dphpc/)

### Report
- Hard page limit: 6 pages.
- Everybody reads [this](http://spcl.inf.ethz.ch/Teaching/2021-dphpc/report.pdf) and [this](http://spcl.inf.ethz.ch/Teaching/2021-dphpc/hoefler-scientific-benchmarking.pdf).
- For latex use: [report.zip](http://spcl.inf.ethz.ch/Teaching/2021-dphpc/report.zip).

Info from [Course Webpage](http://spcl.inf.ethz.ch/Teaching/2021-dphpc/)

### General Links
- [Polybench](http://web.cse.ohio-state.edu/~pouchet.2/software/polybench/)
- [Scientific Benchmarking of Parallel Computing Systems](https://htor.inf.ethz.ch/publications/img/hoefler-scientific-benchmarking.pdf)
- [On the Parallel I/O Optimality of Linear Algebra Kernels](https://arxiv.org/pdf/2108.09337.pdf)
- [NPBench: a benchmarking suite for high-performance NumPy](https://scholar.google.com/citations?view_op=view_citation&hl=en&user=8b6ICngAAAAJ&citation_for_view=8b6ICngAAAAJ:9yKSN-GCB0IC)
- [Project Proposals](https://docs.google.com/document/d/1IXCbJXbaR37X2ULuXYp1Q41qL8Kb6aZvYmtqJpiAqKs/edit#)
- [Repo](https://gitlab.ethz.ch/fhoerold/dphpc-project/-/tree/main)
- [Polybench Files from Course](https://moodle-app2.let.ethz.ch/mod/resource/view.php?id=655148)

### Benchmarking paper
* [Scientific Benchmarking of Parallel Computing Systems](https://htor.inf.ethz.ch/publications/img/hoefler-scientific-benchmarking.pdf)

### Other relevant papers
* [On the Parallel I/O Optimality of Linear Algebra Kernels: Near-Optimal Matrix Factorizations](https://arxiv.org/pdf/2108.09337.pdf)
* [NPBench: a benchmarking suite for high-performance NumPy](https://scholar.google.com/citations?view_op=view_citation&hl=en&user=8b6ICngAAAAJ&citation_for_view=8b6ICngAAAAJ:9yKSN-GCB0IC)
* [Productivity, Portability, Performance: Data-Centric Python](https://scholar.google.com/citations?view_op=view_citation&hl=en&user=8b6ICngAAAAJ&citation_for_view=8b6ICngAAAAJ:2osOgNQ5qMEC)

### Parallel Gram-Schmidt Orthogonalization
* [CPU vs. GPU - Performance comparison for the Gram-Schmidt algorithm](https://link.springer.com/content/pdf/10.1140/epjst/e2012-01638-7.pdf)
* [Efficient Parallel Implementation of Classical Gram-Schmidt Orthogonalization Using Matrix Multiplication](http://pmaa06.irisa.fr/papers_copy/41.pdf)
* [ParallelCGS: Parallel classical Gram-Schmidt algorithm with reorthogonalization (CGS-RO)](https://github.com/Adam85/ParallelCGS)
* [Performance Evaluation of Parallel Gram-Schmidt Re-Orthogonalization Methods](https://www.researchgate.net/publication/2495662_Performance_Evaluation_of_Parallel_Gram-Schmidt_Re-Orthogonalization_Methods)
* [Gram-Schmidt Orthogonalization](http://www.math.usm.edu/lambers/mat610/class0215.pdf)
* [A Stable and Efficient Parallel Block Gram-Schmidt Algorithm](https://link.springer.com/content/pdf/10.1007%2F3-540-48311-X_158.pdf)

### Literature / Sources
- [Some paper](https://letmegooglethat.com/?q=some+paper)

## Project Description

### Goal
Create parallel versions of Polybench benchmarks and evaluate them.

### Description
Select two (or more) of the available Polybench benchmarks with the exception of the 2mm and 3mm samples. These are small benchmarks, of ~100-300 lines of C code each. They can be found here: [Polybench](http://web.cse.ohio-state.edu/~pouchet.2/software/polybench/)

A [zip](https://moodle-app2.let.ethz.ch/mod/resource/view.php?id=655148) with individual C files ready to compile can also be found in Moodle for your convenience.

Your task is to parallelize the selected benchmarks using at least two parallel programming paradigms, then measure the runtime using different configurations.
Compare the speed-ups you obtain.

Remember: a thorough benchmarking and evaluation is essential [Scientific Benchmarking of Parallel Computing Systems](https://htor.inf.ethz.ch/publications/img/hoefler-scientific-benchmarking.pdf)

Multiple benchmarks solve similar problems in similar ways – one parallel solution might work for more than one benchmark.

Info from [Project Proposal](https://docs.google.com/document/d/1IXCbJXbaR37X2ULuXYp1Q41qL8Kb6aZvYmtqJpiAqKs/edit#)

### Overall Hints

You have a baseline to compare to: if you are implementing something that cannot be compared to anything, ask yourself why? Are you maybe solving a problem nobody cares about?
You have enough depth and breadth in your project: the worst thing that can happen is that you finish implementing what you wanted to implement in two weeks, find it to be 10% faster than your baseline and now you have no ideas what to do further. This happens often if your project is very constrained, so make sure you leave room for future work, i.e., try different ways to implement the same algorithm, or different algorithms within the same framework, to solve a problem

very state-of-the art: [On the Parallel I/O Optimality of Linear Algebra Kernels](https://arxiv.org/pdf/2108.09337.pdf)
automatic parallel: [NPBench: a benchmarking suite for high-performance NumPy](https://scholar.google.com/citations?view_op=view_citation&hl=en&user=8b6ICngAAAAJ&citation_for_view=8b6ICngAAAAJ:9yKSN-GCB0IC)

Info from [Project Proposal](https://docs.google.com/document/d/1IXCbJXbaR37X2ULuXYp1Q41qL8Kb6aZvYmtqJpiAqKs/edit#)

- Philosophy: develop a parallel implementation that scales well on multicore
- Includes thorough benchmarking and experimental evaluation. Follow good benchmarking practice, e.g., “Scientific Benchmarking of Parallel Computing Systems”
- Make it a team experience – leadership, team spirit. This is an important part of the learning experience!
- You are in charge of the project: shrink or expand as necessary! Make sure to stick to your timeline!

Info from [Course Slide 0](https://moodle-app2.let.ethz.ch/pluginfile.php/1195051/mod_resource/content/2/lecture0-organization.pdf)

### Polybench Description
PolyBench is a collection of benchmarks containing static control parts. The purpose is to uniformize the execution and monitoring of kernels, typically used in past and current publications. PolyBench features include:
- A single file, tunable at compile-time, used for the kernel instrumentation. It performs extra operations such as cache flushing before the kernel execution, and can set real-time scheduling to prevent OS interference.
- Non-null data initialization, and live-out data dump.
- Syntactic constructs to prevent any dead code elimination on the kernel.
- Parametric loop bounds in the kernels, for general-purpose implementation.
- Clear kernel marking, using `#pragma scop` and `#pragma endscop` delimiters.

Info from: [Polybench Website](http://web.cse.ohio-state.edu/~pouchet.2/software/polybench/#description)
 
## Meetings

### Mail exchange prior to first meeting

You should select at least 2 from different classes (such that the parallelization is not identical) You could then expand to more samples from the same class and try to adapt the scheme you developed for the first 2 if time permits.

### First Meeting on 18.10.2021

- Grade only comes from the report at the end:
    - half of the report should be about evaluation
    - Additional: code delivery and presentation 
- Work on at least:
    - 2 paradigms. 
        - Strongly suggested to start with MPI since it needs the development of an algorithm. This should then be tested on Euler (Maybe can ask Timo for better access). Running on Euler might bring its own bugs.
        - OpenMP easier, has [SIMD directives](https://www.openmp.org/spec-html/5.0/openmpsu42.html)
        - Both can be combined (though some students had better results only using MPI)
        - Other paradigms also possible
    - 2 samples that are from different classes (i.e. are different enough).
        - impress by expanding the algorithm for those two samples to other, similar, samples.
        - note: often good that the one developing the algorithm is not the one evaluating it.
        - look at literature. use a combination or optimization others did not found
- Samples chosen during the meeting:
    - `cholesky` Cholesky Decomposition
    - `gramschmidt` Gram-Schmidt decomposition
- This topic is interesting because:
    - Alexandru works on automatic parallelization of polybench algorithms
    - Our manuell parallelization can then be compared to the automatic ones
    - If there are a lot of manuell parallelization of polybench algorithms one can search for common patterns.
    - [State of the art algorithm](https://arxiv.org/pdf/2108.09337.pdf)
    - [Automatic Parallelization from Alexandru](https://scholar.google.com/citations?view_op=view_citation&hl=en&user=8b6ICngAAAAJ&citation_for_view=8b6ICngAAAAJ:9yKSN-GCB0IC)
    - [Second version thereof](https://scholar.google.com/citations?view_op=view_citation&hl=en&user=8b6ICngAAAAJ&citation_for_view=8b6ICngAAAAJ:2osOgNQ5qMEC)
- Other:
    - One person should develop code and then another should evaluate it, checks & balances / sanity check
- Presentation on the 24th of november should include:
    - half the mpi implementation (SH: I think he said we should _have_ the MPI implementation)
    - preliminary test results for that implementation
    
### DPHPC Presentation Project Polybench 
### Thursday 25.11 at 13.30

### Slides 
https://docs.google.com/presentation/d/1nvvRhMcXOq5qBwWKUm-5vmx4nFqvYonNemCrvm6FJNE/edit?invite=CLSymP0G#slide=id.p

### Structure

#### Cholesky

* Approach
	* // Papers
	* Baselines -> Polybench / BLAS
	* Pipeline #SH

* Algorithms #SF
	* Versions (Left/Right)
	* Left Looking
	* Baseline

* Results
	* Roofline #SF
	* Runtime/Performance Plot #SH

* Next Steps #SH
	* Right Looking 
	* Blocked
	* OpenMP
	* BLAS

* Current Blockers
	* 

#### Gram-Schmidt

* Algorithms
	* Iterative Classical (MPI)
	* Modified (Polybench, MPI)
	* Blocking Modified

* Results
	* Runtime/Performance Plot

* Next Steps
	* Blocked MPI
	* OpenMP

* Current Blockers
	* Magical speedups
	* Polybench challenges

### TODOs before Presentation

* Gram Schmidt benchmarking


### Questions for Alexandru

* Zeitrahmen? Ort? Struktur?
* Wie genau müssen wir uns an die Vorgaben halten bei Gram Schmidt?
	* Do we need to have the same computations, the same numerical stability or only the criterion of computing an orthonormal basis Q + R

### Comments from Alexandru

Next meeting last week of December

#### Cholesky
- dont use log for speedup plots
- focus on large problem size (more realistic scenario)
- if we curious: how many float operations -> find rate operations per element
	- make regression on data you have -> look if theoretical model and empirical data coincide -> extra-p is a tool from alexandru -> much more intuitive way of performance verification "insight of data"
	OPs/Element, check analytical expectation vs. experimental result (EXTRA-P tool) "Performance verification, prove optimality"
	- performance modeling: e.g runtime T(n) function of problem size or T(p) of #processes
- cholesky naive plateau at around 16 and at around 32 (inflection point, at some point slowdown, find this point, even more interested in plots where double process = double problem size (weak scaling) -> shows communication overhead)
- there should be a reason for log-log over linear-linear (needed here to show the whole range but better linear for 4000x4000 or 8000x8000)
- blocked MPI and OpenMP more interesting than right-looking MPI
- full node less variance but can be promised that we get them

#### Gram-Schmidt
- not numerical stable, perfectly fine to not have same results, just have a delta 
- as long as the result is in some delta its good
- orginal baseline in the paper state into paper, but sure, optimized sequential version is also nice to have
- we have to compute Q and R, still important to replicate polybench result

### Comments from Alexandru on second presentation
Notes second presentation - Comments from Alex

Cholesky
- weak-scaling: send revise plots, ideally this week. current "weak-scaling plots" are far of the norm and reader will not understand
- roofline: make clear what point on each line mean. why hick in that line means would be nice to know
- relative speedup to seq.: 
	- explanation that less efficiency for mpi from 16 to 32 processor is due to different cores not valid since not the same problem with openmp. 
	- telling why not performant is very valuable. Course should have taught us. By understanding this even improving bottleneck is great.
	- for SIMD use seq SIMD (openMP for simd) as baseline
- declare that one process holds whole matrix through the whole time
- use microbenchmarks to test hypothesis on performance

Gramschmidt
- cant duplicate complete array on multiple processes
	- one process that reads data, scatters (no one has all data) and at the end gather to one process who writes to file.
	- we need to be explicit declare this. not forced to rewrite everything
	- need to declare bottleneck was memory movement, that is why we duplicate R (array with small 
	- best thing u can do is argue and prove why the bottleneck is memory movement and not memory amount
- speedup-relative to sequential versions: he likes the slide
	- he is suprised by how bad blocking did work -> need good explanation
	- classical-gram-mpi is best result in his view
- also false weak-scaling slide
- biggest thing missing: false weak-scaling plots and understand our data/plots
- dont need to present plots for all versions of the algorithm, no need to detail in all versions except if lessons are being learned from it.

Notes on bottleneck: hierarchy of bottleneck inside clusters is: amount of ram / data movement inside core / network movement / cpu usage
Polybench interested in start benchmarking as soon as everything is in memory (warm). Need to decide and declare what we did.

Real world applications would use MPI-I/O, make clear in report how we do it in our code