import os, sys
import numpy as np
import math

dirname = os.path.dirname(os.path.abspath(__file__))

eps = 0.0

cholesky_ref_path = os.path.join(dirname, "cholesky/ref_output/cholesky_ref_128.dat")

with open(cholesky_ref_path) as cholesky_ref_file:
    cholesky_ref_data = cholesky_ref_file.read()
cholesky_ref = np.fromstring(cholesky_ref_data, sep=' ')

any_fail = False
for filename in os.listdir(os.path.join(dirname, "cholesky/test_outputs")):
    cholesky_out_path = os.path.join(dirname, "cholesky/test_outputs/" + filename)
    with open(cholesky_out_path) as cholesky_out_file:
        cholesky_out_data = cholesky_out_file.read()
    cholesky_out = np.fromstring(cholesky_out_data, sep=' ')

    mse = np.square(cholesky_ref - cholesky_out).mean()

    print(filename + ": MSE = %f and eps = %f" % (mse, eps))

    if (math.isnan(mse) or mse > eps):
        any_fail = True

if (any_fail):
    sys.exit(1)
else:
    sys.exit(0)
