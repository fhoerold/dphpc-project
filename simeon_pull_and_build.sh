#!/bin/sh

DIR="/cluster/home/sihaefliger/projects/dphpc-project"

if [ -d "$DIR" ]; then
	echo "Project directory found at ${DIR}"
	cd "$DIR"
	git pull
	rm -rf ./cholesky/build/
	mkdir ./cholesky/build/
	cd ./cholesky/build/
	cmake -DCMAKE_C_COMPILER=mpicc -DCMAKE_BUILD_TYPE=RELEASE ..
	make
else
	echo "Error: ${DIR} not found. Can not continue."
	exit 1
fi