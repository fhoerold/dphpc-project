# Overleaf git setup

We use git to push graphs from the pipeline to the report. Make sure you follow this guide to not break the pipeline

### Set account password

In order to clone an overleaf project with your ethz account, you need to set an account password first

* Navigate to https://www.overleaf.com/user/settings
* Click Reset Password
* You receive an email by overleaf to set a new password

### Clone overleaf repo

Because submodules are a bit tricky, we go the route of cloning the report side by side to our dphpc-project repo.

The pipeline expects the following folder structure

```
path/to/your/repos
└─ dphpc-project
└─ dphpc-report
```

To clone the report, use

```
cd path/to/your/repos
git clone https://git.overleaf.com/61addd7d10a871dc7b342eeb dphpc-report
```

Login with your full email address + password from previous step